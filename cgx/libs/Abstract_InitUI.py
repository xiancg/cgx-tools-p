# -*- coding: utf-8 -*-
'''
Abstract class to initialize gui components

Created on Apr 08, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import cgx.core
import abc


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Abstract init ui components
##--------------------------------------------------------------------------------------------
class Abstract_InitUI(object):
    __metaclass__ = abc.ABCMeta
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _studioInfo, _projectInfo, _folderStructure=cgx.core.FOLDER_STRUCTURE):
        '''
        Abstract init ui class. Get info from specific applications and set it to the gui components.
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param _folderStructure: Absolute path to the json template file for the project folder structure.
        :type _folderStructure: string
        '''
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##-------------------------------------------------------------------------------------------- 
    def setCurrentProject(self, _projectComponent):
        '''
        Sets project component to item.
        
        Example: _projectComponent.setToItem(self.pInfo.maya_getProject())
        
        :param _projectComponent: Gui component to set the project on.
        :type _projectComponent: Project_Component
        '''
        pass
        
    
    def setCurrentSequence(self, _sequenceComponent):
        '''
        Sets sequence component to item.
        
        Example: _sequenceComponent.setToItem(self.pInfo.maya_getSequence())
        
        :param _sequenceComponent: Gui component to set the project on.
        :type _sequenceComponent: Sequence_Component
        '''
        pass
        
    
    def setCurrentShot(self, _shotComponent):
        '''
        Sets shot component to item.
        
        Example: _shotComponent.setToItem(self.pInfo.maya_getShot())
        
        :param _shotComponent: Gui component to set the project on.
        :type _shotComponent: Shot_Component
        '''
        pass
    
    
    def setCurrentAssetType(self, _assetTypeComponent):
        '''
        Sets asset type component to file if possible.
        :param _assetTypeComponent: Gui component to set the asset type on.
        :type _assetTypeComponent: AssetTypes_Component
        '''
        pass
    
    
    def setCurrentAsset(self, _assetComponent):
        '''
        Sets asset component to file if possible.
        :param _assetComponent: Gui component to set the asset on.
        :type _assetComponent: Shot_Component
        '''
        pass
        
    
    def setCurrentSubAsset(self, _subassetComponent):
        '''
        Sets subasset component to file if possible.
        :param _subassetComponent: Gui component to set the subasset on.
        :type _subassetComponent: SubAssets_Component
        '''
        pass
    
    
    def setCurrentFileType(self, _fileTypeComponent):
        '''
        Sets file type component to file shot if possible.
        :param _fileTypeComponent: Gui component to set the subasset on.
        :type _fileTypeComponent: FileTypes_Component
        '''
        pass
    
          
    def checkFPS(self, _FPSComponent):
        '''
        Updates FPS_Component to red if FPS from project config file and app do not match. Black if they do match. 
        
        Example: _FPSComponent.componentUpdate(self.pInfo.maya_getFPS())
        
        :param _FPSComponent: Gui component to update
        :type _FPSComponent: FPS_Component
        '''
        