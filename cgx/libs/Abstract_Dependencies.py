# -*- coding: utf-8 -*-
'''
Abstract class dependencies.

Created on Mar 25, 2015

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import abc


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Abstract Dependencies
##--------------------------------------------------------------------------------------------
class Abstract_Dependencies(object):
    __metaclass__ = abc.ABCMeta
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self):
        self.__allDependencies = []
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    @abc.abstractmethod
    def fileAttrChange(self, _filePath, _node, _type):
        '''
        Change the file path attribute for the given node.
        :param _filePath: Path to the file
        :type _filePath: string
        :param _node: Node name or object
        :type _node: string
        :param _type: Node type
        :type _type: string
        '''
        pass
    
    
    ##--------------------------------------------------------------------------------------------
    ##Properties
    ##--------------------------------------------------------------------------------------------
    @property
    def allDependencies(self):
        for thisProperty, value in vars(self).iteritems():
            propertyName = thisProperty.split("_")[-1]
            if not propertyName == "allDependencies":
                propertyDeps = eval("self." + propertyName)
                self.__allDependencies += propertyDeps
        return self.__allDependencies


def main():
    pass

if __name__=="__main__":
    main()