# -*- coding: utf-8 -*-
'''
Abstract camera tools library.

Created on Sep 15, 2015

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import abc
import Abstract_TimeRange as tRange
from cgx.libs.Abstract_InitUI import Abstract_InitUI


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Abstract Camera Tools class
##--------------------------------------------------------------------------------------------
class Abstract_CameraTools(object):
    __metaclass__ = abc.ABCMeta
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _studioInfo, _projectInfo, _cam, _path, _sampleBy = 1, _timeRange = tRange.Abstract_TimeRange()):
        '''
        Abstract_CameraExporter object constructor. To bake and export cameras to different formats.
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param _cam: Camera to be exported.
        :type _cam: object
        :param _path: (Optional) Where to export the baked cameras.
        :type _path: object
        :param _sampleBy: (Optional) Sample animation every X frames. Default= 1
        :type _sampleBy: float
        :param _timeRange: (Optional) Abstract_TimeRange object instance. Default= tRange.Abstract_TimeRange()
        :type _timeRange: Abstract_TimeRange
        '''
        self.__path = _path
        if "\\" in self.path:
            self.path = self.path.replace("\\","/")
        self.__cam = _cam
        self.__sampleBy = _sampleBy
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        self.timeRange = _timeRange
        self.app_initUI = Abstract_InitUI(self.sInfo, self.pInfo)
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    @abc.abstractmethod
    def exportCamera(self, _cam, _fileName, **kwargs):
        '''
        Export camera.
        :param _cam: Camera to be exported
        :type _cam: object
        :param kwargs: File formats to be exported. Ex.: _ma=True, _mb=True, _fbx=True, _abc=True
        :type kwargs: dict
        :return: Exported any of the formats or not
        :rtype: boolean
        '''
        return
            
            
    @abc.abstractmethod
    def bakeCamera(self, _name, _offsetStartFrame= False, _newStartFrame = 1):
        '''
        Bake camera animation, including focal length
        :param _name: New camera name
        :type _name: string
        :param _offsetStartFrame: (Optional) Offset entire animation range. Default= False
        :type _offsetStartFrame: boolean
        :param _newStartFrame: (Optional) Starting frame for offset. Default= 1
        :type _newStartFrame: float
        :return: New camera with baked animation
        :rtype: object
        '''
        return
    
    
    @abc.abstractmethod
    def listCams(self):
        '''
        List all cams in current project.
        :return: Cams list
        :rtype: list
        '''
        return []
    
    
    @abc.abstractmethod
    def renameCam(self, _cam, _newName):
        '''
        Rename given cam to given name.
        :param _cam: Original camera
        :type _cam: string
        :param _newName: Name for cam
        :type _newName: string
        '''
        pass
    
    
    @abc.abstractmethod
    def selectCams(self, _camList):
        '''
        Select given cam list in the application itself.
        :param _camList: List of cameras obtained from a tool gui.
        :type _camList: list
        '''
        pass
    
    
    @abc.abstractmethod
    def createCameraFormatsComponent(self, _mainWindow, _parent):
        '''
        Create Camera Formats for the specific application. 
        
        Example: CameraFormats_Component(_mainWindow, _parent)
        
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtGui.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtGui.QWidget
        :return: Component
        :rtype: CameraFormats_Component
        '''
        return 
    
    
    @abc.abstractmethod
    def refreshViewport(self, _bool):
        '''
        Stops viewport refreshing to speed up operations.
         :param _bool: True will stop refresh, False will restart refresh.
        :type _bool: boolean
        '''
        pass
    
    
    def buildCameraPath(self, _sequence, _shot):
        '''
        Given a sequence name and a shot name, rebuild camera export path.
        :param _sequence: Sequence name retrieved from the tool gui.
        :type _sequence: string
        :param _shot: Shot name retrieved from the tool gui.
        :type _shot: string
        :return: Absolute path to camera final location. None if path was impossible to rebuild.
        :rtype: string
        '''
        propertiesDict = {"abstractFolderType":"leaffolder",
                          "concreteFolderType":"camera_leaffolder",
                          "folderFunction":"published_content",
                          "folderConcreteFunction":"published_camera",
                          "placeholder":False}
        camNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)[0]
        if camNode != None:
            seqDict={"abstractFolderType":"subfolder",
                    "concreteFolderType":"sequence_subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
            shotDict= {"abstractFolderType":"subfolder",
                    "concreteFolderType":"shot_subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
            pathList = self.pInfo.rebuildPath(camNode, {_sequence:seqDict,_shot:shotDict})
            pathList.append(camNode.name)
            #Build path string
            server = self.sInfo.getProjectsFrom()
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
            return pathStr
        else:
            return None
    
    
    ##--------------------------------------------------------------------------------------------
    ##Properties
    ##--------------------------------------------------------------------------------------------  
    @property
    def path(self): 
        return self.__path
    @path.setter
    def path(self, p):
        self.__path = p
    
    @property
    def cam(self):
        return self.__cam
    @cam.setter
    def cam(self, c):
        self.__cam = c
    
    @property
    def sampleBy(self):
        return self.__sampleBy
    @sampleBy.setter
    def sampleBy(self, s):
        self.__sampleBy = s
        

##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass

if __name__=="__main__":
    main()