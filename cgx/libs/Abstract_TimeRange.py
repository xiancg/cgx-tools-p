# -*- coding: utf-8 -*-
'''
Time range abstract management library.

Created on Mar 25, 2015

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import abc


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Abstract Time Range
##--------------------------------------------------------------------------------------------
class Abstract_TimeRange(object):
    __metaclass__ = abc.ABCMeta
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self):
        self.__startFrame = 1
        self.__endFrame = 24
        self.__handleIn = 0
        self.__handleOut = 2
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def getTimeSlider (self):
        '''
        Gets the time slider start and end frames and returns them.
        :return: [startFrame,endFrame]
        :rType: list
        '''
        return [0,24]
    
    
    def setFramesToTimeSlider(self):
        '''
        Sets the state of start and end frames to given time slider data.
        '''
        thisTimeRange = self.getTimeSlider()
        if thisTimeRange[0] > self.endFrame:
            self.endFrame = thisTimeRange[1]
            self.startFrame = thisTimeRange[0]
        else:
            self.startFrame = thisTimeRange[0]
            self.endFrame = thisTimeRange[1]
            
    
    def offsetTimeRange(self, _frame):
        '''
        Returns an offseted version of all TimeRange attributes to have startFrame as _frame and keep time relationships.
        :param _frame: Calculate time relationships from this startFrame
        :type _frame: float
        :return: {'absoluteStartFrame':float, 'absoluteEndFrame':float, 'starFrame':float, 'endFrame':float}
        :rType: dict
        '''
        newStartFrame = _frame
        newEndFrame = newStartFrame + (self.endFrame - self.startFrame)
        newAbsolutStartFrame = newStartFrame - self.handleIn
        newAbsolutEndFrame = newEndFrame + self.handleOut
        if newStartFrame > self.startFrame:
            timeChange = newStartFrame - self.startFrame
        elif newStartFrame < self.startFrame:
            timeChange = (self.startFrame - newStartFrame)*-1 
        else:
            timeChange = 0
        
        return {'absoluteStartFrame':newAbsolutStartFrame,
                'absoluteEndFrame':newAbsolutEndFrame,
                'startFrame':newStartFrame,
                'endFrame':newEndFrame,
                'timeChange':timeChange}
        
    
    ##--------------------------------------------------------------------------------------------
    ##Properties
    ##--------------------------------------------------------------------------------------------
    @property
    def startFrame(self):
        return self.__startFrame
    @startFrame.setter
    def startFrame(self, n):
        self.__startFrame = n
    
    @property
    def endFrame(self):
        return self.__endFrame
    @endFrame.setter
    def endFrame(self, n):
        self.__endFrame = n
    
    @property
    def handleIn(self):
        return self.__handleIn
    @handleIn.setter
    def handleIn(self, n):
        self.__handleIn = n
        
    @property
    def handleOut(self):
        return self.__handleOut
    @handleOut.setter
    def handleOut(self, n):
        self.__handleOut = n
    
    @property
    def absoluteStartFrame(self):
        return self.startFrame - self.handleIn
    
    @property
    def absoluteEndFrame(self):
        return self.endFrame + self.handleOut


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass


if __name__=="__main__":
    main()