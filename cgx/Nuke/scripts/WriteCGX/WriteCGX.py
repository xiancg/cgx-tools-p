# -*- coding: utf-8 -*-
'''
Custom write gizmo for Nuke.

Created on Aug 31, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

##--------------------------------------------------------------------------------------------
##Imports
##--------------------------------------------------------------------------------------------
import nuke, os, shutil, errno
from cgx.core.Synchronizer import Synchronizer

##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Functions
##--------------------------------------------------------------------------------------------
def createWriteDir():
    filename = nuke.filename(nuke.thisNode())
    directory = os.path.dirname( filename )
    osdir = nuke.callbacks.filenameFilter( directory )
    # cope with the directory existing already by ignoring that exception
    try:
        os.makedirs( osdir )
    except OSError, e:
        if e.errno != errno.EEXIST:
            raise
    nuke.addBeforeRender(createWriteDir)


def renderOutputModule(_readNode, startFrame, endFrame, increment):
    _readNode.knob('reload').execute()
    for childNode in _readNode.dependent(nuke.INPUTS, forceEvaluate=True):
        nuke.execute(childNode.name(), int(startFrame), int(endFrame), int(increment))


def writeCGXExecute(_node):
    _node.knob('Render_EXR').execute()
    if _node.knob('publish').value():
        publishRender(_node)
    renderOutputModule(_node.node('exrHalfFloatRead'), _node.knob('customFrameRange_first').value(), _node.knob('customFrameRange_last').value(), 1)
    
    

def publishRender(_node):
    sync = Synchronizer()
    syncStatusDict = {1:"Already published",
                    2:"Published but different",
                    3:"Same date, different size",
                    4:"Different date, same size",
                    5:"Both files missing",
                    6:"Not Published",
                    7:"Current file missing"}
    sync.statusDict = syncStatusDict
    publishPathNode = _node.node('PublishPath_NoOp')
    source = publishPathNode.knob('versionedPathWithExtension').value()####NO LE ESTOY PASANDO EL NOMBRE DE UN ARCHIVO EXISTENTE Y POR ESO NO FUNCIONA
    sourceFolder = source.rsplit('/',1)[0]
    sourceFilesAndDirs = os.listdir(sourceFolder)
    for each in sourceFilesAndDirs:
        if os.path.isfile(os.path.join(sourceFolder, each)):
            if each[-3:] == 'exr':
                source = sourceFolder + '/' + each
                break
    target = publishPathNode.knob('publishPath').value()
    
    
    trgFolderPath = target.rsplit("/", 1)[0]
    
    #Create directory
    if not os.path.exists(trgFolderPath):
        try:
            os.makedirs(trgFolderPath)
            sync.log.append("Directory structure created: {}".format(trgFolderPath))
        except [WindowsError, OSError]:
            sync.log.append("Directory structure couldn't be created: {}".format(trgFolderPath))
    
    filesAndDirs = os.listdir(trgFolderPath)
    #Empty directory first
    if len(filesAndDirs) >= 1:
        try:
            shutil.rmtree(trgFolderPath)
            sync.log.append("Directory removed {}".format(trgFolderPath))
        except [WindowsError, OSError]:
            sync.log.append("File currently in use. Couldn't be removed {}".format(trgFolderPath))
    
    #Re-Create directory
    if not os.path.exists(trgFolderPath):
        try:
            os.makedirs(trgFolderPath)
            sync.log.append("Directory structure created: {}".format(trgFolderPath))
        except [WindowsError, OSError]:
            sync.log.append("Directory structure couldn't be created: {}".format(trgFolderPath))
                
    #Copy files            
    sync.processPaths(source, target, isSequence=True)
    #Rename files
    filesAndDirs = os.listdir(trgFolderPath)
    fileList = []
    for each in filesAndDirs:
        if os.path.isfile(os.path.join(trgFolderPath, each)):
            fileList.append(trgFolderPath + '/' + each)
    for each in fileList:
        seq = publishPathNode.knob('seq').value()
        shot = publishPathNode.knob('shot').value()
        nameParts = each.rsplit('/',1)
        fileName = nameParts[1]
        dirPath = nameParts[0]
        fileNameParts = fileName.rsplit('.',1)
        baseNameWithDigits = fileNameParts[0]
        extension = fileNameParts[1]
        digitsNumber = 0
        for char in baseNameWithDigits[::-1]:
            if char.isdigit() or char == '#':
                digitsNumber += 1
            else:
                break
        frameNumber = baseNameWithDigits[-digitsNumber:]
        newFileName = seq + '_' + shot + '.' + frameNumber + '.' + extension
        newName = dirPath + '/' + newFileName
        try:
            os.rename(each, newName)
            sync.log.append("File renamed: {}".format(each))
        except [WindowsError, OSError]:
            sync.log.append("File already exists: {}".format(each))
    print sync.log
    
    
    
    
    
    