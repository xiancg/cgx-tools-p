# -*- coding: utf-8 -*-
'''
Project properties tool.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import sys
if __DEVMODE:
	sys.path.append('M:/CGXtools')

import nuke, nukescripts, os
from cgx.core.JSONManager import JSONManager
from cgx.core.StudioInfo import StudioInfo
from cgx.Nuke.scripts.nuke_libs.Nuke_ProjectInfo import Nuke_ProjectInfo


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Nuke/scripts/ProjectProperties'


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class ProjectProperties(nukescripts.PythonPanel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self):
		nukescripts.PythonPanel.__init__(self, 'Set Project Properties')
		self.sInfo = StudioInfo()
		
		projsList = self.sInfo.getProjects("", 'Comp', _department='Comp')
		self.typeKnob = nuke.Enumeration_Knob('element', 'Projects', projsList)
		self.typeKnob.setValues(projsList)
		self.addKnob(self.typeKnob)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setProperties(self):
		pInfo = Nuke_ProjectInfo(self.sInfo, self.typeKnob.value())
		config_json = pInfo.getProjConfig('Comp')
		self.configFile = JSONManager(config_json, self)
		#FPS
		rootNode = nuke.toNode('root')
		rootNode['fps'].setValue(self.configFile.getValueByProperty('fps', False))
		#COLORSPACE
		rootNode['monitorLut'].setValue(self.configFile.getValueByProperty('colorSpace', False))
		rootNode['defaultViewerLUT'].setValue(self.configFile.getValueByProperty('colorSpace', False))
		viewerNode = nuke.toNode('Viewer1')
		viewerNode['viewerProcess'].setValue(self.configFile.getValueByProperty('colorSpace', False))
		#RESOLUTION
		if self.configFile.getValueByProperty('frameWidth', False) == 1920 and self.configFile.getValueByProperty('frameHeight', False) == 1080:
			rootNode['format'].setValue('HD_1080')
		elif self.configFile.getValueByProperty('frameWidth', False) == 1280 and self.configFile.getValueByProperty('frameHeight', False) == 720:
			rootNode['format'].setValue('HD_720')
		elif self.configFile.getValueByProperty('frameWidth', False) == 3840 and self.configFile.getValueByProperty('frameHeight', False) == 2160:
			rootNode['format'].setValue('UHD_4K')
		elif self.configFile.getValueByProperty('frameWidth', False) == 1024 and self.configFile.getValueByProperty('frameHeight', False) == 1024:
			rootNode['format'].setValue('square_1K') 
		elif self.configFile.getValueByProperty('frameWidth', False) == 2048 and self.configFile.getValueByProperty('frameHeight', False) == 2048:
			rootNode['format'].setValue('square_2K')


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	panel = ProjectProperties()
	if panel.showModalDialog():
		panel.setProperties()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()