# -*- coding: utf-8 -*-
'''
Force reload on all read nodes in the script.

Created on Feb 06, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import nuke
import nukescripts


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Force reload on all read nodes in the script.
##--------------------------------------------------------------------------------------------
def reloadRead():
    nukescripts.clear_selection_recursive()
    nuke.selectAll()
    SelNodes = nuke.selectedNodes()
    for node in SelNodes:
        if node.Class() == 'Read':
            node.knob('reload').execute()
            print node.name()


##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------
def main():
    reloadRead()


if __name__=="__main__":
    main()
