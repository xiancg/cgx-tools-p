# -*- coding: utf-8 -*-
'''
Create one shuffle node for each RGB channel.

Created on Feb 06, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import nuke


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Create one shuffle node for each RGB channel.
##--------------------------------------------------------------------------------------------
def splitRGBInShuffle():
    sel = nuke.selectedNode()
    rr = sel.channels()
    chanList = []
    for i in rr:
        temp = i.split('.')
        if len(chanList) == 0:
            chanList.append(temp[0])
        elif chanList[-1] != temp[0]:
            chanList.append(temp[0])

    if 'rgb' in chanList or 'rgba' in chanList:
        thisChannel = 'rgb'
        if 'rgba' in chanList:
            thisChannel = 'rgba'
        shuffleNodeR = nuke.nodes.Shuffle(name=sel.name() + '_RED')
        shuffleNodeG = nuke.nodes.Shuffle(name=sel.name() + '_GREEN')
        shuffleNodeB = nuke.nodes.Shuffle(name=sel.name() + '_BLUE')
        shuffleNodeR.setInput(0, sel)
        shuffleNodeG.setInput(0, sel)
        shuffleNodeB.setInput(0, sel)
        shuffleNodeR['in'].setValue(thisChannel)
        shuffleNodeG['in'].setValue(thisChannel)
        shuffleNodeB['in'].setValue(thisChannel)
        shuffleNodeR['red'].setValue('red')
        shuffleNodeR['green'].setValue('black')
        shuffleNodeR['blue'].setValue('black')
        shuffleNodeR['alpha'].setValue('red')
        shuffleNodeG['red'].setValue('black')
        shuffleNodeG['green'].setValue('green')
        shuffleNodeG['blue'].setValue('black')
        shuffleNodeG['alpha'].setValue('green')
        shuffleNodeB['red'].setValue('black')
        shuffleNodeB['green'].setValue('black')
        shuffleNodeB['blue'].setValue('blue')
        shuffleNodeB['alpha'].setValue('blue')
        hexColorR = int('%02x%02x%02x%02x' % (255, 0, 0, 1), 16)
        hexColorG = int('%02x%02x%02x%02x' % (0, 255, 0, 1), 16)
        hexColorB = int('%02x%02x%02x%02x' % (0, 0, 255, 1), 16)
        shuffleNodeR['tile_color'].setValue(hexColorR)
        shuffleNodeG['tile_color'].setValue(hexColorG)
        shuffleNodeB['tile_color'].setValue(hexColorB)
        if len(chanList) > 1:
            removeNodeR = nuke.nodes.Remove(name=sel.name() + '_keepRED')
            removeNodeR.setInput(0, shuffleNodeR)
            removeNodeR['operation'].setValue('keep')
            removeNodeR['channels'].setValue('rgba')
            removeNodeG = nuke.nodes.Remove(name=sel.name() + '_keepGREEN')
            removeNodeG.setInput(0, shuffleNodeG)
            removeNodeG['operation'].setValue('keep')
            removeNodeG['channels'].setValue('rgba')
            removeNodeB = nuke.nodes.Remove(name=sel.name() + '_keepBLUE')
            removeNodeB.setInput(0, shuffleNodeB)
            removeNodeB['operation'].setValue('keep')
            removeNodeB['channels'].setValue('rgba')
            removeNodeR['tile_color'].setValue(hexColorR)
            removeNodeG['tile_color'].setValue(hexColorG)
            removeNodeB['tile_color'].setValue(hexColorB)


##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------
def main():
    splitRGBInShuffle()


if __name__ == '__main__':
    main()