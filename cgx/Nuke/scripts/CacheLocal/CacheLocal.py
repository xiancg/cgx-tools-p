# -*- coding: utf-8 -*-
'''
Set all read nodes to cache and read locally.

Created on Feb 06, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import nuke
import nukescripts


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Set all read nodes to cache and read locally
##--------------------------------------------------------------------------------------------
def localServer():
	readKnobList = []
	for e in nuke.allNodes():
		if e.Class()=='Read':
			e['cached'].setValue(1)
			e['cacheLocal'].setValue(0)
		# This handles if you have read nodes inside a group
		readNodes = []
		if e.Class() == 'Group':
			readNodes = e.nodes()
		else:
			readNodes = [e]	
			
		for rn in readNodes:
			readKnob = nuke.getReadFileKnob(rn)
			if readKnob != None:
				if nuke.localisationEnabled(readKnob):
					readKnobList.append(readKnob)
	nuke.localiseFiles(readKnobList)
	

##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------
def main():
	localServer()


if __name__=="__main__":
	main()
