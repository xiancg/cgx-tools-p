# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:/CGXtools/cgx/Nuke/scripts/CollectFiles/ui/config.ui'
#
# Created: Wed Aug 03 17:24:43 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_collectFiles_config(object):
    def setupUi(self, collectFiles_config):
        collectFiles_config.setObjectName("collectFiles_config")
        collectFiles_config.resize(181, 72)
        collectFiles_config.setMinimumSize(QtCore.QSize(181, 72))
        collectFiles_config.setMaximumSize(QtCore.QSize(181, 72))
        self.cancel_BTN = QtGui.QPushButton(collectFiles_config)
        self.cancel_BTN.setGeometry(QtCore.QRect(100, 40, 71, 21))
        self.cancel_BTN.setObjectName("cancel_BTN")
        self.apply_BTN = QtGui.QPushButton(collectFiles_config)
        self.apply_BTN.setGeometry(QtCore.QRect(10, 40, 71, 21))
        self.apply_BTN.setObjectName("apply_BTN")
        self.folderDepth_LABEL = QtGui.QLabel(collectFiles_config)
        self.folderDepth_LABEL.setGeometry(QtCore.QRect(0, 10, 111, 20))
        self.folderDepth_LABEL.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.folderDepth_LABEL.setObjectName("folderDepth_LABEL")
        self.folderDepth_SPINBOX = QtGui.QSpinBox(collectFiles_config)
        self.folderDepth_SPINBOX.setGeometry(QtCore.QRect(120, 10, 51, 22))
        self.folderDepth_SPINBOX.setMinimum(1)
        self.folderDepth_SPINBOX.setObjectName("folderDepth_SPINBOX")

        self.retranslateUi(collectFiles_config)
        QtCore.QMetaObject.connectSlotsByName(collectFiles_config)

    def retranslateUi(self, collectFiles_config):
        collectFiles_config.setWindowTitle(QtGui.QApplication.translate("collectFiles_config", "Collect Files- Configuration", None, QtGui.QApplication.UnicodeUTF8))
        self.cancel_BTN.setText(QtGui.QApplication.translate("collectFiles_config", "Cancel", None, QtGui.QApplication.UnicodeUTF8))
        self.apply_BTN.setText(QtGui.QApplication.translate("collectFiles_config", "Apply", None, QtGui.QApplication.UnicodeUTF8))
        self.folderDepth_LABEL.setText(QtGui.QApplication.translate("collectFiles_config", "Parent folder depth:", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    collectFiles_config = QtGui.QDialog()
    ui = Ui_collectFiles_config()
    ui.setupUi(collectFiles_config)
    collectFiles_config.show()
    sys.exit(app.exec_())

