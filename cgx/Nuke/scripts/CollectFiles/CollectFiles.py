# -*- coding: utf-8 -*-
'''
Collect files tool.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= False
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import sys
if __DEVMODE:
	sys.path.append('M:/CGXtools')

import os, shutil
from tempfile import mkstemp
from PySide import QtCore, QtGui
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
from cgx.core.JSONManager import JSONManager
from cgx.core.Synchronizer import Synchronizer
import nuke, nukescripts
import cgx.gui.DataViewModels as dvm


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Nuke/scripts/CollectFiles'
config_json = appRootFolder + '/cfg/' + 'CollectFiles_config.json'


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(QtGui.QMainWindow):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.initComponents()
			self.initUI()
			self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setupUi(self, collectFiles_MW):
		collectFiles_MW.setObjectName('collectFiles_MW')
		collectFiles_MW.resize(351, 517)
		collectFiles_MW.setMinimumSize(QtCore.QSize(351, 517))
		collectFiles_MW.setMaximumSize(QtCore.QSize(351, 517))
		self.centralwidget = QtGui.QWidget(collectFiles_MW)
		self.centralwidget.setObjectName('centralwidget')
		self.output_LABEL = QtGui.QLabel(self.centralwidget)
		self.output_LABEL.setGeometry(QtCore.QRect(10, 320, 131, 20))
		font = QtGui.QFont()
		font.setWeight(75)
		font.setBold(True)
		self.output_LABEL.setFont(font)
		self.output_LABEL.setObjectName('output_LABEL')
		self.cancel_BTN = QtGui.QPushButton(self.centralwidget)
		self.cancel_BTN.setGeometry(QtCore.QRect(190, 450, 71, 24))
		self.cancel_BTN.setObjectName('cancel_BTN')
		self.browseFiles_BTN = QtGui.QPushButton(self.centralwidget)
		self.browseFiles_BTN.setEnabled(False)
		self.browseFiles_BTN.setGeometry(QtCore.QRect(280, 280, 61, 20))
		self.browseFiles_BTN.setObjectName('browseFiles_BTN')
		self.sep02_LINE = QtGui.QFrame(self.centralwidget)
		self.sep02_LINE.setGeometry(QtCore.QRect(0, 300, 351, 20))
		self.sep02_LINE.setFrameShape(QtGui.QFrame.HLine)
		self.sep02_LINE.setFrameShadow(QtGui.QFrame.Sunken)
		self.sep02_LINE.setObjectName('sep02_LINE')
		self.sep01_LINE = QtGui.QFrame(self.centralwidget)
		self.sep01_LINE.setGeometry(QtCore.QRect(0, 50, 351, 20))
		self.sep01_LINE.setFrameShape(QtGui.QFrame.HLine)
		self.sep01_LINE.setFrameShadow(QtGui.QFrame.Sunken)
		self.sep01_LINE.setObjectName('sep01_LINE')
		self.files_LISTVIEW = QtGui.QListView(self.centralwidget)
		self.files_LISTVIEW.setEnabled(False)
		self.files_LISTVIEW.setGeometry(QtCore.QRect(10, 130, 331, 141))
		self.files_LISTVIEW.setObjectName('files_LISTVIEW')
		self.browseOutput_BTN = QtGui.QPushButton(self.centralwidget)
		self.browseOutput_BTN.setGeometry(QtCore.QRect(280, 340, 61, 20))
		self.browseOutput_BTN.setObjectName('browseOutput_BTN')
		self.files_LABEL = QtGui.QLabel(self.centralwidget)
		self.files_LABEL.setEnabled(False)
		self.files_LABEL.setGeometry(QtCore.QRect(10, 110, 281, 20))
		self.files_LABEL.setObjectName('files_LABEL')
		self.collect_BTN = QtGui.QPushButton(self.centralwidget)
		self.collect_BTN.setGeometry(QtCore.QRect(90, 450, 71, 24))
		self.collect_BTN.setObjectName('collect_BTN')
		self.output_LINEEDIT = QtGui.QLineEdit(self.centralwidget)
		self.output_LINEEDIT.setGeometry(QtCore.QRect(10, 340, 261, 20))
		self.output_LINEEDIT.setObjectName('output_LINEEDIT')
		self.collectOptions_LABEL = QtGui.QLabel(self.centralwidget)
		self.collectOptions_LABEL.setGeometry(QtCore.QRect(10, 10, 121, 20))
		font = QtGui.QFont()
		font.setWeight(75)
		font.setBold(True)
		self.collectOptions_LABEL.setFont(font)
		self.collectOptions_LABEL.setObjectName('collectOptions_LABEL')
		self.sep03_LINE = QtGui.QFrame(self.centralwidget)
		self.sep03_LINE.setGeometry(QtCore.QRect(0, 360, 351, 20))
		self.sep03_LINE.setFrameShape(QtGui.QFrame.HLine)
		self.sep03_LINE.setFrameShadow(QtGui.QFrame.Sunken)
		self.sep03_LINE.setObjectName('sep03_LINE')
		self.selectedOnly_RBTN = QtGui.QRadioButton(self.centralwidget)
		self.selectedOnly_RBTN.setGeometry(QtCore.QRect(160, 30, 171, 20))
		self.selectedOnly_RBTN.setObjectName('selectedOnly_RBTN')
		self.collectOptions_BTNGRP = QtGui.QButtonGroup(collectFiles_MW)
		self.collectOptions_BTNGRP.setObjectName('collectOptions_BTNGRP')
		self.collectOptions_BTNGRP.addButton(self.selectedOnly_RBTN)
		self.batch_CHKBOX = QtGui.QCheckBox(self.centralwidget)
		self.batch_CHKBOX.setGeometry(QtCore.QRect(10, 90, 241, 19))
		self.batch_CHKBOX.setChecked(False)
		self.batch_CHKBOX.setObjectName('batch_CHKBOX')
		self.batch_LABEL = QtGui.QLabel(self.centralwidget)
		self.batch_LABEL.setGeometry(QtCore.QRect(10, 70, 131, 20))
		font = QtGui.QFont()
		font.setWeight(75)
		font.setBold(True)
		self.batch_LABEL.setFont(font)
		self.batch_LABEL.setObjectName('batch_LABEL')
		self.allReadNodes_RBTN = QtGui.QRadioButton(self.centralwidget)
		self.allReadNodes_RBTN.setGeometry(QtCore.QRect(20, 30, 151, 19))
		self.allReadNodes_RBTN.setChecked(True)
		self.allReadNodes_RBTN.setObjectName('allReadNodes_RBTN')
		self.collectOptions_BTNGRP.addButton(self.allReadNodes_RBTN)
		collectFiles_MW.setCentralWidget(self.centralwidget)
		self.retranslateUi(collectFiles_MW)
		QtCore.QMetaObject.connectSlotsByName(collectFiles_MW)

	def retranslateUi(self, collectFiles_MW):
		collectFiles_MW.setWindowTitle(QtGui.QApplication.translate('collectFiles_MW', 'Collect Files', None, QtGui.QApplication.UnicodeUTF8))
		self.output_LABEL.setText(QtGui.QApplication.translate('collectFiles_MW', 'Output Directory', None, QtGui.QApplication.UnicodeUTF8))
		self.cancel_BTN.setText(QtGui.QApplication.translate('collectFiles_MW', 'Cancel', None, QtGui.QApplication.UnicodeUTF8))
		self.browseFiles_BTN.setText(QtGui.QApplication.translate('collectFiles_MW', 'Browse', None, QtGui.QApplication.UnicodeUTF8))
		self.browseOutput_BTN.setText(QtGui.QApplication.translate('collectFiles_MW', 'Browse', None, QtGui.QApplication.UnicodeUTF8))
		self.files_LABEL.setText(QtGui.QApplication.translate('collectFiles_MW', 'Files (Right-click for more options) :', None, QtGui.QApplication.UnicodeUTF8))
		self.collect_BTN.setText(QtGui.QApplication.translate('collectFiles_MW', 'Collect', None, QtGui.QApplication.UnicodeUTF8))
		self.collectOptions_LABEL.setText(QtGui.QApplication.translate('collectFiles_MW', 'Collect options', None, QtGui.QApplication.UnicodeUTF8))
		self.selectedOnly_RBTN.setText(QtGui.QApplication.translate('collectFiles_MW', 'Selected nodes only', None, QtGui.QApplication.UnicodeUTF8))
		self.batch_CHKBOX.setText(QtGui.QApplication.translate('collectFiles_MW', 'Batch process many scripts', None, QtGui.QApplication.UnicodeUTF8))
		self.batch_LABEL.setText(QtGui.QApplication.translate('collectFiles_MW', 'Batch', None, QtGui.QApplication.UnicodeUTF8))
		self.allReadNodes_RBTN.setText(QtGui.QApplication.translate('collectFiles_MW', 'All nodes with files', None, QtGui.QApplication.UnicodeUTF8))

	
	def setConnections(self):
		'''
        Connect signals.
        '''
		self.batch_CHKBOX.stateChanged.connect(self.refreshBatch)
		self.browseFiles_BTN.clicked.connect(self.browseFiles)
		self.browseOutput_BTN.clicked.connect(self.browseDirectory)
		self.collect_BTN.clicked.connect(self.collectFiles)
		self.cancel_BTN.clicked.connect(self.cancelAll)

		#CONTEXT MENUS
		self.files_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.files_LISTVIEW.customContextMenuRequested.connect(self.filesOptions)
		
	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Collect Files"
		self.__toolVersion = __version__
		
		self.filesModelCreated = False
		self.configFile = JSONManager(config_json, self)
		self.folderDepth = self.configFile.getValueByProperty("folderDepth", False)
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.progComp = pipeComponents.Progress_Component(self,self.centralWidget())
		self.progComp.pos = [0,370]
		self.progComp.reset()
		#Add debugMode option
		self.debugMode = QtGui.QAction(self.baseMod.MainMenu_Component.parent)
		self.debugMode.setCheckable(True)
		self.debugMode.setObjectName("edit_debugMode")
		self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[0], self.debugMode)
		className= str(self.__class__.__name__)
		self.debugMode.setText(QtGui.QApplication.translate(className, "Debug Mode", None, QtGui.QApplication.UnicodeUTF8))
		
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		pass
	
	
	def collectFiles(self):
		'''
		Tool main function.
		'''
		if str(self.output_LINEEDIT.text()) == '':
			self.baseMod.statusBar_update("Output directory field can't be empty.", alert=True)
		else:
			if self.batch_CHKBOX.isChecked():
				self.batchCollect()
			elif self.allReadNodes_RBTN.isChecked():
				allNodesWithFiles = []
				for node in nuke.allNodes():
					if 'file' in node.knobs():
						allNodesWithFiles.append(node)
				self.projectCollect(allNodesWithFiles)
			elif self.selectedOnly_RBTN.isChecked():
				selectedNodes = nuke.selectedNodes()
				allNodesWithFiles = []
				for node in selectedNodes:
					if 'file' in node.knobs():
						allNodesWithFiles.append(node)
				self.projectCollect(allNodesWithFiles)
	
	
	def batchCollect(self):
		filesList = self.files_LISTVIEW.model().dataList
		sync = Synchronizer(self)
		if len(filesList) < 1:
			self.baseMod.statusBar_update("Files list can't be empty!", alert=True)
		else:
			self.progComp.reset()
			self.progComp.minimum = 0
			for nukeProj in filesList:
				lastSlash = nukeProj.rfind('/')
				if lastSlash == -1:
					lastSlash = nukeProj.rfind('\\')
				outputDir = str(self.output_LINEEDIT.text())
				finalFolder = outputDir + '/' + nukeProj[lastSlash + 1:-3] + '/Frames'
				self.baseMod.statusBar_update('Processing: ' + nukeProj[lastSlash + 1:], status=True)
				try:
					os.makedirs(finalFolder)
				except WindowsError:
					pass

				currPath = nukeProj
				targPath = finalFolder[:-7] + '/' + nukeProj[lastSlash + 1:]
				sync.processPaths(currPath, targPath)
				newFile = targPath
				newFileRead = open(targPath, 'r')
				filesList = []
				for line in newFileRead:
					line = line.strip()
					if line[:4] == 'file':
						filesList.append(line[5:])

				filesListSet = set(filesList)
				filesListClean = list(filesListSet)
				self.progComp.maximum = len(filesListClean)
				for filePath in filesListClean:
					lastSlash = filePath.rfind('/')
					if lastSlash == -1:
						lastSlash = filePath.rfind('\\')
					fileName = filePath[lastSlash + 1:]
					lastSlashRoot = nukeProj.rfind('/')
					if lastSlashRoot == -1:
						lastSlashRoot = nukeProj.rfind('\\')
					rootDir = nukeProj[:lastSlashRoot + 1]
					if filePath[:2] == './':
						filePath = rootDir + filePath[2:]
					elif filePath[:32] == '[file dirname [value root.name]]':
						filePath = rootDir + filePath[32:]
					srcFolderPath = filePath.rsplit('/', 1)[0]
					if '#' in fileName and os.path.exists(srcFolderPath):
						self.baseMod.statusBar_update('Copying: ' + fileName, status=True)
						fileWithoutExt = fileName.rsplit('.', 1)[0]
						fileExt = fileName.rsplit('.', 1)[1]
						digitsNumber = 0
						for each in fileWithoutExt[::-1]:
							if each == '%':
								digitsNumber += 1
								break
							else:
								digitsNumber += 1

						namePattern = fileWithoutExt[:-digitsNumber]
						filesAndDirs = os.listdir(srcFolderPath)
						preFileList = []
						for each in filesAndDirs:
							if os.path.isfile(os.path.join(srcFolderPath, each)):
								preFileList.append(each)

						fileList = []
						for each in preFileList:
							thisFileExt = each.rsplit('.', 1)[1]
							if each[:len(namePattern)] == namePattern and thisFileExt == fileExt:
								fileList.append(srcFolderPath + '/' + each)

						parentDirs = srcFolderPath.split('/')
						depth = self.folderDepth
						if depth > len(parentDirs):
							depth = len(parentDirs)
						dirStructure = ''
						i = 1
						reversedItems = []
						for each in parentDirs[::-1]:
							if i <= depth:
								reversedItems.append(each)
							else:
								break
							i += 1

						for each in reversedItems[::-1]:
							dirStructure += '/' + each

						currPath = fileList[0]
						targPath = finalFolder + dirStructure + '/' + fileName
						sync.processPaths(currPath, targPath, isSequence=True)
						self.baseMod.statusBar_update('Copied: ' + fileName, status=True)
						self.progComp.value = self.progComp.value + 1
					else:
						self.baseMod.statusBar_update('Copying: ' + fileName, status=True)
						currPath = filePath
						targPath = finalFolder + '/' + fileName
						sync.processPaths(currPath, targPath)
						self.baseMod.statusBar_update('Copied: ' + fileName, status=True)
						self.progComp.value = self.progComp.value + 1

				newFileRead.close()
				self.makeRelative(newFile)
				#Sync log- Debug Mode
				if self.debugMode.isChecked():
					self.debugDialog(',\n'.join(sync.log), self)
			
			#Done Alert!!
			self.baseMod.statusBar_update('Done collecting files!', success=True)
	
	
	def makeRelative(self, file_path):
		"""def to find lines in .nk files and replace them with relative paths expressions"""
		fh, abs_path = mkstemp()
		new_file = open(abs_path, 'w')
		old_file = open(file_path)
		foundProjDirLine = 0
		i = 0
		projDirCounter = 0
		for line in old_file:
			line = line.strip()
			if line[:17] == 'project_directory' and foundProjDirLine == 0:
				new_file.write('project_directory "\\[file dirname \\[value root.name\\]\\]"' + '\n')
				foundProjDirLine = 1
			if projDirCounter == i and i > 0:
				new_file.write('inputs 0\n' + 'project_directory "\\[file dirname \\[value root.name\\]\\]"' + '\n')
			if line[:4] == 'Root' and foundProjDirLine == 0:
				projDirCounter = i + 1
			if line[:5] == 'file ':
				filePath = line[5:]
				lastSlash = filePath.rfind('/')
				if lastSlash == -1:
					lastSlash = filePath.rfind('\\')
				fileName = filePath[lastSlash + 1:]
				if '#' in fileName or '%' in fileName:
					srcFolderPath = filePath.rsplit('/', 1)[0]
					parentDirs = srcFolderPath.split('/')
					depth = self.folderDepth
					if depth > len(parentDirs):
						depth = len(parentDirs)
					dirStructure = ''
					x = 1
					reversedItems = []
					for each in parentDirs[::-1]:
						if x <= depth:
							reversedItems.append(each)
						else:
							break
						x += 1

					for each in reversedItems[::-1]:
						dirStructure += '/' + each

					new_file.write('file ./Frames' + dirStructure + '/' + fileName + '\n')
				else:
					new_file.write('file ./Frames/' + fileName + '\n')
			else:
				new_file.write(line + '\n')
			i += 1

		new_file.close()
		os.close(fh)
		old_file.close()
		os.remove(file_path)
		shutil.move(abs_path, file_path)
	
	
	def projectCollect(self, _nodesList):
		if len(_nodesList) < 1:
			self.baseMod.statusBar_update("No nodes with files found or selected!", alert=True)
		else:
			sync=Synchronizer(self)
			self.progComp.reset()
			self.progComp.minimum = 0
			nukeProj = nuke.Root()['name'].value()
			if nukeProj != '' and nukeProj != None:
				lastSlash = nukeProj.rfind('/')
				if lastSlash == -1:
					lastSlash = nukeProj.rfind('\\')
				outputDir = str(self.output_LINEEDIT.text())
				finalFolder = outputDir + '/' + nukeProj[lastSlash + 1:-3] + '/Frames'
				try:
					os.makedirs(finalFolder)
				except WindowsError:
					pass

				currPath = nukeProj
				targPath = finalFolder[:-7] + '/' + nukeProj[lastSlash + 1:]
				sync.processPaths(currPath, targPath)
				newFile = targPath
				filesList = []
				for node in _nodesList:
					if node['file'].enabled():
						filePath = node['file'].value()
						filesList.append(filePath)

				filesListSet = set(filesList)
				filesListClean = list(filesListSet)
				self.progComp.maximum = len(filesListClean)
				for filePath in filesListClean:
					lastSlash = filePath.rfind('/')
					if lastSlash == -1:
						lastSlash = filePath.rfind('\\')
					fileName = filePath[lastSlash + 1:]
					rootNode = nuke.toNode('root')
					rootPath = rootNode['name'].value()
					lastSlashRoot = rootPath.rfind('/')
					if lastSlashRoot == -1:
						lastSlashRoot = rootPath.rfind('\\')
					rootDir = rootPath[:lastSlashRoot + 1]
					if filePath[:2] == './':
						filePath = rootDir + filePath[2:]
					elif filePath[:32] == '[file dirname [value root.name]]':
						filePath = rootDir + filePath[32:]
					srcFolderPath = filePath.rsplit('/', 1)[0]
					if '%' in fileName and os.path.exists(srcFolderPath):
						self.baseMod.statusBar_update('Copying: ' + fileName, status=True)
						fileWithoutExt = fileName.rsplit('.', 1)[0]
						fileExt = fileName.rsplit('.', 1)[1]
						digitsNumber = 0
						for each in fileWithoutExt[::-1]:
							if each == '%':
								digitsNumber += 1
								break
							else:
								digitsNumber += 1

						namePattern = fileWithoutExt[:-digitsNumber]
						filesAndDirs = os.listdir(srcFolderPath)
						preFileList = []
						for each in filesAndDirs:
							if os.path.isfile(os.path.join(srcFolderPath, each)):
								preFileList.append(each)

						fileList = []
						for each in preFileList:
							thisFileExt = each.rsplit('.', 1)[1]
							if each[:len(namePattern)] == namePattern and thisFileExt == fileExt:
								fileList.append(srcFolderPath + '/' + each)

						parentDirs = srcFolderPath.split('/')
						depth = self.folderDepth
						if depth > len(parentDirs):
							depth = len(parentDirs)
						dirStructure = ''
						i = 1
						reversedItems = []
						for each in parentDirs[::-1]:
							if i <= depth:
								reversedItems.append(each)
							else:
								break
							i += 1

						for each in reversedItems[::-1]:
							dirStructure += '/' + each
							
						if len(fileList) != 0:
							currPath = fileList[0]
							targPath = finalFolder + dirStructure + '/' + fileName
							sync.processPaths(currPath, targPath, isSequence=True)
							self.baseMod.statusBar_update('Copied: ' + fileName, status=True)
							self.progComp.value = self.progComp.value + 1
					else:
						if filePath != '' or filePath == None:
							self.baseMod.statusBar_update('Copying: ' + fileName, status=True)
							currPath = filePath
							targPath = finalFolder + '/' + fileName
							sync.processPaths(currPath, targPath)
						self.baseMod.statusBar_update('Copied: ' + fileName, status=True)
						self.progComp.value = self.progComp.value + 1

				self.makeRelative(newFile)
				
			#Sync log- Debug Mode
			if self.debugMode.isChecked():
				self.debugDialog(',\n'.join(sync.log), self)
			self.progComp.reset()

			#Done Alert!!
			self.baseMod.statusBar_update('Done collecting files!', success=True)
	
	
	def browseFiles(self):
		"""Opens a dialog to browse for files"""
		browseDialog = QtGui.QFileDialog(self)
		browseDialog.setFileMode(browseDialog.FileMode.ExistingFiles)
		browseDialog.setDirectory(self.sInfo.getProjectsFrom('Comp'))
		browseDialog.setNameFilter(browseDialog.tr('Nuke Projects (*.nk)'))
		dialogReturn= browseDialog.exec_()
		if dialogReturn == 1:
			selectedFiles = browseDialog.selectedFiles()
			filesModel = dvm.ObjectsListModel(selectedFiles)
			self.files_LISTVIEW.setModel(filesModel)
			self.files_LISTVIEW.setSelectionMode(self.files_LISTVIEW.MultiSelection)
			self.filesModelCreated = True


	def browseDirectory(self):
		browseDialog = QtGui.QFileDialog(self)
		startDir = self.sInfo.getProjectsFrom('Comp')
		if self.output_LINEEDIT.text() != '' or self.output_LINEEDIT.text() != None:
			if os.path.exists(self.output_LINEEDIT.text()):
				startDir = self.output_LINEEDIT.text()
		browseDialog.setDirectory(startDir)
		browseDialog.setFileMode(browseDialog.FileMode.Directory)
		browseDialog.setOptions(browseDialog.ShowDirsOnly)
		dialogReturn= browseDialog.exec_()
		if dialogReturn == 1:
			selectedDir = browseDialog.selectedFiles()
			self.output_LINEEDIT.setText(selectedDir[0])
	
	
	def refreshBatch(self):
		if self.batch_CHKBOX.isChecked():
			self.files_LISTVIEW.setEnabled(True)
			self.browseFiles_BTN.setEnabled(True)
			self.files_LABEL.setEnabled(True)
			self.allReadNodes_RBTN.setChecked(True)
			self.allReadNodes_RBTN.setEnabled(False)
			self.selectedOnly_RBTN.setEnabled(False)
		else:
			self.files_LISTVIEW.setEnabled(False)
			self.browseFiles_BTN.setEnabled(False)
			self.files_LABEL.setEnabled(False)
			self.allReadNodes_RBTN.setChecked(True)
			self.allReadNodes_RBTN.setEnabled(True)
			self.selectedOnly_RBTN.setEnabled(True)
	
	
	def debugDialog(self,  _text, _parent):
		dialog = QtGui.QDialog(_parent)
		dialog.setObjectName("Dialog")
		dialog.resize(781, 556)
		gridLayout = QtGui.QGridLayout(dialog)
		gridLayout.setObjectName("gridLayout")
		debug_TEXTEDIT = QtGui.QTextEdit(dialog)
		debug_TEXTEDIT.setObjectName("debug_TEXTEDIT")
		debug_TEXTEDIT.setText(_text)
		debug_TEXTEDIT.setReadOnly(True)
		gridLayout.addWidget(debug_TEXTEDIT, 0, 0, 1, 1)
		dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Debug", None, QtGui.QApplication.UnicodeUTF8))
		dialog.show()
	
	
	def cancelAll(self):
		"""Method to close the UI"""
		self.close()
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for files list view
	##--------------------------------------------------------------------------------------------
	def filesOptions(self, pos):
		"""Method that creates the popupmenu"""
		menu = QtGui.QMenu(self.files_LISTVIEW)
		addFilesQ = menu.addAction('Add Files')
		removeFilesQ = menu.addAction('Remove Files')
		menu.popup(self.files_LISTVIEW.mapToGlobal(pos))
		addFilesQ.triggered.connect(self.addFiles)
		removeFilesQ.triggered.connect(self.removeFiles)
		if not self.filesModelCreated:
			addFilesQ.setEnabled(False)
			removeFilesQ.setEnabled(False)
		else:
			addFilesQ.setEnabled(True)
			removeFilesQ.setEnabled(True)

	def addFiles(self):
		"""Opens a dialog to browse for files"""
		browseDialog = QtGui.QFileDialog(self)
		browseDialog.setFileMode(browseDialog.FileMode.ExistingFiles)
		browseDialog.setDirectory(self.sInfo.getProjectsFrom('Comp'))
		browseDialog.setNameFilter(browseDialog.tr('Nuke Projects (*.nk)'))
		dialogReturn= browseDialog.exec_()
		if dialogReturn == 1:
			newFiles = browseDialog.selectedFiles()
			self.files_LISTVIEW.model().insertRows(self.files_LISTVIEW.model().rowCount(), newFiles)

	def removeFiles(self):
		selectedItems = self.files_LISTVIEW.selectedIndexes()
		indexList = []
		for item in selectedItems:
			indexList.append(item.row())

		self.files_LISTVIEW.model().removeRows(sorted(indexList), len(indexList))


##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(QtGui.QDialog):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setupUi(self, Dialog):
		Dialog.setObjectName('Dialog')
		Dialog.resize(361, 231)
		Dialog.setMinimumSize(QtCore.QSize(361, 231))
		Dialog.setMaximumSize(QtCore.QSize(361, 231))
		self.textEdit = QtGui.QTextEdit(Dialog)
		self.textEdit.setGeometry(QtCore.QRect(10, 10, 341, 211))
		self.textEdit.setObjectName('textEdit')
		self.retranslateUi(Dialog)
		QtCore.QMetaObject.connectSlotsByName(Dialog)

	def retranslateUi(self, Dialog):
		Dialog.setWindowTitle(QtGui.QApplication.translate('Dialog', 'Collect Files - Help', None, QtGui.QApplication.UnicodeUTF8))
		self.textEdit.setHtml(QtGui.QApplication.translate('Dialog', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">\n<html><head><meta name="qrichtext" content="1" /><style type="text/css">\np, li { white-space: pre-wrap; }\n</style></head><body style=" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;">\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt; font-weight:600;">::CollectFiles:: </span></p>\n<p style="-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;"><br /></p>\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt;">To collect all files used by one or many Nuke Scripts.</span></p>\n<p style="-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;"><br /></p>\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt; font-weight:600;">BASIC INSTRUCTIONS: </span></p>\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt;">1. Select a collect option</span></p>\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt;">2. Select and output directory</span></p>\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt;">3. Collect!<br /></span></p>\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt; font-weight:600;">TIPS: </span></p>\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt;">&gt;&gt;If using batch processing, right click on the file list for more options</span></p>\n<p style="-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;"><br /></p>\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt;">Author: </span><a href="http://www.chrisgranados.com"><span style=" font-size:8pt; text-decoration: underline; color:#0000ff;">Chris Granados- Xian</span></a><span style=" font-size:8pt;"> </span></p>\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt;">chris.granados@xiancg.com </span><a href="http://www.chrisgranados.com/wp"><span style=" font-size:8pt; text-decoration: underline; color:#0000ff;">http://www.chrisgranados.com/wp</span></a></p></body></html>', None, QtGui.QApplication.UnicodeUTF8))


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(QtGui.QDialog):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setupUi(self, Dialog):
		Dialog.setObjectName('Dialog')
		Dialog.resize(361, 131)
		Dialog.setMinimumSize(QtCore.QSize(361, 131))
		Dialog.setMaximumSize(QtCore.QSize(361, 131))
		self.textEdit = QtGui.QTextEdit(Dialog)
		self.textEdit.setGeometry(QtCore.QRect(10, 10, 341, 111))
		self.textEdit.setReadOnly(True)
		self.textEdit.setObjectName('textEdit')
		self.retranslateUi(Dialog)
		QtCore.QMetaObject.connectSlotsByName(Dialog)

	def retranslateUi(self, Dialog):
		Dialog.setWindowTitle(QtGui.QApplication.translate('Dialog', 'Collect Files - About', None, QtGui.QApplication.UnicodeUTF8))
		self.textEdit.setHtml(QtGui.QApplication.translate('Dialog', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0//EN" "http://www.w3.org/TR/REC-html40/strict.dtd">\n<html><head><meta name="qrichtext" content="1" /><style type="text/css">\np, li { white-space: pre-wrap; }\n</style></head><body style=" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;">\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt; font-weight:600;">::CollectFiles:: </span></p>\n<p style="-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt; font-weight:600;"><br /></p>\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt;">Version: 1.0.0</span></p>\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt;">Released: 10-06-2014</span></p>\n<p style="-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;"><br /></p>\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt;">Author: </span><a href="http://www.chrisgranados.com"><span style=" font-size:8pt; text-decoration: underline; color:#0000ff;">Chris Granados- Xian</span></a><span style=" font-size:8pt;"> </span></p>\n<p style=" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;"><span style=" font-size:8pt;">chris.granados@xiancg.com </span><a href="http://www.chrisgranados.com/wp"><span style=" font-size:8pt; text-decoration: underline; color:#0000ff;">http://www.chrisgranados.com/wp</span></a></p></body></html>', None, QtGui.QApplication.UnicodeUTF8))


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(QtGui.QDialog):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		
		self.configFile = JSONManager(config_json, self)
		self.initUI()
		self.setConnections()
		
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setupUi(self, collectFiles_config):
		collectFiles_config.setObjectName("collectFiles_config")
		collectFiles_config.resize(181, 72)
		collectFiles_config.setMinimumSize(QtCore.QSize(181, 72))
		collectFiles_config.setMaximumSize(QtCore.QSize(181, 72))
		self.cancel_BTN = QtGui.QPushButton(collectFiles_config)
		self.cancel_BTN.setGeometry(QtCore.QRect(100, 40, 71, 21))
		self.cancel_BTN.setObjectName("cancel_BTN")
		self.apply_BTN = QtGui.QPushButton(collectFiles_config)
		self.apply_BTN.setGeometry(QtCore.QRect(10, 40, 71, 21))
		self.apply_BTN.setObjectName("apply_BTN")
		self.folderDepth_LABEL = QtGui.QLabel(collectFiles_config)
		self.folderDepth_LABEL.setGeometry(QtCore.QRect(5, 10, 116, 20))
		self.folderDepth_LABEL.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.folderDepth_LABEL.setObjectName("folderDepth_LABEL")
		self.folderDepth_SPINBOX = QtGui.QSpinBox(collectFiles_config)
		self.folderDepth_SPINBOX.setGeometry(QtCore.QRect(126, 10, 45, 22))
		self.folderDepth_SPINBOX.setMinimum(1)
		self.folderDepth_SPINBOX.setObjectName("folderDepth_SPINBOX")

		self.retranslateUi(collectFiles_config)
		QtCore.QMetaObject.connectSlotsByName(collectFiles_config)


	def retranslateUi(self, collectFiles_config):
		collectFiles_config.setWindowTitle(QtGui.QApplication.translate("collectFiles_config", "Collect Files- Configuration", None, QtGui.QApplication.UnicodeUTF8))
		self.cancel_BTN.setText(QtGui.QApplication.translate("collectFiles_config", "Cancel", None, QtGui.QApplication.UnicodeUTF8))
		self.apply_BTN.setText(QtGui.QApplication.translate("collectFiles_config", "Apply", None, QtGui.QApplication.UnicodeUTF8))
		self.folderDepth_LABEL.setText(QtGui.QApplication.translate("collectFiles_config", "Parent folder depth", None, QtGui.QApplication.UnicodeUTF8))


	def setConnections(self):
		self.cancel_BTN.clicked.connect(self.cancelAll)
		self.apply_BTN.clicked.connect(self.applyConfig)


	def initUI(self):
		"""Read JSON config file"""
		currentDepth = self.configFile.getValueByProperty("folderDepth", False)
		self.folderDepth_SPINBOX.setValue(currentDepth)
	
	
	def applyConfig(self):
		"""Apply changes to JSON file"""
		
		jsonObj = self.configFile.jsonObj
		jsonObj["folderDepth"] = self.folderDepth_SPINBOX.value()
		self.configFile.writeJson(jsonObj, self.configFile.fileName)
		
		self.done(1)
	
	
	def cancelAll(self):
		"""Method to close the UI"""
		self.done(0)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	from cgx.core.StudioInfo import StudioInfo
	sInfo = StudioInfo()
	for qt in QtGui.QApplication.topLevelWidgets():
		try:
			qtname = qt.objectName()
			if qtname == 'collectFiles_MW':
				qt.close()
		except:
			pass
	import cgx.Nuke.scripts.nuke_libs.Nuke_Window as nukeGUI
	from cgx.Nuke.scripts.nuke_libs.Nuke_InitUI import Nuke_InitUI
	from cgx.Nuke.scripts.nuke_libs.Nuke_ProjectInfo import Nuke_ProjectInfo
	pInfo = Nuke_ProjectInfo(sInfo, sInfo.getProjects('','')[0])
	nukeTools = Nuke_InitUI(sInfo, pInfo)
	mainWindow = Main_GUI(sInfo, pInfo, nukeGUI.getNukeWindow(), nukeTools)
	mainWindow.show()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()