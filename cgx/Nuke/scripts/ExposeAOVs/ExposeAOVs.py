# -*- coding: utf-8 -*-
'''
Expose all AOVs in the EXR file.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= False
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import sys
if __DEVMODE:
	sys.path.append('M:/CGXtools')

import nuke, os, random
from PySide import QtCore, QtGui
import cgx.gui.DataViewModels as dvm


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Nuke/scripts/ExposeAOVs'


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(QtGui.QDialog):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent= None):
		'''
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.initTool()
		self.initUI()
		self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setupUi(self, exposeAOVs):
		exposeAOVs.setObjectName('exposeAOVs')
		exposeAOVs.resize(172, 90)
		exposeAOVs.setSizeGripEnabled(False)
		self.cancel_BTN = QtGui.QPushButton(exposeAOVs)
		self.cancel_BTN.setGeometry(QtCore.QRect(90, 60, 71, 24))
		self.cancel_BTN.setObjectName('cancel_BTN')
		self.create_BTN = QtGui.QPushButton(exposeAOVs)
		self.create_BTN.setGeometry(QtCore.QRect(10, 60, 71, 24))
		self.create_BTN.setObjectName('create_BTN')
		self.renderers_LABEL = QtGui.QLabel(exposeAOVs)
		self.renderers_LABEL.setGeometry(QtCore.QRect(10, 10, 101, 20))
		self.renderers_LABEL.setObjectName('renderers_LABEL')
		self.renderers_COMBOBOX = QtGui.QComboBox(exposeAOVs)
		self.renderers_COMBOBOX.setGeometry(QtCore.QRect(10, 30, 151, 22))
		self.renderers_COMBOBOX.setObjectName('renderers_COMBOBOX')
		self.retranslateUi(exposeAOVs)
		QtCore.QMetaObject.connectSlotsByName(exposeAOVs)

	def retranslateUi(self, exposeAOVs):
		exposeAOVs.setWindowTitle(QtGui.QApplication.translate('exposeAOVs', 'Expose AOVs', None, QtGui.QApplication.UnicodeUTF8))
		self.cancel_BTN.setText(QtGui.QApplication.translate('exposeAOVs', 'Cancel', None, QtGui.QApplication.UnicodeUTF8))
		self.create_BTN.setText(QtGui.QApplication.translate('exposeAOVs', 'Create', None, QtGui.QApplication.UnicodeUTF8))
		self.renderers_LABEL.setText(QtGui.QApplication.translate('exposeAOVs', '3D Renderer:', None, QtGui.QApplication.UnicodeUTF8))
	
	
	def setConnections(self):
		'''
        Connect signals.
        '''
		self.create_BTN.clicked.connect(self.exposeAOVs)
		self.cancel_BTN.clicked.connect(self.cancelAll)
		
	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Expose AOVs"
		self.__toolVersion = __version__
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		dataList = ['Arnold', 'VRay']
		renderersModel = dvm.ObjectsListModel(dataList)
		self.renderers_COMBOBOX.setModel(renderersModel)
	
	
	def exposeAOVs(self):
		gradeNodes = 1
		sel = nuke.selectedNode()
		selRenderer = str(self.renderers_COMBOBOX.currentText())
		allChannels = []
		beautyChannels = []
		if selRenderer == 'Arnold':
			allChannels = ['rgba',
			 'N',
			 'P',
			 'direct_diffuse',
			 'direct_specular',
			 'direct_specular_2',
			 'emission',
			 'indirect_diffuse',
			 'indirect_specular',
			 'indirect_specular_2',
			 'reflection',
			 'refraction',
			 'sss',
			 'sheen',
			 'specular',
			 'depth',
			 'uv',
			 'Z',
			 'direct_diffuse_raw',
			 'indirect_diffuse_raw',
			 'diffuse_color',
			 'crypto_asset',
			 'crypto_material',
			 'crypto_object',
			 'single_scatter',
			 'shadow_matte',
			 'direct_backlight',
			 'indirect_backlight',
			 'single_scatter']
			beautyChannels = ['direct_diffuse',
			 'direct_specular',
			 'direct_specular_2',
			 'emission',
			 'indirect_diffuse',
			 'indirect_specular',
			 'indirect_specular_2',
			 'reflection',
			 'refraction',
			 'sss',
			 'sheen',
			 'specular',
			 'single_scatter',
			 'direct_backlight',
			 'indirect_backlight']
		elif selRenderer == 'VRay':
			allChannels = ['rgba',
			 'GI',
			 'SSS',
			 'diffuse',
			 'lighting',
			 'lightselect',
			 'materialID',
			 'multimatte',
			 'normals',
			 'bumpnormals',
			 'materialselect',
			 'reflect',
			 'refract',
			 'selfIllum',
			 'specular',
			 'totalLight',
			 'velocity',
			 'Z',
			 'objectId',
			 'renderId']
			beautyChannels = ['rgba',
			 'GI',
			 'SSS',
			 'diffuse',
			 'lighting',
			 'lightselect',
			 'reflect',
			 'refract',
			 'selfIllum',
			 'specular',
			 'totalLight']
		rr = sel.channels()
		chanList = []
		for i in rr:
			temp = i.split('.')
			if len(chanList) == 0:
				chanList.append(temp[0])
			elif chanList[-1] != temp[0]:
				chanList.append(temp[0])

		nonBeautyNodes = []
		beautyNodes = []
		blackOutNodes = []
		for i in chanList:
			if 'crypto' in i:
				continue
			shuffleNode = nuke.nodes.Shuffle(name=i)
			shuffleNode.setInput(0, sel)
			shuffleNode['in'].setValue(i)
			shuffleNode['in2'].setValue(chanList[0])
			shuffleNode['alpha'].setValue('alpha2')
			shuffleNode['postage_stamp'].setValue(True)
			if gradeNodes == 1:
				if i in beautyChannels:
					removeNode = nuke.nodes.Remove(name=i + '_keep')
					removeNode.setInput(0, shuffleNode)
					removeNode['operation'].setValue('keep')
					removeNode['channels'].setValue('rgba')
					beautyNodes.append(removeNode)
					unpremultNode = nuke.nodes.Unpremult(name='Unpremult_' + i)
					unpremultNode.setInput(0, removeNode)
					beautyNodes.append(unpremultNode)
					gNode = nuke.nodes.Grade(name='Grade_' + i)
					gNode.setInput(0, unpremultNode)
					beautyNodes.append(gNode)
					blackOutNode = nuke.nodes.Shuffle(name='blackOut_' + i)
					blackOutNode.setInput(0, gNode)
					blackOutNode['red'].setValue('black')
					blackOutNode['green'].setValue('black')
					blackOutNode['blue'].setValue('black')
					blackOutNode['alpha'].setValue('black')
					blackOutNode['disable'].setValue(True)
					blackOutNodes.append(blackOutNode)
					beautyNodes.append(blackOutNode)
					beautyNodes.append(shuffleNode)
				elif i not in beautyChannels:
					nonBeautyNodes.append(shuffleNode)
					
		if len(beautyNodes) >= 1:
			mergeNode = nuke.nodes.Merge2()
			mergeNode['operation'].setValue('plus')
			mergeNode['Achannels'].setValue('rgb')
			#Center merge node
			leftNodePos = min([ node.xpos() for node in beautyNodes ])
			rightNodePos = max([ node.xpos() for node in beautyNodes ])
			bottomNodePos = max([ node.ypos() for node in beautyNodes ])
			topNodePos = min([ node.ypos() for node in beautyNodes ])
			mergeNode.setXpos( leftNodePos + ((rightNodePos - leftNodePos)/2) )
			mergeNode.setYpos( bottomNodePos + 130 )
			#Center read node
			sel.setXpos( leftNodePos + ((rightNodePos - leftNodePos)/2) )
			sel.setYpos( topNodePos - 120 )
			#Set IO for merge node
			iVar = 0
			for y in blackOutNodes:
				if iVar == 2:
					iVar += 1
					mergeNode.setInput(iVar, y)
					iVar += 1
					continue
				mergeNode.setInput(iVar, y)
				iVar += 1
			premultNode = nuke.nodes.Premult(name='Premult_Beauty')
			premultNode.setInput(0, mergeNode)
		
		if len(nonBeautyNodes) >= 1:
			for x in nonBeautyNodes:
				x['hide_input'].setValue(True)
	
			bdX = min([ node.xpos() for node in nonBeautyNodes ])
			bdY = min([ node.ypos() for node in nonBeautyNodes ])
			bdW = max([ node.xpos() + node.screenWidth() for node in nonBeautyNodes ]) - bdX
			bdH = max([ node.ypos() + node.screenHeight() for node in nonBeautyNodes ]) - bdY
			left, top, right, bottom = (-10, -80, 10, 10)
			bdX += left
			bdY += top
			bdW += right - left
			bdH += bottom - top
			backdropNode = nuke.nodes.BackdropNode(xpos=bdX, bdwidth=bdW, ypos=bdY, bdheight=bdH,
									label='Utility AOVs',
									tile_color=int(random.random() * 2) + 11,
									note_font_size=42)
			bDropNodes = backdropNode.getNodes(True)
			if len(beautyNodes) != 0:
				backdropNode.setXpos(sel.xpos())
				backdropNode.setYpos(sel.ypos() - 200)
				x = 0
				for each in bDropNodes:
					each.setXpos((backdropNode.xpos() + (30*x) + (each.screenWidth()*x)) + 10 )
					each.setYpos(backdropNode.ypos() + 80 )
					x += 1
			else:
				backdropNode.setXpos(sel.xpos() - (backdropNode.screenWidth()/2) )
				backdropNode.setYpos(sel.ypos() + 120)
				x = 0
				for each in bDropNodes:
					each.setXpos((backdropNode.xpos() + (30*x) + (each.screenWidth()*x)) + 10 )
					each.setYpos(backdropNode.ypos() + 80)
					x += 1
		self.done(1)
	
	
	def cancelAll(self):
		"""Method to close the UI"""
		self.close()


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	for qt in QtGui.QApplication.topLevelWidgets():
		try:
			qtname = qt.objectName()
			if qtname == 'exposeAOVs':
				qt.close()
		except:
			pass
	import cgx.Nuke.scripts.nuke_libs.Nuke_Window as nukeGUI
	mainWindow = Main_GUI(nukeGUI.getNukeWindow())
	mainWindow.show()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()