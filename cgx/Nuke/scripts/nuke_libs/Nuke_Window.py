# -*- coding: utf-8 -*-
'''
Script to get Nuke Window as a QMainWindow instance

Created on Jan 07, 2015

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from PySide import QtCore, QtGui


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Gets the main Nuke Window
##--------------------------------------------------------------------------------------------
def getNukeWindow():
    '''Get the Nuke main window'''
    ptr = QtGui.QApplication.activeWindow()
    return ptr


##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------
def main():
    pass


if __name__=="__main__":
    main()