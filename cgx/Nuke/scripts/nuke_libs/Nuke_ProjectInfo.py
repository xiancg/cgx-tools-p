# -*- coding: utf-8 -*-
'''
Nuke class to manage project info.

Modified on Aug 04, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import nuke, nukescripts
from cgx.core.ProjectInfo import ProjectInfo
import cgx.core


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Nuke Project Info object
##--------------------------------------------------------------------------------------------
class Nuke_ProjectInfo(ProjectInfo):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _studioInfo, _project, _folderStructure=cgx.core.COMP_FOLDER_STRUCTURE):
        super(Nuke_ProjectInfo,self).__init__(_studioInfo, _project, _folderStructure)
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------    
    def nuke_getProject(self):
        '''
        Retrieves the current project set in Nuke.
        :return: Name for the current project set in Nukes.
        :rtype: string
        '''
        nukeProjFullPath = nuke.scriptName()
        if "\\" in nukeProjFullPath:
            nukeProjFullPath.replace("\\","/")
        
        nukeProj = nukeProjFullPath.split("/")[1]
        
        return nukeProj
    
    
    def nuke_getFPS(self):
        '''
        Retrieves the current file frames per second
        :return: Frames per second configuration for the current file in Nuke.
        :rtype: int
        '''
        fpsNumber = nuke.Root()['fps'].value()
        
        return fpsNumber
    
    
    def nuke_getSequence(self):
        '''
        Retrieves the sequence for the current file in Nuke. Defaults to first sequence if no match found.
        :return: Sequences name
        :rtype: string
        '''
        
        filePath = nuke.scriptName()
        splitList = self.__splitPath(filePath)
        
        seqsList = self.getSequences(self.sInfo.getSequencesFrom('Comp'), _department='Comp')
        
        if seqsList != None:
            if len(seqsList) > 0:
                seqName = seqsList[0]
                for each in splitList:
                    if each in seqsList:
                        seqName = each
                        break
                return seqName
            else:
                return ''
        else:
            return ''
        
    
    def nuke_getShot(self):
        '''
        Retrieves the shot for the current file in Nuke. Defaults to first shot if no match found.
        :return: Sequences name
        :rtype: string
        '''
        filePath =nuke.scriptName()
        splitList = self.__splitPath(filePath)
        
        seqName = self.nuke_getSequence()
        shotsList = self.getShots(seqName, self.sInfo.getSequencesFrom('Comp'), _department='Comp')
        
        if shotsList != None:
            if len(shotsList) > 0:
                shotName = shotsList[0]
                for each in splitList:
                    if each in shotsList:
                        shotName = each
                        break
                
                return shotName
            else:
                return None
        else:
            return None
    
    
    def nuke_getAssetTypes(self, exceptions=[]):
        '''
        Retrieves the asset types
        :param _exceptions: List of folders to ignore if found.
        :type _exceptions: list
        :return: List of asset types
        :rtype: list
        '''
        #Get all assetType type folders
        propertiesDict = {"abstractFolderType":"branchfolder",
        "concreteFolderType":"assetType_branchfolder",
        "folderFunction":"wip_content",
        "placeholder":False}
        nodes = self.folderStructure.getPropertiesAndValues(propertiesDict)
        if len(nodes) >= 1:
            assetTypesList = [each.name for each in nodes if each not in exceptions]
            assetTypesSet = set(assetTypesList)
            assetTypesList = list(assetTypesSet)
            return sorted(assetTypesList)
        else:
            return None
    
    
    def nuke_getFrameTypes(self, exceptions=[]):
        '''
        Retrieves the frame types
        :param _exceptions: List of folders to ignore if found.
        :type _exceptions: list
        :return: List of frame types
        :rtype: list
        '''
        #Get all assetType type folders
        propertiesDict = {"abstractFolderType":"branchfolder",
                          "concreteFolderType":"framesType_branchfolder",
                          "folderFunction":"wip_content",
                          "placeholder":False}
        nodes = self.folderStructure.getPropertiesAndValues(propertiesDict)
        if len(nodes) >= 1:
            frameTypesList = [each.name for each in nodes if each not in exceptions]
            frameTypesSet = set(frameTypesList)
            frameTypesList = list(frameTypesSet)
            return sorted(frameTypesList)
        else:
            return None
    
    
    def __splitPath(self, _path):
        '''
        Retrieves the given path split by folders.
        :return: Absolute path to a file.
        :rtype: string
        '''
        slashIndex = _path.rfind("/")
        splitList = []
        if slashIndex == -1:
            splitList = _path.split("\\")
        else:
            splitList = _path.split("/")
            
        return splitList


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass


if __name__=="__main__":
    main()