# -*- coding: utf-8 -*-
'''
Nuke class to initialize gui components

Created on Apr 08, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import cgx.core
from cgx.libs.Abstract_InitUI import Abstract_InitUI


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Nuke Init UI Components
##--------------------------------------------------------------------------------------------
class Nuke_InitUI(Abstract_InitUI):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _studioInfo, _projectInfo, _folderStructure=cgx.core.COMP_FOLDER_STRUCTURE):
        '''
        Nuke init ui class. Get info from Maya and set it to the gui components.
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param _folderStructure: Absolute path to the json template file for the project folder structure.
        :type _folderStructure: string
        '''
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------    
    def setCurrentProject(self, _projectComponent):
        '''
        Sets project component to app project if possible.
        :param _projectComponent: Gui component to set the project on.
        :type _projectComponent: Project_Component
        '''
        _projectComponent.setToItem(self.pInfo.nuke_getProject())
    
    
    def setCurrentSequence(self, _sequenceComponent):
        '''
        Sets sequence component to file sequence if possible.
        :param _sequenceComponent: Gui component to set the sequence on.
        :type _sequenceComponent: Sequence_Component
        '''
        _sequenceComponent.setToItem(self.pInfo.nuke_getSequence())
    
    
    def setCurrentShot(self, _shotComponent):
        '''
        Sets shot component to file if possible.
        :param _shotComponent: Gui component to set the shot on.
        :type _shotComponent: Shot_Component
        '''
        _shotComponent.setToItem(self.pInfo.nuke_getShot())
        
        
    def checkFPS(self, _FPSComponent):
        '''
        Updates FPS_Component to red if FPS from project config file and app do not match. Black if they do match. 
        :param _FPSComponent: Gui component to update
        :type _FPSComponent: FPS_Component
        '''
        _FPSComponent.componentUpdate(self.pInfo.nuke_getFPS())


##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------
def main():
    pass
    

if __name__=="__main__":
    main()