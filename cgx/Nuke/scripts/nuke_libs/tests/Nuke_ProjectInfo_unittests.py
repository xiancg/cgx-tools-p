# -*- coding: utf-8 -*-
'''
unittests for Nuke_ProjectInfo

Created on Aug 03, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import sys
sys.path.append('M:/CGXtools')

import nuke, nukescripts

from cgx.Nuke.scripts.nuke_libs.Nuke_ProjectInfo import Nuke_ProjectInfo
from cgx.core.StudioInfo import StudioInfo
import unittest

##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Test for Abstract Project info object
##--------------------------------------------------------------------------------------------
class Test_Nuke_ProjectInfo(unittest.TestCase):
    ##--------------------------------------------------------------------------------------------
    ##Setup/Teardown
    ##--------------------------------------------------------------------------------------------
    def setUp(self):
        #self.configFolder = "D:/XianCG/CGXtools/core/tests/resources"
        self.configFolder = "M:/CGXtools/cgx/core/cfg"
        #Studio info
        self.gizmoStructureFile= self.configFolder + "/studio_structure.json"
        self.gizmoPropertiesFile= self.configFolder + "/studio_properties.json"
        self.gizmoNamingFile= self.configFolder + "/studio_namingconventions.json"
        self.sInfo = StudioInfo(self.gizmoStructureFile, self.gizmoPropertiesFile, self.gizmoNamingFile)
        self.proj = self.sInfo.getProjects("","")

        #Project info
        self.gizmoFolderStructFile = self.configFolder + "/Gizmo_cfg/3D_structure.json"
        self.pInfo = Nuke_ProjectInfo(self.sInfo, self.gizmoFolderStructFile, self.proj[0])

    def tearDown(self):
        pass
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    #@unittest.SkipTest
    def test_getProject(self):
        print self.pInfo.nuke_getProject()
        #self.assertTrue(len(self.pInfo.getSequences(self.sInfo.getSequencesFrom())) > 0)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass


if __name__=="__main__":
    unittest.main()