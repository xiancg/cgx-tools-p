__CGX_TOOLS_ROOT= "M:/CGXtools"

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import nuke, sys
sys.path.append(__CGX_TOOLS_ROOT)
import cgx.Nuke.scripts.ProjectProperties.ProjectProperties as ProjectProperties
import cgx.Nuke.scripts.ExposeAOVs.ExposeAOVs as ExposeAOVs
import cgx.Nuke.scripts.SplitRGB.SplitRGB as SplitRGB
import cgx.Nuke.scripts.ReloadRead.ReloadRead as ReloadRead
import cgx.Nuke.scripts.CacheLocal.CacheLocal as CacheLocal
import cgx.Nuke.scripts.CollectFiles.CollectFiles as CollectFiles
import cgx.Nuke.scripts.WriteCGX.WriteCGX as WriteCGX


##--------------------------------------------------------------------------------------------
##TOOLS
##--------------------------------------------------------------------------------------------
#CGX
menubar = nuke.menu('Nuke')
cgxMenu = menubar.addMenu('CGX')
cgxMenu.addCommand('Set Project Properties', 'ProjectProperties.main()')
cgxMenu.addCommand('Expose AOVs', 'ExposeAOVs.main()', 'e')
cgxMenu.addCommand('Split RGB', 'SplitRGB.main()', '^e')
cgxMenu.addCommand('Reload All Reads', 'ReloadRead.main()')
cgxMenu.addCommand('Cache Local Files', 'CacheLocal.main()')
cgxMenu.addCommand('Collect Files', 'CollectFiles.main()')
#CGXGIZMOS
cgxMenu.addCommand('Gizmos/WriteCGX',lambda: nuke.createNode('WriteCGX'), '^w')
cgxMenu.addCommand('Gizmos/cgx_vignette',lambda: nuke.createNode('cgx_vignette'))
cgxMenu.addCommand('Gizmos/cgx_chromAb_Boaz',lambda: nuke.createNode('cgx_chromAb_Boaz'))
cgxMenu.addCommand('Gizmos/cgx_chromAb',lambda: nuke.createNode('cgx_chromAb'))
cgxMenu.addCommand('Gizmos/cgx_zFog',lambda: nuke.createNode('cgx_zfog'))
cgxMenu.addCommand('Gizmos/cgx_randomNumbers',lambda: nuke.createNode('cgx_randomNumbers'))


