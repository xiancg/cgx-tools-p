# -*- coding: utf-8 -*-
'''
Maya dependencies implementation of Abstract_Dependencies library.

Created on Mar 26, 2015

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.libs.Abstract_Dependencies import Abstract_Dependencies
import maya.cmds as mc
import maya.mel as mel
import os


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Maya Dependencies
##--------------------------------------------------------------------------------------------
class Maya_Dependencies(Abstract_Dependencies):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self):
        super(Maya_Dependencies,self).__init__()
        self.__textureFiles = []
        self.__pointCacheFiles = []
        self.__alembicFiles = []
        self.__referenceFiles = []
        self.__audioFiles = []
        self.__imagePlaneFiles = []
        self.__exocortexAlembicFiles = []
        self.__yetiFiles = []
        self.__yetiTextureFiles = []
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def fileAttrChange(self, _filePath, _node, _type):
        '''
        Change the file path attribute for the given node.
        :param _filePath: Path to the file
        :type _filePath: string
        :param _node: Node name or object
        :type _node: string
        :param _type: Node type
        :type _type: string
        '''
        if _type == "Alembic":
            mc.setAttr(_node + ".abc_File", _filePath, type= "string")
        elif _type == "Exocortex":
            mc.setAttr(_node + ".fileName", _filePath, type= "string")
        elif _type == "Audio":
            mc.setAttr(_node + ".filename", _filePath, type= "string")
        elif _type == "Image plane":
            origColorSpace = mc.getAttr(_node + '.colorSpace')#Color space preservation
            mc.setAttr(_node + ".imageName", _filePath, type= "string")
            mc.setAttr(_node + '.colorSpace', origColorSpace, type='string')
        elif _type == "Point cache":
            pathParts = _filePath.rsplit("/", 1)
            mc.setAttr(_node + ".cachePath", pathParts[0] + "/", type= "string")
        elif _type == "Reference":
            mc.file(_filePath, loadReference= _node, options= "v=0;", prompt=False)
        elif _type == "Texture":
            sequenceStatus = mc.getAttr(_node + ".useFrameExtension")
            tileStatus = mc.getAttr(_node + ".uvTilingMode")
            origColorSpace = mc.getAttr(_node + '.colorSpace')#Color space preservation
            if sequenceStatus == True:
                mc.setAttr(_node + ".useFrameExtension", False)
                mc.setAttr(_node + ".fileTextureName", _filePath, type= "string")
                mc.setAttr(_node + ".useFrameExtension", True)
                mc.setAttr(_node + '.colorSpace', origColorSpace, type='string')
            elif tileStatus != 0:
                mc.setAttr(_node + ".uvTilingMode", 0)
                mc.setAttr(_node + ".fileTextureName", _filePath, type= "string")
                mc.setAttr(_node + ".uvTilingMode", tileStatus)
                mc.setAttr(_node + '.colorSpace', origColorSpace, type='string')
            else:
                mc.setAttr(_node + ".fileTextureName", _filePath, type= "string")
                mc.setAttr(_node + '.colorSpace', origColorSpace, type='string')
        elif _type == "Yeti cache file":
            mc.setAttr(_node + ".cacheFileName", _filePath, type= "string")
        elif _type == "Yeti texture":
            mc.setAttr(_node + ".imageSearchPath", _filePath, type= "string")
        else:
            mc.warning("Not recognized node type.")
    
    
    ##--------------------------------------------------------------------------------------------
    ##Properties
    ##--------------------------------------------------------------------------------------------
    @property
    def textureFiles (self):
        filesList= mc.ls(type="file", long=True)
        if filesList != None:
            for node in filesList:
                textureDict = {}
                filePath = mc.getAttr(node + ".fileTextureName")
                if filePath == None:
                    filePath = ""
                if os.path.exists(filePath):
                    if "\\" in filePath:
                        filePath = filePath.replace("\\","/")
                    textureDict["filePath"] = filePath
                    textureDict["node"] = node
                    self.__textureFiles.append(textureDict)
                else:
                    if filePath != "":
                        mc.warning(node + " contains a non-existing file.")
                    
        return self.__textureFiles
    
    
    @property
    def pointCacheFiles(self):
        filesList= mc.ls(type="cacheFile", long=True)
        if filesList != None:
            for node in filesList:
                pointCacheDict = {}
                baseDir = mc.getAttr(node + ".cachePath")
                baseName = mc.getAttr(node + ".cacheName")
                xmlPath = baseDir + baseName + ".xml"
                mcxPath = baseDir + baseName + ".mcx"
                mcPath = baseDir + baseName + ".mc"
                if xmlPath == None:
                    xmlPath= ""
                if mcxPath == None:
                    mcxPath= ""
                if mcPath == None:
                    mcPath= ""
                if os.path.exists(xmlPath):
                    if os.path.exists(mcxPath):
                        if "\\" in xmlPath:
                            xmlPath = xmlPath.replace("\\","/")
                        if "\\" in mcxPath:
                            mcxPath = mcxPath.replace("\\","/")
                        pointCacheDict["xmlPath"] = xmlPath
                        pointCacheDict["mcxPath"] = mcxPath
                        pointCacheDict["node"] = node
                        self.__pointCacheFiles.append(pointCacheDict)
                    elif os.path.exists(mcPath):
                        if "\\" in xmlPath:
                            xmlPath = xmlPath.replace("\\","/")
                        if "\\" in mcPath:
                            mcPath = mcPath.replace("\\","/")
                        pointCacheDict["xmlPath"] = xmlPath
                        pointCacheDict["mcxPath"] = mcPath
                        pointCacheDict["node"] = node
                        self.__pointCacheFiles.append(pointCacheDict)
                else:
                    if xmlPath != "" and mcxPath != "" and mcPath!= "":
                        mc.warning(node + " contains a non-existing file.")
                        
        return self.__pointCacheFiles
    
    
    @property
    def alembicFiles(self):
        if mc.pluginInfo("AbcImport", query=True, loaded=True) or mc.pluginInfo("AbcExport", query=True, loaded=True):
            filesList= mc.ls(type="AlembicNode", long=True)
            if filesList != None:
                for node in filesList:
                    alembicDict = {}
                    filePath = mc.getAttr(node + ".abc_File")
                    if filePath == None:
                        filePath= ""
                    if os.path.exists(filePath):
                        if "\\" in filePath:
                            filePath = filePath.replace("\\","/")
                        alembicDict["filePath"] = filePath
                        alembicDict["node"] = node
                        self.__alembicFiles.append(alembicDict)
                    else:
                        if filePath != "":
                            mc.warning(node + " contains a non-existing file.")

        return self.__alembicFiles
    
    
    @property
    def exocortexAlembicFiles(self):
        if mc.pluginInfo("MayaExocortexAlembic", query=True, loaded=True):
            filesList= mc.ls(type="ExocortexAlembicFile", long=True)
            if filesList != None:
                for node in filesList:
                    alembicDict = {}
                    filePath = mc.getAttr(node + ".fileName")
                    if filePath == None:
                        filePath= ""
                    if os.path.exists(filePath):
                        if "\\" in filePath:
                            filePath = filePath.replace("\\","/")
                        alembicDict["filePath"] = filePath
                        alembicDict["node"] = node
                        self.__exocortexAlembicFiles.append(alembicDict)
                    else:
                        if filePath != "":
                            mc.warning(node + " contains a non-existing file.")

        return self.__exocortexAlembicFiles
    
    
    @property
    def referenceFiles(self):
        filesList= mc.file(mc.file(query=True, sn= True), reference=True, query= True)
        if filesList != None:
            if len(filesList) >= 1:
                for filePath in filesList:
                    referenceDict = {}
                    node = mc.referenceQuery(filePath, referenceNode=True)
                    if filePath == None:
                        filePath= ""
                    #For repeated references Maya appends {#} to filepath
                    existCheck = filePath
                    if filePath[-1] == "}":
                        existCheck = filePath[:-3]
                    if os.path.exists(existCheck):
                        if "\\" in filePath:
                            filePath = filePath.replace("\\","/")
                        referenceDict["filePath"] = existCheck
                        referenceDict["node"] = node
                        self.__referenceFiles.append(referenceDict)
                    else:
                        if filePath != "":
                            mc.warning(node + " contains a non-existing file.")

        return self.__referenceFiles
    
    
    @property
    def audioFiles(self):
        filesList= mc.ls(type="audio", long=True)
        if filesList != None:
            for node in filesList:
                audioDict = {}
                filePath = mc.getAttr(node + ".filename")
                if filePath == None:
                    filePath= ""
                if os.path.exists(filePath):
                    if "\\" in filePath:
                        filePath = filePath.replace("\\","/")
                    audioDict["filePath"] = filePath
                    audioDict["node"] = node
                    self.__audioFiles.append(audioDict)
                else:
                    if filePath != "":
                        mc.warning(node + " contains a non-existing file.")

        return self.__audioFiles
    
    
    @property
    def imagePlaneFiles(self):
        filesList= mc.ls(type="imagePlane", long=True)
        if filesList != None:
            for node in filesList:
                textureDict = {}
                filePath = mc.getAttr(node + ".imageName")
                if filePath == None:
                    filePath= ""
                if os.path.exists(filePath):
                    if "\\" in filePath:
                        filePath = filePath.replace("\\","/")
                    textureDict["filePath"] = filePath
                    textureDict["node"] = node
                    self.__imagePlaneFiles.append(textureDict)
                else:
                    if filePath != "":
                        mc.warning(node + " contains a non-existing file.")

        return self.__imagePlaneFiles
    
    
    @property
    def yetiFiles(self):
        if mc.pluginInfo("pgYetiMaya", query=True, loaded=True):
            filesList= mc.ls(type="pgYetiMaya", long=True)
            if filesList != None:
                for node in filesList:
                    yetiDict = {}
                    cacheFilePath = mc.getAttr(node + ".cacheFileName")
                    if cacheFilePath == None:
                        cacheFilePath = ""
                    if os.path.exists(cacheFilePath):
                        if "\\" in cacheFilePath:
                            cacheFilePath = cacheFilePath.replace("\\","/")
                        yetiDict["cacheFileName"] = cacheFilePath
                        yetiDict["node"] = node
                        self.__yetiFiles.append(yetiDict)
                    elif cacheFilePath != "":
                        mc.warning(node + " contains a non-existing file.")
    
        return self.__yetiFiles
    
    
    @property
    def yetiTextureFiles(self):
        if mc.pluginInfo("pgYetiMaya", query=True, loaded=True):
            filesList= mc.ls(type="pgYetiMaya", long=True)
            if filesList != None:
                for node in filesList:
                    textureNodes = mel.eval('pgYetiGraph -listNodes -type "texture" ' + node)
                    if textureNodes != None and len(textureNodes) >= 1:
                        for txtNode in textureNodes:
                            filePath =  mel.eval('pgYetiGraph -node ' + txtNode + ' -param "file_name" -getParamValue ' + node)
                            yetiTexturesDict = {}
                            if filePath == None:
                                filePath= ""
                            if os.path.exists(filePath):
                                if "\\" in filePath:
                                    filePath = filePath.replace("\\","/")
                                yetiTexturesDict["filePath"] = filePath
                                yetiTexturesDict["textureNode"] = txtNode
                                yetiTexturesDict["node"] = node
                                self.__yetiTextureFiles.append(yetiTexturesDict)
                            else:
                                if filePath != "":
                                    mc.warning(node + " contains a non-existing file.")
    
        return self.__yetiTextureFiles


def main():
    pass

if __name__=="__main__":
    main()