# -*- coding: utf-8 -*-
'''
Script to get Maya Window as a QMainWindow instance

Created on Jan 07, 2015

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import maya.OpenMayaUI as mui
import cgx.gui.uiParser as uiParser


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Jason Parks","Nathan Horne","Chris Granados", "Max Rocamora"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Gets the main Maya Window
##--------------------------------------------------------------------------------------------
def getMayaWindow():
    '''Get the Maya main window as a QMainWindow instance'''
    ptr = mui.MQtUtil.mainWindow()
    return uiParser.wrapinstance(long(ptr))


##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------
def main():
    pass


if __name__=="__main__":
    main()