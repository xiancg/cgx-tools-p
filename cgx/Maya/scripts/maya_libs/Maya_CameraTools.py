# -*- coding: utf-8 -*-
'''
Maya camera tools implementation of Abstract_CameraTools library.

Created on Nov 15, 2015

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import maya.cmds as mc
import maya.mel as mel
import os
import cgx.Maya.scripts.maya_libs.Maya_TimeRange as tRange
from cgx.libs.Abstract_CameraTools import Abstract_CameraTools
from cgx.Maya.scripts.maya_libs.Maya_InitUI import Maya_InitUI
from cgx.gui.components.Maya_Components import CameraFormats_Component

##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Camera Tools class
##--------------------------------------------------------------------------------------------
class Maya_CameraTools(Abstract_CameraTools):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _studioInfo, _projectInfo, _cam= "", _path = "", _sampleBy = 1, _timeRange = tRange.Maya_TimeRange()):
        '''
        CameraExporter object constructor. To bake and export Maya cameras to different formats.
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param _cam: (Optional) Camera to be exported. Default = ""
        :type _cam: string
        :param _path: (Optional) Where to export the baked cameras. Default = ""
        :type _path: string
        :param _sampleBy: (Optional) Sample animation every X frames. Default= 1
        :type _sampleBy: float
        :param _timeRange: (Optional) CGX Time Range object instance. Default= TimeRange()
        :type _timeRange: TimeRange
        '''
        super(Maya_CameraTools, self).__init__(_studioInfo, _projectInfo,_cam,_path,_sampleBy,_timeRange)
        
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        self.tRange = _timeRange
        self.tRange.handleIn = 2
        self.app_initUI = Maya_InitUI(self.sInfo, self.pInfo)
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def checkPlugins (self, _gui):
        '''
        Check if plugins are loaded.
        :param _gui: Pointer to parent tool gui QMainWindow.
        :type _gui: QMainWindow
        :return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
        :rtype: dict
        '''
        check = {}
        if _gui.camFormatsComp.fbx_CHKBOX.isChecked():
            if not mc.pluginInfo("fbxmaya", query=True, loaded=True):
                check["FBX"] = "FBX Maya plugin not loaded."
                _gui.baseMod.statusBar_update(check["FBX"], alert=True)
        if _gui.camFormatsComp.abc_CHKBOX.isChecked():
            if not mc.pluginInfo("AbcExport", query=True, loaded=True):
                check["Alembic"] = "ABC Export plugin not loaded."
                _gui.baseMod.statusBar_update(check["Alembic"], alert=True)
        '''
        if len(mc.ls(type="unknown")) >= 1:
            check["Unknown"] = "Unknown nodes found: " + ", ".join(mc.ls(type="unknown"))
            _gui.baseMod.statusBar_update(check["Unknown"], alert=True)'''

        return check
    
    
    def exportCamera(self, _cam, _fileName, formats):
        '''
        Export camera with the given name, to the given file and in the given formats.
        :param _cam: Camera to be exported
        :type _cam: string
        :param _fileName: File name
        :type _fileName: string
        :param kwargs: File formats {'ma'=True, 'mb'=True, 'fbx'=True, 'abc'=True}
        :type kwargs: dict
        :return: Exported any of the formats or not
        :rtype: boolean
        '''
        self.cam = _cam
        success=[False,""]
            
        if "ma" in formats.keys():
            if formats["ma"]:
                try:
                    mc.select(self.cam, replace= True)
                    mc.file(self.path + "/" + _fileName + ".ma", force= True, typ= "mayaAscii", exportSelected=True)
                    success = [True,""]
                except RuntimeError:
                    success= [False,"Something is wrong. Couldn't export {} to {}".format(_cam, self.path + "/" + _fileName + ".ma")]
        if "mb" in formats.keys():
            if formats["mb"]:
                try:
                    mc.select(self.cam, replace= True)
                    mc.file(self.path + "/" + _fileName + ".mb", force= True, typ= "mayaBinary", exportSelected=True)
                    success = [True,""]
                except RuntimeError:
                    success=[False,"Something is wrong. Couldn't export {} to {}".format(_cam, self.path + "/" + _fileName + ".mb")]
        if "fbx" in formats.keys():
            if formats["fbx"]:
                try:
                    mc.select(self.cam, replace= True)
                    mel.eval("FBXExportFileVersion FBX201600;")
                    mel.eval('FBXExport -f "' + self.path + '/' + _fileName + '.fbx"' + ' -s;' )
                    success = [True,""]
                except RuntimeError:
                    success=[False,"Something is wrong. Couldn't export {} to {}".format(_cam, self.path + "/" + _fileName + ".fbx")]
        if "abc" in formats.keys():
            if formats["abc"]:
                try:
                    mc.select(self.cam, replace= True)
                    startFrame = self.timeRange.startFrame - self.timeRange.handleIn
                    endFrame = self.timeRange.endFrame + self.timeRange.handleOut
                    frameRangeStr = " -frameRange " + str(startFrame) + " " + str(endFrame)
                    melEvalStr = 'AbcExport -j "'+ '-root ' + self.cam + frameRangeStr + ' -s ' + str(self.sampleBy) + ' -dataFormat ogawa -ws -file '+ self.path + "/" + _fileName + '.abc' + '";'
                    alembicResult = mel.eval(melEvalStr)
                    success = [True,""]
                except RuntimeError:
                    success=[False,"Something is wrong. Couldn't export {} to {}".format(_cam, self.path + "/" + _fileName + ".abc")]
        
        #Delete cam
        mc.delete(_cam)
        
        return success
            
    
    def bakeCamera(self, _name, _offsetStartFrame= False, _newStartFrame = 1):
        '''
        Bake camera animation, including focal length
        :param _name: New camera name
        :type _name: string
        :param _offsetStartFrame: (Optional) Offset entire animation range. Default= False
        :type _offsetStartFrame: boolean
        :param _newStartFrame: (Optional) Starting frame for offset. Default= 1
        :type _newStartFrame: int
        :return: New camera with baked animation
        :rtype: string
        '''
        newCam = self.cleanCopy(self.cam, _name)
        newCamShape = mc.listRelatives(newCam , shapes = True, noIntermediate = True)
        oldCamShape = mc.listRelatives(self.cam , shapes = True, noIntermediate = True)
        #Make constrain
        pConstrain = mc.parentConstraint(self.cam, newCam, maintainOffset = False)
        #Connect focal length
        mc.connectAttr(oldCamShape[0] + ".focalLength" , newCamShape[0] + ".focalLength", force= True)
        #Clipping planes
        mc.setAttr(newCamShape[0] + ".farClipPlane", 10000)
        mc.setAttr(newCamShape[0] + ".nearClipPlane", 0.1)
        #Bake
        mc.bakeResults(newCam, 
                       simulation= True, 
                       t=(int(self.timeRange.absoluteStartFrame),int(self.timeRange.absoluteEndFrame)), 
                       sampleBy= self.sampleBy, 
                       preserveOutsideKeys= True, 
                       sparseAnimCurveBake= False, 
                       removeBakedAttributeFromLayer= False, 
                       bakeOnOverrideLayer= False, 
                       minimizeRotation= True, 
                       at= ("tx","ty","tz","rx","ry","rz"))
        mc.bakeResults(newCamShape[0], 
                       simulation= True, 
                       t=(int(self.timeRange.absoluteStartFrame),int(self.timeRange.absoluteEndFrame)), 
                       sampleBy= self.sampleBy, 
                       preserveOutsideKeys= True, 
                       sparseAnimCurveBake= False, 
                       removeBakedAttributeFromLayer= False, 
                       bakeOnOverrideLayer= False, 
                       minimizeRotation= False, 
                       at= ("fl"))
        #Delete constrain
        mc.delete(pConstrain)
        #Offset keys
        if _offsetStartFrame:
            timeChangeVar = self.timeRange.offsetTimeRange(_newStartFrame)['timeChange']
            mc.keyframe(newCam, 
                        edit=True, 
                        includeUpperBound= True, 
                        time= (int(self.timeRange.absoluteStartFrame), int(self.timeRange.absoluteEndFrame)), 
                        option= "over", 
                        timeChange= timeChangeVar, 
                        relative= True, 
                        animation= "objects")
            mc.keyframe(newCamShape[0], 
                        edit=True, 
                        includeUpperBound= True, 
                        time= (int(self.timeRange.absoluteStartFrame), int(self.timeRange.absoluteEndFrame)), 
                        option= "over", 
                        timeChange= timeChangeVar, 
                        relative= True, 
                        animation= "objects")
        
        return newCam

    
    def cleanCopy(self, _cam, _thisName):
        '''
        Def to duplicate without extrashapes, with unlocked-unhiden attributes and freezed transformations
        :param _cam: Original camera
        :type _cam: string
        :param _thisName: Name for new copy of _cam
        :type _thisName: string
        :return: Camera copy with new name
        :rtype: string
        '''
        
        dupCam = mc.duplicate(_cam, renameChildren = True)
        #Unparent
        try:
            mc.parent(dupCam[0], world = True)
        except RuntimeError:
            pass
        #Unlock and unhide transforms
        attrs = ["tx", "ty", "tz", "rx", "ry", "rz", "sx", "sy", "sz"]
        for each in attrs:
            thisAttr = dupCam[0] + "." + each
            attrBool = mc.getAttr(thisAttr, lock=True)
            if attrBool:
                mc.setAttr(thisAttr, lock=False)
            attrHideBool = mc.getAttr(thisAttr, keyable=True)
            if not attrHideBool:
                mc.setAttr(thisAttr, keyable=True)
        #Rename
        dupRename = mc.rename(dupCam[0], _thisName)
        #Clean unused shape nodes
        objShape = mc.listRelatives(dupRename, shapes = True, noIntermediate = True)
        allShapes = mc.listRelatives(dupRename, shapes = True)
        for x in allShapes:
            if x != objShape[0]:
                mc.delete(x)
        renameShape = mc.listRelatives(dupRename, shapes = True, noIntermediate = True, fullPath= True)[0]
        #Unlock focal length
        attrBool = mc.getAttr(renameShape + ".focalLength", lock=True)
        if attrBool:
            mc.setAttr(renameShape + ".focalLength", lock=False)
        mc.rename(renameShape, _thisName + "Shape")
        #Clean children nodes
        for e in mc.listRelatives(dupRename, children= True):
            if mc.nodeType(e) != "camera":
                mc.delete(e)
    
        return dupRename
    
    
    def listCams(self):
        '''
        List all cams in current project.
        :return: Cam transforms list
        :rtype: list
        '''
        camShapes = mc.ls(long= True, cameras=True)
        camTransforms = []
        for cam in camShapes:
            camTransforms.append(mc.listRelatives(cam, parent= True, noIntermediate = True, fullPath= True)[0])
        camTransforms.sort()
        
        return camTransforms
    
    
    def renameCam(self, _cam, _newName):
        '''
        Rename given cam to given name.
        :param _cam: Original camera
        :type _cam: string
        :param _newName: Name for cam
        :type _newName: string
        '''
        mc.rename(_cam, _newName)
        
    
    def selectCams(self, _camList):
        '''
        Select given cam list in Maya itself.
        :param _camList: List of cameras obtained from a tool gui.
        :type _camList: list
        '''
        mc.select(_camList, replace=True)
        
    
    def createCameraFormatsComponent(self, _mainWindow, _parent):
        '''
        Create Camera Formats for Maya.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtGui.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtGui.QWidget
        :return: Component
        :rtype: CameraFormats_Component
        '''
        camComp = CameraFormats_Component(_mainWindow, _parent)
        camComp.ma_CHKBOX.setChecked(True)
        camComp.fbx_CHKBOX.setChecked(True)
        camComp.abc_CHKBOX.setChecked(True)
        return camComp
    

    def refreshViewport(self, _bool):
        '''
        Stops viewport refreshing to speed up operations.
        :param _bool: True will stop refresh, False will restart refresh.
        :type _bool: boolean
        '''
        mc.refresh(suspend=_bool)
        
    
    def referenceCam(self, _seq, _shot):
        '''
        Create Camera Formats for Maya.
        :param _seq: Animation sequence name
        :type _seq: str
        :param _shot: Animation shot name
        :type _shot: str
        '''
        camFolder = self.buildCameraPath(_seq,_shot)
        filesAndDirs = os.listdir(camFolder)
        camFile = ""
        for each in filesAndDirs:
            if os.path.isfile(os.path.join(camFolder, each)):
                if each[-3:] == ".ma":
                    camFile = each
                    break
        cameraPath = camFolder + "/" + camFile
        refFile = None
        if os.path.exists(cameraPath):
            refFile = mc.file(cameraPath, reference=True , namespace = camFile[:-3])
            transform = mc.referenceQuery(refFile, nodes=True)[0]
            self.setCameraRange(transform)
            return refFile
    
    
    def setCameraRange(self, _cam):
        transformParts = _cam.split("_")
        startFrame = 0
        endFrame = 100
        i = 0
        for each in transformParts:
            if each[0] == "f":
                startFrame = int(each[1:])
                endFrame = int(transformParts[i+1])
                break
            i += 1
        self.tRange.setTimeSlider(startFrame, endFrame)
        
    
    def removeUnknownNodes(self):
        mc.delete(self.getUnknownNodes())
       
        
    def getUnknownNodes(self):
        nodes = mc.ls(type='unknown')
        return nodes
    

##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass


if __name__=="__main__":
    main()