# -*- coding: utf-8 -*-
'''
Lighting tools library for Maya.

Created on Nov 15, 2015

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets
from cgx.Maya.scripts.maya_libs.Maya_InitUI import Maya_InitUI
from cgx.core.JSONManager import JSONManager
import Red9.core.Red9_Meta as r9Meta
import cgx.Maya.scripts.maya_libs.Maya_MetaData as cgxMeta


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
import cgx.Maya.scripts.workflows.Lighting.LightsManager as lgtM
lgtMFolder = os.path.dirname(os.path.abspath(lgtM.__file__))
lightAttrs_json = lgtMFolder + "/cfg/" + "LightsManager_lightAttrs.json"
import cgx.Maya.scripts.workflows.Lighting.MiniTools as mnTls
mnTlsFolder = os.path.dirname(os.path.abspath(mnTls.__file__))
minitools_json = mnTlsFolder + "/cfg/" + "MiniTools_config.json"


##------------------------------------------------------------------------------------------------
## Class: Maya_LightingTools
##------------------------------------------------------------------------------------------------
class Maya_LightingTools(object):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _studioInfo, _projectInfo):
        self.baseMod = ""
        self.lightAttrsFile = JSONManager(lightAttrs_json, self)
        
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        self.app_initUI = Maya_InitUI(self.sInfo, self.pInfo)
        self.initTools()
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def initTools(self):
        self.lightVis = {}
        lightNodes = self.lightNodes()
        #All scene lights
        allLightsShapes = mc.ls(lights= True, type= lightNodes, long= True)
        allLightsScene = []
        for i in allLightsShapes:
            iTrans = mc.listRelatives(i, parent= True, fullPath= True)
            allLightsScene.append(iTrans[0])
        #List all render layers and lights with visibility off
        renderLyrs = mc.ls(type= "renderLayer")
        for lyr in renderLyrs:
            lyrMembers = mc.editRenderLayerMembers(lyr, query=True , fullNames= True)
            lightsOff = []
            if lyrMembers != None:
                for light in allLightsScene:
                    shapeNode = mc.listRelatives(light, shapes= True, noIntermediate= True, fullPath= True)
                    if light in lyrMembers or shapeNode[0] in lyrMembers:
                        if mc.getAttr(light + ".visibility") == False:
                            lightsOff.append(light)
            self.lightVis[lyr] = lightsOff
        
        self.simpleLightsOff = []
        for light in allLightsScene:
            if mc.getAttr(light + ".visibility") == False:
                self.simpleLightsOff.append(light)
        
        
    ##--------------------------------------------------------------------------------------------
    ##Light nodes
    ##--------------------------------------------------------------------------------------------
    def lightNodes(self):
        lightNodes = self.lightAttrsFile.jsonObj['common']['lightNodes']['nodes']
        engines = []
        for each in self.lightAttrsFile.jsonObj.keys():
            if each != 'common':
                engines.append(each)
        for each in engines:
            if mc.pluginInfo(each, query=True, loaded=True):
                lightNodes += self.lightAttrsFile.jsonObj[each]['lightNodes']['nodes']
        
        return lightNodes
    
    
    ##--------------------------------------------------------------------------------------------
    ##Isolate lights def
    ##--------------------------------------------------------------------------------------------
    def isolateLights (self):
        #All light node types
        lightNodes = self.lightNodes()
        
        #All render layer lights
        allLightsShapes = mc.ls(lights= True, type= lightNodes, long= True)
        allLightsScene = []
        for i in allLightsShapes:
            iTrans = mc.listRelatives(i, parent= True, fullPath=True)
            allLightsScene.append(iTrans[0])
        
        renderLayerLights = []
        currentLyr = mc.editRenderLayerGlobals( query=True, currentRenderLayer=True )
        lyrMembers = mc.editRenderLayerMembers(currentLyr, query=True, fullNames= True)
        if lyrMembers != None:
            for light in allLightsScene:
                shapeNode = mc.listRelatives(light, shapes= True, noIntermediate= True, fullPath= True)
                if light in lyrMembers or shapeNode[0] in lyrMembers:
                    renderLayerLights.append(light)
        
        #Selected scene lights
        selObjs = mc.ls(sl= True, long=True)
        selLights = []
        for e in selObjs:
            eShape = mc.listRelatives(e, shapes = True, noIntermediate = True, fullPath=True)
            eNodeType = mc.nodeType(eShape[0])
            if eNodeType in lightNodes:
                selLights.append(e)
        #Toggle visibilities
        lyr = mc.editRenderLayerGlobals(query=True, currentRenderLayer=True)
        for light in renderLayerLights:
            #Si la luz esta prendida pero no esta seleccionada, apagarla
            if mc.getAttr(light + ".visibility") == True and light not in selLights:
                mc.setAttr((light + ".visibility"), False)
            #De lo contrario, si la luz esta apagada, y no se encuentra en las luces apagadas del render layer, prenderla
            elif mc.getAttr(light + ".visibility") == False and light not in self.lightVis[lyr]:
                mc.setAttr((light + ".visibility"), True)
    
    
    ##--------------------------------------------------------------------------------------------
    ##Simple Isolate lights def
    ##--------------------------------------------------------------------------------------------
    def simpleIsolateLights (self):
        #All light node types
        lightNodes = self.lightNodes()
        #All scene lights
        allLightsShapes = mc.ls(lights= True, type= lightNodes, long= True)
        allLightsScene = []
        for i in allLightsShapes:
            iTrans = mc.listRelatives(i, parent= True, fullPath= True)
            allLightsScene.append(iTrans[0])
        
        #Selected scene lights
        selObjs = mc.ls(sl= True, long=True)
        selLights = []
        for e in selObjs:
            eShape = mc.listRelatives(e, shapes = True, noIntermediate = True, fullPath=True)
            eNodeType = mc.nodeType(eShape[0])
            if eNodeType in lightNodes:
                selLights.append(e)
        
        #Toggle visibilities
        for light in allLightsScene:
            #Si la luz esta prendida pero no esta seleccionada, apagarla
            if mc.getAttr(light + ".visibility") == True and light not in selLights:
                mc.setAttr((light + ".visibility"), False)
            #De lo contrario, si la luz esta apagada, y no se encuentra en las luces que ya estaban apagadas, prenderla
            elif mc.getAttr(light + ".visibility") == False and light not in self.simpleLightsOff:
                mc.setAttr((light + ".visibility"), True)
    
    
    ##--------------------------------------------------------------------------------------------
    ##Look thru light def
    ##--------------------------------------------------------------------------------------------
    def lookThruLight (self):
        selLight= mc.ls(sl= True, long=True)
        if len(selLight) >= 1:
            for e in selLight:
                window = mc.window(width= 629, height= 404, title= e)
                mc.paneLayout()
                thisPanel = mc.modelPanel()
                mc.showWindow( window )
                
                selLightShape = mc.listRelatives(e, shapes = True, noIntermediate = True, fullPath=True)
                minitoolsCfg = JSONManager(minitools_json, self)
                nearClip = minitoolsCfg.getValueByProperty("nearClip", False)
                farClip = minitoolsCfg.getValueByProperty("farClip", False)
                mc.lookThru(thisPanel, selLightShape[0], nc=nearClip, fc= farClip)
        elif len(selLight) == 0:
            self.baseMod.statusBar_update("Please select at least one light to look thru.", alert=True)
    
    
    ##--------------------------------------------------------------------------------------------
    ##Save render view images def
    ##--------------------------------------------------------------------------------------------
    def saveRenderViewImgs (self):
        """Based upon Andrew Chapman's script"""
        #Where to save imgs
        browseDialog = QtWidgets.QFileDialog()
        browseDialog.setFileMode(browseDialog.FileMode.Directory)
        browseDialog.setOptions(browseDialog.ShowDirsOnly)
        dialogReturn = browseDialog.exec_()
        imgsDirPath = ""
        
        if dialogReturn == 1:
            selectedDir = browseDialog.selectedFiles()
            imgsDirPath = selectedDir[0]
            #Render view panel
            rvPanel = mc.getPanel(scriptType= "renderWindowPanel")
            if len(rvPanel) < 1:
                self.baseMod.statusBar_update("Render View not found.", warning=True)
            else:
                rvForm = mc.renderWindowEditor(rvPanel[0], query= True, parent= True)
                if rvForm == None:
                    self.baseMod.statusBar_update("Render View not found.", warning=True)
                else:
                    scrollBarName = rvForm[:rvForm.rfind("renderViewForm")+15] + "scrollBarForm|scrollBar"
                    #Current viewing image
                    currImgIndex = mc.intScrollBar(scrollBarName, query= True, value= True)
                    maxImgIndex = mc.renderWindowEditor("renderView", query= True, nbImages= True) + 1
                    mc.setAttr("defaultRenderGlobals.imageFormat", 32)
                    i = 0
                    for img in range(maxImgIndex):
                        mc.renderWindowEditor(rvPanel, edit= True, displayImage= i)
                        if maxImgIndex != 1:
                            mc.renderWindowEditor(rvPanel, edit= True, displayImage= i- 1)
                        caption = mc.renderWindowEditor(rvPanel, query= True, pcaption= True)
                        if caption != "":
                            caption=  mc.renderWindowEditor(rvPanel, query= True, pcaption= True)
                            tempStr = caption.split(":")
                            frameNum = 0
                            for e in tempStr[1].split(" "):
                                if e.isdigit():
                                    frameNum = int(e)
                                    break
                            mc.renderWindowEditor(rvPanel, edit= True, colorManage= True, writeImage = imgsDirPath + "/" + "lightingWIP_" + "f" + str(frameNum) + ".00" + str(i) +".png")
                        else:
                            mc.renderWindowEditor(rvPanel, edit= True, colorManage= True, writeImage = imgsDirPath + "/" + "lightingWIP." + "00" + str(i) +".png")
                        i += 1
                    #Reset scrollbar
                    mc.renderWindowEditor(rvPanel, edit= True, displayImage= currImgIndex)
                    #Total images written
                    if i == 0:
                        self.baseMod.statusBar_update("No images written.", success=True)
                    elif i ==1:
                        self.baseMod.statusBar_update(str(i) + " Render View image written.", success=True)
                    else:
                        self.baseMod.statusBar_update(str(i) + " Render View images written.", success=True)
    
    
    ##--------------------------------------------------------------------------------------------
    ##Clean up light cameras def
    ##--------------------------------------------------------------------------------------------
    def cleanUpCams (self):
        #All light node types
        lightNodes = self.lightNodes()
        
        #All scene lights
        allLightsShapes = mc.ls(lights= True, type= lightNodes, long= True)
        allLightsScene = []
        for i in allLightsShapes:
            iTrans = mc.listRelatives(i, parent= True, fullPath=True)
            allLightsScene.append(iTrans[0])
            
        #Selected scene lights
        for each in allLightsScene:
            shapes = mc.listRelatives(each, shapes=True, noIntermediate = True, fullPath=True)
            for each in shapes:
                if mc.nodeType(each) == "camera":
                    mc.delete(each)
    
    
    ##--------------------------------------------------------------------------------------------
    ##Toggle and restore Arnold subdivs
    ##--------------------------------------------------------------------------------------------
    def toggleArnoldSubdivs (self):
        """To toggle Arnold subdivs between current state and the user desired setting."""
        allSel = mc.ls(sl=True, long=True)
        metaGeos = r9Meta.getMetaNodes(['CGXDressingMetaGeo'])
        if len(allSel) >= 1:
            if len(metaGeos >= 1):
                for metaGeo in metaGeos:
                    geo = metaGeo.getGeo()
                    for selGeo in allSel:
                        if geo in selGeo:
                            if geo not in ['',None,'None']:
                                shape = mc.listRelatives(geo, shapes=True, noIntermediate=True, fullPath=True)[0]
                                #Read and store current subdiv settings
                                currentSubdiv = mc.getAttr(shape + ".aiSubdivIterations")
                                currentSubdivType = mc.getAttr(shape + ".aiSubdivType")
                                metaToggleSubdiv = metaGeo.aiToggleSubdivIterations
                                metaToggleSubdivType = metaGeo.aiToggleSubdivType
                                #Set the attributes with stored settings
                                metaGeo.aiToggleSubdivIterations = currentSubdiv
                                metaGeo.aiToggleSubdivType = currentSubdivType
                                mc.setAttr(shape + ".aiSubdivIterations", metaToggleSubdiv)
                                mc.setAttr(shape + ".aiSubdivType", metaToggleSubdivType)
                                break
            else:
                mc.warning('No MetaGeo nodes found in the scene.')
        else:
            mc.warning('Please select something to toggle subdivs.')
    
    
    def restoreArnoldSubdivs(self):
        """To restore subdiv settings from the shading team."""
        allSel = mc.ls(sl=True, long=True)
        metaGeos = r9Meta.getMetaNodes(['CGXDressingMetaGeo'])
        if len(allSel) >= 1:
            if len(metaGeos >= 1):
                for metaGeo in metaGeos:
                    origSubdiv = metaGeo.aiSubdivIterations
                    origSubdivType = metaGeo.aiSubdivType
                    geo = metaGeo.getGeo()
                    for selGeo in allSel:
                        if geo in selGeo:
                            if geo not in ['',None,'None']:
                                shape = mc.listRelatives(geo, shapes=True, noIntermediate=True, fullPath=True)[0]
                                mc.setAttr(shape + '.aiSubdivIterations', origSubdiv)
                                mc.setAttr(shape + '.aiSubdivType', origSubdivType)
                                break
            else:
                mc.warning('No MetaGeo nodes found in the scene.')
        else:
            mc.warning('Please select something to restore subdivs to.')
    
    
    ##--------------------------------------------------------------------------------------------
    ##Align light to object center def
    ##--------------------------------------------------------------------------------------------
    def alignLightToObject (self):
        selection = mc.ls(sl=True)
        if len(selection) >= 2:
            mc.align(alignToLead=True, xAxis='mid', yAxis='mid', zAxis='mid')
        elif len(selection) <= 1:
            self.baseMod.statusBar_update("Select at least one light and one object to align the light to.", alert=True)
    
    
    ##--------------------------------------------------------------------------------------------
    ##Aim light to object def
    ##--------------------------------------------------------------------------------------------
    def aimLightToObject (self):
        selection = mc.ls(sl=True)
        if len(selection) >= 2:
            target = selection[-1]
            for each in selection[:-1]:
                mc.aimConstraint(target, each, aimVector=(0,0,-1))
        elif len(selection) <= 1:
            self.baseMod.statusBar_update("Select at least one light and one target to aim the light to.", alert=True)
    
    
    ##--------------------------------------------------------------------------------------------
    ##Create a point light to use as specular for eyes
    ##--------------------------------------------------------------------------------------------
    def specularConstrain (self, _fixed=True):
        selection = mc.ls(sl=True)
        for vertex in selection:
            if '.vtx' in vertex:
                #Create locator
                locator = mc.spaceLocator(name= 'LGT_' +  vertex.rsplit('.vtx')[0] + '_LOC')[0]
                mc.setAttr(locator + '.visibility', 0)
                #Create point light
                pntLightShape = mc.pointLight(name= 'LGT_' + vertex.rsplit('.vtx')[0] + '_01_Specular')
                #Group point light
                pntLight = mc.listRelatives(pntLightShape, parent=True, fullPath=True)[0]
                pntGrp = mc.group(pntLight, name= pntLight + '_GRP')
                #Create point on poly constrain
                vtxUVMap = mc.polyListComponentConversion(vertex, fv=True,tuv=True)
                vtxUVs = mc.polyEditUV(vtxUVMap, query=True)
                thisPopC = mc.pointOnPolyConstraint(vertex, locator, offset=(0,0,0), weight=1)
                thisPopCAttrs = mc.listAttr(thisPopC, ud=True)
                mc.setAttr(thisPopC[0] + "." + thisPopCAttrs[1], vtxUVs[0])
                mc.setAttr(thisPopC[0] + "." + thisPopCAttrs[2], vtxUVs[1])
                locWorld = mc.xform(locator, query=True, rotatePivot = True, ws= True)
                mc.setAttr(pntGrp + '.tx', locWorld[0])
                mc.setAttr(pntGrp + '.ty', locWorld[1])
                mc.setAttr(pntGrp + '.tz', locWorld[2])
                #Create parent constrain
                mc.setAttr(pntGrp + '.tz', locWorld[2] + 5)
                mc.parentConstraint(locator, pntGrp, maintainOffset=True)
                #Create organize group
                organizeGroup = mc.group(pntGrp, locator, name= 'LGT_' +  vertex.rsplit('.vtx')[0] + '_GRP')
                mc.setAttr(organizeGroup + '.tx', lock=True)
                mc.setAttr(organizeGroup + '.ty', lock=True)
                mc.setAttr(organizeGroup + '.tz', lock=True)
                mc.setAttr(organizeGroup + '.rx', lock=True)
                mc.setAttr(organizeGroup + '.ry', lock=True)
                mc.setAttr(organizeGroup + '.rz', lock=True)
                mc.setAttr(organizeGroup + '.sx', lock=True)
                mc.setAttr(organizeGroup + '.sy', lock=True)
                mc.setAttr(organizeGroup + '.sz', lock=True)
                if not _fixed:
                    mc.disconnectAttr(thisPopC[0] + '.constraintRotateX', locator + '.rx')
                    mc.disconnectAttr(thisPopC[0] + '.constraintRotateY', locator + '.ry')
                    mc.disconnectAttr(thisPopC[0] + '.constraintRotateZ', locator + '.rz')


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass

if __name__ == '__main__':
    main()