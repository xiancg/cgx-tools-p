# -*- coding: utf-8 -*-
'''
Maya class to initialize gui components

Created on Apr 08, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import cgx.core
from cgx.libs.Abstract_InitUI import Abstract_InitUI


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Maya Init UI Components
##--------------------------------------------------------------------------------------------
class Maya_InitUI(Abstract_InitUI):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _studioInfo, _projectInfo, _folderStructure=cgx.core.FOLDER_STRUCTURE):
        '''
        Maya init ui class. Get info from Maya and set it to the gui components.
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param _folderStructure: Absolute path to the json template file for the project folder structure.
        :type _folderStructure: string
        '''
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------    
    def setCurrentProject(self, _projectComponent):
        '''
        Sets project component to app project if possible.
        :param _projectComponent: Gui component to set the project on.
        :type _projectComponent: Project_Component
        '''
        _projectComponent.setToItem(self.pInfo.maya_getProject())
    
    
    def setCurrentSequence(self, _sequenceComponent):
        '''
        Sets sequence component to file sequence if possible.
        :param _sequenceComponent: Gui component to set the sequence on.
        :type _sequenceComponent: Sequence_Component
        '''
        _sequenceComponent.setToItem(self.pInfo.maya_getSequence())
    
    
    def setCurrentShot(self, _shotComponent):
        '''
        Sets shot component to file if possible.
        :param _shotComponent: Gui component to set the shot on.
        :type _shotComponent: Shot_Component
        '''
        _shotComponent.setToItem(self.pInfo.maya_getShot())
    
    
    def setCurrentAssetType(self, _assetTypeComponent):
        '''
        Sets asset type component to file if possible.
        :param _assetTypeComponent: Gui component to set the asset type on.
        :type _assetTypeComponent: AssetTypes_Component
        '''
        _assetTypeComponent.setToItem(self.pInfo.maya_getAssetType())
    
    
    def setCurrentAsset(self, _assetComponent):
        '''
        Sets asset component to file if possible.
        :param _assetComponent: Gui component to set the asset on.
        :type _assetComponent: Shot_Component
        '''
        _assetComponent.setToItem(self.pInfo.maya_getAsset())
        
    
    def setCurrentSubAsset(self, _subassetComponent):
        '''
        Sets subasset component to file if possible.
        :param _subassetComponent: Gui component to set the subasset on.
        :type _subassetComponent: SubAssets_Component
        '''
        _subassetComponent.setToItem(self.pInfo.maya_getSubAsset())
    
    
    def setCurrentFileType(self, _fileTypeComponent):
        '''
        Sets file type component to file shot if possible.
        :param _fileTypeComponent: Gui component to set the subasset on.
        :type _fileTypeComponent: FileTypes_Component
        '''
        _fileTypeComponent.setToItem(self.pInfo.maya_getFileType())
    
    
    def setCurrentPipelineStage(self, _pipelineStageComponent):
        '''
        Sets pipeline stage component to file stage if possible.
        :param _pipelineStageComponent: Gui component to set the pipeline stage on.
        :type _pipelineStageComponent: PipelineStages_Component
        '''
        _pipelineStageComponent.setToItem(self.pInfo.maya_getPipelineStage())
        
        
    def checkFPS(self, _FPSComponent):
        '''
        Updates FPS_Component to red if FPS from project config file and app do not match. Black if they do match. 
        :param _FPSComponent: Gui component to update
        :type _FPSComponent: FPS_Component
        '''
        _FPSComponent.componentUpdate(self.pInfo.maya_getFPS())


##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------
def main():
    pass
    

if __name__=="__main__":
    main()