# -*- coding: utf-8 -*-
'''
Maya class to manage project info.

Modified on Apr 04, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com
http://www.chrisgranados.com/
'''


# --------------------------------------------------------------------------------------------
# imports
# --------------------------------------------------------------------------------------------
import maya.cmds as mc
from cgx.core.ProjectInfo import ProjectInfo
import cgx.core


# --------------------------------------------------------------------------------------------
# Metadata
# --------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


# --------------------------------------------------------------------------------------------
# Class: Maya Project Info object
# --------------------------------------------------------------------------------------------
class Maya_ProjectInfo(ProjectInfo):
    # --------------------------------------------------------------------------------------------
    # Constructor
    # --------------------------------------------------------------------------------------------
    def __init__(self, _studioInfo, _project, _folderStructure=cgx.core.FOLDER_STRUCTURE):
        super(Maya_ProjectInfo, self).__init__(_studioInfo, _project, _folderStructure)

    # --------------------------------------------------------------------------------------------
    # Methods
    # --------------------------------------------------------------------------------------------
    def maya_getProject(self):
        '''
        Retrieves the current project set in Maya.
        :return: Name for the current project set in Maya.
        :rtype: string
        '''
        currProj = mc.workspace(fullName=True)
        index = currProj.rfind("/")
        currProjShort = currProj[index + 1:]

        filePath = mc.file(expandName=True, query=True)
        if '\\' in filePath:
            filePath.replace('\\', '/')
        projectByFolder = filePath.split('/')[1]
        if projectByFolder not in ['', None, 'None']:
            currProjShort = projectByFolder

        return currProjShort

    def maya_getFPS(self):
        '''
        Retrieves the current file frames per second
        :return: Frames per second configuration for the current file in Maya.
        :rtype: int
        '''
        fpsValue = mc.currentUnit(q=True, time=True)
        fpsNumber = 0
        if fpsValue == 'game':
            fpsNumber = 15
        elif fpsValue == '23.976fps':
            fpsNumber = 23.976
        elif fpsValue == 'film':
            fpsNumber = 24
        elif fpsValue == 'pal':
            fpsNumber = 25
        elif fpsValue == 'ntsc':
            fpsNumber = 30
        elif fpsValue == 'show':
            fpsNumber = 48
        elif fpsValue == 'palf':
            fpsNumber = 50
        elif fpsValue == 'ntscf':
            fpsNumber = 60
        elif fpsValue[-3:] == "fps":
            fpsNumber = int(fpsValue[:-3].split(".")[0])

        return fpsNumber

    def maya_getSequence(self):
        '''
        Retrieves the sequence for the current file in Maya. Defaults to first
        sequence if no match found.
        :return: Sequences name
        :rtype: string
        '''
        filePath = mc.file(expandName=True, query=True)
        splitList = self.__splitPath(filePath)

        seqsList = self.getSequences(self.sInfo.getSequencesFrom())

        if seqsList is not None:
            if len(seqsList) > 0:
                seqName = seqsList[0]
                for each in splitList:
                    if each in seqsList:
                        seqName = each
                        break

                return seqName
            else:
                return ''
        else:
            return ''

    def maya_getShot(self):
        '''
        Retrieves the shot for the current file in Maya. Defaults to first shot
        if no match found.
        :return: Sequences name
        :rtype: string
        '''
        filePath = mc.file(expandName=True, query=True)
        splitList = self.__splitPath(filePath)

        seqName = self.maya_getSequence()
        shotsList = self.getShots(seqName, self.sInfo.getSequencesFrom())

        if shotsList is not None:
            if len(shotsList) > 0:
                shotName = shotsList[0]
                for each in splitList:
                    if each in shotsList:
                        shotName = each
                        break

                return shotName
            else:
                return None
        else:
            return None

    def maya_getAssetType(self):
        '''
        Retrieves the asset type for the current file in Maya. Defaults to
        first asset type if no match found.
        :return: Asset type
        :rtype: string
        '''
        filePath = mc.file(expandName=True, query=True)
        splitList = self.__splitPath(filePath)

        assetTypes = self.getAssetTypes()

        if assetTypes is not None:
            if len(assetTypes) > 0:
                thisAssetType = assetTypes[0]
                for each in splitList:
                    if each in assetTypes:
                        thisAssetType = each
                        break

                return thisAssetType
            else:
                return None
        else:
            return None

    def maya_getAsset(self):
        '''
        Retrieves the asset name for the current file in Maya. Defaults to
        first asset name if no match found.
        :return: Asset name
        :rtype: string
        '''
        filePath = mc.file(expandName=True, query=True)
        splitList = self.__splitPath(filePath)

        assetType = self.maya_getAssetType()
        if assetType is not None:
            assetsList = self.getAssets(assetType)
            if assetsList is not None:
                assetName = assetsList[0]
                for each in splitList:
                    if each in assetsList:
                        assetName = each
                        break
                return assetName
            else:
                return None
        else:
            return None

    def maya_getSubAsset(self):
        '''
        Retrieves the subasset name for the current file in Maya. Defaults to
        None if no match found.
        :return: Subasset name
        :rtype: string
        '''
        filePath = mc.file(expandName=True, query=True)
        splitList = self.__splitPath(filePath)

        assetType = self.maya_getAssetType()
        assetName = self.maya_getAsset()
        subAssetsList = self.getSubAssets(assetType, assetName)
        if subAssetsList is None:
            subAssetsList = []

        subassetName = None
        for each in splitList:
            if each in subAssetsList:
                subassetName = each
                break

        return subassetName

    def maya_getFileType(self):
        '''
        Retrieves the file type for the current file in Maya. Defaults to first
        file type if no match found.
        :return: File type abbreviation
        :rtype: string
        '''
        filePath = mc.file(expandName=True, query=True)
        splitList = self.__splitPath(filePath)
        fileNameList = splitList[-1].split("_")

        fileTypes = self.getFileTypes()

        thisFileType = fileTypes.items()[0][0] + ":" + fileTypes.items()[0][1]
        if bool(set(fileNameList) & set(fileTypes.values())):
            for key, value in fileTypes.iteritems():
                if value in fileNameList:
                    thisFileType = key + ":" + value
        return thisFileType

    def maya_getPipelineStage(self):
        '''
        Retrieves the pipeline step for the current file in Maya. Defaults to
        first pipeline step if no match found.
        :return: File type abbreviation
        :rtype: string
        '''
        filePath = mc.file(expandName=True, query=True)
        splitList = self.__splitPath(filePath)

        pipelineStages = self.getPipelineStages()

        thisStep = [0]
        for each in splitList:
            if each in pipelineStages:
                thisStep = each
                break

        return thisStep

    def __splitPath(self, _path):
        '''
        Retrieves the given path split by folders.
        :return: Absolute path to a file.
        :rtype: string
        '''
        slashIndex = _path.rfind("/")
        splitList = []
        if slashIndex == -1:
            splitList = _path.split("\\")
        else:
            splitList = _path.split("/")

        return splitList


# --------------------------------------------------------------------------------------------
# Main
# --------------------------------------------------------------------------------------------
def main():
    pass


if __name__ == "__main__":
    main()
