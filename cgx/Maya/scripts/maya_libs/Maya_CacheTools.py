# -*- coding: utf-8 -*-
'''
Cache tools library for Maya.

Created on Nov 15, 2015

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
Pending:
-Implement Progress_Component instead of pBar
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.gui.Qt import QtWidgets
import maya.cmds as mc
import maya.mel as mel
import cgx.Maya.scripts.maya_libs.Maya_TimeRange as tRange
from cgx.Maya.scripts.maya_libs.Maya_InitUI import Maya_InitUI
from cgx.gui.components.Pipeline_Components import CacheFileTypes_Component
from cgx.core.JSONManager import JSONManager
import os


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
import cgx.Maya.scripts.pipeline.CacheExporter.CacheExporter as chExp
chExpFolder = os.path.dirname(os.path.abspath(chExp.__file__))
alembicVersion_json = chExpFolder + "/cfg/" + "AlembicVersion_config.json"


##--------------------------------------------------------------------------------------------
##Class: Cache Tools
##--------------------------------------------------------------------------------------------
class Maya_CacheTools():
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _studioInfo, _projectInfo, _timeRange = tRange.Maya_TimeRange(), _sampleBy = 0.25):
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        self.tRange = _timeRange
        self.app_initUI = Maya_InitUI(self.sInfo, self.pInfo)
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def checkPlugins (self, _gui):
        '''
        Check if plugins are loaded.
        :param _gui: Pointer to parent tool gui QMainWindow.
        :type _gui: QMainWindow
        :return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
        :rtype: dict
        '''
        check = {}
        try:
            if type(_gui.cacheComp) is CacheFileTypes_Component:
                if _gui.cacheComp.alembic_RBTN.isChecked():
                    if not mc.pluginInfo("AbcExport", query=True, loaded=True):
                        check["Alembic"] = "Alembic ABC Export plugin not loaded."
                        _gui.baseMod.statusBar_update(check["Alembic"], alert=True)
                    if not mc.pluginInfo("AbcImport", query=True, loaded=True):
                        check["AlembicImport"] = "Alembic ABC Import plugin not loaded."
                        _gui.baseMod.statusBar_update(check["Alembic"], alert=True)
                    if not mc.pluginInfo("gpuCache", query=True, loaded=True):
                        check["GPUcache"] = "GPU Cache plugin not loaded."
                        _gui.baseMod.statusBar_update(check["Alembic"], alert=True)
                if _gui.cacheComp.alembicExo_RBTN.isChecked():
                    if not mc.pluginInfo("MayaExocortexAlembic", query=True, loaded=True):
                        check["Exocortex"] = "Exocortex Alembic plugin not loaded."
                        _gui.baseMod.statusBar_update(check["Exocortex"], alert=True)
                return check
        except AttributeError:
            if _gui.cacheMod.CacheFileTypes_Component.alembic_RBTN.isChecked():
                if not mc.pluginInfo("AbcExport", query=True, loaded=True):
                    check["Alembic"] = "Alembic ABC Export plugin not loaded."
                    _gui.baseMod.statusBar_update(check["Alembic"], alert=True)
                if not mc.pluginInfo("AbcImport", query=True, loaded=True):
                    check["AlembicImport"] = "Alembic ABC Import plugin not loaded."
                    _gui.baseMod.statusBar_update(check["Alembic"], alert=True)
                if not mc.pluginInfo("gpuCache", query=True, loaded=True):
                    check["GPUcache"] = "GPU Cache plugin not loaded."
                    _gui.baseMod.statusBar_update(check["Alembic"], alert=True)
            if _gui.cacheMod.CacheFileTypes_Component.alembicExo_RBTN.isChecked():
                if not mc.pluginInfo("MayaExocortexAlembic", query=True, loaded=True):
                    check["Exocortex"] = "Exocortex Alembic plugin not loaded."
                    _gui.baseMod.statusBar_update(check["Exocortex"], alert=True)
            return check
    
    
    def createBakingMethods(self,_geoDict, _pBar, _sampleBy):
        return BakingMethods(_geoDict, _pBar, _sampleBy)
    
    
    def createExporter(self, _geom, _timeRange, _pBar, _path, _sampleBy):
        return Exporter(self.sInfo, self.pInfo, _geom, _timeRange, _pBar, _path, _sampleBy)
    

##--------------------------------------------------------------------------------------------
##Class: Baking methods
##--------------------------------------------------------------------------------------------
class BakingMethods():
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _geoDict = {}, _pBar = QtWidgets.QProgressBar(), _sampleBy= 0.25):
        self.geoDict = _geoDict
        self.bakedGeom = []
        self.pBar = _pBar
        self.sampleBy = _sampleBy
        self.clusters = []
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def vertexBakeMethod(self, _geom):
        """def that bakes the animation to the geometry using polyUnite"""
        cleanName = self.geoDict[_geom][1] + "_" + self.geoDict[_geom][0] + "_bkd"
        polyUniteNode = mc.createNode("polyUnite", name = self.geoDict[_geom][1] + "_" + self.geoDict[_geom][0] + "_pu")
        cubeProxy = mc.polyCube(name= cleanName, ch= False)
        cubeShape = mc.listRelatives(cubeProxy[0], shapes= True, fullPath= True, noIntermediate= True)
        mc.connectAttr(_geom + ".worldMatrix[0]", polyUniteNode + ".inputMat[0]", force= True)
        mc.connectAttr(_geom + ".worldMesh[0]", polyUniteNode + ".inputPoly[0]", force= True)
        mc.connectAttr(polyUniteNode + ".output", cubeShape[0] + ".inMesh", force= True)
        mc.connectAttr(_geom + ".visibility", cubeProxy[0] + ".visibility", force= True)
        #Proxy deformer to cheat Alembic
        proxyCluster = mc.cluster(cubeProxy[0], name= cleanName + "_clst")
        self.clusters.append(proxyCluster[1])
        
        self.bakedGeom.append(cubeProxy[0])
        
        #Move progress bar
        self.pBar.setValue(self.pBar.value() + 1)
        
        return cubeProxy[0]

    
    def getGeomList(self):
        """def returns the geom list"""
        
        return self.geoDict.keys()
    
    
    def getBakedGeomList(self):
        """def returns the baked geom list"""
        
        return self.bakedGeom
    
    
    def getClustersList(self):
        """def returns the clusters list"""
        
        return self.clusters
    
    
    def resetGeomClusters (self):
        
        self.bakedGeom = []
        self.clusters = []


##--------------------------------------------------------------------------------------------
##Class: Exporter methods
##--------------------------------------------------------------------------------------------
class Exporter():
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _studioInfo, _projectInfo, _geom = "", _timeRange = tRange.Maya_TimeRange(), _pBar = QtWidgets.QProgressBar(), _path = "", _sampleBy = 0.25):
        self.geom = _geom
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        self.path = _path
        if "\\" in self.path:
            self.path = self.path.replace("\\","/")
        self.sampleBy = _sampleBy
        
        self.timeRange = _timeRange
        self.pBar = _pBar
        
        alembicJson = JSONManager(alembicVersion_json, self)
        self.alembicVersion = alembicJson.getValueByProperty('alembicVersion', False)
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def exportAlembic (self, _rootName = "", _oneFile= False):
        """def exports alembic files for the specified geometry"""
        
        alembicLoaded = mc.pluginInfo("AbcExport", query=True, loaded=True)
        if alembicLoaded:
            startFrame = self.timeRange.startFrame - self.timeRange.handleIn
            endFrame = self.timeRange.endFrame + self.timeRange.handleOut
            frameRangeStr = "-frameRange " + str(int(startFrame)) + " " + str(int(endFrame))
            
            if _oneFile:
                rootsString = ""
                for each in self.geom:
                    rootsString += " -root " + each
                cleanName = _rootName.replace(":","_")
                melEvalStr = 'AbcExport -j "' + frameRangeStr + ' -s ' + str(self.sampleBy) + ' -dataFormat {}'.format(self.alembicVersion) + rootsString + ' -uvWrite -writeVisibility -ws -file ' + self.path + "/" + cleanName + '.abc' + '";'
                alembicResult = mel.eval(melEvalStr)
            else:
                if type(self.geom) is list:
                    for each in self.geom:
                        cleanName = each.replace(":","_")
                        melEvalStr = 'AbcExport -j "'+ '-sl ' + frameRangeStr + ' -s ' + str(self.sampleBy) + ' -dataFormat {}'.format(self.alembicVersion) + ' -root ' + each + ' -uvWrite -writeVisibility -ws -file ' + self.path + "/" + cleanName + '.abc' + '";'
                        alembicResult = mel.eval(melEvalStr)
                else:
                    cleanName = self.geom.replace(":","_")
                    melEvalStr = 'AbcExport -j "' + frameRangeStr + ' -s ' + str(self.sampleBy) + ' -dataFormat {}'.format(self.alembicVersion) + ' -root ' + self.geom + ' -uvWrite -writeVisibility -ws -file ' + self.path + "/" + cleanName + '.abc' + '";'
                    alembicResult = mel.eval(melEvalStr)
            #Move progress bar
            self.pBar.setValue(self.pBar.value() + 1)
        else:
            mc.warning( "Alembic AbcExport.mll plugin is not loaded" )
    
    
    def exportAlembicBake (self, _cleanName, _oneFile= False):
        """def exports alembic files for the specified geometry"""
        
        alembicLoaded = mc.pluginInfo("AbcExport", query=True, loaded=True)
        if alembicLoaded:
            startFrame = self.timeRange.startFrame - self.timeRange.handleIn
            endFrame = self.timeRange.endFrame + self.timeRange.handleOut
            frameRangeStr = "-frameRange " + str(int(startFrame)) + " " + str(int(endFrame))
            
            if _oneFile:
                rootsString = ""
                for each in self.geom:
                    rootsString += " -root " + each
                cleanName = _cleanName.replace(":","_")
                melEvalStr = 'AbcExport -j "' + frameRangeStr + ' -s ' + str(self.sampleBy) + ' -dataFormat {}'.format(self.alembicVersion) + rootsString + ' -uvWrite -writeVisibility -ws -file ' + self.path + "/" + cleanName + '.abc' + '";'
                alembicResult = mel.eval(melEvalStr)
            else:
                if type(self.geom) is list:
                    for each in self.geom:
                        cleanName = each.replace(":","_")
                        melEvalStr = 'AbcExport -j "'+ '-sl ' + frameRangeStr + ' -s ' + str(self.sampleBy) + ' -dataFormat {}'.format(self.alembicVersion) + ' -root ' + each + ' -uvWrite -writeVisibility -ws -file ' + self.path + "/" + cleanName + '.abc' + '";'
                        alembicResult = mel.eval(melEvalStr)
                else:
                    cleanName = self.geom.replace(":","_")
                    melEvalStr = 'AbcExport -j "' + frameRangeStr + ' -s ' + str(self.sampleBy) + ' -dataFormat {}'.format(self.alembicVersion) + ' -root ' + self.geom + ' -uvWrite -writeVisibility -ws -file ' + self.path + "/" + cleanName + '.abc' + '";'
                    alembicResult = mel.eval(melEvalStr)
                
            #Move progress bar
            self.pBar.setValue(self.pBar.value() + 1)
        else:
            mc.warning( "Alembic AbcExport.mll plugin is not loaded" )
        
        
    def exportAlembicBakeExo(self, _cleanName, _oneFile= False, _includeSurface = False):
        """def exports alembic files using Exocortex and the entire transformation hierarchy"""
        
        alembicExoLoaded = mc.pluginInfo("MayaExocortexAlembic", query=True, loaded=True)
        if alembicExoLoaded:
            startFrame = "in=" + str(self.timeRange.startFrame - self.timeRange.handleIn)
            endFrame = "out="  + str(self.timeRange.endFrame + self.timeRange.handleOut)
            step = "step=1"
            substep = "substep=" + str(1/self.sampleBy)
            mainOptions = "purepointcache=1;globalspace=0;withouthierarchy=0;{}=1".format(self.alembicVersion)
            if _includeSurface:
                mainOptions = "purepointcache=0;globalspace=0;withouthierarchy=0;{}=1".format(self.alembicVersion)
            variuosOptions = "normals=1;uvs=1;facesets=0;dynamictopology=0;transformcache=1"
            if _oneFile:
                cleanName = _cleanName.replace(":","_")
                fileNameStr = "filename=" + self.path + "/" + cleanName + ".abc"
                objectsStr = "objects=" + ",".join(self.geom)
                finalStr = ";".join([fileNameStr, objectsStr, startFrame, endFrame, step, substep, mainOptions, variuosOptions])
                mc.ExocortexAlembic_export(j=[finalStr])
            else:
                if type(self.geom) is list:
                    for each in self.geom:
                        cleanName = each.replace(":","_")
                        fileNameStr = "filename=" + self.path + "/" + cleanName + ".abc"
                        objectsStr = "objects=" + each
                        finalStr = ";".join([fileNameStr, objectsStr, startFrame, endFrame, step, substep, mainOptions, variuosOptions])
                        mc.ExocortexAlembic_export(j=[finalStr])
                else:
                    cleanName = self.geom.replace(":","_")
                    fileNameStr = "filename=" + self.path + "/" + cleanName + ".abc"
                    objectsStr = "objects=" + self.geom
                    finalStr = ";".join([fileNameStr, objectsStr, startFrame, endFrame, step, substep, mainOptions, variuosOptions])
                    mc.ExocortexAlembic_export(j=[finalStr])
            
            #Move progress bar
            self.pBar.setValue(self.pBar.value() + 1)
        else:
            mc.warning( "Exocortex MayaExocortexAlembic.mll plugin is not loaded" )
    
    
    def exportAlembicExo(self,_cleanName, _oneFile= False, _includeSurface = False):
        """def exports alembic files using Exocortex and baking all to vertex"""
        
        alembicExoLoaded = mc.pluginInfo("MayaExocortexAlembic", query=True, loaded=True)
        if alembicExoLoaded:
            startFrame = "in=" + str(self.timeRange.startFrame - self.timeRange.handleIn)
            endFrame = "out="  + str(self.timeRange.endFrame + self.timeRange.handleOut)
            step = "step=1"
            substep = "substep=" + str(1/self.sampleBy)
            mainOptions = "purepointcache=1;globalspace=1;withouthierarchy=1;{}=1".format(self.alembicVersion)
            if _includeSurface:
                mainOptions = "purepointcache=0;globalspace=1;withouthierarchy=1;{}=1".format(self.alembicVersion)
            variuosOptions = "normals=1;uvs=1;facesets=0;dynamictopology=0;transformcache=0"
            if _oneFile:
                cleanName = _cleanName.replace(":","_")
                fileNameStr = "filename=" + self.path + "/" + cleanName + ".abc"
                objectsStr = "objects=" + ",".join(self.geom)
                finalStr = ";".join([fileNameStr, objectsStr, startFrame, endFrame, step, substep, mainOptions, variuosOptions])
                mc.ExocortexAlembic_export(j=[finalStr])
            else:
                if type(self.geom) is list:
                    for each in self.geom:
                        cleanName = each.replace(":","_")
                        fileNameStr = "filename=" + self.path + "/" + cleanName + ".abc"
                        objectsStr = "objects=" + each
                        finalStr = ";".join([fileNameStr, objectsStr, startFrame, endFrame, step, substep, mainOptions, variuosOptions])
                        mc.ExocortexAlembic_export(j=[finalStr])
                else:
                    cleanName = self.geom.replace(":","_")
                    fileNameStr = "filename=" + self.path + "/" + cleanName + ".abc"
                    objectsStr = "objects=" + self.geom
                    finalStr = ";".join([fileNameStr, objectsStr, startFrame, endFrame, step, substep, mainOptions, variuosOptions])
                    mc.ExocortexAlembic_export(j=[finalStr])
            
            #Move progress bar
            self.pBar.setValue(self.pBar.value() + 1)
        else:
            mc.warning( "Exocortex MayaExocortexAlembic.mll plugin is not loaded" )

    
    
    def exportPointCache (self, _rootName = "",  _oneFile= False):
        """def exports point cache for the specified geometry"""
        
        startFrame = self.timeRange.startFrame - self.timeRange.handleIn
        endFrame = self.timeRange.endFrame + self.timeRange.handleOut
        if _oneFile:
            mc.select(clear=True)
            mc.select(self.geom, replace= True)
            mel.eval('doCreateGeometryCache 6 ' + '{"3","' + str(startFrame) + '","' + str(endFrame) + '","OneFile","0","'+ self.path +'","0","'+ _rootName + '","0","export","1","' + str(self.sampleBy) + '","1","0","1","mcx","0"};')
        else:
            geomShape = mc.listRelatives(self.geom, shapes = True, noIntermediate = True, fullPath= True)
            cacheFile = mc.cacheFile(fileName= self.geom, sampleMultiplier=1, simulationRate = self.sampleBy, st= startFrame, et= endFrame, worldSpace= True, format= "OneFile", singleCache= True, dir= self.path, points= geomShape[0])
        
        #Move progress bar
        self.pBar.setValue(self.pBar.value() + 1)
  
    
    def exportObj (self, _pathObj):
        """def exports objs for the specified geometry"""
        
        objLoaded = mc.pluginInfo("objExport", query=True, loaded=True)
        if objLoaded:
            if type(self.geom) is list:
                for each in self.geom:
                    geomShape = mc.listRelatives(each , shapes = True, noIntermediate = True, fullPath= True)
                    mc.select(clear= True)
                    mc.select(each, r= True)
                    cleanName = each.replace(":","_")
                    mc.file(_pathObj + "/" + cleanName + ".obj", force= True, options= "groups=0;ptgroups=0;materials=0;smoothing=0;normals=0", typ= "OBJexport", pr= True, es = True)
            else:
                geomShape = mc.listRelatives(self.geom , shapes = True, noIntermediate = True, fullPath= True)
                mc.select(clear= True)
                mc.select(self.geom, r= True)
                cleanName = self.geom.replace(":","_")
                mc.file(_pathObj + "/" + cleanName + ".obj", force= True, options= "groups=0;ptgroups=0;materials=0;smoothing=0;normals=0", typ= "OBJexport", pr= True, es = True)
            
            #Move progress bar
            self.pBar.setValue(self.pBar.value() + 1)
        else:
            mc.warning( "Obj objExport.mll plugin is not loaded" )


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass


if __name__=="__main__":
    main()