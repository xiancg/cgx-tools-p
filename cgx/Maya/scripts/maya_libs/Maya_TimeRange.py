# -*- coding: utf-8 -*-
'''
Maya TimeRange implementation of Abstract_TimeRange library.

Created on Mar 26, 2015

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.libs.Abstract_TimeRange import Abstract_TimeRange
import maya.cmds as mc


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Maya Time Range
##--------------------------------------------------------------------------------------------
class Maya_TimeRange(Abstract_TimeRange):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self):
        super(Maya_TimeRange, self).__init__()
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def getTimeSlider (self):
        '''
        Gets the the time slider start and end frames and returns them.
        :return: [startFrame,endFrame]
        :rType: list
        '''
        
        start = mc.playbackOptions(query= True, minTime = True)
        end = mc.playbackOptions(query= True, maxTime = True)
        thisTimeRange = [start, end]
        
        return thisTimeRange
    
    
    def setTimeSlider(self, _start, _end):
        mc.playbackOptions(edit= True, minTime = _start, animationStartTime= _start)
        mc.playbackOptions(edit= True, maxTime = _end, animationEndTime= _end)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass


if __name__=="__main__":
    main()