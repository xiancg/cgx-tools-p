# -*- coding: utf-8 -*-
'''
unittests for Maya_ProjectInfo

Created on Mar 31, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import sys
sys.path.append('M:/CGXtools')

import maya.standalone
maya.standalone.initialize(name='python')
import maya.cmds as mc
import maya.mel as mel
mel.eval('setProject "M:/Dropbox/DT/Maya/GizmoServer/AgeOfGods_3D";')

from cgx.Maya.scripts.maya_libs.Maya_ProjectInfo import Maya_ProjectInfo
from cgx.core.StudioInfo import StudioInfo
import unittest

##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Test for Abstract Project info object
##--------------------------------------------------------------------------------------------
class Test_Maya_ProjectInfo(unittest.TestCase):
    ##--------------------------------------------------------------------------------------------
    ##Setup/Teardown
    ##--------------------------------------------------------------------------------------------
    def setUp(self):
        #self.configFolder = "D:/XianCG/CGXtools/core/tests/resources"
        self.configFolder = "M:/CGXtools/core/tests/resources"
        #Studio info
        self.gizmoStructureFile= self.configFolder + "/Gizmo_cfg/studio_structure.json"
        self.gizmoPropertiesFile= self.configFolder + "/Gizmo_cfg/studio_properties.json"
        self.gizmoNamingFile= self.configFolder + "/Gizmo_cfg/studio_namingconventions.json"
        self.sinemaStructureFile= self.configFolder + "/Sinema_cfg/studio_structure.json"
        self.sinemaPropertiesFile= self.configFolder + "/Sinema_cfg/studio_properties.json"
        self.sinemaNamingFile= self.configFolder + "/Sinema_cfg/studio_namingconventions.json"
        self.sInfo = StudioInfo(self.gizmoStructureFile, self.gizmoPropertiesFile, self.gizmoNamingFile)
        self.proj = self.sInfo.getProjects("","")

        #Project info
        self.gizmoFolderStructFile = self.configFolder + "/Gizmo_cfg/3D_structure.json"
        self.sinemaFolderStructFile =  self.configFolder + "/Sinema_cfg/project_structure.json"
        self.pInfo = Maya_ProjectInfo(self.sInfo, self.gizmoFolderStructFile, self.proj[0])

    def tearDown(self):
        pass
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    #@unittest.SkipTest
    def test_getSequences_gizmo(self):
        #print self.pInfo.getSequences(self.sInfo.getSequencesFrom())
        self.assertTrue(len(self.pInfo.getSequences(self.sInfo.getSequencesFrom())) > 0)
    
    def test_maya_getProject(self):
        returned= self.pInfo.maya_getProject()
        self.assertEqual(returned,'AgeOfGods_3D')
        
    def test_maya_getFPS(self):
        mc.file('M:/Dropbox/DT/Maya/GizmoServer/AgeOfGods_3D/ASSETS/CHARACTERS/Archer/Published/MOD_Archer.ma', open=True)
        returned = self.pInfo.maya_getFPS()
        self.assertEqual(returned,24)
        
    def test_maya_getSequence(self):
        mc.file('M:/Dropbox/DT/Maya/GizmoServer/AgeOfGods_3D/LIGHTING/SEQ_03/s_010/LGT_SEQ_03_s_010.0001.ma', open=True)
        returned = self.pInfo.maya_getSequence()
        self.assertEqual(returned,'SEQ_03')
    
    def test_maya_getShots(self):
        mc.file('M:/Dropbox/DT/Maya/GizmoServer/AgeOfGods_3D/LIGHTING/SEQ_03/s_010/LGT_SEQ_03_s_010.0001.ma', open=True)
        returned = self.pInfo.maya_getShot()
        self.assertEqual(returned,'s_010')
    
    def test_maya_getAssetType(self):
        mc.file('M:/Dropbox/DT/Maya/GizmoServer/AgeOfGods_3D/ASSETS/CHARACTERS/Archer/Published/MOD_Archer.ma', open=True)
        returned = self.pInfo.maya_getAssetType()
        self.assertEqual(returned,'CHARACTERS')
    
    def test_maya_getAsset(self):
        mc.file('M:/Dropbox/DT/Maya/GizmoServer/AgeOfGods_3D/ASSETS/CHARACTERS/Archer/Published/MOD_Archer.ma', open=True)
        returned = self.pInfo.maya_getAsset()
        self.assertEqual(returned,'Archer')
        
    def test_maya_getFileType_gizmo(self):
        mc.file('M:/Dropbox/DT/Maya/GizmoServer/AgeOfGods_3D/ASSETS/CHARACTERS/Archer/Published/MOD_Archer.ma', open=True)
        returned = self.pInfo.maya_getFileType()
        self.assertEqual(returned,'modeling:MOD')
    
    def test_maya_getPipelineStep(self):
        mc.file('M:/Dropbox/DT/Maya/GizmoServer/AgeOfGods_3D/LIGHTING/SEQ_03/s_010/LGT_SEQ_03_s_010.0001.ma', open=True)
        returned = self.pInfo.maya_getPipelineStep()
        self.assertEqual(returned,'LIGHTING')


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass


if __name__=="__main__":
    unittest.main()