# -*- coding: utf-8 -*-
'''
unittests for Maya_CameraTools library

Created on Sep 15, 2015

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import cgx.Maya.scripts.maya_libs.Maya_CameraTools as maya_cam
import unittest
from mock import MagicMock


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Maya Camera Exporter
##--------------------------------------------------------------------------------------------
class Test_Maya_CameraExporter(unittest.TestCase):
    ##--------------------------------------------------------------------------------------------
    ##Setup/Teardown
    ##--------------------------------------------------------------------------------------------
    def setUp(self):
        self.camExporter = maya_cam.Maya_CameraTools("C_SEQ_01_s_010_f_101_512","C:/")

    def tearDown(self):
        pass
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def test_checkList(self):
        maya_cam.mc.ls = MagicMock(return_value=["unknownPlugin","anotherUnknown"])
        maya_cam.mc.pluginInfo = MagicMock(return_value=True)
        returned = self.camExporter.checkList()
        self.assertEqual(returned, {'Unknown': 'unknownPlugin, anotherUnknown'})
    
    def test_exportCamera_ma(self):
        maya_cam.mc.select = MagicMock(return_value=True)
        maya_cam.mc.file = MagicMock(return_value=True)
        maya_cam.mc.delete = MagicMock(return_value=True)
        returned = self.camExporter.exportCamera(self.camExporter.cam, "C_SEQ_01_s_010", ma=True)
        self.assertTrue(returned)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass


if __name__=="__main__":
    main()