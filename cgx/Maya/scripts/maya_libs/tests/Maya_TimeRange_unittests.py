# -*- coding: utf-8 -*-
'''
unittests for Maya_CameraTools library

Created on Sep 15, 2015

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.Maya.scripts.maya_libs.Maya_TimeRange import Maya_TimeRange
import unittest
from mock import MagicMock


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Maya Time Range
##--------------------------------------------------------------------------------------------
class Test_Maya_TimeRange(unittest.TestCase):
    ##--------------------------------------------------------------------------------------------
    ##Setup/Teardown
    ##--------------------------------------------------------------------------------------------
    def setUp(self):
        self.tRange = Maya_TimeRange()

    def tearDown(self):
        pass
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def test_setFramesToTimeSlider(self):
        self.tRange.getTimeSlider = MagicMock(return_value=[101,217])
        self.tRange.setFramesToTimeSlider()
        self.assertEqual([self.tRange.startFrame,self.tRange.endFrame],[101,217])
        
    def test_offsetTimeRange_offsetTo400(self):
        returned = self.tRange.offsetTimeRange(1001)
        self.assertEqual([returned['startFrame'],returned['endFrame']],[1001,1024])
        self.assertEqual([returned['absoluteStartFrame'],returned['absoluteEndFrame']],[1001,1030])
        self.assertEqual(returned['timeChange'],1000)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass


if __name__=="__main__":
    main()