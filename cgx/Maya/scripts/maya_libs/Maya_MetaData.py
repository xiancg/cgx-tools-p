# -*- coding: utf-8 -*-
'''
Library to manage Metadata using red9 codebase.

Red9 Studio Pack: Maya Pipeline Solutions
Author: Mark Jackson
email: rednineinfo@gmail.com

Red9 blog : http://red9-consultancy.blogspot.co.uk/
MarkJ blog: http://markj3d.blogspot.co.uk

Created on May 22, 2017

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import Red9.core.Red9_Meta as r9Meta


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: CGXCacheMetaTag
##--------------------------------------------------------------------------------------------
class CGXCacheMetaTag(r9Meta.MetaClass):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, *args,**kws):
        super(CGXCacheMetaTag, self).__init__(*args,**kws)
        self.lockState=True
    
    
    def __bindData__(self):
        '''
        bindData allows you to set default attrs on your node, any changes
        in this will be reflected everytime the node is instanciated
        '''
        self.addAttr('export', attrType='bool')
        self.addAttr('assetType', attrType='string')
        self.addAttr('assetName', attrType='string')
        self.addAttr('subassetName', attrType='string')
        self.addAttr('instanceIdentifier', attrType='string')
        self.export = True


##--------------------------------------------------------------------------------------------
##Class: CGXCacheMetaCtrl
##--------------------------------------------------------------------------------------------
class CGXCacheMetaCtrl(r9Meta.MetaClass):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, *args,**kws):
        super(CGXCacheMetaCtrl, self).__init__(*args,**kws)
        self.lockState=True
    
    
    def __bindData__(self):
        '''
        bindData allows you to set default attrs on your node, any changes
        in this will be reflected everytime the node is instanciated
        '''
        self.addAttr('mainControl', attrType='bool')
    
    
    def getCtrl(self):
        childDict = self.getChildren(asMap=True)
        control = ''
        for each in childDict.keys():
            if '.taggedNode' in each:
                control = childDict[each][0]
                break
        return control
    

##--------------------------------------------------------------------------------------------
##Class: CGXMetaPublishedFile
##--------------------------------------------------------------------------------------------
class CGXMetaPublishedFile(r9Meta.MetaClass):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, *args,**kws):
        super(CGXMetaPublishedFile, self).__init__(*args,**kws)
        self.lockState=True
    
    
    def __bindData__(self):
        '''
        bindData allows you to set default attrs on your node, any changes
        in this will be reflected everytime the node is instanciated
        '''
        self.addAttr('fileOrigin', attrType='string')
        self.addAttr('author', attrType='string')
        self.addAttr('time', attrType='string')
    

##--------------------------------------------------------------------------------------------
##Class: CGXDressingMetaAsset
##--------------------------------------------------------------------------------------------
class CGXDressingMetaAsset(r9Meta.MetaClass):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, *args,**kws):
        super(CGXDressingMetaAsset, self).__init__(*args,**kws)
        self.lockState=True
    
    
    def __bindData__(self):
        '''
        bindData allows you to set default attrs on your node, any changes
        in this will be reflected everytime the node is instanciated
        '''
        self.addAttr('assetType', attrType='string')
        self.addAttr('assetName', attrType='string')
        self.addAttr('subassetName', attrType='string')
        self.addAttr('animation', attrType='bool')
        
        
    def getCtrl(self):
        childDict = self.getChildren(asMap=True)
        control = ''
        for each in childDict.keys():
            if '.control' in each:
                control = childDict[each][0]
                break
        return control
        
    
    def gatherInfo(self, level=0, *args, **kws):
        '''
        a generic gather function designed to be overloaded at the class level and used to 
        collect specific information on the given class in a generic way. This is used by the
        r9Aninm format in Pro to collect key info on the system being saved against
        
        :param level: added here for the more robust checking that the rigging systems need
        '''
        data={}
        data['mNode'] = self.mNode
        data['mNodeID'] = self.mNodeID
        data['mClass'] = self.mClass
        data['mClassGrp'] = self.mClassGrp
        data['mSystemRoot']= self.mSystemRoot
        data['lockState'] = self.lockState
        data['assetType'] = self.assetType
        data['assetName'] = self.assetName
        data['subassetName'] = self.subassetName
        return data
    
    @r9Meta.nodeLockManager
    def batchSetInfo(self, data):
        self.assetType = data['assetType']
        self.assetName = data['assetName']
        self.subassetName = data['subassetName']
        

##--------------------------------------------------------------------------------------------
##Class: CGXDressingMetaVariant
##--------------------------------------------------------------------------------------------
class CGXDressingMetaVariant(r9Meta.MetaClass):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, *args,**kws):
        super(CGXDressingMetaVariant, self).__init__(*args,**kws)
        self.lockState=True
    
    
    def __bindData__(self):
        '''
        bindData allows you to set default attrs on your node, any changes
        in this will be reflected everytime the node is instanciated
        '''
        self.addAttr('LOD', attrType='enum', value=1, enumName='HIGH:MID:LOW')
        self.addAttr('LOD_HIGH', attrType='message')
        self.addAttr('LOD_HIGH_file', attrType='string')
        self.addAttr('LOD_MID', attrType='message')
        self.addAttr('LOD_MID_file', attrType='string')
        self.addAttr('LOD_LOW', attrType='message')
        self.addAttr('LOD_LOW_file', attrType='string')
        self.addAttr('shaders_file', attrType='string')
    
    
    def getMetaGeo(self, _level):
        allMetaNodes = self.getChildMetaNodes()
        if _level == 'LOD_HIGH':
            childDict = self.getChildren(asMap=True)
            keyName = self.mNode + '.LOD_HIGH'
            if keyName in childDict.keys():
                mayaNodes = childDict[keyName]
                metaNodesList = []
                for each in allMetaNodes:
                    for mayaNode in mayaNodes:
                        if each.mNode == mayaNode:
                            metaNodesList.append(each)
                            break
                return metaNodesList
            else:
                return []
        elif _level == 'LOD_MID':
            childDict = self.getChildren(asMap=True)
            keyName = self.mNode + '.LOD_MID'
            if keyName in childDict.keys():
                mayaNodes = childDict[keyName]
                metaNodesList = []
                for each in allMetaNodes:
                    for mayaNode in mayaNodes:
                        if each.mNode == mayaNode:
                            metaNodesList.append(each)
                            break
                return metaNodesList
            else:
                return []
        elif _level == 'LOD_LOW':
            childDict = self.getChildren(asMap=True)
            keyName = self.mNode + '.LOD_LOW'
            if keyName in childDict.keys():
                mayaNodes = childDict[keyName]
                metaNodesList = []
                for each in allMetaNodes:
                    for mayaNode in mayaNodes:
                        if each.mNode == mayaNode:
                            metaNodesList.append(each)
                            break
                return metaNodesList
            else:
                return []
    
    
    def gatherInfo(self, level=0, *args, **kws):
        '''
        a generic gather function designed to be overloaded at the class level and used to 
        collect specific information on the given class in a generic way. This is used by the
        r9Aninm format in Pro to collect key info on the system being saved against
        
        :param level: added here for the more robust checking that the rigging systems need
        '''
        data={}
        data['mNode'] = self.mNode
        data['mNodeID'] = self.mNodeID
        data['mClass'] = self.mClass
        data['mClassGrp'] = self.mClassGrp
        data['mSystemRoot']= self.mSystemRoot
        data['lockState'] = self.lockState
        data['LOD'] = self.LOD
        data['LOD_HIGH_file'] = self.LOD_HIGH_file
        data['LOD_MID_file'] = self.LOD_MID_file
        data['LOD_LOW_file'] = self.LOD_LOW_file
        return data
    
    @r9Meta.nodeLockManager
    def batchSetInfo(self, data):
        self.LOD = data['LOD']
        self.LOD_HIGH_file = data['LOD_HIGH_file']
        self.LOD_MID_file = data['LOD_MID_file']
        self.LOD_LOW_file = data['LOD_LOW_file']



##--------------------------------------------------------------------------------------------
##Class: CGXDressingMetaGeo
##--------------------------------------------------------------------------------------------
class CGXDressingMetaGeo(r9Meta.MetaClass):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, *args,**kws):
        super(CGXDressingMetaGeo, self).__init__(*args,**kws)
        self.lockState=True
    
    
    def __bindData__(self):
        '''
        bindData allows you to set default attrs on your node, any changes
        in this will be reflected everytime the node is instanciated
        '''
        self.addAttr('geoName', attrType='string')
        self.addAttr('shader', attrType='string')
        self.addAttr('aiSubdivType', attrType='int')
        self.addAttr('aiSubdivIterations', attrType='int')
        self.addAttr('aiToggleSubdivType', attrType='int')
        self.addAttr('aiToggleSubdivIterations', attrType='int')
        self.addAttr('aiDispHeight', attrType='float')
        self.addAttr('aiDispPadding', attrType='float')
        self.addAttr('aiDispZeroValue', attrType='float')
        self.addAttr('aiOpaque', attrType='bool')
        
    
    def getGeo(self):
        childDict = self.getChildren(asMap=True)
        geo = ''
        for each in childDict.keys():
            if '.taggedNode' in each:
                geo = childDict[each][0]
                break
        return geo
    
    
    def gatherInfo(self, level=0, *args, **kws):
        '''
        a generic gather function designed to be overloaded at the class level and used to 
        collect specific information on the given class in a generic way. This is used by the
        r9Aninm format in Pro to collect key info on the system being saved against
        
        :param level: added here for the more robust checking that the rigging systems need
        '''
        data={}
        data['mNode'] = self.mNode
        data['mNodeID'] = self.mNodeID
        data['mClass'] = self.mClass
        data['mClassGrp'] = self.mClassGrp
        data['mSystemRoot']= self.mSystemRoot
        data['lockState'] = self.lockState
        data['geoName'] = self.geoName
        data['shader'] = self.shader
        data['aiSubdivType'] = self.aiSubdivType
        data['aiSubdivIterations'] = self.aiSubdivIterations
        return data
    
    @r9Meta.nodeLockManager
    def batchSetInfo(self, data):
        self.geoName = data['geoName']
        self.shader =data['shader']
        self.aiSubdivType = data['aiSubdivType']
        self.aiSubdivIterations = data['aiSubdivIterations']
        self.aiToggleSubdivType = data['aiToggleSubdivType']
        self.aiToggleSubdivIterations = data['aiToggleSubdivIterations']
        

##--------------------------------------------------------------------------------------------
##Class: CGXDressingShaderMeta
##--------------------------------------------------------------------------------------------
class CGXDressingShaderMeta(r9Meta.MetaClass):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, *args,**kws):
        super(CGXCacheMetaTag, self).__init__(*args,**kws)
        self.lockState=True
    
    
    def __bindData__(self):
        '''
        bindData allows you to set default attrs on your node, any changes
        in this will be reflected everytime the node is instanciated
        '''
        self.addAttr('geos', attrType='message')
    

    '''
    Para el caso de dressing cada geometria tiene que ser un instance de una clase de meta.
    Lo mismo para cada shader, tiene que ser su propia instancia de meta y estar conectados al nodo
    de la geo por message.
    Todo eso a su vez tiene que ser parte de una MetaClase root.
    Se exporta en un archivo la red de metadata, en otro los shaders y en otro la geo en Alembic.
    Al momento de importar con la tool de dressing se importa la red de metadata y con eso se reconstruyen
    las asignaciones de los shaders.
    En realidad cada shader deberia ser un archivo. Asi si se necesita hacer un update de alguno de los
    shaders, se reemplaza ese archivo y se le pide a la tool que haga el update.
    Entonces es importante que al momento de importar en dressing tambien quede abierta la opcion de hacer
    update.
    Lo que implica que en la metadata debe quedar almacenada tambien la ruta de donde viene el shader
    asi el update se hace sobre esa base.
    '''

##--------------------------------------------------------------------------------------------
##Main
##--------------------------------------------------------------------------------------------
#========================================================================
# This HAS to be at the END of this module so that the RED9_META_REGISTRY
# picks up all inherited subclasses contained within. 
# When this module is imported all subclasses will be added to the registry. 
# I have to do this as outside of a controlled production environment 
# I can't guarantee the Maya / Python boot sequence.
#======================================================================== 
r9Meta.registerMClassInheritanceMapping()


def main():
    pass


if __name__=="__main__":
    main()