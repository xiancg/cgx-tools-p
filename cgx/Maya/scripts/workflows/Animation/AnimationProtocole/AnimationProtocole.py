# -*- coding: utf-8 -*-
'''
Animation protocole to prepare animation files for the rest of the pipeline.

Created on Sep 09, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/

'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
from cgx.gui.Qt import QtWidgets, QtCore
from cgx.gui.Qt import __binding__
import maya.cmds as mc
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.DataViewModels as dvm
import cgx.Maya.scripts.maya_libs.Maya_Dependencies as MayaDeps
from cgx.core.JSONManager import JSONManager
import cgx.Maya.scripts.maya_libs.Maya_TimeRange as MayaTime
import Red9.core.Red9_Meta as r9Meta
import cgx.Maya.scripts.maya_libs.Maya_MetaData as cgxMeta


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/workflows/Animation/AnimationProtocole'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"
config_json = appRootFolder + "/cfg/" + "AnimationProtocole_config.json"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.initComponents()
			self.initUI()
			self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
        Connect signals.
        '''
		#BUTTONS
		self.apply_BTN.clicked.connect(self.applyProtocole)
		self.mainCtrl_BTN.clicked.connect(self.setMainCtrl)
		#REFRESH
		self.rig_COMBOBOX.currentIndexChanged.connect(self.refreshRig)
		self.tPoseWorld_CHKBOX.stateChanged.connect(lambda: self.refreshKeyframes('tPoseWorld'))
		self.tPoseLocal_CHKBOX.stateChanged.connect(lambda: self.refreshKeyframes('tPoseLocal'))
		self.startPoseLocal_CHKBOX.stateChanged.connect(lambda: self.refreshKeyframes('startPoseLocal'))
		self.handleIn_CHKBOX.stateChanged.connect(lambda: self.refreshKeyframes('handleIn'))
		self.handleOut_CHKBOX.stateChanged.connect(lambda: self.refreshKeyframes('handleOut'))
		#CONTEXT MENUS
		self.attrs_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.attrs_LISTVIEW.customContextMenuRequested.connect(self.attributesOptions)
		
	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Animation Protocole"
		self.__toolVersion = __version__
		
		self.configFile = JSONManager(config_json, self)
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		self.populateReferences()
		self.initCtrlsModel()
		self.findCtrls()
		
		#Set attributes model
		model = ConfigObjectsListModel([], self)
		self.attrs_LISTVIEW.setModel(model)
		
		#Set handle out
		mTRange = MayaTime.Maya_TimeRange()
		self.handleOut_SPINBOX.setValue(mTRange.getTimeSlider()[1]+2)
		
	
	def populateReferences(self):
		deps = MayaDeps.Maya_Dependencies()
		nodesList = []
		if len(deps.referenceFiles) >= 1:
			nodesList = [x['node'] for x in deps.referenceFiles ]
			nodesSet = set(nodesList)
			nodesList = list(nodesSet)
		rigsModel = dvm.ObjectsListModel(nodesList, self.rig_COMBOBOX)
		self.rig_COMBOBOX.setModel(rigsModel)
		
	
	def refreshRig(self):
		if self.rig_COMBOBOX.model().dataList != []:
			mc.select(self.rig_COMBOBOX.currentText())
			self.findCtrls()
	
	
	def refreshKeyframes(self, _chkbox):
		if _chkbox == 'tPoseWorld':
			if self.tPoseWorld_CHKBOX.isChecked():
				self.tPoseWorld_SPINBOX.setEnabled(True)
			else:
				self.tPoseWorld_SPINBOX.setEnabled(False)
		elif _chkbox == 'tPoseLocal':
			if self.tPoseLocal_CHKBOX.isChecked():
				self.tPoseLocal_SPINBOX.setEnabled(True)
			else:
				self.tPoseLocal_SPINBOX.setEnabled(False)
		elif _chkbox == 'startPoseLocal':
			if self.startPoseLocal_CHKBOX.isChecked():
				self.startPoseLocal_SPINBOX.setEnabled(True)
			else:
				self.startPoseLocal_SPINBOX.setEnabled(False)
		elif _chkbox == 'handleIn':
			if self.handleIn_CHKBOX.isChecked():
				self.handleIn_SPINBOX.setEnabled(True)
			else:
				self.handleIn_SPINBOX.setEnabled(False)
		elif _chkbox == 'handleOut':
			if self.handleOut_CHKBOX.isChecked():
				self.handleOut_SPINBOX.setEnabled(True)
			else:
				self.handleOut_SPINBOX.setEnabled(False)
	
	
	def findCtrls(self):
		metaCtrls = r9Meta.getMetaNodes(['CGXCacheMetaCtrl'])
		thisNamespace= mc.referenceQuery(self.rig_COMBOBOX.currentText(), namespace=True, shortName=True)
		namespaceMembers = mc.namespaceInfo(thisNamespace, listNamespace=True, recurse=True, dagPath=True)
		if len(metaCtrls) >= 1:
			for metaCtrl in metaCtrls:
				longName = metaCtrl.getCtrl()
				if longName in namespaceMembers:
					shortName = longName
					if "|" in shortName:
						shortName = longName.rsplit("|",1)[1]
					if metaCtrl.mainControl:
						self.mainCtrl_LINEEDIT.setText(shortName)
					else:
						self.ctrls_TABLEVIEW.model().insertRows(self.ctrls_TABLEVIEW.model().rowCount(), 1, [shortName,metaCtrl])
		else:
			self.baseMod.statusBar_update("The scene doesn't have any tagged controls.", alert=True)
	
	
	def initCtrlsModel(self):
		#Set ctrls model
		dataList = []
		ctrlsHeaders = ['ctrl','metaNode']
		ctrlsModel = dvm.DataTableModel(dataList, ctrlsHeaders, self)
		self.ctrls_TABLEVIEW.setModel(ctrlsModel)
		self.ctrls_TABLEVIEW.hideColumn(1)
		htalHeader = self.ctrls_TABLEVIEW.horizontalHeader()
		htalHeader.setStretchLastSection(True)
		for col in range(ctrlsModel.columnCount()):
			self.ctrls_TABLEVIEW.resizeColumnToContents(col)
		self.ctrls_TABLEVIEW.setSelectionMode(self.ctrls_TABLEVIEW.MultiSelection)
		self.ctrls_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
		self.ctrls_TABLEVIEW.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
	
	
	def setMainCtrl(self):
		selShape = mc.listRelatives(mc.ls(sl=True, dag=True, long= True)[0], shapes=True, noIntermediate=True)[0]
		if mc.nodeType(selShape) == 'nurbsCurve':
			transform = mc.listRelatives(selShape, parent=True, fullPath=True)[0]
			self.mainCtrl_LINEEDIT.setText(transform)
	
	
	def listSkipAttrs(self):
		attrsList = []
		if self.skipIKFK_CHKBOX.isChecked():
			attrsList += self.configFile.jsonObj['ikfkAttrs']
		if self.skipSpaceSwitches_CHKBOX.isChecked():
			attrsList += self.configFile.jsonObj['spaceSwitchAttrs']
		if self.skipAttrs_CHKBOX.isChecked():
			attrsList += self.attrs_LISTVIEW.model().dataList
		
		return attrsList
	
	
	def applyProtocole(self):
		skipAttrs = self.listSkipAttrs()

		mainControlCheck = mc.objExists(self.mainCtrl_LINEEDIT.text())
		if not mainControlCheck:
			self.baseMod.statusBar_update(self.mainCtrl_LINEEDIT.text() + " doesn't exist in your scene.", warning=True)
		else:
			mainCtrl = self.mainCtrl_LINEEDIT.text()
			#Filter controls
			curvesOnly= []
			for each in mc.ls(sl=True, transforms=True):
				shape= mc.listRelatives(each, shapes=True, noIntermediate=True, fullPath=True)[0]
				if mc.nodeType(shape) == 'nurbsCurve':
					curvesOnly.append(each)
			ctrlsList = []
			for row in self.ctrls_TABLEVIEW.model().dataList:
				ctrlsList.append(row[0])
			if self.add_RBTN.isChecked():
				for each in curvesOnly:
					if each not in ctrlsList:
						ctrlsList.append(each)
			elif self.skip_RBTN.isChecked():
				for each in sorted(curvesOnly, reverse=True):
					if each in ctrlsList and each not in mainCtrl:
						ctrlsList.remove(each)
			
			#Set keys on all controls
			for control in ctrlsList:
				ctrlAttrs = mc.listAttr(control, keyable=True, visible=True, unlocked=True, multi=False)
				if ctrlAttrs != None:
					for attr in ctrlAttrs:
						if attr not in skipAttrs:
							ctrlAttr= "{}.{}".format(control, attr)
							mainCtrlBool = False
							if control == mainCtrl:
								mainCtrlBool = True
							startFrameValue = mc.getAttr(ctrlAttr, time= self.handleIn_SPINBOX.value()+2)
							if type(startFrameValue) is list or type(startFrameValue) is tuple:
								continue
							if self.handleIn_CHKBOX.isChecked():
								secondFrameValue = mc.getAttr(ctrlAttr, time= self.handleIn_SPINBOX.value()+3)
								change = 2*(max(secondFrameValue,startFrameValue) - min(secondFrameValue,startFrameValue))
								slope = (secondFrameValue-startFrameValue)/((self.handleIn_SPINBOX.value()+3)-(self.handleIn_SPINBOX.value()+2))
								keyValue = startFrameValue-change
								if slope <= 0:
									keyValue = startFrameValue+change
								mc.setKeyframe(ctrlAttr, value=keyValue, time= self.handleIn_SPINBOX.value(), inTangentType='linear', outTangentType='linear', breakdown=False)
							if self.handleOut_CHKBOX.isChecked():
								endFrameValue = mc.getAttr(ctrlAttr, time= self.handleOut_SPINBOX.value()-2)
								penultFrameValue = mc.getAttr(ctrlAttr, time= self.handleOut_SPINBOX.value()-3)
								change = 2*(max(endFrameValue, penultFrameValue) - min(endFrameValue, penultFrameValue))
								slope = (endFrameValue-penultFrameValue)/((self.handleOut_SPINBOX.value()-2)-(self.handleOut_SPINBOX.value()-3))
								keyValue = endFrameValue-change
								if slope > 0:
									keyValue = endFrameValue+change
								mc.setKeyframe(ctrlAttr, value=keyValue, time= self.handleOut_SPINBOX.value(), inTangentType='linear', outTangentType='linear', breakdown=False)
							if self.startPoseLocal_CHKBOX.isChecked():
								handleInValue = mc.getAttr(ctrlAttr, time= self.handleIn_SPINBOX.value())
								mc.setKeyframe(ctrlAttr, value=handleInValue, time= self.startPoseLocal_SPINBOX.value(), inTangentType='linear', outTangentType='linear', breakdown=False)
							if self.tPoseLocal_CHKBOX.isChecked():
								if mainCtrlBool:
									poseLocalValue = mc.getAttr(ctrlAttr, time= self.startPoseLocal_SPINBOX.value())
									mc.setKeyframe(ctrlAttr, value=poseLocalValue, time= self.tPoseLocal_SPINBOX.value(), inTangentType='linear', outTangentType='linear', breakdown=False)
								else:
									metaCtrl = r9Meta.getConnectedMetaNodes(control, source=True, destination=False, mTypes='CGXCacheMetaCtrl')[0]
									if metaCtrl.hasAttr(attr):
										defaultValue = getattr(metaCtrl, attr)
									else:
										defaultValue = mc.attributeQuery(attr, listDefault=True, node= control)[0]
										if 'scale' in attr or 'Scale' in attr:
											if defaultValue < 1:
												defaultValue = 1
									mc.setKeyframe(ctrlAttr, value=defaultValue, time= self.tPoseLocal_SPINBOX.value(), inTangentType='linear', outTangentType='linear', breakdown=False)
							if self.tPoseWorld_CHKBOX.isChecked():
								metaCtrl = r9Meta.getConnectedMetaNodes(control, source=True, destination=False, mTypes='CGXCacheMetaCtrl')[0]
								defaultValue = 0
								if metaCtrl.hasAttr(attr):
									defaultValue = getattr(metaCtrl, attr)
								else:
									defaultValue = mc.attributeQuery(attr, listDefault=True, node= control)[0]
									if 'scale' in attr or 'Scale' in attr:
										if defaultValue < 1:
											defaultValue = 1
								mc.setKeyframe(ctrlAttr, value=defaultValue, time= self.tPoseWorld_SPINBOX.value(), inTangentType='linear', outTangentType='linear', breakdown=False)
			
			self.baseMod.statusBar_update("Animation protocole applied successfully to {}.".format(self.rig_COMBOBOX.currentText()), success=True)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for attributes list view
	##--------------------------------------------------------------------------------------------     
	def attributesOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.attrs_LISTVIEW)
		addTypeQ = menu.addAction("Add attributes")
		deleteTypeQ = menu.addAction("Delete attributes")
		menu.popup(self.attrs_LISTVIEW.mapToGlobal(pos))
		
		addTypeQ.triggered.connect(self.addAttr)
		deleteTypeQ.triggered.connect(self.deleteAttr)
	
	
	def addAttr(self):
		self.attrs_LISTVIEW.model().insertRows(self.attrs_LISTVIEW.model().rowCount(), ["newAttribute"])
	
	
	def deleteAttr(self):
		selectedIndex = self.attrs_LISTVIEW.currentIndex().row()
		self.attrs_LISTVIEW.model().removeRows([selectedIndex], 1)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for ctrls table view
	##--------------------------------------------------------------------------------------------     
	def ctrlsOptions (self,pos):
		'''
        Right-click popup menu for ctrls table.
        '''
		menu = QtWidgets.QMenu(self.ctrls_TABLEVIEW)
		addControlsQ = menu.addAction("Add controls")
		removeControlsQ = menu.addAction("Remove controls")
		selectControlsQ = menu.addAction("Select controls")
		selectMetaNodesQ = menu.addAction("Select Meta Nodes")
		menu.popup(self.ctrls_TABLEVIEW.mapToGlobal(pos))
		
		addControlsQ.triggered.connect(self.addControlsOpt)
		removeControlsQ.triggered.connect(self.removeControlsOpt)
		selectControlsQ.triggered.connect(self.selectControlsOpt)
		selectMetaNodesQ.triggered.connect(self.selectMetaControlsOpt)
	
	
	def addControlsOpt(self):
		allCurveShapes= mc.ls(type='nurbsCurve', long=True)
		allCurveTransforms = [mc.listRelatives(x, parent=True)[0] for x in allCurveShapes]
		setTransforms = set(allCurveTransforms)
		controlsList = list(setTransforms)
		for ctrl in controlsList:
			messageConnection = mc.listConnections(ctrl, source=True, destination=False, type='network')
			if messageConnection != None:
				if mc.nodeType(messageConnection[0]) != 'network':
					metaCtrl = cgxMeta.CGXCacheMetaCtrl(name=ctrl + '_META')
					metaCtrl.connectChildren([ctrl], 'taggedNode')
					self.ctrl_TABLEVIEW.model().insertRows(self.ctrls_TABLEVIEW.model().rowCount(), 1, [ctrl, metaCtrl])
				else:
					self.baseMod.statusBar_update(ctrl + " already tagged.", alert=True)
			else:
				self.baseMod.statusBar_update(ctrl + " already tagged.", alert=True)
	
	
	def removeControlsOpt(self):
		selected = self.ctrls_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selected:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		
		for each in selRows:
			metaNode = self.ctrls_TABLEVIEW.model().dataList[each][1]
			metaNode.delete()
		self.ctrls_TABLEVIEW.model().removeRows(selRows, len(selRows))
	
	
	def selectControlsOpt(self):
		selected = self.ctrls_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selected:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		transform = self.ctrls_TABLEVIEW.model().dataList[selRows[0]][0]
		mc.select(transform, replace=True)
	
	
	def selectMetaControlsOpt(self):
		selected = self.ctrls_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selected:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		metaNode = self.ctrls_TABLEVIEW.model().dataList[selRows[0]][1]
		mc.select(metaNode.mNode, replace=True)
			

##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		
		self.configFile = JSONManager(config_json, self)
		
		self.initUI()
		self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		#BUTTONS
		self.cancel_BTN.clicked.connect(self.cancelAll)
		self.apply_BTN.clicked.connect(self.saveConfig)
		#CONTEXT MENUS
		self.ctrlWildcards_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.ctrlWildcards_LISTVIEW.customContextMenuRequested.connect(self.wildcardsOptions)
		self.ikfkSwitchesAttrs_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.ikfkSwitchesAttrs_LISTVIEW.customContextMenuRequested.connect(self.ikfkOptions)
		self.spaceSwitches_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.spaceSwitches_LISTVIEW.customContextMenuRequested.connect(self.spaceSwitchesOptions)
		self.ctrlSetSuffixes_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.ctrlSetSuffixes_LISTVIEW.customContextMenuRequested.connect(self.ctrlsSetOptions)
		self.mainCtrl_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.mainCtrl_LISTVIEW.customContextMenuRequested.connect(self.mainCtrlOptions)
	
	
	def initUI(self):
		self.populateWildcards()
		self.populateIkFkAttrs()
		self.populateSpaceSwitches()
		self.populateMainCtrl()
		self.populateCtrlsSet()
	
	
	def populateWildcards(self):
		data = self.configFile.jsonObj['controlWildcards']
		model = ConfigObjectsListModel(data, self.ctrlWildcards_LISTVIEW)
		self.ctrlWildcards_LISTVIEW.setModel(model)
	
	
	def populateIkFkAttrs(self):
		data = self.configFile.jsonObj['ikfkAttrs']
		model = ConfigObjectsListModel(data, self.ikfkSwitchesAttrs_LISTVIEW)
		self.ikfkSwitchesAttrs_LISTVIEW.setModel(model)
	
	
	def populateSpaceSwitches(self):
		data = self.configFile.jsonObj['spaceSwitchAttrs']
		model = ConfigObjectsListModel(data, self.spaceSwitches_LISTVIEW)
		self.spaceSwitches_LISTVIEW.setModel(model)
	
	
	def populateMainCtrl(self):
		data = self.configFile.jsonObj['mainControl']
		model = ConfigObjectsListModel(data, self.mainCtrl_LISTVIEW)
		self.mainCtrl_LISTVIEW.setModel(model)
	
	
	def populateCtrlsSet(self):
		data = self.configFile.jsonObj['controlsSetSuffixes']
		model = ConfigObjectsListModel(data, self.ctrlSetSuffixes_LISTVIEW)
		self.ctrlSetSuffixes_LISTVIEW.setModel(model)
	
	
	def saveConfig(self):
		"""Apply changes to JSON file"""
		jsonObj = self.configFile.jsonObj
		self.configFile.jsonObj['controlWildcards'] = self.ctrlWildcards_LISTVIEW.model().dataList
		self.configFile.jsonObj['ikfkAttrs'] = self.ikfkSwitchesAttrs_LISTVIEW.model().dataList
		self.configFile.jsonObj['spaceSwitchAttrs'] = self.spaceSwitches_LISTVIEW.model().dataList
		self.configFile.jsonObj['controlsSetSuffixes'] = self.ctrlSetSuffixes_LISTVIEW.model().dataList
		self.configFile.jsonObj['mainControl'] = self.mainCtrl_LISTVIEW.model().dataList
		self.configFile.writeJson(jsonObj, self.configFile.fileName)
		
		self.done(1)
	
	
	def cancelAll(self):
		"""Method to close the UI"""
		
		self.done(0)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for wildcards
	##--------------------------------------------------------------------------------------------
	def wildcardsOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.ctrlWildcards_LISTVIEW)
		addTypeQ = menu.addAction("Add wildcard")
		deleteTypeQ = menu.addAction("Delete wildcard")
		menu.popup(self.ctrlWildcards_LISTVIEW.mapToGlobal(pos))
		
		addTypeQ.triggered.connect(self.addWildcard)
		deleteTypeQ.triggered.connect(self.deleteWildcard)
	
	
	def addWildcard(self):
		self.ctrlWildcards_LISTVIEW.model().insertRows(self.ctrlWildcards_LISTVIEW.model().rowCount(), ["newWildcard"])
	
	
	def deleteWildcard(self):
		selectedIndex = self.ctrlWildcards_LISTVIEW.currentIndex().row()
		if len(self.ctrlWildcards_LISTVIEW.model().dataList) == 1:
			msgBox = QtWidgets.QMessageBox(self)
			msgBox.setWindowTitle("Warning!")
			msgBox.setText("There has to be at least one wildcard in the list.")
			msgBox.exec_()
		else:
			self.ctrlWildcards_LISTVIEW.model().removeRows([selectedIndex], 1)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for space switches
	##--------------------------------------------------------------------------------------------
	def spaceSwitchesOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.spaceSwitches_LISTVIEW)
		addTypeQ = menu.addAction("Add space switch")
		deleteTypeQ = menu.addAction("Delete space switch")
		menu.popup(self.spaceSwitches_LISTVIEW.mapToGlobal(pos))
		
		addTypeQ.triggered.connect(self.addSpaceSwitch)
		deleteTypeQ.triggered.connect(self.deleteSpaceSwitch)
	
	
	def addSpaceSwitch(self):
		self.spaceSwitches_LISTVIEW.model().insertRows(self.spaceSwitches_LISTVIEW.model().rowCount(), ["newSpaceSwitch"])
	
	
	def deleteSpaceSwitch(self):
		selectedIndex = self.spaceSwitches_LISTVIEW.currentIndex().row()
		if len(self.spaceSwitches_LISTVIEW.model().dataList) == 1:
			msgBox = QtWidgets.QMessageBox(self)
			msgBox.setWindowTitle("Warning!")
			msgBox.setText("There has to be at least one space switch option in the list.")
			msgBox.exec_()
		else:
			self.spaceSwitches_LISTVIEW.model().removeRows([selectedIndex], 1)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for IK/FK switches
	##--------------------------------------------------------------------------------------------
	def ikfkOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.ikfkSwitchesAttrs_LISTVIEW)
		addTypeQ = menu.addAction("Add Ik/Fk switch")
		deleteTypeQ = menu.addAction("Delete Ik/Fk switch")
		menu.popup(self.ikfkSwitchesAttrs_LISTVIEW.mapToGlobal(pos))
		
		addTypeQ.triggered.connect(self.addIkFkSwitch)
		deleteTypeQ.triggered.connect(self.deleteIkFkSwitch)
	
	
	def addIkFkSwitch(self):
		self.ikfkSwitchesAttrs_LISTVIEW.model().insertRows(self.ikfkSwitchesAttrs_LISTVIEW.model().rowCount(), ["newIkFkSwitch"])
	
	
	def deleteIkFkSwitch(self):
		selectedIndex = self.ikfkSwitchesAttrs_LISTVIEW.currentIndex().row()
		if len(self.ikfkSwitchesAttrs_LISTVIEW.model().dataList) == 1:
			msgBox = QtWidgets.QMessageBox(self)
			msgBox.setWindowTitle("Warning!")
			msgBox.setText("There has to be at least one ik/fk switch option in the list.")
			msgBox.exec_()
		else:
			self.ikfkSwitchesAttrs_LISTVIEW.model().removeRows([selectedIndex], 1)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for main control
	##--------------------------------------------------------------------------------------------
	def mainCtrlOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.mainCtrl_LISTVIEW)
		addTypeQ = menu.addAction("Add main control")
		deleteTypeQ = menu.addAction("Delete main control")
		menu.popup(self.mainCtrl_LISTVIEW.mapToGlobal(pos))
		
		addTypeQ.triggered.connect(self.addMainCtrl)
		deleteTypeQ.triggered.connect(self.deleteMainCtrl)
	
	
	def addMainCtrl(self):
		self.mainCtrl_LISTVIEW.model().insertRows(self.mainCtrl_LISTVIEW.model().rowCount(), ["newMainCtrl"])
	
	
	def deleteMainCtrl(self):
		selectedIndex = self.mainCtrl_LISTVIEW.currentIndex().row()
		if len(self.mainCtrl_LISTVIEW.model().dataList) == 1:
			msgBox = QtWidgets.QMessageBox(self)
			msgBox.setWindowTitle("Warning!")
			msgBox.setText("There has to be at least one main control option in the list.")
			msgBox.exec_()
		else:
			self.mainCtrl_LISTVIEW.model().removeRows([selectedIndex], 1)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for controls set
	##--------------------------------------------------------------------------------------------
	def ctrlsSetOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.ctrlSetSuffixes_LISTVIEW)
		addTypeQ = menu.addAction("Add controls set")
		deleteTypeQ = menu.addAction("Delete controls set")
		menu.popup(self.ctrlSetSuffixes_LISTVIEW.mapToGlobal(pos))
		
		addTypeQ.triggered.connect(self.addCtrlsSet)
		deleteTypeQ.triggered.connect(self.deleteCtrlsSet)
	
	
	def addCtrlsSet(self):
		self.ctrlSetSuffixes_LISTVIEW.model().insertRows(self.ctrlSetSuffixes_LISTVIEW.model().rowCount(), ["newControlsSet"])
	
	
	def deleteCtrlsSet(self):
		selectedIndex = self.ctrlSetSuffixes_LISTVIEW.currentIndex().row()
		if len(self.ctrlSetSuffixes_LISTVIEW.model().dataList) == 1:
			msgBox = QtWidgets.QMessageBox(self)
			msgBox.setWindowTitle("Warning!")
			msgBox.setText("There has to be at least one controls set option in the list.")
			msgBox.exec_()
		else:
			self.ctrlSetSuffixes_LISTVIEW.model().removeRows([selectedIndex], 1)


##--------------------------------------------------------------------------------------------
##Class: Objects lists model
##--------------------------------------------------------------------------------------------
class ConfigObjectsListModel(dvm.ObjectsListModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, thisList, parent):
		super(ConfigObjectsListModel, self).__init__(thisList, parent)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def flags(self, index):
		"""Set flags of each cell"""
		return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
		

##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/workflows/Animation/AnimationProtocole_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()