# -*- coding: utf-8 -*-
'''
Given the current layout sequence, split and export files for animation

Created on Sep 15, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/

'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
from cgx.gui.Qt import QtWidgets, QtCore
import maya.cmds as mc
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.DataViewModels as dvm
import cgx.Maya.scripts.maya_libs.Maya_Dependencies as MayaDeps


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/workflows/Layout/ShotFiles'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.initComponents()
			self.initUI()
			self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
        Connect signals.
        '''
		#BUTTONS
		self.cutFiles_BTN.clicked.connect(self.cutFiles)
		#REFRESH
		self.useCamSequencer_CHKBOX.stateChanged.connect(self.refreshUseCamSequencer)
		self.shots_TABLEVIEW.clicked.connect(self.populateReferences)
		self.shots_TABLEVIEW.clicked.connect(self.populateImports)
		#CONTEXT MENUS
		self.shots_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.shots_TABLEVIEW.customContextMenuRequested.connect(self.shotsOptions)
		self.references_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.references_LISTVIEW.customContextMenuRequested.connect(self.referencesOptions)
		self.imports_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.imports_LISTVIEW.customContextMenuRequested.connect(self.importsOptions)
		
	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Shot Files"
		self.__toolVersion = __version__
		
		self.referenceModels = []
		self.importsModels = []
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.baseMod.MainMenu_Component.edit_config.setVisible(False)
		
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		self.populateShots()
		self.initReferenceModels()
		self.initImportsModels()
		self.populateReferences()
		self.populateImports()
	
	
	def cutFiles(self):
		pass
	
	
	def populateShots(self):
		headers = ['Shot Name', 'Source In', 'Source Out', 'Target In', 'Target Out', 'Camera', 'Node']
		dataList = []
		if self.useCamSequencer_CHKBOX.isChecked():
			for shot in mc.ls(type='shot'):
				rowList = []
				for column in headers:
					if column == 'Shot Name':
						rowList.append(mc.getAttr(shot + '.shotName'))
					elif column == 'Source In':
						rowList.append( int( mc.shot(shot, startTime=True, query=True) ) )
					elif column == 'Source Out':
						rowList.append( int( mc.shot(shot, endTime=True, query=True) ) )
					elif column == 'Target In':
						rowList.append(int(1001))
					elif column == 'Target Out':
						rowList.append( int(1001 + ( mc.shot(shot, endTime=True, query=True) - mc.shot(shot, startTime=True, query=True) ) ) )
					elif column == 'Camera':
						rowList.append( mc.shot(shot, currentCamera=True, query=True) )
					elif column == 'Node':
						rowList.append(shot)
				dataList.append(rowList)
		else:
			rowList = []
			for column in headers:
				if column == 'Shot Name':
					rowList.append('SEQ_##_s_###')
				elif column == 'Source In':
					rowList.append( 1001 )
				elif column == 'Source Out':
					rowList.append( 1100 )
				elif column == 'Target In':
					rowList.append(int(1001))
				elif column == 'Target Out':
					rowList.append( 1100 )
				elif column == 'Camera':
					rowList.append( 'None' )
				elif column == 'Node':
					rowList.append('None')
			dataList.append(rowList)
		
		model = ShotsDataTableModel(dataList, headers, self.shots_TABLEVIEW)
		self.shots_TABLEVIEW.setModel(model)
		self.shots_TABLEVIEW.setColumnWidth(1,65)
		self.shots_TABLEVIEW.setColumnWidth(2,65)
		self.shots_TABLEVIEW.setColumnWidth(3,65)
		self.shots_TABLEVIEW.setColumnWidth(4,65)
		self.shots_TABLEVIEW.setColumnWidth(5,130)
		self.shots_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
		self.shots_TABLEVIEW.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
		delegate = ShotsDelegate(self)
		self.shots_TABLEVIEW.setItemDelegate(delegate)
		self.shots_TABLEVIEW.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
		if self.shots_TABLEVIEW.model().rowCount >= 1:
			self.shots_TABLEVIEW.setCurrentIndex(self.shots_TABLEVIEW.model().index(0,0))
		
	
	def populateReferences(self):
		selectedShot = self.shots_TABLEVIEW.currentIndex().row()
		if len(self.referenceModels) >= 1:
			thisModel = self.referenceModels[selectedShot]
			self.references_LISTVIEW.setModel(thisModel)
		self.references_LISTVIEW.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
	
	
	def populateImports(self):
		selectedShot = self.shots_TABLEVIEW.currentIndex().row()
		if len(self.importsModels) >= 1:
			thisModel = self.importsModels[selectedShot]
			self.imports_LISTVIEW.setModel(thisModel)
		self.imports_LISTVIEW.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
	
	
	def referencesNodesList(self):
		deps = MayaDeps.Maya_Dependencies()
		nodesList = []
		if len(deps.referenceFiles) >= 1:
			nodesList = [x['node'] for x in deps.referenceFiles ]
			nodesSet = set(nodesList)
			nodesList = list(nodesSet)
			
			for each in sorted(nodesList, reverse=True):
				if 'RIG_Camera' in each:
					del nodesList[nodesList.index(each)]
			
		return nodesList
	
	
	def initReferenceModels(self):
		if len(self.referenceModels) >= 1:
			for each in self.referenceModels:
				del self.referenceModels[self.referenceModels.index(each)]
		shotsList = self.shots_TABLEVIEW.model().dataList
		i = 0
		for shot in shotsList:
			newModel = dvm.ObjectsListModel(self.referencesNodesList(), self)
			self.referenceModels.append(newModel)
			i += 1
	
	
	def initImportsModels(self):
		if len(self.importsModels) >= 1:
			for each in self.importsModels:
				del self.importsModels[self.importsModels.index(each)]
		shotsList = self.shots_TABLEVIEW.model().dataList
		i = 0
		for shot in shotsList:
			newModel = dvm.ObjectsListModel([], self)
			self.importsModels.append(newModel)
			i += 1
	
	
	def refreshUseCamSequencer(self):
		self.populateShots()
		self.initReferenceModels()
		self.populateReferences()
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for shots table
	##--------------------------------------------------------------------------------------------
	def shotsOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.shots_TABLEVIEW)
		if not self.useCamSequencer_CHKBOX.isChecked():
			addShotQ = menu.addAction("Add Shot")
			deleteShotQ = menu.addAction("Delete Shot")
			addShotQ.triggered.connect(self.addShotOpt)
			deleteShotQ.triggered.connect(self.deleteShotOpt)

			menu.popup(self.shots_TABLEVIEW.mapToGlobal(pos))
		
		
	def addShotOpt(self):
		self.shots_TABLEVIEW.model().insertRows(self.shots_TABLEVIEW.model().rowCount(), 1, ['SEQ_##_s_###', 1001, 1100, 1001, 1100, 'None', 'None'])
		newModel = dvm.ObjectsListModel(self.referencesNodesList(), self)
		self.referenceModels.append(newModel)
		self.populateReferences()
		
	
	def deleteShotOpt(self):
		selected = self.shots_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selected:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		self.shots_TABLEVIEW.model().removeRows(selRows, len(selRows))
		for each in selRows:
			self.referenceModels.pop(each)
		self.populateReferences()
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for references
	##--------------------------------------------------------------------------------------------
	def referencesOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.references_LISTVIEW)
		addReferenceMenu = menu.addMenu('Add reference to shot')
		refActions = []
		for each in self.referencesNodesList():
			newAction = addReferenceMenu.addAction(each)
			refActions.append(newAction)
		removeReferenceQ = menu.addAction('Remove reference from shot')
		selectReferenceContentsQ = menu.addAction('Select reference contents')
		menu.popup(self.references_LISTVIEW.mapToGlobal(pos))
		
		for each in refActions:
			each.triggered.connect(self.addReferenceOpt)
		removeReferenceQ.triggered.connect(self.removeReferenceOpt)
		selectReferenceContentsQ.triggered.connect(self.selectReferenceContentsOpt)
	
	
	def addReferenceOpt(self):
		refName = self.sender().text()
		if refName not in self.references_LISTVIEW.model().dataList:
			self.references_LISTVIEW.model().insertRows(self.references_LISTVIEW.model().rowCount(), [refName])
		else:
			self.baseMod.statusBar_update('{} reference already added to shot.'.format(refName), alert=True)
	
	
	def removeReferenceOpt(self):
		selectedIndexes = self.references_LISTVIEW.selectedIndexes()
		self.references_LISTVIEW.model().removeRows(selectedIndexes, len(selectedIndexes))
		
	
	def selectReferenceContentsOpt(self):
		selected = self.references_LISTVIEW.selectedIndexes()
		if len(selected) <= 0:
			self.baseMod.statusBar_update("Select at least one item from the list.", alert=True)
		else:
			refNodesList = []
			for each in selected:
				refNodesList.append(self.references_LISTVIEW.model().data(each))
			allNodes = []
			for each in refNodesList:
				allNodes += mc.referenceQuery(each, nodes=True, dagPath=True)
			mc.select(allNodes, r=True)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for imports
	##--------------------------------------------------------------------------------------------
	def importsOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.imports_LISTVIEW)
		addImportedNodesQ = menu.addAction('Add imported nodes')
		removeImportedNodesQ = menu.addAction('Remove imported nodes')
		selectNodesQ = menu.addAction('Select nodes')
		menu.popup(self.imports_LISTVIEW.mapToGlobal(pos))
		
		addImportedNodesQ.triggered.connect(self.addImportedNodesOpt)
		removeImportedNodesQ.triggered.connect(self.removeImportedNodesOpt)
		selectNodesQ.triggered.connect(self.selectNodesOpt)
		
	
	def addImportedNodesOpt(self):
		allSel = mc.ls(sl=True)
		notReferenced = []
		referencedFound = False
		for each in allSel:
			if not mc.referenceQuery(each, isNodeReferenced=True):
				notReferenced.append(each)
			else:
				referencedFound = True
				
		self.imports_LISTVIEW.model().insertRows(self.imports_LISTVIEW.model().rowCount(), notReferenced)
		if referencedFound:
			self.baseMod.statusBar_update("Referenced nodes in your selection were ignored.", alert=True)
	
	
	def removeImportedNodesOpt(self):
		selectedIndexes = self.imports_LISTVIEW.selectedIndexes()
		self.imports_LISTVIEW.model().removeRows(selectedIndexes, len(selectedIndexes))
	
	
	def selectNodesOpt(self):
		selected = self.imports_LISTVIEW.selectedIndexes()
		if len(selected) <= 0:
			self.baseMod.statusBar_update("Select at least one item from the list.", alert=True)
		else:
			nodesList = []
			for each in selected:
				nodesList.append(self.imports_LISTVIEW.model().data(each))
			mc.select(nodesList, r=True)


##--------------------------------------------------------------------------------------------
##Class: Objects lists model
##--------------------------------------------------------------------------------------------
class EditableObjectsListModel(dvm.ObjectsListModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, thisList, parent):
		super(EditableObjectsListModel, self).__init__(thisList, parent)
		

	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def flags(self, index):
		"""Set flags of each cell"""
		return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable


##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for shots table
##--------------------------------------------------------------------------------------------
class ShotsDataTableModel(dvm.DataTableModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, dataList, headers, parent):
		super(ShotsDataTableModel, self).__init__(dataList, headers, parent)
		self.parent = parent
			
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def flags(self, index):
		"""Set flags of each cell"""
		text = index.model().data(index, QtCore.Qt.DisplayRole)
		if index.model().headers[index.column()] == 'Node' and text == "None":
			return QtCore.Qt.NoItemFlags
		else:
			return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

					
##--------------------------------------------------------------------------------------------
##Class: Shots Delegate
##--------------------------------------------------------------------------------------------
class ShotsDelegate(QtWidgets.QStyledItemDelegate):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent=None):
		super(ShotsDelegate, self).__init__(parent)
		self.parent = parent

	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def paint(self, painter, option, index):
		QtWidgets.QStyledItemDelegate.paint(self, painter, option, index)
	
	
	def sizeHint(self, option, index):
		return QtWidgets.QStyledItemDelegate.sizeHint(self, option, index)
		
		
	def createEditor(self, parent, option, index):
		modelHeaders = index.model().headers
		if modelHeaders[index.column()] in ['Camera','camera']:
			combobox = QtWidgets.QComboBox(parent)
			camsList = mc.ls(type='camera')
			camsTransforms = [ mc.listRelatives(x, parent=True)[0] for x in camsList ]
			camsTransforms.insert(0,'None')
			combobox.addItems(camsTransforms)
			combobox.setEditable(False)
			combobox.currentIndexChanged.connect(lambda: self.commitEditorData(combobox))
			return combobox
		elif modelHeaders[index.column()] in ['Source In','Source Out','Target In','Target Out']:
			spinbox = QtWidgets.QSpinBox(parent)
			spinbox.setRange(-10000, 10000)
			value = index.model().dataList[index.row()][index.column()]
			spinbox.setValue(value)
			spinbox.valueChanged.connect(lambda: self.commitEditorData(spinbox))
			return spinbox
		else:
			return QtWidgets.QStyledItemDelegate.createEditor(self, parent, option, index)
		
		
	def setEditorData(self, editor, index):
		modelHeaders = index.model().headers
		text = index.model().data(index, QtCore.Qt.DisplayRole)
		if modelHeaders[index.column()] in ['Camera','camera']:
			i = editor.findText(text)
			if i == -1:
				i = 0
			editor.setCurrentIndex(i)
		else:
			QtWidgets.QStyledItemDelegate.setEditorData(self, editor, index)
	
	
	def setModelData(self, editor, model, index):
		QtWidgets.QStyledItemDelegate.setModelData(self, editor, model, index)
	
	
	def commitEditorData(self, editor):
		self.commitData.emit(editor)
		self.closeEditor.emit(editor,self.NoHint)
						

##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		

##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/workflows/Layout/ShotFiles_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()