# -*- coding: utf-8 -*-
'''
Lights Manager for Arnold and VRay.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
Pending:
-Implement mesh lights (can't access the state of the arnold translator attribute)
-Illuminate by default (arnold y common)
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets, QtCore, QtGui
from cgx.gui.Qt import __binding__
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
from cgx.core.JSONManager import JSONManager
import cgx.gui.DataViewModels as dvm
import cgx.Maya.scripts.workflows.Lighting.LightsManager.LightsManager_icons
from cgx.core.Synchronizer import Synchronizer


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/workflows/Lighting/LightsManager'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"
lightAttrs_ui = appRootFolder + "/ui/" + "lightAttrs.ui"
addLightProperties_ui = appRootFolder + "/ui/" + "addLightProperties.ui"
createLight_ui = appRootFolder + "/ui/" + "createLight.ui"
config_json = appRootFolder + "/cfg/" + "LightsManager_config.json"
lightAttrs_json = appRootFolder + "/cfg/" + "LightsManager_lightAttrs.json"
minitools_json = appRootFolder + "/cfg/" + "LightsManager_minitools.json"
import cgx.Maya.scripts.workflows.Lighting.MiniTools as mnTls
mnTlsFolder = os.path.dirname(os.path.abspath(mnTls.__file__))
minitools_config_ui = mnTlsFolder + "/ui/" + "config.ui"
minitools_config_json = mnTlsFolder + "/cfg/" + "MiniTools_config.json"

headerView_cfg = appRootFolder + "/cfg/" + "LightsManager_userPrefs.ini"
userPath = os.path.expanduser("~")
finalPath = userPath + "/.CGXTools/LightsManager_userPrefs.ini"
if os.path.exists(finalPath):
	headerView_cfg = finalPath
else:
	sync = Synchronizer()
	sync.processPaths(headerView_cfg, finalPath)
	headerView_cfg = finalPath

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)
lightAttrsform, lightAttrsbase = uiParser.loadUiType(lightAttrs_ui)
addLightPropertiesform, addLightPropertiesbase = uiParser.loadUiType(addLightProperties_ui)
createLightform, createLightbase = uiParser.loadUiType(createLight_ui)
minitools_configform, minitools_configbase = uiParser.loadUiType(minitools_config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.initComponents()
			self.initUI()
			self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
        Connect signals.
        '''
		#Edit Menu
		self.action_lightAttrs.triggered.connect(self.lightAttrsDialog)
		self.action_saveAttrsOrder.triggered.connect(self.saveAttrsOrder)
		self.action_conformSelected.triggered.connect(self.conformSelected)
		self.action_minitools_config.triggered.connect(self.miniToolsConfigDialog)
		#Main UI
		self.visibleOnly_CHKBOX.clicked.connect(self.applyFilters)
		self.type_COMBOBOX.currentIndexChanged.connect(self.applyFilters)
		self.function_COMBOBOX.currentIndexChanged.connect(self.applyFilters)
		self.renderer_COMBOBOX.currentIndexChanged.connect(self.populateLightTypes)
		self.renderer_COMBOBOX.currentIndexChanged.connect(self.populateLightList)
		self.lightSearch_LINEEDIT.returnPressed.connect(self.applyFilters)
		self.lightSearch_BTN.clicked.connect(self.applyFilters)
		self.manager_TABLEVIEW.doubleClicked.connect(lambda: self.triggerAllSignals("doubleClicked"))
		self.manager_TABLEVIEW.clicked.connect(lambda: self.triggerAllSignals("clicked"))
		self.manager_TABLEVIEW.activated.connect(lambda: self.triggerAllSignals("activated"))
		self.manager_TABLEVIEW.released.connect(lambda: self.triggerAllSignals("released"))
		self.manager_TABLEVIEW.installEventFilter(self)
		self.resetManager_BTN.clicked.connect(self.resetUI)
		#Create light buttons
		self.createAiAreaLight_BTN.clicked.connect(lambda: self.createLight("aiAreaLight"))
		self.createAiSkydome_BTN.clicked.connect(lambda: self.createLight("aiSkyDome"))
		self.createAiMeshLight_BTN.clicked.connect(lambda: self.createLight("aiMeshLight"))
		self.createAiPhotometricLight_BTN.clicked.connect(lambda: self.createLight("aiPhotometricLight"))
		self.createSpotLight_BTN.clicked.connect(lambda: self.createLight("spotLight"))
		self.createDirectionalLight_BTN.clicked.connect(lambda: self.createLight("directionalLight"))
		self.createPointLight_BTN.clicked.connect(lambda: self.createLight("pointLight"))
		self.createAreaLight_BTN.clicked.connect(lambda: self.createLight("areaLight"))
		self.createVRayRectLight_BTN.clicked.connect(lambda: self.createLight("VRayRectLight"))
		self.createVRaySphereLight_BTN.clicked.connect(lambda: self.createLight("VRaySphereLight"))
		self.createVRayDomeLight_BTN.clicked.connect(lambda: self.createLight("VRayDomeLight"))
		self.createVRayIESLight_BTN.clicked.connect(lambda: self.createLight("VRayIESLight"))
		#Tools buttons
		self.toogleLights_BTN.clicked.connect(self.toggleIsolateOpt)
		self.lookThru_BTN.clicked.connect(self.lookThruOpt)
		self.saveImgs_BTN.clicked.connect(self.appTools.saveRenderViewImgs)
		self.cleanUpCams_BTN.clicked.connect(self.appTools.cleanUpCams)
		self.renderSubdivs_BTN.clicked.connect(self.appTools.restoreArnoldSubdivs)
		self.toogleSubdivs_BTN.clicked.connect(self.appTools.toggleArnoldSubdivs)
		#ICONS
		self.lightSearch_BTN.setIcon(QtGui.QIcon(":/LightsManager_lightSearch.png"))
		self.lightSearch_BTN.setIconSize(QtCore.QSize(15,15))
		self.createAiAreaLight_BTN.setIcon(QtGui.QIcon(":/LightsManager_aiAreaLight.png"))
		self.createAiAreaLight_BTN.setIconSize(QtCore.QSize(18,18))
		self.createAiSkydome_BTN.setIcon(QtGui.QIcon(":/LightsManager_aiSkydome.png"))
		self.createAiSkydome_BTN.setIconSize(QtCore.QSize(18,18))
		self.createAiMeshLight_BTN.setIcon(QtGui.QIcon(":/LightsManager_aiMeshLight.png"))
		self.createAiMeshLight_BTN.setIconSize(QtCore.QSize(18,18))
		self.createAiPhotometricLight_BTN.setIcon(QtGui.QIcon(":/LightsManager_aiPhotometricLight.png"))
		self.createAiPhotometricLight_BTN.setIconSize(QtCore.QSize(18,18))
		self.createSpotLight_BTN.setIcon(QtGui.QIcon(":/LightsManager_spotLight.png"))
		self.createSpotLight_BTN.setIconSize(QtCore.QSize(18,18))
		self.createDirectionalLight_BTN.setIcon(QtGui.QIcon(":/LightsManager_directionalLight.png"))
		self.createDirectionalLight_BTN.setIconSize(QtCore.QSize(18,18))
		self.createPointLight_BTN.setIcon(QtGui.QIcon(":/LightsManager_pointLight.png"))
		self.createPointLight_BTN.setIconSize(QtCore.QSize(18,18))
		self.createAreaLight_BTN.setIcon(QtGui.QIcon(":/LightsManager_areaLight.png"))
		self.createAreaLight_BTN.setIconSize(QtCore.QSize(18,18))
		self.createVRayRectLight_BTN.setIcon(QtGui.QIcon(":/LightsManager_VRayRectLight.png"))
		self.createVRayRectLight_BTN.setIconSize(QtCore.QSize(18,18))
		self.createVRaySphereLight_BTN.setIcon(QtGui.QIcon(":/LightsManager_VRaySphereLight.png"))
		self.createVRaySphereLight_BTN.setIconSize(QtCore.QSize(18,18))
		self.createVRayDomeLight_BTN.setIcon(QtGui.QIcon(":/LightsManager_VRayDomeLight.png"))
		self.createVRayDomeLight_BTN.setIconSize(QtCore.QSize(18,18))
		self.createVRayIESLight_BTN.setIcon(QtGui.QIcon(":/LightsManager_VRayIESLight.png"))
		self.createVRayIESLight_BTN.setIconSize(QtCore.QSize(18,18))
		self.toogleLights_BTN.setIcon(QtGui.QIcon(":toggleIsolateLights.png"))
		self.toogleLights_BTN.setIconSize(QtCore.QSize(28,28))
		self.lookThru_BTN.setIcon(QtGui.QIcon(":/lookThru.png"))
		self.lookThru_BTN.setIconSize(QtCore.QSize(28,28))
		self.saveImgs_BTN.setIcon(QtGui.QIcon(":/saveRVImages.png"))
		self.saveImgs_BTN.setIconSize(QtCore.QSize(28,28))
		self.cleanUpCams_BTN.setIcon(QtGui.QIcon(":/cleanUpCams.png"))
		self.cleanUpCams_BTN.setIconSize(QtCore.QSize(28,28))
		self.renderSubdivs_BTN.setIcon(QtGui.QIcon(":/renderSubdiv.png"))
		self.renderSubdivs_BTN.setIconSize(QtCore.QSize(28,28))
		self.toogleSubdivs_BTN.setIcon(QtGui.QIcon(":/toggleSubdiv.png"))
		self.toogleSubdivs_BTN.setIconSize(QtCore.QSize(28,28))
		#CONTEXT MENUS
		self.manager_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.manager_TABLEVIEW.customContextMenuRequested.connect(self.managerOptions)
		
	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
		self.appTools.initTools()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		self.appTools.initTools()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Lights Manager"
		self.__toolVersion = "2.0"
		
		#Objects
		
		self.lightColorBuffer = "#FFFFFF"
		self.colorDialog = QtWidgets.QColorDialog(self)
		self.configFile = JSONManager(config_json, self)
		self.lightAttrsFile = JSONManager(lightAttrs_json, self)
		self.lightVis = {}
		#List all render layers and lights with visibility off
		lightNodes = self.appTools.lightNodes()
		allLightsShapes = mc.ls(lights= True, type= lightNodes, long= True)
		allLightsScene = []
		for i in allLightsShapes:
			iTrans = mc.listRelatives(i, parent= True, fullPath= True)
			allLightsScene.append(iTrans[0])
		renderLyrs = mc.ls(type= "renderLayer")
		for lyr in renderLyrs:
			lyrMembers = mc.editRenderLayerMembers(lyr, query=True , fullNames= True)
			lightsOff = []
			if lyrMembers != None:
				for light in allLightsScene:
					shapeNode = mc.listRelatives(light, shapes= True, noIntermediate= True, fullPath= True)
					if light in lyrMembers or shapeNode[0] in lyrMembers:
						if mc.getAttr(light + ".visibility") == False:
							lightsOff.append(light)
			self.lightVis[lyr] = lightsOff
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		className= str(self.__class__.__name__)
		#Add Import Maya Layers from current file menu option
		self.action_lightAttrs = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.action_lightAttrs.setObjectName("action_lightAttrs")
		self.action_saveAttrsOrder = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.action_saveAttrsOrder.setObjectName("action_saveAttrsOrder")
		self.action_conformSelected = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.action_conformSelected.setObjectName("action_conformSelected")
		self.action_minitools_config = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.action_minitools_config.setObjectName("action_miniTools_config")
		self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[0], self.action_saveAttrsOrder)
		self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[0], self.action_conformSelected)
		self.baseMod.MainMenu_Component.menuEdit.insertSeparator(self.baseMod.MainMenu_Component.edit_reset)
		self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[4], self.action_minitools_config)
		self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[5], self.action_lightAttrs)
		if __binding__ in ('PySide', 'PyQt4'):
			self.action_lightAttrs.setText(QtWidgets.QApplication.translate(className, "Edit Nodes and Attributes", None, QtWidgets.QApplication.UnicodeUTF8))
			self.action_saveAttrsOrder.setText(QtWidgets.QApplication.translate(className, "Save attributes order", None, QtWidgets.QApplication.UnicodeUTF8))
			self.action_conformSelected.setText(QtWidgets.QApplication.translate(className, "Conform selected to naming conventions", None, QtWidgets.QApplication.UnicodeUTF8))
			self.action_minitools_config.setText(QtWidgets.QApplication.translate(className, "MiniTools Configuration", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			self.action_lightAttrs.setText(QtWidgets.QApplication.translate(className, "Edit Nodes and Attributes", None))
			self.action_saveAttrsOrder.setText(QtWidgets.QApplication.translate(className, "Save attributes order", None))
			self.action_conformSelected.setText(QtWidgets.QApplication.translate(className, "Conform selected to naming conventions", None))
			self.action_minitools_config.setText(QtWidgets.QApplication.translate(className, "MiniTools Configuration", None))
		
		#Reimplemented table
		self.manager_TABLEVIEW = ManagerTableView(self)
		self.manager_TABLEVIEW.setGeometry(QtCore.QRect(10, 135, 981, 521))
		self.manager_TABLEVIEW.setObjectName("manager_TABLEVIEW")
		if __binding__ in ('PySide', 'PyQt4'):
			self.manager_TABLEVIEW.horizontalHeader().setMovable(True)
		elif __binding__ in ('PySide2', 'PyQt5'):
			self.manager_TABLEVIEW.horizontalHeader().setSectionsMovable(True)
		self.manager_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)

	
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
		self.appTools.baseMod = self.baseMod
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		self.populateRenderers()
		self.populateFunctions()
		self.populateLightTypes()
		self.populateLightList()
		#Script jobs
		self.sjm = ScriptJobManager()
		self.sjm.createJob(e=['SelectionChanged', lambda: self.updateLightSelection("None")])
		self.sjm.createJob(e=['renderLayerChange', lambda: self.renderLayerChange()])
		self.sjm.createJob(e=['DagObjectCreated', lambda: self.lightCreated()])
	
	
	def checkPlugins(self):
		'''
		Check if plugins are loaded.
		:return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
		:rtype: dict
		'''
		warningDict= {}
		for plugin in self.lightAttrsFile.jsonObj.keys():
			if plugin != "common":
				result = mc.pluginInfo(plugin, query=True, loaded=True)
				if not result:
					warningDict[plugin] = plugin + " not loaded."
		
		return warningDict
	
	
	def populateRenderers(self):
		checkPlugins = self.checkPlugins()
		#Populate renderers
		renderers= self.lightAttrsFile.jsonObj.keys()
		renderers.pop(renderers.index("common"))
		for plugin in renderers:
			if plugin in checkPlugins.keys():
				renderers.pop(renderers.index(plugin))
		rendererModel = dvm.ObjectsListModel(renderers)
		self.renderer_COMBOBOX.setModel(rendererModel)
	
	
	def populateFunctions(self):
		lightFunctions= [each for each in self.configFile.jsonObj['lightFunctions']]
		lightFunctions += ["All"]
		lightFunctions = sorted(lightFunctions)
		lightFunctionsModel = dvm.ObjectsListModel(lightFunctions)
		self.function_COMBOBOX.setModel(lightFunctionsModel)
	
	
	def populateLightTypes(self):
		lightTypes = [each for each in self.lightAttrsFile.jsonObj['common']['lightNodes']['nodes']]
		currentEngine= self.renderer_COMBOBOX.currentText()
		for each in self.renderer_COMBOBOX.model().dataList:
			if currentEngine == each:
				lightTypes += self.lightAttrsFile.jsonObj[each]['lightNodes']['nodes']
				break
		lightTypes += ["All"]
		lightTypes = sorted(lightTypes)
		lightTypesModel = dvm.ObjectsListModel(lightTypes)
		self.type_COMBOBOX.setModel(lightTypesModel)
	
	
	def populateLightList(self):
		defaultHeaders = ["Light Name","fullPath","fullPathShape"]
		allHeaders = defaultHeaders + self.getAttrsList()
		theseLightTypes = []
		for each in self.type_COMBOBOX.model().dataList:
			if each != "All":
				theseLightTypes.append(each)
			
		dataList = []
		#Build datalist
		lightsList = mc.ls(type= theseLightTypes, long=True)
		for row in lightsList:
			rowList = []
			for column in allHeaders:
				if column == "Light Name":
					rowList.append(mc.listRelatives(row, parent=True)[0])
				elif column == "fullPath":
					rowList.append(mc.listRelatives(row, parent=True, fullPath= True)[0])
				elif column == "fullPathShape":
					rowList.append(row)
				elif column == "illuminatesByDefault":
					thisParent= mc.listRelatives(row, parent=True)[0]
					connection = mc.listConnections(thisParent + ".instObjGroups[0]", d=True, s=False, plugs=True)
					if connection != None:
						rowList.append("True")
					else:
						rowList.append("False")
				else:
					theseAttrs = mc.listAttr(row, string = column)
					if theseAttrs != None:
						attrDict = self.getAttr(column)
						if attrDict["uiControl"] == "combobox" or attrDict["uiControl"] == "booleancombobox":
							rowList.append( attrDict["values"][mc.getAttr(row + "." + column)] )#Set correct value according to json light attrs file
						elif attrDict["uiControl"] == "colorswatch":
							lightColor = list(mc.getAttr(row + "." + column)[0])
							thisColor = QtGui.QColor.fromRgbF(lightColor[0],lightColor[1],lightColor[2])
							rowList.append( thisColor.name() )
						else:
							rowList.append(mc.getAttr(row + "." + column))
					else:
						rowList.append("None")
			dataList.append(rowList)
		#Set model
		self.lightListModel = ManagerDataTableModel(dataList, allHeaders, self)
		self.manager_TABLEVIEW.setModel(self.lightListModel)
		self.manager_TABLEVIEW.setColumnWidth(0,120)
		delegate = ManagerDelegate(self)
		self.manager_TABLEVIEW.setItemDelegate(delegate)
		self.lightListModel.dataChanged.connect(self.setMayaAttr)
		if __binding__ in ('PySide', 'PyQt4'):
			from PySide import QtGui as oldQtGui
			self.manager_selModel = oldQtGui.QItemSelectionModel(self.lightListModel)
		elif __binding__ in ('PySide2', 'PyQt5'):
			self.manager_selModel = QtCore.QItemSelectionModel(self.lightListModel)
		self.manager_TABLEVIEW.setSelectionModel(self.manager_selModel)
		self.manager_selModel.currentChanged.connect(self.updateLightSelection)
		#User prefs
		userPath = os.path.expanduser("~")
		finalDir = userPath + "/.CGXTools/LightsManager_userPrefs.ini"
		if os.path.exists(finalDir):
			settings= QtCore.QSettings(finalDir, QtCore.QSettings.IniFormat)
			self.manager_TABLEVIEW.horizontalHeader().restoreState(settings.value(self.renderer_COMBOBOX.currentText()))
		else:
			settings= QtCore.QSettings(headerView_cfg, QtCore.QSettings.IniFormat)
			self.manager_TABLEVIEW.horizontalHeader().restoreState(settings.value(self.renderer_COMBOBOX.currentText()))
		self.manager_TABLEVIEW.hideColumn(self.lightListModel.headers.index("fullPath"))
		self.manager_TABLEVIEW.hideColumn(self.lightListModel.headers.index("fullPathShape"))

		
		#Light search
		completer = QtWidgets.QCompleter(self)
		completer.setModel(self.lightListModel)
		completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
		completer.setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
		completer.setCompletionRole(QtCore.Qt.DisplayRole)
		self.lightSearch_LINEEDIT.setCompleter(completer)
		
		self.applyFilters()
	
	
	def applyFilters(self):
		'''Filters the light list according to user GUI input'''
		theseLightTypes = []
		for each in self.type_COMBOBOX.model().dataList:
			if each != "All":
				theseLightTypes.append(each)
		lightsList = mc.ls(type= theseLightTypes, long=True)
		
		showList = []
		for each in lightsList:
			showBool = True
			if not self.filterByFunction(each):
				showBool = False
			if not self.filterByType(each):
				showBool = False
			if not self.filterByVisibility(each):
				showBool = False
			if not self.filterByName(each):
				showBool = False
			
			if showBool:
				showList.append(each)
		i = 0
		for row in self.lightListModel.dataList:
			if row[self.lightListModel.headers.index("fullPathShape")] in showList:
				self.manager_TABLEVIEW.showRow(i)
			else:
				self.manager_TABLEVIEW.hideRow(i)
			i += 1
	
	
	def filterByName(self, _light):
		'''Filter lights by name'''
		if self.lightSearch_LINEEDIT.text() != '':
			transform = mc.listRelatives(_light, parent=True, fullPath=True)[0]
			if self.lightSearch_LINEEDIT.text() in transform:
				return True
			else:
				return False
		else:
			return True
	
	
	def filterByFunction(self, _light):
		'''Filter lights by function'''
		
		if self.function_COMBOBOX.currentText() != "All":
			transform = mc.listRelatives(_light, parent=True, fullPath=True)[0]
			indexFunction = transform.rfind("_")+1
			lightFunction = transform[indexFunction:]
			if lightFunction == self.function_COMBOBOX.currentText():
				return True
			else:
				return False
		else:
			return True
	
	
	def filterByType(self, _light):
		'''Filter lights by type'''
		if self.type_COMBOBOX.currentText() != "All":
			if mc.nodeType(_light) == self.type_COMBOBOX.currentText():
				return True
			else:
				return False
		else:
			return True
	
	
	def filterByVisibility(self, _light):
		'''Filter lights by visibility'''
		if self.visibleOnly_CHKBOX.isChecked():
			transform = mc.listRelatives(_light, parent=True, fullPath=True)[0]
			if mc.getAttr(_light + ".visibility") == True and mc.getAttr(transform + ".visibility") == True:
				return True
			else:
				return False
		else:
			return True
		
	
	def setMayaAttr(self, indexRow, indexCol):
		'''Method connected to the model dataChanged signal, to set the attrs in Maya'''
		
		modelHeaders = indexRow.model().headers
		dataList = indexRow.model().dataList
		attrName = modelHeaders[indexCol.column()]
		
		lightName = dataList[indexRow.row()][modelHeaders.index("Light Name")]
		if attrName in ["Light Name", "fullPath", "fullPathShape"]:
			if attrName == "Light Name":
				fullPath = dataList[indexRow.row()][modelHeaders.index("fullPath")]
				renameResult = mc.rename(fullPath, lightName)
				shapeNode = mc.listRelatives(renameResult, shapes= True, noIntermediate= True, fullPath= True)[0]
				parentNode = mc.listRelatives(shapeNode, parent= True, fullPath= True)[0]
				fullPathIndex = indexRow.model().index(indexRow.row(), modelHeaders.index("fullPath"))
				fullPathShapeIndex = indexRow.model().index(indexRow.row(), modelHeaders.index("fullPathShape"))
				indexRow.model().setData(fullPathIndex, parentNode)
				indexRow.model().setData(fullPathShapeIndex, shapeNode)
		else:
			lightShape = mc.listRelatives(lightName, shapes= True, noIntermediate= True, fullPath= True)[0]
			value = dataList[indexRow.row()][indexCol.column()]
			attrDict = self.getAttr(attrName)
			if self.attrsOverrides_CHKBOX.isChecked():
				if attrDict["uiControl"] == "booleancombobox" and attrName == "illuminatesByDefault":
					thisRL = mc.editRenderLayerGlobals(query=True, currentRenderLayer=True)
					connection = mc.listConnections(lightName + ".instObjGroups[0]", d=True, s=False, plugs=True)
					if connection == None:
						if thisRL != "defaultRenderLayer":
							mc.editRenderLayerAdjustment(lightName + ".instObjGroups", thisRL)
							mc.sets(lightName, edit=True, forceElement="defaultLightSet")
							#mc.connectAttr(lightName + ".instObjGroups", "defaultLightSet.dagSetMembers", nextAvailable= True)
						else:
							mc.sets(lightName, edit=True, forceElement="defaultLightSet")
							#mc.connectAttr(lightName + ".instObjGroups", "defaultLightSet.dagSetMembers", nextAvailable= True)
					else:
						if thisRL != "defaultRenderLayer":
							mc.editRenderLayerAdjustment(lightName + ".instObjGroups", thisRL)
							mc.sets(lightName, edit=True, remove="defaultLightSet")
							#mc.disconnectAttr(lightName + ".instObjGroups[0]", connection[0])
						else:
							mc.sets(lightName, edit=True, remove="defaultLightSet")
							#mc.disconnectAttr(lightName + ".instObjGroups[0]", connection[0])
				elif attrDict["uiControl"] == "floatslider" or attrDict["uiControl"] == "intslider":
					thisRL = mc.editRenderLayerGlobals(query=True, currentRenderLayer=True)
					if thisRL != "defaultRenderLayer":
						mc.editRenderLayerAdjustment(lightShape + "." + attrName, thisRL)
					mc.setAttr(lightShape + "." + attrName, value)
				elif attrDict["uiControl"] == "combobox" or attrDict["uiControl"] == "booleancombobox":
					valueIndex = attrDict["values"].index(value)
					thisRL = mc.editRenderLayerGlobals(query=True, currentRenderLayer=True)
					if thisRL != "defaultRenderLayer":
						mc.editRenderLayerAdjustment(lightShape + "." + attrName, thisRL)
					mc.setAttr(lightShape + "." + attrName, valueIndex)
				elif attrDict["uiControl"] == "colorswatch":
					thisColor = QtGui.QColor(value)
					thisRL = mc.editRenderLayerGlobals(query=True, currentRenderLayer=True)
					if thisRL != "defaultRenderLayer":
						mc.editRenderLayerAdjustment(lightShape + "." + attrName, thisRL)
					mc.setAttr(lightShape + "." + attrName, thisColor.redF(), thisColor.greenF(), thisColor.blueF(), type= "double3")
			else:
				if attrDict["uiControl"] == "booleancombobox" and attrName == "illuminatesByDefault":
					connection = mc.listConnections(lightName + ".instObjGroups[0]", d=True, s=False, plugs=True)
					if connection == None:
						mc.sets(lightName, edit=True, forceElement="defaultLightSet")
						#mc.connectAttr(lightName + ".instObjGroups", "defaultLightSet.dagSetMembers", nextAvailable= True, force=True)
					else:
						mc.sets(lightName, edit=True, remove="defaultLightSet")
						#mc.disconnectAttr(lightName + ".instObjGroups[0]", connection[0])
				elif attrDict["uiControl"] == "floatslider" or attrDict["uiControl"] == "intslider":
					mc.setAttr(lightShape + "." + attrName, value)
				elif attrDict["uiControl"] == "combobox" or attrDict["uiControl"] == "booleancombobox":
					valueIndex = attrDict["values"].index(value)
					mc.setAttr(lightShape + "." + attrName, valueIndex)
				elif attrDict["uiControl"] == "colorswatch":
					thisColor = QtGui.QColor(value)
					mc.setAttr(lightShape + "." + attrName, thisColor.redF(), thisColor.greenF(), thisColor.blueF(), type= "double3")
	
	
	def getAttrsList(self):
		attrsList = []
		commonDict = self.lightAttrsFile.jsonObj['common']['attributes']
		for each in sorted(commonDict.keys()):
			if commonDict[each]["display"] == "True":
				attrsList.append(each)
		engineDict = self.lightAttrsFile.jsonObj[self.renderer_COMBOBOX.currentText()]['attributes']
		for each in sorted(engineDict.keys()):
			if engineDict[each]["display"] == "True":
				attrsList.append(each)
		return attrsList
	
	
	def getAttr(self, _thisAttr):
		for each in self.lightAttrsFile.jsonObj.keys():
			attrsDict = self.lightAttrsFile.jsonObj[each]['attributes']
			if _thisAttr in attrsDict.keys():
				return attrsDict[_thisAttr]
	
	
	def triggerAllSignals(self, _signal):
		if _signal == "doubleClicked":
			self.editColorItem()
			self.updateLightSelection("lightsManager")
		elif _signal == "released":
			self.updateLightSelection("lightsManager")
		elif _signal == "clicked":
			self.updateLightSelection("lightsManager")
		elif _signal == "activated":
			self.updateLightSelection("lightsManager")
	
	
	def editColorItem(self):
		selected = self.manager_TABLEVIEW.selectedIndexes()
		current = self.manager_TABLEVIEW.currentIndex()
		dataList = current.model().dataList
		modelHeaders = current.model().headers
		columnName = modelHeaders[current.column()]
		if columnName not in ["Light Name","fullPath"]:
			attrDict = self.getAttr(columnName)
			if attrDict["uiControl"] == "colorswatch":
				currentValue = dataList[current.row()][current.column()]
				self.colorDialog.setCurrentColor(QtGui.QColor(currentValue))
				result = self.colorDialog.exec_()
				if result:
					for index in selected:
						index.model().setData(index, self.colorDialog.selectedColor().name())
	
	
	def updateLightSelection(self, _from):
		if _from == "Maya":
			allSel= mc.ls(sl=True,leaf=True,dag=True,long=True, lights=True)
			lightsDict = self.lightIndexAndFullName()
			indexes = self.manager_TABLEVIEW.selectedIndexes()
			selColsDirty = []
			for item in indexes:
				selColsDirty.append(item.column())
			selSet = set(selColsDirty)
			selCols = list(selSet)
			
			self.manager_selModel.clearSelection()
			if len(allSel) >= 1:
				for each in allSel:
					if each in lightsDict.keys():
						if len(selCols) >= 1:
							for column in selCols:
								self.manager_selModel.select(self.lightListModel.index(lightsDict[each], column), self.manager_selModel.Select)
						else:
							self.manager_selModel.select(self.lightListModel.index(lightsDict[each], 0), self.manager_selModel.Select)
		elif _from == "lightsManager":
			dataList= self.lightListModel.dataList
			selLightTypes = self.manager_TABLEVIEW.selectedIndexes()
			selRowsDirty = []
			for item in selLightTypes:
				thisRow = item.row()
				selRowsDirty.append(thisRow)
			selSet = set(selRowsDirty)
			selRows = list(selSet)
			preList = []
			for row in selRows:
				lightName = dataList[row][1]
				preList.append(lightName)
			finalList = []
			for each in preList:
				if mc.objExists(each):
					finalList.append(each)
			mc.select(finalList, replace=True)
	
	
	def lightIndexAndFullName(self):
		lightsDict = {}
		for row in range(len(self.lightListModel.dataList)):
			lightsDict[ self.lightListModel.dataList[row][self.lightListModel.headers.index("fullPathShape")] ] = row

		return lightsDict
	
	
	def lightCreated(self):
		allLights = mc.ls(lights=True)
		if len(allLights) != self.lightListModel.rowCount():
			self.resetUI()
			
	
	def renderLayerChange(self):
		self.resetUI()
	
	
	def eventFilter(self, widget, event):
		if (event.type() == QtCore.QEvent.KeyPress and widget is self.manager_TABLEVIEW):
			key = event.key()
			if key == QtCore.Qt.Key_Return or key == QtCore.Qt.Key_Enter:
				selected = self.manager_TABLEVIEW.selectedIndexes()
				current = self.manager_TABLEVIEW.currentIndex()
				modelHeaders = current.model().headers
				dataList = current.model().dataList
				columnName = modelHeaders[current.column()]
				if columnName not in ["Light Name","fullPath", "fullPathShape"]:
					attrDict = self.getAttr(columnName)
					if attrDict["uiControl"] == "colorswatch":
						currentValue = dataList[current.row()][current.column()]
						self.colorDialog.setCurrentColor(QtGui.QColor(currentValue))
						result = self.colorDialog.exec_()
						if result:
							for index in selected:
								index.model().setData(index, self.colorDialog.selectedColor().name())
							return True
			
		return QtWidgets.QWidget.eventFilter(self, widget, event)
	
	
	def createLight(self, _light):
		prefix = self.configFile.jsonObj['naming']['prefix']
		suffix = self.configFile.jsonObj['naming']['suffix']
		padding = self.configFile.jsonObj['naming']['padding']
		
		if _light == "aiMeshLight":
			import mtoa.ui.arnoldmenu as arnoldmenu
			arnoldmenu.doCreateMeshLight()
		else:
			self.createLightDialog = CreateLightGUI(self)
			result = self.createLightDialog.exec_()
			if result:
				assetName = self.createLightDialog.assetName_LINEEDIT.text()
				function = self.createLightDialog.lightFunction_COMBOBOX.currentText()
				checkResult = self.checkLightName(prefix, assetName, 1, padding, function, suffix)
				if _light == "aiAreaLight":
					import mtoa.utils as mutils
					lightNodes = mutils.createLocator("aiAreaLight", asLight=True)
					self.setDefaultAttrs(lightNodes[0])
					mc.rename(lightNodes[1], checkResult)
				elif _light == "aiSkyDome":
					import mtoa.utils as mutils
					lightNodes = mutils.createLocator("aiSkyDomeLight", asLight=True)
					self.setDefaultAttrs(lightNodes[0])
					mc.rename(lightNodes[1], checkResult)
				elif _light == "aiPhotometricLight":
					import mtoa.utils as mutils
					lightNodes = mutils.createLocator("aiPhotometricLight", asLight=True)
					self.setDefaultAttrs(lightNodes[0])
					mc.rename(lightNodes[1], checkResult)
				elif _light == "spotLight":
					shapeNode = mc.spotLight(name=checkResult)
					self.setDefaultAttrs(shapeNode)
				elif _light == "directionalLight":
					shapeNode = mc.directionalLight(name=checkResult)
					self.setDefaultAttrs(shapeNode)
				elif _light == "pointLight":
					shapeNode = mc.pointLight(name=checkResult)
					self.setDefaultAttrs(shapeNode)
				elif _light == "areaLight":
					shapeNode = mc.createNode("areaLight")
					transform = mc.listRelatives(shapeNode, parent=True)[0]
					self.setDefaultAttrs(shapeNode)
					mc.rename(transform, checkResult)
				elif _light == "VRayRectLight":
					shapeNode = mc.createNode("VRayLightRectShape")
					transform = mc.listRelatives(shapeNode, parent=True)[0]
					self.setDefaultAttrs(shapeNode)
					mc.rename(transform, checkResult)
				elif _light == "VRaySphereLight":
					shapeNode = mc.createNode("VRayLightSphereShape")
					transform = mc.listRelatives(shapeNode, parent=True)[0]
					self.setDefaultAttrs(shapeNode)
					mc.rename(transform, checkResult)
				elif _light == "VRayDomeLight":
					shapeNode = mc.createNode("VRayLightDomeShape")
					transform = mc.listRelatives(shapeNode, parent=True)[0]
					self.setDefaultAttrs(shapeNode)
					mc.rename(transform, checkResult)
				elif _light == "VRayIESLight":
					shapeNode = mc.createNode("VRayLightIESShape")
					transform = mc.listRelatives(shapeNode, parent=True)[0]
					self.setDefaultAttrs(shapeNode)
					mc.rename(transform, checkResult)
				
				self.resetUI()
	
	
	def checkLightName (self, _prefix, _assetName, _number, _padding, _function, _suffix):
		'''Recursive function to check if the light name is already in use and modify it accordingly'''
		
		numberStr = str(_number).zfill(_padding)
		lightName = _prefix + _assetName + "_" +  numberStr + "_" + _function + _suffix
		if not mc.objExists(lightName):
			return lightName
		else:
			lightName = self.checkLightName(_prefix, _assetName, _number + 1, _padding, _function, _suffix)
			return lightName
	
	
	def setDefaultAttrs(self, _thisLight):
		for renderer in self.lightAttrsFile.jsonObj.keys():
			for attrName, attrDict in self.lightAttrsFile.jsonObj[renderer]['attributes'].iteritems():
				value = attrDict["default"]
				if attrName in mc.listAttr(_thisLight):#Check if attribute exists in the object
					if attrDict["uiControl"] == "floatslider" or attrDict["uiControl"] == "intslider":
						mc.setAttr(_thisLight + "." + attrName, value)
					elif attrDict["uiControl"] == "combobox" or attrDict["uiControl"] == "booleancombobox":
						valueIndex = attrDict["values"].index(value)
						mc.setAttr(_thisLight + "." + attrName, valueIndex)
					elif attrDict["uiControl"] == "colorswatch":
						thisColor = QtGui.QColor(value)
						mc.setAttr(_thisLight + "." + attrName, thisColor.redF(), thisColor.greenF(), thisColor.blueF(), type= "double3")
	
	
	def saveAttrsOrder(self):
		userPath = os.path.expanduser("~")
		finalDir = userPath + "/.CGXTools"
		if not os.path.exists(finalDir):
			os.mkdir(finalDir)

		settings = QtCore.QSettings(finalDir + "/LightsManager_userPrefs.ini", QtCore.QSettings.IniFormat)
		currentRenderer = self.renderer_COMBOBOX.currentText()
		
		i = 0
		for renderer in self.renderer_COMBOBOX.model().dataList:
			self.renderer_COMBOBOX.setCurrentIndex(i)
			settings.setValue(renderer, self.manager_TABLEVIEW.horizontalHeader().saveState())
			i+=1
		
		#Back to normal
		modelIndex = 0
		try:
			modelIndex = self.renderer_COMBOBOX.model().dataList.index(currentRenderer)
		except ValueError:
			modelIndex = 0
		self.renderer_COMBOBOX.setCurrentIndex(modelIndex)
	
	
	def conformSelected(self):
		dataList= self.lightListModel.dataList
		prefix = self.configFile.jsonObj['naming']['prefix']
		suffix = self.configFile.jsonObj['naming']['suffix']
		padding = self.configFile.jsonObj['naming']['padding']
		
		selLightTypes = self.manager_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selLightTypes:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		for row in selRows:
			currentName = dataList[row][0]
			if currentName[:len(prefix)] != prefix:
				assetName = currentName
				function = "Custom"
				checkResult = self.checkLightName(prefix, assetName, 1, padding, function, suffix)
				mc.rename(currentName, checkResult)
		
		self.resetUI()
	
	
	def lightAttrsDialog(self):
		"""Method to launch the light attributes UI"""
		lightAttrsDialog = LightAttrs_DialogGUI(self)
		lightAttrsDialog.exec_()
		if lightAttrsDialog == 1:
			self.resetUI()
	
	
	def miniToolsConfigDialog(self):
		"""Method to launch the light attributes UI"""
		miniToolsCfgDialog = MiniToolsConfig_DialogGUI(self)
		miniToolsCfgDialog.exec_()
		if miniToolsCfgDialog == 1:
			self.resetUI()
	
	
	def resetUI(self):
		"""Method to reload everything in the UI"""
		self.configFile = JSONManager(config_json, self)
		self.lightAttrsFile = JSONManager(lightAttrs_json, self)

		self.populateLightList()
		self.appTools.initTools()
		self.updateLightSelection("Maya")
		
	
	def closeEvent(self, e):
		self.sjm.killJobs()
		super(Main_GUI, self).closeEvent(e)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for manager tableview
	##--------------------------------------------------------------------------------------------
	def managerOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.manager_TABLEVIEW)
		toggleIsolateQ = menu.addAction("Toggle Isolate Light")
		lookThruQ = menu.addAction("Look thru Light")
		duplicateLightQ = menu.addAction("Duplicate Light")
		copyColorQ = menu.addAction("Copy light color")
		pasteColorQ = menu.addAction("Paste light color")
		menu.popup(self.manager_TABLEVIEW.mapToGlobal(pos))
		
		toggleIsolateQ.triggered.connect(self.toggleIsolateOpt)
		lookThruQ.triggered.connect(self.lookThruOpt)
		duplicateLightQ.triggered.connect(self.duplicateLightOpt)
		copyColorQ.triggered.connect(self.copyLightColorOpt)
		pasteColorQ.triggered.connect(self.pasteLightColorOpt)
		
	
	def toggleIsolateOpt(self):
		#All light node types
		lightNodes = self.appTools.lightNodes()
		
		#All render layer lights
		allLightsShapes = mc.ls(type= lightNodes, long= True)
		allLightsScene = []
		for i in allLightsShapes:
			iTrans = mc.listRelatives(i, parent= True, fullPath=True)
			allLightsScene.append(iTrans[0])
		
		renderLayerLights = []
		currentLyr = mc.editRenderLayerGlobals( query=True, currentRenderLayer=True )
		lyrMembers = mc.editRenderLayerMembers(currentLyr, query=True, fullNames=True)
		if lyrMembers != None:
			for light in allLightsScene:
				shapeNode = mc.listRelatives(light, shapes= True, noIntermediate= True, fullPath= True)
				if light in lyrMembers or shapeNode[0] in lyrMembers:
					renderLayerLights.append(light)
		
		dataList= self.lightListModel.dataList
		fullPathIndex = self.lightListModel.headers.index("fullPath")
		selLightTypes = self.manager_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selLightTypes:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		finalList = []
		for row in selRows:
			lightName = dataList[row][fullPathIndex]
			finalList.append(lightName)
		
		if len(finalList) == 0:
			finalList = mc.ls(sl= True, long=True)
		
		#Selected scene lights
		selObjs = finalList
		selLights = []
		for e in selObjs:
			eShape = mc.listRelatives(e, shapes = True, noIntermediate = True, fullPath=True)
			eNodeType = mc.nodeType(eShape[0])
			if eNodeType in lightNodes:
				selLights.append(e)
		#Toggle visibilities
		lyr = mc.editRenderLayerGlobals(query=True, currentRenderLayer=True)
		for light in renderLayerLights:
			#If light is ON but not selected, turn it OFF
			if mc.getAttr(light + ".visibility") == True and light not in selLights:
				mc.setAttr((light + ".visibility"), False)
			#Else, if light is OFF and doesn't belong to OFF lights in the render layer, turn it ON
			elif mc.getAttr(light + ".visibility") == False and light not in self.lightVis[lyr]:
				mc.setAttr((light + ".visibility"), True)
	
	
	def lookThruOpt(self):
		lightNodes = self.appTools.lightNodes()
				
		dataList= self.lightListModel.dataList
		selLightTypes = self.manager_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selLightTypes:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		finalList = []
		for row in selRows:
			lightName = dataList[row][0]
			finalList.append(lightName)
		
		selLight= finalList
		if len(selLight) >= 1:
			for e in selLight:
				window = mc.window(width= 629, height= 404, title= e)
				mc.paneLayout()
				thisPanel = mc.modelPanel()
				mc.showWindow( window )
				
				selLightShape = mc.listRelatives(e, shapes = True, noIntermediate = True, fullPath=True)
				if mc.nodeType(selLightShape[0]) in lightNodes:
					mc.lookThru(thisPanel, selLightShape[0], nc=10, fc= 10000000)
		elif len(selLight) == 0:
			self.baseMod.statusBar_update("Please select at least one light to look thru.", alert=True)


	def duplicateLightOpt(self):
		dataList= self.lightListModel.dataList
		selLightTypes = self.manager_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selLightTypes:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		finalList = []
		for row in selRows:
			lightName = dataList[row][0]
			finalList.append(lightName)
		
		selLight= finalList
		if len(selLight) >= 1:
			for e in selLight:
				lightNameParts = e.split("_")
				prefix = self.configFile.jsonObj['naming']['prefix']
				suffix = self.configFile.jsonObj['naming']['suffix']
				padding = self.configFile.jsonObj['naming']['padding']
				#Check if name follows naming conventions
				if lightNameParts[0] + "_" == prefix and len(lightNameParts[2]) == padding:
					if len(lightNameParts) == 5:
						if "_" + lightNameParts[4] == suffix:
							checkResult = self.checkLightName(prefix, lightNameParts[1], 1, padding, lightNameParts[3], suffix)
							mc.duplicate(e, name= checkResult, ic= True)
					elif len(lightNameParts) == 4:
						checkResult = self.checkLightName(prefix, lightNameParts[1], 1, padding, lightNameParts[3], suffix)
						mc.duplicate(e, name= checkResult, ic= True)
				else:
					checkResult = self.checkLightName(prefix, e, 1, padding, "Custom", suffix)
					mc.duplicate(e, name= checkResult, ic= True)
			self.resetUI()
		else:
			self.baseMod.statusBar_update("Please select at least one light to duplicate.", alert=True)

			
	def copyLightColorOpt(self):
		dataList= self.lightListModel.dataList
		headers = self.lightListModel.headers
		selLights = self.manager_TABLEVIEW.selectedIndexes()

		selRowsDirty = []
		for item in selLights:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)

		if len(selRows) == 1:
			for each in selLights:
				value = dataList[each.row()][each.column()]
				column = headers[each.column()]
				attrDict = self.getAttr(column)
				if value != None and attrDict != None:
					if attrDict["uiControl"] == "colorswatch":
						self.lightColorBuffer = dataList[each.row()][each.column()]
					else:
						self.baseMod.statusBar_update("Please select a valid color attribute to copy from.", alert=True)
		elif len(selRows) < 1:
			self.baseMod.statusBar_update("Please select at least one light to copy the color from.", alert=True)
		elif len(selRows) > 1:
			self.baseMod.statusBar_update("Please select only one light to copy the color from.", alert=True)
	
	
	def pasteLightColorOpt(self):
		dataList= self.lightListModel.dataList
		headers = self.lightListModel.headers
		selLights = self.manager_TABLEVIEW.selectedIndexes()
		
		if len(selLights) >= 1:
			for each in selLights:
				value = dataList[each.row()][each.column()]
				column = headers[each.column()]
				attrDict = self.getAttr(column)
				if value != None and attrDict != None:
					if attrDict["uiControl"] == "colorswatch":
						self.lightListModel.setData(each,self.lightColorBuffer)
					else:
						self.baseMod.statusBar_update("Please select a valid color attribute to paste to.", alert=True)
		else:
			self.baseMod.statusBar_update("Please select at least one light to paste the color to.", alert=True)
			

##--------------------------------------------------------------------------------------------
##Script job manager class
##--------------------------------------------------------------------------------------------
class ScriptJobManager(object):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self):
		self.jobsList = []
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def createJob(self, *args, **kwargs):
		jobID = mc.scriptJob(*args, **kwargs)
		self.jobsList.append(jobID)
		
		
	def killJobs(self):
		for jobID in self.jobsList:
			mc.scriptJob(kill=jobID)


##---------------------------------------------------------------------------------------------
##Class: Manager Table View- To be able to edit multiple items at once, reimplement commit data
##---------------------------------------------------------------------------------------------
class ManagerTableView(QtWidgets.QTableView):
	##--------------------------------------------------------------------------------------------
	##Signals
	##--------------------------------------------------------------------------------------------
	released= QtCore.Signal()
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent=None):
		super(ManagerTableView, self).__init__(parent)
		self.parent = parent


	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def commitData(self, editor):
		# call parent commitData first
		super(ManagerTableView, self).commitData(editor)
		
		model = self.currentIndex().model()

		value = model.data(self.currentIndex(), QtCore.Qt.EditRole)
		selected = self.selectedIndexes()
		
		selColumnsDirty = []
		for item in selected:
			thisColumn = item.column()
			selColumnsDirty.append(thisColumn)
		selSet = set(selColumnsDirty)
		selColumns = list(selSet)
		
		if len(selColumns) == 1:
			for itemIndex in selected:
				model.setData(itemIndex, value)
				
	
	def mouseReleaseEvent(self, e):
		super(ManagerTableView, self).mouseReleaseEvent(e)
		self.released.emit()
	

	def keyPressEvent(self, event):
		if event.key() == QtCore.Qt.Key_Delete:
			selected = self.selectedIndexes()
			if len(selected) >= 1:
				selectedRows = []
				for e in selected:
					selectedRows.append(e.row())
					thisLight = self.model().dataList[e.row()][1]
					mc.delete(thisLight)
				self.model().removeRows(selectedRows, len(selectedRows))


##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for manager table
##--------------------------------------------------------------------------------------------
class ManagerDataTableModel(dvm.DataTableModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, dataList, headers, parent):
		super(ManagerDataTableModel, self).__init__(dataList, headers, parent)
		self.parent = parent
			
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def flags(self, index):
		"""Set flags of each cell"""
		text = index.model().data(index, QtCore.Qt.DisplayRole)
		if text != "None":
			return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
		else:
			return QtCore.Qt.NoItemFlags
	
	
	def data(self, index, role):
		"""Collect data and put it in the table"""
		row = index.row()
		column = index.column()
		
		if role in [QtCore.Qt.DisplayRole, QtCore.Qt.ToolTipRole, QtCore.Qt.EditRole]:
			return self.dataList[row][column]
		
		if role == QtCore.Qt.DecorationRole:
			value = self.dataList[row][column]
			column = self.headers[index.column()]
			attrDict = self.parent.getAttr(column)
			if value != None and attrDict != None:
				if attrDict["uiControl"] == "colorswatch":
					pixmap = QtGui.QPixmap(16,16)
					pixmap.fill(QtGui.QColor(value))
					return QtGui.QIcon(pixmap)

##--------------------------------------------------------------------------------------------
##Class: Manager Delegate
##--------------------------------------------------------------------------------------------
class ManagerDelegate(QtWidgets.QStyledItemDelegate):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent=None):
		super(ManagerDelegate, self).__init__(parent)
		self.parent = parent
		self.lightAttrsFile = self.parent.lightAttrsFile

	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def paint(self, painter, option, index):
		QtWidgets.QStyledItemDelegate.paint(self, painter, option, index)
	
	
	def sizeHint(self, option, index):
		return QtWidgets.QStyledItemDelegate.sizeHint(self, option, index)
		
		
	def createEditor(self, parent, option, index):
		modelHeaders = index.model().headers
		#What renderers are being used
		renderersNames = self.parent.renderer_COMBOBOX.model().dataList + ['common']
		#Look for the attribute in the json obj
		for each in renderersNames:
			attrsDict = self.lightAttrsFile.jsonObj[each]['attributes']
			if modelHeaders[index.column()] in attrsDict.keys():
				#Look for the control type and create the corresponding editor
				uiControl = attrsDict[modelHeaders[index.column()]]["uiControl"]
				if uiControl == "combobox" or uiControl == "booleancombobox":
					combobox = QtWidgets.QComboBox(parent)
					comboList = attrsDict[modelHeaders[index.column()]]["values"]
					combobox.addItems(comboList)
					combobox.setEditable(False)
					combobox.currentIndexChanged.connect(lambda: self.commitEditorData(combobox))
					return combobox
				else:
					return QtWidgets.QStyledItemDelegate.createEditor(self, parent, option, index)
			elif modelHeaders[index.column()] == "Light Name":
				return QtWidgets.QStyledItemDelegate.createEditor(self, parent, option, index)
		
		
	def setEditorData(self, editor, index):
		modelHeaders = index.model().headers
		if modelHeaders[index.column()] in ["Light Name", "fullPath"]:
			if modelHeaders[index.column()] == "Light Name":
				QtWidgets.QStyledItemDelegate.setEditorData(self, editor, index)
		else:
			attrDict = self.parent.getAttr(modelHeaders[index.column()])
			text = index.model().data(index, QtCore.Qt.DisplayRole)
			if attrDict["uiControl"] == "combobox" or attrDict["uiControl"] == "booleancombobox":
				i = editor.findText(text)
				if i == -1:
					i = 0
				editor.setCurrentIndex(i)
			else:
				QtWidgets.QStyledItemDelegate.setEditorData(self, editor, index)
	
	
	def setModelData(self, editor, model, index):
		QtWidgets.QStyledItemDelegate.setModelData(self, editor, model, index)
	
	
	def commitEditorData(self, editor):
		self.commitData.emit(editor)
		self.closeEditor.emit(editor,self.NoHint)


##--------------------------------------------------------------------------------------------
##Create create light window
##--------------------------------------------------------------------------------------------
class CreateLightGUI(createLightform, createLightbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent=None):
		super(CreateLightGUI,self).__init__(parent)
		self.setupUi(self)
		self.setConnections()
		self.initUI()
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		#SIGNALS
		self.cancel_BTN.clicked.connect(self.cancelAll)
		self.create_BTN.clicked.connect(self.createLight)
	
	def initUI(self):
		"""Initialize UI with objects in the scene"""
		self.configFile = JSONManager(config_json, self)
		
		#Populate functions
		lightFunctions= self.configFile.jsonObj['lightFunctions']
		lightFunctions = sorted(lightFunctions)
		lightFunctionsModel = dvm.ObjectsListModel(lightFunctions)
		self.lightFunction_COMBOBOX.setModel(lightFunctionsModel)
	
	
	def createLight(self):
		self.done(1)
	
	
	def cancelAll(self):
		self.done(0)


##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create light attrs dialog
##--------------------------------------------------------------------------------------------
class LightAttrs_DialogGUI(lightAttrsform, lightAttrsbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Light Attributes Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(LightAttrs_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		
		self.configFile = JSONManager(config_json, self)
		self.lightAttrsFile = JSONManager(lightAttrs_json, self)
		self.attributesHeaders=["name","display","uiControl","default","minValue","maxValue","values","lightTypes"]
		self.nodesHeaders=["name"]
		self.propertiesModels = {}
		
		self.initUI()
		self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		#SIGNALS
		self.cancel_BTN.clicked.connect(self.cancelAll)
		self.save_BTN.clicked.connect(self.saveAttrs)
		self.renderers_TREEVIEW.clicked.connect(self.populateProperties)
		self.renderers_TREEVIEW.model().RendererRename.connect(self.rendererNameChanged)
		self.properties_TABLEVIEW.doubleClicked.connect(self.editColorItem)
		#CONTEXT MENUS
		self.renderers_TREEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.renderers_TREEVIEW.customContextMenuRequested.connect(self.renderersOptions)
		self.properties_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.properties_TABLEVIEW.customContextMenuRequested.connect(self.propertiesOptions)
		
	
	def initUI(self):
		self.populateRenderers()
		self.initProperties()
		self.populateProperties()
		
	
	def rendererNameChanged(self, oldName, newName):
		self.propertiesModels[newName] = self.propertiesModels.pop(oldName)
		for key, value in self.propertiesModels.iteritems():
			print key, value[0].dataList, value[1].dataList
		
	
	def saveAttrs(self):
		"""Apply changes to JSON file"""
		manager = JSONManager(lightAttrs_json, self)
		manager.rootNode = self.renderers_TREEVIEW.model().dataList
		jsonDict = manager.treeToJson()
		
		for renderer, categories in jsonDict.iteritems():
			i = 0
			for category, attributes in categories.iteritems():
				if category == "lightNodes":
					nodesList = []
					for row in self.propertiesModels[renderer][i].dataList:
						for column in self.nodesHeaders:
							if column == "name":
								nodesList.append(row[0])
					jsonDict[renderer][category] = {"nodes":nodesList}
				elif category == "attributes":
					allAttrsDict= {}
					for attr in self.propertiesModels[renderer][i].dataList:
						attrDict= {}
						for column in self.attributesHeaders:
							if column == "display":
								attrDict["display"]= attr[1]
							elif column == "uiControl":
								attrDict["uiControl"]= attr[2]
							elif column == "default":
								attrDict["default"]= attr[3]
							elif column == "minValue":
								attrDict["minValue"]= attr[4]
							elif column == "maxValue":
								attrDict["maxValue"]= attr[5]
							elif column == "values":
								if attr[6] == "None":
									attrDict["values"]= attr[6]
								else:
									attrDict["values"]= attr[6].split(",")
							elif column == "lightTypes":
								attrDict["lightTypes"]= attr[7].split(",")
						allAttrsDict[attr[0]] = attrDict
					jsonDict[renderer][category] = allAttrsDict
				i += 1
		self.lightAttrsFile.writeJson(jsonDict, self.lightAttrsFile.fileName)
		
		self.done(1)
	
	
	def populateRenderers(self):
		rootNode = self.renderersRootNode()
		treeModel = RenderersDataTreeModel(rootNode,['name'],self)
		self.renderers_TREEVIEW.setModel(treeModel)
		self.renderers_TREEVIEW.setHeaderHidden(True)
		self.renderers_TREEVIEW.expandAll()
		self.renderers_TREEVIEW.setItemsExpandable(False)
		self.renderers_TREEVIEW.setRootIsDecorated(False)
		self.renderers_TREEVIEW.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
		self.renderers_TREEVIEW.setCurrentIndex(self.renderers_TREEVIEW.model().index(0,0))
		
	
	def renderersRootNode(self):
		newRootNode = dvm.TreeNode(['name'],['name'])
		for child in self.lightAttrsFile.rootNode.children:
			node= newRootNode.insertChildren(newRootNode.childCount(), child.data, child.headers, 1)
			for subchild in child.children:
				node.insertChildren(node.childCount(), [subchild.name], ['name'], 1)
		return newRootNode
	
	
	def initProperties(self):
		jsonObj = self.lightAttrsFile.jsonObj
		for renderer in self.renderers_TREEVIEW.model().dataList.children:
			self.propertiesModels[renderer.name] = []
			for category in renderer.children:
				dataList = []
				if category.name == "lightNodes":
					for item in jsonObj[renderer.name][category.name]["nodes"]:
						dataList.append([item])
					newModel = PropertiesDataTableModel(dataList, self.nodesHeaders, self)
					self.propertiesModels[renderer.name].append(newModel)
				elif category.name == "attributes":
					for attr in jsonObj[renderer.name][category.name].keys():
						rowData = []
						rowDict = jsonObj[renderer.name][category.name][attr]
						for column in self.attributesHeaders:
							if column == "name":
								rowData.append(attr)
							elif column == "display":
								rowData.append(rowDict[column])
							elif column == "uiControl":
								rowData.append(rowDict[column])
							elif column == "default":
								rowData.append(rowDict[column])
							elif column == "minValue":
								rowData.append(rowDict[column])
							elif column == "maxValue":
								rowData.append(rowDict[column])
							elif column == "values":
								if type(rowDict[column]) is list:
									rowData.append(",".join(rowDict[column]))
								else:
									rowData.append(rowDict[column])
							elif column == "lightTypes":
								rowData.append(",".join(rowDict[column]))
						dataList.append(rowData)
					newModel = PropertiesDataTableModel(dataList, self.attributesHeaders, self)
					self.propertiesModels[renderer.name].append(newModel)
	
	
	def populateProperties(self):
		node = self.renderers_TREEVIEW.currentIndex().internalPointer()
		self.properties_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
		if node.depth == 1:
			thisModel = self.propertiesModels[node.name][0]
			self.properties_TABLEVIEW.setModel(thisModel)
		elif node.depth == 2:
			row = self.renderers_TREEVIEW.currentIndex().row()
			thisModel = self.propertiesModels[node.parent.name][row]
			self.properties_TABLEVIEW.setModel(thisModel)
		if len(thisModel.headers) > 1:
			self.properties_TABLEVIEW.setColumnWidth(1,50)
			self.properties_TABLEVIEW.setColumnWidth(2,100)
			self.properties_TABLEVIEW.setColumnWidth(3,70)
			self.properties_TABLEVIEW.setColumnWidth(4,50)
			self.properties_TABLEVIEW.setColumnWidth(5,50)
			self.properties_TABLEVIEW.setColumnWidth(6,150)
			self.properties_TABLEVIEW.setColumnWidth(7,300)
			delegate = PropertiesDelegate(self.properties_TABLEVIEW)
			self.properties_TABLEVIEW.setItemDelegate(delegate)
	
	
	def editColorItem(self):
		selected = self.properties_TABLEVIEW.selectedIndexes()
		for index in selected:
			modelHeaders = index.model().headers
			dataList = index.model().dataList
			if modelHeaders[index.column()] == "default":
				uiControl = dataList[index.row()][2]
				if uiControl == "colorswatch":
					currentValue = dataList[index.row()][index.column()]
					colorDialog = QtWidgets.QColorDialog(self)
					colorDialog.setCurrentColor(QtGui.QColor(currentValue))
					result = colorDialog.exec_()
					if result:
						index.model().setData(index, colorDialog.selectedColor().name())
			
				
	def cancelAll(self):
		"""Method to close the UI"""
		self.done(0)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for categories list
	##--------------------------------------------------------------------------------------------
	def renderersOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.renderers_TREEVIEW)
		addRendererQ = menu.addAction("Add renderer")
		deleteRendererQ = menu.addAction("Delete renderer")
		menu.popup(self.renderers_TREEVIEW.mapToGlobal(pos))
		
		addRendererQ.triggered.connect(self.addRenderer)
		deleteRendererQ.triggered.connect(self.deleteRenderer)
	
	
	def addRenderer(self):
		node = self.renderers_TREEVIEW.model().insertRows(self.renderers_TREEVIEW.model().dataList.childCount(), 1, ['newRenderer'])
		modelIndex = self.renderers_TREEVIEW.model().indexFromNode(node)
		self.renderers_TREEVIEW.model().insertRows(node.childCount(), 1, ['lightNodes'], modelIndex)
		self.renderers_TREEVIEW.model().insertRows(node.childCount(), 1, ['attributes'], modelIndex)
		self.renderers_TREEVIEW.expandAll()
		self.propertiesModels['newRenderer'] = []
		nodesModel = PropertiesDataTableModel([], self.nodesHeaders, self)
		self.propertiesModels['newRenderer'].append(nodesModel)
		attrsModel = PropertiesDataTableModel([], self.attributesHeaders, self)
		self.propertiesModels['newRenderer'].append(attrsModel)
		

	def deleteRenderer(self):
		node = self.renderers_TREEVIEW.currentIndex().internalPointer()
		if node.depth == 2:
			node = node.parent
		thisIndex = self.renderers_TREEVIEW.model().indexFromNode(node)
		self.renderers_TREEVIEW.model().removeRows([thisIndex])
		self.populateProperties()
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for properties table
	##--------------------------------------------------------------------------------------------
	def propertiesOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.properties_TABLEVIEW)
		if len(self.properties_TABLEVIEW.model().headers) > 1:
			addAttributeQ = menu.addAction("Add attribute")
			deleteAttributeQ = menu.addAction("Delete attribute")
			addAttributeQ.triggered.connect(self.addAttribute)
			deleteAttributeQ.triggered.connect(self.deleteAttribute)
		else:
			addNodeQ = menu.addAction("Add node")
			deleteNodeQ = menu.addAction("Delete node")
			addNodeQ.triggered.connect(self.addNode)
			deleteNodeQ.triggered.connect(self.deleteNode)
		menu.popup(self.properties_TABLEVIEW.mapToGlobal(pos))
	
	
	def addNode(self):
		node = self.renderers_TREEVIEW.currentIndex().internalPointer()
		if node.depth == 1:
			self.propertiesModels[node.name][0].insertRows(self.propertiesModels[node.name][0].rowCount(),1,['newNode'])
		elif node.depth == 2:
			row = self.renderers_TREEVIEW.currentIndex().row()
			self.propertiesModels[node.parent.name][row].insertRows(self.propertiesModels[node.parent.name][row].rowCount(),1,['newNode'])
	
	
	def deleteNode(self):
		node = self.renderers_TREEVIEW.currentIndex().internalPointer()
		selected = self.properties_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selected:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		if node.depth == 1:
			self.propertiesModels[node.name][0].removeRows(selRows, len(selRows))
		elif node.depth == 2:
			row = self.renderers_TREEVIEW.currentIndex().row()
			self.propertiesModels[node.parent.name][row].removeRows(selRows, len(selRows))
	
	
	def addAttribute(self):
		node = self.renderers_TREEVIEW.currentIndex().internalPointer()
		row = self.renderers_TREEVIEW.currentIndex().row()
		
		addLightPropertyDialog = AddLightPropertyGUI(self)
		result = addLightPropertyDialog.exec_()
		if result:
			dataList = []
			for column in self.attributesHeaders:
				dataList.append(addLightPropertyDialog.attrDict[column])
			self.propertiesModels[node.parent.name][row].insertRows(self.propertiesModels[node.parent.name][row].rowCount(), 1, dataList)
	
	
	def deleteAttribute(self):
		node = self.renderers_TREEVIEW.currentIndex().internalPointer()
		row = self.renderers_TREEVIEW.currentIndex().row()
		if self.propertiesModels[node.parent.name][row].rowCount() == 1:
			msgBox = QtWidgets.QMessageBox(self)
			msgBox.setWindowTitle("Warning!")
			msgBox.setText("There has to be at least one light attribute in the table.")
			msgBox.exec_()
		else:
			selected = self.properties_TABLEVIEW.selectedIndexes()
			selRowsDirty = []
			for item in selected:
				thisRow = item.row()
				selRowsDirty.append(thisRow)
			selSet = set(selRowsDirty)
			selRows = list(selSet)
			self.propertiesModels[node.parent.name][row].removeRows(selRows, len(selRows))


##--------------------------------------------------------------------------------------------
##Class: Tree Data Model reimplementation for renderers tree
##--------------------------------------------------------------------------------------------
class RenderersDataTreeModel(dvm.DataTreeModel):
	RendererRename = QtCore.Signal(str,str)
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, root, headers, parent):
		super(RenderersDataTreeModel, self).__init__(root, headers, parent)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def flags(self, index):
		"""Set flags of each cell"""
		
		node = self.getNode(index)
		if node.parent.parent == None:
			return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
		else:
			return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
	
	
	def rendererNameChanged(self):
		self.RendererRename.emit()
	
	
	def setData(self, index, value, role=QtCore.Qt.EditRole):
		'''
		Set data for specified index and display it.
		:param index: Index to be edited
		:type index: QModelIndex
		:param role: Edit role as default. Reimplement with elif chain for another role.
		:type role: QtCore.Qt.Role
		:return: Item data to be shown in the GUI. Might be None.
		:rtype: object
		'''
		if role != QtCore.Qt.EditRole:
			return False
		else:
			node = self.getNode(index)
			oldName = node.name
			result = node.setData(index.column(),value)
	
			if result:
				self.dataChanged.emit(index, index)
				self.RendererRename.emit(oldName, value)
	
			return result


##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for properties table
##--------------------------------------------------------------------------------------------
class PropertiesDataTableModel(dvm.DataTableModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, dataList, headers, parent):
		super(PropertiesDataTableModel, self).__init__(dataList, headers, parent)
		
			
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def flags(self, index):
		"""Set flags of each cell"""
		
		row = index.row()
		column = index.column()
		columnName = self.headers[index.column()]
		if columnName in ["uiControl","values","lightTypes"]:
			return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
		elif columnName in ["default","minValue","maxValue"]:
			if self.dataList[row][column] == "None":
				return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
			else:
				return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
		else:
			return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
	
	
	def data(self, index, role):
		"""Collect data and put it in the table"""
		row = index.row()
		column = index.column()
		
		if role in [QtCore.Qt.DisplayRole, QtCore.Qt.ToolTipRole, QtCore.Qt.EditRole]:
			return self.dataList[row][column]
	
		if role == QtCore.Qt.DecorationRole:
			if len(self.headers) > 1:
				value = self.dataList[row][column]
				uiControl = self.dataList[row][2]
				if uiControl == "colorswatch":
					if self.headers[column] == "default":
						pixmap = QtGui.QPixmap(16,16)
						pixmap.fill(QtGui.QColor(value))
						return QtGui.QIcon(pixmap)


##--------------------------------------------------------------------------------------------
##Class: Properties Delegate
##--------------------------------------------------------------------------------------------
class PropertiesDelegate(QtWidgets.QStyledItemDelegate):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent=None):
		super(PropertiesDelegate, self).__init__(parent)
		self.parent = parent

	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def paint(self, painter, option, index):
		QtWidgets.QStyledItemDelegate.paint(self, painter, option, index)
	
	
	def sizeHint(self, option, index):
		return QtWidgets.QStyledItemDelegate.sizeHint(self, option, index)
		
	
	def createEditor(self, parent, option, index):
		modelHeaders = index.model().headers
		dataList = index.model().dataList
		if modelHeaders[index.column()] == "display":
			combobox = QtWidgets.QComboBox(parent)
			comboList = ["False","True"]
			combobox.addItems(sorted(comboList))
			combobox.setEditable(False)
			combobox.currentIndexChanged.connect(lambda: self.commitEditorData(combobox))
			return combobox
		elif modelHeaders[index.column()] == "default":
			uiControl = dataList[index.row()][2]
			if uiControl == "combobox" or uiControl == "booleancombobox":
				combobox = QtWidgets.QComboBox(parent)
				comboList = dataList[index.row()][6].split(",")
				combobox.addItems(sorted(comboList))
				combobox.setEditable(False)
				combobox.currentIndexChanged.connect(lambda: self.commitEditorData(combobox))
				return combobox
			else:
				return QtWidgets.QStyledItemDelegate.createEditor(self, parent, option, index)
		else:
			return QtWidgets.QStyledItemDelegate.createEditor(self, parent, option, index)
		
		
	def setEditorData(self, editor, index):
		modelHeaders = index.model().headers
		text = index.model().data(index, QtCore.Qt.DisplayRole)
		if modelHeaders[index.column()] == "display" or modelHeaders[index.column()] == "default":
			if type(editor) is QtWidgets.QComboBox:
				i = editor.findText(text)
				if i == -1:
					i = 0
				editor.setCurrentIndex(i)
			else:
				QtWidgets.QStyledItemDelegate.setEditorData(self, editor, index)
		else:
			QtWidgets.QStyledItemDelegate.setEditorData(self, editor, index)
	
	
	def setModelData(self, editor, model, index):
		QtWidgets.QStyledItemDelegate.setModelData(self, editor, model, index)
	
	
	def commitEditorData(self, editor):
		self.commitData.emit(editor)
		self.closeEditor.emit(editor,self.NoHint)


##--------------------------------------------------------------------------------------------
##Create Light Attrs Window
##--------------------------------------------------------------------------------------------
class AddLightPropertyGUI(addLightPropertiesform, addLightPropertiesbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		super(AddLightPropertyGUI, self).__init__(parent)
		self.setupUi(self)
		
		self.booleanModel = dvm.ObjectsListModel(["False","True"],self)
		self.valuesModel = ConfigObjectsListModel(["newValue"],self)
		self.lightAttrsFile = JSONManager(lightAttrs_json, self)
		
		self.initUI()
		self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		#SIGNALS
		self.cancel_BTN.clicked.connect(self.cancelAll)
		self.add_BTN.clicked.connect(self.addLightAttr)
		self.uiControl_COMBOBOX.currentIndexChanged.connect(self.refreshuiControl)
		self.uiControl_COMBOBOX.currentIndexChanged.connect(self.resetListModels)
		self.color_BTN.clicked.connect(self.editColor)
		self.colorTxt_BTN.clicked.connect(self.editColor)
		#STATUS
		self.color_BTN.setEnabled(False)
		self.colorTxt_BTN.setEnabled(False)
		self.default_COMBOBOX.setEnabled(False)
		self.default_DBLSPINBOX.setEnabled(False)
		self.minValue_DBLSPINBOX.setEnabled(False)
		self.maxValue_DBLSPINBOX.setEnabled(False)
		self.values_LISTVIEW.setEnabled(False)
		#ICONS
		self.labelPixmap = QtGui.QPixmap(16,16)
		self.labelPixmap.fill(QtGui.QColor(255,255,255))
		self.color_BTN.setIcon(QtGui.QIcon(self.labelPixmap))
		#CONTEXT MENUS
		self.values_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.values_LISTVIEW.customContextMenuRequested.connect(self.valuesOptions)
	
	
	def initUI(self):
		#Populate display
		displayDataList = ["False","True"]
		displayModel = dvm.ObjectsListModel(displayDataList)
		self.display_COMBOBOX.setModel(displayModel)
		
		#Populate uiControls
		uiControlDataList = ["intslider","floatslider","colorswatch","combobox","booleancombobox"]
		uiControlModel = dvm.ObjectsListModel(uiControlDataList)
		self.uiControl_COMBOBOX.setModel(uiControlModel)
		
		#Populate lightTypes
		lightTypesDataList = []
		for renderer in self.lightAttrsFile.jsonObj.keys():
			lightTypesDataList += self.lightAttrsFile.jsonObj[renderer]['lightNodes']['nodes']
		lightTypesModel = dvm.ObjectsListModel(lightTypesDataList)
		self.lightTypes_LISTVIEW.setModel(lightTypesModel)
		self.lightTypes_LISTVIEW.setSelectionMode(self.lightTypes_LISTVIEW.MultiSelection)
	
	
	def addLightAttr(self):
		self.attrDict = {}
		selected = self.uiControl_COMBOBOX.currentText()
		selLightTypes = self.lightTypes_LISTVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selLightTypes:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		lightsList = []
		for row in range( self.lightTypes_LISTVIEW.model().rowCount()):
			if row in selRows:
				lightsList.append(self.lightTypes_LISTVIEW.model().dataList[row])
				
		if selected == "intslider":
			if len(self.name_LINEEDIT.text()) == 0:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("You have to specify an attribute name.")
				msgBox.exec_()
			elif self.minValue_SPINBOX.value() > self.maxValue_SPINBOX.value():
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("Min Value can't be higher than Max Value.")
				msgBox.exec_()
			elif self.default_SPINBOX.value() < self.minValue_SPINBOX.value() or self.default_SPINBOX.value() > self.maxValue_SPINBOX.value():
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("Default Value can't be higher than Max Value or lower than Min Value.")
				msgBox.exec_()
			elif len(selLightTypes) < 1:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("You have to select at least one light type from the list.")
				msgBox.exec_()
			else:
				self.attrDict["name"]= self.name_LINEEDIT.text()
				self.attrDict["display"]= self.display_COMBOBOX.currentText()
				self.attrDict["uiControl"]= self.uiControl_COMBOBOX.currentText()
				self.attrDict["default"]= self.default_SPINBOX.value()
				self.attrDict["minValue"]= self.minValue_SPINBOX.value()
				self.attrDict["maxValue"]= self.maxValue_SPINBOX.value()
				self.attrDict["values"]= "None"
				self.attrDict["lightTypes"]= ",".join(lightsList)
				self.done(1)
		elif selected == "floatslider":
			if len(self.name_LINEEDIT.text()) == 0:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("You have to specify an attribute name.")
				msgBox.exec_()
			elif self.minValue_DBLSPINBOX.value() > self.maxValue_DBLSPINBOX.value():
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("Min Value can't be higher than Max Value.")
				msgBox.exec_()
			elif self.default_DBLSPINBOX.value() < self.minValue_DBLSPINBOX.value() or self.default_DBLSPINBOX.value() > self.maxValue_DBLSPINBOX.value():
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("Default Value can't be higher than Max Value or lower than Min Value.")
				msgBox.exec_()
			elif len(selLightTypes) < 1:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("You have to select at least one light type from the list.")
				msgBox.exec_()
			else:
				self.attrDict["name"]= self.name_LINEEDIT.text()
				self.attrDict["display"]= self.display_COMBOBOX.currentText()
				self.attrDict["uiControl"]= self.uiControl_COMBOBOX.currentText()
				self.attrDict["default"]= self.default_DBLSPINBOX.value()
				self.attrDict["minValue"]= self.minValue_DBLSPINBOX.value()
				self.attrDict["maxValue"]= self.maxValue_DBLSPINBOX.value()
				self.attrDict["values"]= "None"
				self.attrDict["lightTypes"]= ",".join(lightsList)
				self.done(1)
		elif selected == "colorswatch":
			if len(self.name_LINEEDIT.text()) == 0:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("You have to specify an attribute name.")
				msgBox.exec_()
			elif len(self.colorTxt_BTN.text()) == 0:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("Default value can't be empty.")
				msgBox.exec_()
			elif len(self.colorTxt_BTN.text()) > 7:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("Colors have to fit in the hexadecimal format #RRGGBB.")
				msgBox.exec_()
			elif self.colorTxt_BTN.text()[0] != "#":
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("Colors have to start with a '#' sign.")
				msgBox.exec_()
			elif len(selLightTypes) < 1:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("You have to select at least one light type from the list.")
				msgBox.exec_()
			else:
				self.attrDict["name"]= self.name_LINEEDIT.text()
				self.attrDict["display"]= self.display_COMBOBOX.currentText()
				self.attrDict["uiControl"]= self.uiControl_COMBOBOX.currentText()
				self.attrDict["default"]= self.colorTxt_BTN.text()
				self.attrDict["minValue"]= "None"
				self.attrDict["maxValue"]= "None"
				self.attrDict["values"]= "None"
				self.attrDict["lightTypes"]= ",".join(lightsList)
				self.done(1)
		elif selected == "booleancombobox":
			if len(self.name_LINEEDIT.text()) == 0:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("You have to specify an attribute name.")
				msgBox.exec_()
			elif self.valuesModel.rowCount() < 1:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("There has to be at least one value y the values list.")
				msgBox.exec_()
			elif len(selLightTypes) < 1:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("You have to select at least one light type from the list.")
				msgBox.exec_()
			else:
				self.attrDict["name"]= self.name_LINEEDIT.text()
				self.attrDict["display"]= self.display_COMBOBOX.currentText()
				self.attrDict["uiControl"]= self.uiControl_COMBOBOX.currentText()
				self.attrDict["default"]= self.default_COMBOBOX.currentText()
				self.attrDict["minValue"]= "None"
				self.attrDict["maxValue"]= "None"
				self.attrDict["values"]= ",".join(["False","True"])
				self.attrDict["lightTypes"]= ",".join(lightsList)
				self.done(1)
		elif selected == "combobox":
			if len(self.name_LINEEDIT.text()) == 0:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("You have to specify an attribute name.")
				msgBox.exec_()
			elif self.valuesModel.rowCount() < 1:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("There has to be at least one value y the values list.")
				msgBox.exec_()
			elif len(selLightTypes) < 1:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("You have to select at least one light type from the list.")
				msgBox.exec_()
			else:
				self.attrDict["name"]= self.name_LINEEDIT.text()
				self.attrDict["display"]= self.display_COMBOBOX.currentText()
				self.attrDict["uiControl"]= self.uiControl_COMBOBOX.currentText()
				self.attrDict["default"]= self.default_COMBOBOX.currentText()
				self.attrDict["minValue"]= "None"
				self.attrDict["maxValue"]= "None"
				self.attrDict["values"]= ",".join(self.valuesModel.dataList)
				self.attrDict["lightTypes"]= ",".join(lightsList)
				self.done(1)
	
	
	def editColor(self):
		currentValue = self.color_BTN.text()
		colorDialog = QtWidgets.QColorDialog(self)
		colorDialog.setCurrentColor(QtGui.QColor(currentValue))
		result = colorDialog.exec_()
		if result:
			self.colorTxt_BTN.setText(colorDialog.selectedColor().name())
			self.labelPixmap.fill(colorDialog.selectedColor())
			self.color_BTN.setIcon(QtGui.QIcon(self.labelPixmap))
	
	
	def refreshuiControl(self):
		selected = self.uiControl_COMBOBOX.currentText()
		if selected == "intslider":
			self.color_BTN.setEnabled(False)
			self.colorTxt_BTN.setEnabled(False)
			self.default_COMBOBOX.setEnabled(False)
			self.default_DBLSPINBOX.setEnabled(False)
			self.minValue_DBLSPINBOX.setEnabled(False)
			self.maxValue_DBLSPINBOX.setEnabled(False)
			self.values_LISTVIEW.setEnabled(False)
			self.default_SPINBOX.setEnabled(True)
			self.minValue_SPINBOX.setEnabled(True)
			self.maxValue_SPINBOX.setEnabled(True)
		elif selected == "floatslider":
			self.color_BTN.setEnabled(False)
			self.colorTxt_BTN.setEnabled(False)
			self.default_COMBOBOX.setEnabled(False)
			self.default_DBLSPINBOX.setEnabled(True)
			self.minValue_DBLSPINBOX.setEnabled(True)
			self.maxValue_DBLSPINBOX.setEnabled(True)
			self.values_LISTVIEW.setEnabled(False)
			self.default_SPINBOX.setEnabled(False)
			self.minValue_SPINBOX.setEnabled(False)
			self.maxValue_SPINBOX.setEnabled(False)
		elif selected == "colorswatch":
			self.color_BTN.setEnabled(True)
			self.colorTxt_BTN.setEnabled(True)
			self.default_COMBOBOX.setEnabled(False)
			self.default_DBLSPINBOX.setEnabled(False)
			self.minValue_DBLSPINBOX.setEnabled(False)
			self.maxValue_DBLSPINBOX.setEnabled(False)
			self.values_LISTVIEW.setEnabled(False)
			self.default_SPINBOX.setEnabled(False)
			self.minValue_SPINBOX.setEnabled(False)
			self.maxValue_SPINBOX.setEnabled(False)
		elif selected == "combobox":
			self.color_BTN.setEnabled(False)
			self.colorTxt_BTN.setEnabled(False)
			self.default_COMBOBOX.setEnabled(True)
			self.default_DBLSPINBOX.setEnabled(False)
			self.minValue_DBLSPINBOX.setEnabled(False)
			self.maxValue_DBLSPINBOX.setEnabled(False)
			self.values_LISTVIEW.setEnabled(True)
			self.default_SPINBOX.setEnabled(False)
			self.minValue_SPINBOX.setEnabled(False)
			self.maxValue_SPINBOX.setEnabled(False)
		elif selected == "booleancombobox":
			self.color_BTN.setEnabled(False)
			self.colorTxt_BTN.setEnabled(False)
			self.default_COMBOBOX.setEnabled(True)
			self.default_DBLSPINBOX.setEnabled(False)
			self.minValue_DBLSPINBOX.setEnabled(False)
			self.maxValue_DBLSPINBOX.setEnabled(False)
			self.values_LISTVIEW.setEnabled(False)
			self.default_SPINBOX.setEnabled(False)
			self.minValue_SPINBOX.setEnabled(False)
			self.maxValue_SPINBOX.setEnabled(False)
	
	
	def resetListModels(self):
		selected = self.uiControl_COMBOBOX.currentText()
		if selected in ["intslider","floatslider","colorswatch","combobox"]:
			self.valuesModel = ConfigObjectsListModel(["newValue"],self)
			self.colorTxt_BTN.setText("#ffffff")
			self.labelPixmap.fill(QtGui.QColor(255,255,255))
			self.color_BTN.setIcon(QtGui.QIcon(self.labelPixmap))
			self.default_COMBOBOX.setModel(self.valuesModel)
			self.default_DBLSPINBOX.setValue(0)
			self.minValue_DBLSPINBOX.setValue(0)
			self.maxValue_DBLSPINBOX.setValue(0)
			self.values_LISTVIEW.setModel(self.valuesModel)
			self.default_SPINBOX.setValue(0)
			self.minValue_SPINBOX.setValue(0)
			self.maxValue_SPINBOX.setValue(0)
		elif selected == "booleancombobox":
			self.colorTxt_BTN.setText("#ffffff")
			self.labelPixmap.fill(QtGui.QColor(255,255,255))
			self.color_BTN.setIcon(QtGui.QIcon(self.labelPixmap))
			self.default_COMBOBOX.setModel(self.booleanModel)
			self.default_DBLSPINBOX.setValue(0)
			self.minValue_DBLSPINBOX.setValue(0)
			self.maxValue_DBLSPINBOX.setValue(0)
			self.values_LISTVIEW.setModel(self.booleanModel)
			self.default_SPINBOX.setValue(0)
			self.minValue_SPINBOX.setValue(0)
			self.maxValue_SPINBOX.setValue(0)
	
	
	def cancelAll(self):
		"""Method to close the UI"""
		
		self.done(0)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for values list
	##--------------------------------------------------------------------------------------------
	def valuesOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.values_LISTVIEW)
		addValueQ = menu.addAction("Add value")
		deleteValueQ = menu.addAction("Delete value")
		menu.popup(self.values_LISTVIEW.mapToGlobal(pos))
		
		addValueQ.triggered.connect(self.addValue)
		deleteValueQ.triggered.connect(self.deleteValue)
	
	
	def addValue(self):
		self.valuesModel.insertRows(self.valuesModel.rowCount(), ["newValue"])
	
	
	def deleteValue(self):
		selectedIndex = self.values_LISTVIEW.currentIndex().row()
		if len(self.valuesModel.dataList) == 1:
			msgBox = QtWidgets.QMessageBox(self)
			msgBox.setWindowTitle("Warning!")
			msgBox.setText("There has to be at least one value in the list.")
			msgBox.exec_()
		else:
			self.valuesModel.removeRows([selectedIndex], 1)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		
		self.configFile = JSONManager(config_json, self)
		
		self.initUI()
		self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		#BUTTONS
		self.cancel_BTN.clicked.connect(self.cancelAll)
		self.save_BTN.clicked.connect(self.saveConfig)
		#CONTEXT MENUS
		self.lightFunctions_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.lightFunctions_LISTVIEW.customContextMenuRequested.connect(self.lightFunctionsOptions)
	
	
	def initUI(self):
		self.populateLightFunctions()
		self.prefix_LINEEDIT.setText(self.configFile.jsonObj['naming']['prefix'])
		self.suffix_LINEEDIT.setText(self.configFile.jsonObj['naming']['suffix'])
		self.padding_SPINBOX.setValue(self.configFile.jsonObj['naming']['padding'])
	
	
	def populateLightFunctions(self):
		lightFunctions = self.configFile.jsonObj['lightFunctions']
		lightFunctionsModel = ConfigObjectsListModel(lightFunctions, self)
		self.lightFunctions_LISTVIEW.setModel(lightFunctionsModel)
		self.lightFunctions_LISTVIEW.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
		self.lightFunctions_LISTVIEW.setCurrentIndex(self.lightFunctions_LISTVIEW.model().index(0,0))
	
	
	def saveConfig(self):
		"""Apply changes to JSON file"""
		
		jsonObj = self.configFile.jsonObj
		lightFunctions = self.lightFunctions_LISTVIEW.model().dataList
		jsonObj['lightFunctions'] = lightFunctions
		self.configFile.jsonObj['naming']['suffix'] = self.suffix_LINEEDIT.text()
		self.configFile.jsonObj['naming']['prefix'] = self.prefix_LINEEDIT.text()
		self.configFile.jsonObj['naming']['padding'] = self.padding_SPINBOX.value()
		self.configFile.writeJson(jsonObj, self.configFile.fileName)
		
		self.done(1)
	
	
	def cancelAll(self):
		"""Method to close the UI"""
		
		self.done(0)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for light functions list
	##--------------------------------------------------------------------------------------------
	def lightFunctionsOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.lightFunctions_LISTVIEW)
		addTypeQ = menu.addAction("Add light function")
		deleteTypeQ = menu.addAction("Delete light function")
		menu.popup(self.lightFunctions_LISTVIEW.mapToGlobal(pos))
		
		addTypeQ.triggered.connect(self.addFunction)
		deleteTypeQ.triggered.connect(self.deleteFunction)
	
	
	def addFunction(self):
		self.lightFunctions_LISTVIEW.model().insertRows(self.lightFunctions_LISTVIEW.model().rowCount(), ["newLightFunction"])
	
	
	def deleteFunction(self):
		selectedIndex = self.lightFunctions_LISTVIEW.currentIndex().row()
		if len(self.lightFunctions_LISTVIEW.model().dataList) == 1:
			msgBox = QtWidgets.QMessageBox(self)
			msgBox.setWindowTitle("Warning!")
			msgBox.setText("There has to be at least one Light Function in the list.")
			msgBox.exec_()
		else:
			self.lightFunctions_LISTVIEW.model().removeRows([selectedIndex], 1)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class MiniToolsConfig_DialogGUI(minitools_configform, minitools_configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(MiniToolsConfig_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		
		self.configFile = JSONManager(minitools_config_json, self)
		self.initUI()
		self.setConnections()
		
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		#SIGNALS
		self.cancel_BTN.clicked.connect(self.cancelAll)
		self.save_BTN.clicked.connect(self.saveConfig)
	
	
	def initUI(self):
		"""Read JSON config file"""
		nearClip = self.configFile.getValueByProperty("nearClip", False)
		farClip = self.configFile.getValueByProperty("farClip", False)
		self.nearClip_DSPINBOX.setValue(nearClip)
		self.farClip_DSPINBOX.setValue(farClip)
	
	
	def saveConfig(self):
		"""Apply changes to JSON file"""
		jsonObj = {}
		jsonObj['nearClip'] = self.nearClip_DSPINBOX.value()
		jsonObj['farClip'] = self.farClip_DSPINBOX.value()
		self.configFile.writeJson(jsonObj, self.configFile.fileName)
		
		self.done(1)
	
	
	def cancelAll(self):
		"""Method to close the UI"""
		
		self.done(0)


##--------------------------------------------------------------------------------------------
##Class: Objects lists model
##--------------------------------------------------------------------------------------------
class ConfigObjectsListModel(dvm.ObjectsListModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, thisList, parent):
		super(ConfigObjectsListModel, self).__init__(thisList, parent)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def flags(self, index):
		"""Set flags of each cell"""
		return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/workflows/Lighting/LightsManager_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()