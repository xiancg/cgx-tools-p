# -*- coding: utf-8 -*-
'''
Create render layers and manage render layer templates.

Created on Feb 06, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/

Pending:
-Impossible to set AIAOVFILTER once node has been created.
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets, QtCore
from cgx.gui.Qt import __binding__
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
from cgx.core.JSONManager import JSONManager
import cgx.gui.DataViewModels as dvm


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/workflows/Lighting/RenderLayerCreator'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"
templates_ui = appRootFolder + "/ui/" + "templates.ui"
config_json = appRootFolder + "/cfg/" + "RenderLayerCreator_config.json"
templates_json = appRootFolder + "/cfg/" + "RenderLayerCreator_templates.json"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)
templatesform, templatesbase = uiParser.loadUiType(templates_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
		:type _studioInfo: core.StudioInfo
		:param _projectInfo: ProjectInfo object to manage all project level properties.
		:type _projectInfo: core.ProjectInfo
		:param parent: Main application window as a QMainWindow instance
		:type parent: QMainWindow
		:param appTools: Application Tools library for Camera Exporter
		:type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.initComponents()
			self.initUI()
			self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
		Connect signals.
		'''
		#Edit Menu
		self.importMayaLayers_action.triggered.connect(self.importMayaLayers)
		#Templates Menu
		self.actionManage_templates.triggered.connect(self.templatesManager)
		self.actionSave_template.triggered.connect(self.saveTemplate)
		#Buttons
		self.bg_BTN.clicked.connect(lambda : self.predefRenderLayers("bg"))
		self.character_BTN.clicked.connect(lambda : self.predefRenderLayers("character"))
		self.masks_BTN.clicked.connect(lambda : self.predefRenderLayers("masks"))
		self.volumetrics_BTN.clicked.connect(lambda : self.predefRenderLayers("volumetrics"))
		self.occlusion_BTN.clicked.connect(lambda : self.predefRenderLayers("occlusion"))
		self.rim_BTN.clicked.connect(lambda : self.predefRenderLayers("rim"))
		self.shadows_BTN.clicked.connect(lambda : self.predefRenderLayers("shadows"))
		self.fullFloatAOVs_BTN.clicked.connect(lambda : self.predefRenderLayers("fullFloat"))
		self.fullFloatMV_BTN.clicked.connect(lambda : self.predefRenderLayers("fullFloatMV"))
		self.custom_BTN.clicked.connect(lambda : self.predefRenderLayers("custom"))
		self.createRenderLayers_BTN.clicked.connect(self.createRenderLayers)
		
		self.renderLayers_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.renderLayers_TABLEVIEW.customContextMenuRequested.connect(self.renderLayersTableOptions)
		
	
	def initTool(self):
		'''
		Initializes tool settings.
		'''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Render Layer Creator"
		self.__toolVersion = __version__
		
		self.templatesCfg = JSONManager(templates_json, self)
		self.configFile = JSONManager(config_json, self)
		self.headers = ["name","type","dataType","autoCrop"]
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		className= str(self.__class__.__name__)
		#Pipeline components
		self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo, _department='Comp')
		self.projSeqShotMod = self.Pipeline_GUIModulesFactory.createModule('ProjSeqShot')
		self.projSeqShotMod.pos = [0,30]
		self.customPathComp = pipeComponents.CustomPath_Component(self,self.centralWidget(), self.sInfo)
		self.customPathComp.pos = [0,60]
		self.customPathComp.fileMode = self.customPathComp.browseDialog.FileMode.Directory
		
		#Add Import Maya Layers from current file menu option
		self.importMayaLayers_action = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.importMayaLayers_action.setObjectName("edit_importMayaLayers")
		self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[0], self.importMayaLayers_action)
		self.baseMod.MainMenu_Component.menuEdit.insertSeparator(self.baseMod.MainMenu_Component.edit_reset)
		
		#Add Templates menu
		self.menuTemplate = QtWidgets.QMenu(self.baseMod.MainMenu_Component.menubar)
		self.menuTemplate.setObjectName("menuTemplate")
		self.menuLoad_template = QtWidgets.QMenu(self.menuTemplate)
		self.menuLoad_template.setObjectName("menuLoad_template")
		self.actionSave_template = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.actionSave_template.setObjectName("actionSave_template")
		self.actionManage_templates = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.actionManage_templates.setObjectName("actionManage_templates")
		self.menuTemplate.addAction(self.menuLoad_template.menuAction())
		self.menuTemplate.addAction(self.actionSave_template)
		self.menuTemplate.addSeparator()
		self.menuTemplate.addAction(self.actionManage_templates)
		self.baseMod.MainMenu_Component.menubar.insertAction(self.baseMod.MainMenu_Component.menubar.actions()[1], self.menuTemplate.menuAction())
		
		if __binding__ in ('PySide', 'PyQt4'):
			self.importMayaLayers_action.setText(QtWidgets.QApplication.translate(className, "Import Maya Layers from current file", None, QtWidgets.QApplication.UnicodeUTF8))
			self.menuTemplate.setTitle(QtWidgets.QApplication.translate(className, "Template", None, QtWidgets.QApplication.UnicodeUTF8))
			self.actionSave_template.setText(QtWidgets.QApplication.translate(className, "Save template", None, QtWidgets.QApplication.UnicodeUTF8))
			self.actionManage_templates.setText(QtWidgets.QApplication.translate(className, "Manage templates", None, QtWidgets.QApplication.UnicodeUTF8))
			self.menuLoad_template.setTitle(QtWidgets.QApplication.translate(className, "Load template", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			self.importMayaLayers_action.setText(QtWidgets.QApplication.translate(className, "Import Maya Layers from current file", None))
			self.menuTemplate.setTitle(QtWidgets.QApplication.translate(className, "Template", None))
			self.actionSave_template.setText(QtWidgets.QApplication.translate(className, "Save template", None))
			self.actionManage_templates.setText(QtWidgets.QApplication.translate(className, "Manage templates", None))
			self.menuLoad_template.setTitle(QtWidgets.QApplication.translate(className, "Load template", None))
	
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
		self.appTools.setCurrentProject(self.projSeqShotMod.Project_Component)
		self.appTools.setCurrentSequence(self.projSeqShotMod.Sequence_Component)
		self.appTools.setCurrentShot(self.projSeqShotMod.Shot_Component)
		self.renderLayers_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)

	
	def initUI(self):
		'''
		Initialize GUI components.
		'''
		if mc.getAttr('defaultRenderGlobals.ren') != "arnold":
			msgBox = QtWidgets.QMessageBox(self)
			msgBox.setWindowTitle("Warning!")
			msgBox.setText("This tool will only work with Arnold.")
			msgBox.exec_()
			self.baseMod.quitApp()
		else:
			self.headers = ["name","type","dataType","autoCrop"]
			renderLayersModel = TemplatesDataTableModel([[self.configFile.getValueByProperty("Prefix", False) + "CUSTOM" + self.configFile.getValueByProperty("Suffix", False),"custom","half","True"]], self.headers, self)
			self.renderLayers_TABLEVIEW.setModel(renderLayersModel)
			delegate = TemplatesDelegate(self)
			self.renderLayers_TABLEVIEW.setItemDelegate(delegate)
			self.populateLoadTemplatesMenu()
	
	
	def createRenderLayers(self):
		dataList= self.renderLayers_TABLEVIEW.model().dataList
		tokensStr = "<RenderLayer>/<Scene>/<RenderLayer>"
		seq = str(self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.currentText())
		shot = str(self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentText())
		finalPath= self.buildProjSeqShotPath(seq, shot) + '/' + tokensStr
		if self.customPathComp.customPath_CHKBOX.isChecked():
			finalPath = str(self.customPathComp.customPath_LINEEDIT.text()) + '/' + tokensStr
		for row in dataList:
			rowDict = {}
			i = 0
			for column in self.headers:
				if column == "name":
					rowDict[column] = row[i]
				elif column == "type":
					rowDict[column] = row[i]
				elif column == "dataType":
					rowDict[column] = row[i]
				elif column == "autoCrop":
					rowDict[column] = row[i]
				i += 1
			newRL = mc.createRenderLayer(empty=True, name= rowDict["name"])
			#mc.editRenderLayerAdjustment("defaultRenderGlobals.imageFilePrefix", layer=newRL)
			mc.setAttr("defaultRenderGlobals.imageFilePrefix", finalPath, type="string")

			mc.editRenderLayerAdjustment("defaultArnoldDriver.mergeAOVs", layer=newRL)
			mc.setAttr("defaultArnoldDriver.mergeAOVs", True)
			mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.ignoreAtmosphere", layer=newRL)
			mc.setAttr("defaultArnoldRenderOptions.ignoreAtmosphere", True)
			volumeScatterNode = "aiVolumeScattering"
			if not mc.objExists(volumeScatterNode):
				volumeScatterNode = mc.createNode("aiVolumeScattering", name="aiVolumeScattering")
				mc.connectAttr(volumeScatterNode + ".message", "defaultArnoldRenderOptions.atmosphere",force=True)
			if rowDict["type"] == "bg":
				mc.editRenderLayerGlobals(currentRenderLayer= newRL)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.halfPrecision", layer=newRL)
				mc.setAttr("defaultArnoldDriver.halfPrecision", True)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.autocrop", layer=newRL)
				mc.setAttr("defaultArnoldDriver.autocrop", True)
				self.beautyAOVs(newRL)
			elif rowDict["type"] == "character":
				mc.editRenderLayerGlobals(currentRenderLayer= newRL)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.halfPrecision", layer=newRL)
				mc.setAttr("defaultArnoldDriver.halfPrecision", True)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.autocrop", layer=newRL)
				mc.setAttr("defaultArnoldDriver.autocrop", True)
				self.beautyAOVs(newRL)
			elif rowDict["type"] == "mask":
				mc.editRenderLayerGlobals(currentRenderLayer= newRL)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.halfPrecision", layer=newRL)
				mc.setAttr("defaultArnoldDriver.halfPrecision", True)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.autocrop", layer=newRL)
				mc.setAttr("defaultArnoldDriver.autocrop", True)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIDiffuseSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIDiffuseSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIGlossySamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIGlossySamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIRefractionSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIRefractionSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GISssSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GISssSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GITotalDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GITotalDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIDiffuseDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIDiffuseDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIGlossyDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIGlossyDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIReflectionDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIReflectionDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIRefractionDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIRefractionDepth", 0)	
			elif rowDict["type"] == "volumetric":
				mc.editRenderLayerGlobals(currentRenderLayer= newRL)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.halfPrecision", layer=newRL)
				mc.setAttr("defaultArnoldDriver.halfPrecision", True)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.autocrop", layer=newRL)
				mc.setAttr("defaultArnoldDriver.autocrop", True)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.ignoreAtmosphere", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.ignoreAtmosphere", False)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIDiffuseSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIDiffuseSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIGlossySamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIGlossySamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIRefractionSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIRefractionSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GISssSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GISssSamples", 0)
			elif rowDict["type"] == "occ":
				mc.editRenderLayerGlobals(currentRenderLayer= newRL)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.halfPrecision", layer=newRL)
				mc.setAttr("defaultArnoldDriver.halfPrecision", True)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.autocrop", layer=newRL)
				mc.setAttr("defaultArnoldDriver.autocrop", True)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIDiffuseSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIDiffuseSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIGlossySamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIGlossySamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIRefractionSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIRefractionSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GISssSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GISssSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GITotalDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GITotalDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIDiffuseDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIDiffuseDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIGlossyDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIGlossyDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIReflectionDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIReflectionDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIRefractionDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIRefractionDepth", 0)
			elif rowDict["type"] == "rim":
				mc.editRenderLayerGlobals(currentRenderLayer= newRL)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.halfPrecision", layer=newRL)
				mc.setAttr("defaultArnoldDriver.halfPrecision", True)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.autocrop", layer=newRL)
				mc.setAttr("defaultArnoldDriver.autocrop", True)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIDiffuseSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIDiffuseSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIGlossySamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIGlossySamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIRefractionSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIRefractionSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GISssSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GISssSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GITotalDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GITotalDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIDiffuseDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIDiffuseDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIGlossyDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIGlossyDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIReflectionDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIReflectionDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIRefractionDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIRefractionDepth", 0)	
			elif rowDict["type"] == "shadows":
				mc.editRenderLayerGlobals(currentRenderLayer= newRL)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.halfPrecision", layer=newRL)
				mc.setAttr("defaultArnoldDriver.halfPrecision", True)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.autocrop", layer=newRL)
				mc.setAttr("defaultArnoldDriver.autocrop", True)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIDiffuseSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIDiffuseSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIGlossySamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIGlossySamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIRefractionSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIRefractionSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GISssSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GISssSamples", 0)
			elif rowDict["type"] == "fullFloat":
				mc.editRenderLayerGlobals(currentRenderLayer= newRL)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.halfPrecision", layer=newRL)
				mc.setAttr("defaultArnoldDriver.halfPrecision", False)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.autocrop", layer=newRL)
				mc.setAttr("defaultArnoldDriver.autocrop", False)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.motion_blur_enable", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.motion_blur_enable", True)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIDiffuseSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIDiffuseSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIGlossySamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIGlossySamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIRefractionSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIRefractionSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GISssSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GISssSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GITotalDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GITotalDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIDiffuseDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIDiffuseDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIGlossyDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIGlossyDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIReflectionDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIReflectionDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIRefractionDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIRefractionDepth", 0)	
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.AASamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.AASamples", 1)
				self.utilityAOVs(newRL)
			elif rowDict["type"] == "fullFloatMV":
				mc.editRenderLayerGlobals(currentRenderLayer= newRL)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.halfPrecision", layer=newRL)
				mc.setAttr("defaultArnoldDriver.halfPrecision", False)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.autocrop", layer=newRL)
				mc.setAttr("defaultArnoldDriver.autocrop", False)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.motion_blur_enable", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.motion_blur_enable", True)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.range_type", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.range_type", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIDiffuseSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIDiffuseSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIGlossySamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIGlossySamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIRefractionSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIRefractionSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GISssSamples", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GISssSamples", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GITotalDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GITotalDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIDiffuseDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIDiffuseDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIGlossyDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIGlossyDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIReflectionDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIReflectionDepth", 0)
				mc.editRenderLayerAdjustment("defaultArnoldRenderOptions.GIRefractionDepth", layer=newRL)
				mc.setAttr("defaultArnoldRenderOptions.GIRefractionDepth", 0)
				for cam in mc.ls(type='camera'):
					if mc.getAttr(cam + '.renderable'):
						transform = mc.listRelatives(cam, parent=True, fullPath=True)[0]
						mc.editRenderLayerMembers(newRL, transform)
						mc.setAttr(cam + '.aiShutterEnd', 0)
						mc.setAttr(cam + '.aiShutterStart', 0)
				self.utilityAOVs(newRL, True)
			elif rowDict["type"] == "custom":
				mc.editRenderLayerGlobals(currentRenderLayer= newRL)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.halfPrecision", layer=newRL)
				mc.setAttr("defaultArnoldDriver.halfPrecision", True)
				mc.editRenderLayerAdjustment("defaultArnoldDriver.autocrop", layer=newRL)
				mc.setAttr("defaultArnoldDriver.autocrop", True)
		self.baseMod.statusBar_update("Render layers created.", success=True)
	
	
	def utilityAOVs(self, _thisRL, _mvOnly=False):
		#Create AOV nodes or use existing ones
		if not _mvOnly:
			normalsAOV = "aiAOV_N"
			if not mc.objExists(normalsAOV):
				normalsAOV = mc.createNode("aiAOV", name="aiAOV_N")
				mc.connectAttr("defaultArnoldFilter.message", normalsAOV + ".outputs[0].filter", force=True)
				mc.connectAttr("defaultArnoldDriver.message", normalsAOV + ".outputs[0].driver", force=True)
				mc.connectAttr(normalsAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
			zdepthAOV = "aiAOV_Z"
			if not mc.objExists(zdepthAOV):
				zdepthAOV = mc.createNode("aiAOV", name="aiAOV_Z")
				filterNode = mc.createNode("aiAOVFilter", name= "aiAOVFilter1")
				
				#mc.setAttr(filterNode + ".", 4) NO SE COMO SE LLAMA EL ATRIBUTO PARA SETEAR EL FILTRO
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("You must set the Filter Type on the ZDepth AOV to 'closest' manually.")
				msgBox.exec_()
				
				mc.connectAttr(filterNode + ".message", zdepthAOV + ".outputs[0].filter", force=True)
				mc.connectAttr("defaultArnoldDriver.message", zdepthAOV + ".outputs[0].driver", force=True)
				mc.connectAttr(zdepthAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
			positionAOV = "aiAOV_P"
			if not mc.objExists(positionAOV):
				positionAOV = mc.createNode("aiAOV", name="aiAOV_P")
				mc.connectAttr("defaultArnoldFilter.message", positionAOV + ".outputs[0].filter", force=True)
				mc.connectAttr("defaultArnoldDriver.message", positionAOV + ".outputs[0].driver", force=True)
				mc.connectAttr(positionAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		if _mvOnly:
			motionVectorAOV = "aiAOV_motionVector"
			if not mc.objExists(motionVectorAOV):
				motionVectorAOV = mc.createNode("aiAOV", name="aiAOV_motionVector")
				mc.connectAttr("defaultArnoldFilter.message", motionVectorAOV + ".outputs[0].filter", force=True)
				mc.connectAttr("defaultArnoldDriver.message", motionVectorAOV + ".outputs[0].driver", force=True)
				mc.connectAttr(motionVectorAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		
		
		#Set attributes
		if not _mvOnly:
			mc.setAttr(normalsAOV + ".name", "N", type="string")
			mc.setAttr(normalsAOV + ".type", 7)
			mc.setAttr(normalsAOV + ".enabled", False)
			mc.editRenderLayerAdjustment(normalsAOV + ".enabled", layer= _thisRL)
			mc.setAttr(normalsAOV + ".enabled", True)
			mc.setAttr(zdepthAOV + ".name", "Z", type="string")
			mc.setAttr(zdepthAOV + ".type", 4)
			mc.setAttr(zdepthAOV + ".enabled", False)
			mc.editRenderLayerAdjustment(zdepthAOV + ".enabled", layer= _thisRL)
			mc.setAttr(zdepthAOV + ".enabled", True)
			mc.setAttr(positionAOV + ".name", "P", type="string")
			mc.setAttr(positionAOV + ".type", 8)
			mc.setAttr(positionAOV + ".enabled", False)
			mc.editRenderLayerAdjustment(positionAOV + ".enabled", layer= _thisRL)
			mc.setAttr(positionAOV + ".enabled", True)
		if _mvOnly:
			mc.setAttr(motionVectorAOV + ".name", "motionvector", type="string")
			mc.setAttr(motionVectorAOV + ".type", 5)
			mc.setAttr(motionVectorAOV + ".enabled", False)
			mc.editRenderLayerAdjustment(motionVectorAOV + ".enabled", layer= _thisRL)
			mc.setAttr(motionVectorAOV + ".enabled", True)
			
	
	def beautyAOVs(self, _thisRL):
		#Create AOV nodes or use existing ones
		refractionAOV = "aiAOV_refraction"
		if not mc.objExists(refractionAOV):
			refractionAOV = mc.createNode("aiAOV", name="aiAOV_refraction")
			mc.connectAttr("defaultArnoldFilter.message", refractionAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", refractionAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(refractionAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		indirectDiffuseAOV = "aiAOV_indirect_diffuse"
		if not mc.objExists(indirectDiffuseAOV):
			indirectDiffuseAOV = mc.createNode("aiAOV", name="aiAOV_indirect_diffuse")
			mc.connectAttr("defaultArnoldFilter.message", indirectDiffuseAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", indirectDiffuseAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(indirectDiffuseAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		directSpecularAOV = "aiAOV_direct_specular"
		if not mc.objExists(directSpecularAOV):
			directSpecularAOV = mc.createNode("aiAOV", name="aiAOV_direct_specular")
			mc.connectAttr("defaultArnoldFilter.message", directSpecularAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", directSpecularAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(directSpecularAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		directSpecular2AOV = "aiAOV_direct_specular_2"
		if not mc.objExists(directSpecular2AOV):
			directSpecular2AOV = mc.createNode("aiAOV", name="aiAOV_direct_specular_2")
			mc.connectAttr("defaultArnoldFilter.message", directSpecular2AOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", directSpecular2AOV + ".outputs[0].driver", force=True)
			mc.connectAttr(directSpecular2AOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		sssAOV = "aiAOV_sss"
		if not mc.objExists(sssAOV):
			sssAOV = mc.createNode("aiAOV", name="aiAOV_sss")
			mc.connectAttr("defaultArnoldFilter.message", sssAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", sssAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(sssAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		reflectionAOV = "aiAOV_reflection"
		if not mc.objExists(reflectionAOV):
			reflectionAOV = mc.createNode("aiAOV", name="aiAOV_reflection")
			mc.connectAttr("defaultArnoldFilter.message", reflectionAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", reflectionAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(reflectionAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		emissionAOV = "aiAOV_emission"
		if not mc.objExists(emissionAOV):
			emissionAOV = mc.createNode("aiAOV", name="aiAOV_emission")
			mc.connectAttr("defaultArnoldFilter.message", emissionAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", emissionAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(emissionAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		refractionOpacityAOV = "aiAOV_refraction_opacity"
		if not mc.objExists(refractionOpacityAOV):
			refractionOpacityAOV = mc.createNode("aiAOV", name="aiAOV_refraction_opacity")
			mc.connectAttr("defaultArnoldFilter.message", refractionOpacityAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", refractionOpacityAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(refractionOpacityAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		indirectSpecularAOV = "aiAOV_indirect_specular"
		if not mc.objExists(indirectSpecularAOV):
			indirectSpecularAOV = mc.createNode("aiAOV", name="aiAOV_indirect_specular")
			mc.connectAttr("defaultArnoldFilter.message", indirectSpecularAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", indirectSpecularAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(indirectSpecularAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		indirectSpecular2AOV = "aiAOV_indirect_specular_2"
		if not mc.objExists(indirectSpecular2AOV):
			indirectSpecular2AOV = mc.createNode("aiAOV", name="aiAOV_indirect_specular_2")
			mc.connectAttr("defaultArnoldFilter.message", indirectSpecular2AOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", indirectSpecular2AOV + ".outputs[0].driver", force=True)
			mc.connectAttr(indirectSpecular2AOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		directDiffuseAOV = "aiAOV_direct_diffuse"
		if not mc.objExists(directDiffuseAOV):
			directDiffuseAOV = mc.createNode("aiAOV", name="aiAOV_direct_diffuse")
			mc.connectAttr("defaultArnoldFilter.message", directDiffuseAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", directDiffuseAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(directDiffuseAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		sheenAOV = "aiAOV_sheen"
		if not mc.objExists(sheenAOV):
			sheenAOV = mc.createNode("aiAOV", name="aiAOV_sheen")
			mc.connectAttr("defaultArnoldFilter.message", sheenAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", sheenAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(sheenAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		specularAOV = "aiAOV_specular"
		if not mc.objExists(specularAOV):
			specularAOV = mc.createNode("aiAOV", name="aiAOV_specular")
			mc.connectAttr("defaultArnoldFilter.message", specularAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", specularAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(specularAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		directBacklightAOV = "aiAOV_direct_backlight"
		if not mc.objExists(directBacklightAOV):
			directBacklightAOV = mc.createNode("aiAOV", name="aiAOV_direct_backlight")
			mc.connectAttr("defaultArnoldFilter.message", directBacklightAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", directBacklightAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(directBacklightAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		indirectBacklightAOV = "aiAOV_indirect_backlight"
		if not mc.objExists(indirectBacklightAOV):
			indirectBacklightAOV = mc.createNode("aiAOV", name="aiAOV_indirect_backlight")
			mc.connectAttr("defaultArnoldFilter.message", indirectBacklightAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", indirectBacklightAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(indirectBacklightAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		diffuseColorAOV = "aiAOV_diffuse_color"
		if not mc.objExists(diffuseColorAOV):
			diffuseColorAOV = mc.createNode("aiAOV", name="aiAOV_diffuse_color")
			mc.connectAttr("defaultArnoldFilter.message", diffuseColorAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", diffuseColorAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(diffuseColorAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		cryptoAssetAOV = "aiAOV_crypto_asset"
		if not mc.objExists(cryptoAssetAOV):
			cryptoAssetAOV = mc.createNode("aiAOV", name="aiAOV_crypto_asset")
			mc.connectAttr("defaultArnoldFilter.message", cryptoAssetAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", cryptoAssetAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(cryptoAssetAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		cryptoMaterialAOV = "aiAOV_crypto_material"
		if not mc.objExists(cryptoMaterialAOV):
			cryptoMaterialAOV = mc.createNode("aiAOV", name="aiAOV_crypto_material")
			mc.connectAttr("defaultArnoldFilter.message", cryptoMaterialAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", cryptoMaterialAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(cryptoMaterialAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		cryptoObjectAOV = "aiAOV_crypto_object"
		if not mc.objExists(cryptoObjectAOV):
			cryptoObjectAOV = mc.createNode("aiAOV", name="aiAOV_crypto_object")
			mc.connectAttr("defaultArnoldFilter.message", cryptoObjectAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", cryptoObjectAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(cryptoObjectAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		shadowMatteAOV = "aiAOV_shadow_matte"
		if not mc.objExists(shadowMatteAOV):
			shadowMatteAOV = mc.createNode("aiAOV", name="aiAOV_shadow_matte")
			mc.connectAttr("defaultArnoldFilter.message", shadowMatteAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", shadowMatteAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(shadowMatteAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		singleScatterAOV = "aiAOV_single_scatter"
		if not mc.objExists(singleScatterAOV):
			singleScatterAOV = mc.createNode("aiAOV", name="aiAOV_single_scatter")
			mc.connectAttr("defaultArnoldFilter.message", singleScatterAOV + ".outputs[0].filter", force=True)
			mc.connectAttr("defaultArnoldDriver.message", singleScatterAOV + ".outputs[0].driver", force=True)
			mc.connectAttr(singleScatterAOV + ".message", "defaultArnoldRenderOptions.aovList", nextAvailable=True, force=True)
		
		
		#Set attributes
		mc.setAttr(refractionAOV + ".name", "refraction", type="string")
		mc.setAttr(refractionAOV + ".type", 5)
		mc.setAttr(refractionAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(refractionAOV + ".enabled", layer= _thisRL)
		mc.setAttr(refractionAOV + ".enabled", True)
		mc.setAttr(indirectDiffuseAOV + ".name", "indirect_diffuse", type="string")
		mc.setAttr(indirectDiffuseAOV + ".type", 5)
		mc.setAttr(indirectDiffuseAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(indirectDiffuseAOV + ".enabled", layer= _thisRL)
		mc.setAttr(indirectDiffuseAOV + ".enabled", True)
		mc.setAttr(directSpecularAOV + ".name", "direct_specular", type="string")
		mc.setAttr(directSpecularAOV + ".type", 5)
		mc.setAttr(directSpecularAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(directSpecularAOV + ".enabled", layer= _thisRL)
		mc.setAttr(directSpecularAOV + ".enabled", True)
		mc.setAttr(directSpecular2AOV + ".name", "direct_specular_2", type="string")
		mc.setAttr(directSpecular2AOV + ".type", 5)
		mc.setAttr(directSpecular2AOV + ".enabled", False)
		mc.editRenderLayerAdjustment(directSpecular2AOV + ".enabled", layer= _thisRL)
		mc.setAttr(directSpecular2AOV + ".enabled", True)
		mc.setAttr(sssAOV + ".name", "sss", type="string")
		mc.setAttr(sssAOV + ".type", 5)
		mc.setAttr(sssAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(sssAOV + ".enabled", layer= _thisRL)
		mc.setAttr(sssAOV + ".enabled", True)
		mc.setAttr(reflectionAOV + ".name", "reflection", type="string")
		mc.setAttr(reflectionAOV + ".type", 5)
		mc.setAttr(reflectionAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(reflectionAOV + ".enabled", layer= _thisRL)
		mc.setAttr(reflectionAOV + ".enabled", True)
		mc.setAttr(emissionAOV + ".name", "emission", type="string")
		mc.setAttr(emissionAOV + ".type", 5)
		mc.setAttr(emissionAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(emissionAOV + ".enabled", layer= _thisRL)
		mc.setAttr(emissionAOV + ".enabled", True)
		mc.setAttr(refractionOpacityAOV + ".name", "refraction_opacity", type="string")
		mc.setAttr(refractionOpacityAOV + ".type", 5)
		mc.setAttr(refractionOpacityAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(refractionOpacityAOV + ".enabled", layer= _thisRL)
		mc.setAttr(refractionOpacityAOV + ".enabled", True)
		mc.setAttr(indirectSpecularAOV + ".name", "indirect_specular", type="string")
		mc.setAttr(indirectSpecularAOV + ".type", 5)
		mc.setAttr(indirectSpecularAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(indirectSpecularAOV + ".enabled", layer= _thisRL)
		mc.setAttr(indirectSpecularAOV + ".enabled", True)
		mc.setAttr(indirectSpecular2AOV + ".name", "indirect_specular_2", type="string")
		mc.setAttr(indirectSpecular2AOV + ".type", 5)
		mc.setAttr(indirectSpecular2AOV + ".enabled", False)
		mc.editRenderLayerAdjustment(indirectSpecular2AOV + ".enabled", layer= _thisRL)
		mc.setAttr(indirectSpecular2AOV + ".enabled", True)
		mc.setAttr(directDiffuseAOV + ".name", "direct_diffuse", type="string")
		mc.setAttr(directDiffuseAOV + ".type", 5)
		mc.setAttr(directDiffuseAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(directDiffuseAOV + ".enabled", layer= _thisRL)
		mc.setAttr(directDiffuseAOV + ".enabled", True)
		mc.setAttr(sheenAOV + ".name", "sheen", type="string")
		mc.setAttr(sheenAOV + ".type", 5)
		mc.setAttr(sheenAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(sheenAOV + ".enabled", layer= _thisRL)
		mc.setAttr(sheenAOV + ".enabled", True)
		mc.setAttr(specularAOV + ".name", "specular", type="string")
		mc.setAttr(specularAOV + ".type", 5)
		mc.setAttr(specularAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(specularAOV + ".enabled", layer= _thisRL)
		mc.setAttr(specularAOV + ".enabled", True)
		mc.setAttr(directBacklightAOV + ".name", "direct_backlight", type="string")
		mc.setAttr(directBacklightAOV + ".type", 5)
		mc.setAttr(directBacklightAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(directBacklightAOV + ".enabled", layer= _thisRL)
		mc.setAttr(directBacklightAOV + ".enabled", True)
		mc.setAttr(indirectBacklightAOV + ".name", "indirect_backlight", type="string")
		mc.setAttr(indirectBacklightAOV + ".type", 5)
		mc.setAttr(indirectBacklightAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(indirectBacklightAOV + ".enabled", layer= _thisRL)
		mc.setAttr(indirectBacklightAOV + ".enabled", True)
		mc.setAttr(diffuseColorAOV + ".name", "diffuse_color", type="string")
		mc.setAttr(diffuseColorAOV + ".type", 5)
		mc.setAttr(diffuseColorAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(diffuseColorAOV + ".enabled", layer= _thisRL)
		mc.setAttr(diffuseColorAOV + ".enabled", True)
		mc.setAttr(cryptoAssetAOV + ".name", "crypto_asset", type="string")
		mc.setAttr(cryptoAssetAOV + ".type", 5)
		mc.setAttr(cryptoAssetAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(cryptoAssetAOV + ".enabled", layer= _thisRL)
		mc.setAttr(cryptoAssetAOV + ".enabled", True)
		mc.setAttr(cryptoMaterialAOV + ".name", "crypto_material", type="string")
		mc.setAttr(cryptoMaterialAOV + ".type", 5)
		mc.setAttr(cryptoMaterialAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(cryptoMaterialAOV + ".enabled", layer= _thisRL)
		mc.setAttr(cryptoMaterialAOV + ".enabled", True)
		mc.setAttr(cryptoObjectAOV + ".name", "crypto_object", type="string")
		mc.setAttr(cryptoObjectAOV + ".type", 5)
		mc.setAttr(cryptoObjectAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(cryptoObjectAOV + ".enabled", layer= _thisRL)
		mc.setAttr(cryptoObjectAOV + ".enabled", True)
		mc.setAttr(shadowMatteAOV + ".name", "shadow_matte", type="string")
		mc.setAttr(shadowMatteAOV + ".type", 5)
		mc.setAttr(shadowMatteAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(shadowMatteAOV + ".enabled", layer= _thisRL)
		mc.setAttr(shadowMatteAOV + ".enabled", True)
		mc.setAttr(singleScatterAOV + ".name", "single_scatter", type="string")
		mc.setAttr(singleScatterAOV + ".type", 5)
		mc.setAttr(singleScatterAOV + ".enabled", False)
		mc.editRenderLayerAdjustment(singleScatterAOV + ".enabled", layer= _thisRL)
		mc.setAttr(singleScatterAOV + ".enabled", True)
	
	
	def populateLoadTemplatesMenu(self):
		jsonObj= self.templatesCfg.jsonObj
		templatesDict = jsonObj
		self.menuLoad_template.clear()
		for each in templatesDict.keys():
			thisTemplateQ = self.menuLoad_template.addAction(each)
			thisTemplateQ.triggered.connect(lambda thisKey=each: self.loadTemplate(thisKey))
	
	
	def loadTemplate(self, _template):
		jsonObj= self.templatesCfg.jsonObj
		templatesDict = jsonObj
		dataList = []
		thisTemplateDict = templatesDict[_template]
		for key, value in thisTemplateDict.iteritems():
			rowData = []
			for column in self.headers:
				if column == "name":
					rowData.append(key)
				elif column == "type":
					rowData.append(value[column])
				elif column == "dataType":
					rowData.append(value[column])
				elif column == "autoCrop":
					rowData.append(value[column])
			dataList.append(rowData)
		renderLayersModel = TemplatesDataTableModel(dataList, self.headers, self)
		self.renderLayers_TABLEVIEW.setModel(renderLayersModel)
		delegate = TemplatesDelegate(self)
		self.renderLayers_TABLEVIEW.setItemDelegate(delegate)
		
	
	def saveTemplate(self):
		"""Append changes to JSON file"""
		jsonObj = self.templatesCfg.jsonObj
		currentNames = jsonObj.keys()
		inputDialog = QtWidgets.QInputDialog(self)
		text, ok = inputDialog.getText(self, 'Template', 'New template name:')
		if ok:
			if text in currentNames:
				msgBox = QtWidgets.QMessageBox(inputDialog)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("Name is already taken.")
				msgBox.exec_()
			else:
				newTemplateName = text
				tableDataList = self.renderLayers_TABLEVIEW.model().dataList
				templateDict = {}
				for row in tableDataList:
					rowDict = {}
					i = 0
					for column in self.headers:
						if column == "name":
							templateDict[row[i]] = rowDict
						elif column == "type":
							rowDict[column] = row[i]
						elif column == "dataType":
							rowDict[column] = row[i]
						elif column == "autoCrop":
							rowDict[column] = row[i]
						i += 1
				jsonObj[newTemplateName] = templateDict
				self.templatesCfg.writeJson(jsonObj, self.templatesCfg.fileName)
				
				self.templatesCfg = JSONManager(templates_json, self)
				self.populateLoadTemplatesMenu()
	
	
	def importMayaLayers(self):
		allLayers  = mc.ls(type="renderLayer")
		for lyr in allLayers:
			if not mc.referenceQuery(lyr, isNodeReferenced= True):
				if lyr != "defaultRenderLayer":
					mc.editRenderLayerGlobals(currentRenderLayer= lyr)
					dataTypeVar = mc.getAttr("defaultArnoldDriver.halfPrecision")
					autoCropVar = mc.getAttr("defaultArnoldDriver.autocrop")
					if dataTypeVar == True:
						dataTypeVar = "half"
					else:
						dataTypeVar = "float"
					if autoCropVar == True:
						autoCropVar = "True"
					else:
						autoCropVar = "False"
					self.renderLayers_TABLEVIEW.model().insertRows(self.renderLayers_TABLEVIEW.model().rowCount(), 1, [lyr,"custom",dataTypeVar,autoCropVar])
	
	
	def predefRenderLayers(self, _btn):
		if _btn == "bg":
			self.renderLayers_TABLEVIEW.model().insertRows(self.renderLayers_TABLEVIEW.model().rowCount(), 1, [self.configFile.getValueByProperty("Prefix", False) + "BG" + self.configFile.getValueByProperty("Suffix", False),"bg","half","True"])
		elif _btn == "character":
			self.renderLayers_TABLEVIEW.model().insertRows(self.renderLayers_TABLEVIEW.model().rowCount(), 1, [self.configFile.getValueByProperty("Prefix", False) + "CHAR" + self.configFile.getValueByProperty("Suffix", False),"character","half","True"])
		elif _btn == "masks":
			self.renderLayers_TABLEVIEW.model().insertRows(self.renderLayers_TABLEVIEW.model().rowCount(), 1, [self.configFile.getValueByProperty("Prefix", False) + "MASK" + self.configFile.getValueByProperty("Suffix", False),"mask","half","True"])
		elif _btn == "volumetrics":
			self.renderLayers_TABLEVIEW.model().insertRows(self.renderLayers_TABLEVIEW.model().rowCount(), 1, [self.configFile.getValueByProperty("Prefix", False) + "VOL" + self.configFile.getValueByProperty("Suffix", False),"volumetric","half","True"])
		elif _btn == "occlusion":
			self.renderLayers_TABLEVIEW.model().insertRows(self.renderLayers_TABLEVIEW.model().rowCount(), 1, [self.configFile.getValueByProperty("Prefix", False) + "OCC" + self.configFile.getValueByProperty("Suffix", False),"occ","half","True"])
		elif _btn == "rim":
			self.renderLayers_TABLEVIEW.model().insertRows(self.renderLayers_TABLEVIEW.model().rowCount(), 1, [self.configFile.getValueByProperty("Prefix", False) + "RIM" + self.configFile.getValueByProperty("Suffix", False),"rim","half","True"])
		elif _btn == "shadows":
			self.renderLayers_TABLEVIEW.model().insertRows(self.renderLayers_TABLEVIEW.model().rowCount(), 1, [self.configFile.getValueByProperty("Prefix", False) + "SHDW" + self.configFile.getValueByProperty("Suffix", False),"shadows","half","True"])
		elif _btn == "fullFloat":
			self.renderLayers_TABLEVIEW.model().insertRows(self.renderLayers_TABLEVIEW.model().rowCount(), 1, [self.configFile.getValueByProperty("Prefix", False) + "FULLFLOAT" + self.configFile.getValueByProperty("Suffix", False),"fullFloat","float","False"])
		elif _btn == "fullFloatMV":
			self.renderLayers_TABLEVIEW.model().insertRows(self.renderLayers_TABLEVIEW.model().rowCount(), 1, [self.configFile.getValueByProperty("Prefix", False) + "FULLFLOAT_MV" + self.configFile.getValueByProperty("Suffix", False),"fullFloatMV","float","False"])
		elif _btn == "custom":
			self.renderLayers_TABLEVIEW.model().insertRows(self.renderLayers_TABLEVIEW.model().rowCount(), 1, [self.configFile.getValueByProperty("Prefix", False) + "CUSTOM" + self.configFile.getValueByProperty("Suffix", False),"custom","half","True"])
	
	
	
	def templatesManager(self):
		templatesDialog = Templates_GUI(self)
		returnDialog = templatesDialog.exec_()
		if returnDialog == 1:
			self.resetTool()
			
	
	def buildProjSeqShotPath(self, _seq, _shot):
		propertiesDict = {"abstractFolderType":"branchfolder",
						"concreteFolderType":"framesType_branchfolder",
						"folderFunction":"wip_content",
						"folderConcreteFunction":"CGRenders",
						"placeholder":False}
		
		rendersCGNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)[0]
		if rendersCGNode != None:
			seqDict={"name":"$SEQ_##",
					"abstractFolderType":"subfolder",
					"concreteFolderType":"sequence_subfolder",
					"folderFunction":"grouping",
					"folderConcreteFunction":"",
					"placeholder":True}
			shotDict={"name":"$s_###",
					"abstractFolderType":"subfolder",
					"concreteFolderType":"shot_subfolder",
					"folderFunction":"grouping",
					"folderConcreteFunction":"",
					"placeholder":True}
			pathList = self.pInfo.rebuildPath(rendersCGNode, {_seq:seqDict, _shot:shotDict})
			pathList.append(rendersCGNode.name)
			
			#Build path string
			server = self.sInfo.getProjectsFrom(_department='Comp')
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
			
			return pathStr
		else:
			return None
			
	
	##--------------------------------------------------------------------------------------------
	##Methods for render layers table
	##--------------------------------------------------------------------------------------------
	def renderLayersTableOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.renderLayers_TABLEVIEW)
		addRenderLayerQ = menu.addAction("Add render layer")
		deleteRenderLayerQ = menu.addAction("Delete render layer")
		menu.popup(self.renderLayers_TABLEVIEW.mapToGlobal(pos))
		
		addRenderLayerQ.triggered.connect(self.addRenderLayer)
		deleteRenderLayerQ.triggered.connect(self.deleteRenderLayer)
	
	
	def addRenderLayer(self):
		self.renderLayers_TABLEVIEW.model().insertRows(self.renderLayers_TABLEVIEW.model().rowCount(), 1, [self.configFile.getValueByProperty("Prefix", False) + "CUSTOM" + self.configFile.getValueByProperty("Suffix", False),"custom","half","True"])
	
	
	def deleteRenderLayer(self):
		if self.renderLayers_TABLEVIEW.model().rowCount() == 1:
			self.baseMod.statusBar_update("There has to be at least one render layer in the table.", alert=True)
		else:
			selected = self.renderLayers_TABLEVIEW.selectedIndexes()
			selRowsDirty = []
			for item in selected:
				thisRow = item.row()
				selRowsDirty.append(thisRow)
			selSet = set(selRowsDirty)
			selRows = list(selSet)
			self.renderLayers_TABLEVIEW.model().removeRows(selRows, len(selRows))
	
	
##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
		:type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
		:type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
		:type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		
		self.configFile = JSONManager(config_json, self)
		self.suffix = self.configFile.getValueByProperty("Suffix", False)
		self.prefix = self.configFile.getValueByProperty("Prefix", False)
		
		self.initUI()
		self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		#SIGNALS
		self.cancel_BTN.clicked.connect(self.cancelAll)
		self.apply_BTN.clicked.connect(self.applyConfig)
		

	def initUI(self):
		"""Read JSON config file"""
		self.prefix_LINEEDIT.setText(self.prefix)
		self.suffix_LINEEDIT.setText(self.suffix)
		

	def applyConfig(self):
		"""Apply changes to JSON file"""
		newSuffix = self.suffix_LINEEDIT.text()
		newPrefix = self.prefix_LINEEDIT.text()
		
		jsonObj = self.configFile.jsonObj
		jsonObj["Suffix"] = newSuffix
		jsonObj["Prefix"] = newPrefix
		self.configFile.writeJson(jsonObj, self.configFile.fileName)
		
		self.done(1)
	

	def cancelAll(self):
		"""Method to close the UI"""
		
		self.done(0)


##--------------------------------------------------------------------------------------------
##Create templates window
##--------------------------------------------------------------------------------------------
class Templates_GUI(templatesform, templatesbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		super(Templates_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.configFile = JSONManager(templates_json, self)
		self.modelsList = []
		self.headers = ["name","type","dataType","autoCrop"]
		
		self.setConnections()
		self.initUI()
		
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		#SIGNALS
		self.cancel_BTN.clicked.connect(self.cancelAll)
		self.save_BTN.clicked.connect(self.saveTemplates)
		self.templates_LISTVIEW.clicked.connect(self.populateTemplate)
		
		self.templates_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.templates_LISTVIEW.customContextMenuRequested.connect(self.templateNamesOptions)
		self.templates_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.templates_TABLEVIEW.customContextMenuRequested.connect(self.templatesTableOptions)
	
	
	def initUI(self):
		"""Read JSON config file"""
		
		templatesNamesList = self.configFile.jsonObj.keys()
		templatesNamesModel = TemplatesObjectsListModel(templatesNamesList, self)
		self.templates_LISTVIEW.setModel(templatesNamesModel)
		self.templates_LISTVIEW.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
		self.templates_LISTVIEW.setCurrentIndex(self.templates_LISTVIEW.model().index(0,0))
		
		#Init models
		self.initModels()
		self.populateTemplate()
		
	
	def initModels(self):
		'''Create a model for each item index (name independent) in the listview and store them in a list'''

		templateNames = self.templates_LISTVIEW.model().dataList
		self.templates_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)

		for template in templateNames:
			dataList = []
			for key, value in self.configFile.jsonObj[template].iteritems():
				rowData = []
				for column in self.headers:
					if column == "name":
						rowData.append(key)
					elif column == "type":
						rowData.append(value[column])
					elif column == "dataType":
						rowData.append(value[column])
					elif column == "autoCrop":
						rowData.append(value[column])
				dataList.append(rowData)
			newModel = TemplatesDataTableModel(dataList, self.headers, self)
			self.modelsList.append(newModel)
		
	
	def populateTemplate(self):
		selectedName = self.templates_LISTVIEW.currentIndex().row()
		thisModel = self.modelsList[selectedName]
		self.templates_TABLEVIEW.setModel(thisModel)
		delegate = TemplatesDelegate(self)
		self.templates_TABLEVIEW.setItemDelegate(delegate)
		
			
	def saveTemplates(self):
		"""Apply changes to JSON file"""
		jsonObj = {}
		templateNames = self.templates_LISTVIEW.model().dataList
		for each in range(len(templateNames)):
			tableDataList = self.modelsList[each].dataList
			templateDict = {}
			for row in tableDataList:
				rowDict = {}
				i = 0
				for column in self.headers:
					if column == "name":
						templateDict[row[i]] = rowDict
					elif column == "type":
						rowDict[column] = row[i]
					elif column == "dataType":
						rowDict[column] = row[i]
					elif column == "autoCrop":
						rowDict[column] = row[i]
					i += 1
			jsonObj[templateNames[each]] = templateDict
		self.configFile.writeJson(jsonObj, self.configFile.fileName)
		
		self.done(1)
	
	
	def cancelAll(self):
		"""Method to close the UI"""
		
		self.done(0)
		
	
	##--------------------------------------------------------------------------------------------
	##Methods for template names
	##--------------------------------------------------------------------------------------------
	def templateNamesOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.templates_LISTVIEW)
		addTemplateQ = menu.addAction("Add template")
		deleteTemplateQ = menu.addAction("Delete template")
		menu.popup(self.templates_LISTVIEW.mapToGlobal(pos))
		
		addTemplateQ.triggered.connect(self.addTemplate)
		deleteTemplateQ.triggered.connect(self.deleteTemplate)
	
	
	def addTemplate(self):
		self.templates_LISTVIEW.model().insertRows(self.templates_LISTVIEW.model().rowCount(), ["New template"])
		newModel = TemplatesDataTableModel([["RND_custom","custom","half","True"]], self.headers, self)
		self.modelsList.append(newModel)
		self.populateTemplate()
	
	
	def deleteTemplate(self):
		if len(self.templates_LISTVIEW.model().dataList) == 1:
			msgBox = QtWidgets.QMessageBox(self)
			msgBox.setWindowTitle("Warning!")
			msgBox.setText("There has to be at least one template in the list.")
			msgBox.exec_()
		else:
			selectedIndex = self.templates_LISTVIEW.currentIndex().row()
			self.templates_LISTVIEW.model().removeRows([selectedIndex], 1)
			self.modelsList.pop(selectedIndex)
			self.populateTemplate()
		
	
	##--------------------------------------------------------------------------------------------
	##Methods for templates table
	##--------------------------------------------------------------------------------------------
	def templatesTableOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.templates_TABLEVIEW)
		addRenderLayerQ = menu.addAction("Add render layer")
		deleteRenderLayerQ = menu.addAction("Delete render layer")
		menu.popup(self.templates_TABLEVIEW.mapToGlobal(pos))
		
		addRenderLayerQ.triggered.connect(self.addRenderLayer)
		deleteRenderLayerQ.triggered.connect(self.deleteRenderLayer)
	
	
	def addRenderLayer(self):
		selectedIndex = self.templates_LISTVIEW.currentIndex().row()
		thisModel = self.modelsList[selectedIndex]
		thisModel.insertRows(thisModel.rowCount(QtCore.QModelIndex()), 1, ["RND_custom","custom","half","True"])
	
	
	def deleteRenderLayer(self):
		selectedIndex = self.templates_LISTVIEW.currentIndex().row()
		thisModel = self.modelsList[selectedIndex]
		if thisModel.rowCount() == 1:
			msgBox = QtWidgets.QMessageBox(self)
			msgBox.setWindowTitle("Warning!")
			msgBox.setText("There has to be at least one render layer in the table.")
			msgBox.exec_()
		else:
			selected = self.templates_TABLEVIEW.selectedIndexes()
			selRowsDirty = []
			for item in selected:
				thisRow = item.row()
				selRowsDirty.append(thisRow)
			selSet = set(selRowsDirty)
			selRows = list(selSet)
			thisModel.removeRows(selRows, len(selRows))


##--------------------------------------------------------------------------------------------
##Class: Objects lists model
##--------------------------------------------------------------------------------------------
class TemplatesObjectsListModel(dvm.ObjectsListModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, thisList, parent):
		super(TemplatesObjectsListModel, self).__init__(thisList, parent)
		

	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def flags(self, index):
		"""Set flags of each cell"""
		return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable


##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for templates table
##--------------------------------------------------------------------------------------------
class TemplatesDataTableModel(dvm.DataTableModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, dataList, headers, parent):
		super(TemplatesDataTableModel, self).__init__(dataList, headers, parent)


	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def flags(self, index):
		"""Set flags of each cell"""
		if not index.isValid():
			return QtCore.Qt.NoItemFlags
		else:
			return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable


##--------------------------------------------------------------------------------------------
##Class: Render Layer Types Delegate
##--------------------------------------------------------------------------------------------
class TemplatesDelegate(QtWidgets.QItemDelegate):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent=None):
		super(TemplatesDelegate, self).__init__(parent)
		
			
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def createEditor(self, parent, option, index):
		modelHeaders = index.model().headers
		if modelHeaders[index.column()] == "type":
			combobox = QtWidgets.QComboBox(parent)
			dataList = ["character","bg","mask","volumetric","shadows","occ","rim","custom","fullFloat","fullFloatMV"]
			combobox.addItems(sorted(dataList))
			combobox.setEditable(False)
			return combobox
		elif modelHeaders[index.column()] == "dataType":
			combobox = QtWidgets.QComboBox(parent)
			dataList = ["half","float"]
			combobox.addItems(sorted(dataList))
			combobox.setEditable(False)
			return combobox
		elif modelHeaders[index.column()] == "autoCrop":
			combobox = QtWidgets.QComboBox(parent)
			dataList = ["True","False"]
			combobox.addItems(sorted(dataList))
			combobox.setEditable(False)
			return combobox
		else:
			return QtWidgets.QItemDelegate.createEditor(self, parent, option,index)
		
		
	def setEditorData(self, editor, index):
		modelHeaders = index.model().headers
		text = index.model().data(index, QtCore.Qt.DisplayRole)
		if modelHeaders[index.column()] == "type":
			i = editor.findText(text)
			if i == -1:
				i = 0
			editor.setCurrentIndex(i)
		elif modelHeaders[index.column()] == "dataType":
			i = editor.findText(text)
			if i == -1:
				i = 0
			editor.setCurrentIndex(i)
		elif modelHeaders[index.column()] == "autoCrop":
			i = editor.findText(text)
			if i == -1:
				i = 0
			editor.setCurrentIndex(i)
		else:
			QtWidgets.QItemDelegate.setEditorData(self, editor, index)
	
	
	def setModelData(self, editor, model, index):
		modelHeaders = index.model().headers
		if modelHeaders[index.column()] == "type":
			model.setData(index, editor.currentText())
		elif modelHeaders[index.column()] == "dataType":
			model.setData(index, editor.currentText())
		elif modelHeaders[index.column()] == "autoCrop":
			model.setData(index, editor.currentText())
		else:
			QtWidgets.QItemDelegate.setModelData(self, editor, model, index)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/workflows/Lighting/RenderLayerCreator_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()