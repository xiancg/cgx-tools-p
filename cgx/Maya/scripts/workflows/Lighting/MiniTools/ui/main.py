# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:\CGXtools\cgx\Maya\scripts\workflows\Lighting\MiniTools\ui/main.ui'
#
# Created: Tue Aug 01 16:08:28 2017
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_miniLightingTools_MW(object):
    def setupUi(self, miniLightingTools_MW):
        miniLightingTools_MW.setObjectName("miniLightingTools_MW")
        miniLightingTools_MW.resize(162, 242)
        miniLightingTools_MW.setMinimumSize(QtCore.QSize(162, 242))
        miniLightingTools_MW.setMaximumSize(QtCore.QSize(162, 242))
        self.centralwidget = QtGui.QWidget(miniLightingTools_MW)
        self.centralwidget.setObjectName("centralwidget")
        self.lookThru_BTN = QtGui.QPushButton(self.centralwidget)
        self.lookThru_BTN.setGeometry(QtCore.QRect(110, 10, 41, 41))
        self.lookThru_BTN.setText("")
        self.lookThru_BTN.setObjectName("lookThru_BTN")
        self.toogleLights_BTN = QtGui.QPushButton(self.centralwidget)
        self.toogleLights_BTN.setGeometry(QtCore.QRect(10, 10, 41, 41))
        self.toogleLights_BTN.setText("")
        self.toogleLights_BTN.setObjectName("toogleLights_BTN")
        self.saveImgs_BTN = QtGui.QPushButton(self.centralwidget)
        self.saveImgs_BTN.setGeometry(QtCore.QRect(110, 110, 41, 41))
        self.saveImgs_BTN.setText("")
        self.saveImgs_BTN.setObjectName("saveImgs_BTN")
        self.toogleSubdivs_BTN = QtGui.QPushButton(self.centralwidget)
        self.toogleSubdivs_BTN.setGeometry(QtCore.QRect(10, 110, 41, 41))
        self.toogleSubdivs_BTN.setText("")
        self.toogleSubdivs_BTN.setObjectName("toogleSubdivs_BTN")
        self.renderSubdivs_BTN = QtGui.QPushButton(self.centralwidget)
        self.renderSubdivs_BTN.setGeometry(QtCore.QRect(60, 110, 41, 41))
        self.renderSubdivs_BTN.setText("")
        self.renderSubdivs_BTN.setObjectName("renderSubdivs_BTN")
        self.cleanUpCams_BTN = QtGui.QPushButton(self.centralwidget)
        self.cleanUpCams_BTN.setGeometry(QtCore.QRect(10, 160, 41, 41))
        self.cleanUpCams_BTN.setText("")
        self.cleanUpCams_BTN.setObjectName("cleanUpCams_BTN")
        self.alignLight_BTN = QtGui.QPushButton(self.centralwidget)
        self.alignLight_BTN.setGeometry(QtCore.QRect(10, 60, 41, 41))
        self.alignLight_BTN.setText("")
        self.alignLight_BTN.setObjectName("alignLight_BTN")
        self.aimLight_BTN = QtGui.QPushButton(self.centralwidget)
        self.aimLight_BTN.setGeometry(QtCore.QRect(60, 60, 41, 41))
        self.aimLight_BTN.setText("")
        self.aimLight_BTN.setObjectName("aimLight_BTN")
        self.simpleIsolate_BTN = QtGui.QPushButton(self.centralwidget)
        self.simpleIsolate_BTN.setGeometry(QtCore.QRect(60, 10, 41, 41))
        self.simpleIsolate_BTN.setText("")
        self.simpleIsolate_BTN.setObjectName("simpleIsolate_BTN")
        self.specularConstrain_BTN = QtGui.QPushButton(self.centralwidget)
        self.specularConstrain_BTN.setGeometry(QtCore.QRect(110, 60, 41, 41))
        self.specularConstrain_BTN.setText("")
        self.specularConstrain_BTN.setObjectName("specularConstrain_BTN")
        miniLightingTools_MW.setCentralWidget(self.centralwidget)
        self.actionHelp = QtGui.QAction(miniLightingTools_MW)
        self.actionHelp.setObjectName("actionHelp")
        self.actionAbout = QtGui.QAction(miniLightingTools_MW)
        self.actionAbout.setObjectName("actionAbout")
        self.actionReset = QtGui.QAction(miniLightingTools_MW)
        self.actionReset.setObjectName("actionReset")
        self.actionConfiguration = QtGui.QAction(miniLightingTools_MW)
        self.actionConfiguration.setObjectName("actionConfiguration")

        self.retranslateUi(miniLightingTools_MW)
        QtCore.QMetaObject.connectSlotsByName(miniLightingTools_MW)

    def retranslateUi(self, miniLightingTools_MW):
        miniLightingTools_MW.setWindowTitle(QtGui.QApplication.translate("miniLightingTools_MW", "Mini Lighting Tools", None, QtGui.QApplication.UnicodeUTF8))
        self.lookThru_BTN.setToolTip(QtGui.QApplication.translate("miniLightingTools_MW", "Look Thru Lights", None, QtGui.QApplication.UnicodeUTF8))
        self.toogleLights_BTN.setToolTip(QtGui.QApplication.translate("miniLightingTools_MW", "Toggle Isolate Lights", None, QtGui.QApplication.UnicodeUTF8))
        self.saveImgs_BTN.setToolTip(QtGui.QApplication.translate("miniLightingTools_MW", "Save Render View Images", None, QtGui.QApplication.UnicodeUTF8))
        self.toogleSubdivs_BTN.setToolTip(QtGui.QApplication.translate("miniLightingTools_MW", "Toggle Arnold Subdivs", None, QtGui.QApplication.UnicodeUTF8))
        self.renderSubdivs_BTN.setToolTip(QtGui.QApplication.translate("miniLightingTools_MW", "Set render subdivs", None, QtGui.QApplication.UnicodeUTF8))
        self.cleanUpCams_BTN.setToolTip(QtGui.QApplication.translate("miniLightingTools_MW", "Clean up light cams", None, QtGui.QApplication.UnicodeUTF8))
        self.alignLight_BTN.setToolTip(QtGui.QApplication.translate("miniLightingTools_MW", "Align: Select lights first, target last", None, QtGui.QApplication.UnicodeUTF8))
        self.aimLight_BTN.setToolTip(QtGui.QApplication.translate("miniLightingTools_MW", "Aim: Select lights first, target last", None, QtGui.QApplication.UnicodeUTF8))
        self.simpleIsolate_BTN.setToolTip(QtGui.QApplication.translate("miniLightingTools_MW", "Aim: Select lights first, target last", None, QtGui.QApplication.UnicodeUTF8))
        self.specularConstrain_BTN.setToolTip(QtGui.QApplication.translate("miniLightingTools_MW", "Specular: Select vertex", None, QtGui.QApplication.UnicodeUTF8))
        self.actionHelp.setText(QtGui.QApplication.translate("miniLightingTools_MW", "Help", None, QtGui.QApplication.UnicodeUTF8))
        self.actionAbout.setText(QtGui.QApplication.translate("miniLightingTools_MW", "About", None, QtGui.QApplication.UnicodeUTF8))
        self.actionReset.setText(QtGui.QApplication.translate("miniLightingTools_MW", "Reset", None, QtGui.QApplication.UnicodeUTF8))
        self.actionConfiguration.setText(QtGui.QApplication.translate("miniLightingTools_MW", "Configuration", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    miniLightingTools_MW = QtGui.QMainWindow()
    ui = Ui_miniLightingTools_MW()
    ui.setupUi(miniLightingTools_MW)
    miniLightingTools_MW.show()
    sys.exit(app.exec_())

