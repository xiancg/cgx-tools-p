# -*- coding: utf-8 -*-
'''
Lighting tools collection

Created on Mar 28, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
from cgx.gui.Qt import QtWidgets, QtCore, QtGui
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.Maya.scripts.workflows.Lighting.MiniTools.MiniTools_icons
from cgx.core.JSONManager import JSONManager


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.5.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/workflows/Lighting/MiniTools'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"
config_json = appRootFolder + "/cfg/" + "MiniTools_config.json"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		self.setupBtns()
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.initComponents()
			self.initUI()
			self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setupBtns(self):
		self.lookThru_BTN = MiniTools_BTN(self.centralwidget)
		self.lookThru_BTN.setGeometry(QtCore.QRect(110, 10, 41, 41))
		self.lookThru_BTN.setText("")
		self.lookThru_BTN.setObjectName("lookThru_BTN")
		self.toogleLights_BTN = MiniTools_BTN(self.centralwidget)
		self.toogleLights_BTN.setGeometry(QtCore.QRect(10, 10, 41, 41))
		self.toogleLights_BTN.setText("")
		self.toogleLights_BTN.setObjectName("toogleLights_BTN")
		self.saveImgs_BTN = MiniTools_BTN(self.centralwidget)
		self.saveImgs_BTN.setGeometry(QtCore.QRect(110, 110, 41, 41))
		self.saveImgs_BTN.setText("")
		self.saveImgs_BTN.setObjectName("saveImgs_BTN")
		self.toogleSubdivs_BTN = MiniTools_BTN(self.centralwidget)
		self.toogleSubdivs_BTN.setGeometry(QtCore.QRect(10, 110, 41, 41))
		self.toogleSubdivs_BTN.setText("")
		self.toogleSubdivs_BTN.setObjectName("toogleSubdivs_BTN")
		self.renderSubdivs_BTN = MiniTools_BTN(self.centralwidget)
		self.renderSubdivs_BTN.setGeometry(QtCore.QRect(60, 110, 41, 41))
		self.renderSubdivs_BTN.setText("")
		self.renderSubdivs_BTN.setObjectName("renderSubdivs_BTN")
		self.cleanUpCams_BTN = MiniTools_BTN(self.centralwidget)
		self.cleanUpCams_BTN.setGeometry(QtCore.QRect(10, 160, 41, 41))
		self.cleanUpCams_BTN.setText("")
		self.cleanUpCams_BTN.setObjectName("cleanUpCams_BTN")
		self.alignLight_BTN = MiniTools_BTN(self.centralwidget)
		self.alignLight_BTN.setGeometry(QtCore.QRect(10, 60, 41, 41))
		self.alignLight_BTN.setText("")
		self.alignLight_BTN.setObjectName("alignLight_BTN")
		self.aimLight_BTN = MiniTools_BTN(self.centralwidget)
		self.aimLight_BTN.setGeometry(QtCore.QRect(60, 60, 41, 41))
		self.aimLight_BTN.setText("")
		self.aimLight_BTN.setObjectName("aimLight_BTN")
		self.simpleIsolate_BTN = MiniTools_BTN(self.centralwidget)
		self.simpleIsolate_BTN.setGeometry(QtCore.QRect(60, 10, 41, 41))
		self.simpleIsolate_BTN.setText("")
		self.simpleIsolate_BTN.setObjectName("simpleIsolate_BTN")
		self.specularConstrain_BTN = MiniTools_BTN(self.centralwidget)
		self.specularConstrain_BTN.setGeometry(QtCore.QRect(110, 60, 41, 41))
		self.specularConstrain_BTN.setText("")
		self.specularConstrain_BTN.setObjectName("specularConstrain_BTN")
	
	
	def setConnections(self):
		'''
        Connect signals.
        '''
		#SIGNALS
		self.toogleLights_BTN.clicked.connect(self.appTools.isolateLights)
		self.lookThru_BTN.clicked.connect(self.appTools.lookThruLight)
		self.saveImgs_BTN.clicked.connect(self.appTools.saveRenderViewImgs)
		self.cleanUpCams_BTN.clicked.connect(self.appTools.cleanUpCams)
		self.renderSubdivs_BTN.clicked.connect(self.appTools.restoreArnoldSubdivs)
		self.toogleSubdivs_BTN.clicked.connect(self.appTools.toggleArnoldSubdivs)
		self.alignLight_BTN.clicked.connect(self.appTools.alignLightToObject)
		self.aimLight_BTN.clicked.connect(self.appTools.aimLightToObject)
		self.simpleIsolate_BTN.clicked.connect(self.appTools.simpleIsolateLights)
		self.specularConstrain_BTN.clicked.connect(self.appTools.specularConstrain)
		
		#ICONS
		self.toogleLights_BTN.setIcon(QtGui.QIcon(":/toggleIsolateLights.png"))
		self.toogleLights_BTN.setIconSize(QtCore.QSize(38,38))
		self.lookThru_BTN.setIcon(QtGui.QIcon(":/lookThru.png"))
		self.lookThru_BTN.setIconSize(QtCore.QSize(38,38))
		self.saveImgs_BTN.setIcon(QtGui.QIcon(":/saveRVImages.png"))
		self.saveImgs_BTN.setIconSize(QtCore.QSize(38,38))
		self.cleanUpCams_BTN.setIcon(QtGui.QIcon(":/cleanUpCams.png"))
		self.cleanUpCams_BTN.setIconSize(QtCore.QSize(38,38))
		self.renderSubdivs_BTN.setIcon(QtGui.QIcon(":/renderSubdiv.png"))
		self.renderSubdivs_BTN.setIconSize(QtCore.QSize(38,38))
		self.toogleSubdivs_BTN.setIcon(QtGui.QIcon(":/toggleSubdiv.png"))
		self.toogleSubdivs_BTN.setIconSize(QtCore.QSize(38,38))
		self.aimLight_BTN.setIcon(QtGui.QIcon(":/aimLight.png"))
		self.aimLight_BTN.setIconSize(QtCore.QSize(38,38))
		self.alignLight_BTN.setIcon(QtGui.QIcon(":/alignLight.png"))
		self.alignLight_BTN.setIconSize(QtCore.QSize(38,38))
		self.simpleIsolate_BTN.setIcon(QtGui.QIcon(":/simpleIsolate.png"))
		self.simpleIsolate_BTN.setIconSize(QtCore.QSize(38,38))
		self.specularConstrain_BTN.setIcon(QtGui.QIcon(":/specularConstrain.png"))
		self.specularConstrain_BTN.setIconSize(QtCore.QSize(38,38))
		
		#CONTEXT MENUS
		self.specularConstrain_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.specularConstrain_BTN.rightClick.connect(self.miniToolsOptions)
		
	
	def initTool(self):
		'''
        Initializes tool settings, also reset the tool on demand.
        '''
		self.initAttrs()
		self.appTools.initTools()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		self.appTools.initTools()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Mini Lighting Tools"
		self.__toolVersion = __version__
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		
		
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
		self.appTools.baseMod = self.baseMod
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		pass
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for assigning shaders as a new network
	##--------------------------------------------------------------------------------------------
	def miniToolsOptions (self, pos, btnObject):
		'''
		Right-click popup menu for shading assignment options.
		'''
		menu = QtWidgets.QMenu(btnObject)
		notFixedSpecularQ = menu.addAction("Not fixed specular")
		menu.popup(btnObject.mapToGlobal(pos))
		
		if btnObject.objectName() == 'specularConstrain_BTN':
			notFixedSpecularQ.triggered.connect(self.specularConstrainNotFixed)
	
	
	def specularConstrainNotFixed(self):
		self.appTools.specularConstrain(False)


##--------------------------------------------------------------------------------------------
##Button reimplementation
##--------------------------------------------------------------------------------------------
class MiniTools_BTN(QtWidgets.QPushButton):
	##--------------------------------------------------------------------------------------------
	##Signals
	##--------------------------------------------------------------------------------------------
	rightClick = QtCore.Signal(QtCore.QPoint, super)
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent=None):
		super(MiniTools_BTN, self).__init__(parent)
		self.parent = parent
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def mousePressEvent(self, event):
		if event.type() == QtCore.QEvent.MouseButtonPress:
			if event.button() == QtCore.Qt.RightButton:
				cursor = QtGui.QCursor()
				self.rightClick.emit(self.mapFromGlobal(cursor.pos()), self)
			else:
				super(MiniTools_BTN, self).mousePressEvent(event)
	

##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		
		self.configFile = JSONManager(config_json, self)
		self.initUI()
		self.setConnections()
		
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		#SIGNALS
		self.cancel_BTN.clicked.connect(self.cancelAll)
		self.save_BTN.clicked.connect(self.saveConfig)
	
	
	def initUI(self):
		"""Read JSON config file"""
		nearClip = self.configFile.getValueByProperty("nearClip", False)
		farClip = self.configFile.getValueByProperty("farClip", False)
		self.nearClip_DSPINBOX.setValue(nearClip)
		self.farClip_DSPINBOX.setValue(farClip)
	
	
	def saveConfig(self):
		"""Apply changes to JSON file"""
		jsonObj = {}
		jsonObj['nearClip'] = self.nearClip_DSPINBOX.value()
		jsonObj['farClip'] = self.farClip_DSPINBOX.value()
		self.configFile.writeJson(jsonObj, self.configFile.fileName)
		
		self.done(1)
	
	
	def cancelAll(self):
		"""Method to close the UI"""
		
		self.done(0)

##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/workflows/Lighting/MiniTools_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()