# -*- coding: utf-8 -*-
'''
Dressing importer tool.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
from cgx.gui.Qt import QtWidgets, QtCore
import maya.cmds as mc
import Red9.core.Red9_Meta as r9Meta
import cgx.Maya.scripts.maya_libs.Maya_MetaData as cgxMeta
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
import cgx.gui.DataViewModels as dvm


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/workflows/Lighting/DressingImporter'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.setConnections()
			self.initComponents()
			self.initUI()
			self.checkPlugins()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
        Connect signals.
        '''
		#SIGNALS
		self.projAssetsMod.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.populatePublishedDressings)
		self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentIndexChanged.connect(self.populatePublishedDressings)
		self.projAssetsMod.FileTypes_Component.fileType_COMBOBOX.currentIndexChanged.connect(self.populatePublishedDressings)
		self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentIndexChanged.connect(self.populatePublishedDressings)
		self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentIndexChanged.connect(self.populatePublishedDressings)
		self.projAssetsMod.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.refreshBlockedComboboxes)
		self.importDressing_BTN.clicked.connect(self.importDressing)
		self.deleteAsset_BTN.clicked.connect(self.deleteAsset)
		#CHKBOX
		self.customPathComp.customPath_CHKBOX.clicked.connect(self.refreshCustomPath)
		
	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Dressing Importer"
		self.__toolVersion = __version__
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.baseMod.MainMenu_Component.edit_config.setVisible(False)
		self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo, tRange=self.appTools.tRange)
		self.projAssetsMod = self.Pipeline_GUIModulesFactory.createModule('ProjAssets')
		self.projAssetsMod.pos = [10,40]
		self.customPathComp = pipeComponents.CustomPath_Component(self,self.centralWidget(), self.sInfo)
		self.customPathComp.pos = [10,180]
		self.customPathComp.fileMode = self.customPathComp.browseDialog.FileMode.Directory
		
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
		self.appTools.app_initUI.setCurrentProject(self.projAssetsMod.Project_Component)
		self.appTools.app_initUI.setCurrentAssetType(self.projAssetsMod.AssetTypes_Component)
		self.appTools.app_initUI.setCurrentAsset(self.projAssetsMod.Assets_Component)
		self.appTools.app_initUI.setCurrentSubAsset(self.projAssetsMod.SubAssets_Component)
		self.appTools.app_initUI.setCurrentFileType(self.projAssetsMod.FileTypes_Component)
		self.projAssetsMod.AssetTypes_Component.setToItem('SETS')
		self.projAssetsMod.AssetTypes_Component.container.setEnabled(False)
		self.projAssetsMod.FileTypes_Component.setToItem('dressing:DRS')
		self.projAssetsMod.FileTypes_Component.container.setEnabled(False)
		self.projAssetsMod.SubAssets_Component.container.setEnabled(False)
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		self.populatePublishedDressings()
	
	
	def checkPlugins(self):
		'''
        Check if plugins are loaded.
        :return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
        :rtype: dict
        '''
		if not mc.pluginInfo("MayaExocortexAlembic", query=True, loaded=True):
			self.baseMod.statusBar_update("Exocortex Alembic plugin not loaded.", warning=True)	
			return False
		elif not mc.pluginInfo("gpuCache", query=True, loaded=True):
			self.baseMod.statusBar_update("GPU Cache plugin not loaded.", warning=True)	
			return False
		elif not mc.pluginInfo("mtoa", query=True, loaded=True):
			self.baseMod.statusBar_update("MToA plugin not loaded.", warning=True)	
			return False
		else:
			return True
		
	
	def importDressing(self):
		dressingPath = self.buildAssetPath() + '/' + self.publishedDressings_COMBOBOX.currentText()
		if os.path.exists(dressingPath):
			#Import metanetwork
			fileNamespace = self.publishedDressings_COMBOBOX.currentText().rsplit('.',1)[0]
			networkNodes = mc.file(dressingPath, i=True, namespace=fileNamespace, returnNewNodes=True)
			metaAssets = r9Meta.getMetaNodes(['CGXDressingMetaAsset'])
			filterImportedMetaAssets = []
			for each in metaAssets:
				if each.mNode in networkNodes:
					filterImportedMetaAssets.append(each)
			i = 1
			usedAssets = []
			usedVariants = []
			for metaAsset in filterImportedMetaAssets:
				assetName = metaAsset.assetName
				subassetName = metaAsset.subassetName
				newNamespace = fileNamespace + ':' + assetName
				if subassetName not in ['',None,'None']:
					newNamespace = fileNamespace + ':' + subassetName
				metaVariants = metaAsset.getChildMetaNodes()
				for variant in metaVariants:
					variantName = variant.mNodeID.rsplit('_',1)[0]
					alembicFile = variant.LOD_MID_file
					if variant.LOD == 0:
						alembicFile = variant.LOD_HIGH_file
					if variant.LOD == 2:
						alembicFile = variant.LOD_LOW_file
					strLOD = 'MID'
					if variant.LOD == 0:
						strLOD = 'HIGH'
					elif variant.LOD == 2:
						strLOD = 'LOW'
						
					#CREATE GPU
					gpuNode = ''
					gpuTransform = ''
					gpuNode = mc.createNode('gpuCache', n = newNamespace + ':' + variantName + '_' + str(i) + '_GPUShape')
					preTransform = mc.listRelatives(gpuNode, parent=True, fullPath=True)[0]
					gpuTransform = mc.rename(preTransform, newNamespace + ':' + variantName + '_' + str(i) + '_GPU')
					mc.setAttr(gpuNode + '.cacheGeomPath', '|', type='string')
					mc.setAttr(gpuNode + '.cacheFileName', alembicFile, type='string')
					metaAsset.connectChildren([gpuTransform],'gpuNode')
					geoCenter = mc.xform(gpuTransform, ws=True, rp=True, query=True)
					bbox = mc.xform(gpuTransform, bb=True, query=True)
					xDelta = abs(bbox[0] - bbox[3])
					yDelta = abs(bbox[1] - bbox[4])
					zDelta = abs(bbox[2] - bbox[5])
					ctrlRadius = max([xDelta,zDelta])
					if ctrlRadius == 0:
						ctrlRadius = 2
					mc.setAttr(gpuTransform + '.tx', lock=True)
					mc.setAttr(gpuTransform + '.ty', lock=True)
					mc.setAttr(gpuTransform + '.tz', lock=True)
					mc.setAttr(gpuTransform + '.rx', lock=True)
					mc.setAttr(gpuTransform + '.ry', lock=True)
					mc.setAttr(gpuTransform + '.rz', lock=True)
					mc.setAttr(gpuTransform + '.sx', lock=True)
					mc.setAttr(gpuTransform + '.sy', lock=True)
					mc.setAttr(gpuTransform + '.sz', lock=True)
					#mc.setAttr(gpuTransform + '.overrideEnabled', 1)
					#mc.setAttr(gpuTransform + '.overrideDisplayType', 2)
					
					#CREATE EXOCORTEX
					exoTimeNode = ''
					exoFileNode = ''
					if metaAsset.animation == False:
						for checkNode in mc.ls(type='ExocortexAlembicFile', long= True):
							if mc.getAttr(checkNode + '.fileName') == alembicFile:
								exoFileNode = checkNode
								break
						if exoFileNode in ['',None,'None']:
							exoFileNode = mc.createNode("ExocortexAlembicFile", name= newNamespace + ':' + variantName + "_exoFile")
							exoTimeNode = mc.createNode("ExocortexAlembicTimeControl", name= newNamespace + ':' + variantName + "_exoTimeControl")
							mc.setAttr(exoFileNode + ".fileName", alembicFile, type="string")
							mc.connectAttr(exoTimeNode + ".outTime", exoFileNode + ".inTime", force=True)
							mc.connectAttr(mc.ls(type="time")[0] + ".outTime", exoTimeNode + ".inTime", force=True)
						else:
							exoTimeNode = mc.listConnections(exoFileNode + '.inTime', source=True, destination=False)[0]
					else:
						exoTimeNode = mc.createNode("ExocortexAlembicTimeControl", name= newNamespace + ':' + variantName + "_exoTimeControl")
						exoFileNode = mc.createNode("ExocortexAlembicFile", name= newNamespace + ':' + variantName + "_exoFile")
						mc.setAttr(exoFileNode + ".fileName", alembicFile, type="string")
						mc.connectAttr(exoTimeNode + ".outTime", exoFileNode + ".inTime", force=True)
						mc.connectAttr(mc.ls(type="time")[0] + ".outTime", exoTimeNode + ".inTime", force=True)
					metaGeos = variant.getMetaGeo('LOD_' + strLOD)
					transformsList = []
					exoNodesExpr = []
					for metaGeo in metaGeos:
						newName = newNamespace + ':' + variantName + ':' + metaGeo.geoName + '_' + str(i)
						cubeProxy = mc.polyCube(name= newName, ch= False)
						cubeShape = mc.listRelatives(cubeProxy[0], shapes= True, fullPath= True, noIntermediate= True)[0]
						#Create nodes
						exoXFormNode = mc.createNode("ExocortexAlembicXform", name= newName + "_exoXForm")
						exoPolyMeshNode = mc.createNode('ExocortexAlembicPolyMesh', name= newName + '_exoPolyMesh')
						exoNodesExpr = [exoXFormNode, exoPolyMeshNode]
						#Set attributes
						mc.setAttr(exoXFormNode + ".identifier", "/LOD_" + strLOD + '_' + metaGeo.geoName + '_bkd', type="string")
						mc.setAttr(exoPolyMeshNode + ".identifier", "/LOD_" + strLOD + '_' + metaGeo.geoName + '_bkd' + "/LOD_" + strLOD + '_' + metaGeo.geoName + '_bkdShape', type="string")
						mc.setAttr(cubeShape + '.aiSubdivType', metaGeo.aiSubdivType)
						mc.setAttr(cubeShape + '.aiSubdivIterations', metaGeo.aiSubdivIterations)
						mc.setAttr(cubeShape + '.aiDispHeight', metaGeo.aiDispHeight)
						mc.setAttr(cubeShape + '.aiDispPadding', metaGeo.aiDispPadding)
						mc.setAttr(cubeShape + '.aiDispZeroValue', metaGeo.aiDispZeroValue)
						mc.setAttr(cubeShape + '.aiOpaque', metaGeo.aiOpaque)
						mc.setAttr(cubeProxy[0] + '.visibility', 0)
						#mc.setAttr(cubeProxy[0] + '.overrideEnabled', 1)
						#mc.setAttr(cubeProxy[0] + '.overrideDisplayType', 2)
						#Connect nodes
						mc.connectAttr(exoTimeNode + ".outTime", exoPolyMeshNode + ".inTime", force=True)
						mc.connectAttr(exoFileNode + ".outFileName", exoPolyMeshNode + ".fileName", force=True)
						mc.connectAttr(exoFileNode + ".outFileName", exoXFormNode + ".fileName", force=True)
						mc.connectAttr(exoTimeNode + ".outTime", exoXFormNode + ".inTime", force=True)
						mc.connectAttr(exoXFormNode + ".translate", cubeProxy[0] + ".translate", force=True)
						mc.connectAttr(exoXFormNode + ".rotate", cubeProxy[0] + ".rotate", force=True)
						mc.connectAttr(exoXFormNode + ".scale", cubeProxy[0] + ".scale", force=True)
						mc.connectAttr(exoPolyMeshNode + '.outMesh', cubeShape + '.inMesh', force=True)
						metaGeo.connectChildren([cubeProxy[0]],'taggedNode')
						exoNodesList = [exoTimeNode, exoFileNode, exoXFormNode, exoPolyMeshNode]
						metaGeo.connectChildren(exoNodesList, 'exoNodes')
						transformsList.append(cubeProxy[0])
						
					#CREATE CTRL AND SET TRANSFORMS
					ctrlName = assetName
					if subassetName not in ['',None,'None']:
						ctrlName = subassetName
					assetControl = mc.circle(name= ':' + newNamespace + ':' + ctrlName + '_' + str(i) + '_CTRL',
												center=geoCenter,
												normal = (0,1,0),
												sw=360,
												degree=3,
												sections=8,
												constructionHistory=False,
												radius= ctrlRadius)[0]
					mc.parent(gpuTransform, assetControl)
					mc.parent(transformsList, assetControl)
					mc.setAttr(assetControl + '.tx', metaAsset.tx)
					mc.setAttr(assetControl + '.ty', metaAsset.ty)
					mc.setAttr(assetControl + '.tz', metaAsset.tz)
					mc.setAttr(assetControl + '.rx', metaAsset.rx)
					mc.setAttr(assetControl + '.ry', metaAsset.ry)
					mc.setAttr(assetControl + '.rz', metaAsset.rz)
					mc.setAttr(assetControl + '.sx', metaAsset.sx)
					mc.setAttr(assetControl + '.sy', metaAsset.sy)
					mc.setAttr(assetControl + '.sz', metaAsset.sz)
					
					#CREATE SWITCHER EXPRESSION
					mc.addAttr(assetControl, ln='previs', at='enum', keyable=True, en = ':GPU:EXO')
					ctrlAttribute = assetControl + '.previs'
					expStr = self.formatExpression(ctrlAttribute, gpuTransform, gpuNode, transformsList, exoNodesExpr)
					expNode = mc.expression(s=expStr, o=assetControl, alwaysEvaluate=True, uc='all', name = newNamespace + ':' + ctrlName + '_EXP')
					#Connect control to metasset
					metaAsset.connectChildren([assetControl],'control')
					metaAsset.connectChildren([expNode], 'expression')
				
					#IMPORT AND ASSIGN SHADERS
					if variant.mNodeID in usedVariants:
						sceneShaders = mc.ls(materials=True)
						sceneShaders.pop(sceneShaders.index('lambert1'))
						sceneShaders.pop(sceneShaders.index('particleCloud1'))
						for metaGeo in metaGeos:
							geometry = metaGeo.getChildren(mAttrs='taggedNode')[0]
							if metaGeo.shader != 'lambert1':
								for shdNode in sceneShaders:
									if metaGeo.shader in shdNode:
										shadingGroup = mc.listConnections(shdNode + '.outColor', destination=True, source=False)[0]
										mc.sets(geometry, edit= True, forceElement= shadingGroup)
										break
							else:
								shadingGroup = mc.listConnections('lambert1.outColor', destination=True, source=False)[0]
								mc.sets(geometry, edit= True, forceElement= shadingGroup)
					else:
						shadersFile = variant.shaders_file
						shadersNamespace = newNamespace + ':' + variantName
						tmpNamesapce = 'tmp'
						shaderNodes = mc.file(shadersFile, i=True, namespace= tmpNamesapce, returnNewNodes=True)
						mc.namespace( mv=(tmpNamesapce, shadersNamespace), force=True)
						mc.namespace(removeNamespace=tmpNamesapce)
						for metaGeo in metaGeos:
							geometry = metaGeo.getChildren(mAttrs='taggedNode')[0]
							if metaGeo.shader != 'lambert1':
								shaderName = shadersNamespace + ':' + metaGeo.shader
								shadingGroup = mc.sets(renderable=True, noSurfaceShader=True, empty=True, name = shaderName + 'SG')
								mc.connectAttr(shaderName + '.outColor', shadingGroup + '.surfaceShader')
								mc.sets(geometry, edit= True, forceElement= shadingGroup)
							else:
								shadingGroup = mc.listConnections('lambert1.outColor', destination=True, source=False)[0]
								mc.sets(geometry, edit= True, forceElement= shadingGroup)
					usedVariants.append(variant.mNodeID)
				usedAssets.append(metaAsset.mNodeID)
				i += 1
			
			#ADD CALLBACKS
			#PRE
			'''
			python(
			import maya.cmds as mc
			import Red9.core.Red9_Meta as r9Meta
			for metaAsset in r9Meta.getMetaNodes(['CGXDressingMetaAsset']):
				ctrl = metaAsset.getCtrl()
				mc.setAttr(ctrl + '.previs', 1)
			)
			'''
			preOneline = r'''python("import maya.cmds as mc\nimport Red9.core.Red9_Meta as r9Meta\nfor metaAsset in r9Meta.getMetaNodes(['CGXDressingMetaAsset']):\n\tctrl = metaAsset.getCtrl()\n\tmc.setAttr(ctrl + '.previs', 1)")'''
			#POST
			'''
			python(
			import maya.cmds as mc
			import Red9.core.Red9_Meta as r9Meta
			for metaAsset in r9Meta.getMetaNodes(['CGXDressingMetaAsset']):
				ctrl = metaAsset.getCtrl()
				mc.setAttr(ctrl + '.previs', 1)
			)
			'''
			postOneline = r'''python("import maya.cmds as mc\nimport Red9.core.Red9_Meta as r9Meta\nfor metaAsset in r9Meta.getMetaNodes(['CGXDressingMetaAsset']):\n\tctrl = metaAsset.getCtrl()\n\tmc.setAttr(ctrl + '.previs', 0)")'''
			mc.setAttr('defaultRenderGlobals.preMel', preOneline, type='string')
			mc.setAttr('defaultRenderGlobals.postMel', postOneline, type='string')
			mc.setAttr('defaultRenderGlobals.preRenderMel', preOneline, type='string')
			mc.setAttr('defaultRenderGlobals.postRenderMel', postOneline, type='string')
			mc.setAttr('defaultRenderGlobals.preRenderLayerMel', preOneline, type='string')
			mc.setAttr('defaultRenderGlobals.postRenderLayerMel', postOneline, type='string')
			self.baseMod.statusBar_update('{} dressing imported.'.format(dressingPath), success=True)
			
		else:
			self.baseMod.statusBar_update('{} file not found.'.format(dressingPath), alert=True)
	
	
	def formatExpression(self, _ctrlAttribute, _gpuTransform, _gpuShape, _geoNodes, _exoNodes):
		posibilities = []

		stringBlock = _gpuTransform + '.visibility = {};\n'.format(1)
		stringBlock += _gpuShape + '.frozen = {};\n'.format(0)
		for geo in _geoNodes:
			stringBlock += geo + '.visibility = {};\n'.format(0)
		for exo in _exoNodes:
			stringBlock += exo + '.frozen = {};\n'.format(1)
		posibilities.append(stringBlock)
		
		stringBlock = _gpuTransform + '.visibility = {};\n'.format(0)
		stringBlock += _gpuShape + '.frozen = {};\n'.format(1)
		for geo in _geoNodes:
			stringBlock += geo + '.visibility = {};\n'.format(1)
		for exo in _exoNodes:
			stringBlock += exo + '.frozen = {};\n'.format(0)
		posibilities.append(stringBlock)
		
		expStr = ''
		i = 0
		for block in posibilities:
			if i == 0:
				expStr += 'if ({}'.format(_ctrlAttribute) + '== {})'.format(i) + '\n' + '{' + '\n' + block + '\n' + '}'
			else:
				expStr += 'else if ({} '.format(_ctrlAttribute) + '== {})'.format(i) + '\n' + '{' + '\n' + block + '\n' + '}'
			i+=1
		
		return expStr
	
	
	def updateDressing(self):
		
		'''
		How to compare two networks.
		Import new network.
		Find pair network.
		Navigate recursively through imported network. If there is a match in pair network, copy attributes and reimport shaders (deleting old ones first).
		If there is no match, import asset and create it.
		'''
		pass
	
	
	def deleteAsset(self):
		selectedCtrls = mc.ls(sl=True,long=True)
		filteredCtrlsSelection = []
		for ctrl in selectedCtrls:
			shapeNode = mc.listRelatives(ctrl, shapes=True, noIntermediate=True, fullPath=True)[0]
			if mc.nodeType(shapeNode) == 'nurbsCurve':
				metaNodes = []
				metaAsset = r9Meta.getConnectedMetaNodes(ctrl, source=True)[0]
				metaNodes.append(metaAsset)
				metaVariants = metaAsset.getChildMetaNodes()
				metaNodes += metaVariants
				for variant in metaVariants:
					metaNodes += variant.getChildMetaNodes()
				
				for each in metaNodes:
					namespaceCheck = each.nameSpaceFull()
					each.delete()
					if mc.namespace(exists=namespaceCheck):
						objsList = mc.namespaceInfo(namespaceCheck, listNamespace=True)
						if objsList in ['',None,'None']:
							mc.namespace(removeNamespace=namespaceCheck)
					
				filteredCtrlsSelection.append(ctrl)
				mc.delete(ctrl)
		
	
	def populatePublishedDressings(self):
		assetPath = self.buildAssetPath()
		filesList = []
		if assetPath != None:
			if os.path.exists(assetPath):
				if os.path.exists(assetPath):
					filesList = [ f for f in os.listdir(assetPath) if os.path.isfile(os.path.join(assetPath,f)) ]
			if len(filesList) >= 1:
				for each in sorted(filesList, reverse=True):
					if each.split("_")[0] != self.projAssetsMod.FileTypes_Component.fileType_COMBOBOX.currentText().split(":")[1]:
						indexNum = filesList.index(each)
						del(filesList[indexNum])
		
		filesModel = dvm.ObjectsListModel(filesList, self)
		self.publishedDressings_COMBOBOX.setModel(filesModel)
		self.projAssetsMod.FileTypes_Component.setToItem('dressing:DRS')
	
	
	def buildAssetPath(self):
		assetType = self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
		assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
		subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
		
		propertiesDict = {"concreteFolderType":"published_leaffolder",
						"abstractFolderType":"branchfolder",
						"folderFunction":"published_content",
						"folderConcreteFunction":"published_asset",
						"placeholder":False}
		if subAssetName not in [None,""]:
			propertiesDict = {"concreteFolderType":"published_leaffolder",
						"abstractFolderType":"branchfolder",
						"folderFunction":"published_content",
						"folderConcreteFunction":"published_subasset",
						"placeholder":False}
		publishedNodes = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
		
		assetTypeDict={"name":assetType,
					"concreteFolderType":"assetType_mainfolder",
					"abstractFolderType":"mainfolder",
					"folderFunction":"grouping",
					"folderConcreteFunction":"",
					"placeholder":False}
		assetDict= {"name":"$CharacterName",
				"concreteFolderType":"asset_subfolder",
				"abstractFolderType":"subfolder",
				"folderFunction":"grouping",
				"folderConcreteFunction":"",
				"placeholder":True}
		if assetType == 'PROPS':
			assetDict= {"name":"$PropName",
				"concreteFolderType":"asset_subfolder",
				"abstractFolderType":"subfolder",
				"folderFunction":"grouping",
				"folderConcreteFunction":"",
				"placeholder":True}
		elif assetType == 'VEHICLES':
			assetDict= {"name":"$VehicleName",
				"concreteFolderType":"asset_subfolder",
				"abstractFolderType":"subfolder",
				"folderFunction":"grouping",
				"folderConcreteFunction":"",
				"placeholder":True}
		elif assetType == 'SETS':
			assetDict= {"name":"$SetName",
				"concreteFolderType":"asset_subfolder",
				"abstractFolderType":"subfolder",
				"folderFunction":"grouping",
				"folderConcreteFunction":"",
				"placeholder":True}
		
		subAssetDict= {"name":"$assetName_$subassetname",
					"concreteFolderType":"subasset_subfolder",
					"abstractFolderType":"subfolder",
					"folderFunction":"grouping",
					"folderConcreteFunction":"",
					"placeholder":True}
		for node in publishedNodes:
			pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict})
			if subAssetName not in [None,""]:
				pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict,subAssetName:subAssetDict})
			pathList.append(node.name)
			#Build path string
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
			
			if "$" not in pathStr:
				return pathStr
	
	
	def refreshCustomPath(self):
		if self.customPathComp.customPath_CHKBOX.isChecked():
			self.projAssetsMod.container.setEnabled(False)
		else:
			self.projAssetsMod.container.setEnabled(True)
	
	
	def refreshBlockedComboboxes(self):
		self.projAssetsMod.AssetTypes_Component.setToItem('SETS')
		self.projAssetsMod.FileTypes_Component.setToItem('dressing:DRS')


##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/workflows/Lighting/DressingImporter_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()