Hice una prueba con las torres de RunLauraRun. La carga del archivo con referencias tardo 19segs, con gpu tardo 9segs.

Unidad de Alembic:
--PARENTCONTROL
----GPU
----Alembic


-Se generan los subassets con shaders en archivos Maya.
-Se hace el dressing referenciando estos subassets las veces que sea necesario y moviendolos, rotandolos y escalandolos en el espacio.
-Si se necesita modificar vertices, se importa la referencia.

-Luego se corre un script de un solo boton que crea una unidad Alembic por cada referencia. Si la referencia es duplicada, solo crea la unidad vez y le aplica las transformaciones.
Si se detectan modificaciones a nivel vertice, se crea una unidad Alembic nueva


En el caso de las torres:
-Cada torre es un archivo de Maya. Cada torre tiene un grupo master. No debe haber geometria suelta.
-Se hace el dressing referenciando estas torres las veces que sea necesario y moviendolas, rotandolas y escalandolas desde el grupo master.
-Se corre el script. Se crearian solo tres unidades de Alembic, una por cada torre. Se importan esas unidades en escena, se le aplican las transformaciones de cada referencia y se le asignan los shaders. Se borran las referencias originales.
Si se encontro una referencia con deformaciones, se crea una unidad mas de Alembic y se aplica el mismo procedimiento, esta vez teniendo en cuenta un rango de tiempo por si esa deformacion ademas esta animada.
-Se publica este archivo lleno de Alembics.
-Lighting referencia este archivo lleno de Alembics.

Casos especiales:
-Si lighting tiene que cambiar alguna de las torres, lo puede hacer moviendo, rotando y escalando y guardando los edits al archivo original.
-Si se agrega una torre mas, se lo hace dentro del archivo consolidado. Se referencia la torre deseada, se acomoda y se consolida de nuevo.
-Si se agrega geometria sin referenciar, debe ponerse dentro de un grupo master y consolidar ese grupo master. Se creara una unidad Alembic para eso especificamente. En teoria se podria armar un entorno entero asi y consolidar todo, aunque no seria tan ahorrativo.
-Se puede hacer el dressing solo con modelos y luego trabajar el shading sobre el consolidado mientras lighting trabaja en grises.


DUDAS:
-Es necesario armar todo primero con referencias? No se podria consolidar cada subasset y hacer el dressing trabajando directamente con las unidades importadas y duplicadas? --> Se puede, pero si se generan deformaciones, no van a funcionar con el GPU. En ese caso habria que reconsolidar con la deformacion, generando una unidad Alembic nueva.
