# -*- coding: utf-8 -*-
'''
Dressing Publisher tool.

Created on June 14, 2017

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/

'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os, time, socket
from cgx.gui.Qt import QtWidgets, QtCore, QtGui
from cgx.gui.Qt import __binding__
import maya.cmds as mc
import Red9.core.Red9_Meta as r9Meta
import cgx.Maya.scripts.maya_libs.Maya_MetaData as cgxMeta
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
from cgx.core.Synchronizer import Synchronizer
import cgx.gui.DataViewModels as dvm
import cgx.Maya.scripts.pipeline.AssetPublisher.AssetPublisher_icons


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/workflows/SetDressing/DressingPublisher'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for cache tools
        :type appTools: Class inherits from Abstract_CacheTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.setConnections()
			self.initComponents()
			self.initUI()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
        Connect signals.
        '''
		#SIGNALS
		self.projAssetsMod.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.init_seqShotMod)
		self.projAssetsMod.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.populatePublished)
		self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentIndexChanged.connect(self.populatePublished)
		self.projAssetsMod.FileTypes_Component.fileType_COMBOBOX.currentIndexChanged.connect(self.populatePublished)
		self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentIndexChanged.connect(self.populatePublished)
		self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentIndexChanged.connect(self.populatePublished)
		self.publishDressing_BTN.clicked.connect(self.publishDressing)
		#CHKBOX
		self.customPathComp.customPath_CHKBOX.clicked.connect(self.refreshCustomPath)
		#REFRESH
		self.shotPublish_CHKBOX.clicked.connect(self.refreshShotPublish)
		self.customSuffix_CHKBOX.clicked.connect(self.refreshCustomSuffix)
		
	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Dressing Publisher"
		self.__toolVersion = __version__
		
		self.syncStatusDict = {1:"Already published",
							2:"Published but different",
							3:"Same date, different size",
							4:"Different date, same size",
							5:"Both files missing",
							6:"Not Published",
							7:"Current file missing"}
		
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.baseMod.MainMenu_Component.edit_config.setVisible(False)
		self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo, tRange=self.appTools.tRange)
		self.projAssetsMod = self.Pipeline_GUIModulesFactory.createModule('ProjAssets')
		self.projAssetsMod.pos = [10,35]
		self.seqShotMod = self.Pipeline_GUIModulesFactory.createModule('SeqShot')
		self.seqShotMod.pos = [10,228]
		self.customPathComp = pipeComponents.CustomPath_Component(self,self.centralWidget(), self.sInfo)
		self.customPathComp.pos = [10,286]
		self.customPathComp.fileMode = self.customPathComp.browseDialog.FileMode.Directory
		self.fpsComp = pipeComponents.FPS_Component(self,self.centralWidget(), self.pInfo)
		self.fpsComp.pos = [276,40]
		#Add debugMode option
		className= str(self.__class__.__name__)
		self.debugMode = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.debugMode.setCheckable(True)
		self.debugMode.setObjectName("edit_debugMode")
		self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[0], self.debugMode)
		if __binding__ in ('PySide', 'PyQt4'):
			self.debugMode.setText(QtWidgets.QApplication.translate(className, "Debug Mode", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			self.debugMode.setText(QtWidgets.QApplication.translate(className, "Debug Mode", None))
		
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
		self.appTools.app_initUI.setCurrentProject(self.projAssetsMod.Project_Component)
		self.appTools.app_initUI.setCurrentAssetType(self.projAssetsMod.AssetTypes_Component)
		self.appTools.app_initUI.setCurrentAsset(self.projAssetsMod.Assets_Component)
		self.appTools.app_initUI.setCurrentSubAsset(self.projAssetsMod.SubAssets_Component)
		self.appTools.app_initUI.setCurrentFileType(self.projAssetsMod.FileTypes_Component)
		self.fpsComp.initComponent(self.pInfo.maya_getFPS())
		self.appTools.app_initUI.checkFPS(self.fpsComp)
		self.projAssetsMod.AssetTypes_Component.setToItem('SETS')
		self.projAssetsMod.AssetTypes_Component.container.setEnabled(False)
		self.projAssetsMod.FileTypes_Component.setToItem('dressing:DRS')
		self.projAssetsMod.FileTypes_Component.container.setEnabled(False)
		self.seqShotMod.container.setEnabled(False)
	
	
	def checkPlugins(self):
		'''
        Check if plugins are loaded.
        :return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
        :rtype: dict
        '''
		if not mc.pluginInfo("MayaExocortexAlembic", query=True, loaded=True):
			self.baseMod.statusBar_update("Exocortex Alembic plugin not loaded.", warning=True)	
			return False
		elif not mc.pluginInfo("gpuCache", query=True, loaded=True):
			self.baseMod.statusBar_update("GPU Cache plugin not loaded.", warning=True)	
			return False
		elif not mc.pluginInfo("mtoa", query=True, loaded=True):
			self.baseMod.statusBar_update("MToA plugin not loaded.", warning=True)
			return False
		else:
			return True
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		self.populatePublished()
	
	
	def init_seqShotMod(self):
		self.seqShotMod.initComponents()
		
		
	def publishDressing(self):
		metaAssets = r9Meta.getMetaNodes(['CGXDressingMetaAsset'])
		if len(metaAssets) >= 1:
			if mc.getAttr("defaultRenderLayer.renderable"):
				mc.setAttr("defaultRenderLayer.renderable", False)
				#mc.file(save=True, force=True)
			else:
				pass
				#mc.file(save=True, force=True)
				
			sync = Synchronizer(self)
			sync.statusDict = self.syncStatusDict
			
			trgPath = self.buildAssetPath()
			if self.customPathComp.customPath_CHKBOX.isChecked():
				trgPath = self.customPathComp.customPath_LINEEDIT.text()
			#Source path
			origFile = mc.file(expandName= True, query= True)
			#Backup path
			backupPath = self.buildBackupPath()
			#FILE NAME
			newName = self.buildFileName(origFile)
			#File metadata
			self.createFileMetadata(origFile)
			
			#IF THERE IS A PUBLISHED FILE ALREADY, BACK UP THAT MAYA PROJECT FIRST
			if self.backup_CHKBOX.isChecked():
				backupPath = self.backupFile(backupPath, trgPath, newName, sync)
			
			#CLEAN AND EXPORT METANETWORK
			deleteCtrlsList= []
			for metaAsset in metaAssets:
				ctrl = metaAsset.getCtrl()
				tx = mc.getAttr(ctrl + '.tx')
				ty = mc.getAttr(ctrl + '.ty')
				tz = mc.getAttr(ctrl + '.tz')
				rx = mc.getAttr(ctrl + '.rx')
				ry = mc.getAttr(ctrl + '.ry')
				rz = mc.getAttr(ctrl + '.rz')
				sx = mc.getAttr(ctrl + '.sx')
				sy = mc.getAttr(ctrl + '.sy')
				sz = mc.getAttr(ctrl + '.sz')
				metaAsset.addAttr(attr='tx', attrType='float', value=tx)
				metaAsset.addAttr(attr='ty', attrType='float', value=ty)
				metaAsset.addAttr(attr='tz', attrType='float', value=tz)
				metaAsset.addAttr(attr='rx', attrType='float', value=rx)
				metaAsset.addAttr(attr='ry', attrType='float', value=ry)
				metaAsset.addAttr(attr='rz', attrType='float', value=rz)
				metaAsset.addAttr(attr='sx', attrType='float', value=sx)
				metaAsset.addAttr(attr='sy', attrType='float', value=sy)
				metaAsset.addAttr(attr='sz', attrType='float', value=sz)
				metaVariants = metaAsset.getChildMetaNodes(mAttrs='')
				pickedVariant = mc.getAttr(ctrl + '.variants',asString=True)
				pickedLOD = mc.getAttr(ctrl + '.LOD', asString=True)
				for metaVariant in metaVariants:
					alembicFilesDict = {'HIGH':metaVariant.LOD_HIGH_file,'MID':metaVariant.LOD_MID_file,'LOW':metaVariant.LOD_LOW_file}
					if alembicFilesDict[pickedLOD] not in ['','None',None]:
						if not os.path.exists(alembicFilesDict[pickedLOD]):
							for lodOption in alembicFilesDict.keys():
								if os.path.exists(alembicFilesDict[lodOption]):
									if lodOption == 'HIGH':
										mc.setAttr(ctrl + '.variants', 0)
									elif lodOption == 'MID':
										mc.setAttr(ctrl + '.variants', 1)
									elif lodOption == 'LOW':
										mc.setAttr(ctrl + '.variants', 2)
									pickedVariant = mc.getAttr(ctrl + '.variants',asString=True)
									break
					strLOD = 'MID'
					if metaVariant.LOD == 0:
						strLOD = 'HIGH'
					elif metaVariant.LOD == 2:
						strLOD = 'LOW'
					if pickedVariant != metaVariant.mNodeID.rsplit('_',1)[0]:
						for metaGeo in metaVariant.getMetaGeo('LOD_' + strLOD):
							metaGeo.delete()
						metaVariant.delete()
				deleteCtrlsList.append(ctrl)
			mc.delete(deleteCtrlsList)
			metaNodes = r9Meta.getMetaNodes()
			selList = [x.mNode for x in metaNodes]
			mc.select(selList, replace=True)
			dressingFile = trgPath + "/" + newName
			
			mc.file(dressingFile, options='v=1;', preserveReferences=True, force=True, exportSelected=True, typ='mayaAscii')
			self.baseMod.statusBar_update("Published dressing to {}".format(dressingFile), success=True)
			#Sync log- Debug Mode
			if self.debugMode.isChecked():
				self.debugDialog(',\n'.join(sync.log), self)
			#RE-OPEN ORIGINAL FILE
			mc.file(origFile, open=True, force=True, prompt= False)
			self.resetTool()
		else:
			self.baseMod.statusBar_update("No Meta Assets found on this project.", success=True)
	
	
	def createFileMetadata(self, _origFile):
		existingMetaNodes = r9Meta.getMetaNodes(['CGXMetaPublishedFile'])
		if len(existingMetaNodes) == 0:
			metaNode = cgxMeta.CGXMetaPublishedFile(name='file_META')
		else:
			metaNode = existingMetaNodes[0]
		metaNode.fileOrigin = _origFile
		currWorkstation = socket.gethostname()
		metaNode.author = str(currWorkstation)
		metaNode.time = str(time.ctime(os.path.getmtime(_origFile)))
	
	
	def backupFile(self, backupPath, trgPath, newName, sync):
		#Modify bkup path to add version
		allEntries = []
		if os.path.exists(backupPath):
			allEntries = os.listdir(backupPath)
		if len(allEntries) == 0 or allEntries == None:
			backupPath += "/v_001"
		else:
			versionDirs = []
			for folder in allEntries:
				if folder.startswith("v"):
					versionDirs.append(folder)
			intVersions = []
			digitsNumber = 3
			for folder in versionDirs:
				versionNumber = folder.rsplit("_",1)[1]
				digitsNumber = len(versionNumber)
				intVersion = int(versionNumber)
				intVersions.append(intVersion)
			newVersion = str(max(intVersions) + 1).zfill(digitsNumber)
			backupPath += "/v_" + newVersion
		if os.path.exists(trgPath + "/" + newName):
			self.baseMod.statusBar_update("Backing up existing file to: " + backupPath + "/" + newName, status=True)
			sync.processPaths(trgPath + "/" + newName, backupPath + "/" + newName)
			
		return backupPath
	
	
	def populatePublished(self):
		sync = Synchronizer(self)
		sync.statusDict = self.syncStatusDict
		trgPath = self.buildAssetPath()
		filesList = []
		if trgPath != None:
			if os.path.exists(trgPath):
				filesList = [ f for f in os.listdir(trgPath) if os.path.isfile(os.path.join(trgPath,f)) ]
		
		if len(filesList) >= 1:
			for each in sorted(filesList, reverse=True):
				if each.split("_")[0] != self.projAssetsMod.FileTypes_Component.fileType_COMBOBOX.currentText().split(":")[1]:
					indexNum = filesList.index(each)
					del(filesList[indexNum])
			currentFile = mc.file(expandName= True, query= True)

		headers = ["Status","Name","Last Update","Size(Bytes)"]
		dataList = []
		for each in filesList:
			rowDataList = []
			for column in headers:
				if column == "Status":
					rowDataList.append(sync.getSyncStatus(currentFile, trgPath + "/" + each))
				elif column == "Name":
					rowDataList.append(each)
				elif column == "Last Update":
					fileModDate = time.ctime(os.path.getmtime(trgPath + "/" + each))
					rowDataList.append(fileModDate)
				elif column == "Size(Bytes)":
					statinfo = os.stat(trgPath + "/" + each)
					fileSize = float(statinfo.st_size)
					rowDataList.append(fileSize)
			dataList.append(rowDataList)
		publishedModel = PublishedDataTableModel(dataList, headers, self.published_TABLEVIEW)
		self.published_TABLEVIEW.setModel(publishedModel)
		self.published_TABLEVIEW.resizeColumnsToContents()
		self.published_TABLEVIEW.selectRow(0)
		self.published_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
	
	
	def buildFileName(self, srcPath):
		newName = srcPath
		fileType = self.projAssetsMod.FileTypes_Component.fileType_COMBOBOX.currentText().split(":")[1]
		assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
		subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
		if subAssetName not in [None,""]:
			assetName = subAssetName
		seq = self.seqShotMod.Sequence_Component.seq_COMBOBOX.currentText()
		shot = self.seqShotMod.Shot_Component.shot_COMBOBOX.currentText()
		customSuffix = self.customSuffix_LINEEDIT.text()
		if self.customSuffix_CHKBOX.isChecked():
			fileName = srcPath.rsplit("/", 1)[1]
			newName = "{}_{}_{}.{}".format(fileType, assetName, customSuffix, "ma")
			if fileName[-2:] == "mb":
				newName = "{}_{}_{}.{}".format(fileType, assetName, customSuffix, "mb")
			#SHOT DEPENDENT ASSET FILE NAME
			if self.shotPublish_CHKBOX.isChecked():
				fileName = srcPath.rsplit("/", 1)[1]
				newName = "{}_{}_{}_{}_{}.{}".format(fileType, assetName, seq, shot, customSuffix, "ma")
				if fileName[-2:] == "mb":
					newName = "{}_{}_{}_{}_{}.{}".format(fileType, assetName, seq, shot, customSuffix, "mb")
		else:
			fileName = srcPath.rsplit("/", 1)[1]
			newName = "{}_{}.{}".format(fileType, assetName, "ma")
			if fileName[-2:] == "mb":
				newName = "{}_{}.{}".format(fileType, assetName, "mb")
			#SHOT DEPENDENT ASSET FILE NAME
			if self.shotPublish_CHKBOX.isChecked():
				fileName = srcPath.rsplit("/", 1)[1]
				newName = "{}_{}_{}_{}.{}".format(fileType, assetName, seq, shot, "ma")
				if fileName[-2:] == "mb":
					newName = "{}_{}_{}_{}.{}".format(fileType, assetName, seq, shot, "mb")
		
		return newName
	
	
	def buildAssetPath(self):
		assetType = self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
		assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
		subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
		
		propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
		if subAssetName not in [None,""]:
			propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_subasset",
                        "placeholder":False}
		publishedNodes = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
		
		assetTypeDict={"name":assetType,
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
		assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		if assetType == 'PROPS':
			assetDict= {"name":"$PropName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'VEHICLES':
			assetDict= {"name":"$VehicleName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'SETS':
			assetDict= {"name":"$SetName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		
		subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
		for node in publishedNodes:
			pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict})
			if subAssetName not in [None,""]:
				pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict,subAssetName:subAssetDict})
			pathList.append(node.name)
			#Build path string
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
			
			if "$" not in pathStr:
				return pathStr
	
	
	def buildBackupPath(self):
		assetType = self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
		assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
		subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
		bkupPropertiesDict = {"abstractFolderType":"subfolder",
                              "concreteFolderType":"technical_subfolder",
                              "folderFunction":"published_content",
                              "folderConcreteFunction":"published_backup",
                              "placeholder":False}
		bkupNode = self.pInfo.folderStructure.getPropertiesAndValues(bkupPropertiesDict)
		bkupPathStr = ""
		if len(bkupNode) == 1:
			node = bkupNode[0]
			pathList = self.pInfo.rebuildPath(node)
			pathList.append(node.name)
			#Build path string
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			bkupPathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])

		propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
		if subAssetName not in [None,""]:
			propertiesDict = {"concreteFolderType":"published_leaffolder",
	                        "abstractFolderType":"branchfolder",
	                        "folderFunction":"published_content",
	                        "folderConcreteFunction":"published_subasset",
	                        "placeholder":False}
		assetTypeDict={"name":"CHARACTERS",
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
		assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		if assetType == 'PROPS':
			assetDict= {"name":"$PropName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'VEHICLES':
			assetDict= {"name":"$VehicleName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'SETS':
			assetDict= {"name":"$SetName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
		assetNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
		assetPathStr = ""
		for node in assetNode:
			pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict})
			if subAssetName not in [None,""]:
				pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict,subAssetName:subAssetDict})
			pathList.append(node.name)
			pathStr = "/".join(pathList[1:])
			
			if "$" not in pathStr:
				assetPathStr = pathStr
				break
		
		finalPathStr = bkupPathStr + "/" + assetPathStr
		
		return finalPathStr
	
	
	def refreshShotPublish(self):
		if self.shotPublish_CHKBOX.isChecked():
			self.seqShotMod.container.setEnabled(True)
		else:
			self.seqShotMod.container.setEnabled(False)
	
	
	def refreshCustomSuffix(self):
		if self.customSuffix_CHKBOX.isChecked():
			self.customSuffix_LINEEDIT.setEnabled(True)
		else:
			self.customSuffix_LINEEDIT.setEnabled(False)
	
	
	def refreshCustomPath(self):
		if self.customPathComp.customPath_CHKBOX.isChecked():
			self.projAssetsMod.container.setEnabled(False)
		else:
			self.projAssetsMod.container.setEnabled(True)
	
	
	def debugDialog(self,  _text, _parent):
		dialog = QtWidgets.QDialog(_parent)
		dialog.setObjectName("Dialog")
		dialog.resize(781, 556)
		gridLayout = QtWidgets.QGridLayout(dialog)
		gridLayout.setObjectName("gridLayout")
		debug_TEXTEDIT = QtWidgets.QTextEdit(dialog)
		debug_TEXTEDIT.setObjectName("debug_TEXTEDIT")
		debug_TEXTEDIT.setText(_text)
		debug_TEXTEDIT.setReadOnly(True)
		gridLayout.addWidget(debug_TEXTEDIT, 0, 0, 1, 1)
		if __binding__ in ('PySide', 'PyQt4'):
			dialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog", "Debug", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			dialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog", "Debug", None))
		dialog.show()


##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for published files table
##--------------------------------------------------------------------------------------------
class PublishedDataTableModel(dvm.DataTableModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self,dataList, headers, parent):
		super(PublishedDataTableModel, self).__init__(dataList, headers, parent)
	

	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def data(self, index, role):
		"""Collect data and put it in the table"""
		row = index.row()
		column = index.column()
		
		if role == QtCore.Qt.DisplayRole:
			value = self.dataList[row][column]
			return value
		
		if role == QtCore.Qt.ToolTipRole:
			value = self.dataList[row][column]
			return value
		
		if role == QtCore.Qt.EditRole:
			return self.dataList[row][column]
		
		if role == QtCore.Qt.DecorationRole:
			value = self.dataList[row][column]
			if self.headers[column] == "Status":
				if value == "Already published":
					return QtGui.QIcon(":/AssetPublisher_inSync.png")
				elif value == "Published but different":
					return QtGui.QIcon(":/AssetPublisher_outOfSync.png")
				else:
					return QtGui.QIcon(":/AssetPublisher_rest.png")


##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/workflows/SetDressing/DressingPublisher_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()