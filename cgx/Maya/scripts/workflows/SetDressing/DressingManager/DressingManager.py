# -*- coding: utf-8 -*-
'''
Dressing Manager tool.

Created on June 13, 2017

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/

'''


__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os, random
from cgx.gui.Qt import QtWidgets, QtCore
from cgx.gui.Qt import __binding__
import Red9.core.Red9_Meta as r9Meta
import cgx.Maya.scripts.maya_libs.Maya_MetaData as cgxMeta
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
import cgx.gui.DataViewModels as dvm
import maya.cmds as mc


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/workflows/SetDressing/DressingManager'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.setConnections()
			self.initComponents()
			self.initUI()
			self.checkPlugins()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
        Connect signals.
        '''
		#SIGNALS
		self.projAssetsMod.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.populateVariants)
		self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentIndexChanged.connect(self.populateVariants)
		self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentIndexChanged.connect(self.populateVariants)
		self.projAssetsMod.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.refreshBlockedComboboxes)
		#BUTTONS
		self.importAsset_BTN.clicked.connect(self.importAsset)
		self.duplicateAsset_BTN.clicked.connect(self.duplicateAsset)
		self.deleteAsset_BTN.clicked.connect(self.deleteAsset)
		self.randomizeVariant_BTN.clicked.connect(self.randomizeVariant)
		#CHKBOX
		self.customPathComp.customPath_CHKBOX.clicked.connect(self.refreshCustomPath)
		
	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		self.checkPlugins()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Dressing Manager"
		self.__toolVersion = __version__
		
		#Set time range
		self.appTools.tRange.handleIn = 2
		self.appTools.tRange.handleOut = 2
		self.appTools.tRange.startFrame = 1
		self.appTools.tRange.endFrame = 100
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.baseMod.MainMenu_Component.edit_config.setVisible(False)
		self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo, tRange=self.appTools.tRange)
		self.projAssetsMod = self.Pipeline_GUIModulesFactory.createModule('ProjAssets')
		self.projAssetsMod.pos = [10,40]
		self.customPathComp = pipeComponents.CustomPath_Component(self,self.centralWidget(), self.sInfo)
		self.customPathComp.pos = [10,200]
		self.customPathComp.fileMode = self.customPathComp.browseDialog.FileMode.Directory
		self.fpsComp = pipeComponents.FPS_Component(self,self.centralWidget(), self.pInfo)
		self.fpsComp.pos = [275,40]
		#Add reference camera option
		self.manager_opt = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.manager_opt.setObjectName("manager")
		self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[0], self.manager_opt)
		className= str(self.__class__.__name__)
		if __binding__ in ('PySide', 'PyQt4'):
			self.manager_opt.setText(QtWidgets.QApplication.translate(className, "Manager", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			self.manager_opt.setText(QtWidgets.QApplication.translate(className, "Manager", None))
			
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
		self.appTools.app_initUI.setCurrentProject(self.projAssetsMod.Project_Component)
		self.appTools.app_initUI.setCurrentAssetType(self.projAssetsMod.AssetTypes_Component)
		self.appTools.app_initUI.setCurrentAsset(self.projAssetsMod.Assets_Component)
		self.appTools.app_initUI.setCurrentSubAsset(self.projAssetsMod.SubAssets_Component)
		self.appTools.app_initUI.setCurrentFileType(self.projAssetsMod.FileTypes_Component)
		self.fpsComp.initComponent(self.pInfo.maya_getFPS())
		self.appTools.app_initUI.checkFPS(self.fpsComp)
		self.projAssetsMod.AssetTypes_Component.setToItem('SETS')
		self.projAssetsMod.AssetTypes_Component.container.setEnabled(False)
		self.projAssetsMod.FileTypes_Component.setToItem('shading:SHD')
		self.projAssetsMod.FileTypes_Component.container.setEnabled(False)
		self.LOD_MID_RBTN.setChecked(True)
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		self.populateVariants()
	
	
	def checkPlugins(self):
		'''
        Check if plugins are loaded.
        :return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
        :rtype: dict
        '''
		if not mc.pluginInfo("MayaExocortexAlembic", query=True, loaded=True):
			self.baseMod.statusBar_update("Exocortex Alembic plugin not loaded.", warning=True)	
			return False
		elif not mc.pluginInfo("gpuCache", query=True, loaded=True):
			self.baseMod.statusBar_update("GPU Cache plugin not loaded.", warning=True)	
			return False
		elif not mc.pluginInfo("mtoa", query=True, loaded=True):
			self.baseMod.statusBar_update("MToA plugin not loaded.", warning=True)
			return False
		else:
			return True
		
	
	def importAsset(self):
		if self.checkPlugins():
			#LOD
			selectedLod = 'LOD_MID'
			if self.LOD_HIGH_RBTN.isChecked():
				selectedLod = 'LOD_HIGH'
			if self.LOD_LOW_RBTN.isChecked():
				selectedLod = 'LOD_LOW'
			
			assetType = self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
			assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
			subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
			assetPath = self.buildAssetPath(assetType, assetName, subAssetName)
			assetFile = assetPath + '/Cache/' + self.variant_COMBOBOX.currentText() + '/' + selectedLod + '_bkd_1.abc'
			metaNetworkFile = assetPath + '/metaNetwork.ma'
			if os.path.exists(assetFile):
				if os.path.exists(metaNetworkFile):
					#Create namespace
					newNamespace = assetName
					if subAssetName not in ['',None,'None']:
						newNamespace = subAssetName
					if not mc.namespace(exists=':{}'.format(newNamespace)):
						#mc.namespace(add=newNamespace)
						#Import metanetwork
						networkNodes = mc.file(metaNetworkFile, i=True, namespace=newNamespace, returnNewNodes=True)
						metaAssets = r9Meta.getMetaNodes(['CGXDressingMetaAsset'])
						metaAsset = ''
						for each in networkNodes:
							for metaNode in metaAssets:
								if metaNode.mNode == each:
									metaAsset = metaNode
									break
							if metaAsset != '':
								break
						metaVariants = metaAsset.getChildMetaNodes()
						#Create GPU nodes
						gpuTransforms = []
						for each in metaVariants:
							if self.LOD_MID_RBTN.isChecked():
								each.LOD = 'MID'
							elif self.LOD_HIGH_RBTN.isChecked():
								each.LOD = 'HIGH'
							elif self.LOD_LOW_RBTN.isChecked():
								each.LOD = 'LOW'
							
							variantName = each.mNodeID.rsplit('_',1)[0]
							if each.LOD == 0:
								if each.LOD_HIGH_file not in [None,'','None']:
									if os.path.exists(each.LOD_HIGH_file):
										gpuNode = mc.createNode('gpuCache', n = newNamespace + ':' + variantName + '_GPU')
										transform = mc.listRelatives(gpuNode, parent=True, fullPath=True)[0]
										result = mc.rename(transform, newNamespace + ':' + variantName)
										gpuNode = mc.listRelatives(result, shapes=True, noIntermediate=True, fullPath=True)[0]
										gpuTransforms.append(result)
										mc.setAttr(gpuNode + '.cacheGeomPath', '|', type='string')
										mc.setAttr(gpuNode + '.cacheFileName', each.LOD_HIGH_file, type='string')
							elif each.LOD == 1:
								if each.LOD_MID_file not in [None,'','None']:
									if os.path.exists(each.LOD_MID_file):
										gpuNode = mc.createNode('gpuCache', n = newNamespace + ':' + variantName + '_GPU')
										transform = mc.listRelatives(gpuNode, parent=True, fullPath=True)[0]
										result = mc.rename(transform, newNamespace + ':' + variantName)
										gpuNode = mc.listRelatives(result, shapes=True, noIntermediate=True, fullPath=True)[0]
										gpuTransforms.append(result)
										mc.setAttr(gpuNode + '.cacheGeomPath', '|', type='string')
										mc.setAttr(gpuNode + '.cacheFileName', each.LOD_MID_file, type='string')
							elif each.LOD == 2:
								if each.LOD_LOW_file not in [None,'','None']:
									if os.path.exists(each.LOD_LOW_file):
										gpuNode = mc.createNode('gpuCache', n = newNamespace + ':' + variantName + '_GPU')
										transform = mc.listRelatives(gpuNode, parent=True, fullPath=True)[0]
										result = mc.rename(transform, newNamespace + ':' + variantName)
										gpuNode = mc.listRelatives(result, shapes=True, noIntermediate=True, fullPath=True)[0]
										gpuTransforms.append(result)
										mc.setAttr(gpuNode + '.cacheGeomPath', '|', type='string')
										mc.setAttr(gpuNode + '.cacheFileName', each.LOD_LOW_file, type='string')
						#Create control
						xList = []
						yList = []
						zList = []
						geoCenter = []
						for transform in gpuTransforms:
							if self.variant_COMBOBOX.currentText() in transform:
								geoCenter = mc.xform(transform, ws=True, rp=True, query=True)
							#shape = mc.listRelatives(transform, shapes=True, noIntermediate=True, fullPath=True)[0]
							#bboxMin = mc.getAttr(shape + '.boundingBoxMin')[0]
							#bboxMax = mc.getAttr(shape + '.boundingBoxMax')[0]
							bbox = mc.xform(transform, bb=True, query=True)
							xDelta = abs(bbox[0] - bbox[3])
							xList.append(xDelta)
							yDelta = abs(bbox[1] - bbox[4])
							yList.append(yDelta)
							zDelta = abs(bbox[2] - bbox[5])
							zList.append(zDelta)
							mc.setAttr(transform + '.tx', lock=True)
							mc.setAttr(transform + '.ty', lock=True)
							mc.setAttr(transform + '.tz', lock=True)
							mc.setAttr(transform + '.rx', lock=True)
							mc.setAttr(transform + '.ry', lock=True)
							mc.setAttr(transform + '.rz', lock=True)
							mc.setAttr(transform + '.sx', lock=True)
							mc.setAttr(transform + '.sy', lock=True)
							mc.setAttr(transform + '.sz', lock=True)
							if self.variant_COMBOBOX.currentText() not in transform:
								mc.setAttr(transform + '.visibility', 0)
							#mc.setAttr(transform + '.overrideEnabled', 1)
							#mc.setAttr(transform + '.overrideDisplayType', 2)
						xMax = max(xList)
						yMax = max(yList)
						zMax = max(zList)
						ctrlName = assetName
						if subAssetName not in ['',None,'None']:
							ctrlName = subAssetName
						radio = max([xMax,zMax])
						if radio == 0:
							radio = 2
						assetControl = mc.circle(name= ':' + newNamespace + ':' + ctrlName + '_CTRL',
												center=geoCenter,
												normal = (0,1,0),
												sw=360,
												degree=3,
												sections=8,
												constructionHistory=False,
												radius= radio)[0]
						mc.addAttr(assetControl, ln='LOD', at='enum', keyable=True, en = "HIGH:MID:LOW:")
						enumString = ''
						for each in metaVariants:
							variantName = each.mNodeID.rsplit('_',1)[0]
							enumString += variantName + ':'
						mc.addAttr(assetControl, ln='variants', at='enum', keyable=True, en = enumString)
						mc.parent(gpuTransforms, assetControl)
						#Connect attrs and create expressions
						for each in metaVariants:
							mc.connectAttr(assetControl + '.LOD', each.mNode + '.LOD', force=True)
						ctrlAttribute = assetControl + '.variants'
						expStr = self.formatExpression(ctrlAttribute, gpuTransforms)
						expNode = mc.expression(s=expStr, o=assetControl, alwaysEvaluate=True, uc='all', name = newNamespace + ':' + ctrlName + '_EXP')
						#Connect control to metasset
						metaAsset.connectChildren([assetControl],'control')
						metaAsset.connectChildren([expNode], 'expression')
						#Connect gpunodes to metavariants
						for variant in metaVariants:
							for node in gpuTransforms:
								if node in variant.mNode:
									variant.connectChildren([node],'gpuNode')
									break
						mc.select(assetControl, replace=True)
					else:
						self.baseMod.statusBar_update('Asset already being used, use the duplicate function.', alert=True)
				else:
					self.baseMod.statusBar_update('MetaNetwork file not found.', alert=True)
			else:
				self.baseMod.statusBar_update('{} level of detail not found for this variant.'.format(selectedLod), alert=True)
	
	
	def duplicateAsset(self):
		selectedCtrls = mc.ls(sl=True,long=True)
		allNewCtrls = []
		copies= self.copies_SPINBOX.value()
		for ctrl in selectedCtrls:
			shapeNode = mc.listRelatives(ctrl, shapes=True, noIntermediate=True, fullPath=True)[0]
			if mc.nodeType(shapeNode) == 'nurbsCurve':
				origMetaAsset = r9Meta.getConnectedMetaNodes(ctrl, source=True)[0]
				assetPath = self.buildAssetPath(origMetaAsset.assetType, origMetaAsset.assetName, origMetaAsset.subassetName)
				metaNetworkFile = assetPath + '/metaNetwork.ma'
				if os.path.exists(metaNetworkFile):
					for copy in range(copies):
						existingNamespace = origMetaAsset.assetName
						if origMetaAsset.subassetName not in ['',None,'None']:
							existingNamespace = origMetaAsset.subassetName
						#Duplicate Ctrl and GPU
						newCtrl = mc.duplicate(ctrl, name = ctrl, instanceLeaf= True, renameChildren=True)
						children =  newCtrl[1:]
						gpuTransforms = []
						#Digit suffix
						digitCount = 0
						for x in newCtrl[0][::-1]:
							if x.isdigit():
								digitCount += 1
							else:
								break
						for child in children:
							if digitCount > 0:
								newName = ':' + existingNamespace + ':' + child + str( newCtrl[0][len(newCtrl[0])-digitCount:] )
							else:
								newName = ':' + existingNamespace + ':' + child
							gpuTransforms.append(mc.rename(child, newName, ignoreShape=True))
						ctrlAttribute = newCtrl[0] + '.variants'
						expStr = self.formatExpression(ctrlAttribute, gpuTransforms)
						expNode = mc.expression(s=expStr, o=newCtrl[0], alwaysEvaluate=True, uc='all', name = newCtrl[0] + '_EXP')
							
						#Import metanetwork
						networkNodes = mc.file(metaNetworkFile, i=True, namespace=existingNamespace, returnNewNodes=True, mergeNamespacesOnClash=True)
						metaAssets = r9Meta.getMetaNodes(['CGXDressingMetaAsset'])
						metaAsset = ''
						for each in networkNodes:
							for metaNode in metaAssets:
								if metaNode.mNode == each:
									metaAsset = metaNode
									break
							if metaAsset != '':
								break
						metaVariants = metaAsset.getChildMetaNodes()
						#Connect control to metasset
						metaAsset.connectChildren([newCtrl[0]],'control')
						metaAsset.connectChildren([expNode], 'expression')
						#Connect gpunodes to metavariants
						for variant in metaVariants:
							mc.connectAttr(newCtrl[0] + '.LOD', variant.mNode + '.LOD', force=True)
							for node in gpuTransforms:
								if node in variant.mNode:
									variant.connectChildren([node],'gpuNode')
									break
						allNewCtrls.append(newCtrl[0])
			else:
				self.baseMod.statusBar_update('Select controls to duplicate assets.', alert=True)
		mc.select(allNewCtrls, replace=True)
	
	
	def deleteAsset(self):
		selectedCtrls = mc.ls(sl=True,long=True)
		filteredCtrlsSelection = []
		checkNamespace = ''
		for ctrl in selectedCtrls:
			shapeNode = mc.listRelatives(ctrl, shapes=True, noIntermediate=True, fullPath=True)[0]
			if mc.nodeType(shapeNode) == 'nurbsCurve':
				metaNodes = []
				metaAsset = r9Meta.getConnectedMetaNodes(ctrl, source=True)[0]
				checkNamespace = metaAsset.assetName
				if metaAsset.subassetName not in ['',None,'None']:
					checkNamespace = metaAsset.subassetName
				metaNodes.append(metaAsset)
				metaVariants = metaAsset.getChildMetaNodes()
				metaNodes += metaVariants
				for variant in metaVariants:
					metaNodes += variant.getChildMetaNodes()
				
				for each in metaNodes:
					each.delete()
				filteredCtrlsSelection.append(ctrl)
			else:
				self.baseMod.statusBar_update('Select controls ONLY to delete assets.', alert=True)
			
		mc.delete(filteredCtrlsSelection)
		#Delete namespace if empty
		chkInfo = mc.namespaceInfo(checkNamespace, listNamespace=True)
		if chkInfo == None:
			mc.namespace(removeNamespace = checkNamespace)
	
	
	def randomizeVariant(self):
		selectedCtrls = mc.ls(sl=True,long=True)
		for ctrl in selectedCtrls:
			randLen = len(mc.attributeQuery('variants',node=ctrl, listEnum=True)[0].split(':')) - 1
			randVar = random.randint(0,randLen)
			mc.setAttr(ctrl + '.variants', randVar)
	
	
	def formatExpression(self, _ctrlAttribute, _gpuTransforms):
		blocksCount = len(_gpuTransforms)
		posibilities = []
		for i in range(blocksCount):
			stringBlock = ''
			x = 0
			for variant in _gpuTransforms:
				if x == i:
					stringBlock += variant + '.visibility = {};\n'.format(1)
				else:
					stringBlock += variant + '.visibility = {};\n'.format(0)
				x += 1
			posibilities.append(stringBlock)
		
		expStr = ''
		i = 0
		for block in posibilities:
			if i == 0:
				expStr += 'if ({}'.format(_ctrlAttribute) + '== {})'.format(i) + '\n' + '{' + '\n' + block + '\n' + '}'
			else:
				expStr += 'else if ({} '.format(_ctrlAttribute) + '== {})'.format(i) + '\n' + '{' + '\n' + block + '\n' + '}'
			i+=1
		
		return expStr
	
	
	def populateVariants(self):
		assetType = self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
		assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
		subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
		assetPath = self.buildAssetPath(assetType, assetName, subAssetName) + '/Cache'
		#List dirs and look for _VAR items
		foldersList = []
		if assetPath != None:
			if os.path.exists(assetPath):
				foldersList = [ f for f in os.listdir(assetPath) if os.path.isdir(os.path.join(assetPath,f)) ]
		dataList = []
		if len(foldersList) >= 1:
			for each in sorted(foldersList, reverse=True):
				if each.rsplit("_",1)[1] == 'VAR':
					dataList.append(each)
		variantsModel = dvm.ObjectsListModel(dataList, self)
		self.variant_COMBOBOX.setModel(variantsModel)
		
	
	
	def buildAssetPath(self, assetType, assetName, subAssetName):
		propertiesDict = {"concreteFolderType":"published_leaffolder",
						"abstractFolderType":"branchfolder",
						"folderFunction":"published_content",
						"folderConcreteFunction":"published_asset",
						"placeholder":False}
		if subAssetName not in [None,""]:
			propertiesDict = {"concreteFolderType":"published_leaffolder",
						"abstractFolderType":"branchfolder",
						"folderFunction":"published_content",
						"folderConcreteFunction":"published_subasset",
						"placeholder":False}
		publishedNodes = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
		
		assetTypeDict={"name":assetType,
					"concreteFolderType":"assetType_mainfolder",
					"abstractFolderType":"mainfolder",
					"folderFunction":"grouping",
					"folderConcreteFunction":"",
					"placeholder":False}
		assetDict= {"name":"$CharacterName",
				"concreteFolderType":"asset_subfolder",
				"abstractFolderType":"subfolder",
				"folderFunction":"grouping",
				"folderConcreteFunction":"",
				"placeholder":True}
		if assetType == 'PROPS':
			assetDict= {"name":"$PropName",
				"concreteFolderType":"asset_subfolder",
				"abstractFolderType":"subfolder",
				"folderFunction":"grouping",
				"folderConcreteFunction":"",
				"placeholder":True}
		elif assetType == 'VEHICLES':
			assetDict= {"name":"$VehicleName",
				"concreteFolderType":"asset_subfolder",
				"abstractFolderType":"subfolder",
				"folderFunction":"grouping",
				"folderConcreteFunction":"",
				"placeholder":True}
		elif assetType == 'SETS':
			assetDict= {"name":"$SetName",
				"concreteFolderType":"asset_subfolder",
				"abstractFolderType":"subfolder",
				"folderFunction":"grouping",
				"folderConcreteFunction":"",
				"placeholder":True}
		
		subAssetDict= {"name":"$assetName_$subassetname",
					"concreteFolderType":"subasset_subfolder",
					"abstractFolderType":"subfolder",
					"folderFunction":"grouping",
					"folderConcreteFunction":"",
					"placeholder":True}
		for node in publishedNodes:
			pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict})
			if subAssetName not in [None,""]:
				pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict,subAssetName:subAssetDict})
			pathList.append(node.name)
			#Build path string
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
			
			if "$" not in pathStr:
				return pathStr
	
	
	def refreshCustomPath(self):
		if self.customPathComp.customPath_CHKBOX.isChecked():
			self.projAssetsMod.container.setEnabled(False)
		else:
			self.projAssetsMod.container.setEnabled(True)
	
	
	def refreshBlockedComboboxes(self):
		self.projAssetsMod.AssetTypes_Component.setToItem('SETS')
		self.projAssetsMod.FileTypes_Component.setToItem('shading:SHD')


##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/workflows/SetDressing/DressingManager_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()