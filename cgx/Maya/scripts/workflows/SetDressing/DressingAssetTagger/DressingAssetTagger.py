# -*- coding: utf-8 -*-
'''
Dressing asset tagging tool.

Created on June 5, 2017

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
from cgx.gui.Qt import QtWidgets, QtCore, QtGui
from cgx.gui.Qt import __binding__
import Red9.core.Red9_Meta as r9Meta
import cgx.Maya.scripts.maya_libs.Maya_MetaData as cgxMeta
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityLookdev import SanityLookdev
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityModeling import SanityModeling
from cgx.Maya.scripts.maya_libs.Maya_Dependencies import Maya_Dependencies
from cgx.core.Synchronizer import Synchronizer
import cgx.gui.DataViewModels as dvm

import maya.cmds as mc


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
    appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/workflows/SetDressing/DressingAssetTagger'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"
lod_ui = appRootFolder + "/ui/" + "levelOfDetail.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)
lodform, lodbase = uiParser.loadUiType(lod_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
        '''
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
        '''
        super(Main_GUI,self).__init__(parent)
        self.setupUi(self)
        
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        
        self.appTools = appTools
        if self.appTools != None:
            self.initTool()
            self.createComponents()
            self.setConnections()
            self.initComponents()
            self.initUI()
            self.checkPlugins()
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        
        
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def setConnections(self):
        '''
        Connect signals.
        '''
        #BUTTONS
        self.publishDressingAsset_BTN.clicked.connect(self.preSanityCheck)
        #TABLEVIEW
        self.variants_TABLEVIEW.clicked.connect(self.populateLOD)
        #CHKBOX
        self.exportAnimation_CHKBOX.clicked.connect(self.refreshExportAnimation)
        self.customPathComp.customPath_CHKBOX.clicked.connect(self.refreshCustomPath)
        self.publishAllDeps_CHKBOX.clicked.connect(self.refreshPublishDependencies)
        self.minmaxComp.timeSlider_CHKBOX.stateChanged.connect(self.refreshTimeRangeOptions)
        #CONTEXT MENUS
        self.variants_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.variants_TABLEVIEW.customContextMenuRequested.connect(self.variantsOptions)
        self.lod_TREEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.lod_TREEVIEW.customContextMenuRequested.connect(self.lodOptions)
        self.dependencies_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.dependencies_TABLEVIEW.customContextMenuRequested.connect(self.depsOptions)
        
    
    def initTool(self):
        '''
        Initializes tool settings.
        '''
        self.initAttrs()
    
    
    def resetTool(self):
        self.initAttrs()
        self.initComponents()
        self.initUI()
        self.checkPlugins()
        
        
    def initAttrs(self):
        '''
        Initializes variables and components for this tool.
        '''
        self.__client = "defaultClient"
        self.__toolName = "Dressing Asset Tagger"
        self.__toolVersion = __version__
        
        #Set time range
        self.appTools.tRange.handleIn = 0
        self.appTools.tRange.handleOut = 0
        self.appTools.tRange.startFrame = 1
        self.appTools.tRange.endFrame = 1
        
        #Attributes
        self.variantHeaders = ['variant','metaNode']
        self.lodHeaders = ["LOD", "Root/Geo", "internalName", "metaNode"]
        self.lodModels = []
        self.syncStatusDict = {1:"Already published",
                            2:"Published but different",
                            3:"Same date, different size",
                            4:"Different date, same size",
                            5:"Both files missing",
                            6:"Not Published",
                            7:"Current file missing"}
        
    
    def createComponents(self):
        self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
        self.baseMod = self.Base_GUIModulesFactory.createModule('base')
        self.baseMod.MainMenu_Component.edit_config.setVisible(False)
        self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo, tRange=self.appTools.tRange)
        self.projAssetsMod = self.Pipeline_GUIModulesFactory.createModule('ProjAssets')
        self.projAssetsMod.pos = [0,40]
        self.customPathComp = pipeComponents.CustomPath_Component(self,self.centralWidget(), self.sInfo)
        self.customPathComp.pos = [10,195]
        self.fpsComp = pipeComponents.FPS_Component(self,self.centralWidget(), self.pInfo)
        self.fpsComp.pos = [275,390]
        self.minmaxComp = pipeComponents.MinMax_Component(self,self.centralWidget())
        self.minmaxComp.pos = [10,400]
        self.cacheComp = pipeComponents.CacheBakingMethods_Component(self,self.centralWidget())
        self.cacheComp.pos = [10,310]
        
        #Add Remove Existing Tags menu option
        self.edit_removeTags = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
        self.edit_removeTags.setObjectName("edit_removeTags")
        self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[1], self.edit_removeTags)
        className= str(self.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.edit_removeTags.setText(QtWidgets.QApplication.translate(className, "Remove Existing Tags", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.edit_removeTags.setText(QtWidgets.QApplication.translate(className, "Remove Existing Tags", None))
        
    
    def initComponents(self):
        self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
        self.appTools.app_initUI.setCurrentProject(self.projAssetsMod.Project_Component)
        self.appTools.app_initUI.setCurrentAssetType(self.projAssetsMod.AssetTypes_Component)
        self.appTools.app_initUI.setCurrentAsset(self.projAssetsMod.Assets_Component)
        self.appTools.app_initUI.setCurrentSubAsset(self.projAssetsMod.SubAssets_Component)
        self.appTools.app_initUI.setCurrentFileType(self.projAssetsMod.FileTypes_Component)
        self.fpsComp.initComponent(self.pInfo.maya_getFPS())
        self.appTools.app_initUI.checkFPS(self.fpsComp)
        self.cacheComp.container.setEnabled(False)
        self.minmaxComp.container.setEnabled(False)
        self.fpsComp.container.setEnabled(False)
        self.timeRange_LABEL.setEnabled(False)
        self.projAssetsMod.AssetTypes_Component.setToItem('SETS')
        self.projAssetsMod.AssetTypes_Component.container.setEnabled(False)
        self.projAssetsMod.FileTypes_Component.setToItem('shading:SHD')
        self.projAssetsMod.FileTypes_Component.container.setEnabled(False)
    
    
    def initUI(self):
        '''
        Initialize GUI components.
        '''
        self.refreshVariants()
        self.refreshTimeRangeOptions()
        self.populateDependencies()
        
        
    def checkPlugins(self):
        '''
        Check if plugins are loaded.
        :return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
        :rtype: dict
        '''
        if not mc.pluginInfo("MayaExocortexAlembic", query=True, loaded=True):
            self.baseMod.statusBar_update("Exocortex Alembic plugin not loaded.", warning=True)    
            return False
        elif not mc.pluginInfo("mtoa", query=True, loaded=True):
            self.baseMod.statusBar_update("MToA plugin not loaded.", warning=True)
            return False   
        else:
            return True
        
    
    def preSanityCheck(self):
        totalIssues, finalDataList = self.sanityCheck()
        if totalIssues >= 1:
            self.checkListDialog(finalDataList, totalIssues, self)
        else:
            self.publishDressingAsset()
    
    
    def publishDressingAsset(self):
        metaVariants = r9Meta.getMetaNodes(['CGXDressingMetaVariant'])
        if len(metaVariants) >= 1:
            mc.file(save=True, force=True)
            origFile = mc.file(expandName= True, query= True)
            assetFolder = self.buildAssetPath()     
                   
            #EXPORT SHADERS
            shaderFiles = []
            for variant in metaVariants:
                metaGeos = variant.getChildMetaNodes()
                shadersList = []
                for node in metaGeos:
                    shadersList.append(node.shader)
                shadersSet = set(shadersList)
                shadersList = list(shadersSet)
                mc.select(shadersList, replace=True)
                shadersFile = assetFolder + '/' + variant.mNodeID.rsplit('_',1)[0] + '_shaders.ma'
                variant.shaders_file = shadersFile
                shaderFiles.append(shadersFile)
                mc.file(shadersFile, options='v=1;', preserveReferences=True, force=True, exportSelected=True, typ='mayaAscii')
            self.baseMod.statusBar_update("Exported shaders to {}".format(assetFolder), status=True)
            
            #EXPORT ALEMBICS
            if self.exportAnimation_CHKBOX.isChecked():
                self.appTools.tRange.startFrame = self.minmaxComp.timeRangeMin_SPINBOX.value()
                self.appTools.tRange.endFrame = self.minmaxComp.timeRangeMax_SPINBOX.value()
            else:
                self.appTools.tRange.startFrame = 1
                self.appTools.tRange.endFrame = 1
            self.exportCache()
            
            #EXPORT METANETWORK
            assetType = self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
            assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
            subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
            metaNodeName = assetName + "_META"
            if subAssetName not in [None,'','None']:
                metaNodeName = subAssetName + "_META"
            metaAsset = cgxMeta.CGXDressingMetaAsset(name=metaNodeName)
            metaAsset.assetType = assetType
            metaAsset.assetName = assetName
            metaAsset.subassetName = subAssetName
            if self.exportAnimation_CHKBOX.isChecked():
                metaAsset.animation = True
            metaAsset.connectChildren(metaVariants, 'variants')
            metaGeos = r9Meta.getMetaNodes(['CGXDressingMetaGeo'])
            metaNodesList = metaVariants + metaGeos
            metaNodesList.append(metaAsset)
            selList = [x.mNode for x in metaNodesList]
            mc.select(selList, replace=True)
            metaFile = assetFolder + '/metaNetwork.ma'
            mc.file(metaFile, options='v=1;', preserveReferences=True, force=True, exportSelected=True, typ='mayaAscii')
            metaAsset.delete()
            self.baseMod.statusBar_update("Exported Meta Network to {}".format(metaFile), status=True)
            
            #COPY DEPENDENCIES
            for each in shaderFiles:
                mc.file(each, open=True, force=True, prompt= False)
                self.publishDependencies(assetFolder)
                mc.file(save=True, force=True)
            #RE-OPEN ORIGINAL FILE
            mc.file(origFile, open=True, force=True, prompt= False)
            self.baseMod.statusBar_update("Done publishing dressing asset.", success=True)
        else:
            self.baseMod.statusBar_update("No Meta Variants found on this scene.", warning=True)
    
    
    def publishDependencies(self, trgPath):
        sync = Synchronizer(self)
        sync.statusDict = self.syncStatusDict
        
        #List selected deps
        selected = self.dependencies_TABLEVIEW.selectedIndexes()
        selRowsDirty = []
        for item in selected:
            row = item.row()
            selRowsDirty.append(row)
        selSet = set(selRowsDirty)
        selRows = list(selSet)
        if self.publishAllDeps_CHKBOX.isChecked():
            selRows = range(len(self.dependencies_TABLEVIEW.model().dataList))
        
        lastIndex = 0
        mayaDeps = Maya_Dependencies()
        repeatedDependencies = []
        for index in selRows:
            rowDict = {}
            i = 0
            for header in self.dependencies_TABLEVIEW.model().headers:
                rowDict[header] = self.dependencies_TABLEVIEW.model().dataList[index][i]
                i += 1
            srcPath = rowDict["File"]
            fileName = rowDict["File"].rsplit("/", 1)[1]
            
            #DO NOT COPY THE SAME FILE OVER AND OVER IF IT'S USED BY DIFFERENT NODES
            syncFile = True
            if srcPath in repeatedDependencies: 
                self.baseMod.statusBar_update("Skipped dependency, already copied: " + srcPath, status=True)
                syncFile = False
            else:
                repeatedDependencies.append(srcPath)
            if mc.objExists(rowDict["Node"]):
                if rowDict["Type"] in ["Texture","Image plane"]:
                    finalPath = trgPath + "/Maps/" + fileName
                    sequenceStatus = mc.getAttr(rowDict["Node"] + ".useFrameExtension")
                    tileStatus = 0
                    if mc.attributeQuery("uvTilingMode", node=rowDict["Node"],exists=True):
                        tileStatus = mc.getAttr(rowDict["Node"] + ".uvTilingMode")
                    if sequenceStatus == True or tileStatus != 0:
                        if syncFile:
                            self.skipByStatus(sync, srcPath, finalPath, isSequence=True)
                        #Change node filepath to published location
                        mayaDeps.fileAttrChange(finalPath, rowDict["Node"], rowDict["Type"])
                    else:
                        if syncFile:
                            self.skipByStatus(sync, srcPath, finalPath, isTexture=True)
                        #Change node filepath to published location
                        mayaDeps.fileAttrChange(finalPath, rowDict["Node"], rowDict["Type"])
                elif rowDict["Type"] in ["Alembic","Exocortex","Point cache"]:
                    finalPath = trgPath + "/Cache/" + fileName
                    if syncFile:
                        self.skipByStatus(sync, srcPath, finalPath)
                    #Change node filepath to published location
                    mayaDeps.fileAttrChange(finalPath, rowDict["Node"], rowDict["Type"])
                elif rowDict["Type"] in ["Reference"]:
                    finalPath = trgPath + "/Reference/" + fileName
                    if syncFile:
                        self.skipByStatus(sync, srcPath, finalPath)
                    #Change node filepath to published location
                    mayaDeps.fileAttrChange(finalPath, rowDict["Node"], rowDict["Type"])
                elif rowDict["Type"] in ["Audio"]:
                    finalPath = trgPath + "/Audio/" + fileName
                    if syncFile:
                        self.skipByStatus(sync, srcPath, finalPath)
                    #Change node filepath to published location
                    mayaDeps.fileAttrChange(finalPath, rowDict["Node"], rowDict["Type"])
                elif rowDict["Type"] in ["Yeti cache file"]:
                    finalPath = trgPath + "/Hair/" + fileName
                    if syncFile:    
                        self.skipByStatus(sync, srcPath, finalPath)
                    #Change node filepath to published location
                    mayaDeps.fileAttrChange(finalPath, rowDict["Node"], rowDict["Type"])
                elif rowDict["Type"] in ["Yeti texture"]:
                    finalPath = trgPath + "/Maps/" + fileName
                    if syncFile:    
                        self.skipByStatus(sync, srcPath, finalPath, isTexture=True)
                    #Change node filepath to published location
                    mayaDeps.fileAttrChange(trgPath + "/Maps", rowDict["Node"], rowDict["Type"])
                elif rowDict["Type"] in ["Custom"]:
                    finalPath = trgPath + "/Custom/" + fileName
                    if syncFile:    
                        self.skipByStatus(sync, srcPath, finalPath, isTexture=True)
    
    
    def skipByStatus(self, sync, _source, _target, **kwargs):
        if sync.getSyncStatus(_source, _target) == "In sync":
            self.baseMod.statusBar_update("Dependency skipped, already existed: " + _target, status=True)
        else:
            self.baseMod.statusBar_update("Processing dependency: " + _source, status=True)
            if 'isSequence' in kwargs.keys():
                if kwargs['isSequence']:
                    sync.processPaths(_source, _target, isSequence=True)
            elif 'isTexture' in kwargs.keys():
                if kwargs['isTexture']:
                    sync.processPaths(_source, _target, isTexture=True)
            else:
                sync.processPaths(_source, _target)
    
    
    def exportCache(self):
        check = self.checkPlugins()
        if check:
            #Stop refresh
            mc.refresh(suspend=True)
            currentEvalManager = mc.evaluationManager( query=True, mode=True )[0]
            mc.evaluationManager(mode='off')
            
            assetType = self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
            assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
            subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
            
            #Collect geometry for baking and create Baking Method object
            variants = self.variants_TABLEVIEW.model().dataList
            
            for i in range(len(variants)):
                geoDict = {} #GEO_CURRENT_NAME = [GEO_USER_NAME, rootName, rootNode]. Using node name to store reference to real object, and datalist for user values
                variantModel = self.lodModels[i]
                roots = variantModel.dataList.children
                #CREATE FOLDERS
                cachePath = self.buildCachePath(assetType, assetName, subAssetName)
                cachePath += '/' + variants[i][self.variantHeaders.index('variant')]
                if not os.path.exists(cachePath):
                    os.makedirs(cachePath)
                for each in roots:
                    childGeom = each.children
                    for item in childGeom:
                        geoCurrentName = item.data[2]
                        geoUserName = item.name
                        if geoCurrentName in geoDict:
                            self.baseMod.statusBar_update("Found duplicated names in the project. Some geometry will be missing after exporting.", warning=True)
                        else:
                            geoDict[geoCurrentName] = [geoUserName, each.name, each]
                bakingMethods = self.appTools.createBakingMethods(geoDict, QtWidgets.QProgressBar(), 1)
                
                #FILE PER ROOTNODE
                #Bake geometry first, then export
                #Create a list of used roots
                rootList = []
                for e in geoDict.keys():
                    if geoDict[e][2] not in rootList:
                        rootList.append(geoDict[e][2])
                #Loop through rootList and compare to geoDict to bake and export
                for rootNode in rootList:
                    rootName = rootNode.name
                    bakedGeomList = []
                    for e in geoDict.keys():
                        if geoDict[e][2] == rootNode:
                            #Bake geometry
                            bakedGeom = ""
                            if self.cacheComp.vertexBake_RBTN.isChecked():
                                bakedGeom = bakingMethods.vertexBakeMethod(e)
                                bakedGeomList.append(bakedGeom)
                            elif self.cacheComp.alembicBake_RBTN.isChecked():
                                bakedGeom = e
                                bakedGeomList.append(bakedGeom)
                    exporter = self.appTools.createExporter(bakedGeomList, self.appTools.tRange, QtWidgets.QProgressBar(), cachePath, 1)
                    #Export baked geom list
                    cleanName = '{}_bkd_1'.format(rootName)
                    if subAssetName not in ['',None,'None']:
                        cleanName = '{}_bkd_1'.format(rootName)
                    if self.cacheComp.alembicBake_RBTN.isChecked():
                        exporter.exportAlembicBakeExo(cleanName, True, True)
                    else:
                        exporter.exportAlembicExo(cleanName, True, True)
                    metaVariant = variants[i][self.variantHeaders.index('metaNode')]
                    if rootName == 'LOD_HIGH':
                        metaVariant.LOD_HIGH_file = cachePath + '/' + cleanName + '.abc'
                    elif rootName == 'LOD_MID':
                        metaVariant.LOD_MID_file = cachePath + '/' + cleanName + '.abc'
                    elif rootName == 'LOD_LOW':
                        metaVariant.LOD_LOW_file = cachePath + '/' + cleanName + '.abc'

                    #Delete baked geometry
                    if not self.cacheComp.alembicBake_RBTN.isChecked():
                        mc.delete(bakedGeomList)
            
            #Restart refresh
            mc.refresh(suspend=False)
            mc.evaluationManager(mode=currentEvalManager)
            
            #Done!!
            self.baseMod.statusBar_update("Done exporting caches.", status=True)
    
    
    def sanityCheck (self):
        """Runs the sanity check modules over the current file"""
        
        checkSelectedOnly = True
        checkModNodes = []
        existingMetaGeos = r9Meta.getMetaNodes(['CGXDressingMetaGeo'])
        checkSHDNodes = []
        for metaGeo in existingMetaGeos:
            checkModNodes.append(metaGeo.geoName)
            checkSHDNodes.append(metaGeo.shader)
        finalDataList = []
        
        #MODELING
        self.checkModeling = SanityModeling([], checkModNodes, checkSelectedOnly)
        checkModelingResult =  self.checkModeling.check(self.checkModeling.checkMethods, checkModNodes, checkSelectedOnly)
            
        #SHADING-MODELS
        self.checkLookdev = SanityLookdev([], checkModNodes, checkSelectedOnly)
        checkLookdevModelsResult = self.checkLookdev.check(self.checkLookdev.checkMethods, checkModNodes, checkSelectedOnly)
        
        #SHADING-SHADERS
        checkSHDNodesSet = set(checkSHDNodes)
        checkSHDNodes = list(checkSHDNodesSet)
        self.checkLookdev = SanityLookdev([], checkSHDNodes, checkSelectedOnly)
        checkLookdevShadersResult = self.checkLookdev.check(self.checkLookdev.checkMethods, checkSHDNodes, checkSelectedOnly)
        
        totalIssues = checkModelingResult[1] + checkLookdevModelsResult[1] + checkLookdevShadersResult[1]
        if totalIssues >= 1:
            finalDataList = checkModelingResult[0] + checkLookdevModelsResult[0] + checkLookdevShadersResult[0]
        
        return totalIssues, finalDataList
    
    
    def refreshVariants(self):
        metaVariants = r9Meta.getMetaNodes(['CGXDressingMetaVariant'])
        if len(metaVariants) >= 1:
            self.populateVariants()
        else:
            self.autoCreateVariants()
            
    
    def autoCreateVariants(self):
        #VARIANT NAMING CONVENTION, TRANSFORM WHOSE PARENT IS WORLDSPACE: XXX_VAR
        variantsList = []
        for each in mc.ls(type='transform'):
            if each[-3:] == 'VAR':
                variantsList.append(each)
        
        dataList = []
        if len(variantsList) >= 1:
            for variant in variantsList:
                rowData = []
                for column in self.variantHeaders:
                    if column == "variant":
                        rowData.append(variant)
                    elif column == "metaNode":
                        metaVariant = cgxMeta.CGXDressingMetaVariant(name=variant + "_META")
                        metaVariant.connectChildren([variant], 'taggedNode')
                        rowData.append(metaVariant)
                dataList.append(rowData)
            
        variantsModel = VariantsDataTableModel(dataList, self.variantHeaders, self)
        self.variants_TABLEVIEW.setModel(variantsModel)
        self.variants_TABLEVIEW.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.variants_TABLEVIEW.setCurrentIndex(self.variants_TABLEVIEW.model().index(0,0))
        self.variants_TABLEVIEW.hideColumn(1)
        htalHeader = self.variants_TABLEVIEW.horizontalHeader()
        htalHeader.setStretchLastSection(True)
        for col in range(variantsModel.columnCount()):
            self.variants_TABLEVIEW.resizeColumnToContents(col)
        
        #Init models
        self.refreshLODModels()
    

    def populateVariants(self):
        metaVariants = r9Meta.getMetaNodes(['CGXDressingMetaVariant'])
        dataList = []
        if len(metaVariants) >= 1:
            for variant in metaVariants:
                rowData = []
                for column in self.variantHeaders:
                    if column == "variant":
                        childDict = variant.getChildren(asMap=True)
                        keyName = variant.mNode + '.taggedNode'
                        transform = childDict[keyName][0]
                        if "|" in transform:
                            transform = transform.rsplit("|",1)[1]
                        rowData.append(transform)
                    elif column == "metaNode":
                        rowData.append(variant)
                dataList.append(rowData)
            
        variantsModel = VariantsDataTableModel(dataList, self.variantHeaders, self)
        self.variants_TABLEVIEW.setModel(variantsModel)
        self.variants_TABLEVIEW.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.variants_TABLEVIEW.setCurrentIndex(self.variants_TABLEVIEW.model().index(0,0))
        self.variants_TABLEVIEW.hideColumn(1)
        htalHeader = self.variants_TABLEVIEW.horizontalHeader()
        htalHeader.setStretchLastSection(True)
        for col in range(variantsModel.columnCount()):
            self.variants_TABLEVIEW.resizeColumnToContents(col)
        
        #Init models
        self.refreshLODModels()
    
    
    def refreshLODModels(self):
        variants = self.variants_TABLEVIEW.model().dataList
        if len(variants) >= 1:
            for variant in variants:
                lodRoot = dvm.TreeNode(["LOD", "Root/Geo", "internalName", "metaNode"], self.lodHeaders, parent=None)
                lodModel = LODDataTreeModel(lodRoot, self.lodHeaders, self.lod_TREEVIEW)
                lodList = ['LOD_HIGH','LOD_MID','LOD_LOW']
                metaNode = variant[self.variantHeaders.index('metaNode')]
                for lodLevel in lodList:
                    if len(metaNode.getMetaGeo(lodLevel)) >= 1:
                        self.initLODModels(variant, lodLevel, False, lodRoot, lodModel)
                    else:
                        self.initLODModels(variant, lodLevel, True, lodRoot, lodModel)
                self.lodModels.append(lodModel)
                for col in range(lodModel.columnCount()):
                    self.lod_TREEVIEW.resizeColumnToContents(col)
            
            self.populateLOD()
    
    
    def initLODModels(self, _variant, _lodLevel, _autoCreate, _lodRoot, _lodModel):
        #LOD NAMING CONVENTION IS LOD_HIGH LOD_MID LOD_LOW
        metaNode = _variant[self.variantHeaders.index('metaNode')]
        variant = _variant[self.variantHeaders.index('variant')]
        transforms = mc.listRelatives(variant, children=True, type='transform', fullPath=True)
        #Find all transforms grouping geos using given naming conventions
        geosLOD_HIGH = []
        geosLOD_MID = []
        geosLOD_LOW = []
        for lodTransform in transforms:
            if 'LOD_HIGH' in lodTransform:
                geosLOD_HIGH = mc.listRelatives(lodTransform, allDescendents= True, type='mesh', noIntermediate=True, fullPath=True)
            if 'LOD_MID' in lodTransform:
                geosLOD_MID = mc.listRelatives(lodTransform, allDescendents= True, type='mesh', noIntermediate=True, fullPath=True)
            if 'LOD_LOW' in lodTransform:
                geosLOD_LOW = mc.listRelatives(lodTransform, allDescendents= True, type='mesh', noIntermediate=True, fullPath=True)
        if _lodLevel == 'LOD_HIGH':
            if _autoCreate and len(geosLOD_HIGH) >= 1:
                LODTreeNode = _lodModel.insertRows(_lodRoot.childCount(), 1,['LOD_HIGH','root','LOD_HIGH',metaNode], _lodModel.indexFromNode(_lodRoot))
                for geo in geosLOD_HIGH:
                    transform = mc.listRelatives(geo, parent=True, fullPath=True)[0]
                    #Create metaGeo and fill its attributes, then insert in tree
                    shortName = transform
                    if "|" in transform:
                        shortName = transform.rsplit("|",1)[1]
                    shortName = transform.rsplit('|',1)[1]
                    metaGeo = cgxMeta.CGXDressingMetaGeo(name=shortName + "_META")
                    metaGeo.geoName = shortName
                    shaderGroup = mc.listConnections(geo, type= "shadingEngine")[0]
                    shaderNode = mc.listConnections(shaderGroup + ".surfaceShader")[0]
                    metaGeo.shader = shaderNode
                    metaGeo.aiSubdivType = mc.getAttr(geo + '.aiSubdivType')
                    metaGeo.aiSubdivIterations =mc.getAttr(geo + '.aiSubdivIterations')
                    metaGeo.aiToggleSubdivType = mc.getAttr(geo + '.aiSubdivType')
                    metaGeo.aiToggleSubdivIterations = mc.getAttr(geo + '.aiSubdivIterations')
                    metaGeo.aiDispHeight = mc.getAttr(geo + '.aiDispHeight')
                    metaGeo.aiDispPadding = mc.getAttr(geo + '.aiDispPadding')
                    metaGeo.aiDispZeroValue = mc.getAttr(geo + '.aiDispZeroValue')
                    metaGeo.aiOpaque = mc.getAttr(geo + '.aiOpaque')
                    metaGeo.connectChildren([transform], 'taggedNode')
                    metaNode.connectChildren([metaGeo], 'LOD_HIGH')
                    _lodModel.insertRows(LODTreeNode.childCount(), 1,[shortName,'geo',metaGeo.geoName,metaGeo], _lodModel.indexFromNode(LODTreeNode))
            else:
                if len(metaNode.getMetaGeo('LOD_HIGH')) >= 1:
                    LODTreeNode = _lodModel.insertRows(_lodRoot.childCount(), 1,['LOD_HIGH','root','LOD_HIGH',metaNode], _lodModel.indexFromNode(_lodRoot))
                    for metaGeo in metaNode.getMetaGeo('LOD_HIGH'):
                        updatedGeoName = metaGeo.getChildren(mAttrs='taggedNode')
                        if len(updatedGeoName) == 1:
                            if "|" in updatedGeoName[0]:
                                updatedGeoName[0] = updatedGeoName[0].rsplit('|',1)[1]
                            metaGeo.geoName = updatedGeoName[0]
                        shortName = metaGeo.geoName
                        if "|" in shortName:
                            shortName = shortName.rsplit('|',1)[1]
                        _lodModel.insertRows(LODTreeNode.childCount(), 1,[shortName,'geo',metaGeo.geoName,metaGeo], _lodModel.indexFromNode(LODTreeNode))
        if _lodLevel == 'LOD_MID':
            if _autoCreate and len(geosLOD_MID) >= 1:
                LODTreeNode = _lodModel.insertRows(_lodRoot.childCount(), 1,['LOD_MID','root','LOD_MID',metaNode], _lodModel.indexFromNode(_lodRoot))
                for geo in geosLOD_MID:
                    transform = mc.listRelatives(geo, parent=True, fullPath=True)[0]
                    #Create metaGeo and fill its attributes, then insert in tree
                    shortName = transform
                    if "|" in transform:
                        shortName = transform.rsplit("|",1)[1]
                    metaGeo = cgxMeta.CGXDressingMetaGeo(name=shortName + "_META")
                    metaGeo.geoName = shortName
                    shaderGroup = mc.listConnections(geo, type= "shadingEngine")[0]
                    shaderNode = mc.listConnections(shaderGroup + ".surfaceShader")[0]
                    metaGeo.shader = shaderNode
                    metaGeo.aiSubdivType = mc.getAttr(geo + '.aiSubdivType')
                    metaGeo.aiSubdivIterations =mc.getAttr(geo + '.aiSubdivIterations')
                    metaGeo.aiToggleSubdivType = mc.getAttr(geo + '.aiSubdivType')
                    metaGeo.aiToggleSubdivIterations = mc.getAttr(geo + '.aiSubdivIterations')
                    metaGeo.aiDispHeight = mc.getAttr(geo + '.aiDispHeight')
                    metaGeo.aiDispPadding = mc.getAttr(geo + '.aiDispPadding')
                    metaGeo.aiDispZeroValue = mc.getAttr(geo + '.aiDispZeroValue')
                    metaGeo.aiOpaque = mc.getAttr(geo + '.aiOpaque')
                    metaGeo.connectChildren([transform], 'taggedNode')
                    metaNode.connectChildren([metaGeo], 'LOD_MID')
                    _lodModel.insertRows(LODTreeNode.childCount(), 1,[shortName,'geo',metaGeo.geoName,metaGeo], _lodModel.indexFromNode(LODTreeNode))
            else:
                if len(metaNode.getMetaGeo('LOD_MID')) >= 1:
                    LODTreeNode = _lodModel.insertRows(_lodRoot.childCount(), 1,['LOD_MID','root','LOD_MID',metaNode], _lodModel.indexFromNode(_lodRoot))
                    for metaGeo in metaNode.getMetaGeo('LOD_MID'):
                        updatedGeoName = metaGeo.getChildren(mAttrs='taggedNode')
                        if len(updatedGeoName) == 1:
                            if "|" in updatedGeoName[0]:
                                updatedGeoName[0] = updatedGeoName[0].rsplit('|',1)[1]
                            metaGeo.geoName = updatedGeoName[0]
                        shortName = metaGeo.geoName
                        if "|" in shortName:
                            shortName = shortName.rsplit('|',1)[1]
                        _lodModel.insertRows(LODTreeNode.childCount(), 1,[shortName,'geo',metaGeo.geoName,metaGeo], _lodModel.indexFromNode(LODTreeNode))
        if _lodLevel == 'LOD_LOW':
            if _autoCreate and len(geosLOD_LOW) >= 1:
                LODTreeNode = _lodModel.insertRows(_lodRoot.childCount(), 1,['LOD_LOW','root','LOD_LOW',metaNode], _lodModel.indexFromNode(_lodRoot))
                for geo in geosLOD_LOW:
                    transform = mc.listRelatives(geo, parent=True, fullPath=True)[0]
                    #Create metaGeo and fill its attributes, then insert in tree
                    shortName = transform
                    if "|" in transform:
                        shortName = transform.rsplit("|",1)[1]
                    metaGeo = cgxMeta.CGXDressingMetaGeo(name=shortName + "_META")
                    metaGeo.geoName = shortName
                    shaderGroup = mc.listConnections(geo, type= "shadingEngine")[0]
                    shaderNode = mc.listConnections(shaderGroup + ".surfaceShader")[0]
                    metaGeo.shader = shaderNode
                    metaGeo.aiSubdivType = mc.getAttr(geo + '.aiSubdivType')
                    metaGeo.aiSubdivIterations =mc.getAttr(geo + '.aiSubdivIterations')
                    metaGeo.aiToggleSubdivType = mc.getAttr(geo + '.aiSubdivType')
                    metaGeo.aiToggleSubdivIterations = mc.getAttr(geo + '.aiSubdivIterations')
                    metaGeo.aiDispHeight = mc.getAttr(geo + '.aiDispHeight')
                    metaGeo.aiDispPadding = mc.getAttr(geo + '.aiDispPadding')
                    metaGeo.aiDispZeroValue = mc.getAttr(geo + '.aiDispZeroValue')
                    metaGeo.aiOpaque = mc.getAttr(geo + '.aiOpaque')
                    metaGeo.connectChildren([transform], 'taggedNode')
                    metaNode.connectChildren([metaGeo], 'LOD_LOW')
                    _lodModel.insertRows(LODTreeNode.childCount(), 1,[shortName,'geo',metaGeo.geoName,metaGeo], _lodModel.indexFromNode(LODTreeNode))
            else:
                if len(metaNode.getMetaGeo('LOD_LOW')) >= 1:
                    LODTreeNode = _lodModel.insertRows(_lodRoot.childCount(), 1,['LOD_LOW','root','LOD_LOW',metaNode], _lodModel.indexFromNode(_lodRoot))
                    for metaGeo in metaNode.getMetaGeo('LOD_LOW'):
                        updatedGeoName = metaGeo.getChildren(mAttrs='taggedNode')
                        if len(updatedGeoName) == 1:
                            if "|" in updatedGeoName[0]:
                                updatedGeoName[0] = updatedGeoName[0].rsplit('|',1)[1]
                            metaGeo.geoName = updatedGeoName[0]
                        shortName = metaGeo.geoName
                        if "|" in shortName:
                            shortName = shortName.rsplit('|',1)[1]
                        _lodModel.insertRows(LODTreeNode.childCount(), 1,[shortName,'geo',metaGeo.geoName,metaGeo], _lodModel.indexFromNode(LODTreeNode))
        
        
    def populateLOD(self):
        selected = self.variants_TABLEVIEW.selectedIndexes()
        selRowsDirty = []
        for item in selected:
            thisRow = item.row()
            selRowsDirty.append(thisRow)
        selSet = set(selRowsDirty)
        selRows = list(selSet)
        selectedVariant = selRows[0]
        thisModel = self.lodModels[selectedVariant]
        self.lod_TREEVIEW.setModel(thisModel)
        
        self.lod_TREEVIEW.hideColumn(1)
        self.lod_TREEVIEW.hideColumn(2)
        self.lod_TREEVIEW.hideColumn(3)
        self.lod_TREEVIEW.setSelectionMode(self.lod_TREEVIEW.MultiSelection)
        self.lod_TREEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.lod_TREEVIEW.expandAll()
    
    
    def populateDependencies(self):
        projDeps = Maya_Dependencies()
        headers = ["Type", "File", "Node"]
        dataList = []
        
        #Alembic files
        for depFile in projDeps.alembicFiles:
            rowDataList = []
            for column in headers:
                if column == "Type":
                    rowDataList.append("Alembic")
                elif column == "File":
                    rowDataList.append(depFile["filePath"])
                elif column == "Node":
                    rowDataList.append(depFile["node"])
            dataList.append(rowDataList)
        #Exocortex Alembic files
        for depFile in projDeps.exocortexAlembicFiles:
            rowDataList = []
            for column in headers:
                if column == "Type":
                    rowDataList.append("Exocortex")
                elif column == "File":
                    rowDataList.append(depFile["filePath"])
                elif column == "Node":
                    rowDataList.append(depFile["node"])
            dataList.append(rowDataList)
        #Audio files
        for depFile in projDeps.audioFiles:
            rowDataList = []
            for column in headers:
                if column == "Type":
                    rowDataList.append("Audio")
                elif column == "File":
                    rowDataList.append(depFile["filePath"])
                elif column == "Node":
                    rowDataList.append(depFile["node"])
            dataList.append(rowDataList)
        #Image plane files
        for depFile in projDeps.imagePlaneFiles:
            rowDataList = []
            for column in headers:
                if column == "Type":
                    rowDataList.append("Image plane")
                elif column == "File":
                    rowDataList.append(depFile["filePath"])
                elif column == "Node":
                    rowDataList.append(depFile["node"])
            dataList.append(rowDataList)
        #Point cache files
        for depFile in projDeps.pointCacheFiles:
            rowDataList = []
            for column in headers:
                if column == "Type":
                    rowDataList.append("Point cache")
                elif column == "File":
                    rowDataList.append(depFile["xmlPath"])
                elif column == "Node":
                    rowDataList.append(depFile["node"])
            dataList.append(rowDataList)
            rowDataList = []
            for column in headers:
                if column == "Type":
                    rowDataList.append("Point cache")
                elif column == "File":
                    rowDataList.append(depFile["mcxPath"])
                elif column == "Node":
                    rowDataList.append(depFile["node"])
            dataList.append(rowDataList)
        #Reference files
        for depFile in projDeps.referenceFiles:
            rowDataList = []
            for column in headers:
                if column == "Type":
                    rowDataList.append("Reference")
                elif column == "File":
                    rowDataList.append(depFile["filePath"])
                elif column == "Node":
                    rowDataList.append(depFile["node"])
            dataList.append(rowDataList)
        #Texture files
        for depFile in projDeps.textureFiles:
            rowDataList = []
            for column in headers:
                if column == "Type":
                    rowDataList.append("Texture")
                elif column == "File":
                    rowDataList.append(depFile["filePath"])
                elif column == "Node":
                    rowDataList.append(depFile["node"])
            dataList.append(rowDataList)
        #Yeti files
        for depFile in projDeps.yetiFiles:
            if depFile["cacheFileName"] != "":
                rowDataList = []
                for column in headers:
                    if column == "Type":
                        rowDataList.append("Yeti cache file")
                    elif column == "File":
                        rowDataList.append(depFile["cacheFileName"])
                    elif column == "Node":
                        rowDataList.append(depFile["node"])
                dataList.append(rowDataList)
        #Yeti textures
        for depFile in projDeps.yetiTextureFiles:
            rowDataList = []
            for column in headers:
                if column == "Type":
                    rowDataList.append("Yeti texture")
                elif column == "File":
                    rowDataList.append(depFile["filePath"])
                elif column == "Node":
                    rowDataList.append(depFile["node"])
            dataList.append(rowDataList)


        depsModel = dvm.DataTableModel(dataList, headers)
        self.dependencies_TABLEVIEW.setModel(depsModel)
        self.dependencies_TABLEVIEW.resizeColumnsToContents()
        self.dependencies_TABLEVIEW.selectRow(0)
        self.dependencies_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        htalHeader = self.dependencies_TABLEVIEW.horizontalHeader()
        htalHeader.setStretchLastSection(True)
    
    
    def refreshExportAnimation(self):
        if self.exportAnimation_CHKBOX.isChecked():
            self.cacheComp.container.setEnabled(True)
            self.minmaxComp.container.setEnabled(True)
            self.fpsComp.container.setEnabled(True)
            self.timeRange_LABEL.setEnabled(True)
        else:
            self.cacheComp.container.setEnabled(False)
            self.minmaxComp.container.setEnabled(False)
            self.fpsComp.container.setEnabled(False)
            self.timeRange_LABEL.setEnabled(False)
    
    
    def refreshCustomPath(self):
        if self.customPathComp.customPath_CHKBOX.isChecked():
            self.projAssetsMod.container.setEnabled(False)
        else:
            self.projAssetsMod.container.setEnabled(True)
            
    
    def refreshPublishDependencies(self):
        if self.publishAllDeps_CHKBOX.isChecked():
            self.dependencies_TABLEVIEW.setEnabled(False)
        else:
            self.dependencies_TABLEVIEW.setEnabled(True)
            
    
    def refreshTimeRangeOptions(self):
        '''
        Refresh MinMax components if Use time slider is selected.
        '''
        if self.minmaxComp.timeSlider_CHKBOX.isChecked():
            self.appTools.tRange.setFramesToTimeSlider()
            self.minmaxComp.timeRangeMin_SPINBOX.setValue(self.appTools.tRange.startFrame)
            self.minmaxComp.timeRangeMax_SPINBOX.setValue(self.appTools.tRange.endFrame)
            
    
    def checkListDialog(self, _list, _totalIssues, _parent):
        chekDialog = CheckList_DialogGUI(_list, _totalIssues, _parent)
        chekDialog.show()
    
    
    def buildCachePath(self, _assetType, _assetName, _subAssetName):
        propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
        if _subAssetName not in [None,""]:
            propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_subasset",
                        "placeholder":False}
        publishedNodes = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
        
        assetTypeDict={"name":_assetType,
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
        assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        if _assetType == 'PROPS':
            assetDict= {"name":"$PropName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        elif _assetType == 'VEHICLES':
            assetDict= {"name":"$VehicleName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        elif _assetType == 'SETS':
            assetDict= {"name":"$SetName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
        for node in publishedNodes:
            pathList = self.pInfo.rebuildPath(node, {_assetType:assetTypeDict,_assetName:assetDict})
            if _subAssetName not in [None,""]:
                pathList = self.pInfo.rebuildPath(node, {_assetType:assetTypeDict,_assetName:assetDict,_subAssetName:subAssetDict})
            pathList.append(node.name)
            #Build path string
            server = self.sInfo.getProjectsFrom()
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
            
            if "$" not in pathStr:
                return pathStr + "/Cache"
    
    
    def buildAssetPath(self):
        assetType = self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
        assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
        subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
        
        propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
        if subAssetName not in [None,""]:
            propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_subasset",
                        "placeholder":False}
        publishedNodes = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
        
        assetTypeDict={"name":assetType,
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
        assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        if assetType == 'PROPS':
            assetDict= {"name":"$PropName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        elif assetType == 'VEHICLES':
            assetDict= {"name":"$VehicleName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        elif assetType == 'SETS':
            assetDict= {"name":"$SetName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        
        subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
        for node in publishedNodes:
            pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict})
            if subAssetName not in [None,""]:
                pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict,subAssetName:subAssetDict})
            pathList.append(node.name)
            #Build path string
            server = self.sInfo.getProjectsFrom()
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
            
            if "$" not in pathStr:
                return pathStr
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods for adding/removing variants
    ##--------------------------------------------------------------------------------------------     
    def variantsOptions (self,pos):
        '''
        Right-click popup menu for cameras list.
        '''
        menu = QtWidgets.QMenu(self.variants_TABLEVIEW)
        addVariantQ = menu.addAction("Add variant")
        removeVariantQ = menu.addAction("Remove variant")
        selectVariantTransformQ = menu.addAction("Select variant transform")
        selectMetaVariantNodeQ = menu.addAction("Select MetaVariant Node")
        menu.popup(self.variants_TABLEVIEW.mapToGlobal(pos))
        
        addVariantQ.triggered.connect(self.addVariantOpt)
        removeVariantQ.triggered.connect(self.removeVariantOpt)
        selectVariantTransformQ.triggered.connect(self.selectVariantTransformOpt)
        selectMetaVariantNodeQ.triggered.connect(self.selectMetaVariantNodeOpt)
    
    
    def addVariantOpt(self):
        allSel = mc.ls(sl=True, type='transform')
        if len(allSel) == 1:
            transform = allSel[0]
            if transform[-3:] == 'VAR':
                metaVariant = cgxMeta.CGXDressingMetaVariant(name=transform + "_META")
                metaVariant.connectChildren([transform], 'taggedNode')
                self.variants_TABLEVIEW.model().insertRows(self.variants_TABLEVIEW.model().rowCount(), 1, [transform,metaVariant])
            else:
                self.baseMod.statusBar_update('Variant transform naming must end with _VAR.', alert=True)
        elif len(allSel) > 1:
            self.baseMod.statusBar_update('Please select just one transform to add as variant.', alert=True)
        elif len(allSel) < 1:
            self.baseMod.statusBar_update('Please select one transform to add as variant.', alert=True)
    
    
    def removeVariantOpt(self):
        selected = self.variants_TABLEVIEW.selectedIndexes()
        selRowsDirty = []
        for item in selected:
            thisRow = item.row()
            selRowsDirty.append(thisRow)
        selSet = set(selRowsDirty)
        selRows = list(selSet)
        
        for each in selRows:
            metaNode = self.variants_TABLEVIEW.model().dataList[each][self.variantHeaders.index('metaNode')]
            metaNode.delete()
        self.variants_TABLEVIEW.model().removeRows(selRows, len(selRows))
    
    
    def selectVariantTransformOpt(self):
        selected = self.variants_TABLEVIEW.selectedIndexes()
        selRowsDirty = []
        for item in selected:
            thisRow = item.row()
            selRowsDirty.append(thisRow)
        selSet = set(selRowsDirty)
        selRows = list(selSet)
        transform = self.variants_TABLEVIEW.model().dataList[selRows[0]][self.variantHeaders.index('variant')]
        mc.select(transform, replace=True)
    
    
    def selectMetaVariantNodeOpt(self):
        selected = self.variants_TABLEVIEW.selectedIndexes()
        selRowsDirty = []
        for item in selected:
            thisRow = item.row()
            selRowsDirty.append(thisRow)
        selSet = set(selRowsDirty)
        selRows = list(selSet)
        metaNode = self.variants_TABLEVIEW.model().dataList[selRows[0]][self.variantHeaders.index('metaNode')]
        mc.select(metaNode.mNode, replace=True)
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods for adding/removing elements on tree view
    ##--------------------------------------------------------------------------------------------     
    def lodOptions (self,pos):
        '''
        Right-click popup menu for cameras list.
        '''
        menu = QtWidgets.QMenu(self.lod_TREEVIEW)
        addLODQ = menu.addAction("Add level of detail")
        addGeoQ = menu.addAction("Add geo")
        removeTagQ = menu.addAction("Remove tag")
        selectGeoQ = menu.addAction("Select geo")
        selectMetaGeoQ = menu.addAction("Select MetaGeo Node")
        menu.popup(self.lod_TREEVIEW.mapToGlobal(pos))
        
        addLODQ.triggered.connect(self.addLODOpt)
        addGeoQ.triggered.connect(self.addGeoOpt)
        removeTagQ.triggered.connect(self.removeTagOpt)
        selectGeoQ.triggered.connect(self.selectGeoOpt)
        selectMetaGeoQ.triggered.connect(self.selectMetaGeoOpt)
    
    
    def addLODOpt(self):
        selected = self.variants_TABLEVIEW.selectedIndexes()
        selRowsDirty = []
        for item in selected:
            thisRow = item.row()
            selRowsDirty.append(thisRow)
        selSet = set(selRowsDirty)
        selRows = list(selSet)
        metaNode = self.variants_TABLEVIEW.model().dataList[selRows[0]][self.variantHeaders.index('metaNode')]
        lodDialog = SelectLODGUI(self)
        result = lodDialog.exec_()
        if result:
            selectedLod = lodDialog.lod_COMBOBOX.currentText()
            foundMatch = False
            for child in self.lod_TREEVIEW.model().dataList.children:
                if selectedLod == child.name:
                    foundMatch = True
                    break
            if not foundMatch:
                self.lod_TREEVIEW.model().insertRows(self.lod_TREEVIEW.model().dataList.childCount(), 1,[selectedLod,'root',selectedLod,metaNode], self.lod_TREEVIEW.model().indexFromNode(self.lod_TREEVIEW.model().dataList))
                self.populateLOD()
            else:
                self.baseMod.statusBar_update('Level of Detail {} already exists in this variant.'.format(selectedLod), alert=True)
    
    
    def addGeoOpt(self):
        allSel = mc.ls(sl= True, long=True)
        if len(allSel) <= 0:
            self.baseMod.statusBar_update("Nothing is selected. Select at least one node to be tagged.", status=True)
        else:
            existingMetaTags = r9Meta.getMetaNodes(['CGXDressingMetaGeo'])
            usedGeom = []
            for tag in existingMetaTags:
                usedGeom += tag.getChildren(mAttrs='taggedNode')
            for item in allSel:
                if usedGeom.count(item) == 0:
                    selectedRootItem = self.filterSelected(self.lod_TREEVIEW.selectedIndexes())
                    itemShape = mc.listRelatives(item , shapes = True, noIntermediate = True, fullPath=True)[0]
                    thisNodeType = mc.nodeType(itemShape)
                    if thisNodeType in ["mesh", "nurbsSurface", "subdiv"]:
                        if len(selectedRootItem) >= 1:
                            #Add item to tree view and metatag
                            indexNode = self.lod_TREEVIEW.model().getNode(selectedRootItem[0])
                            metaVariant = indexNode.data[3]
                            shortName = item
                            if "|" in item:
                                shortName = item.rsplit("|",1)[1]
                            metaGeo = cgxMeta.CGXDressingMetaGeo(name=shortName + "_META")
                            metaGeo.geoName = item
                            shaderGroup = mc.listConnections(itemShape, type= "shadingEngine")[0]
                            shaderNode = mc.listConnections(shaderGroup + ".surfaceShader")[0]
                            metaGeo.shader = shaderNode
                            metaGeo.aiSubdivType = mc.getAttr(itemShape + '.aiSubdivType')
                            metaGeo.aiSubdivIterations =mc.getAttr(itemShape + '.aiSubdivIterations')
                            metaGeo.aiToggleSubdivType = mc.getAttr(itemShape + '.aiSubdivType')
                            metaGeo.aiToggleSubdivIterations = mc.getAttr(itemShape + '.aiSubdivIterations')
                            metaGeo.aiDispHeight = mc.getAttr(itemShape + '.aiDispHeight')
                            metaGeo.aiDispPadding = mc.getAttr(itemShape + '.aiDispPadding')
                            metaGeo.aiDispZeroValue = mc.getAttr(itemShape + '.aiDispZeroValue')
                            metaGeo.aiOpaque = mc.getAttr(itemShape + '.aiOpaque')
                            metaGeo.connectChildren([item], 'taggedNode')
                            metaVariant.connectChildren([metaGeo], indexNode.data[0])
                            shortName = item.rsplit("|",1)[1]
                            self.lod_TREEVIEW.model().insertRows(indexNode.childCount(), 1,[shortName,'geo',metaGeo.geoName,metaGeo], self.lod_TREEVIEW.model().indexFromNode(indexNode))
                        else:
                            self.baseMod.statusBar_update("You must select one Character/Prop item to add this geo to.", status=True)
                    else:
                        self.baseMod.statusBar_update(item + " is not a valid node.", alert=True)
                else:
                    self.baseMod.statusBar_update(item + " has already been tagged.", alert=True)
    
    
    def removeTagOpt(self):
        selected = self.filterSelected(self.lod_TREEVIEW.selectedIndexes())
        if len(selected) >= 1:
            skipItems = []
            for item in selected:
                if item.isValid():
                    tagType= self.lod_TREEVIEW.model().getNode(item).data
                    metaNode = tagType[3]
                    if tagType[1] == 'root':
                        if mc.referenceQuery(metaNode.mNode, isNodeReferenced= True):
                            skipItems.append(item)
                            self.baseMod.statusBar_update("The metatag node is referenced. You must remove referenced tags from the referenced file.", alert=True)
                        else:
                            for child in metaNode.getChildren(mAttrs=tagType[0]):
                                metaNode.disconnectChild(child)
                            metaNode.delete()
                    elif tagType[1] == 'geo':
                        metaGeo = self.lod_TREEVIEW.model().getNode(item).data[3]
                        if mc.referenceQuery(metaGeo.geoName, isNodeReferenced= True):
                            self.baseMod.statusBar_update(metaGeo.geoName + " is referenced. It'll be removed from this tag just for this project.", alert=True)
                        metaNode.disconnectChild(metaGeo)
                        metaGeo.delete()
            #Remove from UI
            removeList = [x for x in selected if x not in skipItems]
            self.lod_TREEVIEW.model().removeRows(removeList)
    
    
    def selectGeoOpt(self):
        selected = self.filterSelected(self.lod_TREEVIEW.selectedIndexes())
        mc.select(clear=True)
        if len(selected) >= 1:
            for e in range(len(selected)):
                item = selected[e]
                if item.isValid():
                    tagType= self.lod_TREEVIEW.model().getNode(item).data
                    if tagType[1] == "root":
                        children = self.lod_TREEVIEW.model().getNode(item).children
                        for x in children:
                            geoName = x.data[2]
                            mc.select(geoName, add = True)
                    elif tagType[1] == "geo":
                        geoName = item.internalPointer().data[2]
                        mc.select(geoName, add = True)
    
    
    def selectMetaGeoOpt(self):
        selected = self.filterSelected(self.lod_TREEVIEW.selectedIndexes())
        mc.select(clear=True)
        if len(selected) >= 1:
            for e in range(len(selected)):
                item = selected[e]
                if item.isValid():
                    tagType= self.lod_TREEVIEW.model().getNode(item).data
                    if tagType[1] == "root":
                        children = self.lod_TREEVIEW.model().getNode(item).children
                        for x in children:
                            metaNode = x.data[3]
                            mc.select(metaNode.mNode, add = True)
                    elif tagType[1] == "geo":
                        metaNode = item.internalPointer().data[3]
                        mc.select(metaNode.mNode, add = True)
    
    
    def filterSelected(self, _indexes):
        '''
        Given a list of indexes, return only one index per row.
        '''
        if len(_indexes) > 0:
            #Group by parent first
            filterDict = {}
            for each in _indexes:
                if each.parent() not in filterDict.keys():
                    filterDict[each.parent()] = [each]
                else:
                    filterDict[each.parent()].append(each)
            #Delete redundancies
            filteredList = []
            for key in filterDict.keys():
                usedRows = []
                for item in filterDict[key]:
                    if item.row() not in usedRows:
                        usedRows.append(item.row())
                        filteredList.append(item)
            
            return filteredList
        else:
            return _indexes
        

    ##--------------------------------------------------------------------------------------------
    ##Methods for deps table view
    ##--------------------------------------------------------------------------------------------
    def depsOptions (self,pos):
        """Method that creates the popupmenu"""
        menu = QtWidgets.QMenu(self.dependencies_TABLEVIEW)
        reloadDepsQ = menu.addAction("Reload dependencies")
        if not self.publishAllDeps_CHKBOX.isChecked():
            selectNodeQ = menu.addAction("Select node")
        menu.addSeparator()
        addCustomDepQ = menu.addAction("Add custom dependency")
        menu.popup(self.dependencies_TABLEVIEW.mapToGlobal(pos))
        
        reloadDepsQ.triggered.connect(self.reloadDeps)
        if not self.publishAllDeps_CHKBOX.isChecked():
            selectNodeQ.triggered.connect(self.selectNode)
        addCustomDepQ.triggered.connect(self.addCustomDep)


    def selectNode(self):
        #List selected deps
        selected = self.dependencies_TABLEVIEW.selectedIndexes()
        selRowsDirty = []
        for item in selected:
            row = item.row()
            selRowsDirty.append(row)
        selSet = set(selRowsDirty)
        selRows = list(selSet)
        if len(selRows) > 1:
            self.baseMod.statusBar_update("Please select one dependency only to select the corresponding node.", alert=True)
        elif len(selRows) < 1:
            self.baseMod.statusBar_update("Please select at least one dependency to select the corresponding node.", alert=True)
        else:
            for index in selRows:
                rowDict = {}
                i = 0
                for header in self.dependencies_TABLEVIEW.model().headers:
                    rowDict[header] = self.dependencies_TABLEVIEW.model().dataList[index][i]
                    i += 1
                if rowDict["Node"] != None:
                    mc.select(rowDict["Node"], add=True)


    def reloadDeps(self):
        self.populateDependencies()


    def addCustomDep(self):
        browseDialog = QtWidgets.QFileDialog(self)
        browseDialog.setDirectory(self.sInfo.getProjectsFrom())
        browseDialog.setFileMode(browseDialog.FileMode.ExistingFile)
        browseDialog.setLabelText(browseDialog.Accept,"Select")
        browseDialog.setWindowTitle("Choose custom dependency")
        dialogReturn = browseDialog.exec_()
        if dialogReturn == 1:
            thisFile = browseDialog.selectedFiles()[0]
            if "\\" in thisFile:
                thisFile = thisFile.replace("\\","/")
            self.dependencies_TABLEVIEW.model().insertRows(self.dependencies_TABLEVIEW.model().rowCount(QtCore.QModelIndex()), 1, ["Custom", thisFile, None])



##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for variants table
##--------------------------------------------------------------------------------------------
class VariantsDataTableModel(dvm.DataTableModel):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, dataList, headers, parent):
        super(VariantsDataTableModel, self).__init__(dataList, headers, parent)


    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def flags(self, index):
        """Set flags of each cell"""
        if not index.isValid():
            return QtCore.Qt.NoItemFlags
        else:
            return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable


##--------------------------------------------------------------------------------------------
##Class: Tree Data Model reimplementation for LOD tree
##--------------------------------------------------------------------------------------------
class LODDataTreeModel(dvm.DataTreeModel):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, root, headers, parent):
        '''
        DataTreeModel for Model/View programming.
        :param root: root TreeNode for this model.
        :type root: TreeNode
        :param headers: list of headers to be displayed in the table. Must be same len as data passed to rootnode
        :type headers: list
        :param parent: QWidget that uses this model. Default=None
        :type parent: QWidget
        '''
        super(LODDataTreeModel, self).__init__(root, headers, parent)
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def flags(self, index):
        '''
        Set flags for each item.  Re-implement if different flags are needed and use conditional chains if per-item flags are needed.
        :param index: Item to be filtered
        :type index: QModelIndex
        :return: Flags to be used
        :rtype: QtCore.Qt.Flags
        '''
        text = index.model().data(index, QtCore.Qt.DisplayRole)
        if text in ["None","geo","root"] and index.column() in [1,2,3]:
            return QtCore.Qt.NoItemFlags
        else:
            return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
    
    
    def setData(self, index, value, role=QtCore.Qt.EditRole):
        '''
        Set data for specified index and display it.
        :param index: Index to be edited
        :type index: QModelIndex
        :param role: Edit role as default. Reimplement with elif chain for another role.
        :type role: QtCore.Qt.Role
        :return: Item data to be shown in the GUI. Might be None.
        :rtype: object
        '''
        if role != QtCore.Qt.EditRole:
            return False
        else:
            node = self.getNode(index)
            result = node.setData(index.column(),value)
            if result:
                metaNode = node.data[4]
                if index.column() == 3:
                    metaNode.instanceIdentifier = value
                if index.column() == 0:
                    metaNode.assetName = value
                    metaNode.subassetName = ""
                    if node.dataValue(3) in ["None","",None] and node.dataValue(1) == "root":
                        node.setData(3,value)
                        metaNode.instanceIdentifier = value
                self.dataChanged.emit(index, index)
    
            return result


##--------------------------------------------------------------------------------------------
##Create dialog
##--------------------------------------------------------------------------------------------
class CheckList_DialogGUI(QtWidgets.QDialog):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _list, _totalIssues, parent):
        super(CheckList_DialogGUI,self).__init__(parent)
        self.parent = parent
        self.totalIssues = _totalIssues
        self.setupUi(self)
        self.setConnections()
        self.initModel(_list, _totalIssues)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def setupUi(self, checkList_DIALOG):
        checkList_DIALOG.setObjectName("checkList_DIALOG")
        checkList_DIALOG.resize(801, 664)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(checkList_DIALOG.sizePolicy().hasHeightForWidth())
        checkList_DIALOG.setSizePolicy(sizePolicy)
        checkList_DIALOG.setMinimumSize(QtCore.QSize(801, 664))
        checkList_DIALOG.setMaximumSize(QtCore.QSize(801, 664))
        self.gridLayoutWidget = QtWidgets.QWidget(checkList_DIALOG)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 781, 651))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.totalIssues_LABEL = QtWidgets.QLabel(self.gridLayoutWidget)
        self.totalIssues_LABEL.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.totalIssues_LABEL.setObjectName("totalIssues_LABEL")
        self.gridLayout.addWidget(self.totalIssues_LABEL, 2, 2, 1, 3)
        self.checkList_TABLEVIEW = QtWidgets.QTableView(self.gridLayoutWidget)
        self.checkList_TABLEVIEW.setObjectName("checkList_TABLEVIEW")
        self.gridLayout.addWidget(self.checkList_TABLEVIEW, 0, 1, 1, 4)
        self.yes_BTN = QtWidgets.QPushButton(self.gridLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.yes_BTN.sizePolicy().hasHeightForWidth())
        self.yes_BTN.setSizePolicy(sizePolicy)
        self.yes_BTN.setObjectName("yes_BTN")
        self.gridLayout.addWidget(self.yes_BTN, 3, 3, 1, 1)
        self.no_BTN = QtWidgets.QPushButton(self.gridLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.no_BTN.sizePolicy().hasHeightForWidth())
        self.no_BTN.setSizePolicy(sizePolicy)
        self.no_BTN.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.no_BTN.setObjectName("no_BTN")
        self.gridLayout.addWidget(self.no_BTN, 3, 4, 1, 1)

        self.retranslateUi(checkList_DIALOG)
        QtCore.QMetaObject.connectSlotsByName(checkList_DIALOG)


    def retranslateUi(self, checkList_DIALOG):
        if __binding__ in ('PySide', 'PyQt4'):
            checkList_DIALOG.setWindowTitle(QtWidgets.QApplication.translate("checkList_DIALOG", "Check List", None, QtWidgets.QApplication.UnicodeUTF8))
            self.totalIssues_LABEL.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "This file has a total of {} issues. Would you like to submit anyway?".format(self.totalIssues), None, QtWidgets.QApplication.UnicodeUTF8))
            self.yes_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "Yes", None, QtWidgets.QApplication.UnicodeUTF8))
            self.no_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "No", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            checkList_DIALOG.setWindowTitle(QtWidgets.QApplication.translate("checkList_DIALOG", "Check List", None))
            self.totalIssues_LABEL.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "This file has a total of {} issues. Would you like to submit anyway?".format(self.totalIssues), None))
            self.yes_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "Yes", None))
            self.no_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "No", None))
    
    
    def setConnections(self):
        self.yes_BTN.clicked.connect(self.acceptPublish)
        self.no_BTN.clicked.connect(self.close)
        #CONTEXT MENUS
        self.checkList_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.checkList_TABLEVIEW.customContextMenuRequested.connect(self.checklistOptions)
        
        
    def initModel(self, _list, _totalIssues):
        #MODEL
        headers = ["Level", "Check", "Node", "Description"]
        self.checkModel = CheckListDataTableModel(_list, headers, self)
        proxyModel =  QtCore.QSortFilterProxyModel(self)
        proxyModel.setSourceModel(self.checkModel)
        self.checkList_TABLEVIEW.setModel(proxyModel)
        self.checkList_TABLEVIEW.sortByColumn(0,QtCore.Qt.DescendingOrder)
        self.checkList_TABLEVIEW.setAlternatingRowColors(True)
        self.checkList_TABLEVIEW.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.checkList_TABLEVIEW.setColumnWidth(0,35)
        self.checkList_TABLEVIEW.setColumnWidth(1,130)
        self.checkList_TABLEVIEW.setColumnWidth(2,140)
        self.checkList_TABLEVIEW.setColumnWidth(3,300)
        self.checkList_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        htalHeader = self.checkList_TABLEVIEW.horizontalHeader()
        htalHeader.setStretchLastSection(True)
    
    
    def acceptPublish(self):
        self.parent.publishDressingAsset()
        self.close()
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods for chek list dialog
    ##--------------------------------------------------------------------------------------------
    def checklistOptions (self, pos):
        """Method that creates the popupmenu"""
        menu = QtWidgets.QMenu(self.checkList_TABLEVIEW)
        selectNodeQ = menu.addAction("Select Node")
        menu.popup(self.checkList_TABLEVIEW.mapToGlobal(pos))
    
        selectNodeQ.triggered.connect(self.selectNodeOpt)
    
    
    def selectNodeOpt(self):
        dataList= self.checkModel.dataList
        selRowsProxy = self.checkList_TABLEVIEW.selectedIndexes()
        proxyModel = self.checkList_TABLEVIEW.model()
        selRows = []
        for each in selRowsProxy:
            selRows.append(proxyModel.mapToSource(each))
        selRowsDirty = []
        for item in selRows:
            thisRow = item.row()
            selRowsDirty.append(thisRow)
        selSet = set(selRowsDirty)
        selRows = list(selSet)
        finalList = []
        for row in selRows:
            nodeName = dataList[row][2]
            finalList.append(nodeName)
        mc.select(finalList, replace=True)


##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for CheckList table
##--------------------------------------------------------------------------------------------
class CheckListDataTableModel(dvm.DataTableModel):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, dataList, headers, parent):
        super(CheckListDataTableModel, self).__init__(dataList, headers, parent)
        self.parent = parent
            
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def flags(self, index):
        """Set flags of each cell"""
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
    
    
    def data(self, index, role):
        """Collect data and put it in the table"""
        row = index.row()
        column = index.column()
        
        if role == QtCore.Qt.DisplayRole:
            return self.dataList[row][column]

        if role == QtCore.Qt.BackgroundRole:
            if index.column() == 0:
                if self.dataList[row][column] >= 85:
                    return QtGui.QColor(255, 0, 0, 255)
                elif self.dataList[row][column] < 85 and self.dataList[row][column] >= 50:
                    return QtGui.QColor(255, 127, 0, 255)
                elif self.dataList[row][column] < 50 and self.dataList[row][column] >= 1:
                    return QtGui.QColor(0, 127, 255, 255)
                
        if role in [QtCore.Qt.ToolTipRole, QtCore.Qt.WhatsThisRole]:
            if self.dataList[row][0] >= 85:
                return "FIX IT OR DIE!"
            elif self.dataList[row][0] < 85 and self.dataList[row][0] >= 50:
                return "FIX IT, DON'T BE LAZY!"
            elif self.dataList[row][0] < 50 and self.dataList[row][0] >= 1:
                return "FIX IT, YOU FREAKING PERFECTIONIST!"


##--------------------------------------------------------------------------------------------
##Create select lod window
##--------------------------------------------------------------------------------------------
class SelectLODGUI(lodform, lodbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent=None):
        super(SelectLODGUI,self).__init__(parent)
        self.setupUi(self)
        self.setConnections()
        self.initUI()
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def setConnections(self):
        #SIGNALS
        self.cancel_BTN.clicked.connect(self.cancelAll)
        self.add_BTN.clicked.connect(self.createLight)
    
    def initUI(self):
        #Populate lod levels
        lodLevels= ['LOD_HIGH','LOD_MID','LOD_LOW']
        lodLevelsModel = dvm.ObjectsListModel(lodLevels)
        self.lod_COMBOBOX.setModel(lodLevelsModel)
    
    
    def createLight(self):
        self.done(1)
    
    
    def cancelAll(self):
        self.done(0)


##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent):
        '''
        Creates Help Dialog for current tool.
        :param parent: Main tools window.
        :type parent: QMainWindow
        '''
        super(Help_DialogGUI,self).__init__(parent)
        self.setupUi(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent):
        '''
        Creates About Dialog for current tool.
        :param parent: Main tools window.
        :type parent: QMainWindow
        '''
        super(About_DialogGUI,self).__init__(parent)
        self.setupUi(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent):
        '''
        Creates Config Dialog for current tool.
        :param parent: Main tools window.
        :type parent: QMainWindow
        '''
        super(Config_DialogGUI,self).__init__(parent)
        self.setupUi(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        
            
##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    if __DEVMODE:
        x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/workflows/SetDressing/DressingAssetTagger_dev.py','r')
        exec x.read()
        x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
    main()