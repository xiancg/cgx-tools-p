# -*- coding: utf-8 -*-
'''
Shaders Factory

Created on May 04, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import cgx.Maya.scripts.workflows.Shading.ShadingOverrides.Shaders as shd


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"



##------------------------------------------------------------------------------------------------
## ShadersFactory
##------------------------------------------------------------------------------------------------
class ShadersFactory(object):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------	   
	def __init__(self):
		self.shaderNames = ['RedMask',
                            'GreenMask',
                            'BlueMask',
                            'HoldoutMask',
                            'BlackMask',
                            'WhiteMask',
                            'UV',
                            'RedRim',
                            'GreenRim',
                            'BlueRim',
                            'WhiteRim',
                            'OCC',
                            'Incidence',
                            'Diffuse',
                            'ShadowCatcher']
		self.shadersList = []
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------	   
	def createShader(self, _shader):
		if _shader == "RedMask":
			shaderObj = shd.RedMask_Shader()
			return shaderObj
		elif _shader == "GreenMask":
			shaderObj = shd.GreenMask_Shader()
			return shaderObj
		elif _shader == "BlueMask":
			shaderObj = shd.BlueMask_Shader()
			return shaderObj
		elif _shader == "HoldoutMask":
			shaderObj = shd.HoldoutMask_Shader()
			return shaderObj
		elif _shader == "BlackMask":
			shaderObj = shd.BlackMask_Shader()
			return shaderObj
		elif _shader == "WhiteMask":
			shaderObj = shd.WhiteMask_Shader()
			return shaderObj
		elif _shader == "UV":
			shaderObj = shd.UV_Shader()
			return shaderObj
		elif _shader == "OCC":
			shaderObj = shd.OCC_Shader()
			return shaderObj
		elif _shader == "RedRim":
			shaderObj = shd.RedRim_Shader()
			return shaderObj
		elif _shader == "GreenRim":
			shaderObj = shd.GreenRim_Shader()
			return shaderObj
		elif _shader == "BlueRim":
			shaderObj = shd.BlueRim_Shader()
			return shaderObj
		elif _shader == "WhiteRim":
			shaderObj = shd.WhiteRim_Shader()
			return shaderObj
		elif _shader == "Incidence":
			shaderObj = shd.Incidence_Shader()
			return shaderObj
		elif _shader == "Diffuse":
			shaderObj = shd.Diffuse_Shader()
			return shaderObj
		elif _shader == "ShadowCatcher":
			shaderObj = shd.ShadowCatcher_Shader()
			return shaderObj
		else:
			return None
		

##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------
def main():
	pass
	

if __name__=="__main__":
	main()