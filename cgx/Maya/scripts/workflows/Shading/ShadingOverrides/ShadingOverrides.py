# -*- coding: utf-8 -*-
'''
Shading overrides tool.

Created on Mar 31, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/

Pending: Agregar un metodo de AssignShaderWithMaps que funcione por click derecho y que permita
elegir mapas del shader original y a que conectarlos en el shader nuevo.
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets, QtCore, QtGui
from cgx.gui.Qt import __binding__
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.Maya.scripts.workflows.Shading.ShadingOverrides.ShadingOverrides_icons
from cgx.Maya.scripts.workflows.Shading.ShadingOverrides.ShadersFactory import ShadersFactory


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/workflows/Shading/ShadingOverrides'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		self.setupBtns()
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.initComponents()
			self.initUI()
			self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setupBtns(self):
		self.redMask_BTN = ShadingOverride_BTN(self.centralwidget)
		self.redMask_BTN.setGeometry(QtCore.QRect(10, 30, 61, 51))
		self.redMask_BTN.setText("")
		self.redMask_BTN.setObjectName("redMask_BTN")
		self.greenMask_BTN = ShadingOverride_BTN(self.centralwidget)
		self.greenMask_BTN.setGeometry(QtCore.QRect(80, 30, 61, 51))
		self.greenMask_BTN.setText("")
		self.greenMask_BTN.setObjectName("greenMask_BTN")
		self.blueMask_BTN = ShadingOverride_BTN(self.centralwidget)
		self.blueMask_BTN.setGeometry(QtCore.QRect(150, 30, 61, 51))
		self.blueMask_BTN.setText("")
		self.blueMask_BTN.setObjectName("blueMask_BTN")
		self.redRim_BTN = ShadingOverride_BTN(self.centralwidget)
		self.redRim_BTN.setGeometry(QtCore.QRect(10, 250, 61, 50))
		self.redRim_BTN.setText("")
		self.redRim_BTN.setObjectName("redRim_BTN")
		self.occ_BTN = ShadingOverride_BTN(self.centralwidget)
		self.occ_BTN.setGeometry(QtCore.QRect(80, 310, 61, 50))
		self.occ_BTN.setText("")
		self.occ_BTN.setObjectName("occ_BTN")
		self.incidence_BTN = ShadingOverride_BTN(self.centralwidget)
		self.incidence_BTN.setGeometry(QtCore.QRect(10, 370, 61, 50))
		self.incidence_BTN.setText("")
		self.incidence_BTN.setObjectName("incidence_BTN")
		self.greenRim_BTN = ShadingOverride_BTN(self.centralwidget)
		self.greenRim_BTN.setGeometry(QtCore.QRect(80, 250, 61, 50))
		self.greenRim_BTN.setText("")
		self.greenRim_BTN.setObjectName("greenRim_BTN")
		self.blueRim_BTN = ShadingOverride_BTN(self.centralwidget)
		self.blueRim_BTN.setGeometry(QtCore.QRect(150, 250, 61, 50))
		self.blueRim_BTN.setText("")
		self.blueRim_BTN.setObjectName("blueRim_BTN")
		self.holdoutMask_BTN = ShadingOverride_BTN(self.centralwidget)
		self.holdoutMask_BTN.setGeometry(QtCore.QRect(10, 90, 61, 51))
		self.holdoutMask_BTN.setText("")
		self.holdoutMask_BTN.setObjectName("holdoutMask_BTN")
		self.uv_BTN = ShadingOverride_BTN(self.centralwidget)
		self.uv_BTN.setGeometry(QtCore.QRect(150, 310, 61, 50))
		self.uv_BTN.setText("")
		self.uv_BTN.setObjectName("uv_BTN")
		self.blackMask_BTN = ShadingOverride_BTN(self.centralwidget)
		self.blackMask_BTN.setGeometry(QtCore.QRect(80, 90, 61, 51))
		self.blackMask_BTN.setText("")
		self.blackMask_BTN.setObjectName("blackMask_BTN")
		self.whiteMask_BTN = ShadingOverride_BTN(self.centralwidget)
		self.whiteMask_BTN.setGeometry(QtCore.QRect(150, 90, 61, 51))
		self.whiteMask_BTN.setText("")
		self.whiteMask_BTN.setObjectName("whiteMask_BTN")
		self.shave_BTN = ShadingOverride_BTN(self.centralwidget)
		self.shave_BTN.setGeometry(QtCore.QRect(10, 150, 61, 51))
		self.shave_BTN.setText("")
		self.shave_BTN.setObjectName("shave_BTN")
		self.diffuse_BTN = ShadingOverride_BTN(self.centralwidget)
		self.diffuse_BTN.setGeometry(QtCore.QRect(80, 370, 61, 51))
		self.diffuse_BTN.setText("")
		self.diffuse_BTN.setObjectName("diffuse_BTN")
		self.whiteRim_BTN = ShadingOverride_BTN(self.centralwidget)
		self.whiteRim_BTN.setGeometry(QtCore.QRect(10, 310, 61, 50))
		self.whiteRim_BTN.setText("")
		self.whiteRim_BTN.setObjectName("whiteRim_BTN")
		self.shadowCatcher_BTN = ShadingOverride_BTN(self.centralwidget)
		self.shadowCatcher_BTN.setGeometry(QtCore.QRect(150, 370, 61, 51))
		self.shadowCatcher_BTN.setText("")
		self.shadowCatcher_BTN.setObjectName("shadowCatcher_BTN")
	
	
	def setConnections(self):
		'''
        Connect signals.
        '''
		#ACTIONS
		self.actionRevertShader.triggered.connect(self.revertShader)
		#BUTTONS
		self.redMask_BTN.clicked.connect(lambda: self.assignShader('RedMask', False))
		self.greenMask_BTN.clicked.connect(lambda: self.assignShader('GreenMask'))
		self.blueMask_BTN.clicked.connect(lambda: self.assignShader('BlueMask'))
		self.holdoutMask_BTN.clicked.connect(lambda: self.assignShader('HoldoutMask'))
		self.blackMask_BTN.clicked.connect(lambda: self.assignShader('BlackMask'))
		self.whiteMask_BTN.clicked.connect(lambda: self.assignShader('WhiteMask'))
		self.uv_BTN.clicked.connect(lambda: self.assignShader('UV'))
		self.redRim_BTN.clicked.connect(lambda: self.assignShader('RedRim'))
		self.greenRim_BTN.clicked.connect(lambda: self.assignShader('GreenRim'))
		self.blueRim_BTN.clicked.connect(lambda: self.assignShader('BlueRim'))
		self.whiteRim_BTN.clicked.connect(lambda: self.assignShader('WhiteRim'))
		self.occ_BTN.clicked.connect(lambda: self.assignShader('OCC'))
		self.incidence_BTN.clicked.connect(lambda: self.assignShader('Incidence'))
		self.diffuse_BTN.clicked.connect(lambda: self.assignShader('Diffuse'))
		self.shadowCatcher_BTN.clicked.connect(lambda: self.assignShader('ShadowCatcher'))
		self.shave_BTN.clicked.connect(self.hairMask)
		#CONTEXT MENUS
		self.redMask_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.redMask_BTN.rightClick.connect(self.assignShaderOptions)
		self.greenMask_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.greenMask_BTN.rightClick.connect(self.assignShaderOptions)
		self.blueMask_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.blueMask_BTN.rightClick.connect(self.assignShaderOptions)
		self.whiteMask_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.whiteMask_BTN.rightClick.connect(self.assignShaderOptions)
		self.blackMask_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.blackMask_BTN.rightClick.connect(self.assignShaderOptions)
		self.holdoutMask_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.holdoutMask_BTN.rightClick.connect(self.assignShaderOptions)
		self.uv_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.uv_BTN.rightClick.connect(self.assignShaderOptions)
		self.occ_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.occ_BTN.rightClick.connect(self.assignShaderOptions)
		self.incidence_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.incidence_BTN.rightClick.connect(self.assignShaderOptions)
		self.diffuse_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.diffuse_BTN.rightClick.connect(self.assignShaderOptions)
		self.shadowCatcher_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.shadowCatcher_BTN.rightClick.connect(self.assignShaderOptions)
		self.redRim_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.redRim_BTN.rightClick.connect(self.assignShaderOptions)
		self.greenRim_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.greenRim_BTN.rightClick.connect(self.assignShaderOptions)
		self.blueRim_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.blueRim_BTN.rightClick.connect(self.assignShaderOptions)
		self.whiteRim_BTN.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.whiteRim_BTN.rightClick.connect(self.assignShaderOptions)
		
		#ICONS
		self.redMask_BTN.setIcon(QtGui.QIcon(":/redMask.png"))
		self.redMask_BTN.setIconSize(QtCore.QSize(58,48))
		self.greenMask_BTN.setIcon(QtGui.QIcon(":/greenMask.png"))
		self.greenMask_BTN.setIconSize(QtCore.QSize(58,48))
		self.blueMask_BTN.setIcon(QtGui.QIcon(":/blueMask.png"))
		self.blueMask_BTN.setIconSize(QtCore.QSize(58,48))
		self.holdoutMask_BTN.setIcon(QtGui.QIcon(":/holdoutMask.png"))
		self.holdoutMask_BTN.setIconSize(QtCore.QSize(58,48))
		self.blackMask_BTN.setIcon(QtGui.QIcon(":/blackMask.png"))
		self.blackMask_BTN.setIconSize(QtCore.QSize(58,48))
		self.whiteMask_BTN.setIcon(QtGui.QIcon(":/whiteMask.png"))
		self.whiteMask_BTN.setIconSize(QtCore.QSize(58,48))
		self.uv_BTN.setIcon(QtGui.QIcon(":/uv.png"))
		self.uv_BTN.setIconSize(QtCore.QSize(58,48))
		self.redRim_BTN.setIcon(QtGui.QIcon(":/redRim.png"))
		self.redRim_BTN.setIconSize(QtCore.QSize(58,48))
		self.greenRim_BTN.setIcon(QtGui.QIcon(":/greenRim.png"))
		self.greenRim_BTN.setIconSize(QtCore.QSize(58,48))
		self.blueRim_BTN.setIcon(QtGui.QIcon(":/blueRim.png"))
		self.blueRim_BTN.setIconSize(QtCore.QSize(58,48))
		self.whiteRim_BTN.setIcon(QtGui.QIcon(":/whiteRim.png"))
		self.whiteRim_BTN.setIconSize(QtCore.QSize(58,48))
		self.occ_BTN.setIcon(QtGui.QIcon(":/occ.png"))
		self.occ_BTN.setIconSize(QtCore.QSize(58,48))
		self.incidence_BTN.setIcon(QtGui.QIcon(":/incidence.png"))
		self.incidence_BTN.setIconSize(QtCore.QSize(58,48))
		self.diffuse_BTN.setIcon(QtGui.QIcon(":/diffuse.png"))
		self.diffuse_BTN.setIconSize(QtCore.QSize(58,48))
		self.shave_BTN.setIcon(QtGui.QIcon(":/shaveMask.png"))
		self.shave_BTN.setIconSize(QtCore.QSize(58,48))
		self.shadowCatcher_BTN.setIcon(QtGui.QIcon(":/shadowCatcher.png"))
		self.shadowCatcher_BTN.setIconSize(QtCore.QSize(58,48))

	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Shading Overrides"
		self.__toolVersion = __version__
		
		self.factory = ShadersFactory()
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.baseMod.MainMenu_Component.edit_config.setVisible(False)
		#Add revert shader menu option
		self.actionRevertShader = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.actionRevertShader.setObjectName("actionRevertShader")
		self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[0], self.actionRevertShader)
		className= str(self.__class__.__name__)
		if __binding__ in ('PySide', 'PyQt4'):
			self.actionRevertShader.setText(QtWidgets.QApplication.translate(className, "Revert to original shader", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			self.actionRevertShader.setText(QtWidgets.QApplication.translate(className, "Revert to original shader", None))
			
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		#Check active renderers
		if mc.getAttr('defaultRenderGlobals.ren') != "arnold" and mc.getAttr('defaultRenderGlobals.ren') != "vray":
			msgBox = QtWidgets.QMessageBox(self)
			msgBox.setWindowTitle("Warning!")
			msgBox.setText("Either Arnold or VRay must be set in your Render Settings for this tool to be used.")
			msgBox.exec_()
			self.baseMod.quitApp()
		else:
			#Create shader objs
			for each in self.factory.shaderNames:
				self.factory.shadersList.append(self.factory.createShader(each))
				
			#Check if shader set exist
			allSets = mc.ls(type="objectSet")
			cgxSets = []
			for set in allSets:
				if set == "CGX_shaders":
					cgxSets.append(set)
			
			if len(cgxSets) > 1:
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("You have more than one CGX_shaders set on your scene. Please delete all and restart the tool.")
				msgBox.exec_()
				self.baseMod.quitApp()
			#If no CGX_shaders found, create shaders and set
			elif len(cgxSets) < 1:
				shadersList = []
				for each in self.factory.shadersList:
					shadersList.append(each.create())
				cgxShadersSet= mc.sets(shadersList, name= "CGX_shaders")
			#If CGX_shader exists but its number is higher than self.factory.shadersList
			elif len(cgxSets) == 1 and mc.sets("CGX_shaders", query= True, size= True) > len(self.factory.shaderNames):
				msgBox = QtWidgets.QMessageBox(self)
				msgBox.setWindowTitle("Warning!")
				msgBox.setText("You have more default CGX_shaders than normal, please delete your CGX_shaders set and restart the tool.")
				msgBox.exec_()
				self.baseMod.quitApp()
			#If CGX_shader exists but its number is lower than self.factory.shadersList
			elif len(cgxSets) == 1 and mc.sets("CGX_shaders", query= True, size= True) < len(self.factory.shaderNames):
				currentShaders = mc.sets("CGX_shaders", query= True)
				currentShadersClean = []
				for dirty in currentShaders:
					currentShadersClean.append(dirty.split("_")[0])
				createList = []
				for shader in self.factory.shaderNames:
					if shader not in currentShadersClean:
						createList.append(shader)
				for shader in currentShaders:
					shaderClean = shader.split("_")[0]
					for shaderObj in self.factory.shadersList:
						if shaderClean == shaderObj.name:
							sg = mc.listConnections(shader + ".outColor")
							shaderObj.node = shader
							shaderObj.shadingGroup = sg[0]
				for shader in createList:
					result = self.factory.createShader(shader)
					for shaderObj in sorted(self.factory.shadersList, reverse=True):
						if type(result) == type(shaderObj):
							del(self.factory.shadersList[self.factory.shadersList.index(shaderObj)])
							self.factory.shadersList.append(result)
							break
					node = result.create()
					mc.sets(node, edit= True, forceElement= "CGX_shaders")
			#If CGX_shaders exists and number is correct, use existing shaders
			else:
				currentShaders = mc.sets("CGX_shaders", query= True)
				currentShadersClean = []
				for dirty in currentShaders:
					currentShadersClean.append(dirty.split("_")[0])
				foreignShaders= False
				for shader in currentShadersClean:
					if shader not in self.factory.shaderNames:
						foreignShaders = True
						break
				if foreignShaders:
					msgBox = QtWidgets.QMessageBox(self)
					msgBox.setWindowTitle("Warning!")
					msgBox.setText("You have foreign shaders in your CGX_shaders set. Delete CGX_shaders set and restart the tool.")
					msgBox.exec_()
					self.baseMod.quitApp()
				else:
					for shader in currentShaders:
						for shaderObj in self.factory.shadersList:
							shaderClean = shader.split("_")[0]
							if shaderClean == shaderObj.name:
								sg = mc.listConnections(shader + ".outColor")
								shaderObj.node = shader
								shaderObj.shadingGroup = sg[0]
							
	
	def assignShader(self, _shader, newNetwork= False):
		allSel = mc.ls(sl=True, dag=True, leaf=True, long= True, ni= True)
		for each in allSel:
			checkAssignedShader = mc.listConnections(each, type= "shadingEngine")
			if checkAssignedShader == None:
				self.baseMod.statusBar_update(each + " has no shaders assigned and it may brake your render layers.", warning=True)
			else:
				#Creates a new shading network and connects opacity and displacement maps to it
				checkDisp = self.checkDisplacement(each)
				checkOpct = self.checkOpacity(each)
				if newNetwork:
					#NO DISP OR OPACITY
					if checkDisp == False and checkOpct == None:
						for shaderObj in self.factory.shadersList:
							if _shader == shaderObj.name:
								shaderObj.addToNetwork(each)
					#HAS DISP ONLY
					elif checkDisp == True and checkOpct == None:
						self.assignWithDisplacement(_shader, each)
					#HAS OPACITY ONLY
					elif checkDisp == False and checkOpct != None:
						self.assignWithOpacity(_shader, each, checkOpct, None)
					#HAS DISP AND OPACITY
					elif checkDisp == True and checkOpct != None:
						shaderObj = self.assignWithDisplacement(_shader, each)
						self.assignWithOpacity(_shader, each, checkOpct, shaderObj)
				#Use Arnold's Enable Matte overrides
				else:
					if not mc.pluginInfo('mtoa', query=True, loaded=True):
						self.baseMod.statusBar_update("Right-click and use Create new network if not using Arnold.", warning=True)
					else:
						for shaderObj in self.factory.shadersList:
							if _shader == shaderObj.name:
								result = shaderObj.assignAsOverride(each, checkAssignedShader[0], checkOpct)
								if not result:
									self.baseMod.statusBar_update("Override couldn't be created.", warning=True)
								else:
									self.baseMod.statusBar_update("Override created.", success=True)
		
	
	def assignWithDisplacement(self, _shader, _obj):
		shaderGroup = mc.listConnections(_obj, type= "shadingEngine")
		dispNode = mc.listConnections(shaderGroup[0] + ".displacementShader", type= "displacementShader")
		for shaderName in self.factory.shaderNames:
			if _shader == shaderName:
				newShader = self.factory.createShader(_shader)
				newShader.create()
				newShader.addToNetwork(_obj)
				mc.connectAttr(dispNode[0] + ".displacement", newShader.shadingGroup + ".displacementShader", force=True)
				return newShader
	
	
	def assignWithOpacity(self, _shader, _obj, checkOpct, shaderObj):
		for shaderName in self.factory.shaderNames:
			if _shader == shaderName:
				newShader = shaderObj 
				if shaderObj == None:
					newShader = self.factory.createShader(_shader)
					newShader.create()
					newShader.addToNetwork(_obj)
				if len(checkOpct) == 1:
					if _shader == "Diffuse":
						luminanceNode = mc.shadingNode("luminance", asUtility= True)
						mc.connectAttr(checkOpct[0], luminanceNode + ".value", force=True)
						mc.connectAttr(luminanceNode + ".outValue", newShader.node + ".matteOpacity", force= True)
						reverseNode = mc.shadingNode("reverse", asUtility= True)
						mc.connectAttr(checkOpct[0], reverseNode + ".input", force= True)
						mc.connectAttr(reverseNode + ".output", newShader.node + ".transparency", force= True)
					else:
						mc.connectAttr(checkOpct[0], newShader.node + ".outMatteOpacity", force= True)
						reverseNode = mc.shadingNode("reverse", asUtility= True)
						mc.connectAttr(checkOpct[0], reverseNode + ".input", force= True)
						mc.connectAttr(reverseNode + ".output", newShader.node + ".outTransparency", force= True)
				else:
					if _shader == "Diffuse":
						mc.connectAttr(checkOpct[0], newShader.node + ".matteOpacity", force= True)
					else:
						mc.connectAttr(checkOpct[0], newShader.node + ".outMatteOpacityR", force= True)
						mc.connectAttr(checkOpct[1], newShader.node + ".outMatteOpacityG", force= True)
						mc.connectAttr(checkOpct[2], newShader.node + ".outMatteOpacityB", force= True)
						reverseNode = mc.shadingNode("reverse", asUtility= True)
						mc.connectAttr(checkOpct[0], reverseNode + ".inputX", force= True)
						mc.connectAttr(checkOpct[1], reverseNode + ".inputY", force= True)
						mc.connectAttr(checkOpct[2], reverseNode + ".inputZ", force= True)
						mc.connectAttr(reverseNode + ".output", newShader.node + ".outTransparency", force= True)
				break
	
	
	def checkDisplacement(self, _obj):
		shaderGroup = mc.listConnections(_obj, type= "shadingEngine")
		dispExist = mc.connectionInfo(shaderGroup[0] + ".displacementShader", isDestination= True)
		
		return dispExist
	
	
	def checkOpacity(self, _obj):
		opacityFile = []
		shaderGroup = mc.listConnections(_obj, type= "shadingEngine")
		shaderNode = mc.listConnections(shaderGroup[0] + ".surfaceShader")
		if mc.nodeType(shaderNode[0]) == "aiRaySwitch":
			cameraShader = mc.listConnections(shaderNode[0] + ".camera")
			if mc.attributeQuery('opacityR', n= cameraShader[0] , exists=True):
				opacityCompExist = mc.connectionInfo(cameraShader[0] + ".opacityR", isDestination= True)
				if opacityCompExist:
					opacityRInput = mc.listConnections(cameraShader[0] + ".opacityR", plugs= True)
					if opacityRInput == None:
						opacityInput = mc.listConnections(cameraShader[0] + ".opacity", plugs= True)
						opacityFile.append(opacityInput[0])
					elif opacityRInput != None:
						opacityGInput = mc.listConnections(cameraShader[0] + ".opacityG", plugs= True) 
						opacityBInput = mc.listConnections(cameraShader[0] + ".opacityB", plugs= True)
						opacityFile.append(opacityRInput[0])
						opacityFile.append(opacityGInput[0])
						opacityFile.append(opacityBInput[0])
		elif mc.nodeType(shaderNode[0]) == "surfaceShader":
			if mc.attributeQuery('outMatteOpacityR', n= shaderNode[0] , exists=True):
				opacityCompExist = mc.connectionInfo(shaderNode[0] + ".outMatteOpacityR", isDestination= True)
				if opacityCompExist:
					opacityRInput = mc.listConnections(shaderNode[0] + ".outMatteOpacityR", plugs= True)
					if opacityRInput == None:
						opacityInput = mc.listConnections(shaderNode[0] + ".outMatteOpacity", plugs= True)
						opacityFile.append(opacityInput[0])
					elif opacityRInput != None:
						opacityGInput = mc.listConnections(shaderNode[0] + ".outMatteOpacityG", plugs= True) 
						opacityBInput = mc.listConnections(shaderNode[0] + ".outMatteOpacityB", plugs= True)
						opacityFile.append(opacityRInput[0])
						opacityFile.append(opacityGInput[0])
						opacityFile.append(opacityBInput[0])
		elif mc.nodeType(shaderNode[0]) == "lambert":
			if mc.attributeQuery('transparencyR', n= shaderNode[0] , exists=True):
				opacityCompExist = mc.connectionInfo(shaderNode[0] + ".transparencyR", isDestination= True)
				if opacityCompExist:
					opacityRInput = mc.listConnections(shaderNode[0] + ".transparencyR", plugs= True)
					if opacityRInput == None:
						opacityInput = mc.listConnections(shaderNode[0] + ".transparency", plugs= True)
						opacityFile.append(opacityInput[0])
					elif opacityRInput != None:
						opacityGInput = mc.listConnections(shaderNode[0] + ".transparencyG", plugs= True) 
						opacityBInput = mc.listConnections(shaderNode[0] + ".transparencyB", plugs= True)
						opacityFile.append(opacityRInput[0])
						opacityFile.append(opacityGInput[0])
						opacityFile.append(opacityBInput[0])
		elif mc.nodeType(shaderNode[0]) == "VrayMtl":
			if mc.attributeQuery('opacityMapR', n= shaderNode[0] , exists=True):
				opacityCompExist = mc.connectionInfo(shaderNode[0] + ".opacityMapR", isDestination= True)
				if opacityCompExist:
					opacityRInput = mc.listConnections(shaderNode[0] + ".opacityMapR", plugs= True)
					if opacityRInput == None:
						opacityInput = mc.listConnections(shaderNode[0] + ".opacityMap", plugs= True)
						opacityFile.append(opacityInput[0])
					elif opacityRInput != None:
						opacityGInput = mc.listConnections(shaderNode[0] + ".opacityMapG", plugs= True) 
						opacityBInput = mc.listConnections(shaderNode[0] + ".opacityMapB", plugs= True)
						opacityFile.append(opacityRInput[0])
						opacityFile.append(opacityGInput[0])
						opacityFile.append(opacityBInput[0])
		else:
			if mc.attributeQuery('opacityR', n= shaderNode[0] , exists=True):
				opacityCompExist = mc.connectionInfo(shaderNode[0] + ".opacityR", isDestination= True)
				if opacityCompExist:
					opacityRInput = mc.listConnections(shaderNode[0] + ".opacityR", plugs= True)
					if opacityRInput == None:
						opacityInput = mc.listConnections(shaderNode[0] + ".opacity", plugs= True)
						opacityFile.append(opacityInput[0])
					elif opacityRInput != None:
						opacityGInput = mc.listConnections(shaderNode[0] + ".opacityG", plugs= True) 
						opacityBInput = mc.listConnections(shaderNode[0] + ".opacityB", plugs= True)
						opacityFile.append(opacityRInput[0])
						opacityFile.append(opacityGInput[0])
						opacityFile.append(opacityBInput[0])
		#Return
		if opacityFile == []:
			return None
		else:
			return opacityFile
	
	
	def hairMask (self):
		allSel = mc.ls(sl= True, long=True)
		if len(allSel) < 1:
			self.baseMod.statusBar_update("Select at least one ShaveHair node.", alert=True)
		else:
			aiNode = mc.createNode("aiUtility", name= "redHairMask_aiUtility")
			mc.setAttr(aiNode + ".shadeMode", 2)
			mc.setAttr(aiNode + ".color", 1, 0, 0, type= "double3")
			for e in allSel:
				if mc.nodeType(e) == "transform":
					thisNode = mc.listRelatives(e, shapes = True, noIntermediate = True, fullPath= True)
					if mc.nodeType(thisNode[0]) == "shaveHair":
						mc.editRenderLayerAdjustment(thisNode[0] + ".aiHairShader")
						mc.connectAttr(aiNode+ ".outColor", thisNode[0] + ".aiHairShader", force= True)
					else:
						self.baseMod.statusBar_update("Node is not of type shaveHair: {}".format(thisNode[0]), alert=True)
				elif mc.nodeType(e) == "shaveHair":
					mc.editRenderLayerAdjustment(e + ".aiHairShader")
					mc.connectAttr(aiNode+ ".outColor", e + ".aiHairShader", force= True)
	
	
	def revertShader(self):
		allSel = mc.ls(sl=True, dag=True, leaf=True, long= True, ni= True)
		if len(allSel) < 1:
			self.baseMod.statusBar_update("Select at least one mesh to revert.", alert=True)
		else:
			for each in allSel:
				if mc.nodeType(each) == "mesh":
					currLyr = mc.editRenderLayerGlobals(query=True, currentRenderLayer=True)
					#Remove overrides
					checkAssignedShader = mc.listConnections(each, type= "shadingEngine")
					shadingNode = mc.listConnections(checkAssignedShader[0] + '.surfaceShader', d=False, s=True)[0]
					layerOverrides= mc.listConnections(currLyr + '.adjustments', source=True, plugs=True, type=mc.nodeType(shadingNode))
					if layerOverrides != None:
						for override in layerOverrides:
							if shadingNode in override:
								mc.editRenderLayerAdjustment(override, remove=True)
					#Get default layer shader
					mc.editRenderLayerGlobals(currentRenderLayer="defaultRenderLayer")
					defShadingGroup = mc.listConnections(each, type= "shadingEngine")[0]
					#Revert shader
					mc.editRenderLayerGlobals(currentRenderLayer=currLyr)
					mc.sets(each, edit= True, forceElement= defShadingGroup)
				elif mc.nodeType(each) == "transform":
					shapeNode = mc.listRelatives(each, shapes=True, noIntermediate=True, fullPath=True)[0]
					if mc.nodeType(shapeNode) == "mesh":
						currLyr = mc.editRenderLayerGlobals(query=True, currentRenderLayer=True)
						#Remove overrides
						checkAssignedShader = mc.listConnections(each, type= "shadingEngine")
						shadingNode = mc.listConnections(checkAssignedShader[0] + '.surfaceShader', d=False, s=True)[0]
						layerOverrides= mc.listConnections(currLyr + '.adjustments', source=True, plugs=True, type=mc.nodeType(shadingNode))
						if layerOverrides != None:
							for override in layerOverrides:
								if shadingNode in override:
									mc.editRenderLayerAdjustment(override, remove=True)
						#Get default layer shader
						mc.editRenderLayerGlobals(currentRenderLayer="defaultRenderLayer")
						defShadingGroup = mc.listConnections(shapeNode, type= "shadingEngine")[0]
						#Revert shader
						mc.editRenderLayerGlobals(currentRenderLayer=currLyr)
						mc.sets(shapeNode, edit= True, forceElement= defShadingGroup)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for assigning shaders as a new network
	##--------------------------------------------------------------------------------------------
	def assignShaderOptions (self, pos, btnObject):
		'''
		Right-click popup menu for shading assignment options.
		'''
		menu = QtWidgets.QMenu(btnObject)
		createNewNetQ = menu.addAction("Create new network")
		menu.popup(btnObject.mapToGlobal(pos))
		
		if btnObject.objectName() == 'redMask_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('RedMask'))
		elif btnObject.objectName() == 'greenMask_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('GreenMask'))
		elif btnObject.objectName() == 'blueMask_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('BlueMask'))
		elif btnObject.objectName() == 'whiteMask_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('WhiteMask'))
		elif btnObject.objectName() == 'blackMask_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('BlackMask'))
		elif btnObject.objectName() == 'holdoutMask_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('HoldoutMask'))
		elif btnObject.objectName() == 'uv_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('UV'))
		elif btnObject.objectName() == 'occ_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('OCC'))
		elif btnObject.objectName() == 'incidence_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('Incidence'))
		elif btnObject.objectName() == 'diffuse_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('Diffuse'))
		elif btnObject.objectName() == 'shadowCatcher_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('ShadowCatcher'))
		elif btnObject.objectName() == 'redRim_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('RedRim'))
		elif btnObject.objectName() == 'greenRim_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('GreenRim'))
		elif btnObject.objectName() == 'blueRim_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('BlueRim'))
		elif btnObject.objectName() == 'whiteRim_BTN':
			createNewNetQ.triggered.connect(lambda: self.createNewNetOpt('WhiteRim'))
	
	
	def createNewNetOpt(self, _shader):
		self.assignShader(_shader, newNetwork= True)


##--------------------------------------------------------------------------------------------
##Button reimplementation
##--------------------------------------------------------------------------------------------
class ShadingOverride_BTN(QtWidgets.QPushButton):
	##--------------------------------------------------------------------------------------------
	##Signals
	##--------------------------------------------------------------------------------------------
	rightClick = QtCore.Signal(QtCore.QPoint, super)
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent=None):
		super(ShadingOverride_BTN, self).__init__(parent)
		self.parent = parent
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def mousePressEvent(self, event):
		if event.type() == QtCore.QEvent.MouseButtonPress:
			if event.button() == QtCore.Qt.RightButton:
				cursor = QtGui.QCursor()
				self.rightClick.emit(self.mapFromGlobal(cursor.pos()), self)
			else:
				super(ShadingOverride_BTN, self).mousePressEvent(event)
			

##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/workflows/Shading/ShadingOverrides_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()