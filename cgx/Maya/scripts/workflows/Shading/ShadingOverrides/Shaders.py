# -*- coding: utf-8 -*-
'''
Shaders Factory

Created on May 04, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import maya.cmds as mc


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##------------------------------------------------------------------------------------------------
## Abstract_Shader
##------------------------------------------------------------------------------------------------
class Abstract_Shader(object):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        self.__name = 'Abstract'
        self.__node = ''
        self.__shadingGroup = ''
        self.__members = []
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def create(self):
        pass
    
    
    def addToNetwork (self, _obj):
        if mc.nodeType(_obj) in ['transform']:
            _obj = mc.listRelatives(_obj, shapes=True, noIntermediate= True, fullPath=True)[0]
        mc.sets(_obj, edit= True, forceElement= self.shadingGroup)
        if mc.sets(_obj, isMember=self.shadingGroup):
            self.members.append(_obj)
    
    
    def assignAsOverride(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            currentRL= mc.editRenderLayerGlobals(query=True, currentRenderLayer=True)
            mc.editRenderLayerAdjustment(shadingNode + '.aiEnableMatte', layer=currentRL)
            mc.setAttr(shadingNode + '.aiEnableMatte', True)
            
            mc.editRenderLayerAdjustment(shadingNode + '.aiMatteColorA', layer=currentRL)
            if _checkOpct != None:
                checkLuminanceNode = mc.listConnections(_checkOpct[0], d=True, s=False)
                luminanceNode = ''
                for each in checkLuminanceNode:
                    if mc.nodeType(each) == 'luminance':
                        if mc.isConnected(_checkOpct[0], each + ".value"):
                            luminanceNode = each
                            break
                if luminanceNode == '':
                    nodeName = _checkOpct[0].split('.')[0]
                    attributeName = _checkOpct[0].split('.')[1]
                    if mc.attributeQuery(attributeName, node=nodeName, attributeType=True) in ['float3','double3']:
                        luminanceNode = mc.shadingNode("luminance", asUtility= True)
                        childAttrs =mc.attributeQuery(attributeName, node=nodeName, listChildren=True)
                        mc.connectAttr(nodeName + '.' + childAttrs[0], luminanceNode + ".valueR", force=True)
                        mc.connectAttr(nodeName + '.' + childAttrs[1], luminanceNode + ".valueG", force=True)
                        mc.connectAttr(nodeName + '.' + childAttrs[2], luminanceNode + ".valueB", force=True)
                        mc.connectAttr(luminanceNode + '.outValue', shadingNode + ".aiMatteColorA", force= True)
                    else:
                        if not mc.isConnected(_checkOpct[0], shadingNode + ".aiMatteColorA"):
                            mc.connectAttr(_checkOpct[0], shadingNode + ".aiMatteColorA", force= True)
                else:
                    if not mc.isConnected(luminanceNode + ".outValue", shadingNode + ".aiMatteColorA"):
                        mc.connectAttr(luminanceNode + ".outValue", shadingNode + ".aiMatteColorA", force= True)
            else:
                if mc.connectionInfo(shadingNode + '.aiMatteColorA', isDestination=True):
                    sourceConnection = mc.connectionInfo(shadingNode + '.aiMatteColorA', sourceFromDestination=True)
                    mc.disconnectAttr(sourceConnection, shadingNode + '.aiMatteColorA')
                    opacityValue = mc.getAttr(shadingNode + '.opacityR')
                    mc.setAttr(shadingNode + '.aiMatteColorA', opacityValue)
                else:
                    opacityValue = mc.getAttr(shadingNode + '.opacityR')
                    mc.setAttr(shadingNode + '.aiMatteColorA', opacityValue)
            
            mc.editRenderLayerAdjustment(shadingNode + '.aiMatteColor', layer=currentRL)
            if mc.connectionInfo(shadingNode + '.aiMatteColor', isDestination=True):
                sourceConnection = mc.connectionInfo(shadingNode + '.aiMatteColor', sourceFromDestination=True)
                mc.disconnectAttr(sourceConnection, shadingNode + '.aiMatteColor')
                
            self.setOverrides(_obj, _assignedSG, _checkOpct)
            return True
        else:
            return False
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        pass
    
    
    ##--------------------------------------------------------------------------------------------
    ##Properties
    ##--------------------------------------------------------------------------------------------
    @property
    def node(self):
        return self.__node
    @node.setter
    def node(self,n):
        self.__node = n
    
    @property
    def shadingGroup(self):
        return self.__shadingGroup
    @shadingGroup.setter
    def shadingGroup(self,s):
        self.__shadingGroup = s
    
    @property
    def name(self):
        return self.__name
    @name.setter
    def name(self,n):
        self.__name = n
    
    @property
    def members(self):
        return self.__members
    @members.setter
    def members(self,m):
        self.__members.append(m)
        

##------------------------------------------------------------------------------------------------
## RedMask_Shader
##------------------------------------------------------------------------------------------------
class RedMask_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(RedMask_Shader, self).__init__()
        self.name = 'RedMask'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.node = mc.shadingNode("surfaceShader", asShader= True, name= self.name + "_surface")
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "SG", empty= True)
        mc.setAttr(self.node + ".outColor", 1, 0, 0, type= "double3")
        #Connect nodes
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        
        return self.node
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            if not mc.connectionInfo(shadingNode + '.aiMatteColorA', isDestination=True):
                mc.setAttr(shadingNode + '.aiMatteColorA', 1)
            mc.setAttr(shadingNode + '.aiMatteColor', 1, 0, 0, type='double3')
        

##------------------------------------------------------------------------------------------------
## GreenMask_Shader
##------------------------------------------------------------------------------------------------
class GreenMask_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(GreenMask_Shader, self).__init__()
        self.name = 'GreenMask'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.node = mc.shadingNode("surfaceShader", asShader= True, name= self.name + "_surface")
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "SG", empty= True)
        mc.setAttr(self.node + ".outColor", 0, 1, 0, type= "double3")
        #Connect nodes
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        
        return self.node
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            if not mc.connectionInfo(shadingNode + '.aiMatteColorA', isDestination=True):
                mc.setAttr(shadingNode + '.aiMatteColorA', 1)
            mc.setAttr(shadingNode + '.aiMatteColor', 0, 1, 0, type='double3')


##------------------------------------------------------------------------------------------------
## BlueMask_Shader
##------------------------------------------------------------------------------------------------
class BlueMask_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(BlueMask_Shader, self).__init__()
        self.name = 'BlueMask'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.node = mc.shadingNode("surfaceShader", asShader= True, name= self.name + "_surface")
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "SG", empty= True)
        mc.setAttr(self.node + ".outColor", 0, 0, 1, type= "double3")
        #Connect nodes
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        
        return self.node
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            if not mc.connectionInfo(shadingNode + '.aiMatteColorA', isDestination=True):
                mc.setAttr(shadingNode + '.aiMatteColorA', 1)
            mc.setAttr(shadingNode + '.aiMatteColor', 0, 0, 1, type='double3')
    

##------------------------------------------------------------------------------------------------
## HoldoutMask_Shader
##------------------------------------------------------------------------------------------------
class HoldoutMask_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(HoldoutMask_Shader, self).__init__()
        self.name = 'HoldoutMask'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.node = mc.shadingNode("surfaceShader", asShader= True, name= self.name + "_surface")
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "SG", empty= True)
        mc.setAttr(self.node + ".outMatteOpacity", 0, 0, 0, type= "double3")
        #Connect nodes
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        
        return self.node
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            if not mc.connectionInfo(shadingNode + '.aiMatteColorA', isDestination=True):
                mc.setAttr(shadingNode + '.aiMatteColorA', 0)
            else:
                sourceConnection = mc.connectionInfo(shadingNode + '.aiMatteColorA', sourceFromDestination=True)
                mc.disconnectAttr(sourceConnection, shadingNode + '.aiMatteColorA')
                mc.setAttr(shadingNode + '.aiMatteColorA', 0)
            mc.setAttr(shadingNode + '.aiMatteColor', 0, 0, 0, type='double3')


##------------------------------------------------------------------------------------------------
## BlackMask_Shader
##------------------------------------------------------------------------------------------------
class BlackMask_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(BlackMask_Shader, self).__init__()
        self.name = 'BlackMask'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.node = mc.shadingNode("surfaceShader", asShader= True, name= self.name + "_surface")
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "SG", empty= True)
        mc.setAttr(self.node + ".outColor", 0, 0, 0, type= "double3")
        #Connect nodes
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        
        return self.node
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            if not mc.connectionInfo(shadingNode + '.aiMatteColorA', isDestination=True):
                mc.setAttr(shadingNode + '.aiMatteColorA', 1)
            mc.setAttr(shadingNode + '.aiMatteColor', 0, 0, 0, type='double3')


##------------------------------------------------------------------------------------------------
## WhiteMask_Shader
##------------------------------------------------------------------------------------------------
class WhiteMask_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(WhiteMask_Shader, self).__init__()
        self.name = 'WhiteMask'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.node = mc.shadingNode("surfaceShader", asShader= True, name= self.name + "_surface")
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "SG", empty= True)
        mc.setAttr(self.node + ".outColor", 1, 1, 1, type= "double3")
        #Connect nodes
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        
        return self.node
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            if not mc.connectionInfo(shadingNode + '.aiMatteColorA', isDestination=True):
                mc.setAttr(shadingNode + '.aiMatteColorA', 1)
            mc.setAttr(shadingNode + '.aiMatteColor', 1, 1, 1, type='double3')


##------------------------------------------------------------------------------------------------
## RedRim_Shader
##------------------------------------------------------------------------------------------------
class RedRim_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(RedRim_Shader, self).__init__()
        self.name = 'RedRim'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.node = mc.shadingNode("lambert", asShader= True, name= self.name + "_shader")
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "_shaderSG", empty= True)
        mc.setAttr(self.node + ".outColor", 1, 1, 1, type= "double3")
        
        rampNode = self.nodesNetwork()
        
        mc.connectAttr(rampNode + ".outColor", self.node + ".color")
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        
        return self.node
    
    
    def nodesNetwork(self):
        #Extra nodes
        rampNode = mc.shadingNode("ramp", asTexture= True, name= "rimRed_ramp")
        samplerInfoNode =  mc.shadingNode("samplerInfo", asUtility= True, name= "samplerInfoRimRed_ramp")
        #Connect nodes
        mc.removeMultiInstance(rampNode + ".colorEntryList[1]", b= True)
        mc.setAttr(rampNode + ".colorEntryList[2].position", 1)
        mc.setAttr(rampNode + ".colorEntryList[2].color", 0, 0, 0, type= "double3")
        mc.setAttr(rampNode + ".colorEntryList[0].position", 0)
        mc.setAttr(rampNode + ".colorEntryList[0].color", 1, 0, 0, type= "double3")
        mc.connectAttr(samplerInfoNode + ".facingRatio", rampNode + ".vCoord")
        
        return rampNode
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            mc.connectAttr(self.node + '.outColor', shadingNode + '.aiMatteColor', force=True)


##------------------------------------------------------------------------------------------------
## GreenRim_Shader
##------------------------------------------------------------------------------------------------
class GreenRim_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(GreenRim_Shader, self).__init__()
        self.name = 'GreenRim'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.node = mc.shadingNode("lambert", asShader= True, name= self.name + "_shader")
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "_shaderSG", empty= True)
        mc.setAttr(self.node + ".outColor", 1, 1, 1, type= "double3")
        
        rampNode = self.nodesNetwork()
        
        mc.connectAttr(rampNode + ".outColor", self.node + ".color")
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        
        return self.node
    
    
    def nodesNetwork(self):
        #Extra nodes
        rampNode = mc.shadingNode("ramp", asTexture= True, name= "rimGreen_ramp")
        samplerInfoNode =  mc.shadingNode("samplerInfo", asUtility= True, name= "samplerInfoRimGreen_ramp")
        #Connect nodes
        mc.removeMultiInstance(rampNode + ".colorEntryList[1]", b= True)
        mc.setAttr(rampNode + ".colorEntryList[2].position", 1)
        mc.setAttr(rampNode + ".colorEntryList[2].color", 0, 0, 0, type= "double3")
        mc.setAttr(rampNode + ".colorEntryList[0].position", 0)
        mc.setAttr(rampNode + ".colorEntryList[0].color", 0, 1, 0, type= "double3")
        mc.connectAttr(samplerInfoNode + ".facingRatio", rampNode + ".vCoord")
        
        return rampNode
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            mc.connectAttr(self.node + '.outColor', shadingNode + '.aiMatteColor', force=True)


##------------------------------------------------------------------------------------------------
## BlueRim_Shader
##------------------------------------------------------------------------------------------------
class BlueRim_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(BlueRim_Shader, self).__init__()
        self.name = 'BlueRim'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.node = mc.shadingNode("lambert", asShader= True, name= self.name + "_shader")
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "_shaderSG", empty= True)
        mc.setAttr(self.node + ".outColor", 1, 1, 1, type= "double3")
       
        rampNode = self.nodesNetwork()
       
        mc.connectAttr(rampNode + ".outColor", self.node + ".color")
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        
        return self.node
    
    
    def nodesNetwork(self):
        #Extra nodes
        rampNode = mc.shadingNode("ramp", asTexture= True, name= "rimBlue_ramp")
        samplerInfoNode =  mc.shadingNode("samplerInfo", asUtility= True, name= "samplerInfoRimBlue_ramp")
        #Connect nodes
        mc.removeMultiInstance(rampNode + ".colorEntryList[1]", b= True)
        mc.setAttr(rampNode + ".colorEntryList[2].position", 1)
        mc.setAttr(rampNode + ".colorEntryList[2].color", 0, 0, 0, type= "double3")
        mc.setAttr(rampNode + ".colorEntryList[0].position", 0)
        mc.setAttr(rampNode + ".colorEntryList[0].color", 0, 0, 1, type= "double3")
        mc.connectAttr(samplerInfoNode + ".facingRatio", rampNode + ".vCoord")
        
        return rampNode
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            mc.connectAttr(self.node + '.outColor', shadingNode + '.aiMatteColor', force=True)


##------------------------------------------------------------------------------------------------
## WhiteRim_Shader
##------------------------------------------------------------------------------------------------
class WhiteRim_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(WhiteRim_Shader, self).__init__()
        self.name = 'WhiteRim'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.node = mc.shadingNode("lambert", asShader= True, name= self.name + "_shader")
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "_shaderSG", empty= True)
        mc.setAttr(self.node + ".outColor", 1, 1, 1, type= "double3")
        
        rampNode = self.nodesNetwork()
        
        mc.connectAttr(rampNode + ".outColor", self.node + ".color")
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        
        return self.node
    
    
    def nodesNetwork(self):
        #Extra nodes
        rampNode = mc.shadingNode("ramp", asTexture= True, name= "rimWhite_ramp")
        samplerInfoNode =  mc.shadingNode("samplerInfo", asUtility= True, name= "samplerInfoRimWhite_ramp")
        #Connect nodes
        mc.removeMultiInstance(rampNode + ".colorEntryList[1]", b= True)
        mc.setAttr(rampNode + ".colorEntryList[2].position", 1)
        mc.setAttr(rampNode + ".colorEntryList[2].color", 0, 0, 0, type= "double3")
        mc.setAttr(rampNode + ".colorEntryList[0].position", 0)
        mc.setAttr(rampNode + ".colorEntryList[0].color", 1, 1, 1, type= "double3")
        mc.connectAttr(samplerInfoNode + ".facingRatio", rampNode + ".vCoord")
        
        return rampNode
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            mc.connectAttr(self.node + '.outColor', shadingNode + '.aiMatteColor', force=True)


##------------------------------------------------------------------------------------------------
## UV_Shader
##------------------------------------------------------------------------------------------------
class UV_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(UV_Shader, self).__init__()
        self.name = 'UV'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.node = mc.shadingNode("surfaceShader", asShader= True, name= self.name + "_surface")
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "SG", empty= True)
        
        layeredNode = self.nodesNetwork()
        
        #Connect nodes
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        mc.connectAttr(layeredNode + ".outColor", self.node + ".outColor")
        
        return self.node
    
    
    def nodesNetwork(self):
        #Ramp green
        rampGreenNode = mc.shadingNode("ramp", asTexture= True, name= "greenUV_ramp")
        placeTwoDGreenNode = mc.shadingNode("place2dTexture", asUtility= True, name= "place2DTextureGreenUV_ramp")
        mc.connectAttr(placeTwoDGreenNode + ".outUV", rampGreenNode + ".uv")
        mc.connectAttr(placeTwoDGreenNode + ".outUvFilterSize", rampGreenNode + ".uvFilterSize")
        mc.removeMultiInstance(rampGreenNode + ".colorEntryList[1]", b= True)
        mc.setAttr(rampGreenNode + ".colorEntryList[2].position", 1)
        mc.setAttr(rampGreenNode + ".colorEntryList[2].color", 0, 1, 0, type= "double3")
        mc.setAttr(rampGreenNode + ".colorEntryList[0].position", 0)
        mc.setAttr(rampGreenNode + ".colorEntryList[0].color", 0, 0, 0, type= "double3")
        mc.setAttr(rampGreenNode + ".type", 0)
        #Ramp red
        rampRedNode = mc.shadingNode("ramp", asTexture= True, name= "redUV_ramp")
        placeTwoDRedNode = mc.shadingNode("place2dTexture", asUtility= True, name= "place2DTextureRedUV_ramp")
        mc.connectAttr(placeTwoDRedNode + ".outUV", rampRedNode + ".uv")
        mc.connectAttr(placeTwoDRedNode + ".outUvFilterSize", rampRedNode + ".uvFilterSize")
        mc.removeMultiInstance(rampRedNode + ".colorEntryList[1]", b= True)
        mc.setAttr(rampRedNode + ".colorEntryList[2].position", 1)
        mc.setAttr(rampRedNode + ".colorEntryList[2].color", 1, 0, 0, type= "double3")
        mc.setAttr(rampRedNode + ".colorEntryList[0].position", 0)
        mc.setAttr(rampRedNode + ".colorEntryList[0].color", 0, 0, 0, type= "double3")
        mc.setAttr(rampRedNode + ".type", 1)
        #Layered texture
        layeredNode = mc.shadingNode("layeredTexture", asTexture= True, name= "uv_layeredTexture")
        mc.connectAttr(rampGreenNode + ".outColor", layeredNode+ ".inputs[1].color", force= True)
        mc.connectAttr(rampRedNode + ".outColor", layeredNode+ ".inputs[0].color", force= True)
        mc.setAttr(layeredNode + ".inputs[0].blendMode", 4)
        mc.setAttr(layeredNode + ".inputs[1].blendMode", 4)
        
        return layeredNode
       
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            if not mc.connectionInfo(shadingNode + '.aiMatteColorA', isDestination=True):
                mc.setAttr(shadingNode + '.aiMatteColorA', 1)
            
            mc.connectAttr(self.node + ".outColor", shadingNode + ".aiMatteColor", force= True)


##------------------------------------------------------------------------------------------------
## OCC_Shader
##------------------------------------------------------------------------------------------------
class OCC_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(OCC_Shader, self).__init__()
        self.name = 'OCC'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.node = mc.shadingNode("surfaceShader", asShader= True, name= self.name + "_surface")
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "SG", empty= True)
        
        occNode = self.nodesNetwork()
        
        #Connect nodes
        mc.connectAttr(occNode + ".outColor", self.node + ".outColor")
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        
        return self.node
    
    
    def nodesNetwork(self):
        occNode=None
        if mc.getAttr('defaultRenderGlobals.ren') == "arnold":
            occNode = mc.shadingNode("aiAmbientOcclusion", asShader= True, name= "ambOcc_aiShader")
            mc.setAttr(occNode + ".samples", 5)
        elif mc.getAttr('defaultRenderGlobals.ren') == "vray":
            occNode = mc.shadingNode("VRayDirt", asTexture= True, name= "ambOcc_vrayShader")
            mc.setAttr(occNode + ".subdivs", 32)
            mc.setAttr(occNode + ".radius", 6)
            mc.setAttr(occNode + ".doubleSided", 1)

        return occNode
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            if not mc.connectionInfo(shadingNode + '.aiMatteColorA', isDestination=True):
                mc.setAttr(shadingNode + '.aiMatteColorA', 1)
            
            mc.connectAttr(self.node + ".outColor", shadingNode + ".aiMatteColor", force= True)


##------------------------------------------------------------------------------------------------
## Incidence_Shader
##------------------------------------------------------------------------------------------------
class Incidence_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(Incidence_Shader, self).__init__()
        self.name = 'Incidence'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.node = mc.shadingNode("surfaceShader", asShader= True, name= self.name + "_surface")
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "SG", empty= True)
        
        rampNode = self.nodesNetwork()
        
        mc.connectAttr(rampNode + ".outColor", self.node + ".outColor")
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        
        return self.node
    
    
    def nodesNetwork(self):
        #Extra nodes
        rampNode = mc.shadingNode("ramp", asTexture= True, name= "incidence_ramp")
        samplerInfoNode =  mc.shadingNode("samplerInfo", asUtility= True, name= "samplerInfo_ramp")
        #Connect nodes
        mc.removeMultiInstance(rampNode + ".colorEntryList[1]", b= True)
        mc.setAttr(rampNode + ".colorEntryList[2].position", 1)
        mc.setAttr(rampNode + ".colorEntryList[2].color", 0, 0, 0, type= "double3")
        mc.setAttr(rampNode + ".colorEntryList[0].position", 0)
        mc.setAttr(rampNode + ".colorEntryList[0].color", 1, 1, 1, type= "double3")
        mc.connectAttr(samplerInfoNode + ".facingRatio", rampNode + ".vCoord")
        
        return rampNode
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            
            if not mc.connectionInfo(shadingNode + '.aiMatteColorA', isDestination=True):
                mc.setAttr(shadingNode + '.aiMatteColorA', 1)
                
            mc.connectAttr(self.node + ".outColor", shadingNode + ".aiMatteColor", force= True)


##------------------------------------------------------------------------------------------------
## Diffuse_Shader
##------------------------------------------------------------------------------------------------
class Diffuse_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(Diffuse_Shader, self).__init__()
        self.name = 'Diffuse'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.node = mc.shadingNode("lambert", asShader= True, name= self.name + "_shader")
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "_shaderSG", empty= True)
        mc.setAttr(self.node + ".outColor", 1, 1, 1, type= "double3")
        
        gammaNode = self.nodesNetwork()
        
        mc.connectAttr(gammaNode + ".outValue", self.node + ".color", force= True)
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        
        return self.node
    
    
    def nodesNetwork(self):
        #Extra nodes
        gammaNode = mc.shadingNode("gammaCorrect", asUtility= True, name= "diffuse_gammaCorrect")
        #Connect nodes
        mc.setAttr(gammaNode + ".gammaX", 0.454)
        mc.setAttr(gammaNode + ".gammaY", 0.454)
        mc.setAttr(gammaNode + ".gammaZ", 0.454)
        mc.setAttr(gammaNode + ".value", 0.5, 0.5, 0.5, type= "double3")
        
        return gammaNode
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            mc.connectAttr(self.node + '.outColor', shadingNode + '.aiMatteColor', force=True)


##------------------------------------------------------------------------------------------------
## ShadowCatcher_Shader
##------------------------------------------------------------------------------------------------
class ShadowCatcher_Shader(Abstract_Shader):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self):
        super(ShadowCatcher_Shader, self).__init__()
        self.name = 'ShadowCatcher'
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def create(self):
        self.shadingGroup = mc.sets(renderable= True, noSurfaceShader= True, name= self.name + "_shaderSG", empty= True)
        if mc.getAttr('defaultRenderGlobals.ren') == "arnold":
            if int(mc.pluginInfo('mtoa', version=True, query=True)[0]) >= 2:
                self.node = mc.shadingNode("aiShadowMatte", asShader= True, name= self.name + "_shader")
            else:
                self.node = mc.shadingNode("aiShadowCatcher", asShader= True, name= self.name + "_shader")
        elif mc.getAttr('defaultRenderGlobals.ren') == "vray":
            self.node = mc.shadingNode("VRayMtlWrapper", asShader= True, name= self.name + "_shader")
            lambertNode = mc.shadingNode("lambert", asShader= True, name= "shadowCatcher_lambert")
            mc.setAttr(self.shadingNode[0] + ".matteSurface", 1)
            mc.setAttr(self.shadingNode[0] + ".shadows", 1)
            mc.setAttr(self.shadingNode[0] + ".alphaContribution", -1)
            mc.setAttr(self.shadingNode[0] + ".affectAlpha", 1)
            mc.connectAttr(lambertNode + ".outColor", self.node + ".baseMaterial", force= True)
        #Connect nodes
        mc.connectAttr(self.node + ".outColor", self.shadingGroup + ".surfaceShader")
        
        return self.node
    
    
    def setOverrides(self, _obj, _assignedSG, _checkOpct):
        shadingNode = mc.listConnections(_assignedSG + '.surfaceShader', d=False, s=True)[0]
        if mc.attributeQuery('aiEnableMatte', node=shadingNode, exists=True):
            mc.connectAttr(self.node + '.outColor', shadingNode + '.aiMatteColor', force=True)
    

##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------
def main():
    pass
    

if __name__=="__main__":
    main()