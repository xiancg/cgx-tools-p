# -*- coding: utf-8 -*-
'''
Create gammaNode for shading networks.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import maya.cmds as mc


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Isolate lights def
##--------------------------------------------------------------------------------------------
def createGammaNodes ():
	#If there is a selected node check it's input and output connections to bridge them
	allSel = mc.ls(sl= True)
	if len(allSel) >= 1:
		#Create connected gamma node
		allNodeTypes = mc.listNodeTypes("texture")
		for nodeTex in allSel:
			if mc.nodeType(nodeTex) in allNodeTypes:
				#Get connections
				connectedNode = mc.connectionInfo(nodeTex + ".outColor", isSource= True)
				#Create gamma node
				thisGammaNode = mc.shadingNode("gammaCorrect" ,asUtility= True)
				mc.setAttr(thisGammaNode + ".gammaX", 0.454)
				mc.setAttr(thisGammaNode + ".gammaY", 0.454)
				mc.setAttr(thisGammaNode + ".gammaZ", 0.454)
				#Rebuild connections
				if connectedNode:
					connectTo = mc.listConnections(nodeTex + ".outColor",  destination= True, source= False, plugs= True)
					for e in connectTo:
						mc.connectAttr(thisGammaNode + ".outValue", e, force= True)
				mc.connectAttr(nodeTex + ".outColor", thisGammaNode + ".value", force= True)
			else:
				#Create gamma node only
				thisGammaNode = mc.shadingNode("gammaCorrect" ,asUtility= True)
				mc.setAttr(thisGammaNode + ".gammaX", 0.454)
				mc.setAttr(thisGammaNode + ".gammaY", 0.454)
				mc.setAttr(thisGammaNode + ".gammaZ", 0.454)
	else:
		#Create gamma node only
		thisGammaNode = mc.shadingNode("gammaCorrect" ,asUtility= True)
		mc.setAttr(thisGammaNode + ".gammaX", 0.454)
		mc.setAttr(thisGammaNode + ".gammaY", 0.454)
		mc.setAttr(thisGammaNode + ".gammaZ", 0.454)


##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------   
def main ():
	createGammaNodes()
	
if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()