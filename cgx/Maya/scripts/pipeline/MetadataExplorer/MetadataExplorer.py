# -*- coding: utf-8 -*-
'''
Explore the metadata (fileInfo) on Maya files

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets, QtCore
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.DataViewModels as dvm


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/pipeline/MetadataExplorer'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.initComponents()
			self.initUI()
			self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
        Connect signals.
        '''
		self.open_BTN.clicked.connect(self.openSelectedFile)
		self.cancel_BTN.clicked.connect(self.baseMod.quitApp)
		self.customPath_BTN.clicked.connect(self.browsePath)
		
	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Metadata Explorer"
		self.__toolVersion = "2.0"
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.baseMod.MainMenu_Component.edit_config.setVisible(False)
	
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		pass
	
	
	def browsePath(self):
		"""Creates a file dialog to browse for a folder"""
		if self.saveAlert():
			browseDialog = QtWidgets.QFileDialog(self)
			browseDialog.setDirectory(self.sInfo.getProjectsFrom())
			browseDialog.setFileMode(browseDialog.FileMode.ExistingFiles)
			browseDialog.setNameFilters(["Maya Scenes (*.ma *.mb)", "Maya Binary (*.mb)", "Maya ASCII (*.ma)"])
			browseDialog.setLabelText(browseDialog.Accept,"Select")
			browseDialog.setWindowTitle("Select Maya files")
			dialogReturn = browseDialog.exec_()
			if dialogReturn == 1:
				selectedFiles = browseDialog.selectedFiles()
				self.populateMetadata(selectedFiles)
	
	
	def saveAlert(self):
		msgBox = QtWidgets.QMessageBox(self)
		msgBox.setText("Metadata Explorer needs to close your current project.")
		msgBox.setInformativeText("Do you want to save your changes?")
		msgBox.setStandardButtons(QtWidgets.QMessageBox.Save | QtWidgets.QMessageBox.Discard | QtWidgets.QMessageBox.Cancel)
		msgBox.setDefaultButton(QtWidgets.QMessageBox.Save)
		ret = msgBox.exec_()
		
		if ret == QtWidgets.QMessageBox.Save:
			currFile = mc.file(query=True, expandName=True)
			if currFile[0] == "C":
				browseDialog = QtWidgets.QFileDialog(self)
				browseDialog.setDirectory(self.uiConfig.getProjectsFolder())
				browseDialog.setFileMode(browseDialog.FileMode.AnyFile)
				browseDialog.setNameFilters(["Maya Binary (*.mb)", "Maya ASCII (*.ma)"])
				browseDialog.setLabelText(browseDialog.Accept,"Save")
				browseDialog.setWindowTitle("Save current project")
				dialogReturn = browseDialog.exec_()
				if dialogReturn == 1:
					selectedFiles = browseDialog.selectedFiles()
					finalPath = selectedFiles[0]
					if "\\" in finalPath:
						finalPath = finalPath.replace("\\","/")
					mc.file(rename= finalPath)
					mc.file(save=True, force=True)
					mc.file(new=True, force=True)
					return True
				else:
					return False
			else:
				mc.file(save=True, force=True)
				mc.file(new=True, force=True)
				return True
		elif ret == QtWidgets.QMessageBox.Discard:
			mc.file(new=True, force=True)
			return True
		elif ret == QtWidgets.QMessageBox.Cancel:
			return False
	
	
	def populateMetadata(self, _folder):
		mbList = []
		maList = []
		infoDict = {}
		for item in _folder:
			if item.endswith(".ma"):
				maList.append(item)
			elif item.endswith(".mb"):
				mbList.append(item)
		for each in maList:
			mc.file(each, open=True, loadReferenceDepth= "none", force=True, ignoreVersion= True, prompt= False)
			infoDictOrganized= self.filterInfoList(mc.fileInfo(query=True))
			infoDict[each] = infoDictOrganized
		for each in mbList:
			mc.file(each, open=True, loadReferenceDepth= "none", force=True, ignoreVersion= True, prompt= False)
			infoDictOrganized= self.filterInfoList(mc.fileInfo(query=True))
			infoDict[each] = infoDictOrganized
		
		mc.file(new=True, force=True)
		
		rowsList = []
		headers = ["File","Notes","Project","Seq","Shot","Stage","Author"]
		for each in sorted(infoDict.keys()):
			rowData = []
			for column in headers:
				if column == "File":
					rowData.append(each)
				elif column == "Notes":
					if "CGXnotes" in infoDict[each].keys():
						rowData.append(infoDict[each]["CGXnotes"])
					else:
						rowData.append(None)
				elif column == "Project":
					if "CGXproject" in infoDict[each].keys():
						rowData.append(infoDict[each]["CGXproject"])
					else:
						rowData.append(None)
				elif column == "Seq":
					if "CGXseq" in infoDict[each].keys():
						rowData.append(infoDict[each]["CGXseq"])
					else:
						rowData.append(None)
				elif column == "Shot":
					if "CGXshot" in infoDict[each].keys():
						rowData.append(infoDict[each]["CGXshot"])
					else:
						rowData.append(None)
				elif column == "Stage":
					if "CGXstage" in infoDict[each].keys():
						rowData.append(infoDict[each]["CGXstage"])
					else:
						rowData.append(None)
				elif column == "Author":
					if "CGXauthor" in infoDict[each].keys():
						rowData.append(infoDict[each]["CGXauthor"])
					else:
						rowData.append(None)
				else:
					rowData.append(None)
			rowsList.append(rowData)

		filesModel = dvm.DataTableModel(rowsList, headers, self)
		self.metadata_TABLEVIEW.setModel(filesModel)
		self.metadata_TABLEVIEW.resizeColumnsToContents()
		self.metadata_TABLEVIEW.setSelectionMode(self.metadata_TABLEVIEW.SingleSelection)
		self.metadata_TABLEVIEW.selectRow(0)
		self.metadata_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)

	
	
	def filterInfoList(self, _infoList):
		finalDict = {}
		i=0
		CGXkeys = ["CGXnotes","CGXproject","CGXseq","CGXshot","CGXstage","CGXauthor"]
		for item in _infoList:
			if item in CGXkeys:
				finalDict[item] = _infoList[i+1]
			i+=1

		return finalDict
	
	
	def openSelectedFile(self):
		selected = self.metadata_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selected:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		thisItem = self.metadata_TABLEVIEW.model().dataList[selRows[0]]
		mc.file(thisItem[0], open=True, force=True, prompt= False)
		
		self.close()
		
	
	##--------------------------------------------------------------------------------------------
	##Methods for metadata table view
	##--------------------------------------------------------------------------------------------
	def metadataOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.metadata_TABLEVIEW)
		openQ = menu.addAction("Open")
		menu.popup(self.metadata_TABLEVIEW.mapToGlobal(pos))
		
		openQ.triggered.connect(self.openSelectedFile)
	

##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/pipeline/MetadataExplorer_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()