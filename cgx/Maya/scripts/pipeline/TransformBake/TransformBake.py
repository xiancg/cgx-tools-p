# -*- coding: utf-8 -*-
'''
Bake objects or vertex transforms into a locator

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets
import cgx.Maya.scripts.maya_libs.Maya_Window as mayaGUI


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Transform bake def
##--------------------------------------------------------------------------------------------
def transformBake ():
    allSel = mc.ls(sl=True)
    if len(allSel) < 1:
        msgBox = QtWidgets.QMessageBox(mayaGUI.getMayaWindow())
        msgBox.setWindowTitle("Warning!")
        msgBox.setText("Please select at least one vertex or transform to bake.")
        msgBox.exec_()
    else:
        text, ok = QtWidgets.QInputDialog.getText(mayaGUI.getMayaWindow(), 'Sample By', 'Each ### frames:')
        sampleBy = 1
        if ok:
            sampleBy = float(text)
            #Stop refresh
            mc.refresh(suspend=True)
            for item in allSel:
                if ".vtx" in item:
                    vtxExp = mc.filterExpand(item , selectionMask = 31 , expand = True)
                    for vtx in vtxExp:
                        #THIS DOESN'T WORK IF UVS ARE OVERLAPED
                        cleanObjName = vtx[:vtx.index(".vtx")]
                        thisLoc = mc.spaceLocator(name= vtx + "__LOC_bkd", p=(0, 0, 0))
                        #Make constrain
                        vtxUVMap = mc.polyListComponentConversion(vtx, fv=True,tuv=True)
                        vtxUVs = mc.polyEditUV(vtxUVMap, query=True)
                        thisPopC = mc.pointOnPolyConstraint(vtx, thisLoc, offset=(0,0,0), weight=1)
                        thisPopCAttrs = mc.listAttr(thisPopC, ud=True)
                        mc.setAttr(thisPopC[0] + "." + thisPopCAttrs[1], vtxUVs[0])
                        mc.setAttr(thisPopC[0] + "." + thisPopCAttrs[2], vtxUVs[1])
                        #Bake animation
                        start = mc.playbackOptions(query= True, minTime = True)
                        end = mc.playbackOptions(query= True, maxTime = True)
                        mc.bakeResults(thisLoc, simulation= True, t=(int(start),int(end)), sampleBy= sampleBy, preserveOutsideKeys= True, sparseAnimCurveBake= False, removeBakedAttributeFromLayer= False, bakeOnOverrideLayer= False, minimizeRotation= True, at= ("tx","ty","tz","rx","ry","rz","sx","sy","sz"))
                        #Delete constrain
                        mc.delete(thisPopC[0])
                else: 
                    if mc.nodeType(item) == "transform":
                        thisLoc = mc.spaceLocator(name= item + "__LOC_bkd", p=(0, 0, 0))
                        #Make constrains
                        pConstrain = mc.parentConstraint(item, thisLoc, maintainOffset = False)
                        sConstrain = mc.scaleConstraint(item, thisLoc, maintainOffset = False)
                        #Bake animation
                        start = mc.playbackOptions(query= True, minTime = True)
                        end = mc.playbackOptions(query= True, maxTime = True)
                        mc.bakeResults(thisLoc, simulation= True, t=(int(start),int(end)), sampleBy= sampleBy, preserveOutsideKeys= True, sparseAnimCurveBake= False, removeBakedAttributeFromLayer= False, bakeOnOverrideLayer= False, minimizeRotation= True, at= ("tx","ty","tz","rx","ry","rz","sx","sy","sz"))
                        #Delete constrain
                        mc.delete(pConstrain,sConstrain)
                    else:
                        msgBox = QtWidgets.QMessageBox()
                        msgBox.setWindowTitle("Warning!")
                        msgBox.setText("Only Vertex or Transforms accepted.")
                        msgBox.exec_()
            
            #Restart refresh
            mc.refresh(suspend=False)
            
            #Done Alert!!
            msgBox = QtWidgets.QMessageBox(mayaGUI.getMayaWindow())
            msgBox.setWindowTitle("Done!")
            msgBox.setText("Done baking transforms.")
            msgBox.exec_()


##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------   
def main ():
    transformBake()
    
if __name__ == '__main__' or 'eclipsePython' in __name__:
    main()