# -*- coding: utf-8 -*-
'''
Camera Exporter tool.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/

Pending:
-Abstract camera naming conventions
-Add stereo functionality
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
from cgx.gui.Qt import QtWidgets, QtCore, QtGui
from cgx.gui.Qt import __binding__
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
import cgx.gui.DataViewModels as dvm
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityAnimation import SanityAnimation


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.1.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/pipeline/CameraExporter'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.initComponents()
			self.initUI()
			self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
        Connect signals.
        '''
		self.export_BTN.clicked.connect(self.preSanityCheck)
		self.cancel_BTN.clicked.connect(self.baseMod.quitApp)
		self.referenceCam_opt.triggered.connect(self.referenceCamera)
		
		#CONTEXT MENUS
		self.cameras_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.cameras_LISTVIEW.customContextMenuRequested.connect(self.camerasOptions)
		
	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Camera Exporter"
		self.__toolVersion = __version__
		
		#Set time range
		self.appTools.tRange.handleIn = 2
		self.appTools.tRange.handleOut = 2
		self.appTools.tRange.startFrame = 1
		self.appTools.tRange.endFrame = 100
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.baseMod.MainMenu_Component.edit_config.setVisible(False)
		self.camFormatsComp = self.appTools.createCameraFormatsComponent(self,self.centralWidget())
		self.camFormatsComp.pos = [0,20]
		self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo, tRange=self.appTools.tRange)
		self.projSeqShotMod = self.Pipeline_GUIModulesFactory.createModule('ProjSeqShot')
		self.projSeqShotMod.pos = [0,125]
		self.customPathComp = pipeComponents.CustomPath_Component(self,self.centralWidget(), self.sInfo)
		self.customPathComp.pos = [0,155]
		self.customPathComp.fileMode = self.customPathComp.browseDialog.FileMode.Directory
		self.timeRangeMod = self.Pipeline_GUIModulesFactory.createModule('TimeRange')
		self.timeRangeMod.pos = [0,262]
		#Add reference camera option
		self.referenceCam_opt = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.referenceCam_opt.setObjectName("reference_camera")
		self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[0], self.referenceCam_opt)
		className= str(self.__class__.__name__)
		if __binding__ in ('PySide', 'PyQt4'):
			self.referenceCam_opt.setText(QtWidgets.QApplication.translate(className, "Reference Camera", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			self.referenceCam_opt.setText(QtWidgets.QApplication.translate(className, "Reference Camera", None))
			
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
		self.appTools.app_initUI.setCurrentProject(self.projSeqShotMod.Project_Component)
		self.appTools.app_initUI.setCurrentSequence(self.projSeqShotMod.Sequence_Component)
		self.appTools.app_initUI.setCurrentShot(self.projSeqShotMod.Shot_Component)
		self.timeRangeMod.FPS_Component.initComponent(self.pInfo.maya_getFPS())
		self.appTools.app_initUI.checkFPS(self.timeRangeMod.FPS_Component)
		self.appTools.tRange.setFramesToTimeSlider()
		self.timeRangeMod.initComponents()
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		self.camerasModel = dvm.ObjectsListModel(self.appTools.listCams(), self)
		self.cameras_LISTVIEW.setModel(self.camerasModel)
		self.cameras_LISTVIEW.setSelectionMode(self.cameras_LISTVIEW.MultiSelection)
	
	
	def checkPlugins(self):
		'''
        Check if plugins are loaded.
        :return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
        :rtype: dict
        '''
		checkDict = self.appTools.checkPlugins(self)
		return checkDict
	
	
	def sanityCheck(self, _camsList):
		"""Runs the sanity check modules over the current file"""
		
		checkNodes = _camsList
		checkSelectedOnly = True
		checkAnimation = SanityAnimation([], checkNodes, checkSelectedOnly)
		checkAnimationResult = checkAnimation.check(["Film Aspect Ratio"], checkNodes, checkSelectedOnly)
		totalIssues = checkAnimationResult[1]
		finalDataList = checkAnimationResult[0]
		
		return totalIssues, finalDataList
	
	
	def preSanityCheck(self):
		check = self.checkPlugins()
		if len(check.keys()) <= 0:
			selCams = self.cameras_LISTVIEW.selectedIndexes()
			if len(selCams) < 1:
				self.baseMod.statusBar_update('You must select at least one camera to export.', alert=True)
			else:
				camsList = [item.data() for item in selCams]
				totalIssues, finalDataList = self.sanityCheck(camsList)
				if totalIssues >= 1:
					self.checkListDialog(finalDataList, totalIssues, self)
				else:
					self.exportCameras()
			
	
	def exportCameras(self):
		'''
		Tool main function. Gets all data from GUI and passes it to appTools to export camera with given parameters.
		'''
		self.appTools.removeUnknownNodes()
		
		if len(self.appTools.getUnknownNodes()) == 0:
			selCams = self.cameras_LISTVIEW.selectedIndexes()
			camsList = [item.data() for item in selCams]
			
			#Stop refresh
			self.appTools.refreshViewport(True)
			
			shot = str(self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentText())
			seq = str(self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.currentText())
			seqShotStr = seq + "_" + shot
			i = 0
			offsetFrameDict = self.appTools.tRange.offsetTimeRange(self.timeRangeMod.OffsetStartFrame_Component.offsetStartFrame_SPINBOX.value())
			for cam in camsList:
				camName = "C_" + seqShotStr + "_f{}_{}__CAM".format( int(self.appTools.tRange.startFrame), int(self.appTools.tRange.endFrame) )
				if self.customPathComp.customPath_CHKBOX.isChecked():
					seqShotStr = "custom"
				if self.timeRangeMod.OffsetStartFrame_Component.offsetStartFrame_CHKBOX.isChecked():
					camName = "C_" + seqShotStr + "_f{}_{}__CAM".format( offsetFrameDict['startFrame'], offsetFrameDict['endFrame'] )
				if len(camsList) > 1:
					zeroFillInt = str(i).zfill(2)
					if self.timeRangeMod.OffsetStartFrame_Component.offsetStartFrame_CHKBOX.isChecked():
						camName = "C_{}_".format(zeroFillInt) + seqShotStr + "_f{}_{}__CAM".format( offsetFrameDict['startFrame'], offsetFrameDict['endFrame'] )
					else:
						camName = "C_{}_".format(zeroFillInt) + seqShotStr + "_f{}_{}__CAM".format( int(self.appTools.tRange.startFrame), int(self.appTools.tRange.endFrame) )
					if self.useListNames_CHKBOX.isChecked():
						camName = cam + "_bkd"
					i += 1
				if self.useListNames_CHKBOX.isChecked():
					camName = cam + "_bkd"
			
			#Path
			exportPath = self.appTools.buildCameraPath(seq,shot)
			if self.customPathComp.customPath_CHKBOX.isChecked():
				exportPath = str( self.customPathComp.customPath_LINEEDIT.text() )
			sampleBy = float(self.timeRangeMod.SampleBy_Component.sampleBy_DBLSPINBOX.value())
			self.appTools.cam = cam
			self.appTools.path = exportPath
			self.appTools.sampleBy = sampleBy
			bakedCam = self.appTools.bakeCamera(camName, 
											self.timeRangeMod.OffsetStartFrame_Component.offsetStartFrame_CHKBOX.isChecked(), 
											offsetFrameDict['startFrame'])
			selectedFormats = {}
			for key, value in self.camFormatsComp.components.iteritems():
				if value.isChecked():
					selectedFormats[key] = True
			
			#File Name
			fileName = camName.rsplit("_f")[0]
			if "|" in fileName:
				fileName = fileName.lstrip("|")
			if fileName[-4:] == "_dup":
				fileName = fileName[:-4]
			
			result=self.appTools.exportCamera(bakedCam, fileName, selectedFormats)
			
			#Done- Update Status
			if result[0]:
				self.baseMod.statusBar_update("Done exporting cameras.", success=True)
			else:
				self.baseMod.statusBar_update(result[1], warning=True)
		
			#Restart refresh
			self.appTools.refreshViewport(False)
		else:
			self.baseMod.statusBar_update('Referenced or locked unknown nodes found in your scene.', warning=True)
	
	
	def referenceCamera(self):
		shot = str(self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentText())
		seq = str(self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.currentText())
		self.appTools.referenceCam(seq, shot)
	
	
	def checkListDialog(self, _list, _totalIssues, _parent):
		chekDialog = CheckList_DialogGUI(_list, _totalIssues, self.appTools, _parent)
		chekDialog.show()
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for cameras list view
	##--------------------------------------------------------------------------------------------     
	def camerasOptions (self,pos):
		'''
		Right-click popup menu for cameras list.
		'''
		menu = QtWidgets.QMenu(self.cameras_LISTVIEW)
		selectAllQ = menu.addAction("Select All")
		renameQ = menu.addAction("Rename camera")
		selectCamQ = menu.addAction("Select camera in scene")
		bakeCamQ = menu.addAction("Bake camera only")
		menu.popup(self.cameras_LISTVIEW.mapToGlobal(pos))
		
		selectAllQ.triggered.connect(self.selectAllOpt)
		renameQ.triggered.connect(self.renameCameraOpt)
		selectCamQ.triggered.connect(self.selectCamOpt)
		bakeCamQ.triggered.connect(self.bakeCamOpt)
	
	
	def selectAllOpt(self):
		'''
		Select all items listed.
		'''
		self.cameras_LISTVIEW.selectAll()
	
	
	def renameCameraOpt(self):
		'''
        Rename selected cam to user input given name. Uses apptools to perform its operation.
        '''
		selCams = self.cameras_LISTVIEW.selectedIndexes()
		if len(selCams) > 1:
			message= "You can rename only one camera at a time."
			self.baseMod.statusBar_update(message, alert=True)
		else:
			text, ok = QtWidgets.QInputDialog.getText(self, 'Rename Camera', 'New name:')
			if ok:
				self.appTools.renameCam(selCams[0].data(), text)
				self.camerasModel = dvm.ObjectsListModel(self.appTools.listCams())
				self.cameras_LISTVIEW.setModel(self.camerasModel)
				
				
	def selectCamOpt(self):
		'''
		Select items in host app. Uses apptools to perform selection.
		'''
		dataList= self.camerasModel.dataList
		selCams = self.cameras_LISTVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selCams:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		finalList = []
		for row in selRows:
			camName = dataList[row]
			finalList.append(camName)
		self.appTools.selectCams(finalList)
	
	
	def bakeCamOpt(self):
		'''
		Select items in host app. Uses apptools to perform selection.
		'''
		dataList= self.camerasModel.dataList
		selCams = self.cameras_LISTVIEW.selectedIndexes()
		offsetFrameDict = self.appTools.tRange.offsetTimeRange(self.timeRangeMod.OffsetStartFrame_Component.offsetStartFrame_SPINBOX.value())
		selRowsDirty = []
		for item in selCams:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		finalList = []
		for row in selRows:
			camName = dataList[row]
			finalList.append(camName)
		
		for each in finalList:
			self.appTools.cam = each
			self.appTools.bakeCamera(each + '_bkd',
									self.timeRangeMod.OffsetStartFrame_Component.offsetStartFrame_CHKBOX.isChecked(),
									offsetFrameDict['startFrame'])
			self.baseMod.statusBar_update("Baking " + each, status=True)
		
		self.baseMod.statusBar_update("Done baking cameras.", success=True)


##--------------------------------------------------------------------------------------------
##Create dialog
##--------------------------------------------------------------------------------------------
class CheckList_DialogGUI(QtWidgets.QDialog):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _list, _totalIssues, appTools, parent):
		super(CheckList_DialogGUI,self).__init__(parent)
		self.parent = parent
		self.appTools = appTools
		self.totalIssues = _totalIssues
		self.setupUi(self)
		self.setConnections()
		self.initModel(_list, _totalIssues)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setupUi(self, checkList_DIALOG):
		checkList_DIALOG.setObjectName("checkList_DIALOG")
		checkList_DIALOG.resize(801, 664)
		sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(checkList_DIALOG.sizePolicy().hasHeightForWidth())
		checkList_DIALOG.setSizePolicy(sizePolicy)
		checkList_DIALOG.setMinimumSize(QtCore.QSize(801, 664))
		checkList_DIALOG.setMaximumSize(QtCore.QSize(801, 664))
		self.gridLayoutWidget = QtWidgets.QWidget(checkList_DIALOG)
		self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 781, 651))
		self.gridLayoutWidget.setObjectName("gridLayoutWidget")
		self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
		self.gridLayout.setContentsMargins(0, 0, 0, 0)
		self.gridLayout.setObjectName("gridLayout")
		self.totalIssues_LABEL = QtWidgets.QLabel(self.gridLayoutWidget)
		self.totalIssues_LABEL.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.totalIssues_LABEL.setObjectName("totalIssues_LABEL")
		self.gridLayout.addWidget(self.totalIssues_LABEL, 2, 2, 1, 3)
		self.checkList_TABLEVIEW = QtWidgets.QTableView(self.gridLayoutWidget)
		self.checkList_TABLEVIEW.setObjectName("checkList_TABLEVIEW")
		self.gridLayout.addWidget(self.checkList_TABLEVIEW, 0, 1, 1, 4)
		self.yes_BTN = QtWidgets.QPushButton(self.gridLayoutWidget)
		sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.yes_BTN.sizePolicy().hasHeightForWidth())
		self.yes_BTN.setSizePolicy(sizePolicy)
		self.yes_BTN.setObjectName("yes_BTN")
		self.gridLayout.addWidget(self.yes_BTN, 3, 3, 1, 1)
		self.no_BTN = QtWidgets.QPushButton(self.gridLayoutWidget)
		sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.no_BTN.sizePolicy().hasHeightForWidth())
		self.no_BTN.setSizePolicy(sizePolicy)
		self.no_BTN.setLayoutDirection(QtCore.Qt.LeftToRight)
		self.no_BTN.setObjectName("no_BTN")
		self.gridLayout.addWidget(self.no_BTN, 3, 4, 1, 1)

		self.retranslateUi(checkList_DIALOG)
		QtCore.QMetaObject.connectSlotsByName(checkList_DIALOG)


	def retranslateUi(self, checkList_DIALOG):
		if __binding__ in ('PySide', 'PyQt4'):
			checkList_DIALOG.setWindowTitle(QtWidgets.QApplication.translate("checkList_DIALOG", "Check List", None, QtWidgets.QApplication.UnicodeUTF8))
			self.totalIssues_LABEL.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "This file has a total of {} issues. Would you like to export anyway?".format(self.totalIssues), None, QtWidgets.QApplication.UnicodeUTF8))
			self.yes_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "Yes", None, QtWidgets.QApplication.UnicodeUTF8))
			self.no_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "No", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			checkList_DIALOG.setWindowTitle(QtWidgets.QApplication.translate("checkList_DIALOG", "Check List", None))
			self.totalIssues_LABEL.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "This file has a total of {} issues. Would you like to export anyway?".format(self.totalIssues), None))
			self.yes_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "Yes", None))
			self.no_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "No", None))
	
	
	def setConnections(self):
		self.yes_BTN.clicked.connect(self.acceptPublish)
		self.no_BTN.clicked.connect(self.close)
		#CONTEXT MENUS
		self.checkList_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.checkList_TABLEVIEW.customContextMenuRequested.connect(self.checklistOptions)
		
		
	def initModel(self, _list, _totalIssues):
		#MODEL
		headers = ["Level", "Check", "Node", "Description"]
		self.checkModel = CheckListDataTableModel(_list, headers, self)
		if __binding__ in ('PySide', 'PyQt4'):
			from PySide import QtGui as oldGui
			proxyModel =  oldGui.QSortFilterProxyModel(self)
		elif __binding__ in ('PySide2', 'PyQt5'):
			proxyModel =  QtCore.QSortFilterProxyModel(self)
		proxyModel.setSourceModel(self.checkModel)
		self.checkList_TABLEVIEW.setModel(proxyModel)
		self.checkList_TABLEVIEW.sortByColumn(0,QtCore.Qt.DescendingOrder)
		self.checkList_TABLEVIEW.setAlternatingRowColors(True)
		self.checkList_TABLEVIEW.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
		self.checkList_TABLEVIEW.setColumnWidth(0,35)
		self.checkList_TABLEVIEW.setColumnWidth(1,130)
		self.checkList_TABLEVIEW.setColumnWidth(2,140)
		self.checkList_TABLEVIEW.setColumnWidth(3,300)
		self.checkList_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
		htalHeader = self.checkList_TABLEVIEW.horizontalHeader()
		htalHeader.setStretchLastSection(True)
	
	
	def acceptPublish(self):
		self.parent.exportCameras()
		self.close()
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for chek list dialog
	##--------------------------------------------------------------------------------------------
	def checklistOptions (self, pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.checkList_TABLEVIEW)
		selectNodeQ = menu.addAction("Select Node")
		menu.popup(self.checkList_TABLEVIEW.mapToGlobal(pos))
	
		selectNodeQ.triggered.connect(self.selectNodeOpt)
	
	
	def selectNodeOpt(self):
		dataList= self.checkModel.dataList
		selRowsProxy = self.checkList_TABLEVIEW.selectedIndexes()
		proxyModel = self.checkList_TABLEVIEW.model()
		selRows = []
		for each in selRowsProxy:
			selRows.append(proxyModel.mapToSource(each))
		selRowsDirty = []
		for item in selRows:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		finalList = []
		for row in selRows:
			nodeName = dataList[row][2]
			finalList.append(nodeName)
		self.appTools.selectCams(finalList)


##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for CheckList table
##--------------------------------------------------------------------------------------------
class CheckListDataTableModel(dvm.DataTableModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, dataList, headers, parent):
		super(CheckListDataTableModel, self).__init__(dataList, headers, parent)
		self.parent = parent
			
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def flags(self, index):
		"""Set flags of each cell"""
		return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
	
	
	def data(self, index, role):
		"""Collect data and put it in the table"""
		row = index.row()
		column = index.column()
		
		if role == QtCore.Qt.DisplayRole:
			return self.dataList[row][column]

		if role == QtCore.Qt.BackgroundRole:
			if index.column() == 0:
				if self.dataList[row][column] >= 85:
					return QtGui.QColor(255, 0, 0, 255)
				elif self.dataList[row][column] < 85 and self.dataList[row][column] >= 50:
					return QtGui.QColor(255, 127, 0, 255)
				elif self.dataList[row][column] < 50 and self.dataList[row][column] >= 1:
					return QtGui.QColor(0, 127, 255, 255)
				
		if role in [QtCore.Qt.ToolTipRole, QtCore.Qt.WhatsThisRole]:
			if self.dataList[row][0] >= 85:
				return "FIX IT OR DIE!"
			elif self.dataList[row][0] < 85 and self.dataList[row][0] >= 50:
				return "FIX IT, DON'T BE LAZY!"
			elif self.dataList[row][0] < 50 and self.dataList[row][0] >= 1:
				return "FIX IT, YOU FREAKING PERFECTIONIST!"


##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/pipeline/CameraExporter_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()