# -*- coding: utf-8 -*-
'''
Hard reload the entire CGX tools package.

Created on Aug 01, 2017

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import sys

##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Hard reload def
##--------------------------------------------------------------------------------------------
def hardReload():
    module = 'cgx'
    for k in sys.modules.keys():
        if k.lower().startswith(module):
            del sys.modules[k]


##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------   
def main ():
    hardReload()
    
if __name__ == '__main__' or 'eclipsePython' in __name__:
    main()