# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:\CGXtools\cgx\packages\pipeline\LocalDependencies\ui\debug.ui'
#
# Created: Mon Jul 04 13:11:40 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(781, 556)
        self.gridLayout = QtGui.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.debug_TEXTEDIT = QtGui.QTextEdit(Dialog)
        self.debug_TEXTEDIT.setObjectName("debug_TEXTEDIT")
        self.gridLayout.addWidget(self.debug_TEXTEDIT, 0, 0, 1, 1)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Debug", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

