# -*- coding: utf-8 -*-
'''
Local Dependencies tool.

Created on Feb 06, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os, time
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets, QtCore, QtGui
from cgx.gui.Qt import __binding__
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
from cgx.core.JSONManager import JSONManager
import cgx.gui.DataViewModels as dvm
from cgx.Maya.scripts.maya_libs.Maya_Dependencies import Maya_Dependencies
from cgx.core.Synchronizer import Synchronizer
import cgx.Maya.scripts.pipeline.LocalDependencies.LocalDependencies_icons


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/pipeline/LocalDependencies'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"


form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
##Load config from user specific location, if exists. Else copy it there and load.
##------------------------------------------------------------------------------------------------
config_json = appRootFolder + "/cfg/" + "LocalDependencies_config.json"
userPath = os.path.expanduser("~")
finalPath = userPath + "/.CGXTools/LocalDependencies_config.json"
if os.path.exists(finalPath):
	config_json = finalPath
else:
	sync = Synchronizer()
	sync.processPaths(config_json, finalPath)
	config_json = finalPath


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.initComponents()
			self.initUI()
			self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
        Connect signals.
        '''
		#Actions
		self.server_BTN.clicked.connect(self.readFromServer)
		self.local_BTN.clicked.connect(self.readFromLocal)
		self.useAllItems_CHKBOX.stateChanged.connect(self.useAllDeps)
		self.search_LINEEDIT.returnPressed.connect(self.applyFilters)
		self.search_BTN.clicked.connect(self.applyFilters)
		self.search_COMBOBOX.currentIndexChanged.connect(self.refreshCompleterColumn)
		#Filters
		self.references_CHKBOX.stateChanged.connect(self.applyFilters)
		self.exocortex_CHKBOX.stateChanged.connect(self.applyFilters)
		self.textures_CHKBOX.stateChanged.connect(self.applyFilters)
		self.alembic_CHKBOX.stateChanged.connect(self.applyFilters)
		self.pointCache_CHKBOX.stateChanged.connect(self.applyFilters)
		self.audio_CHKBOX.stateChanged.connect(self.applyFilters)
		self.yetiTexture_CHKBOX.stateChanged.connect(self.applyFilters)
		self.yetiCache_CHKBOX.stateChanged.connect(self.applyFilters)
		self.imagePlanes_CHKBOX.stateChanged.connect(self.applyFilters)
		self.selectAll_BTN.clicked.connect(self.selectAllFilters)
		self.selectNone_BTN.clicked.connect(self.selectNoneFilters)
		#ICONS
		self.search_BTN.setIcon(QtGui.QIcon(":/LocalDependencies_search.png"))
		self.search_BTN.setIconSize(QtCore.QSize(15,15))
		
		#CONTEXT MENUS
		self.deps_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.deps_TABLEVIEW.customContextMenuRequested.connect(self.depsOptions)
		
	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Local Dependencies"
		self.__toolVersion = __version__
		
		self.projDeps = Maya_Dependencies()
		self.syncStatusDict = {1:"In sync", 2:"Out of sync", 3:"Same date, different size", 4:"Different date, same size", 5:"Both paths missing", 6:"Server file missing", 7:"Local file missing"}
		self.configFile = JSONManager(config_json, self)
		
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.progComp = pipeComponents.Progress_Component(self,self.centralWidget())
		self.progComp.pos = [270,510]
		self.progComp.reset()
		#Add debugMode option
		self.debugMode = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.debugMode.setCheckable(True)
		self.debugMode.setObjectName("edit_debugMode")
		self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[0], self.debugMode)
		className= str(self.__class__.__name__)
		if __binding__ in ('PySide', 'PyQt4'):
			self.debugMode.setText(QtWidgets.QApplication.translate(className, "Debug Mode", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			self.debugMode.setText(QtWidgets.QApplication.translate(className, "Debug Mode", None))
			
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
		
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		self.populateDependencies()
	
	
	def checkPlugins(self):
		'''
        Check if plugins are loaded.
        :return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
        :rtype: dict
        '''
		checkDict = self.appTools.checkPlugins(self)
		return checkDict
	
	
	def populateAlembic(self, _headers):
		sync = Synchronizer(self)
		sync.statusDict = self.syncStatusDict
		dataList = []
		for depFile in self.projDeps.alembicFiles:
			rowDataList = []
			localPath = self.configFile.getValueByProperty("localDisk", False) + depFile["filePath"][3:]
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			serverPath = server + "/" + depFile["filePath"][3:]
			for column in _headers:
				if column == "Type":
					rowDataList.append("Alembic")
				elif column == "File":
					rowDataList.append(depFile["filePath"])
				elif column == "Sync Status":
					rowDataList.append(sync.getSyncStatus(serverPath,localPath))
				elif column == "Local last mod":
					if os.path.exists(localPath):
						timeStr = time.ctime(os.path.getmtime(localPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Server last mod":
					if os.path.exists(serverPath):
						timeStr = time.ctime(os.path.getmtime(serverPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		
		return dataList


	def populateExocortex(self, _headers):
		sync = Synchronizer(self)
		sync.statusDict = self.syncStatusDict
		dataList = []
		for depFile in self.projDeps.exocortexAlembicFiles:
			rowDataList = []
			localPath = self.configFile.getValueByProperty("localDisk", False) + depFile["filePath"][3:]
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			serverPath = server + "/" + depFile["filePath"][3:]
			for column in _headers:
				if column == "Type":
					rowDataList.append("Exocortex")
				elif column == "File":
					rowDataList.append(depFile["filePath"])
				elif column == "Sync Status":
					rowDataList.append(sync.getSyncStatus(serverPath,localPath))
				elif column == "Local last mod":
					if os.path.exists(localPath):
						timeStr = time.ctime(os.path.getmtime(localPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Server last mod":
					if os.path.exists(serverPath):
						timeStr = time.ctime(os.path.getmtime(serverPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		
		return dataList


	def populateAudio(self, _headers):
		sync = Synchronizer(self)
		sync.statusDict = self.syncStatusDict
		dataList = []
		for depFile in self.projDeps.audioFiles:
			rowDataList = []
			localPath = self.configFile.getValueByProperty("localDisk", False) + depFile["filePath"][3:]
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			serverPath = server + "/" + depFile["filePath"][3:]
			for column in _headers:
				if column == "Type":
					rowDataList.append("Audio")
				elif column == "File":
					rowDataList.append(depFile["filePath"])
				elif column == "Sync Status":
					rowDataList.append(sync.getSyncStatus(serverPath,localPath))
				elif column == "Local last mod":
					if os.path.exists(localPath):
						timeStr = time.ctime(os.path.getmtime(localPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Server last mod":
					if os.path.exists(serverPath):
						timeStr = time.ctime(os.path.getmtime(serverPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		
		return dataList


	def populateImagePlanes(self, _headers):
		sync = Synchronizer(self)
		sync.statusDict = self.syncStatusDict
		dataList = []
		for depFile in self.projDeps.imagePlaneFiles:
			rowDataList = []
			localPath = self.configFile.getValueByProperty("localDisk", False) + depFile["filePath"][3:]
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			serverPath = server + "/" + depFile["filePath"][3:]
			for column in _headers:
				if column == "Type":
					rowDataList.append("Image plane")
				elif column == "File":
					rowDataList.append(depFile["filePath"])
				elif column == "Sync Status":
					rowDataList.append(sync.getSyncStatus(serverPath,localPath))
				elif column == "Local last mod":
					if os.path.exists(localPath):
						timeStr = time.ctime(os.path.getmtime(localPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Server last mod":
					if os.path.exists(serverPath):
						timeStr = time.ctime(os.path.getmtime(serverPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		
		return dataList


	def populatePointCache(self, _headers):
		sync = Synchronizer(self)
		sync.statusDict = self.syncStatusDict
		dataList = []
		for depFile in self.projDeps.pointCacheFiles:
			rowDataList = []
			xmlLocalPath = self.configFile.getValueByProperty("localDisk", False) + depFile["xmlPath"][3:]
			mcxLocalPath = self.configFile.getValueByProperty("localDisk", False) + depFile["mcxPath"][3:]
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			xmlServerPath = server + "/" + depFile["xmlPath"][3:]
			mcxServerPath = server + "/" + depFile["mcxPath"][3:]
			for column in _headers:
				if column == "Type":
					rowDataList.append("Point cache")
				elif column == "File":
					rowDataList.append(depFile["xmlPath"])
				elif column == "Sync Status":
					rowDataList.append(sync.getSyncStatus(xmlServerPath,xmlLocalPath))
				elif column == "Local last mod":
					if os.path.exists(xmlLocalPath):
						timeStr = time.ctime(os.path.getmtime(xmlLocalPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Server last mod":
					if os.path.exists(xmlServerPath):
						timeStr = time.ctime(os.path.getmtime(xmlServerPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
			rowDataList = []
			for column in _headers:
				if column == "Type":
					rowDataList.append("Point cache")
				elif column == "File":
					rowDataList.append(depFile["mcxPath"])
				elif column == "Sync Status":
					rowDataList.append(sync.getSyncStatus(mcxServerPath,mcxLocalPath))
				elif column == "Local last mod":
					if os.path.exists(mcxLocalPath):
						timeStr = time.ctime(os.path.getmtime(mcxLocalPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Server last mod":
					if os.path.exists(mcxServerPath):
						timeStr = time.ctime(os.path.getmtime(mcxServerPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		
		return dataList


	def populateReferences(self, _headers):
		sync = Synchronizer(self)
		sync.statusDict = self.syncStatusDict
		dataList = []
		for depFile in self.projDeps.referenceFiles:
			rowDataList = []
			localPath = self.configFile.getValueByProperty("localDisk", False) + depFile["filePath"][3:]
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			serverPath = server + "/" + depFile["filePath"][3:]
			for column in _headers:
				if column == "Type":
					rowDataList.append("Reference")
				elif column == "File":
					rowDataList.append(depFile["filePath"])
				elif column == "Sync Status":
					rowDataList.append(sync.getSyncStatus(serverPath,localPath))
				elif column == "Local last mod":
					if os.path.exists(localPath):
						timeStr = time.ctime(os.path.getmtime(localPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Server last mod":
					if os.path.exists(serverPath):
						timeStr = time.ctime(os.path.getmtime(serverPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		
		return dataList


	def populateTextures(self, _headers):
		sync = Synchronizer(self)
		sync.statusDict = self.syncStatusDict
		dataList = []
		for depFile in self.projDeps.textureFiles:
			rowDataList = []
			localPath = self.configFile.getValueByProperty("localDisk", False) + depFile["filePath"][3:]
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			serverPath = server + "/" + depFile["filePath"][3:]
			for column in _headers:
				if column == "Type":
					rowDataList.append("Texture")
				elif column == "File":
					rowDataList.append(depFile["filePath"])
				elif column == "Sync Status":
					rowDataList.append(sync.getSyncStatus(serverPath,localPath))
				elif column == "Local last mod":
					if os.path.exists(localPath):
						timeStr = time.ctime(os.path.getmtime(localPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Server last mod":
					if os.path.exists(serverPath):
						timeStr = time.ctime(os.path.getmtime(serverPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		
		return dataList


	def populateYetiFiles(self, _headers):
		sync = Synchronizer(self)
		sync.statusDict = self.syncStatusDict
		dataList = []
		for depFile in self.projDeps.yetiFiles:
			rowDataList = []
			cacheLocalPath = self.configFile.getValueByProperty("localDisk", False) + depFile["cacheFileName"][3:]
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			cacheServerPath =  server + "/" + depFile["cacheFileName"][3:]
			if self.yetiCache_CHKBOX.isChecked():
				if depFile["cacheFileName"] != "":
					for column in _headers:
						if column == "Type":
							rowDataList.append("Yeti cache file")
						elif column == "File":
							rowDataList.append(depFile["cacheFileName"])
						elif column == "Sync Status":
							rowDataList.append(sync.getSyncStatus(cacheServerPath,cacheLocalPath))
						elif column == "Local last mod":
							if os.path.exists(cacheLocalPath):
								timeStr = time.ctime(os.path.getmtime(cacheLocalPath))
								rowDataList.append(timeStr)
							else:
								rowDataList.append(None)
						elif column == "Server last mod":
							if os.path.exists(cacheServerPath):
								timeStr = time.ctime(os.path.getmtime(cacheServerPath))
								rowDataList.append(timeStr)
							else:
								rowDataList.append(None)
						elif column == "Node":
							rowDataList.append(depFile["node"])
					dataList.append(rowDataList)

		return dataList


	def populateYetiTextures(self, _headers):
		sync = Synchronizer(self)
		sync.statusDict = self.syncStatusDict
		dataList = []
		for depFile in self.projDeps.yetiTextureFiles:
			rowDataList = []
			localPath = self.configFile.getValueByProperty("localDisk", False) + depFile["filePath"][3:]
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			serverPath = server + "/" + depFile["filePath"][3:]
			for column in _headers:
				if column == "Type":
					rowDataList.append("Yeti texture")
				elif column == "File":
					rowDataList.append(depFile["filePath"])
				elif column == "Sync Status":
					rowDataList.append(sync.getSyncStatus(serverPath,localPath))
				elif column == "Local last mod":
					if os.path.exists(localPath):
						timeStr = time.ctime(os.path.getmtime(localPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Server last mod":
					if os.path.exists(serverPath):
						timeStr = time.ctime(os.path.getmtime(serverPath))
						rowDataList.append(timeStr)
					else:
						rowDataList.append(None)
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		
		return dataList
	
	
	def populateDependencies(self):
		headers = ["Type", "File", "Sync Status", "Local last mod", "Server last mod", "Node"]
		dataList = []
		
		#Alembic files
		if self.alembic_CHKBOX.isChecked():
			dataList += self.populateAlembic(headers)

		#Exocortex Alembic files
		if self.exocortex_CHKBOX.isChecked():
			dataList += self.populateExocortex(headers)

		#Audio files
		if self.audio_CHKBOX.isChecked():
			dataList += self.populateAudio(headers)

		#Image plane files
		if self.imagePlanes_CHKBOX.isChecked():
			dataList += self.populateImagePlanes(headers)

		#Point cache files
		if self.pointCache_CHKBOX.isChecked():
			dataList += self.populatePointCache(headers)

		#Reference files
		if self.references_CHKBOX.isChecked():
			dataList += self.populateReferences(headers)

		#Texture files
		if self.textures_CHKBOX.isChecked():
			dataList += self.populateTextures(headers)

		#Yeti files
		if self.yetiCache_CHKBOX.isChecked():
			dataList += self.populateYetiFiles(headers)

		#Yeti textures
		if self.yetiTexture_CHKBOX.isChecked():
			dataList += self.populateYetiTextures(headers)

		depsModel = DepsDataTableModel(self.sInfo, dataList, headers, self)
		self.deps_TABLEVIEW.setModel(depsModel)
		self.deps_TABLEVIEW.resizeColumnsToContents()
		self.deps_TABLEVIEW.selectRow(0)
		self.deps_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
		
		self.populateSearch(headers)
		
		self.applyFilters()
	
	
	def populateSearch(self, headers):
		headersModel = dvm.ObjectsListModel(headers)
		self.search_COMBOBOX.setModel(headersModel)
		self.search_COMBOBOX.setCurrentIndex(1)
		completer = QtWidgets.QCompleter(self)
		completer.setModel(self.deps_TABLEVIEW.model())
		completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
		completer.setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
		completer.setCompletionRole(QtCore.Qt.DisplayRole)
		self.search_LINEEDIT.setCompleter(completer)
		
	
	def refreshCompleterColumn(self):
		self.search_LINEEDIT.completer().setCompletionColumn(self.deps_TABLEVIEW.model().headers.index(self.search_COMBOBOX.currentText()))
		
		
	def applyFilters(self):
		'''Filters dependencies list according to user GUI input'''
		depsList = self.deps_TABLEVIEW.model().dataList
		
		showList = []
		for dep in depsList:
			showBool = True
			if not self.filterDependencies(dep):
				showBool = False
			if not self.filterBySearch(dep):
				showBool = False
			
			if showBool:
				showList.append(dep[self.deps_TABLEVIEW.model().headers.index("Node")])
		
		i = 0
		for row in self.deps_TABLEVIEW.model().dataList:
			if row[self.deps_TABLEVIEW.model().headers.index("Node")] in showList:
				if row[self.deps_TABLEVIEW.model().headers.index("Type")] == 'Yeti texture' and self.yetiTexture_CHKBOX.isChecked():
					self.deps_TABLEVIEW.showRow(i)
				elif row[self.deps_TABLEVIEW.model().headers.index("Type")] == 'Yeti cache file' and self.yetiCache_CHKBOX.isChecked():
					self.deps_TABLEVIEW.showRow(i)
				else:
					self.deps_TABLEVIEW.showRow(i)
			else:
				if row[self.deps_TABLEVIEW.model().headers.index("Type")] == 'Yeti texture' and not self.yetiTexture_CHKBOX.isChecked():
					self.deps_TABLEVIEW.hideRow(i)
				elif row[self.deps_TABLEVIEW.model().headers.index("Type")] == 'Yeti cache file' and not self.yetiCache_CHKBOX.isChecked():
					self.deps_TABLEVIEW.hideRow(i)
				else:
					self.deps_TABLEVIEW.hideRow(i)
			i += 1
	
	
	def filterDependencies(self, dep):
		if mc.nodeType(dep[self.deps_TABLEVIEW.model().headers.index('Node')]) == "AlembicNode" and self.alembic_CHKBOX.isChecked():
			return True
		elif mc.nodeType(dep[self.deps_TABLEVIEW.model().headers.index('Node')]) == "ExocortexAlembicFile" and self.exocortex_CHKBOX.isChecked():
			return True
		elif mc.nodeType(dep[self.deps_TABLEVIEW.model().headers.index('Node')]) == "reference" and self.references_CHKBOX.isChecked():
			return True
		elif mc.nodeType(dep[self.deps_TABLEVIEW.model().headers.index('Node')]) == "file" and self.textures_CHKBOX.isChecked():
			return True
		elif mc.nodeType(dep[self.deps_TABLEVIEW.model().headers.index('Node')]) == "cacheFile" and self.pointCache_CHKBOX.isChecked():
			return True
		elif mc.nodeType(dep[self.deps_TABLEVIEW.model().headers.index('Node')]) == "audio" and self.audio_CHKBOX.isChecked():
			return True
		elif mc.nodeType(dep[self.deps_TABLEVIEW.model().headers.index('Node')]) == "pgYetiMaya" and self.yetiCache_CHKBOX.isChecked() and dep[self.deps_TABLEVIEW.model().headers.index('Type')] == 'Yeti cache file':
			return True
		elif mc.nodeType(dep[self.deps_TABLEVIEW.model().headers.index('Node')]) == "pgYetiMaya" and self.yetiTexture_CHKBOX.isChecked() and dep[self.deps_TABLEVIEW.model().headers.index('Type')] == 'Yeti texture':
			return True
		elif mc.nodeType(dep[self.deps_TABLEVIEW.model().headers.index('Node')]) == "imagePlane" and self.imagePlanes_CHKBOX.isChecked():
			return True
		else:
			return False
	
	
	def filterBySearch(self, dep):
		'''Filter rows by name'''
		if self.search_LINEEDIT.text() != '':
			if self.search_LINEEDIT.text() in dep[self.deps_TABLEVIEW.model().headers.index(self.search_COMBOBOX.currentText())]:
				return True
			else:
				return False
		else:
			return True
		
	
	def selectAllFilters(self):
		self.alembic_CHKBOX.setChecked(True)
		self.exocortex_CHKBOX.setChecked(True)
		self.audio_CHKBOX.setChecked(True)
		self.imagePlanes_CHKBOX.setChecked(True)
		self.pointCache_CHKBOX.setChecked(True)
		self.references_CHKBOX.setChecked(True)
		self.yetiTexture_CHKBOX.setChecked(True)
		self.yetiCache_CHKBOX.setChecked(True)
		self.textures_CHKBOX.setChecked(True)


	def selectNoneFilters(self):
		self.alembic_CHKBOX.setChecked(False)
		self.exocortex_CHKBOX.setChecked(False)
		self.audio_CHKBOX.setChecked(False)
		self.imagePlanes_CHKBOX.setChecked(False)
		self.pointCache_CHKBOX.setChecked(False)
		self.references_CHKBOX.setChecked(False)
		self.yetiTexture_CHKBOX.setChecked(False)
		self.yetiCache_CHKBOX.setChecked(False)
		self.textures_CHKBOX.setChecked(False)
	
	
	def useAllDeps(self):
		if self.useAllItems_CHKBOX.isChecked():
			self.deps_TABLEVIEW.setEnabled(False)
		else:
			self.deps_TABLEVIEW.setEnabled(True)
	
	
	def debugDialog(self,  _text, _parent):
		dialog = QtWidgets.QDialog(_parent)
		dialog.setObjectName("Dialog")
		dialog.resize(781, 556)
		gridLayout = QtWidgets.QGridLayout(dialog)
		gridLayout.setObjectName("gridLayout")
		debug_TEXTEDIT = QtWidgets.QTextEdit(dialog)
		debug_TEXTEDIT.setObjectName("debug_TEXTEDIT")
		debug_TEXTEDIT.setText(_text)
		debug_TEXTEDIT.setReadOnly(True)
		gridLayout.addWidget(debug_TEXTEDIT, 0, 0, 1, 1)
		if __binding__ in ('PySide', 'PyQt4'):
			dialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog", "Debug", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			dialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog", "Debug", None))
		dialog.show()
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for dependencies table view
	##--------------------------------------------------------------------------------------------
	def depsOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.deps_TABLEVIEW)
		readFromServerQ = menu.addAction("Read from server")
		readFromLocalQ = menu.addAction("Read from local")
		syncDepQ = menu.addAction("Sync local with server")
		if not self.useAllItems_CHKBOX.isChecked():
			selectNodeQ = menu.addAction("Select node")
		menu.popup(self.deps_TABLEVIEW.mapToGlobal(pos))
		
		readFromServerQ.triggered.connect(self.readFromServer)
		readFromLocalQ.triggered.connect(self.readFromLocal)
		syncDepQ.triggered.connect(self.syncFilesOnly)
		if not self.useAllItems_CHKBOX.isChecked():
			selectNodeQ.triggered.connect(self.selectNode)
	
	
	def selectNode(self):
		#List selected deps
		selected = self.deps_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selected:
			row = item.row()
			selRowsDirty.append(row)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		
		if len(selRows) > 1:
			self.baseMod.statusBar_update("Please select one dependency only to select the corresponding node.", alert=True)
		elif len(selRows) < 1:
			self.baseMod.statusBar_update("Please select at least one dependency to select the corresponding node.", alert=True)
		else:
			for index in selRows:
				rowDict = {}
				i = 0
				for header in self.deps_TABLEVIEW.model().headers:
					rowDict[header] = self.deps_TABLEVIEW.model().dataList[index][i]
					i += 1
				mc.select(rowDict["Node"])
	
	
	def readFromServer(self):
		"""Set deps to read server files"""
		#List selected deps
		selected = self.deps_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selected:
			row = item.row()
			selRowsDirty.append(row)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		if self.useAllItems_CHKBOX.isChecked():
			selRows = range(len(self.deps_TABLEVIEW.model().dataList))
		
		#Set progress bar
		self.progComp.reset()
		self.progComp.minimum = 0
		self.progComp.maximum = len(selRows)
		
		if len(selRows) < 1:
			self.baseMod.statusBar_update("Please select at least one dependency to read from server.", alert=True)
		else:
			sync = Synchronizer(self)
			sync.statusDict = self.syncStatusDict
			lastIndex = 0
			for index in selRows:
				rowDict = {}
				i = 0
				for header in self.deps_TABLEVIEW.model().headers:
					rowDict[header] = self.deps_TABLEVIEW.model().dataList[index][i]
					i += 1
				self.baseMod.statusBar_update("Working on: " + rowDict["File"], status=True)
				localPath = self.configFile.getValueByProperty("localDisk", False) + rowDict["File"][3:]
				server = self.sInfo.getProjectsFrom()
				if server [-1] in ["/","\\"]:
					server = server[:-1]
				serverPath = server + "/" + rowDict["File"][3:]
				syncStatus = sync.getSyncStatus(serverPath,localPath)
				if syncStatus in ["Local file missing","Out of sync","In sync"]:
					if rowDict["Type"] == "Yeti texture":
						fileDir = rowDict["File"].rsplit("/", 1)[0]
						yetiPath = self.sInfo.getProjectsFrom() + fileDir[3:]
						self.projDeps.fileAttrChange(yetiPath, rowDict["Node"], rowDict["Type"])
					else:
						self.projDeps.fileAttrChange(serverPath, rowDict["Node"], rowDict["Type"])
				elif syncStatus == "Server file missing":
					mc.warning(serverPath + " is missing.")
				elif syncStatus == "Both paths missing":
					mc.warning(serverPath + " and " + localPath + " are missing.")
				elif syncStatus == "Different date, same size":
					mc.warning("Local and server dates are different, size is the same. Please resolve this issue manually.")
					if rowDict["Type"] == "Yeti texture":
						fileDir = rowDict["File"].rsplit("/", 1)[0]
						yetiPath = self.sInfo.getProjectsFrom() + fileDir[3:]
						self.projDeps.fileAttrChange(yetiPath, rowDict["Node"], rowDict["Type"])
					else:
						self.projDeps.fileAttrChange(serverPath, rowDict["Node"], rowDict["Type"])
				elif syncStatus == "Same date, different size":
					mc.warning("Local and server file sizes are different, date is the same. Please resolve this issue manually.")
				lastIndex = index
				
				self.progComp.value = self.progComp.value + 1
			
			#Sync log- Debug Mode
			if self.debugMode.isChecked():
				self.debugDialog(',\n'.join(sync.log), self)
			
			self.resetTool()
			self.deps_TABLEVIEW.selectRow(lastIndex)
			
			self.progComp.reset()
			
			#Done Alert!!
			self.baseMod.statusBar_update("Done setting reads from server!", success=True)
	
	
	def readFromLocal(self):
		"""Set deps to read local files"""
		#List selected deps
		selected = self.deps_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selected:
			row = item.row()
			selRowsDirty.append(row)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		if self.useAllItems_CHKBOX.isChecked():
			selRows = range(len(self.deps_TABLEVIEW.model().dataList))
		
		#Set progress bar
		self.progComp.reset()
		self.progComp.minimum = 0
		self.progComp.maximum = len(selRows)
		
		if len(selRows) < 1:
			self.baseMod.statusBar_update("Please select at least one dependency to sync and read from local.", alert=True)
		else:
			sync = Synchronizer(self)
			sync.statusDict = self.syncStatusDict
			lastIndex = 0
			repeatedDependencies = []
			for index in selRows:
				rowDict = {}
				i = 0
				for header in self.deps_TABLEVIEW.model().headers:
					rowDict[header] = self.deps_TABLEVIEW.model().dataList[index][i]
					i += 1
				self.baseMod.statusBar_update("Working on: " + rowDict["File"], status=True)
				localPath = self.configFile.getValueByProperty("localDisk", False) + rowDict["File"][3:]
				server = self.sInfo.getProjectsFrom()
				if server [-1] in ["/","\\"]:
					server = server[:-1]
				serverPath = server + "/" + rowDict["File"][3:]
				syncStatus = sync.getSyncStatus(serverPath,localPath)
				
				#DO NOT COPY THE SAME FILE OVER AND OVER IF IT'S USED BY DIFFERENT NODES
				syncFile = True
				if localPath in repeatedDependencies:
					self.baseMod.statusBar_update("Skipped dependency, already copied: " + localPath, status=True)
					syncFile = False
				else:
					repeatedDependencies.append(localPath)
					
				if syncStatus in ["Local file missing","Out of sync", "Different date, same size", "Same date, different size"]:
					if rowDict["Type"] == "Texture":
						sequenceStatus = mc.getAttr(rowDict["Node"] + ".useFrameExtension")
						tileStatus = mc.getAttr(rowDict["Node"] + ".uvTilingMode")
						if sequenceStatus == True or tileStatus != 0:
							if syncFile:
								sync.processPaths(serverPath,localPath, isSequence=True)
						else:
							if syncFile:
								sync.processPaths(serverPath, localPath, isTexture=True)
						self.projDeps.fileAttrChange(localPath, rowDict["Node"], rowDict["Type"])
					elif rowDict["Type"] == "Yeti texture":
						fileDir = self.configFile.getValueByProperty("localDisk", False) + rowDict["File"].rsplit("/", 1)[0][3:]
						yetiPath = self.configFile.getValueByProperty("localDisk", False) + rowDict["File"][3:]
						if syncFile:
							sync.processPaths(serverPath, yetiPath)
						self.projDeps.fileAttrChange(fileDir, rowDict["Node"], rowDict["Type"])
					else:
						if syncFile:
							sync.processPaths(serverPath,localPath)
						self.projDeps.fileAttrChange(localPath, rowDict["Node"], rowDict["Type"])
				elif syncStatus == "Server file missing":
					mc.warning(serverPath + " is missing.")
				elif syncStatus == "Missing dependency":
					mc.warning(serverPath + " and " + localPath + " are missing.")
				elif syncStatus == "In sync":
					if rowDict["Type"] == "Yeti texture":
						fileDir = self.configFile.getValueByProperty("localDisk", False) + rowDict["File"].rsplit("/", 1)[0][3:]
						self.projDeps.fileAttrChange(fileDir, rowDict["Node"], rowDict["Type"])
					else:
						self.projDeps.fileAttrChange(localPath, rowDict["Node"], rowDict["Type"])
				lastIndex = index
				self.progComp.value = self.progComp.value + 1

			
			#Sync log- Debug Mode
			if self.debugMode.isChecked():
				self.debugDialog(',\n'.join(sync.log), self)
			
			self.resetTool()
			self.deps_TABLEVIEW.selectRow(lastIndex)
			
			self.progComp.reset()
			
			#Done Alert!!
			self.baseMod.statusBar_update("Done syncing and setting reads from local!", success=True)
	
	
	def syncFilesOnly(self):
		#List selected deps
		selected = self.deps_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selected:
			row = item.row()
			selRowsDirty.append(row)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		if self.useAllItems_CHKBOX.isChecked():
			selRows = range(len(self.deps_TABLEVIEW.model().dataList))

		#Set progress bar
		self.progComp.reset()
		self.progComp.minimum = 0
		self.progComp.maximum = len(selRows)

		if len(selRows) < 1:
			self.baseMod.statusBar_update("Please select at least one dependency to sync.", alert=True)
		else:
			sync = Synchronizer(self)
			sync.statusDict = self.syncStatusDict
			lastIndex = 0
			repeatedDependencies = []
			for index in selRows:
				rowDict = {}
				i = 0
				for header in self.deps_TABLEVIEW.model().headers:
					rowDict[header] = self.deps_TABLEVIEW.model().dataList[index][i]
					i += 1
				self.baseMod.statusBar_update("Working on: " + rowDict["File"], status=True)
				localPath = self.configFile.getValueByProperty("localDisk", False) + rowDict["File"][3:]
				server = self.sInfo.getProjectsFrom()
				if server [-1] in ["/","\\"]:
					server = server[:-1]
				serverPath = server + "/" + rowDict["File"][3:]
				syncStatus = sync.getSyncStatus(serverPath,localPath)
				#DO NOT COPY THE SAME FILE OVER AND OVER IF IT'S USED BY DIFFERENT NODES
				syncFile = True
				if localPath in repeatedDependencies:
					self.baseMod.statusBar_update("Skipped dependency, already copied: " + localPath, status=True)
					syncFile = False
				else:
					repeatedDependencies.append(localPath)

				if syncStatus in ["Local file missing","Out of sync", "Different date, same size", "Same date, different size"]:
					if rowDict["Type"] == "Texture":
						sequenceStatus = mc.getAttr(rowDict["Node"] + ".useFrameExtension")
						tileStatus = mc.getAttr(rowDict["Node"] + ".uvTilingMode")
						if sequenceStatus == True or tileStatus != 0:
							if syncFile:
								sync.processPaths(serverPath,localPath, isTexture=False, isSequence=True)
						else:
							if syncFile:
								sync.processPaths(serverPath,localPath, isTexture=True)
					elif rowDict["Type"] == "Yeti texture":
						yetiPath = self.configFile.getValueByProperty("localDisk", False) + rowDict["File"][3:]
						if syncFile:
							sync.processPaths(serverPath, yetiPath)
					else:
						if syncFile:
							sync.processPaths(serverPath,localPath)
				elif syncStatus == "Server file missing":
					mc.warning(serverPath + " is missing.")
				elif syncStatus == "Missing dependency":
					mc.warning(serverPath + " and " + localPath + " are missing.")
				lastIndex = index
				self.progComp.value = self.progComp.value + 1
			
			
			#Sync log- Debug Mode
			if self.debugMode.isChecked():
				self.debugDialog(',\n'.join(sync.log), self)
			
			self.resetTool()
			self.deps_TABLEVIEW.selectRow(lastIndex)
			
			self.progComp.reset()
			
			#Done Alert!!
			self.baseMod.statusBar_update("Done syncing!", success=True)
	

##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for dependencies table
##--------------------------------------------------------------------------------------------
class DepsDataTableModel(dvm.DataTableModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, sInfo, dataList, headers, parent):
		super(DepsDataTableModel, self).__init__(dataList, headers, parent)
		self.configFile = JSONManager(config_json)
		self.sInfo = sInfo


	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def data(self, index, role):
		"""Collect data and put it in the table"""
		row = index.row()
		column = index.column()
		
		if role == QtCore.Qt.DisplayRole:
			value = self.dataList[row][column]
			return value
		
		if role == QtCore.Qt.ToolTipRole:
			value = self.dataList[row][column]
			return value
		
		if role == QtCore.Qt.EditRole:
			return self.dataList[row][column]
		
		if role == QtCore.Qt.DecorationRole:
			value = self.dataList[row][column]
			if self.headers[column] == "File":
				if value[:3] == self.sInfo.getProjectsFrom():
					return QtGui.QIcon(":/LocalDependencies_server.png")
				elif value[:3] == self.configFile.getValueByProperty("localDisk", False):
					return QtGui.QIcon(":/LocalDependencies_local.png")
			if self.headers[column] == "Sync Status":
				if value == "In sync":
					return QtGui.QIcon(":/LocalDependencies_inSync.png")
				else:
					return QtGui.QIcon(":/LocalDependencies_outOfSync.png")
	

##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
		:type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		
		self.configFile = JSONManager(config_json, self)
		self.localDisk = self.configFile.getValueByProperty("localDisk", False)
		
		self.initUI()
		self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		#SIGNALS
		self.cancel_BTN.clicked.connect(self.cancelAll)
		self.apply_BTN.clicked.connect(self.applyConfig)
		self.localDisk_BTN.clicked.connect(self.browseDisk)
	
	
	def initUI(self):
		"""Read JSON config file"""
		self.localDisk_LINEEDIT.setText(self.localDisk)
	
	
	def browseDisk(self):
		"""Creates a file dialog to browse for a folder"""
		browseDialog = QtWidgets.QFileDialog(self)
		browseDialog.setFileMode(browseDialog.FileMode.Directory)
		browseDialog.setOptions(browseDialog.ShowDirsOnly)
		dialogReturn = browseDialog.exec_()
		if dialogReturn == 1:
			selectedDir = browseDialog.selectedFiles()
			self.localDisk_LINEEDIT.setText(selectedDir[0])
	
	
	def applyConfig(self):
		"""Apply changes to JSON file"""
		newLocalDisk = self.localDisk_LINEEDIT.text()
		if len(newLocalDisk) > 3:
			newLocalDisk += "/"
		
		jsonObj = self.configFile.jsonObj
		jsonObj["localDisk"] = newLocalDisk
		self.configFile.writeJson(jsonObj, self.configFile.fileName)
		
		self.done(1)
	
	
	def cancelAll(self):
		"""Method to close the UI"""
		
		self.done(0)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/pipeline/LocalDependencies_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()