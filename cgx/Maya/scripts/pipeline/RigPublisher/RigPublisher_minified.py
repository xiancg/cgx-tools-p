__DEVMODE=True
import sys,os,json,time,shutil
import maya.cmds as mc
from PySide import QtCore,QtGui
import pysideuic
import shiboken
import xml.etree.ElementTree as xml
from cStringIO import StringIO
import maya.OpenMayaUI as mui
__author__="Chris Granados"
__copyright__="Copyright 2017, Chris Granados"
__credits__=["Chris Granados","Max Rocamora"]
__version__="2.0.0"
__email__="chris.granados@xiancg.com"
def loadUiType(uiFile):
 parsed=xml.parse(uiFile)
 widget_class=parsed.find('widget').get('class')
 form_class=parsed.find('class').text
 with open(uiFile,'r')as f:
  o=StringIO()
  frame={}
  pysideuic.compileUi(f,o,indent=0)
  pyc=compile(o.getvalue(),'<string>','exec')
  exec pyc in frame
  form_class=frame['Ui_%s'%form_class]
  base_class=eval('QtGui.%s'%widget_class)
 return form_class,base_class
def wrapinstance(ptr,base=None):
 if ptr is None:
  return None
 ptr=long(ptr)
 if globals().has_key('shiboken'):
  if base is None:
   qObj=shiboken.wrapInstance(long(ptr),QtCore.QObject)
   metaObj=qObj.metaObject()
   cls=metaObj.className()
   superCls=metaObj.superClass().className()
   if hasattr(QtGui,cls):
    base=getattr(QtGui,cls)
   elif hasattr(QtGui,superCls):
    base=getattr(QtGui,superCls)
   else:
    base=QtGui.QWidget
  return shiboken.wrapInstance(long(ptr),base)
 else:
  return None
def getMayaWindow():
 ptr=mui.MQtUtil.mainWindow()
 return wrapinstance(long(ptr))
appRootFolder=os.path.dirname(__file__)
if __DEVMODE:
 appRootFolder='M:/CGXtools/cgx/Maya/scripts/pipeline/RigPublisher'
sys.path.append(appRootFolder)
import RigPublisher_icons
main_ui=appRootFolder+"/ui/"+"main.ui"
help_ui=appRootFolder+"/ui/"+"help.ui"
about_ui=appRootFolder+"/ui/"+"about.ui"
config_ui=appRootFolder+"/ui/"+"config.ui"
configfile=appRootFolder+"/cfg/"+"RigPublisher_config.json"
form,base=loadUiType(main_ui)
helpform,helpbase=loadUiType(help_ui)
aboutform,aboutbase=loadUiType(about_ui)
configform,configbase=loadUiType(config_ui)
class Main_GUI(form,base):
 def __init__(self,parent=getMayaWindow()):
  super(Main_GUI,self).__init__(parent)
  self.setupUi(self)
  self.initTool()
  self.createComponents()
  self.setConnections()
  self.initComponents()
  self.checkConfig()
  self.initUI()
  self.setAttribute(QtCore.Qt.WA_DeleteOnClose,True)
 def setConnections(self):
  self.edit_quit.triggered.connect(self.quitApp)
  self.edit_config.triggered.connect(lambda:self.createDialog(Config_DialogGUI))
  self.help_help.triggered.connect(lambda:self.createDialog(Help_DialogGUI))
  self.help_about.triggered.connect(lambda:self.createDialog(About_DialogGUI))
  self.statusbar.messageChanged.connect(self.statusBar_style)
  self.customSuffix_CHKBOX.clicked.connect(self.refreshCustomSuffix)
  self.publishDeps_CHKBOX.clicked.connect(self.refreshDependencies)
  self.assetType_COMBOBOX.currentIndexChanged.connect(self.refreshAssetType)
  self.assetName_COMBOBOX.currentIndexChanged.connect(self.refreshAssets)
  self.subAssetName_COMBOBOX.currentIndexChanged.connect(self.refreshSubAssets)
  self.publishRig_BTN.clicked.connect(self.preSanityCheck)
  self.dependencies_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
  self.dependencies_TABLEVIEW.customContextMenuRequested.connect(self.depsOptions)
 def initTool(self):
  self.initAttrs()
 def initAttrs(self):
  self.__client="defaultClient"
  self.__toolName="Rig Publisher"
  self.__toolVersion="2.0"
  self.jsonConfig=ConfigFileJSON(configfile)
  self.usedGeom=[]
 def createComponents(self):
  self.menubar=QtGui.QMenuBar(self)
  self.menubar.setGeometry(QtCore.QRect(0,0,452,18))
  self.menubar.setObjectName("menubar")
  self.menuEdit=QtGui.QMenu(self.menubar)
  self.menuEdit.setObjectName("menuEdit")
  self.menuHelp=QtGui.QMenu(self.menubar)
  self.menuHelp.setObjectName("menuHelp")
  self.setMenuBar(self.menubar)
  self.edit_reset=QtGui.QAction(self)
  self.edit_reset.setObjectName("edit_reset")
  self.edit_config=QtGui.QAction(self)
  self.edit_config.setObjectName("edit_config")
  self.edit_quit=QtGui.QAction(self)
  self.edit_quit.setObjectName("edit_quit")
  self.edit_debugMode=QtGui.QAction(self)
  self.edit_debugMode.setCheckable(True)
  self.edit_debugMode.setObjectName("edit_debugMode")
  self.help_help=QtGui.QAction(self)
  self.help_help.setObjectName("help_help")
  self.help_about=QtGui.QAction(self)
  self.help_about.setObjectName("help_about")
  self.menuEdit.addAction(self.edit_debugMode)
  self.menuEdit.addSeparator()
  self.menuEdit.addAction(self.edit_reset)
  self.menuEdit.addAction(self.edit_config)
  self.menuEdit.addSeparator()
  self.menuEdit.addAction(self.edit_quit)
  self.menuHelp.addAction(self.help_help)
  self.menuHelp.addAction(self.help_about)
  self.menubar.addAction(self.menuEdit.menuAction())
  self.menubar.addAction(self.menuHelp.menuAction())
  self.statusbar=QtGui.QStatusBar(self)
  self.statusbar.setObjectName("statusbar")
  self.setStatusBar(self.statusbar)
  self.locateComponents()
  QtCore.QMetaObject.connectSlotsByName(self)
 def locateComponents(self):
  className=str(self.__class__.__name__)
  self.menuEdit.setTitle(QtGui.QApplication.translate(className,"Edit",None,QtGui.QApplication.UnicodeUTF8))
  self.menuHelp.setTitle(QtGui.QApplication.translate(className,"Help",None,QtGui.QApplication.UnicodeUTF8))
  self.edit_debugMode.setText(QtGui.QApplication.translate(className,"Debug Mode",None,QtGui.QApplication.UnicodeUTF8))
  self.edit_reset.setText(QtGui.QApplication.translate(className,"Reset Tool",None,QtGui.QApplication.UnicodeUTF8))
  self.edit_config.setText(QtGui.QApplication.translate(className,"Configuration",None,QtGui.QApplication.UnicodeUTF8))
  self.edit_quit.setText(QtGui.QApplication.translate(className,"Quit",None,QtGui.QApplication.UnicodeUTF8))
  self.help_help.setText(QtGui.QApplication.translate(className,"Help",None,QtGui.QApplication.UnicodeUTF8))
  self.help_about.setText(QtGui.QApplication.translate(className,"About",None,QtGui.QApplication.UnicodeUTF8))
 def initComponents(self):
  self.statusBar_update(self.__toolName+" "+self.__toolVersion+" loaded.",status=True)
 def statusBar_update(self,_message,_timeout=5000,**kwargs):
  self.statusbar.clearMessage()
  if 'status' in kwargs.keys():
   if kwargs['status']:
    self.statusbar.showMessage(_message,_timeout)
    self.statusbar.setStyleSheet("background: rgba(172, 172, 172,100);")
  elif 'warning' in kwargs.keys():
   if kwargs['warning']:
    self.statusbar.showMessage(_message)
    self.statusbar.setStyleSheet("background: rgba(255, 0, 0,100);")
  elif 'success' in kwargs.keys():
   if kwargs['success']:
    self.statusbar.showMessage(_message)
    self.statusbar.setStyleSheet("background: rgba(0, 255, 0,100);")
  elif 'alert' in kwargs.keys():
   if kwargs['alert']:
    self.statusbar.showMessage(_message,_timeout)
    self.statusbar.setStyleSheet("background: rgba(255, 255, 0,100);")
 def statusBar_style(self):
  if self.statusbar.currentMessage()=="":
   self.statusbar.setStyleSheet("background: rgba(0, 0, 0, 0);")
 def checkConfig(self):
  if os.path.exists(configfile):
   self.jsonConfig=ConfigFileJSON(configfile)
   if not os.path.exists(self.jsonConfig.getFolder('sharedFolder'))or not os.path.exists(self.jsonConfig.getFolder('riggingFolder')):
    configDialog=Config_DialogGUI(self)
    dialogReturn=configDialog.exec_()
    if dialogReturn==1:
     self.jsonConfig=ConfigFileJSON(configfile)
    else:
     self.quitApp()
  else:
   configDialog=Config_DialogGUI(self)
   dialogReturn=configDialog.exec_()
   if dialogReturn==1:
    self.jsonConfig=ConfigFileJSON(configfile)
   else:
    self.quitApp()
 def initUI(self):
  self.customSuffix_COMBOBOX.setEnabled(False)
  self.populateAssetTypes()
  self.populateAssets()
  self.populateSubAssets()
  self.populateSuffixes()
  self.populateDependencies()
  self.populatePublished()
 def populateAssetTypes(self):
  datalist=['CHARACTERS','PROPS','VEHICLES']
  model=ObjectsListModel(datalist,self)
  self.assetType_COMBOBOX.setModel(model)
  filePath=mc.file(expandName=True,query=True)
  splitList=self.__splitPath(filePath)
  item=datalist[0]
  for each in splitList:
   if each in datalist:
    item=each
    break
  i=len(datalist)
  for x in range(i):
   if datalist[x]==item:
    self.assetType_COMBOBOX.setCurrentIndex(x)
 def populateAssets(self):
  assetType=self.assetType_COMBOBOX.currentText()
  path=self.jsonConfig.getFolder('riggingFolder')+'/ASSETS/'+assetType
  assetsList=[]
  if os.path.exists(path):
   for folder in os.listdir(path):
    if folder[0]!='$':
     assetsList.append(folder)
  assetsModel=ObjectsListModel(assetsList)
  self.assetName_COMBOBOX.setModel(assetsModel)
  if len(assetsList)>=1:
   filePath=mc.file(expandName=True,query=True)
   splitList=self.__splitPath(filePath) 
   item=assetsList[0]
   for each in splitList:
    if each in assetsList:
     item=each
     break
   i=len(assetsList)
   for x in range(i):
    if assetsList[x]==item:
     self.assetName_COMBOBOX.setCurrentIndex(x)
 def populateSubAssets(self):
  assetType=self.assetType_COMBOBOX.currentText()
  asset=self.assetName_COMBOBOX.currentText()
  path=self.jsonConfig.getFolder('riggingFolder')+'/ASSETS/'+assetType+'/'+asset+'/SUBASSETS'
  subassetsList=[None]
  if os.path.exists(path):
   for folder in os.listdir(path):
    if folder[0]!='$':
     subassetsList.append(folder)
  else:
   subassetsList=[None]
  subassetsModel=ObjectsListModel(subassetsList)
  self.subAssetName_COMBOBOX.setModel(subassetsModel)
  filePath=mc.file(expandName=True,query=True)
  splitList=self.__splitPath(filePath)
  item=subassetsList[0]
  for each in splitList:
   if each in subassetsList:
    item=each
    break
  i=len(subassetsList)
  for x in range(i):
   if subassetsList[x]==item:
    self.subAssetName_COMBOBOX.setCurrentIndex(x)
 def populateSuffixes(self):
  datalist=['Layout','Mocap','Proxy','Custom']
  model=ObjectsListModel(datalist,self)
  self.customSuffix_COMBOBOX.setModel(model)
 def populateDependencies(self):
  projDeps=Maya_Dependencies()
  headers=["Type","File","Node"]
  dataList=[]
  for depFile in projDeps.imagePlaneFiles:
   rowDataList=[]
   for column in headers:
    if column=="Type":
     rowDataList.append("Image plane")
    elif column=="File":
     rowDataList.append(depFile["filePath"])
    elif column=="Node":
     rowDataList.append(depFile["node"])
   dataList.append(rowDataList)
  for depFile in projDeps.referenceFiles:
   rowDataList=[]
   for column in headers:
    if column=="Type":
     rowDataList.append("Reference")
    elif column=="File":
     rowDataList.append(depFile["filePath"])
    elif column=="Node":
     rowDataList.append(depFile["node"])
   dataList.append(rowDataList)
  for depFile in projDeps.textureFiles:
   rowDataList=[]
   for column in headers:
    if column=="Type":
     rowDataList.append("Texture")
    elif column=="File":
     rowDataList.append(depFile["filePath"])
    elif column=="Node":
     rowDataList.append(depFile["node"])
   dataList.append(rowDataList)
  depsModel=DataTableModel(dataList,headers)
  self.dependencies_TABLEVIEW.setModel(depsModel)
  self.dependencies_TABLEVIEW.resizeColumnsToContents()
  self.dependencies_TABLEVIEW.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
  self.dependencies_TABLEVIEW.selectRow(0)
  self.dependencies_TABLEVIEW.setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
 def populatePublished(self):
  sync=Synchronizer(self)
  sync.statusDict={1:"Already published",2:"Published but different",3:"Same date, different size",4:"Different date, same size",5:"Both files missing",6:"Not Published",7:"Current file missing"}
  trgPath=self.jsonConfig.getFolder('sharedFolder')+'/ASSETS/'+self.assetType_COMBOBOX.currentText()+'/'+self.assetName_COMBOBOX.currentText()+'/Published'
  if self.subAssetName_COMBOBOX.currentText()not in[None,'']:
   trgPath=self.jsonConfig.getFolder('sharedFolder')+'/ASSETS/'+self.assetType_COMBOBOX.currentText()+'/'+self.assetName_COMBOBOX.currentText()+'/SUBASSETS/'+self.subAssetName_COMBOBOX.currentText()+'/Published'
  filesList=[]
  if trgPath!=None:
   if os.path.exists(trgPath):
    filesList=[f for f in os.listdir(trgPath)if os.path.isfile(os.path.join(trgPath,f))]
  if len(filesList)>=1:
   for each in sorted(filesList,reverse=True):
    if each.split("_")[0]!='RIG':
     indexNum=filesList.index(each)
     del(filesList[indexNum])
   currentFile=mc.file(expandName=True,query=True)
  headers=["Status","Name","Last Update","Size(Bytes)"]
  dataList=[]
  for each in filesList:
   rowDataList=[]
   for column in headers:
    if column=="Status":
     rowDataList.append(sync.getSyncStatus(currentFile,trgPath+"/"+each))
    elif column=="Name":
     rowDataList.append(each)
    elif column=="Last Update":
     fileModDate=time.ctime(os.path.getmtime(trgPath+"/"+each))
     rowDataList.append(fileModDate)
    elif column=="Size(Bytes)":
     statinfo=os.stat(trgPath+"/"+each)
     fileSize=float(statinfo.st_size)
     rowDataList.append(fileSize)
   dataList.append(rowDataList)
  publishedModel=PublishedDataTableModel(dataList,headers,self.published_TABLEVIEW)
  self.published_TABLEVIEW.setModel(publishedModel)
  self.published_TABLEVIEW.resizeColumnsToContents()
  self.published_TABLEVIEW.selectRow(0)
  self.published_TABLEVIEW.setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
 def refreshCustomSuffix(self):
  if self.customSuffix_CHKBOX.isChecked():
   self.customSuffix_COMBOBOX.setEnabled(True)
  else:
   self.customSuffix_COMBOBOX.setEnabled(False)
 def refreshDependencies(self):
  if self.publishDeps_CHKBOX.isChecked():
   self.dependencies_TABLEVIEW.setEnabled(True)
  else:
   self.dependencies_TABLEVIEW.setEnabled(False)
 def refreshAssetType(self):
  self.populateAssets()
  self.populateSubAssets()
 def refreshAssets(self):
  self.populateSubAssets()
  self.populatePublished()
 def refreshSubAssets(self):
  self.populatePublished()
 def publishRig(self):
  mc.file(save=True,force=True)
  sync=Synchronizer(self)
  sync.statusDict={1:"Already published",2:"Published but different",3:"Same date, different size",4:"Different date, same size",5:"Both files missing",6:"Not Published",7:"Current file missing"}
  assetType=self.assetType_COMBOBOX.currentText()
  assetName=self.assetName_COMBOBOX.currentText()
  subAssetName=self.subAssetName_COMBOBOX.currentText()
  trgPath=self.jsonConfig.getFolder('sharedFolder')+'/ASSETS/'+assetType+'/'+assetName+'/Published'
  if subAssetName not in[None,'']:
   trgPath=self.jsonConfig.getFolder('sharedFolder')+'/ASSETS/'+assetType+'/'+assetName+'/SUBASSETS/'+subAssetName+'/Published'
  if not os.path.exists(trgPath):
   os.makedirs(trgPath)
  srcPath=mc.file(expandName=True,query=True)
  origFile=srcPath
  newName=self.buildFileName(srcPath)
  mc.file(rename=trgPath+"/"+newName)
  self.statusBar_update("Saving published file to: "+trgPath+"/"+newName,status=True)
  mc.file(save=True,force=True)
  srcPath=mc.file(expandName=True,query=True)
  selRows=range(len(self.dependencies_TABLEVIEW.model().dataList))
  lastIndex=0
  mayaDeps=Maya_Dependencies()
  repeatedDependencies=[]
  for index in selRows:
   rowDict={}
   i=0
   for header in self.dependencies_TABLEVIEW.model().headers:
    rowDict[header]=self.dependencies_TABLEVIEW.model().dataList[index][i]
    i+=1
   srcPath=rowDict["File"]
   fileName=rowDict["File"].rsplit("/",1)[1]
   syncFile=True
   if srcPath in repeatedDependencies:
    self.statusBar_update("Skipped dependency, already copied: "+srcPath,status=True)
    syncFile=False
   else:
    repeatedDependencies.append(srcPath)
   if rowDict["Type"]in["Texture","Image plane"]:
    finalPath=trgPath+"/Maps/"+fileName
    sequenceStatus=mc.getAttr(rowDict["Node"]+".useFrameExtension")
    tileStatus=0
    if mc.attributeQuery("uvTilingMode",node=rowDict["Node"],exists=True):
     tileStatus=mc.getAttr(rowDict["Node"]+".uvTilingMode")
    if sequenceStatus==True or tileStatus!=0:
     if syncFile:
      self.skipByStatus(sync,srcPath,finalPath,isSequence=True)
     mayaDeps.fileAttrChange(finalPath,rowDict["Node"],rowDict["Type"])
    else:
     if syncFile:
      self.skipByStatus(sync,srcPath,finalPath,isTexture=True)
     mayaDeps.fileAttrChange(finalPath,rowDict["Node"],rowDict["Type"])
   elif rowDict["Type"]in["Reference"]:
    finalPath=trgPath+"/Reference/"+fileName
    if syncFile:
     self.skipByStatus(sync,srcPath,finalPath)
    mayaDeps.fileAttrChange(finalPath,rowDict["Node"],rowDict["Type"])
   elif rowDict["Type"]in["Custom"]:
    finalPath=trgPath+"/Custom/"+fileName
    if syncFile: 
     self.skipByStatus(sync,srcPath,finalPath,isTexture=True)
   lastIndex=index
  mc.file(save=True,force=True)
  self.dependencies_TABLEVIEW.selectRow(lastIndex)
  mc.file(origFile,open=True,force=True,prompt=False)
  if self.edit_debugMode.isChecked():
   self.debugDialog(',\n'.join(sync.log),self)
  self.statusBar_update("Done publishing: "+trgPath,success=True)
  self.resetTool()
 def skipByStatus(self,sync,_source,_target,**kwargs):
  if sync.getSyncStatus(_source,_target)=="In sync":
   self.statusBar_update("Dependency skipped, already existed: "+_target,status=True)
  else:
   self.statusBar_update("Processing dependency: "+_source,status=True)
   if 'isSequence' in kwargs.keys():
    if kwargs['isSequence']:
     sync.processPaths(_source,_target,isSequence=True)
   elif 'isTexture' in kwargs.keys():
    if kwargs['isTexture']:
     sync.processPaths(_source,_target,isTexture=True)
   else:
    sync.processPaths(_source,_target)
 def buildFileName(self,srcPath):
  newName=srcPath
  fileType='RIG'
  assetName=self.assetName_COMBOBOX.currentText()
  subAssetName=self.subAssetName_COMBOBOX.currentText()
  if subAssetName not in[None,""]:
   assetName=subAssetName
  customSuffix=self.customSuffix_COMBOBOX.currentText()
  if self.customSuffix_CHKBOX.isChecked():
   fileName=srcPath.rsplit("/",1)[1]
   newName="{}_{}_{}.{}".format(fileType,assetName,customSuffix,"ma")
   if fileName[-2:]=="mb":
    newName="{}_{}_{}.{}".format(fileType,assetName,customSuffix,"mb")
  else:
   fileName=srcPath.rsplit("/",1)[1]
   newName="{}_{}.{}".format(fileType,assetName,"ma")
   if fileName[-2:]=="mb":
    newName="{}_{}.{}".format(fileType,assetName,"mb")
  return newName
 def preSanityCheck(self):
  totalIssues,finalDataList=self.sanityCheck()
  if totalIssues>=1:
   checkListDialog=CheckList_DialogGUI(finalDataList,totalIssues,self)
   dialogReturn=checkListDialog.exec_()
   if dialogReturn==1:
    self.publishRig()
  else:
   self.publishRig()
 def sanityCheck(self):
  checkSelectedOnly=False
  checkNodes=[]
  finalDataList=[]
  totalIssues=0
  checkRigging=SanityRigging([],checkNodes,checkSelectedOnly)
  checkRiggingResult=checkRigging.check(checkRigging.checkMethods,checkNodes,checkSelectedOnly)
  totalIssues=checkRiggingResult[1]
  if totalIssues>=1:
   finalDataList= checkRiggingResult[0]
  return totalIssues,finalDataList
 def __splitPath(self,_path):
  slashIndex=_path.rfind("/")
  splitList=[]
  if slashIndex==-1:
   splitList=_path.split("\\")
  else:
   splitList=_path.split("/")
  return splitList
 def quitApp(self):
  self.close()
 def resetTool(self):
  self.initAttrs()
  self.initComponents()
  self.initUI()
 def createDialog(self,_dialogClass):
  createdDialog=_dialogClass(self)
  createdDialog.show()
 def debugDialog(self,_text,_parent):
  dialog=QtGui.QDialog(_parent)
  dialog.setObjectName("Dialog")
  dialog.resize(781,556)
  gridLayout=QtGui.QGridLayout(dialog)
  gridLayout.setObjectName("gridLayout")
  debug_TEXTEDIT=QtGui.QTextEdit(dialog)
  debug_TEXTEDIT.setObjectName("debug_TEXTEDIT")
  debug_TEXTEDIT.setText(_text)
  debug_TEXTEDIT.setReadOnly(True)
  gridLayout.addWidget(debug_TEXTEDIT,0,0,1,1)
  dialog.setWindowTitle(QtGui.QApplication.translate("Dialog","Debug",None,QtGui.QApplication.UnicodeUTF8))
  dialog.show()
 def depsOptions(self,pos):
  menu=QtGui.QMenu(self.dependencies_TABLEVIEW)
  reloadDepsQ=menu.addAction("Reload dependencies")
  selectNodeQ=menu.addAction("Select node")
  menu.addSeparator()
  addCustomDepQ=menu.addAction("Add custom dependency")
  menu.popup(self.dependencies_TABLEVIEW.mapToGlobal(pos))
  reloadDepsQ.triggered.connect(self.reloadDeps)
  selectNodeQ.triggered.connect(self.selectNode)
  addCustomDepQ.triggered.connect(self.addCustomDep)
 def selectNode(self):
  selected=self.dependencies_TABLEVIEW.selectedIndexes()
  selRowsDirty=[]
  for item in selected:
   row=item.row()
   selRowsDirty.append(row)
  selSet=set(selRowsDirty)
  selRows=list(selSet)
  if len(selRows)>1:
   self.statusBar_update("Please select one dependency only to select the corresponding node.",alert=True)
  elif len(selRows)<1:
   self.statusBar_update("Please select at least one dependency to select the corresponding node.",alert=True)
  else:
   for index in selRows:
    rowDict={}
    i=0
    for header in self.dependencies_TABLEVIEW.model().headers:
     rowDict[header]=self.dependencies_TABLEVIEW.model().dataList[index][i]
     i+=1
    if rowDict["Node"]!=None:
     mc.select(rowDict["Node"],add=True)
 def reloadDeps(self):
  self.populateDependencies()
 def addCustomDep(self):
  browseDialog=QtGui.QFileDialog(self)
  browseDialog.setDirectory(self.jsonConfig.getFolder('riggingFolder'))
  browseDialog.setFileMode(browseDialog.FileMode.ExistingFile)
  browseDialog.setLabelText(browseDialog.Accept,"Select")
  browseDialog.setWindowTitle("Choose custom dependency")
  dialogReturn=browseDialog.exec_()
  if dialogReturn==1:
   thisFile=browseDialog.selectedFiles()[0]
   if "\\" in thisFile:
    thisFile=thisFile.replace("\\","/")
   self.dependencies_TABLEVIEW.model().insertRows(self.dependencies_TABLEVIEW.model().rowCount(QtCore.QModelIndex()),1,["Custom",thisFile,None])
class BakingMethods():
 def __init__(self,_geoDict={},_sampleBy=0.25):
  self.geoDict=_geoDict
  self.bakedGeom=[]
  self.sampleBy=_sampleBy
  self.clusters=[]
 def vertexBakeMethod(self,_geom):
  cleanName=self.geoDict[_geom][1]+"_"+self.geoDict[_geom][0]+"_bkd"
  polyUniteNode=mc.createNode("polyUnite",name=self.geoDict[_geom][1]+"_"+self.geoDict[_geom][0]+"_pu")
  cubeProxy=mc.polyCube(name=cleanName,ch=False)
  cubeShape=mc.listRelatives(cubeProxy[0],shapes=True,fullPath=True,noIntermediate=True)
  mc.connectAttr(_geom+".worldMatrix[0]",polyUniteNode+".inputMat[0]",force=True)
  mc.connectAttr(_geom+".worldMesh[0]",polyUniteNode+".inputPoly[0]",force=True)
  mc.connectAttr(polyUniteNode+".output",cubeShape[0]+".inMesh",force=True)
  mc.connectAttr(_geom+".visibility",cubeProxy[0]+".visibility",force=True)
  proxyCluster=mc.cluster(cubeProxy[0],name=cleanName+"_clst")
  self.clusters.append(proxyCluster[1])
  self.bakedGeom.append(cubeProxy[0])
  return cubeProxy[0]
 def getGeomList(self):
  return self.geoDict.keys()
 def getBakedGeomList(self):
  return self.bakedGeom
 def getClustersList(self):
  return self.clusters
 def resetGeomClusters(self):
  self.bakedGeom=[]
  self.clusters=[]
class Exporter():
 def __init__(self,_geom="",_path="",_sampleBy=0.25):
  self.geom=_geom
  self.path=_path
  if "\\" in self.path:
   self.path=self.path.replace("\\","/")
  self.sampleBy=_sampleBy
  self.startFrame=1
  self.endFrame=100
 def exportAlembicExo(self,_cleanName,_oneFile=False,_includeSurface=False):
  alembicExoLoaded=mc.pluginInfo("MayaExocortexAlembic",query=True,loaded=True)
  if alembicExoLoaded:
   startFrame="in="+str(self.startFrame)
   endFrame="out=" +str(self.endFrame)
   step="step=1"
   substep="substep="+str(1/self.sampleBy)
   mainOptions="purepointcache=1;globalspace=1;withouthierarchy=1;ogawa=1"
   if _includeSurface:
    mainOptions="purepointcache=0;globalspace=1;withouthierarchy=1;ogawa=1"
   variuosOptions="normals=1;uvs=1;facesets=0;dynamictopology=0;transformcache=0"
   if _oneFile:
    cleanName=_cleanName.replace(":","_")
    fileNameStr="filename="+self.path+"/"+cleanName+".abc"
    objectsStr="objects="+",".join(self.geom)
    finalStr=";".join([fileNameStr,objectsStr,startFrame,endFrame,step,substep,mainOptions,variuosOptions])
    mc.ExocortexAlembic_export(j=[finalStr])
   else:
    if type(self.geom)is list:
     for each in self.geom:
      cleanName=each.replace(":","_")
      fileNameStr="filename="+self.path+"/"+cleanName+".abc"
      objectsStr="objects="+each
      finalStr=";".join([fileNameStr,objectsStr,startFrame,endFrame,step,substep,mainOptions,variuosOptions])
      mc.ExocortexAlembic_export(j=[finalStr])
    else:
     cleanName=self.geom.replace(":","_")
     fileNameStr="filename="+self.path+"/"+cleanName+".abc"
     objectsStr="objects="+self.geom
     finalStr=";".join([fileNameStr,objectsStr,startFrame,endFrame,step,substep,mainOptions,variuosOptions])
     mc.ExocortexAlembic_export(j=[finalStr])
  else:
   mc.warning("Exocortex MayaExocortexAlembic.mll plugin is not loaded")
class SanityRigging():
 def __init__(self,_checkList,_checkNodes,_checkSelectedOnly):
  self.checkList=_checkList
  self.checkNodes=_checkNodes
  self.checkSelectedOnly=_checkSelectedOnly
  self.checkMethods=["Missing cgxgeoset","Duplicate names","NonZero Transforms","NonZero Pivot","Animation Curves","Empty Namespace","Existing Namespaces"]
 def check(self,_checkList,_checkNodes,_checkSelectedOnly):
  output=[]
  self.checkList=_checkList
  self.checkSelectedOnly=_checkSelectedOnly
  self.checkNodes=_checkNodes
  issuesNumber=0
  for chk in self.checkList:
   defName=chk.replace(" ","_").lower()
   if defName in dir(self):
    result=eval('self.'+defName+'()')
    output+=result[0]
    issuesNumber+=result[1]
   else:
    msgBox=QtGui.QMessageBox()
    msgBox.setWindowTitle("Warning!")
    msgBox.setText("No method for "+defName+" was found.")
    msgBox.exec_()
  return output,issuesNumber
 def formatList(self,_checkType,_numIssues,_affectedNodes,_level):
  checkList=[]
  for item in _affectedNodes.keys():
   checkList.append([_level,_checkType,_affectedNodes[item][0],_affectedNodes[item][1]])
  return checkList,_numIssues
 def duplicate_names(self):
  dupNames=[]
  dupNamesDict={}
  if self.checkSelectedOnly==True:
   dupItemsList=[]
   for obj in self.checkNodes:
    if mc.nodeType(obj)=="mesh":
     if '|' in obj:
      dupItemsList.append(obj)
    elif mc.nodeType(obj)=="transform":
     transShapes=mc.listRelatives(obj,shapes=True,fullPath=True)
     if transShapes!=None:
      for shape in transShapes:
       if mc.nodeType(shape)=="mesh":
        if '|' in obj:
         dupItemsList.append(obj)
        break
   dupNames=dupItemsList
   listID=1
   for each in dupNames:
    dupNamesDict[listID]=[each,"Duplicate name"]
    listID+=1
  else:
   allShapes=mc.ls(type="mesh",shortNames=True)
   dupShapesList=[]
   for obj in allShapes:
    if '|' in obj:
     dupShapesList.append(obj)
   allTrans=mc.ls(type="transform",shortNames=True)
   allMeshTrans=[]
   for trans in allTrans:
    transShapes=mc.listRelatives(trans,shapes=True,fullPath=True)
    if transShapes!=None:
     for shape in transShapes:
      if mc.nodeType(shape)=="mesh":
       allMeshTrans.append(trans)
       break
   dupTransList=[]
   for obj in allMeshTrans:
    if '|' in obj:
     dupTransList.append(obj)
   dupNames=dupTransList+dupShapesList
   listID=1
   for each in dupNames:
    dupNamesDict[listID]=[each,"Duplicate name"]
    listID+=1
  rList=self.formatList("Duplicate names",len(dupNamesDict),dupNamesDict,100)
  return rList
 def nonzero_transforms(self):
  nonZeroTrans={}
  if self.checkSelectedOnly==True:
   nonZeroTransList={}
   listID=1
   for obj in self.checkNodes:
    if mc.nodeType(obj)=="transform":
     transShapes=mc.listRelatives(obj,shapes=True,fullPath=True)
     if transShapes!=None:
      for shape in transShapes:
       if mc.nodeType(shape)=="mesh":
        translateObj=mc.xform(obj,query=True,translation=True,worldSpace=True)
        rotateObj=mc.xform(obj,query=True,rotation=True,worldSpace=True)
        scaleObj=mc.xform(obj,query=True,scale=True,worldSpace=True)
        if translateObj[0]!=0 or translateObj[1]!=0 or translateObj[2]!=0 or rotateObj[0]!=0 or rotateObj[1]!=0 or rotateObj[2]!=0 or scaleObj[0]!=1 or scaleObj[1]!=1 or scaleObj[2]!=1:
         nonZeroTransList[listID]=[obj,"Has non-zero transforms."]
         listID+=1
        break
   nonZeroTrans=nonZeroTransList
  else:
   allTrans=mc.ls(type="transform",shortNames=True)
   allMeshTrans=[]
   for trans in allTrans:
    transShapes=mc.listRelatives(trans,shapes=True,fullPath=True)
    if transShapes!=None:
     for shape in transShapes:
      if mc.nodeType(shape)=="mesh":
       allMeshTrans.append(trans)
       break
   nonZeroTransList={}
   listID=1
   for obj in allMeshTrans:
    translateObj=mc.xform(obj,query=True,translation=True,worldSpace=True)
    rotateObj=mc.xform(obj,query=True,rotation=True,worldSpace=True)
    scaleObj=mc.xform(obj,query=True,scale=True,worldSpace=True)
    if translateObj[0]!=0 or translateObj[1]!=0 or translateObj[2]!=0 or rotateObj[0]!=0 or rotateObj[1]!=0 or rotateObj[2]!=0 or scaleObj[0]!=1 or scaleObj[1]!=1 or scaleObj[2]!=1:
     nonZeroTransList[listID]=[obj,"Has non-zero transforms."]
     listID+=1
   nonZeroTrans=nonZeroTransList
  rList=self.formatList("NonZero Transforms",len(nonZeroTrans),nonZeroTrans,100)
  return rList
 def nonzero_pivot(self):
  nonZeroPivot={}
  if self.checkSelectedOnly==True:
   nonZeroPivotList={}
   listID=1
   for obj in self.checkNodes:
    if mc.nodeType(obj)=="transform":
     transShapes=mc.listRelatives(obj,shapes=True,fullPath=True)
     if transShapes!=None:
      for shape in transShapes:
       if mc.nodeType(shape)=="mesh":
        pivot=mc.xform(obj,pivots=True,query=True,ws=True)
        if pivot[0]!=0 or pivot[1]!=0 or pivot[2]!=0:
         nonZeroPivotList[listID]=[obj,"Has non-zero pivot."]
         listID+=1
        break
   nonZeroPivot=nonZeroPivotList
  else:
   allTrans=mc.ls(type="transform",shortNames=True)
   allMeshTrans=[]
   for trans in allTrans:
    transShapes=mc.listRelatives(trans,shapes=True,fullPath=True)
    if transShapes!=None:
     for shape in transShapes:
      if mc.nodeType(shape)=="mesh":
       allMeshTrans.append(trans)
       break
   nonZeroPivotList={}
   listID=1
   for obj in allMeshTrans:
    pivot=mc.xform(obj,pivots=True,query=True,ws=True)
    if pivot[0]!=0 or pivot[1]!=0 or pivot[2]!=0:
     nonZeroPivotList[listID]=[obj,"Has non-zero pivot."]
     listID+=1
   nonZeroPivot=nonZeroPivotList
  rList=self.formatList("NonZero Pivot",len(nonZeroPivot),nonZeroPivot,100)
  return rList
 def animation_curves(self):
  animationCurves=[]
  animationCurvesDict={}
  if self.checkSelectedOnly==True:
   animationCurvesList=[]
   for obj in self.checkNodes:
    if mc.nodeType(obj)=="transform":
     if mc.keyframe(obj,time=(':',),query=True,keyframeCount=True):
      animationCurvesList.append(obj)
   animationCurves=animationCurvesList
   listID=1
   for each in animationCurves:
    animationCurvesDict[listID]=[each,"Has animation curves."]
    listID+=1
  else:
   allTrans=mc.ls(type="transform",shortNames=True)
   allMeshTrans=[]
   for trans in allTrans:
    transShapes=mc.listRelatives(trans,shapes=True,fullPath=True)
    if transShapes!=None:
     for shape in transShapes:
      if mc.nodeType(shape)=="mesh":
       allMeshTrans.append(trans)
       break
   animationCurvesList=[]
   for obj in allMeshTrans:
    if mc.nodeType(obj)=="transform":
     if mc.keyframe(obj,time=(':',),query=True,keyframeCount=True):
      animationCurvesList.append(obj)
   animationCurves=animationCurvesList
   listID=1
   for each in animationCurves:
    animationCurvesDict[listID]=[each,"Has animation curves."]
    listID+=1
  rList=self.formatList("Animation Curves",len(animationCurvesDict),animationCurvesDict,100)
  return rList
 def existing_namespaces(self):
  existingNamespaces=[]
  initNamespacesList=mc.namespaceInfo(":",listOnlyNamespaces=True)
  defaultNamespaces=["UI","shared"]
  namespacesList=[]
  for each in initNamespacesList:
   if each not in defaultNamespaces:
    namespacesList.append(each)
  if namespacesList!=None or len(namespacesList)>=1:
   for each in namespacesList:
    thisChildren=self.existing_namespace_recursive(each)
    existingNamespaces.append(each)
    existingNamespaces+=thisChildren
  existingNamespacesDict={}
  listID=1
  for each in existingNamespaces:
   existingNamespacesDict[listID]=[each,"Existing Namespace."]
   listID+=1
  rList=self.formatList("Existing Namespaces",len(existingNamespacesDict),existingNamespacesDict,100)
  return rList
 def existing_namespace_recursive(self,_namespace):
  allChildNamespaces=mc.namespaceInfo(_namespace,listOnlyNamespaces=True)
  if allChildNamespaces!=None:
   finalChildren=[]
   for each in allChildNamespaces:
    thisChildren=self.existing_namespace_recursive(each)
    if len(thisChildren)>=1:
     finalChildren+=thisChildren
   return allChildNamespaces+finalChildren
  else:
   return[]
 def empty_namespace(self):
  emptyNamespaces=[]
  currentNamespace=mc.namespaceInfo(currentNamespace=True)
  if mc.namespaceInfo(currentNamespace,isRootNamespace=True):
   allChildNamespaces=mc.namespaceInfo(currentNamespace,listOnlyNamespaces=True)
   if allChildNamespaces!=None:
    for each in allChildNamespaces:
     thisEmpty=self.empty_namespace_recursive(each)
     if thisEmpty!=None:
      emptyNamespaces.append(thisEmpty)
  emptyNamespacesDict={}
  listID=1
  for each in emptyNamespaces:
   emptyNamespacesDict[listID]=[each,"EMPTY Namespace."]
   listID+=1
  rList=self.formatList("Empty Namespaces",len(emptyNamespacesDict),emptyNamespacesDict,70)
  return rList
 def empty_namespace_recursive(self,_namespace):
  allChildNamespaces=mc.namespaceInfo(_namespace,listOnlyNamespaces=True)
  if allChildNamespaces!=None:
   for each in allChildNamespaces:
    thisEmpty=self.empty_namespace_recursive(each)
    if thisEmpty!=None:
     return thisEmpty
  elif mc.namespaceInfo(_namespace,listNamespace=True)==None:
   if _namespace!="shared":
    return _namespace
  else:
   return None
 def missing_cgxgeoset(self):
  missinggeosets={}
  allSets=mc.ls(type="objectSet")
  cgxSets=[]
  for mayaset in allSets:
   if mayaset[-9:]=="cgxGeoSet":
    cgxSets.append(mayaset)
  listID=1
  if len(cgxSets)==0:
   missinggeosets[listID]=["cgxGeoSet","This rig hasn't been correctly tagged for the export tools."]
   listID+=1
  rList=self.formatList("Missing cgxGeoSet",len(missinggeosets),missinggeosets,100)
  return rList
class CheckList_DialogGUI(QtGui.QDialog):
 def __init__(self,_list,_totalIssues,parent):
  super(CheckList_DialogGUI,self).__init__(parent)
  self.parent=parent
  self.totalIssues=_totalIssues
  self.setupUi(self)
  self.setConnections()
  self.initModel(_list,_totalIssues)
  self.setAttribute(QtCore.Qt.WA_DeleteOnClose,True)
 def setupUi(self,checkList_DIALOG):
  checkList_DIALOG.setObjectName("checkList_DIALOG")
  checkList_DIALOG.resize(801,664)
  sizePolicy=QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum,QtGui.QSizePolicy.Fixed)
  sizePolicy.setHorizontalStretch(0)
  sizePolicy.setVerticalStretch(0)
  sizePolicy.setHeightForWidth(checkList_DIALOG.sizePolicy().hasHeightForWidth())
  checkList_DIALOG.setSizePolicy(sizePolicy)
  checkList_DIALOG.setMinimumSize(QtCore.QSize(801,664))
  checkList_DIALOG.setMaximumSize(QtCore.QSize(801,664))
  self.gridLayoutWidget=QtGui.QWidget(checkList_DIALOG)
  self.gridLayoutWidget.setGeometry(QtCore.QRect(10,10,781,651))
  self.gridLayoutWidget.setObjectName("gridLayoutWidget")
  self.gridLayout=QtGui.QGridLayout(self.gridLayoutWidget)
  self.gridLayout.setContentsMargins(0,0,0,0)
  self.gridLayout.setObjectName("gridLayout")
  self.totalIssues_LABEL=QtGui.QLabel(self.gridLayoutWidget)
  self.totalIssues_LABEL.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
  self.totalIssues_LABEL.setObjectName("totalIssues_LABEL")
  self.gridLayout.addWidget(self.totalIssues_LABEL,2,2,1,3)
  self.checkList_TABLEVIEW=QtGui.QTableView(self.gridLayoutWidget)
  self.checkList_TABLEVIEW.setObjectName("checkList_TABLEVIEW")
  self.gridLayout.addWidget(self.checkList_TABLEVIEW,0,1,1,4)
  self.yes_BTN=QtGui.QPushButton(self.gridLayoutWidget)
  sizePolicy=QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum,QtGui.QSizePolicy.Fixed)
  sizePolicy.setHorizontalStretch(0)
  sizePolicy.setVerticalStretch(0)
  sizePolicy.setHeightForWidth(self.yes_BTN.sizePolicy().hasHeightForWidth())
  self.yes_BTN.setSizePolicy(sizePolicy)
  self.yes_BTN.setObjectName("yes_BTN")
  self.gridLayout.addWidget(self.yes_BTN,3,3,1,1)
  self.no_BTN=QtGui.QPushButton(self.gridLayoutWidget)
  sizePolicy=QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum,QtGui.QSizePolicy.Fixed)
  sizePolicy.setHorizontalStretch(0)
  sizePolicy.setVerticalStretch(0)
  sizePolicy.setHeightForWidth(self.no_BTN.sizePolicy().hasHeightForWidth())
  self.no_BTN.setSizePolicy(sizePolicy)
  self.no_BTN.setLayoutDirection(QtCore.Qt.LeftToRight)
  self.no_BTN.setObjectName("no_BTN")
  self.gridLayout.addWidget(self.no_BTN,3,4,1,1)
  self.retranslateUi(checkList_DIALOG)
  QtCore.QMetaObject.connectSlotsByName(checkList_DIALOG)
 def retranslateUi(self,checkList_DIALOG):
  checkList_DIALOG.setWindowTitle(QtGui.QApplication.translate("checkList_DIALOG","Check List",None,QtGui.QApplication.UnicodeUTF8))
  self.totalIssues_LABEL.setText(QtGui.QApplication.translate("checkList_DIALOG","This file has a total of {} issues. Would you like to submit anyway?".format(self.totalIssues),None,QtGui.QApplication.UnicodeUTF8))
  self.yes_BTN.setText(QtGui.QApplication.translate("checkList_DIALOG","Yes",None,QtGui.QApplication.UnicodeUTF8))
  self.no_BTN.setText(QtGui.QApplication.translate("checkList_DIALOG","No",None,QtGui.QApplication.UnicodeUTF8))
 def setConnections(self):
  self.yes_BTN.clicked.connect(self.acceptPublish)
  self.no_BTN.clicked.connect(self.cancelPublish)
  self.checkList_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
  self.checkList_TABLEVIEW.customContextMenuRequested.connect(self.checklistOptions)
 def initModel(self,_list,_totalIssues):
  headers=["Level","Check","Node","Description"]
  self.checkModel=CheckListDataTableModel(_list,headers,self)
  proxyModel= QtGui.QSortFilterProxyModel(self)
  proxyModel.setSourceModel(self.checkModel)
  self.checkList_TABLEVIEW.setModel(proxyModel)
  self.checkList_TABLEVIEW.sortByColumn(0,QtCore.Qt.DescendingOrder)
  self.checkList_TABLEVIEW.setAlternatingRowColors(True)
  self.checkList_TABLEVIEW.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)
  self.checkList_TABLEVIEW.setColumnWidth(0,35)
  self.checkList_TABLEVIEW.setColumnWidth(1,130)
  self.checkList_TABLEVIEW.setColumnWidth(2,140)
  self.checkList_TABLEVIEW.setColumnWidth(3,300)
  self.checkList_TABLEVIEW.setHorizontalScrollMode(QtGui.QAbstractItemView.ScrollPerPixel)
  htalHeader=self.checkList_TABLEVIEW.horizontalHeader()
  htalHeader.setStretchLastSection(True)
 def acceptPublish(self):
  self.done(1)
 def cancelPublish(self):
  self.done(0)
 def checklistOptions(self,pos):
  menu=QtGui.QMenu(self.checkList_TABLEVIEW)
  selectNodeQ=menu.addAction("Select Node")
  menu.popup(self.checkList_TABLEVIEW.mapToGlobal(pos))
  selectNodeQ.triggered.connect(self.selectNodeOpt)
 def selectNodeOpt(self):
  dataList=self.checkModel.dataList
  selRowsProxy=self.checkList_TABLEVIEW.selectedIndexes()
  proxyModel=self.checkList_TABLEVIEW.model()
  selRows=[]
  for each in selRowsProxy:
   selRows.append(proxyModel.mapToSource(each))
  selRowsDirty=[]
  for item in selRows:
   thisRow=item.row()
   selRowsDirty.append(thisRow)
  selSet=set(selRowsDirty)
  selRows=list(selSet)
  finalList=[]
  for row in selRows:
   nodeName=dataList[row][2]
   finalList.append(nodeName)
  mc.select(finalList,replace=True)
class Synchronizer():
 def __init__(self,_parentUI=None):
  self.parentUI=_parentUI
  self.__statusDict={1:"In sync",2:"Out of sync",3:"Same date, different size",4:"Different date, same size",5:"Both paths missing",6:"Source file missing",7:"Target file missing",8:"Different kind of path (file-file, dir-dir)"}
  self.__log=[]
 def getSyncStatus(self,_srcPath,_trgPath):
  if os.path.exists(_srcPath)and os.path.exists(_trgPath):
   srcTime=time.ctime(os.path.getmtime(_srcPath))
   trgTime=time.ctime(os.path.getmtime(_trgPath))
   srcSize=os.path.getsize(_srcPath)
   trgSize=os.path.getsize(_trgPath)
   if os.path.isdir(_srcPath)and os.path.isdir(_trgPath):
    srcSize=self.getDirSize(_srcPath)
    trgSize=self.getDirSize(_trgPath)
   if srcTime==trgTime and srcSize==trgSize:
    return self.statusDict[1]
   elif srcTime!=trgTime and srcSize!=trgSize:
    return self.statusDict[2]
   elif srcTime==trgTime and srcSize!=trgSize:
    return self.statusDict[3]
   elif srcTime!=trgTime and srcSize==trgSize:
    return self.statusDict[4]
  elif not os.path.exists(_srcPath)and not os.path.exists(_trgPath):
   return self.statusDict[5]
  elif not os.path.exists(_srcPath):
   return self.statusDict[6]
  elif not os.path.exists(_trgPath):
   return self.statusDict[7]
  elif os.path.isdir(_srcPath)or os.path.isdir(_trgPath):
   return self.statusDict[8]
  else:
   return "Failed to find status."
 def getDirSize(self,_path):
  if os.path.isdir(_path):
   total_size=0
   for dirpath,dirnames,filenames in os.walk(_path):
    for f in filenames:
     fp=os.path.join(dirpath,f)
     total_size+=os.path.getsize(fp)
   return total_size
  else:
   return None
 def getSequenceFiles(self,_parentFolder,_fileWithExt):
  fileName,fileExt=_fileWithExt.rsplit(".",1)
  digitsNumber=0
  for each in fileName[::-1]:
   if each.isdigit():
    digitsNumber+=1
   else:
    break
  namePattern=fileName[:-digitsNumber]
  hashStr="#"*digitsNumber
  self.log.append("Name pattern for given sequence is: {}{}.{}".format(namePattern,hashStr,fileExt))
  filesAndDirs=os.listdir(_parentFolder)
  preFileList=[]
  for each in filesAndDirs:
   if os.path.isfile(os.path.join(_parentFolder,each)):
    preFileList.append(each)
  fileList=[]
  for each in preFileList:
   thisFileExt=each.rsplit(".",1)[1]
   if each[:len(namePattern)]==namePattern and thisFileExt==fileExt:
    fileList.append(_parentFolder+"/"+each)
  return fileList
 def processPaths(self,_srcPath,_trgPath,**kwargs):
  if "\\" in _srcPath:
   _srcPath=_srcPath.replace("\\","/")
  if "\\" in _trgPath:
   _trgPath=_trgPath.replace("\\","/")
  passedDirs=False
  if os.path.isdir(_srcPath):
   passedDirs=True
  if _srcPath==_trgPath:
   self.log.append("Source and target are the same, it will be skipped: {}".format(_srcPath))
   return False
  elif not os.path.exists(_srcPath):
   self.log.append("Source file doesn't exist, it will be skipped: {}".format(_srcPath))
   return False
  elif passedDirs:
   result=self.__processDirs(_srcPath,_trgPath)
   self.log.append("Finished processing directories: {} and {}".format(_srcPath,_trgPath))
   return result
  else:
   result=self.__processFiles(_srcPath,_trgPath,kwargs)
   self.log.append("Finished processing files: {} and {}".format(_srcPath,_trgPath))
   return result
 def __processDirs(self,_srcPath,_trgPath):
  if not os.path.exists(_trgPath):
   try:
    shutil.copytree(_srcPath,_trgPath)
    self.log.append("Copied {} to {}".format(_srcPath,_trgPath))
   except IOError:
    self.log.append("The specified source doesn't exist: {}".format(_srcPath))
   return True
  else:
   self.log.append("The specified target directory must not exist: {}".format(_trgPath))
   return False
 def __processFiles(self,_srcPath,_trgPath,kwargs):
  pathParts=_trgPath.rsplit("/",1)
  finalFolder=pathParts[0]
  if "isTexture" in kwargs.keys():
   if kwargs["isTexture"]:
    if not os.path.exists(finalFolder):
     try:
      os.makedirs(finalFolder)
      self.log.append("Directory structure created: {}".format(finalFolder))
     except WindowsError:
      self.log.append("Directory structure couldn't be created: {}".format(finalFolder))
      return False
    else:
     self.log.append("Directory structure already existed: {}".format(finalFolder))
    try:
     shutil.copy2(_srcPath,finalFolder)
     self.log.append("Copied {} to {}".format(_srcPath,finalFolder))
    except IOError:
     self.log.append("The specified source doesn't exist: {}".format(_srcPath))
     return False
    txPath=_srcPath.rsplit(".",1)[0]+".tx"
    if os.path.exists(txPath):
     try:
      shutil.copy2(txPath,finalFolder)
      self.log.append("Copied {} to {}".format(txPath,finalFolder))
     except IOError:
      self.log.append("The specified source TX file doesn't exist: {}".format(txPath))
    else:
     self.log.append("The specified source TX file doesn't exist: {}".format(txPath))
    return True
   else:
    return False
  elif "isSequence" in kwargs.keys():
   if kwargs["isSequence"]:
    srcFolderPath=_srcPath.rsplit("/",1)[0]
    trgFolderPath=_trgPath.rsplit("/",1)[0]
    fileWithExt=pathParts[1]
    fileList=self.getSequenceFiles(srcFolderPath,fileWithExt)
    if not os.path.exists(trgFolderPath):
     try:
      os.makedirs(trgFolderPath)
      self.log.append("Directory structure created: {}".format(trgFolderPath))
     except WindowsError:
      self.log.append("Directory structure couldn't be created: {}".format(_trgPath))
      return False
    else:
     self.log.append("Directory structure already existed: {}".format(finalFolder))
    for each in fileList:
     try:
      shutil.copy2(each,trgFolderPath)
      self.log.append("Copied {} to {}".format(each,trgFolderPath))
     except IOError:
      self.log.append("The specified source doesn't exist: {}".format(each))
     txPath=each.rsplit(".",1)[0]+".tx"
     if os.path.exists(txPath):
      try:
       shutil.copy2(txPath,trgFolderPath)
       self.log.append("Copied {} to {}".format(txPath,trgFolderPath))
      except IOError:
       self.log.append("The specified source TX file doesn't exist: {}".format(txPath))
     else:
      self.log.append("The specified source TX file doesn't exist: {}".format(txPath))
    return True
   else:
    return False
  else:
   if not os.path.exists(finalFolder):
    try:
     os.makedirs(finalFolder)
     self.log.append("Directory structure created: {}".format(finalFolder))
    except WindowsError:
     self.log.append("Directory structure couldn't be created: {}".format(finalFolder))
     return False
   else:
    self.log.append("Directory structure already existed: {}".format(finalFolder))
   try:
    shutil.copy2(_srcPath,finalFolder)
    self.log.append("Copied {} to {}".format(_srcPath,finalFolder))
   except IOError:
    self.log.append("The specified source doesn't exist: {}".format(_srcPath))
   return True
 def __repr__(self):
  output=""
  for each in self.log:
   output+="--> "+each +"\n"
  return output
 @property
 def statusDict(self):
  return self.__statusDict
 @statusDict.setter
 def statusDict(self,d):
  self.__statusDict=d
 @property
 def log(self):
  return self.__log
class Maya_Dependencies():
 def __init__(self):
  self.__allDependencies=[]
  self.__textureFiles=[]
  self.__referenceFiles=[]
  self.__imagePlaneFiles=[]
 def fileAttrChange(self,_filePath,_node,_type):
  if _type=="Image plane":
   mc.setAttr(_node+".imageName",_filePath,type="string")
  elif _type=="Reference":
   mc.file(_filePath,loadReference=_node,options="v=0;",prompt=False)
  elif _type=="Texture":
   sequenceStatus=mc.getAttr(_node+".useFrameExtension")
   tileStatus=mc.getAttr(_node+".uvTilingMode")
   if sequenceStatus==True:
    mc.setAttr(_node+".useFrameExtension",False)
    mc.setAttr(_node+".fileTextureName",_filePath,type="string")
    mc.setAttr(_node+".useFrameExtension",True)
   elif tileStatus!=0:
    mc.setAttr(_node+".uvTilingMode",0)
    mc.setAttr(_node+".fileTextureName",_filePath,type="string")
    mc.setAttr(_node+".uvTilingMode",tileStatus)
   else:
    mc.setAttr(_node+".fileTextureName",_filePath,type="string")
  else:
   mc.warning("Not recognized node type.")
 @property
 def allDependencies(self):
  for thisProperty,value in vars(self).iteritems():
   propertyName=thisProperty.split("_")[-1]
   if not propertyName=="allDependencies":
    propertyDeps=eval("self."+propertyName)
    self.__allDependencies+=propertyDeps
  return self.__allDependencies
 @property
 def textureFiles(self):
  filesList=mc.ls(type="file",long=True)
  if filesList!=None:
   for node in filesList:
    textureDict={}
    filePath=mc.getAttr(node+".fileTextureName")
    if filePath==None:
     filePath=""
    if os.path.exists(filePath):
     if "\\" in filePath:
      filePath=filePath.replace("\\","/")
     textureDict["filePath"]=filePath
     textureDict["node"]=node
     self.__textureFiles.append(textureDict)
    else:
     if filePath!="":
      mc.warning(node+" contains a non-existing file.")
  return self.__textureFiles
 @property
 def referenceFiles(self):
  filesList=mc.file(mc.file(query=True,sn=True),reference=True,query=True)
  if filesList!=None:
   if len(filesList)>=1:
    for filePath in filesList:
     referenceDict={}
     node=mc.referenceQuery(filePath,referenceNode=True)
     if filePath==None:
      filePath=""
     existCheck=filePath
     if filePath[-1]=="}":
      existCheck=filePath[:-3]
     if os.path.exists(existCheck):
      if "\\" in filePath:
       filePath=filePath.replace("\\","/")
      referenceDict["filePath"]=existCheck
      referenceDict["node"]=node
      self.__referenceFiles.append(referenceDict)
     else:
      if filePath!="":
       mc.warning(node+" contains a non-existing file.")
  return self.__referenceFiles
 @property
 def imagePlaneFiles(self):
  filesList=mc.ls(type="imagePlane",long=True)
  if filesList!=None:
   for node in filesList:
    textureDict={}
    filePath=mc.getAttr(node+".imageName")
    if filePath==None:
     filePath=""
    if os.path.exists(filePath):
     if "\\" in filePath:
      filePath=filePath.replace("\\","/")
     textureDict["filePath"]=filePath
     textureDict["node"]=node
     self.__imagePlaneFiles.append(textureDict)
    else:
     if filePath!="":
      mc.warning(node+" contains a non-existing file.")
  return self.__imagePlaneFiles
class ObjectsListModel(QtCore.QAbstractListModel):
 def __init__(self,dataList=[],parent=None):
  super(ObjectsListModel,self).__init__(parent)
  self.__dataList=dataList
  if dataList==None:
   self.__dataList=[]
 def rowCount(self,parent=QtCore.QModelIndex()):
  return len(self.dataList)
 def flags(self,index):
  if not index.isValid():
   return QtCore.Qt.NoItemFlags
  else:
   return QtCore.Qt.ItemIsEnabled|QtCore.Qt.ItemIsSelectable
 def data(self,index,role=QtCore.Qt.DisplayRole):
  row=index.row()
  if not index.isValid():
   return None
  elif row>=len(self.dataList)or row<0:
   return None
  elif role in[QtCore.Qt.DisplayRole,QtCore.Qt.ToolTipRole,QtCore.Qt.EditRole]:
   return self.dataList[row]
  else:
   return None
 def setData(self,index,value,role=QtCore.Qt.EditRole):
  row=index.row()
  if not index.isValid():
   return False
  elif row>=len(self.dataList)or row<0:
   return False
  if role==QtCore.Qt.EditRole:
   if value!=None:
    self.dataList[row]=value
    self.dataChanged.emit(index,index)
    return True
   else:
    return False
  else:
   return False
 def insertRows(self,position,rows,parent=QtCore.QModelIndex()):
  if len(rows)<=0:
   return False
  elif position>len(self.dataList):
   return False
  elif position<0:
   return False
  else:
   self.beginInsertRows(parent,position,position+len(rows)-1)
   for item in rows[::-1]:
    self.dataList.insert(position,item)
   self.endInsertRows()
   return True
 def removeRows(self,indexList,count,parent=QtCore.QModelIndex()):
  QMIndexList=all(isinstance(x,QtCore.QModelIndex)for x in indexList)
  if QMIndexList:
   intIndexList=sorted([index.row()for index in indexList])
   indexList=intIndexList
  for i in indexList:
   if i>len(self.dataList)-1:
    return False
   elif i<0:
    return False
  if len(indexList)!=count:
   return False
  else:
   position=indexList[0]
   self.beginRemoveRows(parent,position,position+(count-1))
   for i in sorted(indexList,reverse=True):
    del(self.dataList[i])
   self.endRemoveRows()
   return True
 def index(self,row,column=0,parent=QtCore.QModelIndex()):
  if row<len(self.dataList)and row>-1:
   return self.createIndex(row,0,self.dataList[row])
  else:
   return QtCore.QModelIndex()
 @property
 def dataList(self):
  return self.__dataList
class DataTableModel(QtCore.QAbstractTableModel):
 def __init__(self,dataList=[[]],headers=[],parent=None):
  super(DataTableModel,self).__init__(parent)
  self.__dataList=dataList
  if dataList==None:
   self.__dataList=[]
  self.__headers=headers
  if headers==None:
   self.__headers=[]
 def rowCount(self,parent=QtCore.QModelIndex()):
  if len(self.dataList)==1:
   if self.dataList[0]==[]:
    return 0
   else:
    return len(self.dataList)
  else:
   return len(self.dataList)
 def columnCount(self,parent=QtCore.QModelIndex()):
  return len(self.headers)
 def flags(self,index):
  if not index.isValid():
   return QtCore.Qt.NoItemFlags
  else:
   return QtCore.Qt.ItemIsEnabled|QtCore.Qt.ItemIsSelectable
 def data(self,index,role):
  row=index.row()
  column=index.column()
  if not index.isValid():
   return None
  elif row>=len(self.dataList)or row<0:
   return None
  elif column>=len(self.headers)or column<0:
   return None
  if role in[QtCore.Qt.DisplayRole,QtCore.Qt.ToolTipRole,QtCore.Qt.EditRole]:
   value=self.dataList[row][column]
   return value
  else:
   return None
 def headerData(self,section,orientation,role):
  if orientation==QtCore.Qt.Horizontal:
   if role in[QtCore.Qt.DisplayRole,QtCore.Qt.ToolTipRole,QtCore.Qt.EditRole]:
    if section>len(self.headers)-1:
     return None
    else:
     value=self.headers[section]
     return value
   else:
    return None
  else:
   return None
 def setData(self,index,value,role=QtCore.Qt.EditRole):
  row=index.row()
  column=index.column()
  if not index.isValid():
   return False
  elif row>=len(self.dataList)or row<0:
   return False
  elif column>=len(self.headers)or column<0:
   return False
  if role==QtCore.Qt.EditRole:
   if value!=None:
    self.dataList[row][column]=value
    self.dataChanged.emit(index,index)
    return True
   else:
    return False
  else:
   return False
 def insertRows(self,position,count,data,parent=QtCore.QModelIndex()):
  if position <0:
   return False
  else:
   manyRows=all(isinstance(x,list)for x in data)
   if manyRows:
    rowsLen=all(len(x)==self.columnCount()for x in data)
    if rowsLen:
     self.beginInsertRows(parent,position,position+(count-1))
     for row in data:
      self.dataList.insert(position,row)
     self.endInsertRows()
     return True
    else:
     return False
   else:
    if len(data)==self.columnCount():
     self.beginInsertRows(parent,position,position+(count-1))
     self.dataList.insert(position,data)
     self.endInsertRows()
     return True
    else:
     return False
 def insertColumns(self,position,columns,parent=QtCore.QModelIndex()):
  if position <0:
   return False
  else:
   if type(columns)is list:
    self.beginInsertColumns(parent,position,position+len(columns)-1)
    for i in columns[::-1]:
     self.headers.insert(position,i)
     for row in self.dataList:
      row.insert(position,"Type here")
    self.endInsertColumns()
    return True
   else:
    return False
 def removeRows(self,indexList,count,parent=QtCore.QModelIndex()):
  QMIndexList=all(isinstance(x,QtCore.QModelIndex)for x in indexList)
  if QMIndexList:
   intIndexList=sorted([index.row()for index in indexList])
   indexList=intIndexList
  for i in indexList:
   if i>len(self.dataList)-1:
    return False
   elif i<0:
    return False
  if len(indexList)!=count:
   return False
  else:
   position=indexList[0]
   self.beginRemoveRows(parent,position,position+(count-1))
   for i in sorted(indexList,reverse=True):
    del(self.dataList[i])
   self.endRemoveRows()
   return True
 def removeColumns(self,indexList,count,parent=QtCore.QModelIndex()):
  QMIndexList=all(isinstance(x,QtCore.QModelIndex)for x in indexList)
  if QMIndexList:
   intIndexList=sorted([index.column()for index in indexList])
   indexList=intIndexList
  for i in indexList:
   if i>len(self.headers)-1:
    return False
   elif i<0:
    return False
  if len(indexList)!=count:
   return False
  else:
   position=indexList[0]
   self.beginRemoveColumns(parent,position,position+(count-1))
   for i in sorted(indexList,reverse=True):
    del(self.headers[i])
    for row in self.dataList:
     del(row[i])
   self.endRemoveColumns()
   return True
 def index(self,row,column,parent=QtCore.QModelIndex()):
  if row>-1 and column>-1:
   if row<len(self.dataList)and column<len(self.headers):
    return self.createIndex(row,column,self.dataList[row][column])
   else:
    return QtCore.QModelIndex()
  else:
   return QtCore.QModelIndex()
 @property
 def dataList(self):
  return self.__dataList
 @property
 def headers(self):
  return self.__headers
class CheckListDataTableModel(DataTableModel):
 def __init__(self,dataList,headers,parent):
  super(CheckListDataTableModel,self).__init__(dataList,headers,parent)
  self.parent=parent
 def flags(self,index):
  return QtCore.Qt.ItemIsEnabled|QtCore.Qt.ItemIsSelectable
 def data(self,index,role):
  row=index.row()
  column=index.column()
  if role==QtCore.Qt.DisplayRole:
   return self.dataList[row][column]
  if role==QtCore.Qt.BackgroundRole:
   if index.column()==0:
    if self.dataList[row][column]>=85:
     return QtGui.QColor(255,0,0,255)
    elif self.dataList[row][column]<85 and self.dataList[row][column]>=50:
     return QtGui.QColor(255,127,0,255)
    elif self.dataList[row][column]<50 and self.dataList[row][column]>=1:
     return QtGui.QColor(0,127,255,255)
  if role in[QtCore.Qt.ToolTipRole,QtCore.Qt.WhatsThisRole]:
   if self.dataList[row][0]>=85:
    return "FIX IT OR DIE!"
   elif self.dataList[row][0]<85 and self.dataList[row][0]>=50:
    return "FIX IT, DON'T BE LAZY!"
   elif self.dataList[row][0]<50 and self.dataList[row][0]>=1:
    return "FIX IT, YOU FREAKING PERFECTIONIST!"
class PublishedDataTableModel(DataTableModel):
 def __init__(self,dataList,headers,parent):
  super(PublishedDataTableModel,self).__init__(dataList,headers,parent)
 def data(self,index,role):
  row=index.row()
  column=index.column()
  if role==QtCore.Qt.DisplayRole:
   value=self.dataList[row][column]
   return value
  if role==QtCore.Qt.ToolTipRole:
   value=self.dataList[row][column]
   return value
  if role==QtCore.Qt.EditRole:
   return self.dataList[row][column]
  if role==QtCore.Qt.DecorationRole:
   value=self.dataList[row][column]
   if self.headers[column]=="Status":
    if value=="Already published":
     return QtGui.QIcon(":/RigPublisher_inSync.png")
    elif value=="Published but different":
     return QtGui.QIcon(":/RigPublisher_outOfSync.png")
    else:
     return QtGui.QIcon(":/RigPublisher_rest.png")
class Help_DialogGUI(helpform,helpbase):
 def __init__(self,parent):
  super(Help_DialogGUI,self).__init__(parent)
  self.setupUi(self)
  self.setAttribute(QtCore.Qt.WA_DeleteOnClose,True)
class About_DialogGUI(aboutform,aboutbase):
 def __init__(self,parent):
  super(About_DialogGUI,self).__init__(parent)
  self.setupUi(self)
  self.setAttribute(QtCore.Qt.WA_DeleteOnClose,True)
class Config_DialogGUI(configform,configbase):
 def __init__(self,parent):
  super(Config_DialogGUI,self).__init__(parent)
  self.setupUi(self)
  self.setWindowFlags(self.windowFlags()|QtCore.Qt.CustomizeWindowHint)
  self.setWindowFlags(self.windowFlags()&~QtCore.Qt.WindowCloseButtonHint)
  self.setConnections()
  self.parent=parent
  self.jsonConfig=ConfigFileJSON(configfile)
  self.initUI()
  self.setAttribute(QtCore.Qt.WA_DeleteOnClose,True)
 def setConnections(self):
  self.cancel_BTN.clicked.connect(self.cancelAll)
  self.apply_BTN.clicked.connect(self.applyConfig)
  self.sharedFolder_BTN.clicked.connect(lambda:self.browsePath('sharedFolder'))
  self.riggingFolder_BTN.clicked.connect(lambda:self.browsePath('riggingFolder'))
 def initUI(self):
  if self.jsonConfig.jsonObj!=None:
   self.sharedFolder_LINEEDIT.setText(self.jsonConfig.jsonObj['sharedFolder'])
   self.riggingFolder_LINEEDIT.setText(self.jsonConfig.jsonObj['riggingFolder'])
 def browsePath(self,_folderType):
  directory=self.jsonConfig.getFolder(_folderType)
  browseDialog=QtGui.QFileDialog(self)
  fileMode=browseDialog.FileMode.Directory
  browseDialog.setFileMode(fileMode)
  if os.path.exists(directory):
   browseDialog.setDirectory(directory)
  dialogReturn=browseDialog.exec_()
  if dialogReturn==1:
   selectedFiles=browseDialog.selectedFiles()
   if _folderType=='sharedFolder':
    self.sharedFolder_LINEEDIT.setText(selectedFiles[0])
   elif _folderType=='riggingFolder':
    self.riggingFolder_LINEEDIT.setText(selectedFiles[0])
 def applyConfig(self):
  newSharedFolder=self.sharedFolder_LINEEDIT.text()
  newRiggingFolder=self.riggingFolder_LINEEDIT.text()
  if os.path.exists(newSharedFolder)and os.path.exists(newRiggingFolder):
   if os.path.exists(newSharedFolder+'/.sync')and os.path.exists(newRiggingFolder+'/.sync'):
    newJsonObj={}
    newJsonObj['sharedFolder']=newSharedFolder
    newJsonObj['riggingFolder']=newRiggingFolder
    self.jsonConfig.writeJson(newJsonObj)
    self.done(1)
   else:
    msgBox=QtGui.QMessageBox(self)
    msgBox.setWindowTitle("Warning!")
    msgBox.setText("These are not Resilio Sync folders.")
    msgBox.exec_()
  else:
   msgBox=QtGui.QMessageBox(self)
   msgBox.setWindowTitle("Warning!")
   msgBox.setText("Folders must already exist in your file system.")
   msgBox.exec_()
 def cancelAll(self):
  self.done(0)
class ConfigFileJSON():
 def __init__(self,_fileName='config.json'):
  self.fileName=_fileName
  self.jsonObj=self.getJsonObj()
 def getFolder(self,_folderType):
  if _folderType=='sharedFolder':
   folder=self.jsonObj['sharedFolder']
   return folder
  elif _folderType=='riggingFolder':
   folder=self.jsonObj['riggingFolder']
   return folder
  else:
   return None
 def getJsonObj(self):
  if os.path.exists(self.fileName):
   fIn=open(self.fileName,'r')
   jsonObj=json.load(fIn)
   fIn.close()
   return jsonObj
  else:
   return None
 def writeJson(self,_data):
  fOut=open(self.fileName,'w')
  json.dump(_data,fOut,indent=4)
  fOut.close()
def main():
 if __DEVMODE:
  x=open('M:/CGXtools/DEV/Maya/plugin_portable/oneLiners/pipeline/RigPublisher_dev.py','r')
  exec x.read()
  x.close()
if __name__=='__main__' or 'eclipsePython' in __name__:
 main()
# Created by pyminifier (https://github.com/liftoff/pyminifier)
