# -*- coding: utf-8 -*-
'''
Cache importer tool.

Created on Feb 06, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os, re
import maya.cmds as mc
import maya.mel as mel
from cgx.gui.Qt import QtWidgets, QtCore
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents

import cgx.gui.DataViewModels as dvm


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/pipeline/CacheImporter'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.setConnections()
			self.initComponents()
			self.initUI()
			
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
        Connect signals.
        '''
		#SIGNALS
		self.cancel_BTN.clicked.connect(self.baseMod.quitApp)
		self.import_BTN.clicked.connect(self.preImportCaches)
		self.customPathComp.Accepted.connect(self.populateList)
		self.projSeqShotMod.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.populateList)
		self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.currentIndexChanged.connect(self.populateList)
		self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentIndexChanged.connect(self.populateList)
		self.assetsMod.Assets_Component.assetName_COMBOBOX.currentIndexChanged.connect(self.populateList)
		self.assetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentIndexChanged.connect(self.populateList)
		#REFRESH
		self.projSeqShotMod.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.assetsMod.initComponents)
		self.cacheComp.fileType_BTNGRP.buttonClicked.connect(self.refreshFileType)
		self.cacheComp.fileType_BTNGRP.buttonClicked.connect(self.refreshFileType)
		self.mergeReplace_BTNGRP.buttonClicked.connect(self.refreshMergeReplace)
		self.customPathComp.customPath_CHKBOX.stateChanged.connect(self.refreshCustomPath)
		self.import360_CHKBOX.stateChanged.connect(self.refreshImport360)
		self.customSuffix_CHKBOX.stateChanged.connect(self.refreshCustomSuffix)
		self.addPrefixSuffix_CHKBOX.stateChanged.connect(self.refreshRenameSelectedGeo)
		#STATUS
		self.cacheComp.alembicExo_RBTN.setChecked(True)
		self.merge_RBTN.setChecked(True)
		self.import360_CHKBOX.setChecked(True)
		self.projSeqShotMod.Shot_Component.shot_COMBOBOX.setEnabled(False)
		self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.setEnabled(False)
		self.addPrefixSuffix_CHKBOX.setChecked(True)
		self.customSuffix_LINEEDIT.setEnabled(False)
		
		#CONTEXT MENUS
		self.charsProps_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.charsProps_LISTVIEW.customContextMenuRequested.connect(self.charsPropsOptions)
		
	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Cache Importer"
		self.__toolVersion = "2.0"
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.baseMod.MainMenu_Component.edit_config.setVisible(False)
		self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo, tRange=self.appTools.tRange)
		self.cacheComp = pipeComponents.CacheFileTypes_Component(self,self.centralWidget())
		self.cacheComp.pos = [0,0]
		self.projSeqShotMod = self.Pipeline_GUIModulesFactory.createModule('ProjSeqShot')
		self.projSeqShotMod.pos = [0,95]
		self.assetsMod = self.Pipeline_GUIModulesFactory.createModule('Assets')
		self.assetsMod.pos = [0,189]
		self.assetsMod.FileTypes_Component.container.setVisible(False)
		self.customPathComp = pipeComponents.CustomPath_Component(self,self.centralWidget(), self.sInfo)
		self.customPathComp.pos = [0,320]
		self.customPathComp.fileMode = self.customPathComp.browseDialog.FileMode.Directory
		self.fpsComp = pipeComponents.FPS_Component(self,self.centralWidget(), self.pInfo)
		self.fpsComp.pos = [262,155]
		self.progComp = pipeComponents.Progress_Component(self,self.centralWidget())
		self.progComp.pos = [340,471]
		self.progComp.reset()
	
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
		self.appTools.app_initUI.setCurrentProject(self.projSeqShotMod.Project_Component)
		self.appTools.app_initUI.setCurrentSequence(self.projSeqShotMod.Sequence_Component)
		self.appTools.app_initUI.setCurrentShot(self.projSeqShotMod.Shot_Component)
		self.appTools.app_initUI.setCurrentAssetType(self.assetsMod.AssetTypes_Component)
		self.appTools.app_initUI.setCurrentAsset(self.assetsMod.Assets_Component)
		self.appTools.app_initUI.setCurrentSubAsset(self.assetsMod.SubAssets_Component)
		self.fpsComp.initComponent(self.pInfo.maya_getFPS())
		self.appTools.app_initUI.checkFPS(self.fpsComp)
		self.appTools.tRange.setFramesToTimeSlider()
		
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		self.populateList()
	
	
	def checkPlugins(self):
		'''
        Check if plugins are loaded.
        :return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
        :rtype: dict
        '''
		checkDict = self.appTools.checkPlugins(self)
		return checkDict
	
	
	def preImportCaches(self):
		check = self.checkPlugins()
		if len(check.keys()) <= 0:
			self.importCaches()
		else:
			if self.cacheComp.CacheFileTypes_Component.alembic_RBTN.isChecked():
				if "Alembic" in check.keys():
					self.baseMod.statusBar_update(check["Alembic"], alert=True)
				else:
					self.importCaches()
			elif self.cacheComp.CacheFileTypes_Component.alembicExo_RBTN.isChecked():
				if "Exocortex" in check.keys():
					self.baseMod.statusBar_update(check["Exocortex"], alert=True)
				else:
					self.importCaches()
	
	
	def importCaches(self):
		"""Import caches"""
		
		selected = self.charsProps_LISTVIEW.selectedIndexes()
		if len(selected) <= 0:
			self.baseMod.statusBar_update("Select at least one item in Characters/Props list to import.", alert=True)
		else:
			#Set progress bar
			self.progComp.reset()
			self.progComp.minimum = 0
			
			#Get folder
			targFolder= self.buildProjSeqShotPath()
			if self.customPathComp.customPath_CHKBOX.isChecked():
				targFolder = str(self.customPathComp.customPath_LINEEDIT.text()) + "/"
			elif self.import360_CHKBOX.isChecked():
				targFolder = self.buildCachePath()
			
			#Selected items in list
			selectedNames = []
			for each in selected:
				selectedNames.append( str( each.data() ) )
	
			#More progress bar
			multProgress = 1
			if self.importOBJs_RBTN.isChecked():
				multProgress = 2
			self.progComp.maximum = len(selectedNames) * multProgress
			
			#POINT CACHE
			if self.cacheComp.pointCache_RBTN.isChecked():
				self.importPointCaches(targFolder, selectedNames)
			#EXOCORTEX ALEMBIC
			elif self.cacheComp.alembicExo_RBTN.isChecked():
				self.importAlembicExo(targFolder, selectedNames)
			#ALEMBIC
			else:
				self.importAlembic(targFolder, selectedNames)
	
			#Reset progress bar
			self.progComp.reset()
			
			#Done Alert!!
			self.baseMod.statusBar_update("Done importing caches.", success=True)
	
	
	def importPointCaches(self, targFolder, selectedNames):
		if self.importOBJs_RBTN.isChecked():
			objsFolder = targFolder + "/OBJs/"
			if self.customPath_CHKBOX.isChecked():
				objsFolder = str(self.customPathComp.customPath_LINEEDIT.text()) + "/OBJs/"
			listaObjs = mc.getFileList(folder=(objsFolder), filespec="*.obj")
			listaObjsClean = []
			for obj in listaObjs:
				for sel in selectedNames:
					if obj[:-4] == sel:
						listaObjsClean.append(obj)
						break
				if len(listaObjsClean) >= 1:
					for each in listaObjsClean:
						varFile = mc.file((objsFolder + each), i= True , returnNewNodes= True)
						mc.rename(varFile[0], each[:-4])
						#Move progress bar
						self.progComp.value = self.progComp.value + 1
					for each in selectedNames:
						transformName = each
						objShape = mc.listRelatives(transformName , shapes = True, noIntermediate = True)
						switch = mel.eval('createHistorySwitch("' +  objShape[0] + '",false);')
						cacheNode = mc.cacheFile(f=(targFolder + each + ".xml"), ia='%s.inp[0]' % switch , attachFile=True)
						mc.connectAttr((cacheNode + ".inRange") , (switch + ".playFromCache") , force=True)
						#Move progress bar
						self.progComp.value = self.progComp.value + 1
				else:
					self.baseMod.statusBar_update("No OBJs were found in {}".format(objsFolder), warning=True)
		else:
			allSelShapes = mc.ls(sl=True, dag=True, long= True, ni= True, geometry=True)
			allSel = []
			if self.addPrefixSuffix_CHKBOX.isChecked() and self.import360_CHKBOX.isChecked():
				allSel = self.renameGeo(allSelShapes)
			else:
				for each in allSelShapes:
					transform = mc.listRelatives(each, parent=True, fullPath=True)[0]
					allSel.append(transform)
			for each in allSel:
				transformNode = each
				if '|' in each:
					transformNode = each.rsplit('|', 1)[1]
				shape = mc.listRelatives(each, shapes=True, noIntermediate=True, fullPath= True)[0]
				switch = mel.eval('createHistorySwitch("' + shape + '",false);')
				cacheNode = mc.cacheFile(f=(targFolder + transformNode + ".xml"), ia='%s.inp[0]' % switch , attachFile=True)
				mc.connectAttr((cacheNode + ".inRange") , (switch + ".playFromCache") , force=True)
				#Move progress bar
				self.progComp.value = self.progComp.value + 1
	
	
	def importAlembicExo(self, targFolder, selectedNames):
		#REPLACE OPERATION
		if self.replace_RBTN.isChecked():
			if self.replaceSelectedGeo_CHKBOX.isChecked():
				allSelShapes = mc.ls(sl=True, dag=True, long= True, ni= True, geometry=True)
				allSel = []
				for shape in allSelShapes:
					thisTransform = mc.listRelatives(shape, parent= True, fullPath= True)
					allSel.append(thisTransform[0])
				if len(allSel) < 1:
					self.baseMod.statusBar_update("Please select at least one object in your scene to be replaced.", alert=True)
				elif len(selectedNames) > 1 :
					self.baseMod.statusBar_update("Please select one Alembic file only from the list if using the Replace Selected Geometry option.", alert=True)
				else:
					for each in selectedNames:
						reObj = re.search("\d", each[::-1])
						digitIndex = None
						if reObj:
							digitIndex = reObj.start()
							digitIndex = -digitIndex
							if digitIndex == 0:
								digitIndex = None
						i = 0
						for sel in allSel:
							baseName = each[:digitIndex] + str(i+1)
							ctrlName = baseName + "_CTRL"
							parentControl = self.createParentControl(ctrlName, sel)
							self.createAlembicExo(targFolder, each, baseName, parentControl)
							if self.createGPU_CHKBOX.isChecked():
								self.createGPU(self, targFolder, each, baseName, parentControl)
							#Clear selection
							mc.select(clear= True)
							#Delete old object
							mc.delete(sel)
							i+=1
					#Move progress bar
					self.progComp.value = self.progComp.value + 1
			else:
				i = 0
				for each in selectedNames:
					reObj = re.search("\d", each[::-1])
					digitIndex = None
					if reObj:
						digitIndex = reObj.start()
						digitIndex = -digitIndex
						if digitIndex == 0:
							digitIndex = None
					baseName = each[:digitIndex] + str(i+1)
					ctrlName = baseName + "_CTRL"
					parentControl = self.createParentControl(ctrlName)
					self.createAlembicExo(targFolder, each, baseName, parentControl)
					if self.createGPU_CHKBOX.isChecked():
						self.createGPU(self, targFolder, each, baseName, parentControl)
					i+=1
					#Move progress bar
					self.progComp.value = self.progComp.value + 1
		else:
			#EXOCORTEX ATTACH-MERGE OPERATION
			allSelShapes = mc.ls(sl=True, dag=True, long= True, ni= True, geometry=True)
			if len(allSelShapes) < 1:
				self.baseMod.statusBar_update("Please select at least one object in your scene to merge the selected caches into.", alert=True)
			else:
				allSel = []
				if self.addPrefixSuffix_CHKBOX.isChecked() and self.import360_CHKBOX.isChecked():
					allSel = self.renameGeo(allSelShapes)
				else:
					for each in allSelShapes:
						transform = mc.listRelatives(each, parent=True, fullPath=True)[0]
						allSel.append(transform)
				for each in allSel:
					if len(mc.listHistory(each)) > 1:
						mc.delete(each , ch = 1)
					transformNode = each
					if '|' in each:
						transformNode = each.rsplit('|', 1)[1]
					shapeNode = mc.listRelatives(each, shapes=True, noIntermediate=True)[0]
					#Create nodes
					exoTimeNode = mc.createNode("ExocortexAlembicTimeControl", name= transformNode + "_exoTimeControl")
					exoFileNode = mc.createNode("ExocortexAlembicFile", name= transformNode + "_exoFile")
					exoXFormNode = mc.createNode("ExocortexAlembicXform", name= transformNode + "_exoXForm")
					exoDeformNode = mc.deformer(shapeNode, type="ExocortexAlembicPolyMeshDeform", name= transformNode + "_exoPolyMeshDeform")[0]
					#Set attributes
					fileNameStr = targFolder + "/" + selectedNames[0] + ".abc"
					mc.setAttr(exoFileNode + ".fileName", fileNameStr, type="string")
					mc.setAttr(exoXFormNode + ".identifier", "/" + transformNode, type="string")
					mc.setAttr(exoDeformNode + ".identifier", "/" + transformNode + "/" + shapeNode, type="string")
					#Connect nodes
					mc.connectAttr(mc.ls(type="time")[0] + ".outTime", exoTimeNode + ".inTime", force=True)
					mc.connectAttr(exoTimeNode + ".outTime", exoFileNode + ".inTime", force=True)
					mc.connectAttr(exoTimeNode + ".outTime", exoDeformNode + ".inTime", force=True)
					mc.connectAttr(exoFileNode + ".outFileName", exoDeformNode + ".fileName", force=True)
					mc.connectAttr(exoFileNode + ".outFileName", exoXFormNode + ".fileName", force=True)
					mc.connectAttr(exoTimeNode + ".outTime", exoXFormNode + ".inTime", force=True)
					mc.connectAttr(exoXFormNode + ".translate", transformNode + ".translate", force=True)
					mc.connectAttr(exoXFormNode + ".rotate", transformNode + ".rotate", force=True)
					mc.connectAttr(exoXFormNode + ".scale", transformNode + ".scale", force=True)
					mc.connectAttr(exoXFormNode + ".outVisibility", transformNode + ".visibility", force=True)
					#mc.connectAttr(exoDeformNode + ".outputGeometry[0]", shapeNode + ".inMesh", force=True)

					#Move progress bar
					self.progComp.value = self.progComp.value + 1
	
	
	def createAlembicExo(self, targFolder, fileName, baseName, parentControl):
		#Import Exocortex Alembic and put it under parentControl
		fileNameStr = "filename=" + targFolder + fileName + ".abc"
		mainOptions = "attachToExisting=0"
		variuosOptions = "normals=1;uvs=0;facesets=0"
		finalStr = ";".join([fileNameStr, mainOptions, variuosOptions])
		importExo = mc.ExocortexAlembic_import(j=[finalStr])
		importedExoTransform = ""
		for node in importExo:
			if mc.nodeType(node) == "transform":
				importedExoTransform = node
				break
		#Parent imported Alembic under control
		mc.parent(importedExoTransform, parentControl)
		#Set locked transforms
		mc.setAttr(importedExoTransform + ".translateX", lock= True)
		mc.setAttr(importedExoTransform + ".translateY", lock= True)
		mc.setAttr(importedExoTransform + ".translateZ", lock= True)
		mc.setAttr(importedExoTransform + ".rotateX", lock= True)
		mc.setAttr(importedExoTransform + ".rotateY", lock= True)
		mc.setAttr(importedExoTransform + ".rotateZ", lock= True)
		mc.setAttr(importedExoTransform + ".scaleX", lock= True)
		mc.setAttr(importedExoTransform + ".scaleY", lock= True)
		mc.setAttr(importedExoTransform + ".scaleZ", lock= True)
		
		#Set parent control radius
		shapeCtrl = mc.listRelatives(parentControl, shapes=True, noIntermediate=True, fullPath=True)[0]
		makeNurbsNode = mc.listConnections(shapeCtrl + ".create", s=True, d=False)[0]
		bbox= mc.xform(importedExoTransform, query=True, boundingBox=True)
		distX = abs(bbox[0] - bbox[3])
		distZ = abs(bbox[2] - bbox[5])
		if distX > distZ:
			mc.setAttr(makeNurbsNode + ".radius", distX + (distX*0.1))
		else:
			mc.setAttr(makeNurbsNode + ".radius", distZ + (distZ*0.1))
		mc.delete(parentControl, ch=True)
		
	
	
	def importAlembic(self, targFolder, selectedNames):
		#REPLACE OPERATION
		if self.replace_RBTN.isChecked():
			if self.replaceSelectedGeo_CHKBOX.isChecked():
				allSelShapes = mc.ls(sl=True, dag=True, long= True, ni= True, geometry=True)
				allSel = []
				for shape in allSelShapes:
					thisTransform = mc.listRelatives(shape, parent= True, fullPath= True)
					allSel.append(thisTransform[0])
				if len(allSel) < 1:
					self.baseMod.statusBar_update("Please select at least one object in your scene to be replaced.", alert=True)
				elif len(selectedNames) > 1 :
					self.baseMod.statusBar_update("Please select one Alembic file only from the list if using the Replace Selected Geometry option.", alert=True)
				else:
					for each in selectedNames:
						reObj = re.search("\d", each[::-1])
						digitIndex = None
						if reObj:
							digitIndex = reObj.start()
							digitIndex = -digitIndex
							if digitIndex == 0:
								digitIndex = None
						i = 0
						for sel in allSel:
							baseName = each[:digitIndex] + str(i+1)
							ctrlName = baseName + "_CTRL"
							parentControl = self.createParentControl(ctrlName, sel)
							self.createAlembic(targFolder, each, baseName, parentControl)
							if self.createGPU_CHKBOX.isChecked():
								self.createGPU(self, targFolder, each, baseName, parentControl)
							#Clear selection
							mc.select(clear= True)
							#Delete old object
							mc.delete(sel)
							i+=1
						#Move progress bar
						self.progComp.value = self.progComp.value + 1
			else:
				i = 0
				for each in selectedNames:
					reObj = re.search("\d", each[::-1])
					digitIndex = None
					if reObj:
						digitIndex = reObj.start()
						digitIndex = -digitIndex
						if digitIndex == 0:
							digitIndex = None
					baseName = each[:digitIndex] + str(i+1)
					ctrlName = baseName + "_CTRL"
					parentControl = self.createParentControl(ctrlName)
					self.createAlembic(targFolder, each, baseName, parentControl)
					if self.createGPU_CHKBOX.isChecked():
						self.createGPU(self, targFolder, each, baseName, parentControl)
					i+=1
					#Move progress bar
					self.progComp.value = self.progComp.value + 1
		else:
			#ALEMBIC MERGE OPERATION
			allSelShapes = mc.ls(sl=True, dag=True, long= True, ni= True, geometry=True)
			allSel = []
			for shape in allSelShapes:
				thisTransform = mc.listRelatives(shape, parent= True)[0]
				allSel.append(thisTransform)
			if len(allSel) < 1:
				self.baseMod.statusBar_update("Please select at least one object in your scene to merge the selected caches into.", alert=True)
			else:
				duplicates = {}
				oldAlembics = {}
				oldPolyTransf = {}
				filterObjectsStr = '-filterObjects "'
				connectStr = '-connect "'
				for obj in allSel:
					namespaceIndex = obj.rfind(":")
					cleanName = obj
					if namespaceIndex == -1:
						filterObjectsStr += obj + ' '
					else:
						filterObjectsStr += obj[namespaceIndex+1:] + ' '
						cleanName = obj[namespaceIndex+1:]
					connectStr += obj + ' '
					#Duplicar original para tener objeto con las UVs
					dupObj = self.cleanCopy(obj, cleanName + "_dup")
					duplicates[obj] = dupObj
					#Old connections
					shapeNode = mc.listRelatives(obj, shapes=True, noIntermediate= False, fullPath=True)
					shapeConnections = mc.listConnections(shapeNode[0] + ".inMesh", d= False, s=True)
					if shapeConnections != None:
						if mc.nodeType(shapeConnections[0]) == "polyTransfer":
							oldPolyTransf[obj] = mc.listConnections(shapeNode[0] + ".inMesh", d= False, s=True)[0]
						elif mc.nodeType(shapeConnections[0]) == "AlembicNode":
							oldAlembics[obj] = mc.listConnections(shapeNode[0] + ".inMesh", d= False, s=True)[0]
				for each in selectedNames:
					#Hacer el import de alembic
					evalStr = 'AbcImport -mode import ' + filterObjectsStr.rstrip() + '" ' + connectStr.rstrip() + '" "' + targFolder + each + '.abc";'
					importStr = mel.eval(evalStr)
					for obj in allSel:
						#Find alembic node
						shapeNode = mc.listRelatives(obj, shapes=True, noIntermediate= False, fullPath=True)
						#Poly transfer or Alembic
						shapeConnections = mc.listConnections(shapeNode[0] + ".inMesh", d= False, s=True)
						namespaceIndex = obj.rfind(":")
						cleanName = obj
						if namespaceIndex != -1:
							cleanName = obj[namespaceIndex+1:]
						if shapeConnections != None:
							if mc.nodeType(shapeConnections[0]) == "polyTransfer":
								#Conectar nodos con polyTransfer
								mc.connectAttr(duplicates[obj] + ".outMesh", shapeConnections[0] + ".otherPoly", force=True)
								#Borrar conexion del duplicado al polyTransfer
								mc.disconnectAttr(duplicates[obj] + ".outMesh", shapeConnections[0] + ".otherPoly")
							elif mc.nodeType(shapeConnections[0]) == "AlembicNode":
								connectionAttr = mc.listConnections(shapeNode[0] + ".inMesh", d= False, s=True, plugs= True)
								#Si no existe ya, crear nodo polyTransfer
								polyTransNode = mc.createNode("polyTransfer", name= cleanName + "_plyTrf")
								#Conectar nodos con polyTransfer
								mc.connectAttr(connectionAttr[0], polyTransNode + ".inputPolymesh", force= True)
								mc.connectAttr(duplicates[obj] + ".outMesh", polyTransNode + ".otherPoly", force=True)
								mc.connectAttr(polyTransNode + ".output", shapeNode[0] + ".inMesh", force= True)
								#Borrar conexion del duplicado al polyTransfer
								mc.disconnectAttr(duplicates[obj] + ".outMesh", polyTransNode + ".otherPoly")
							elif mc.nodeType(shapeConnections[0]) == "groupParts":
								connectionAttr = mc.listConnections(shapeNode[0] + ".inMesh", d= False, s=True, plugs= True)
								#Si no existe ya, crear nodo polyTransfer
								polyTransNode = mc.createNode("polyTransfer", name= cleanName + "_plyTrf")
								#Conectar nodos con polyTransfer
								mc.connectAttr(connectionAttr[0], polyTransNode + ".inputPolymesh", force= True)
								mc.connectAttr(duplicates[obj] + ".outMesh", polyTransNode + ".otherPoly", force=True)
								mc.connectAttr(polyTransNode + ".output", shapeNode[0] + ".inMesh", force= True)
								#Borrar conexion del duplicado al polyTransfer
								mc.disconnectAttr(duplicates[obj] + ".outMesh", polyTransNode + ".otherPoly")
							else:
								self.baseMod.statusBar_update("Couldn't recognize input connection. " + obj + " might be left UV-less.", alert=True)
					if len(oldPolyTransf.keys()) > 0:
						for key in oldPolyTransf.keys():
							if mc.objExists(oldPolyTransf[key]):
								outputConnections = mc.listConnections(oldPolyTransf[key], d= True, s=False)
								if outputConnections == None:
									mc.delete(oldPolyTransf[key])
					if len(oldAlembics.keys()) > 0:
						for key in oldAlembics.keys():
							if mc.objExists(oldAlembics[key]):
								outputConnections = mc.listConnections(oldAlembics[key], d= True, s=False)
								if outputConnections == None:
									mc.delete(oldAlembics[key])
					#Move progress bar
					self.progComp.value = self.progComp.value + 1
				if len(duplicates.keys()) > 0:
					for key in duplicates.keys():
						mc.delete(duplicates[key])
						
	
	def createParentControl(self, ctrlName, sel=None):
		#Create parent control, import Alembics and put them under it
		parentControl = mc.circle( nr=(0, 1, 0), c=(0, 0, 0), name= ctrlName, ch= True)
		parentShape = mc.listRelatives(parentControl, shapes= True, noIntermediate= True)
		mc.setAttr(parentShape[0] + ".overrideEnabled", 1)
		mc.setAttr(parentShape[0] + ".overrideColor", 13)
		if sel != None:
			#Get transformations
			translateObj = mc.xform(sel, query=True, translation=True,worldSpace=True)
			rotateObj = mc.xform(sel, query=True, rotation=True,worldSpace=True)
			scaleObj = mc.xform(sel, query=True, scale=True,worldSpace=True)
			transX= translateObj[0]
			transY= translateObj[1]
			transZ= translateObj[2]
			rotX= rotateObj[0]
			rotY= rotateObj[1]
			rotZ= rotateObj[2]
			scaleX= scaleObj[0]
			scaleY= scaleObj[1]
			scaleZ= scaleObj[2]
			#Get rotation order
			rotOrder = mc.xform(sel, query=True, rotateOrder=True)
			#Set rotation order
			rotOrderOptions = ['xyz','yzx','zxy','xzy','yxz','zyx']
			mc.setAttr(parentControl[0] + ".rotateOrder", rotOrderOptions.index(rotOrder))
			#Set transformations
			mc.setAttr(parentControl[0] + ".translateX", transX)
			mc.setAttr(parentControl[0] + ".translateY", transY)
			mc.setAttr(parentControl[0] + ".translateZ", transZ)
			mc.setAttr(parentControl[0] + ".rotateX", rotX)
			mc.setAttr(parentControl[0] + ".rotateY", rotY)
			mc.setAttr(parentControl[0] + ".rotateZ", rotZ)
			mc.setAttr(parentControl[0] + ".scaleX", scaleX)
			mc.setAttr(parentControl[0] + ".scaleY", scaleY)
			mc.setAttr(parentControl[0] + ".scaleZ", scaleZ)
		
		return parentControl[0]
		
	
	def createGPU(self, targFolder, fileName, baseName, parentControl):
		#crear gpu node
		gpuNode = mc.createNode("gpuCache", name= baseName +  "_gpuNode")
		gpuTransform= mc.listRelatives(gpuNode, parent= True)
		gpuTransformRename = mc.rename(gpuTransform, baseName + "_gpu")
		mc.setAttr(gpuTransformRename + ".cacheFileName", targFolder + fileName + ".abc", type = "string")
		mc.parent(gpuTransformRename, parentControl)
		#Set locked transforms
		mc.setAttr(gpuTransformRename + ".translateX", lock= True)
		mc.setAttr(gpuTransformRename + ".translateY", lock= True)
		mc.setAttr(gpuTransformRename + ".translateZ", lock= True)
		mc.setAttr(gpuTransformRename + ".rotateX", lock= True)
		mc.setAttr(gpuTransformRename + ".rotateY", lock= True)
		mc.setAttr(gpuTransformRename + ".rotateZ", lock= True)
		mc.setAttr(gpuTransformRename + ".scaleX", lock= True)
		mc.setAttr(gpuTransformRename + ".scaleY", lock= True)
		mc.setAttr(gpuTransformRename + ".scaleZ", lock= True)
	
	
	def createAlembic(self, targFolder, fileName, baseName, parentControl):
		normalEvalStr = 'AbcImport -mode import -reparent ' + parentControl + ' "' + targFolder + fileName + '.abc";'
		normalAlembicNode = mel.eval(normalEvalStr)
		#Find Alembic Node again
		controlChilds = mc.listRelatives(parentControl, children=True, fullPath= True, type= "transform")
		newTransformRename = mc.rename(controlChilds[0], baseName + "_MSH_bkd")
		#Set locked transforms
		mc.setAttr(newTransformRename + ".translateX", lock= True)
		mc.setAttr(newTransformRename + ".translateY", lock= True)
		mc.setAttr(newTransformRename + ".translateZ", lock= True)
		mc.setAttr(newTransformRename + ".rotateX", lock= True)
		mc.setAttr(newTransformRename + ".rotateY", lock= True)
		mc.setAttr(newTransformRename + ".rotateZ", lock= True)
		mc.setAttr(newTransformRename + ".scaleX", lock= True)
		mc.setAttr(newTransformRename + ".scaleY", lock= True)
		mc.setAttr(newTransformRename + ".scaleZ", lock= True)
		
		#Set parent control radius
		shapeCtrl = mc.listRelatives(parentControl, shapes=True, noIntermediate=True, fullPath=True)[0]
		makeNurbsNode = mc.listConnections(shapeCtrl + ".create", s=True, d=False)[0]
		bbox= mc.xform(newTransformRename, query=True, boundingBox=True)
		distX = abs(bbox[0] - bbox[3])
		distZ = abs(bbox[2] - bbox[5])
		if distX > distZ:
			mc.setAttr(makeNurbsNode + ".radius", distX + (distX*0.1))
		else:
			mc.setAttr(makeNurbsNode + ".radius", distZ + (distZ*0.1))
		mc.delete(parentControl, ch=True)
		mc.setAttr(newTransformRename + ".visibility", 0)
		
	
	def renameGeo(self, _selGeo):
		name= self.assetsMod.Assets_Component.assetName_COMBOBOX.currentText()
		if self.assetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText() not in [None, ""]:
			name = self.assetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
		customSuffix = self.customSuffix_LINEEDIT.text()
		renamedTransforms = []
		for each in _selGeo:
			transform = mc.listRelatives(each, parent=True, fullPath=True)[0]
			transformNode = transform
			if '|' in transformNode:
				transformNode = transform.rsplit('|', 1)[1]
			if self.customSuffix_CHKBOX.isChecked():
				result = mc.rename(transform, name + "_" + customSuffix + "_" + transformNode + "_bkd")
				if '|' in transform:
					fullPathResult = transform.rsplit('|',1)[0] + '|' + result
					renamedTransforms.append(fullPathResult)
				else:
					renamedTransforms.append(result)
			else:
				result = mc.rename(transform, name + "_" + transformNode + "_bkd")
				if '|' in transform:
					fullPathResult = transform.rsplit('|',1)[0] + '|' + result
					renamedTransforms.append(fullPathResult)
				else:
					renamedTransforms.append(result)
		
		return renamedTransforms
	
	
	def cleanCopy(self, _geom, _thisName):
		"""def to get clean geometry"""
	
		polyUniteNode = mc.createNode("polyUnite", name = _thisName + "_pu")
		cubeProxy = mc.polyCube(name= _thisName, ch= False)
		cubeShape = mc.listRelatives(cubeProxy[0], shapes= True, fullPath= True, noIntermediate= True)
		mc.connectAttr(_geom + ".worldMatrix[0]", polyUniteNode + ".inputMat[0]", force= True)
		mc.connectAttr(_geom + ".worldMesh[0]", polyUniteNode + ".inputPoly[0]", force= True)
		mc.connectAttr(polyUniteNode + ".output", cubeShape[0] + ".inMesh", force= True)
		mc.delete(cubeProxy[0], ch = 1)
		
		return cubeProxy[0]
	
	
	def populateList(self):
		"""To populate the Characters/Props list with the shot exported caches"""
		#Clean list first
		charsPropsModel = dvm.ObjectsListModel([], self)
		self.charsProps_LISTVIEW.setModel(charsPropsModel)
		#Get folder
		targFolder= self.buildProjSeqShotPath()
		if self.customPathComp.customPath_CHKBOX.isChecked():
			targFolder = str(self.customPathComp.customPath_LINEEDIT.text()) + "/"
		elif self.import360_CHKBOX.isChecked():
			targFolder = self.buildCachePath()
		
		#Get file type
		fileType = ""
		if self.cacheComp.pointCache_RBTN.isChecked():
			fileType = "*.mc"
		else:
			fileType = "*.abc"
		
		#Files list
		filesList = mc.getFileList(folder= targFolder, filespec= fileType)
		filesListMcx = mc.getFileList(folder= targFolder, filespec= "*.mcx")
		if filesList != None:
			if self.cacheComp.pointCache_RBTN.isChecked():
				filesList.extend(filesListMcx)
			cleanList = []
			for e in filesList:
				if e[-3:] == "mcx":
					cleanList.append( e[:-( len(".mcx") )] )
				elif e[-2:] == "mc":
					cleanList.append( e[:-( len(".mc") )] )
				elif e[-3:] == "abc":
					cleanList.append( e[:-( len(".abc") )] )
			cleanList.sort()
			
			if len(cleanList) >= 1:
				#Set Chars/Props model
				charsPropsModel = dvm.ObjectsListModel(cleanList, self)
				self.charsProps_LISTVIEW.setModel(charsPropsModel)   
		else:
			charsPropsModel = dvm.ObjectsListModel([], self)
			self.charsProps_LISTVIEW.setModel(charsPropsModel)
			result = "This shot doesn't have any cache files to import."
			if self.customPathComp.customPath_CHKBOX.isChecked():
				result = "This path doesn't have any cache files to import."
			elif self.import360_CHKBOX.isChecked():
				result = "This asset doesn't have any cache files to import."
				
			self.baseMod.statusBar_update(result, status=True)
	
	
	def buildCachePath(self):
		assetType = self.assetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
		assetName = self.assetsMod.Assets_Component.assetName_COMBOBOX.currentText()
		subAssetName = self.assetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
		propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
		if subAssetName not in [None,""]:
			propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_subasset",
                        "placeholder":False}
		publishedNodes = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
		
		assetTypeDict={"name":assetType,
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
		assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		if assetType == 'PROPS':
			assetDict= {"name":"$PropName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'VEHICLES':
			assetDict= {"name":"$VehicleName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'SETS':
			assetDict= {"name":"$SetName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
		for node in publishedNodes:
			pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict})
			if subAssetName not in [None,""]:
				pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict,subAssetName:subAssetDict})
			pathList.append(node.name)
			#Build path string
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
			
			if "$" not in pathStr:
				return pathStr + "/Cache360"
	
	
	def buildProjSeqShotPath(self):
		seq = str(self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.currentText())
		shot = str(self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentText())
		propertiesDict = {"abstractFolderType":"leaffolder",
		                  "concreteFolderType":"published_leaffolder",
		                  "folderFunction":"published_content",
		                  "folderConcreteFunction":"published_cache",
		                  "placeholder":False}
		cacheNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)[0]
		if cacheNode != None:
			seqDict={"name":"$SEQ_##",
		            "abstractFolderType":"subfolder",
		            "concreteFolderType":"sequence_subfolder",
		            "folderFunction":"grouping",
		            "folderConcreteFunction":"",
		            "placeholder":True}
			shotDict={"name":"$s_###",
		            "abstractFolderType":"subfolder",
		            "concreteFolderType":"shot_subfolder",
		            "folderFunction":"grouping",
		            "folderConcreteFunction":"",
		            "placeholder":True}
			pathList = self.pInfo.rebuildPath(cacheNode, {seq:seqDict, shot:shotDict})
			pathList.append(cacheNode.name)
			
			#Build path string
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
			
			return pathStr
		else:
			return None			
	
	
	def refreshFileType(self):
		if self.cacheComp.pointCache_RBTN.isChecked():
			self.importOBJs_RBTN.setChecked(True)
			self.useSceneGeo_RBTN.setChecked(False)
			self.importOBJs_RBTN.setEnabled(True)
			self.useSceneGeo_RBTN.setEnabled(True)
			self.createGPU_CHKBOX.setEnabled(False)
			self.replaceSelectedGeo_CHKBOX.setEnabled(False)
			self.merge_RBTN.setEnabled(False)
			self.replace_RBTN.setEnabled(False)
			self.alembicOpts_LABEL.setEnabled(False)
			self.pointCacheOpts_LABEL.setEnabled(True)
			self.charsProps_LISTVIEW.setSelectionMode(self.charsProps_LISTVIEW.MultiSelection)
			self.populateList()
		elif self.cacheComp.alembic_RBTN.isChecked():
			self.importOBJs_RBTN.setChecked(False)
			self.useSceneGeo_RBTN.setChecked(False)
			self.importOBJs_RBTN.setEnabled(False)
			self.useSceneGeo_RBTN.setEnabled(False)
			if self.merge_RBTN.isChecked():
				self.merge_RBTN.setEnabled(True)
				self.replace_RBTN.setEnabled(True)
				self.alembicOpts_LABEL.setEnabled(True)
				self.createGPU_CHKBOX.setEnabled(False)
				self.replaceSelectedGeo_CHKBOX.setEnabled(False)
				self.createGPU_CHKBOX.setChecked(False)
				self.replaceSelectedGeo_CHKBOX.setChecked(False)             
			else:
				self.merge_RBTN.setEnabled(True)
				self.replace_RBTN.setEnabled(True)
				self.alembicOpts_LABEL.setEnabled(True)
				self.createGPU_CHKBOX.setEnabled(True)
				self.replaceSelectedGeo_CHKBOX.setEnabled(True)
				self.createGPU_CHKBOX.setChecked(True)
				self.replaceSelectedGeo_CHKBOX.setChecked(True)
			self.pointCacheOpts_LABEL.setEnabled(False)
			self.charsProps_LISTVIEW.setSelectionMode(self.charsProps_LISTVIEW.SingleSelection)
			self.populateList()
		else:
			self.importOBJs_RBTN.setChecked(False)
			self.useSceneGeo_RBTN.setChecked(False)
			self.importOBJs_RBTN.setEnabled(False)
			self.useSceneGeo_RBTN.setEnabled(False)
			if self.merge_RBTN.isChecked():
				self.merge_RBTN.setEnabled(True)
				self.replace_RBTN.setEnabled(True)
				self.alembicOpts_LABEL.setEnabled(True)
				self.createGPU_CHKBOX.setEnabled(False)
				self.replaceSelectedGeo_CHKBOX.setEnabled(False)
				self.createGPU_CHKBOX.setChecked(False)
				self.replaceSelectedGeo_CHKBOX.setChecked(False)               
			else:
				self.merge_RBTN.setEnabled(True)
				self.replace_RBTN.setEnabled(True)
				self.alembicOpts_LABEL.setEnabled(True)
				self.createGPU_CHKBOX.setEnabled(False)
				self.replaceSelectedGeo_CHKBOX.setEnabled(True)
				self.createGPU_CHKBOX.setChecked(False)
				self.replaceSelectedGeo_CHKBOX.setChecked(True)
			self.pointCacheOpts_LABEL.setEnabled(False)
			self.charsProps_LISTVIEW.setSelectionMode(self.charsProps_LISTVIEW.SingleSelection)
			self.populateList()
	
	
	def refreshMergeReplace(self):
		if self.merge_RBTN.isChecked():
			self.createGPU_CHKBOX.setEnabled(False)
			self.replaceSelectedGeo_CHKBOX.setEnabled(False)
			self.createGPU_CHKBOX.setChecked(False)
			self.replaceSelectedGeo_CHKBOX.setChecked(False)
		else:
			self.createGPU_CHKBOX.setEnabled(True)
			self.replaceSelectedGeo_CHKBOX.setEnabled(True)
			self.createGPU_CHKBOX.setChecked(True)
			self.replaceSelectedGeo_CHKBOX.setChecked(True)
	
	
	def refreshCustomPath(self):
		if self.customPathComp.customPath_CHKBOX.isChecked():
			self.projSeqShotMod.Project_Component.project_COMBOBOX.setEnabled(False)
			self.projSeqShotMod.Shot_Component.shot_COMBOBOX.setEnabled(False)
			self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.setEnabled(False)
			self.import360_CHKBOX.setChecked(False)
			self.import360_CHKBOX.setEnabled(False)
			self.addPrefixSuffix_CHKBOX.setEnabled(False)
			self.customSuffix_CHKBOX.setEnabled(False)
			self.customSuffix_LINEEDIT.setEnabled(False)
			self.assetsMod.container.setEnabled(False)
			self.populateList()
		else:
			if self.import360_CHKBOX.isChecked():
				self.projSeqShotMod.Project_Component.project_COMBOBOX.setEnabled(True)
				self.projSeqShotMod.Shot_Component.shot_COMBOBOX.setEnabled(False)
				self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.setEnabled(False)
				self.assetsMod.container.setEnabled(True)
				self.addPrefixSuffix_CHKBOX.setEnabled(True)
				self.customSuffix_CHKBOX.setEnabled(True)
				self.customSuffix_LINEEDIT.setEnabled(True)
			else:
				self.projSeqShotMod.Project_Component.project_COMBOBOX.setEnabled(True)
				self.projSeqShotMod.Shot_Component.shot_COMBOBOX.setEnabled(True)
				self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.setEnabled(True)
				self.assetsMod.container.setEnabled(False)
				self.addPrefixSuffix_CHKBOX.setEnabled(False)
				self.customSuffix_CHKBOX.setEnabled(False)
				self.customSuffix_LINEEDIT.setEnabled(False)
			self.customPathComp.customPath_BTN.setEnabled(False)
			self.customPathComp.customPath_LINEEDIT.setEnabled(False)
			self.import360_CHKBOX.setEnabled(True)
			self.populateList()
	
	
	def refreshImport360(self):
		if self.import360_CHKBOX.isChecked():
			self.projSeqShotMod.Shot_Component.shot_COMBOBOX.setEnabled(False)
			self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.setEnabled(False)
			self.customPathComp.customPath_BTN.setEnabled(False)
			self.customPathComp.customPath_LINEEDIT.setEnabled(False)
			self.customPathComp.customPath_CHKBOX.setChecked(False)
			self.assetsMod.container.setEnabled(True)
			self.addPrefixSuffix_CHKBOX.setEnabled(True)
			self.addPrefixSuffix_CHKBOX.setChecked(True)
			self.customSuffix_CHKBOX.setEnabled(True)
			self.customSuffix_CHKBOX.setChecked(False)
			self.customSuffix_LINEEDIT.setEnabled(True)
			self.populateList()
		else:
			self.projSeqShotMod.Project_Component.project_COMBOBOX.setEnabled(True)
			self.projSeqShotMod.Shot_Component.shot_COMBOBOX.setEnabled(True)
			self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.setEnabled(True)
			self.assetsMod.container.setEnabled(False)
			self.addPrefixSuffix_CHKBOX.setEnabled(False)
			self.customSuffix_CHKBOX.setEnabled(False)
			self.customSuffix_LINEEDIT.setEnabled(False)
			if self.customPathComp.customPath_CHKBOX.isChecked():
				self.customPathComp.customPath_BTN.setEnabled(True)
				self.customPathComp.customPath_LINEEDIT.setEnabled(True)
				self.projSeqShotMod.Project_Component.project_COMBOBOX.setEnabled(False)
				self.projSeqShotMod.Shot_Component.shot_COMBOBOX.setEnabled(False)
				self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.setEnabled(False)
	
	
	def refreshCustomSuffix(self):
		if self.customSuffix_CHKBOX.isChecked():
			self.customSuffix_LINEEDIT.setEnabled(True)
		else:
			self.customSuffix_LINEEDIT.setEnabled(False)


	def refreshRenameSelectedGeo(self):
		if self.addPrefixSuffix_CHKBOX.isChecked():
			if self.customSuffix_CHKBOX.isChecked():
				self.customSuffix_LINEEDIT.setEnabled(True)
			self.customSuffix_CHKBOX.setEnabled(True)
		else:
			self.customSuffix_LINEEDIT.setEnabled(False)
			self.customSuffix_CHKBOX.setEnabled(False)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for cache nodes list view
	##--------------------------------------------------------------------------------------------     
	def charsPropsOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.charsProps_LISTVIEW)
		selectAllQ = menu.addAction("Select All")
		menu.popup(self.charsProps_LISTVIEW.mapToGlobal(pos))
		
		selectAllQ.triggered.connect(self.selectAllList)


	def selectAllList(self):
		self.charsProps_LISTVIEW.selectAll()
	

##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/pipeline/CacheImporter_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()
