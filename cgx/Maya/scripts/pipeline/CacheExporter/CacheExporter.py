# -*- coding: utf-8 -*-
'''
Cache Exporter tool. Export animation into cache files.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
from cgx.gui.Qt import QtWidgets, QtCore
from cgx.gui.Qt import __binding__
import Red9.core.Red9_Meta as r9Meta
import cgx.Maya.scripts.maya_libs.Maya_MetaData as cgxMeta
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
import cgx.gui.DataViewModels as dvm
from cgx.core.JSONManager import JSONManager

import maya.cmds as mc


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "3.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
    appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/pipeline/CacheExporter'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"
alembicVersion_ui = appRootFolder + "/ui/" + "alembicVersion.ui"
config_json = appRootFolder + "/cfg/" + "CacheExporter_config.json"
alembicVersion_json = appRootFolder + "/cfg/" + "AlembicVersion_config.json"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)
alembicVersionform, alembicVersionbase = uiParser.loadUiType(alembicVersion_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
        '''
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
        '''
        super(Main_GUI,self).__init__(parent)
        self.setupUi(self)
        
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        
        self.appTools = appTools
        if self.appTools != None:
            self.initTool()
            self.createComponents()
            self.initComponents()
            self.initUI()
            self.setConnections()
            self.checkPlugins()
            self.refreshTree()
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        
        
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def setConnections(self):
        '''
        Connect signals.
        '''
        #SIGNALS
        self.cancel_BTN.clicked.connect(self.baseMod.quitApp)
        self.export_BTN.clicked.connect(self.preExportCaches)
        self.alembicVersion_opt.triggered.connect(self.alembicVersionDialog)
        #REFRESH
        self.useTags_CHKBOX.stateChanged.connect(self.refreshUseTagsOnly)
        self.exportPose_CHKBOX.stateChanged.connect(self.refreshExportPose)
        self.folderPerCharProp_CHKBOX.stateChanged.connect(self.refreshFolderPerCharProp)
        self.customFolderName_CHKBOX.stateChanged.connect(self.refreshCustomFolderName)
        self.exportForVFX_CHKBOX.stateChanged.connect(self.refreshExportForVFX)
        #STATUS
        self.filePerCharProp_CHKBOX.setChecked(True)
        self.customFolderName_CHKBOX.setEnabled(False)
        self.poseName_LINEEDIT.setEnabled(False)

        #CONTEXT MENUS
        self.charsProps_TREEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.charsProps_TREEVIEW.customContextMenuRequested.connect(self.charsPropsOptions)
        
    
    def initTool(self):
        '''
        Initializes tool settings.
        '''
        self.initAttrs()

    
    def resetTool(self):
        self.initAttrs()
        self.initComponents()
        self.initUI()
        self.checkPlugins()
        self.refreshTree()
        self.folderPerCharProp_CHKBOX.setChecked(False)
        self.customFolderName_CHKBOX.setChecked(False)
        
        
    def initAttrs(self):
        '''
        Initializes variables and components for this tool.
        '''
        self.__client = "defaultClient"
        self.__toolName = "Cache Exporter"
        self.__toolVersion = __version__
        
        #Set time range
        self.appTools.tRange.handleIn = 2
        self.appTools.tRange.handleOut = 2
        self.appTools.tRange.startFrame = 1001
        self.appTools.tRange.endFrame = 1101
        
        #Attributes
        self.configFile = JSONManager(config_json, self)
    
    
    def createComponents(self):
        self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
        self.baseMod = self.Base_GUIModulesFactory.createModule('base')
        self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo, tRange=self.appTools.tRange)
        self.cacheMod = self.Pipeline_GUIModulesFactory.createModule('Cache') 
        self.cacheMod.pos = [0,20]
        self.projSeqShotMod = self.Pipeline_GUIModulesFactory.createModule('ProjSeqShot')
        self.projSeqShotMod.pos = [0,145]
        self.customPathComp = pipeComponents.CustomPath_Component(self,self.centralWidget(), self.sInfo)
        self.customPathComp.pos = [0,175]
        self.customPathComp.fileMode = self.customPathComp.browseDialog.FileMode.Directory
        self.timeRangeMod = self.Pipeline_GUIModulesFactory.createModule('TimeRange')
        self.timeRangeMod.pos = [0,430]
        self.timeRangeMod.size = [331,101]
        self.timeRangeMod.OffsetStartFrame_Component.container.setVisible(False)
        self.timeRangeMod.SampleBy_Component.sampleBy_DBLSPINBOX.setValue(1)
        self.progComp = pipeComponents.Progress_Component(self,self.centralWidget())
        self.progComp.pos = [340,435]
        self.progComp.reset()
        #Add alembic version option
        self.alembicVersion_opt = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
        self.alembicVersion_opt.setObjectName("alembic_version")
        self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[2], self.alembicVersion_opt)
        className= str(self.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.alembicVersion_opt.setText(QtWidgets.QApplication.translate(className, "Alembic Version", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.alembicVersion_opt.setText(QtWidgets.QApplication.translate(className, "Alembic Version", None))
    
    
    def initComponents(self):
        self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
        self.appTools.app_initUI.setCurrentProject(self.projSeqShotMod.Project_Component)
        self.appTools.app_initUI.setCurrentSequence(self.projSeqShotMod.Sequence_Component)
        self.appTools.app_initUI.setCurrentShot(self.projSeqShotMod.Shot_Component)
        self.timeRangeMod.FPS_Component.initComponent(self.pInfo.maya_getFPS())
        self.appTools.app_initUI.checkFPS(self.timeRangeMod.FPS_Component)
        self.appTools.tRange.setFramesToTimeSlider()
        self.timeRangeMod.initComponents()
    
    
    def initUI(self):
        '''
        Initialize GUI components.
        '''        
        pass
    
    
    def checkPlugins(self):
        '''
        Check if plugins are loaded.
        :return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
        :rtype: dict
        '''
        checkDict = self.appTools.checkPlugins(self)
        return checkDict
    
    
    def preExportCaches(self):
        check = self.checkPlugins()
        if len(check.keys()) <= 0:
            self.exportCaches()
        else:
            if self.cacheMod.CacheFileTypes_Component.alembic_RBTN.isChecked():
                if "Alembic" in check.keys():
                    self.baseMod.statusBar_update(check["Alembic"], alert=True)
                else:
                    self.exportCaches()
            elif self.cacheMod.CacheFileTypes_Component.alembicExo_RBTN.isChecked():
                if "Exocortex" in check.keys():
                    self.baseMod.statusBar_update(check["Exocortex"], alert=True)
                else:
                    self.exportCaches()
    
    
    def exportCaches(self):
        """Export caches"""
        currentEvalManager = mc.evaluationManager( query=True, mode=True )[0]
        try:
            #Create geo dict
            sampleBy = float(self.timeRangeMod.SampleBy_Component.sampleBy_DBLSPINBOX.value())
            geoDict, bakingMethods = self.createGeoDict(sampleBy)
            
            #Set progress bar
            self.progComp.reset()
            self.progComp.minimum = 0
            multProgress = 2
            if self.exportObjs_CHKBOX.isChecked():
                multProgress = 3
                
            #Stop refresh
            mc.refresh(suspend=True)
            mc.evaluationManager(mode='off')
            self.progComp.maximum = len(geoDict.keys()) * multProgress
            
            #Folder per Character/Prop creation
            self.fileAndFolderCombinations(geoDict, bakingMethods, sampleBy)
            
            self.progComp.reset()
            
            #Restart refresh
            mc.refresh(suspend=False)
            mc.evaluationManager(mode=currentEvalManager)
        except OSError as err:
            self.baseMod.statusBar_update("There was an error, nothing was exported. Check Script Editor.", warning=True)
            print("OS Error: {}".format(err))
            #Restart refresh
            mc.refresh(suspend=False)
            mc.evaluationManager(mode=currentEvalManager)
        except ValueError as err:
            self.baseMod.statusBar_update("There was an error, nothing was exported. Check Script Editor.", warning=True)
            print("Value Error: {}".format(err))
            #Restart refresh
            mc.refresh(suspend=False)
            mc.evaluationManager(mode=currentEvalManager)
        except RuntimeError as err:
            self.baseMod.statusBar_update("There was an error, nothing was exported. Check Script Editor.", warning=True)
            print("Runtime Error: {}".format(err))
            #Restart refresh
            mc.refresh(suspend=False)
            mc.evaluationManager(mode=currentEvalManager)
        except IndexError as err:
            self.baseMod.statusBar_update("There was an error, nothing was exported. Check Script Editor.", warning=True)
            print("Runtime Error: {}".format(err))
            #Restart refresh
            mc.refresh(suspend=False)
            mc.evaluationManager(mode=currentEvalManager)
        except StandardError as err:
            self.baseMod.statusBar_update("There was an error, nothing was exported. Check Script Editor.", warning=True)
            print("StandardError: {}".format(err))
            #Restart refresh
            mc.refresh(suspend=False)
            mc.evaluationManager(mode=currentEvalManager)
        else:
            #Restart refresh
            mc.refresh(suspend=False)
            mc.evaluationManager(mode=currentEvalManager)
            #Done Alert!!
            self.baseMod.statusBar_update("Done exporting caches.", success=True)
    
    
    def createGeoDict(self, _sampleBy):
        '''Collect geometry for baking and create Baking Method object'''
        geoDict = {} #GEO_CURRENT_NAME = [GEO_USER_NAME, rootName, rootNode]. Using node name to store reference to real object, and datalist for user values
        if self.useTags_CHKBOX.isChecked():
            roots = self.charsProps_TREEVIEW.model().dataList.children
            for each in roots:
                metaNode = each.data[4] #Get metanode
                #If export is active for this metanode then proceed
                if metaNode.export == True:
                    childGeom = each.children
                    for item in childGeom:
                        geoCurrentName = item.data[2]
                        geoUserName = item.name
                        if geoCurrentName in geoDict:
                            self.baseMod.statusBar_update("Found duplicated names in the project. Some geometry will be missing after exporting.", warning=True)
                        else:
                            geoDict[geoCurrentName] = [geoUserName, each.name, each]
                bakingMethods = self.appTools.createBakingMethods(geoDict, self.progComp.progress_PBAR, _sampleBy)
            return geoDict, bakingMethods
        else:
            selected = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
            if len(selected) >= 1:
                for item in selected:
                    if item.isValid():
                        thisNode = item.internalPointer()
                        tagType= thisNode.data[1]
                        addedGeo = []
                        if tagType == "root":
                            children = thisNode.children
                            for child in children:
                                geoCurrentName = child.data[2]
                                geoUserName = child.name
                                geoDict[geoCurrentName] = [geoUserName, thisNode.name, thisNode]
                                addedGeo.append(child)
                        elif tagType == "geo":
                            if thisNode not in addedGeo:
                                geoCurrentName = thisNode.data[2]
                                geoUserName = thisNode.name
                                parentNode = thisNode.parent
                                geoDict[geoCurrentName] = [geoUserName, parentNode.name, parentNode]
                                addedGeo.append(thisNode)
            bakingMethods = self.appTools.createBakingMethods(geoDict, self.progComp.progress_PBAR, _sampleBy)
            return geoDict, bakingMethods
    
    
    def fileAndFolderCombinations(self, _geoDict, _bakingMethods, _sampleBy):
        roots = self.charsProps_TREEVIEW.model().dataList.children
        if not self.useTags_CHKBOX.isChecked():
            roots = []
            selected = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
            if len(selected) >= 1:
                for item in selected:
                    if item.isValid():
                        thisNode = item.internalPointer()
                        if thisNode.data[1] == "root":
                            roots.append(thisNode)
                        elif thisNode.data[1] == "geo":
                            parentNode = thisNode.parent
                            if parentNode not in roots:
                                roots.append(parentNode)
        rootNames = []
        for each in roots:
            rootNames.append(each.name)
        rootNamesSet = set(rootNames)
        rootNamesClean = list(rootNamesSet)
        charPropInstances = {}
        for each in rootNamesClean:
            charPropCount = rootNames.count(each)
            charPropInstances[each] = [charPropCount,1]#Dict con nombre del char/prop y numero de repeticiones
        
        #If file and folder per char is selected
        if self.folderPerCharProp_CHKBOX.isChecked() and self.filePerCharProp_CHKBOX.isChecked():
            foldersDict = self.folderPerCharProp(roots, charPropInstances)
            self.filePerCharProp(_geoDict, _sampleBy, foldersDict, _bakingMethods)
        #If only folder per char is selected
        elif self.folderPerCharProp_CHKBOX.isChecked() and not self.filePerCharProp_CHKBOX.isChecked():
            foldersDict = self.folderPerCharProp(roots, charPropInstances)
            self.filePerGeometry(_geoDict, _sampleBy, foldersDict, _bakingMethods)
        #If only file per char is selected
        elif not self.folderPerCharProp_CHKBOX.isChecked() and self.filePerCharProp_CHKBOX.isChecked():
            foldersDict = self.filePerCharPropFolders(roots, charPropInstances)
            self.filePerCharProp(_geoDict, _sampleBy, foldersDict, _bakingMethods)
        #If none is selected
        else:
            foldersDict = self.defaultFolders()
            self.filePerGeometry(_geoDict, _sampleBy, foldersDict, _bakingMethods)
        
        return foldersDict
    
    
    def folderPerCharProp(self, _roots, _charPropInstances):
        seq = str(self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.currentText())
        shot = str(self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentText())
        
        foldersDict = {}
        
        for each in _roots:
            rootName = each.name
            if self.customPathComp.customPath_CHKBOX.isChecked():
                self.baseMod.statusBar_update("Creating folders for: " + rootName, status=True)
                customPath = str(self.customPathComp.customPath_LINEEDIT.text())
                finalPathCache = customPath + "/" + each.dataValue(3)
                finalPathObjs = customPath + "/" + each.dataValue(3) + "/OBJs"
                finalPathIncludeGeo = customPath + "/" + each.dataValue(3) + "/includeGeo"
                finalPathVFX = customPath + "/" + each.dataValue(3) + "/VFX"
                foldersDict[str(id(each))] = [finalPathCache, finalPathObjs, finalPathIncludeGeo, finalPathVFX]
                if not os.path.exists(finalPathCache):
                    os.makedirs(finalPathCache)
                if self.exportObjs_CHKBOX.isChecked():
                    if not os.path.exists(finalPathObjs):
                        os.makedirs(finalPathObjs)
                if self.exportForVFX_CHKBOX.isChecked():
                    if not os.path.exists(finalPathVFX):
                        os.makedirs(finalPathVFX)
                if not os.path.exists(finalPathIncludeGeo):
                    os.makedirs(finalPathIncludeGeo)
            elif self.exportPose_CHKBOX.isChecked():
                self.baseMod.statusBar_update("Creating folders for: " + rootName, status=True)
                poseName = str(self.poseName_LINEEDIT.text())
                publishPath = self.buildPoseCachePath(poseName)
                finalPathCache = publishPath + "/" + each.dataValue(3)
                finalPathObjs = publishPath + "/" + each.dataValue(3) + "/OBJs"
                finalPathIncludeGeo = publishPath + "/" + each.dataValue(3) + "/includeGeo"
                finalPathVFX = publishPath + "/" + each.dataValue(3) + "/VFX"
                foldersDict[str(id(each))] = [finalPathCache, finalPathObjs, finalPathIncludeGeo, finalPathVFX]
                if not os.path.exists(finalPathCache):
                    os.makedirs(finalPathCache)
                if self.exportObjs_CHKBOX.isChecked():
                    if not os.path.exists(finalPathObjs):
                        os.makedirs(finalPathObjs)
                if self.exportForVFX_CHKBOX.isChecked():
                    if not os.path.exists(finalPathVFX):
                        os.makedirs(finalPathVFX)
                if not os.path.exists(finalPathIncludeGeo):
                    os.makedirs(finalPathIncludeGeo)
            else:
                self.baseMod.statusBar_update("Creating folders for: " + rootName, status=True)
                publishPath = self.buildProjSeqShotPath(seq, shot)
                finalPathCache = publishPath + "/" + each.dataValue(3)
                finalPathObjs = finalPathCache + "/OBJs"
                finalPathIncludeGeo = finalPathCache + "/includeGeo"
                finalPathVFX = self.buildVFXPath(seq, shot) + "/" + each.dataValue(3)
                foldersDict[str(id(each))] = [finalPathCache, finalPathObjs, finalPathIncludeGeo, finalPathVFX]
                if not os.path.exists(finalPathCache):
                    os.makedirs(finalPathCache)
                if self.exportObjs_CHKBOX.isChecked():
                    if not os.path.exists(finalPathObjs):
                        os.makedirs(finalPathObjs)
                if self.exportForVFX_CHKBOX.isChecked():
                    if not os.path.exists(finalPathVFX):
                        os.makedirs(finalPathVFX)
                if not os.path.exists(finalPathIncludeGeo):
                    os.makedirs(finalPathIncludeGeo)
        
        return foldersDict
    
    
    def filePerCharPropFolders(self, _roots, _charPropInstances):
        seq = str(self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.currentText())
        shot = str(self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentText())
        
        foldersDict = {}
        
        for each in _roots:
            rootName = each.name
            if self.customPathComp.customPath_CHKBOX.isChecked():
                self.baseMod.statusBar_update("Creating folders for: " + rootName, status=True)
                customPath = str(self.customPathComp.customPath_LINEEDIT.text())
                finalPathCache = customPath
                finalPathObjs = customPath + "/" + each.dataValue(3) + "/OBJs"
                finalPathIncludeGeo = customPath + "/includeGeo"
                finalPathVFX = customPath + "/VFX"
                foldersDict[str(id(each))] = [finalPathCache, finalPathObjs, finalPathIncludeGeo, finalPathVFX]
                if not os.path.exists(finalPathCache):
                    os.makedirs(finalPathCache)
                if self.exportObjs_CHKBOX.isChecked():
                    if not os.path.exists(finalPathObjs):
                        os.makedirs(finalPathObjs)
                if self.exportForVFX_CHKBOX.isChecked():
                    if not os.path.exists(finalPathVFX):
                        os.makedirs(finalPathVFX)
                if not os.path.exists(finalPathIncludeGeo):
                    os.makedirs(finalPathIncludeGeo)
            elif self.exportPose_CHKBOX.isChecked():
                self.baseMod.statusBar_update("Creating folders for: " + rootName, status=True)
                poseName = str(self.poseName_LINEEDIT.text())
                publishPath = self.buildPoseCachePath(poseName)
                finalPathCache = publishPath
                finalPathObjs = publishPath + "/OBJs"
                finalPathIncludeGeo = publishPath + "/includeGeo"
                finalPathVFX = publishPath + "/VFX"
                foldersDict[str(id(each))] = [finalPathCache, finalPathObjs, finalPathIncludeGeo, finalPathVFX]
                if not os.path.exists(finalPathCache):
                    os.makedirs(finalPathCache)
                if self.exportObjs_CHKBOX.isChecked():
                    if not os.path.exists(finalPathObjs):
                        os.makedirs(finalPathObjs)
                if self.exportForVFX_CHKBOX.isChecked():
                    if not os.path.exists(finalPathVFX):
                        os.makedirs(finalPathVFX)
                if not os.path.exists(finalPathIncludeGeo):
                    os.makedirs(finalPathIncludeGeo)
            else:
                self.baseMod.statusBar_update("Creating folders for: " + rootName, status=True)
                publishPath = self.buildProjSeqShotPath(seq, shot)
                finalPathCache = publishPath
                finalPathObjs = publishPath + "/OBJs"
                finalPathIncludeGeo = publishPath + "/includeGeo"
                finalPathVFX = self.buildVFXPath(seq, shot)
                foldersDict[str(id(each))] = [finalPathCache, finalPathObjs, finalPathIncludeGeo, finalPathVFX]
                if not os.path.exists(finalPathCache):
                    os.makedirs(finalPathCache)
                if self.exportObjs_CHKBOX.isChecked():
                    if not os.path.exists(finalPathObjs):
                        os.makedirs(finalPathObjs)
                if self.exportForVFX_CHKBOX.isChecked():
                    if not os.path.exists(finalPathVFX):
                        os.makedirs(finalPathVFX)
                if not os.path.exists(finalPathIncludeGeo):
                    os.makedirs(finalPathIncludeGeo)
            
        return foldersDict
    
    
    def defaultFolders(self):
        seq = str(self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.currentText())
        shot = str(self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentText())
        
        foldersDict = {}
        
        if self.customPathComp.customPath_CHKBOX.isChecked():
            customPath = str(self.customPathComp.customPath_LINEEDIT.text())
            customPathObjs = customPath + "/OBJs"
            customPathIncludeGeo = customPath + "/includeGeo"
            customPathVFX = customPath + "/VFX"
            if self.exportObjs_CHKBOX.isChecked():
                if not os.path.exists(customPath+"/OBJs"):
                    os.makedirs(customPath+"/OBJs")
            if self.exportForVFX_CHKBOX.isChecked():
                if not os.path.exists(customPathVFX):
                    os.makedirs(customPathVFX)
            if not os.path.exists(customPathIncludeGeo):
                os.makedirs(customPathIncludeGeo)
            foldersDict["custDef"] = [customPath, customPathObjs, customPathIncludeGeo, customPathVFX]
        elif self.exportPose_CHKBOX.isChecked():
            poseName = str(self.poseName_LINEEDIT.text())
            publishPath = self.buildPoseCachePath(poseName)
            finalPathCache = publishPath
            finalPathObjs = publishPath + "/OBJs"
            finalPathIncludeGeo = publishPath + "/includeGeo"
            finalPathVFX = publishPath + "/VFX"
            foldersDict["custDef"] = [finalPathCache, finalPathObjs, finalPathIncludeGeo, finalPathVFX]
            if not os.path.exists(finalPathCache):
                os.makedirs(finalPathCache)
            if self.exportObjs_CHKBOX.isChecked():
                if not os.path.exists(finalPathObjs):
                    os.makedirs(finalPathObjs)
            if self.exportForVFX_CHKBOX.isChecked():
                if not os.path.exists(finalPathVFX):
                    os.makedirs(finalPathVFX)
            if not os.path.exists(finalPathIncludeGeo):
                os.makedirs(finalPathIncludeGeo)
        else:
            publishPath = self.buildProjSeqShotPath(seq, shot)
            finalPathCache = publishPath
            finalPathObjs = publishPath + "/OBJs"
            finalPathIncludeGeo = publishPath + "/includeGeo"
            finalPathVFX = self.buildVFXPath(seq, shot)
            foldersDict["custDef"] = [finalPathCache, finalPathObjs, finalPathIncludeGeo, finalPathVFX]
            if not os.path.exists(finalPathCache):
                os.makedirs(finalPathCache)
            if self.exportObjs_CHKBOX.isChecked():
                if not os.path.exists(finalPathObjs):
                    os.makedirs(finalPathObjs)
            if self.exportForVFX_CHKBOX.isChecked():
                if not os.path.exists(finalPathVFX):
                    os.makedirs(finalPathVFX)
            if not os.path.exists(finalPathIncludeGeo):
                os.makedirs(finalPathIncludeGeo)
                    
        return foldersDict
    
    
    def filePerCharProp(self, _geoDict, _sampleBy, _foldersDict, _bakingMethods):
        #Bake geometry first, then export
        #Create a list of used roots
        rootList = []
        for e in _geoDict.keys():
            if _geoDict[e][2] not in rootList:
                rootList.append(_geoDict[e][2])
        metadataDict = {}
        metaID = 0
        #Loop through rootList and compare to geoDict to bake and export
        for rootNode in rootList:
            rootName = rootNode.name
            bakedGeomList = []
            for e in _geoDict.keys():
                if _geoDict[e][2] == rootNode:
                    #Bake geometry
                    bakedGeom = ""
                    self.baseMod.statusBar_update("Baking: " + e, status=True)
                    #Bake with vertex bake method
                    if self.cacheMod.CacheBakingMethods_Component.vertexBake_RBTN.isChecked():
                        bakedGeom = _bakingMethods.vertexBakeMethod(e)
                        bakedGeomList.append(bakedGeom)
                    elif self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                        bakedGeom = e
                        bakedGeomList.append(bakedGeom)
                        
            #Find includeGeo tags
            includeGeoTags = self.configFile.getValueByProperty('includeGeoTags', False)
            includeGeoBool = False
            for tag in includeGeoTags:
                if tag in rootName:
                    includeGeoBool = True
                    break
            
            rootNodeID = str(id(rootNode))
            metatag = rootNode.data[4]
            exporter = ''
            if includeGeoBool and not self.exportForVFX_CHKBOX.isChecked():
                exporter = self.appTools.createExporter(bakedGeomList, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict[rootNodeID][2], _sampleBy)
                metadataDict[metaID] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName,'instanceIdentifier':metatag.instanceIdentifier, 'cacheFolder':_foldersDict[rootNodeID][2]}
            elif self.exportForVFX_CHKBOX.isChecked():
                exporter = self.appTools.createExporter(bakedGeomList, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict[rootNodeID][3], _sampleBy)
                metadataDict[metaID] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName,'instanceIdentifier':metatag.instanceIdentifier, 'cacheFolder':_foldersDict[rootNodeID][3]}
            else:
                exporter = self.appTools.createExporter(bakedGeomList, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict[rootNodeID][0], _sampleBy)
                metadataDict[metaID] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName,'instanceIdentifier':metatag.instanceIdentifier, 'cacheFolder':_foldersDict[rootNodeID][0]}
            
            #Export baked geom list
            cleanName = rootName + "_bkd_1"
            if not self.folderPerCharProp_CHKBOX.isChecked() and self.filePerCharProp_CHKBOX.isChecked():
                cleanName = rootNode.dataValue(3)
            self.baseMod.statusBar_update("Writing: " + cleanName, status=True)
            if self.cacheMod.CacheFileTypes_Component.pointCache_RBTN.isChecked():
                exporter.exportPointCache(cleanName, True)
                if self.exportObjs_CHKBOX.isChecked():
                    exporter.exportObj(_foldersDict[rootNodeID][1])
            elif self.cacheMod.CacheFileTypes_Component.alembic_RBTN.isChecked():
                if self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                    exporter.exportAlembicBake(cleanName, True)
                else:
                    exporter.exportAlembic(cleanName, True)
            elif self.cacheMod.CacheFileTypes_Component.alembicExo_RBTN.isChecked():
                if self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                    if includeGeoBool or self.exoExportSurface_CHKBOX.isChecked() or self.exportForVFX_CHKBOX.isChecked():
                        exporter.exportAlembicBakeExo(cleanName, True, True)
                    else:
                        exporter.exportAlembicBakeExo(cleanName, True)
                else:
                    if includeGeoBool or self.exoExportSurface_CHKBOX.isChecked() or self.exportForVFX_CHKBOX.isChecked():
                        exporter.exportAlembicExo(cleanName, True, True)
                    else:
                        exporter.exportAlembicExo(cleanName, True)
                    
            #Delete baked geometry
            if not self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                mc.delete(bakedGeomList)
            
            #Increment metadataDict ID
            metaID += 1
        
        #Write json with metadata
        self.writeMetadata(metadataDict)
    
    
    def filePerGeometry(self, _geoDict, _sampleBy, _foldersDict, _bakingMethods):
        metadataDict = {}
        metaID = 0
        usedIDs = {}
        #Bake geometry and export at the same time
        for e in _geoDict.keys():
            #Get metanode to write metadata
            thisNode = _geoDict[e][2]
            parentNode = thisNode.parent
            metatag = parentNode.data[4]
            if parentNode.name not in usedIDs.keys():
                usedIDs[parentNode.name] = metaID
                metaID += 1
                
            bakedGeom = ""
            exporter = ""
            #Find includeGeo tags
            includeGeoTags = self.configFile.getValueByProperty('includeGeoTags', False)
            includeGeoBool = False
            for tag in includeGeoTags:
                if tag in _geoDict[e][1]:
                    includeGeoBool = True
                    break
            #Bake with vertex bake method
            if self.cacheMod.CacheBakingMethods_Component.vertexBake_RBTN.isChecked():
                self.baseMod.statusBar_update("Baking: " + e, status=True)
                bakedGeom = _bakingMethods.vertexBakeMethod(e)
                #Create output object
                self.baseMod.statusBar_update("Writing: " + e, status=True)
                if "custDef" in _foldersDict.keys():
                    if includeGeoBool and not self.exportForVFX_CHKBOX.isChecked():
                        exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict["custDef"][2], _sampleBy)
                        metadataDict[usedIDs[parentNode.name]] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName, 'instanceIdentifier':metatag.instanceIdentifier, 'cacheFolder':_foldersDict["custDef"][2]}
                    elif self.exportForVFX_CHKBOX.isChecked():
                        exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict["custDef"][3], _sampleBy)
                        metadataDict[usedIDs[parentNode.name]] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName, 'instanceIdentifier':metatag.instanceIdentifier,'cacheFolder':_foldersDict["custDef"][3]}
                    else:
                        exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict["custDef"][0], _sampleBy)
                        metadataDict[usedIDs[parentNode.name]] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName, 'instanceIdentifier':metatag.instanceIdentifier,'cacheFolder':_foldersDict["custDef"][0]}
                else:
                    rootNode = str(id(_geoDict[e][2]))
                    if includeGeoBool and not self.exportForVFX_CHKBOX.isChecked():
                        exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict[rootNode][2], _sampleBy)
                        metadataDict[usedIDs[parentNode.name]] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName, 'instanceIdentifier':metatag.instanceIdentifier,'cacheFolder':_foldersDict[rootNode][2]}
                    elif self.exportForVFX_CHKBOX.isChecked():
                        exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict[rootNode][3], _sampleBy)
                        metadataDict[usedIDs[parentNode.name]] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName, 'instanceIdentifier':metatag.instanceIdentifier,'cacheFolder':_foldersDict[rootNode][3]}
                    else:
                        exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict[rootNode][0], _sampleBy)
                        metadataDict[usedIDs[parentNode.name]] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName, 'instanceIdentifier':metatag.instanceIdentifier,'cacheFolder':_foldersDict[rootNode][0]}
            #Bake with normal alembic procedures (transforms when it feels it's convenient, with entire hierarchies)
            elif self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                self.baseMod.statusBar_update("Baking: " + e, status=True)
                bakedGeom = e
                #Create output object
                self.baseMod.statusBar_update("Writing: " + e, status=True)
                if "custDef" in _foldersDict.keys():
                    if includeGeoBool and not self.exportForVFX_CHKBOX.isChecked():
                        exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict["custDef"][2], _sampleBy)
                        metadataDict[usedIDs[parentNode.name]] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName, 'instanceIdentifier':metatag.instanceIdentifier,'cacheFolder':_foldersDict["custDef"][2]}
                    elif self.exportForVFX_CHKBOX.isChecked():
                        exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict["custDef"][3], _sampleBy)
                        metadataDict[usedIDs[parentNode.name]] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName, 'instanceIdentifier':metatag.instanceIdentifier,'cacheFolder':_foldersDict["custDef"][3]}
                    else:
                        exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict["custDef"][0], _sampleBy)
                        metadataDict[usedIDs[parentNode.name]] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName, 'instanceIdentifier':metatag.instanceIdentifier,'cacheFolder':_foldersDict["custDef"][0]}
                else:
                    rootNode = str(id(_geoDict[e][2]))
                    if includeGeoBool and not self.exportForVFX_CHKBOX.isChecked():
                        exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict[rootNode][2], _sampleBy)
                        metadataDict[usedIDs[parentNode.name]] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName, 'instanceIdentifier':metatag.instanceIdentifier,'cacheFolder':_foldersDict[rootNode][2]}
                    elif self.exportForVFX_CHKBOX.isChecked():
                        exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict[rootNode][3], _sampleBy)
                        metadataDict[usedIDs[parentNode.name]] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName, 'instanceIdentifier':metatag.instanceIdentifier,'cacheFolder':_foldersDict[rootNode][3]}
                    else:
                        exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, _foldersDict[rootNode][0], _sampleBy)
                        metadataDict[usedIDs[parentNode.name]] = {'assetType':metatag.assetType, 'assetName':metatag.assetName, 'subassetName': metatag.subassetName, 'instanceIdentifier':metatag.instanceIdentifier,'cacheFolder':_foldersDict[rootNode][0]}
            
            #Export
            if self.cacheMod.CacheFileTypes_Component.pointCache_RBTN.isChecked():
                exporter.exportPointCache()
                if self.exportObjs_CHKBOX.isChecked():
                    exporter.exportObj(_foldersDict[rootNode][1])
            elif self.cacheMod.CacheFileTypes_Component.alembic_RBTN.isChecked():
                cleanName = _geoDict[e][1] + "_" + _geoDict[e][0] + "_bkd"
                if self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                    exporter.exportAlembicBake(cleanName)
                else:
                    exporter.exportAlembic()
            elif self.cacheMod.CacheFileTypes_Component.alembicExo_RBTN.isChecked():
                cleanName = _geoDict[e][1] + "_" + _geoDict[e][0] + "_bkd"
                if self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                    if includeGeoBool or self.exoExportSurface_CHKBOX.isChecked() or self.exportForVFX_CHKBOX.isChecked():
                        exporter.exportAlembicBakeExo(cleanName, False, True)
                    else:
                        exporter.exportAlembicBakeExo(cleanName)
                else:
                    if includeGeoBool or self.exoExportSurface_CHKBOX.isChecked() or self.exportForVFX_CHKBOX.isChecked():
                        exporter.exportAlembicExo(cleanName, False, True)
                    else:
                        exporter.exportAlembicExo(cleanName)
                    
            #Delete baked geometry
            if not self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                #Delete clusters list
                clustersList = _bakingMethods.getClustersList()
                mc.delete(clustersList)
                mc.delete(bakedGeom)
                _bakingMethods.resetGeomClusters()
        
        #Write json with metadata
        self.writeMetadata(metadataDict)
                
    
    def writeMetadata(self, _metadataDict):
        jsonFolder = self.defaultFolders()["custDef"][0]
        jsonMger = JSONManager()
        if os.path.exists(jsonFolder + '/metadata.json'):
            jsonMger.fileName = jsonFolder
            jsonMger.parseJsonObj(jsonFolder + '/metadata.json')
            
            existingInstances = []
            for dictid in jsonMger.jsonObj.keys():
                existingInstances.append(jsonMger.jsonObj[dictid]['instanceIdentifier'])
            
            for key in _metadataDict.keys():
                for dictid in jsonMger.jsonObj.keys():
                    if jsonMger.jsonObj[dictid]['assetName'] == _metadataDict[key]['assetName'] and jsonMger.jsonObj[dictid]['subassetName'] == _metadataDict[key]['subassetName']:
                        if jsonMger.jsonObj[dictid]['instanceIdentifier'] == _metadataDict[key]['instanceIdentifier']:
                            jsonMger.jsonObj[dictid] = _metadataDict[key]
                            break
                        elif jsonMger.jsonObj[dictid]['instanceIdentifier'] != _metadataDict[key]['instanceIdentifier'] and _metadataDict[key]['instanceIdentifier'] not in existingInstances:
                            jsonMger.jsonObj[len(jsonMger.jsonObj.keys())] = _metadataDict[key]
                            break
                        
            result = jsonMger.writeJson(jsonMger.jsonObj, jsonFolder + '/metadata.json')
            if result:
                self.baseMod.statusBar_update("Wrote metadata to {}".format(jsonFolder), success=True)
            else:
                self.baseMod.statusBar_update("There was a problem writing the metadata to {}".format(jsonFolder), alert=True)
        else:
            result = jsonMger.writeJson(_metadataDict, jsonFolder + '/metadata.json')
            if result:
                self.baseMod.statusBar_update("Wrote metadata to {}".format(jsonFolder), success=True)
            else:
                self.baseMod.statusBar_update("There was a problem writing the metadata to {}".format(jsonFolder), alert=True)
        
    
    def buildPoseCachePath(self, _poseName="EstaPose"):
        propertiesDict = {"abstractFolderType":"leaffolder",
                          "concreteFolderType":"published_leaffolder",
                          "folderFunction":"published_content",
                          "folderConcreteFunction":"published_pose",
                          "placeholder":False}
        publishedPoseNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)[0]
        if publishedPoseNode != None:
            poseNameDict={"name":"$poseName",
                        "concreteFolderType":"asset_subfolder",
                        "abstractFolderType":"subfolder",
                        "folderFunction":"work_content",
                        "folderConcreteFunction":"pose_folder",
                        "placeholder":True}
            pathList = self.pInfo.rebuildPath(publishedPoseNode, {_poseName:poseNameDict})
            pathList.append(publishedPoseNode.name)
            
            #Build path string
            server = self.sInfo.getProjectsFrom()
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
            
            return pathStr
        else:
            return None
    
    
    def buildProjSeqShotPath(self, _seq, _shot):
        propertiesDict = {"abstractFolderType":"leaffolder",
                          "concreteFolderType":"published_leaffolder",
                          "folderFunction":"published_content",
                          "folderConcreteFunction":"published_cache",
                          "placeholder":False}
        cacheNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)[0]
        if cacheNode != None:
            seqDict={"name":"$SEQ_##",
                    "abstractFolderType":"subfolder",
                    "concreteFolderType":"sequence_subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
            shotDict={"name":"$s_###",
                    "abstractFolderType":"subfolder",
                    "concreteFolderType":"shot_subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
            pathList = self.pInfo.rebuildPath(cacheNode, {_seq:seqDict, _shot:shotDict})
            pathList.append(cacheNode.name)
            
            #Build path string
            server = self.sInfo.getProjectsFrom()
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
            
            return pathStr
        else:
            return None
    
    
    def buildVFXPath(self, _seq, _shot):
        propertiesDict = {"abstractFolderType":"branchfolder",
                          "concreteFolderType":"published_branchfolder",
                          "folderFunction":"published_content",
                          "folderConcreteFunction":"publishedVFX_cache",
                          "placeholder":False}
        cacheNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)[0]
        if cacheNode != None:
            seqDict={"name":"$SEQ_##",
                    "abstractFolderType":"subfolder",
                    "concreteFolderType":"sequence_subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
            shotDict={"name":"$s_###",
                    "abstractFolderType":"subfolder",
                    "concreteFolderType":"shot_subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
            pathList = self.pInfo.rebuildPath(cacheNode, {_seq:seqDict, _shot:shotDict})
            pathList.append(cacheNode.name)
            
            #Build path string
            server = self.sInfo.getProjectsFrom()
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
            
            return pathStr
        else:
            return None
            
    
    def addTreeItem (self, item, itemType, internalName, metatag=None):
        if itemType == "root":
            instID = metatag.instanceIdentifier
            if instID in ["",None]:
                instID = ""
            return self.charsProps_TREEVIEW.model().insertRows(self.charsProps_TREEVIEW.model().dataList.childCount(), 1,[item,itemType,item,instID,metatag])
        elif itemType == "geo":
            selected = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
            if selected[0].isValid():
                indexNode = self.charsProps_TREEVIEW.model().getNode(selected[0])
                return self.charsProps_TREEVIEW.model().insertRows(indexNode.childCount(), 1, [item,itemType,internalName,"None",metatag], selected[0])
    
    
    def populateTree(self):
        """To populate the Characters/Props tree with the scene pre-tagged geometry"""
        
        metaTags = r9Meta.getMetaNodes(['CGXCacheMetaTag'])
        if len(metaTags) >= 1:
            for metatag in metaTags:
                #Create root item
                rootname = metatag.assetName
                if metatag.subassetName not in ["",None]:
                    rootname = metatag.assetName + "_" + metatag.subassetName
                charProp = self.addTreeItem(rootname, "root", rootname, metatag)
                #Create geo item
                for child in metatag.getChildren(mAttrs='taggedNodes'):
                    #Remove namespace if exists
                    childCleanName = child
                    if '|' in childCleanName:
                        childCleanName = childCleanName.rsplit('|',1)[1]
                    if mc.referenceQuery(child, isNodeReferenced= True):
                        thisNamespace = mc.referenceQuery(child, namespace= True, shortName= True)
                        if thisNamespace != "":
                            childCleanName = childCleanName[len(thisNamespace)+1:]
                    self.charsProps_TREEVIEW.model().insertRows(charProp.childCount(), 1,[childCleanName,"geo",child,"None", metatag], self.charsProps_TREEVIEW.model().indexFromNode(charProp))
            self.populateInstanceNames()
        else:
            self.baseMod.statusBar_update("The scene doesn't have any tagged Character/Prop.", status=True)
    
    
    def populateInstanceNames(self):
        roots = self.charsProps_TREEVIEW.model().dataList.children
        if not self.useTags_CHKBOX.isChecked():
            roots = []
            selected = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
            if len(selected) >= 1:
                for item in selected:
                    if item.isValid():
                        thisNode = item.internalPointer()
                        if thisNode.data[1] == "root":
                            roots.append(thisNode)
                        elif thisNode.data[1] == "geo":
                            parentNode = thisNode.parent
                            if parentNode not in roots:
                                roots.append(parentNode)
        rootNames = []
        for each in roots:
            rootNames.append(each.name)
        rootNamesSet = set(rootNames)
        rootNamesClean = list(rootNamesSet)
        charPropInstances = {}
        for each in rootNamesClean:
            charPropCount = rootNames.count(each)
            charPropInstances[each] = [charPropCount,1]#Dict con nombre del char/prop y numero de repeticiones
        
        for each in roots: 
            folderName = each.data[3]
            rootName = each.name
            prevList = charPropInstances[rootName]
            if folderName in [None,"","None"]:
                folderName = rootName + "_bkd_" + str(prevList[1])
                each.setData(3,folderName)
                metaNode = each.data[4]
                metaNode.instanceIdentifier = folderName
            
            prevList[1] += 1
            charPropInstances[rootName] = prevList
    
    
    def refreshTree(self):
        #Set Chars/Props model
        headers = ["Characters/Props", "Root/Geo", "internalName", "instanceName", "metaNode"]
        charsPropsRoot = dvm.TreeNode(["Characters/Props", "Root/Geo", "internalName", "instanceName","metaNode"], headers, parent=None)
        self.charsProps_TREEVIEW.reset()
        charsPropsModel = CharPropsDataTreeModel(charsPropsRoot,headers, self.charsProps_TREEVIEW)
        self.charsProps_TREEVIEW.setModel(charsPropsModel)
        self.charsProps_TREEVIEW.hideColumn(1)
        self.charsProps_TREEVIEW.hideColumn(2)
        self.charsProps_TREEVIEW.hideColumn(4)
        for col in range(charsPropsModel.columnCount()):
            self.charsProps_TREEVIEW.resizeColumnToContents(col)
        self.charsProps_TREEVIEW.setSelectionMode(self.charsProps_TREEVIEW.MultiSelection)
        self.populateTree()
        self.refreshCustomFolderName()
    
    
    def refreshCustomFolderName(self):
        if self.customFolderName_CHKBOX.isChecked():
            self.charsProps_TREEVIEW.showColumn(3)
            for col in range(self.charsProps_TREEVIEW.model().columnCount()):
                self.charsProps_TREEVIEW.resizeColumnToContents(col)
        else:
            self.charsProps_TREEVIEW.hideColumn(3)
            for col in range(self.charsProps_TREEVIEW.model().columnCount()):
                self.charsProps_TREEVIEW.resizeColumnToContents(col)
    
    
    def refreshUseTagsOnly(self):
        if self.useTags_CHKBOX.isChecked():
            self.charsProps_TREEVIEW.setEnabled(False)
            self.refreshTree()
        else:
            self.charsProps_TREEVIEW.setEnabled(True)
    
    
    def refreshExportPose(self):
        if self.exportPose_CHKBOX.isChecked():
            self.projSeqShotMod.Project_Component.container.setEnabled(True)
            self.projSeqShotMod.Sequence_Component.container.setEnabled(False)
            self.projSeqShotMod.Shot_Component.container.setEnabled(False)
            self.customPathComp.container.setEnabled(False)
            self.customPathComp.customPath_CHKBOX.setChecked(False)
            self.poseName_LINEEDIT.setEnabled(True)
        else:
            self.projSeqShotMod.Sequence_Component.container.setEnabled(True)
            self.projSeqShotMod.Shot_Component.container.setEnabled(True)
            self.customPathComp.container.setEnabled(True)
            self.poseName_LINEEDIT.setEnabled(False)
    
    
    def refreshFolderPerCharProp(self):
        if self.folderPerCharProp_CHKBOX.isChecked():
            self.customFolderName_CHKBOX.setEnabled(True)
        else:
            self.customFolderName_CHKBOX.setEnabled(False)
            self.customFolderName_CHKBOX.setChecked(False)
        self.refreshCustomFolderName()
        
        
    def refreshExportForVFX(self):
        if self.exportForVFX_CHKBOX.isChecked():
            self.timeRangeMod.SampleBy_Component.sampleBy_DBLSPINBOX.setValue(1)
        else:
            self.timeRangeMod.SampleBy_Component.sampleBy_DBLSPINBOX.setValue(0.25)
            
            
    def alembicVersionDialog(self):
        alembicVerDialog = AlembicVersion_DialogGUI(self)
        alembicVerDialog.show()
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods for Characters/Props tree view
    ##--------------------------------------------------------------------------------------------     
    def charsPropsOptions (self,pos):
        """Method that creates the popupmenu"""
        menu = QtWidgets.QMenu(self.charsProps_TREEVIEW)
        addCharPropQ = menu.addAction("Add Char/Prop")
        addGeoTagQ = menu.addAction("Add Geo")
        removeItemQ = menu.addAction("Remove Item")
        selectAllQ = menu.addAction("Select All")
        selectgGeoQ = menu.addAction("Select Geo")
        selectMetaNodeQ = menu.addAction("Select Meta Node")
        refreshTreeQ = menu.addAction("Refresh tree")
        menu.popup(self.charsProps_TREEVIEW.mapToGlobal(pos))
        
        addCharPropQ.triggered.connect(self.addCharProp)
        addGeoTagQ.triggered.connect(self.tagGeo)
        removeItemQ.triggered.connect(self.removeTag)
        selectAllQ.triggered.connect(self.selectAllTree)
        selectgGeoQ.triggered.connect(self.selectGeoForItem)
        selectMetaNodeQ.triggered.connect(self.selectMetaNode)
        refreshTreeQ.triggered.connect(self.refreshTree)
    
    
    def addCharProp(self):
        metatag = cgxMeta.CGXCacheMetaTag(name='proxyChar')
        metatag.assetType = 'Custom'
        self.addTreeItem("Type Character/Prop name here", "root", "None", metatag)
    
    
    def tagGeo(self):
        allSel = mc.ls(sl= True, long=True)
        if len(allSel) <= 0:
            self.baseMod.statusBar_update("Nothing is selected. Select at least one node to be tagged.", status=True)
        else:
            existingMetaTags = r9Meta.getMetaNodes(['CGXCacheMetaTag'])
            usedGeom = []
            for tag in existingMetaTags:
                usedGeom += tag.getChildren(mAttrs='taggedNodes')
            for item in allSel:
                if mc.nodeType(item) in ['transform']:
                    if usedGeom.count(item) == 0:
                        selectedRootItem = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
                        if selectedRootItem != None:
                            itemShape = mc.listRelatives(item , shapes = True, noIntermediate = True, fullPath=True)[0]
                            thisNodeType = mc.nodeType(itemShape)
                            if thisNodeType in ["mesh", "nurbsSurface", "subdiv"]:
                                if len(selectedRootItem) >= 1:
                                    #Add item to tree view and metatag
                                    indexNode = self.charsProps_TREEVIEW.model().getNode(selectedRootItem[0])
                                    metatag = indexNode.data[4]
                                    metatag.connectChildren([item], 'taggedNodes')
                                    shortName = item.rsplit("|",1)[1]
                                    self.addTreeItem(shortName, "geo", item, metatag)
                                else:
                                    self.baseMod.statusBar_update("You must select one Character/Prop item to add this geo to.", status=True)
                            else:
                                self.baseMod.statusBar_update(item + " is not a valid node.", alert=True)
                        else:
                            self.baseMod.statusBar_update("Select a root node in the list.", alert=True)
                    else:
                        self.baseMod.statusBar_update(item + " has already been tagged.", alert=True)
                else:
                    self.baseMod.statusBar_update("Select only the transform nodes of all the geometries you want to tag.", alert=True)
    
    
    def removeTag(self):
        selected = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
        if len(selected) >= 1:
            skipItems = []
            for item in selected:
                if item.isValid():
                    tagType= self.charsProps_TREEVIEW.model().getNode(item).data
                    metaNode = tagType[4]
                    if tagType[1] == 'root':
                        if mc.referenceQuery(metaNode.mNode, isNodeReferenced= True):
                            skipItems.append(item)
                            self.baseMod.statusBar_update("The metatag node is referenced. You must remove referenced tags from the referenced file.", alert=True)
                        else:
                            for child in metaNode.getChildren():
                                metaNode.disconnectChild(child)
                            metaNode.delete()
                    elif tagType[1] == 'geo':
                        geoName = self.charsProps_TREEVIEW.model().getNode(item).data[2]
                        if mc.referenceQuery(geoName, isNodeReferenced= True):
                            self.baseMod.statusBar_update(geoName + " is referenced. It'll be removed from this tag just for this project.", alert=True)
                        metaNode.disconnectChild(geoName)
            #Remove from UI
            removeList = [x for x in selected if x not in skipItems]
            self.charsProps_TREEVIEW.model().removeRows(removeList)
                        

    def selectAllTree(self):
        self.charsProps_TREEVIEW.selectAll()
    
    
    def selectGeoForItem(self):
        selected = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
        mc.select(clear=True)
        if len(selected) >= 1:
            for e in range(len(selected)):
                item = selected[e]
                if item.isValid():
                    tagType= self.charsProps_TREEVIEW.model().getNode(item).data
                    if tagType[1] == "root":
                        children = self.charsProps_TREEVIEW.model().getNode(item).children
                        for x in children:
                            geoName = x.data[2]
                            mc.select(geoName, add = True)
                    elif tagType[1] == "geo":
                        geoName = item.internalPointer().data[2]
                        mc.select(geoName, add = True)
                        
    
    def selectMetaNode(self):
        selected = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
        metatags = []
        if len(selected) >= 1:
            for each in selected:
                treeNode = self.charsProps_TREEVIEW.model().getNode(each)
                metatags.append(treeNode.data[4].mNode)
        mc.select(metatags, replace=True)
    
    
    def filterSelected(self, _indexes):
        '''
        Given a list of indexes, return only one index per row.
        '''
        if len(_indexes) > 0:
            #Group by parent first
            filterDict = {}
            for each in _indexes:
                if each.parent() not in filterDict.keys():
                    filterDict[each.parent()] = [each]
                else:
                    filterDict[each.parent()].append(each)
            #Delete redundancies
            filteredList = []
            for key in filterDict.keys():
                usedRows = []
                for item in filterDict[key]:
                    if item.row() not in usedRows:
                        usedRows.append(item.row())
                        filteredList.append(item)
            
            return filteredList
        else:
            return _indexes


##--------------------------------------------------------------------------------------------
##Class: Tree Data Model reimplementation for Chars/Props tree
##--------------------------------------------------------------------------------------------
class CharPropsDataTreeModel(dvm.DataTreeModel):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, root, headers, parent):
        '''
        DataTreeModel for Model/View programming.
        :param root: root TreeNode for this model.
        :type root: TreeNode
        :param headers: list of headers to be displayed in the table. Must be same len as data passed to rootnode
        :type headers: list
        :param parent: QWidget that uses this model. Default=None
        :type parent: QWidget
        '''
        super(CharPropsDataTreeModel, self).__init__(root, headers, parent)
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def flags(self, index):
        '''
        Set flags for each item.  Re-implement if different flags are needed and use conditional chains if per-item flags are needed.
        :param index: Item to be filtered
        :type index: QModelIndex
        :return: Flags to be used
        :rtype: QtCore.Qt.Flags
        '''
        text = index.model().data(index, QtCore.Qt.DisplayRole)
        if text in ["None","geo","root"] and index.column() in [1,2,3]:
            return QtCore.Qt.NoItemFlags
        else:
            return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
    
    
    def setData(self, index, value, role=QtCore.Qt.EditRole):
        '''
        Set data for specified index and display it.
        :param index: Index to be edited
        :type index: QModelIndex
        :param role: Edit role as default. Reimplement with elif chain for another role.
        :type role: QtCore.Qt.Role
        :return: Item data to be shown in the GUI. Might be None.
        :rtype: object
        '''
        if role != QtCore.Qt.EditRole:
            return False
        else:
            node = self.getNode(index)
            result = node.setData(index.column(),value)
            if result:
                metaNode = node.data[4]
                if index.column() == 3:
                    metaNode.instanceIdentifier = value
                if index.column() == 0:
                    metaNode.assetName = value
                    metaNode.subassetName = ""
                    if node.dataValue(3) in ["None","",None] and node.dataValue(1) == "root":
                        node.setData(3,value)
                        metaNode.instanceIdentifier = value
                self.dataChanged.emit(index, index)
    
            return result

    
##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent):
        '''
        Creates Help Dialog for current tool.
        :param parent: Main tools window.
        :type parent: QMainWindow
        '''
        super(Help_DialogGUI,self).__init__(parent)
        self.setupUi(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent):
        '''
        Creates About Dialog for current tool.
        :param parent: Main tools window.
        :type parent: QMainWindow
        '''
        super(About_DialogGUI,self).__init__(parent)
        self.setupUi(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent):
        '''
        Creates Config Dialog for current tool.
        :param parent: Main tools window.
        :type parent: QMainWindow
        '''
        super(Config_DialogGUI,self).__init__(parent)
        self.setupUi(self)
        
        self.configFile = JSONManager(config_json, self)
        
        self.initUI()
        self.setConnections()
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def setConnections(self):
        #SIGNALS
        self.cancel_BTN.clicked.connect(self.cancelAll)
        self.apply_BTN.clicked.connect(self.applyConfig)
        
        self.includeGeo_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.includeGeo_LISTVIEW.customContextMenuRequested.connect(self.includeGeoOptions)
        
    
    def initUI(self):
        """Read JSON config file"""
        
        includeGeoTagsList = self.configFile.getValueByProperty('includeGeoTags', False)
        includeGeoTagsModel = ConfigObjectsListModel(includeGeoTagsList, self)
        self.includeGeo_LISTVIEW.setModel(includeGeoTagsModel)
        
    
    def applyConfig(self):
        """Apply changes to JSON file"""
        
        jsonObj = self.configFile.jsonObj
        newTags = self.includeGeo_LISTVIEW.model().dataList
        newTagsSet = set(newTags)
        jsonObj['includeGeoTags'] = list(newTagsSet)
        self.configFile.writeJson(jsonObj, self.configFile.fileName)
        
        self.done(1)
    
    
    def cancelAll(self):
        """Method to close the UI"""
        
        self.done(0)
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods for include geo tags list
    ##--------------------------------------------------------------------------------------------
    def includeGeoOptions (self,pos):
        """Method that creates the popupmenu"""
        menu = QtWidgets.QMenu(self.includeGeo_LISTVIEW)
        addIncludeGeoTagQ = menu.addAction("Add include geometry tags")
        deleteIncludeGeoTagQ = menu.addAction("Delete include geometry tags")
        menu.popup(self.includeGeo_LISTVIEW.mapToGlobal(pos))
        
        addIncludeGeoTagQ.triggered.connect(self.addIncludeGeoTagOpt)
        deleteIncludeGeoTagQ.triggered.connect(self.deleteIncludeGeoTagOpt)
    
    
    def addIncludeGeoTagOpt(self):
        self.includeGeo_LISTVIEW.model().insertRows(self.includeGeo_LISTVIEW.model().rowCount(), ["newValue"])
    
    
    def deleteIncludeGeoTagOpt(self):
        selectedIndex = self.includeGeo_LISTVIEW.currentIndex().row()
        if len(self.includeGeo_LISTVIEW.model().dataList) == 1:
            msgBox = QtWidgets.QMessageBox(self)
            msgBox.setWindowTitle("Warning!")
            msgBox.setText("There has to be at least one value in the list.")
            msgBox.exec_()
        else:
            self.includeGeo_LISTVIEW.model().removeRows([selectedIndex], 1)


##--------------------------------------------------------------------------------------------
##Class: Objects lists model
##--------------------------------------------------------------------------------------------
class ConfigObjectsListModel(dvm.ObjectsListModel):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, thisList, parent):
        super(ConfigObjectsListModel, self).__init__(thisList, parent)
        
        
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def flags(self, index):
        """Set flags of each cell"""
        return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable


##--------------------------------------------------------------------------------------------
##Create alembic dialog
##--------------------------------------------------------------------------------------------
class AlembicVersion_DialogGUI(alembicVersionform, alembicVersionbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent):
        '''
        Creates Config Dialog for current tool.
        :param parent: Main tools window.
        :type parent: QMainWindow
        '''
        super(AlembicVersion_DialogGUI,self).__init__(parent)
        self.setupUi(self)
        
        self.configFile = JSONManager(alembicVersion_json, self)
        
        self.initUI()
        self.setConnections()
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def setConnections(self):
        #SIGNALS
        self.cancel_BTN.clicked.connect(self.cancelAll)
        self.apply_BTN.clicked.connect(self.applyConfig)
        
    
    def initUI(self):
        """Read JSON config file"""
        
        version = self.configFile.getValueByProperty('alembicVersion', False)
        if version == 'hdf5':
            self.hdf5_RADIOBTN.setChecked(True)
            self.ogawa_RADIOBTN.setChecked(False)
        elif version == 'ogawa':
            self.hdf5_RADIOBTN.setChecked(False)
            self.ogawa_RADIOBTN.setChecked(True)
        
    
    def applyConfig(self):
        """Apply changes to JSON file"""
        
        jsonObj = self.configFile.jsonObj
        version = 'hdf5'
        if self.ogawa_RADIOBTN.isChecked():
            version = 'ogawa'
        jsonObj['alembicVersion'] = version
        self.configFile.writeJson(jsonObj, self.configFile.fileName)
        
        self.done(1)
    
    
    def cancelAll(self):
        """Method to close the UI"""
        
        self.done(0)
        
            
##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    if __DEVMODE:
        x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/pipeline/CacheExporter_dev.py','r')
        exec x.read()
        x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
    main()