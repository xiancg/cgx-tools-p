# -*- coding: utf-8 -*-
'''
Asset publisher.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os, time, socket
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets, QtCore, QtGui
from cgx.gui.Qt import __binding__
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
from cgx.core.Synchronizer import Synchronizer
from cgx.Maya.scripts.maya_libs.Maya_TimeRange import Maya_TimeRange
from cgx.Maya.scripts.maya_libs.Maya_Dependencies import Maya_Dependencies
import cgx.gui.DataViewModels as dvm
import Red9.core.Red9_Meta as r9Meta
import cgx.Maya.scripts.maya_libs.Maya_MetaData as cgxMeta
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityAnimation import SanityAnimation
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityHair import SanityHair
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityLighting import SanityLighting
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityLookdev import SanityLookdev
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityModeling import SanityModeling
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityRigging import SanityRigging
import cgx.Maya.scripts.pipeline.AssetPublisher.AssetPublisher_icons


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/pipeline/AssetPublisher'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.setConnections()
			self.initComponents()
			self.initUI()
			
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
        Connect signals.
        '''
		#SIGNALS
		self.projAssetsMod.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.init_seqShotMod)
		self.projAssetsMod.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.populatePublished)
		self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentIndexChanged.connect(self.populatePublished)
		self.projAssetsMod.FileTypes_Component.fileType_COMBOBOX.currentIndexChanged.connect(self.populatePublished)
		self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentIndexChanged.connect(self.populatePublished)
		self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentIndexChanged.connect(self.populatePublished)
		self.dontpublish_share.triggered.connect(self.shareOnly)
		self.publishAsset_BTN.clicked.connect(self.preSanityCheck)
		#REFRESH
		self.publishDeps_CHKBOX.clicked.connect(self.refreshPublishDependencies)
		self.publishAllDeps_CHKBOX.clicked.connect(self.refreshPublishAll)
		self.shotPublish_CHKBOX.clicked.connect(self.refreshShotPublish)
		self.customSuffix_CHKBOX.clicked.connect(self.refreshCustomSuffix)
		#STATUS
		self.seqShotMod.container.setEnabled(False)
		
		#CONTEXT MENUS
		self.dependencies_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.dependencies_TABLEVIEW.customContextMenuRequested.connect(self.depsOptions)

	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Asset Publisher"
		self.__toolVersion = "2.0"
		
		self.tRange = Maya_TimeRange()
		self.syncStatusDict = {1:"Already published",
							2:"Published but different",
							3:"Same date, different size",
							4:"Different date, same size",
							5:"Both files missing",
							6:"Not Published",
							7:"Current file missing"}
		
		
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.baseMod.MainMenu_Component.edit_config.setVisible(False)
		self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo, tRange=self.tRange)
		self.projAssetsMod = self.Pipeline_GUIModulesFactory.createModule('ProjAssets')
		self.projAssetsMod.pos = [10,35]
		self.customPathComp = pipeComponents.CustomPath_Component(self,self.centralWidget(), self.sInfo)
		self.customPathComp.pos = [10,276]
		self.customPathComp.fileMode = self.customPathComp.browseDialog.FileMode.Directory
		self.seqShotMod = self.Pipeline_GUIModulesFactory.createModule('SeqShot')
		self.seqShotMod.pos = [10,218]
		self.fpsComp = pipeComponents.FPS_Component(self,self.centralWidget(), self.pInfo)
		self.fpsComp.pos = [276,40]
		#Add Dont publish just share menu option
		self.dontpublish_share = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.dontpublish_share.setObjectName("edit_dontpublish_share")
		self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[0], self.dontpublish_share)
		className= str(self.__class__.__name__)
		#Add debugMode option
		self.debugMode = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.debugMode.setCheckable(True)
		self.debugMode.setObjectName("edit_debugMode")
		self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[0], self.debugMode)
		if __binding__ in ('PySide', 'PyQt4'):
			self.dontpublish_share.setText(QtWidgets.QApplication.translate(className, "Don't publish, just share", None, QtWidgets.QApplication.UnicodeUTF8))
			self.debugMode.setText(QtWidgets.QApplication.translate(className, "Debug Mode", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			self.dontpublish_share.setText(QtWidgets.QApplication.translate(className, "Don't publish, just share", None))
			self.debugMode.setText(QtWidgets.QApplication.translate(className, "Debug Mode", None))
	
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
		self.appTools.setCurrentProject(self.projAssetsMod.Project_Component)
		self.appTools.setCurrentAssetType(self.projAssetsMod.AssetTypes_Component)
		self.appTools.setCurrentAsset(self.projAssetsMod.Assets_Component)
		self.appTools.setCurrentSubAsset(self.projAssetsMod.SubAssets_Component)
		self.appTools.setCurrentFileType(self.projAssetsMod.FileTypes_Component)
		self.seqShotMod.initComponents()
		self.appTools.setCurrentSequence(self.seqShotMod.Sequence_Component)
		self.appTools.setCurrentShot(self.seqShotMod.Shot_Component)
		self.fpsComp.initComponent(self.pInfo.maya_getFPS())
		self.appTools.checkFPS(self.fpsComp)
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		self.pupulateFileData()
		self.populateDependencies()
		self.populatePublished()
	
	
	def init_seqShotMod(self):
		self.seqShotMod.initComponents()
	
	
	def checkPlugins(self):
		'''
        Check if plugins are loaded.
        :return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
        :rtype: dict
        '''
		checkDict = self.appTools.checkPlugins(self)
		return checkDict
	
	
	def preSanityCheck(self):
		totalIssues, finalDataList = self.sanityCheck(self.projAssetsMod.FileTypes_Component.fileType_COMBOBOX.currentText().split(":")[1])
		if totalIssues >= 1:
			self.checkListDialog(finalDataList, totalIssues, self)
		else:
			self.publishAsset()
	
	
	def sanityCheck (self, _fileType):
		"""Runs the sanity check modules over the current file"""
		
		checkSelectedOnly = False
		checkNodes = []
		if self.selectedNodesOnly_CHKBOX.isChecked():
			checkSelectedOnly = True
			checkNodes = mc.ls(sl= True)
		
		finalDataList = []
		totalIssues = 0
		if _fileType in ["MOD"]:
			self.checkModeling = SanityModeling([], checkNodes, checkSelectedOnly)
			checkModelingResult =  self.checkModeling.check(self.checkModeling.checkMethods, checkNodes, checkSelectedOnly)
			totalIssues = checkModelingResult[1]
			if totalIssues >= 1:
				finalDataList = checkModelingResult[0]
		elif _fileType in ["SHD"]:
			self.checkModeling = SanityModeling([], checkNodes, checkSelectedOnly)
			self.checkLookdev = SanityLookdev([], checkNodes, checkSelectedOnly)
			checkModelingResult =  self.checkModeling.check(self.checkModeling.checkMethods, checkNodes, checkSelectedOnly)
			checkLookdevResult = self.checkLookdev.check(self.checkLookdev.checkMethods, checkNodes, checkSelectedOnly)
			totalIssues = checkModelingResult[1] + checkLookdevResult[1]
			if totalIssues >= 1:
				finalDataList = checkModelingResult[0] + checkLookdevResult[0]
		elif _fileType in ["HAR"]:
			self.checkModeling = SanityModeling([], checkNodes, checkSelectedOnly)
			self.checkLookdev = SanityLookdev([], checkNodes, checkSelectedOnly)
			self.checkHair = SanityHair([], checkNodes, checkSelectedOnly)
			checkModelingResult =  self.checkModeling.check(self.checkModeling.checkMethods, checkNodes, checkSelectedOnly)
			checkLookdevResult = self.checkLookdev.check(self.checkLookdev.checkMethods, checkNodes, checkSelectedOnly)
			checkHairResult = self.checkHair.check(self.checkHair.checkMethods, checkNodes, checkSelectedOnly)
			totalIssues = checkModelingResult[1] + checkLookdevResult[1] + checkHairResult[1]
			if totalIssues >= 1:
				finalDataList = checkModelingResult[0] + checkLookdevResult[0] + checkHairResult[0]
		elif _fileType in ["LGT"]:
			self.checkLighting = SanityLighting([], checkNodes, checkSelectedOnly)
			checkLightingResult = self.checkLighting.check(self.checkLighting.checkMethods, checkNodes, checkSelectedOnly)
			totalIssues = checkLightingResult[1]
			if totalIssues >= 1:
				finalDataList = checkLightingResult[0]
		elif _fileType in ["ANI"]:
			self.checkAnimation = SanityAnimation([], checkNodes, checkSelectedOnly)
			checkAnimationResult = self.checkAnimation.check(self.checkAnimation.checkMethods, checkNodes, checkSelectedOnly)
			totalIssues = checkAnimationResult[1]
			if totalIssues >= 1:
				finalDataList = checkAnimationResult[0]
		elif _fileType in ["RIG"]:
			self.checkModeling = SanityModeling([], checkNodes, checkSelectedOnly)
			self.checkRigging = SanityRigging([], checkNodes, checkSelectedOnly)
			checkModelingResult =  self.checkModeling.check(self.checkModeling.checkMethods, checkNodes, checkSelectedOnly)
			checkRiggingResult = self.checkRigging.check(self.checkRigging.checkMethods, checkNodes, checkSelectedOnly)
			totalIssues = checkModelingResult[1] + checkRiggingResult[1]
			if totalIssues >= 1:
				finalDataList = checkModelingResult[0] + checkRiggingResult[0]
		elif _fileType in ["LAY", "TRK", "VFX", "SIM"]:
			pass
		
		return totalIssues, finalDataList
	
	
	def publishAllDepsCheck(self):
		if self.publishDeps_CHKBOX.isChecked():
			if self.publishAllDeps_CHKBOX.isChecked():
				return False
			else:
				selected = self.dependencies_TABLEVIEW.selectedIndexes()
				if len(selected) <= 0:
					return True
				else:
					return False
		else:
			return False
		
	
	def createMetadata(self, _origFile):
		existingMetaNodes = r9Meta.getMetaNodes(['CGXMetaPublishedFile'])
		if len(existingMetaNodes) == 0:
			metaNode = cgxMeta.CGXMetaPublishedFile(name='file_META')
		else:
			metaNode = existingMetaNodes[0]
		metaNode.fileOrigin = _origFile
		currWorkstation = socket.gethostname()
		metaNode.author = str(currWorkstation)
		metaNode.time = str(time.ctime(os.path.getmtime(_origFile)))
	
	
	def publishAsset(self):
		'''Main function'''
		if self.publishAllDepsCheck():
			self.baseMod.statusBar_update("Please select at least one dependency to publish.", alert=True)
		else:
			mc.file(save=True, force=True)
			
			sync = Synchronizer(self)
			sync.statusDict = self.syncStatusDict
			
			trgPath = self.buildAssetPath()
			if self.customPathComp.customPath_CHKBOX.isChecked():
				trgPath = self.customPathComp.customPath_LINEEDIT.text()
			#Share path
			sharePath = self.buildSharedPath()
			#Backup path
			backupPath = self.buildBackupPath()
			#Source path
			srcPath = mc.file(expandName= True, query= True)
			origFile = srcPath
			#Create metadata
			self.createMetadata(srcPath)
			
			#Turn off defaultRenderLayer
			if self.projAssetsMod.FileTypes_Component.fileType_COMBOBOX.currentText() == "SHD":
				if mc.getAttr("defaultRenderLayer.renderable"):
					mc.setAttr("defaultRenderLayer.renderable", False)
					mc.file(save=True, force=True)
			
			#FILE NAME
			newName = self.buildFileName(srcPath)

			#IF THERE IS A PUBLISHED FILE ALREADY, BACK UP THAT MAYA PROJECT FIRST
			if self.backup_CHKBOX.isChecked():
				backupPath = self.backupFile(backupPath, trgPath, newName, sync)
				
			#SAVE MAYA FILE TO PUBLISHED LOCATION
			mc.file(rename= trgPath + "/" + newName)
			self.baseMod.statusBar_update("Saving published file to: " + trgPath + "/" + newName, status=True)
			mc.file(save=True, force=True)
			#Published source path
			srcPath = mc.file(expandName= True, query= True)
			
			#COPY TO SHARED LOCATION
			if self.share_CHKBOX.isChecked():
				finalSharePath = sharePath + "/" + newName
				self.baseMod.statusBar_update("Sharing file to: " + finalSharePath, status=True)
				sync.processPaths(srcPath, finalSharePath)
			
			#BACKUP DEPENDENCIES FROM THE OLD MAYA PROJECT
			if self.backup_CHKBOX.isChecked():
				if self.publishDeps_CHKBOX.isChecked():
					if os.path.exists(backupPath + "/" + newName):
						mc.file(backupPath + "/" + newName, open=True, force=True, prompt= False)
						dependenciesObj = Maya_Dependencies()
						repeatedDependencies = []
						#Alembic files
						for depDict in dependenciesObj.alembicFiles:
							fileName = depDict["filePath"].rsplit("/", 1)[1]
							finalBkupPath = backupPath + "/Cache/" + fileName
							if finalBkupPath in repeatedDependencies: #DO NOT COPY THE SAME FILE OVER AND OVER IF IT'S USED BY DIFFERENT NODES
								self.baseMod.statusBar_update("Skipped backup dependency, already copied: " + finalBkupPath, status=True)
							else:
								repeatedDependencies.append(finalBkupPath)
								self.skipByStatus(sync, depDict["filePath"], finalBkupPath)
								#Change node filepath to backup location
								dependenciesObj.fileAttrChange(finalBkupPath, depDict["node"], "Alembic")
						#Exocortex Alembic files
						for depDict in dependenciesObj.exocortexAlembicFiles:
							fileName = depDict["filePath"].rsplit("/", 1)[1]
							finalBkupPath = backupPath + "/Cache/" + fileName
							if finalBkupPath in repeatedDependencies: #DO NOT COPY THE SAME FILE OVER AND OVER IF IT'S USED BY DIFFERENT NODES
								self.baseMod.statusBar_update("Skipped backup dependency, already copied: " + finalBkupPath, status=True)
							else:
								repeatedDependencies.append(finalBkupPath)
								self.skipByStatus(sync, depDict["filePath"], finalBkupPath)
								#Change node filepath to backup location
								dependenciesObj.fileAttrChange(finalBkupPath, depDict["node"], "Exocortex")
						#Audio files
						for depDict in dependenciesObj.audioFiles:
							fileName = depDict["filePath"].rsplit("/", 1)[1]
							finalBkupPath = backupPath + "/Audio/" + fileName
							if finalBkupPath in repeatedDependencies: #DO NOT COPY THE SAME FILE OVER AND OVER IF IT'S USED BY DIFFERENT NODES
								self.baseMod.statusBar_update("Skipped backup dependency, already copied: " + finalBkupPath, status=True)
							else:
								repeatedDependencies.append(finalBkupPath)
								self.skipByStatus(sync, depDict["filePath"], finalBkupPath)
								#Change node filepath to backup location
								dependenciesObj.fileAttrChange(finalBkupPath, depDict["node"], "Audio")
						#Image plane files
						for depDict in dependenciesObj.imagePlaneFiles:
							fileName = depDict["filePath"].rsplit("/", 1)[1]
							finalBkupPath = backupPath + "/Maps/" + fileName
							if finalBkupPath in repeatedDependencies: #DO NOT COPY THE SAME FILE OVER AND OVER IF IT'S USED BY DIFFERENT NODES
								self.baseMod.statusBar_update("Skipped backup dependency, already copied: " + finalBkupPath, status=True)
							else:
								repeatedDependencies.append(finalBkupPath)
								sequenceStatus = mc.getAttr(depDict["node"] + ".useFrameExtension")
								if sequenceStatus == True:
									self.skipByStatus(sync, depDict["filePath"], finalBkupPath, isSequence=True)
									#Change node filepath to backup location
									dependenciesObj.fileAttrChange(finalBkupPath, depDict["node"], "Image plane")
								else:
									self.skipByStatus(sync, depDict["filePath"], finalBkupPath, isTexture=True)
									#Change node filepath to backup location
									dependenciesObj.fileAttrChange(finalBkupPath, depDict["node"], "Image plane")
						#Point cache files
						for depDict in dependenciesObj.pointCacheFiles:
							fileNameXml = depDict["xmlPath"].rsplit("/", 1)[1]
							finalBkupPathXml = backupPath + "/Cache/" + fileNameXml 
							fileNameMcx = depDict["mcxPath"].rsplit("/", 1)[1]
							finalBkupPathMcx = backupPath + "/Cache/" + fileNameMcx
							if finalBkupPathMcx in repeatedDependencies: #DO NOT COPY THE SAME FILE OVER AND OVER IF IT'S USED BY DIFFERENT NODES
								self.baseMod.statusBar_update("Skipped backup dependency, already copied: " + finalBkupPathMcx, status=True)
							else:
								repeatedDependencies.append(finalBkupPathMcx)
								self.skipByStatus(sync, depDict["xmlPath"], finalBkupPathXml)
								self.skipByStatus(sync, depDict["mcxPath"], finalBkupPathMcx)
								#Change node filepath to backup location
								dependenciesObj.fileAttrChange(finalBkupPath, depDict["node"], "Point cache")
						#Reference files
						for depDict in dependenciesObj.referenceFiles:
							fileName = depDict["filePath"].rsplit("/", 1)[1]
							finalBkupPath = backupPath + "/Reference/" + fileName
							if finalBkupPath in repeatedDependencies: #DO NOT COPY THE SAME FILE OVER AND OVER IF IT'S USED BY DIFFERENT NODES
								self.baseMod.statusBar_update("Skipped backup dependency, already copied: " + finalBkupPath, status=True)
							else:
								repeatedDependencies.append(finalBkupPath)
								self.skipByStatus(sync, depDict["filePath"], finalBkupPath)
								#Change node filepath to backup location
								dependenciesObj.fileAttrChange(finalBkupPath, depDict["node"], "Reference")
						#Texture files
						for depDict in dependenciesObj.textureFiles:
							fileName = depDict["filePath"].rsplit("/", 1)[1]
							finalBkupPath = backupPath + "/Maps/" + fileName
							if finalBkupPath in repeatedDependencies: #DO NOT COPY THE SAME FILE OVER AND OVER IF IT'S USED BY DIFFERENT NODES
								self.baseMod.statusBar_update("Skipped backup dependency, already copied: " + finalBkupPath, status=True)
							else:
								repeatedDependencies.append(finalBkupPath)
								sequenceStatus = mc.getAttr(depDict["node"] + ".useFrameExtension")
								tileStatus = mc.getAttr(depDict["node"] + ".uvTilingMode")
								if sequenceStatus == True or tileStatus != 0:
									self.skipByStatus(sync, depDict["filePath"], finalBkupPath, isSequence=True)
									#Change node filepath to backup location
									dependenciesObj.fileAttrChange(finalBkupPath, depDict["node"], "Texture")
								else:
									self.skipByStatus(sync, depDict["filePath"], finalBkupPath, isTexture=True)
									#Change node filepath to backup location
									dependenciesObj.fileAttrChange(finalBkupPath, depDict["node"], "Texture")
						#Yeti files
						for depDict in dependenciesObj.yetiFiles:
							if depDict["cacheFileName"] != "":
								cacheFileName = depDict["cacheFileName"].rsplit("/", 1)[1]
								cacheFinalBkupPath = backupPath + "/Hair/" + cacheFileName
								if cacheFinalBkupPath in repeatedDependencies: #DO NOT COPY THE SAME FILE OVER AND OVER IF IT'S USED BY DIFFERENT NODES
									self.baseMod.statusBar_update("Skipped backup dependency, already copied: " + cacheFinalBkupPath, status=True)
								else:
									repeatedDependencies.append(cacheFinalBkupPath)
								self.skipByStatus(sync, depDict["cacheFileName"], cacheFinalBkupPath)
								dependenciesObj.fileAttrChange(cacheFinalBkupPath, depDict["node"], "Yeti cache file")
						#Yeti texture files
						for depDict in dependenciesObj.yetiTextureFiles:
							fileName = depDict["filePath"].rsplit("/", 1)[1]
							finalBkupPath = backupPath + "/Maps/" + fileName
							if finalBkupPath in repeatedDependencies: #DO NOT COPY THE SAME FILE OVER AND OVER IF IT'S USED BY DIFFERENT NODES
								self.baseMod.statusBar_update("Skipped backup dependency, already copied: " + finalBkupPath, status=True)
							else:
								repeatedDependencies.append(finalBkupPath)
								self.skipByStatus(sync, depDict["filePath"], finalBkupPath)
								#Change node filepath to backup location
								dependenciesObj.fileAttrChange(finalBkupPath, depDict["node"], "Yeti texture")
						mc.file(save=True, force=True)

			#COPY DEPENDENCIES
			if self.publishDeps_CHKBOX.isChecked():
				#Reopen published file if a backup was made for dependencies
				if self.backup_CHKBOX.isChecked():
					if self.publishDeps_CHKBOX.isChecked():
						mc.file(srcPath, open=True, force=True, prompt= False)
				#List selected deps
				selected = self.dependencies_TABLEVIEW.selectedIndexes()
				selRowsDirty = []
				for item in selected:
					row = item.row()
					selRowsDirty.append(row)
				selSet = set(selRowsDirty)
				selRows = list(selSet)
				if self.publishAllDeps_CHKBOX.isChecked():
					selRows = range(len(self.dependencies_TABLEVIEW.model().dataList))
				
				lastIndex = 0
				mayaDeps = Maya_Dependencies()
				seq = self.seqShotMod.Sequence_Component.seq_COMBOBOX.currentText()
				shot = self.seqShotMod.Shot_Component.shot_COMBOBOX.currentText()
				repeatedDependencies = []
				for index in selRows:
					rowDict = {}
					i = 0
					for header in self.dependencies_TABLEVIEW.model().headers:
						rowDict[header] = self.dependencies_TABLEVIEW.model().dataList[index][i]
						i += 1
					srcPath = rowDict["File"]
					fileName = rowDict["File"].rsplit("/", 1)[1]
					
					#DO NOT COPY THE SAME FILE OVER AND OVER IF IT'S USED BY DIFFERENT NODES
					syncFile = True
					if srcPath in repeatedDependencies: 
						self.baseMod.statusBar_update("Skipped dependency, already copied: " + srcPath, status=True)
						syncFile = False
					else:
						repeatedDependencies.append(srcPath)
						
					if rowDict["Type"] in ["Texture","Image plane"]:
						finalPath = trgPath + "/Maps/" + fileName
						finalSharePath = sharePath + "/Maps/" + fileName
						if self.shotPublish_CHKBOX.isChecked():
							finalPath = trgPath + "/Maps/" + seq + "_" + shot + "/" + fileName
							finalSharePath = sharePath + "/Maps/" + seq + "_" + shot + "/" + fileName
						sequenceStatus = mc.getAttr(rowDict["Node"] + ".useFrameExtension")
						tileStatus = 0
						if mc.attributeQuery("uvTilingMode", node=rowDict["Node"],exists=True):
							tileStatus = mc.getAttr(rowDict["Node"] + ".uvTilingMode")
						if sequenceStatus == True or tileStatus != 0:
							if syncFile:
								self.skipByStatus(sync, srcPath, finalPath, isSequence=True)
							#Change node filepath to published location
							mayaDeps.fileAttrChange(finalPath, rowDict["Node"], rowDict["Type"])
							if self.share_CHKBOX.isChecked():
								if syncFile:
									self.skipByStatus(sync, srcPath, finalSharePath, isSequence=True)
						else:
							if syncFile:
								self.skipByStatus(sync, srcPath, finalPath, isTexture=True)
							#Change node filepath to published location
							mayaDeps.fileAttrChange(finalPath, rowDict["Node"], rowDict["Type"])
							if self.share_CHKBOX.isChecked():
								if syncFile:
									self.skipByStatus(sync, srcPath, finalSharePath, isTexture=True)
					elif rowDict["Type"] in ["Alembic","Exocortex","Point cache"]:
						finalPath = trgPath + "/Cache/" + fileName
						finalSharePath = sharePath + "/Cache/" + fileName
						if self.shotPublish_CHKBOX.isChecked():
							finalPath = trgPath + "/Cache/" + seq + "_" + shot + "/" + fileName
							finalSharePath = sharePath + "/Cache/" + seq + "_" + shot + "/" + fileName
						if syncFile:
							self.skipByStatus(sync, srcPath, finalPath)
						#Change node filepath to published location
						mayaDeps.fileAttrChange(finalPath, rowDict["Node"], rowDict["Type"])
						if self.share_CHKBOX.isChecked():
							if syncFile:
								self.skipByStatus(sync, srcPath, finalSharePath)
					elif rowDict["Type"] in ["Reference"]:
						finalPath = trgPath + "/Reference/" + fileName
						finalSharePath = sharePath + "/Reference/" + fileName
						if self.shotPublish_CHKBOX.isChecked():
							finalPath = trgPath + "/Reference/" + seq + "_" + shot + "/" + fileName
							finalSharePath = sharePath + "/Reference/" + seq + "_" + shot + "/" + fileName
						if syncFile:
							self.skipByStatus(sync, srcPath, finalPath)
						#Change node filepath to published location
						mayaDeps.fileAttrChange(finalPath, rowDict["Node"], rowDict["Type"])
						if self.share_CHKBOX.isChecked():
							if syncFile:
								self.skipByStatus(sync, srcPath, finalSharePath)
					elif rowDict["Type"] in ["Audio"]:
						finalPath = trgPath + "/Audio/" + fileName
						finalSharePath = sharePath + "/Audio/" + fileName
						if self.shotPublish_CHKBOX.isChecked():
							finalPath = trgPath + "/Audio/" + seq + "_" + shot + "/" + fileName
							finalSharePath = sharePath + "/Audio/" + seq + "_" + shot + "/" + fileName
						if syncFile:
							self.skipByStatus(sync, srcPath, finalPath)
						#Change node filepath to published location
						mayaDeps.fileAttrChange(finalPath, rowDict["Node"], rowDict["Type"])
						if self.share_CHKBOX.isChecked():
							if syncFile:
								self.skipByStatus(sync, srcPath, finalSharePath)
					elif rowDict["Type"] in ["Yeti cache file"]:
						finalPath = trgPath + "/Hair/" + fileName
						finalSharePath = sharePath + "/Hair/" + fileName
						if self.shotPublish_CHKBOX.isChecked():
							finalPath = trgPath + "/Hair/" + seq + "_" + shot + "/" + fileName
							finalSharePath = sharePath + "/Hair/" + seq + "_" + shot + "/" + fileName
						if syncFile:	
							self.skipByStatus(sync, srcPath, finalPath)
						#Change node filepath to published location
						mayaDeps.fileAttrChange(finalPath, rowDict["Node"], rowDict["Type"])
						if self.share_CHKBOX.isChecked():
							if syncFile:
								self.skipByStatus(sync, srcPath, finalSharePath)
					elif rowDict["Type"] in ["Yeti texture"]:
						finalPath = trgPath + "/Maps/" + fileName
						finalSharePath = sharePath + "/Maps/" + fileName
						if self.shotPublish_CHKBOX.isChecked():
							finalPath = trgPath + "/Maps/" + seq + "_" + shot + "/" + fileName
							finalSharePath = sharePath + "/Maps/" + seq + "_" + shot + "/" + fileName
						if syncFile:	
							self.skipByStatus(sync, srcPath, finalPath, isTexture=True)
						#Change node filepath to published location
						mayaDeps.fileAttrChange(trgPath + "/Maps", rowDict["Node"], rowDict["Type"])
						if self.share_CHKBOX.isChecked():
							if syncFile:
								self.skipByStatus(sync, srcPath, finalSharePath, isTexture=True)
					elif rowDict["Type"] in ["Custom"]:
						finalPath = trgPath + "/Custom/" + fileName
						finalSharePath = sharePath + "/Custom/" + fileName
						if self.shotPublish_CHKBOX.isChecked():
							finalPath = trgPath + "/Custom/" + seq + "_" + shot + "/" + fileName
							finalSharePath = sharePath + "/Custom/" + seq + "_" + shot + "/" + fileName
						if syncFile:	
							self.skipByStatus(sync, srcPath, finalPath, isTexture=True)
						if self.share_CHKBOX.isChecked():
							if syncFile:
								self.skipByStatus(sync, srcPath, finalSharePath, isTexture=True)
		
					lastIndex = index
				mc.file(save=True, force=True)
				self.dependencies_TABLEVIEW.selectRow(lastIndex)
				
				#IF SHARED, OPEN SHARED AND CHANGE PATHS
				if self.share_CHKBOX.isChecked():
					finalMayaSharePath = sharePath + "/" + newName
					mc.file(finalMayaSharePath, open=True, force=True, prompt= False)
					lastIndex = 0
					for index in selRows:
						rowDict = {}
						i = 0
						for header in self.dependencies_TABLEVIEW.model().headers:
							rowDict[header] = self.dependencies_TABLEVIEW.model().dataList[index][i]
							i += 1
						fileName = rowDict["File"].rsplit("/", 1)[1]
						self.baseMod.statusBar_update("Processing path for shared dependency: " + rowDict["File"], status=True)
						if rowDict["Type"] in ["Texture","Image plane"]:
							finalSharePath = sharePath + "/Maps/" + fileName
							if self.shotPublish_CHKBOX.isChecked():
								finalSharePath = sharePath + "/Maps/" + seq + "_" + shot + "/" + fileName
							mayaDeps.fileAttrChange(finalSharePath, rowDict["Node"], rowDict["Type"])
						elif rowDict["Type"] in ["Alembic","Exocortex","Point cache"]:
							finalSharePath = sharePath + "/Cache/" + fileName + "/" + fileName
							if self.shotPublish_CHKBOX.isChecked():
								finalSharePath = sharePath + "/Cache/" + seq + "_" + shot + "/" + fileName + "/" + fileName
							mayaDeps.fileAttrChange(finalSharePath, rowDict["Node"], rowDict["Type"])
						elif rowDict["Type"] in ["Reference"]:
							finalSharePath = sharePath + "/Reference/" + fileName
							if self.shotPublish_CHKBOX.isChecked():
								finalSharePath = sharePath + "/Reference/" + seq + "_" + shot + "/" + fileName
							mayaDeps.fileAttrChange(finalSharePath, rowDict["Node"], rowDict["Type"])
						elif rowDict["Type"] in ["Audio"]:
							finalSharePath = sharePath + "/Audio/" + fileName
							if self.shotPublish_CHKBOX.isChecked():
								finalSharePath = sharePath + "/Audio/" + seq + "_" + shot + "/" + fileName
							mayaDeps.fileAttrChange(finalSharePath, rowDict["Node"], rowDict["Type"])
						elif rowDict["Type"] in ["Yeti cache file"]:
							finalSharePath = sharePath + "/Hair/" + fileName
							if self.shotPublish_CHKBOX.isChecked():
								finalSharePath = sharePath + "/Hair/" + seq + "_" + shot + "/" + fileName
							mayaDeps.fileAttrChange(finalSharePath, rowDict["Node"], rowDict["Type"])
						elif rowDict["Type"] in ["Yeti texture"]:
							finalSharePath = sharePath + "/Maps/" + fileName
							if self.shotPublish_CHKBOX.isChecked():
								finalSharePath = sharePath + "/Maps/" + seq + "_" + shot + "/" + fileName
							mayaDeps.fileAttrChange(finalSharePath, rowDict["Node"], rowDict["Type"])
					mc.file(save=True, force=True)

			#RE-OPEN ORIGINAL FILE
			mc.file(origFile, open=True, force=True, prompt= False)

			#self.publishToShotgun()
			
			#Sync log- Debug Mode
			if self.debugMode.isChecked():
				self.debugDialog(',\n'.join(sync.log), self)
			
			#Done!!
			self.baseMod.statusBar_update("Done publishing: " + trgPath, success=True)
			self.resetTool()
	
	
	def skipByStatus(self, sync, _source, _target, **kwargs):
		if sync.getSyncStatus(_source, _target) == "In sync":
			self.baseMod.statusBar_update("Dependency skipped, already existed: " + _target, status=True)
		else:
			self.baseMod.statusBar_update("Processing dependency: " + _source, status=True)
			if 'isSequence' in kwargs.keys():
				if kwargs['isSequence']:
					sync.processPaths(_source, _target, isSequence=True)
			elif 'isTexture' in kwargs.keys():
				if kwargs['isTexture']:
					sync.processPaths(_source, _target, isTexture=True)
			else:
				sync.processPaths(_source, _target)
	
	
	def backupFile(self, backupPath, trgPath, newName, sync):
		#Modify bkup path to add version
		allEntries = []
		if os.path.exists(backupPath):
			allEntries = os.listdir(backupPath)
		if len(allEntries) == 0 or allEntries == None:
			backupPath += "/v_001"
		else:
			versionDirs = []
			for folder in allEntries:
				if folder.startswith("v"):
					versionDirs.append(folder)
			intVersions = []
			digitsNumber = 3
			for folder in versionDirs:
				versionNumber = folder.rsplit("_",1)[1]
				digitsNumber = len(versionNumber)
				intVersion = int(versionNumber)
				intVersions.append(intVersion)
			newVersion = str(max(intVersions) + 1).zfill(digitsNumber)
			backupPath += "/v_" + newVersion
		if os.path.exists(trgPath + "/" + newName):
			self.baseMod.statusBar_update("Backing up existing file to: " + backupPath + "/" + newName, status=True)
			sync.processPaths(trgPath + "/" + newName, backupPath + "/" + newName)
			
		return backupPath
	
	
	def shareOnly(self):
		#Asset shared path
		trgPath = self.buildSharedPath()
		#Source path
		srcPath = mc.file(expandName= True, query= True)
		
		#FILE NAME
		newName = self.buildFileName(srcPath)

		#Share path
		sharePath = trgPath + "/" + newName
		
		msgBox = QtWidgets.QMessageBox(self)
		msgBox.setWindowTitle("Warning!")
		msgBox.setText("Are you sure you want to ONLY share this file?")
		msgBox.setInformativeText("Final shared path: {}".format(sharePath))
		msgBox.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
		msgBox.setDefaultButton(QtWidgets.QMessageBox.Cancel)
		userInput = msgBox.exec_()
		if userInput == QtWidgets.QMessageBox.Ok:
			if not os.path.exists(trgPath):
				try:
					os.makedirs(trgPath)
					self.baseMod.statusBar_update("Directory structure created: {}".format(trgPath), status=True)
				except WindowsError:
					self.baseMod.statusBar_update("Directory structure couldn't be created: {}".format(trgPath), warning=True)

			self.baseMod.statusBar_update("Sharing current file to: " + sharePath, status=True)
			
			sync = Synchronizer(self)
			sync.statusDict = self.syncStatusDict
			
			#Share!!
			mc.file(save=True, force=True)
			mc.file(rename= sharePath)
			result = mc.file(save=True, force=True)
			
			if result:
				#DEPENDENCIES
				#List selected deps
				selected = self.dependencies_TABLEVIEW.selectedIndexes()
				selRowsDirty = []
				for item in selected:
					row = item.row()
					selRowsDirty.append(row)
				selSet = set(selRowsDirty)
				selRows = list(selSet)
				if self.publishAllDeps_CHKBOX.isChecked():
					selRows = range(len(self.dependencies_TABLEVIEW.model().dataList))
				
				mayaDeps = Maya_Dependencies()
				repeatedDependencies = []
				for index in selRows:
					rowDict = {}
					i = 0
					for header in self.dependencies_TABLEVIEW.model().headers:
						rowDict[header] = self.dependencies_TABLEVIEW.model().dataList[index][i]
						i += 1
					srcPath = rowDict["File"]
					fileName = rowDict["File"].rsplit("/", 1)[1]
					
					#DO NOT COPY THE SAME FILE OVER AND OVER IF IT'S USED BY DIFFERENT NODES
					syncFile = True
					if srcPath in repeatedDependencies: 
						self.baseMod.statusBar_update("Skipped dependency, already copied: " + srcPath, status=True)
						syncFile = False
					else:
						repeatedDependencies.append(srcPath)
						
					if rowDict["Type"] in ["Texture","Image plane"]:
						finalSharePath = trgPath + "/Maps/" + fileName
						sequenceStatus = mc.getAttr(rowDict["Node"] + ".useFrameExtension")
						tileStatus = 0
						if mc.attributeQuery("uvTilingMode", node=rowDict["Node"],exists=True):
							tileStatus = mc.getAttr(rowDict["Node"] + ".uvTilingMode")
						if sequenceStatus == True or tileStatus != 0:
							if syncFile:
								self.skipByStatus(sync, srcPath, finalSharePath, isSequence=True)
							#Change node filepath to shared location
							mayaDeps.fileAttrChange(finalSharePath, rowDict["Node"], rowDict["Type"])
						else:
							if syncFile:
								self.skipByStatus(sync, srcPath, finalSharePath, isTexture=True)
							#Change node filepath to published location
							mayaDeps.fileAttrChange(finalSharePath, rowDict["Node"], rowDict["Type"])
					elif rowDict["Type"] in ["Alembic","Exocortex","Point cache"]:
						finalSharePath = trgPath + "/Cache/" + fileName
						if syncFile:
							self.skipByStatus(sync, srcPath, finalSharePath)
						#Change node filepath to published location
						mayaDeps.fileAttrChange(finalSharePath, rowDict["Node"], rowDict["Type"])
					elif rowDict["Type"] in ["Reference"]:
						finalSharePath = trgPath + "/Reference/" + fileName
						if syncFile:
							self.skipByStatus(sync, srcPath, finalSharePath)
						#Change node filepath to published location
						mayaDeps.fileAttrChange(finalSharePath, rowDict["Node"], rowDict["Type"])
					elif rowDict["Type"] in ["Audio"]:
						finalSharePath = trgPath + "/Audio/" + fileName
						if syncFile:
							self.skipByStatus(sync, srcPath, finalSharePath)
						#Change node filepath to published location
						mayaDeps.fileAttrChange(finalSharePath, rowDict["Node"], rowDict["Type"])
					elif rowDict["Type"] in ["Yeti cache file"]:
						finalSharePath = trgPath + "/Hair/" + fileName
						if syncFile:	
							self.skipByStatus(sync, srcPath, finalSharePath)
						#Change node filepath to published location
						mayaDeps.fileAttrChange(finalSharePath, rowDict["Node"], rowDict["Type"])
					elif rowDict["Type"] in ["Yeti texture"]:
						finalSharePath = trgPath + "/Maps/" + fileName
						if syncFile:	
							self.skipByStatus(sync, srcPath, finalSharePath, isTexture=True)
						#Change node filepath to published location
						mayaDeps.fileAttrChange(trgPath + "/Maps", rowDict["Node"], rowDict["Type"])
					elif rowDict["Type"] in ["Custom"]:
						finalSharePath = trgPath + "/Custom/" + fileName
						if syncFile:	
							self.skipByStatus(sync, srcPath, finalSharePath, isTexture=True)

				self.baseMod.statusBar_update("Done sharing." + sharePath, success=True)
			else:
				self.baseMod.statusBar_update("Something went wrong while processing the file.", warning=True)
	
	
	def pupulateFileData(self):
		srcPath = mc.file(expandName= True, query= True)
		self.lastUpdate_LINEEDIT.setText(time.ctime(os.path.getmtime(srcPath)))
		self.size_LINEEDIT.setText(str(os.path.getsize(srcPath)))
	
	
	def populateDependencies(self):
		projDeps = Maya_Dependencies()
		headers = ["Type", "File", "Node"]
		dataList = []
		
		#Alembic files
		for depFile in projDeps.alembicFiles:
			rowDataList = []
			for column in headers:
				if column == "Type":
					rowDataList.append("Alembic")
				elif column == "File":
					rowDataList.append(depFile["filePath"])
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		#Exocortex Alembic files
		for depFile in projDeps.exocortexAlembicFiles:
			rowDataList = []
			for column in headers:
				if column == "Type":
					rowDataList.append("Exocortex")
				elif column == "File":
					rowDataList.append(depFile["filePath"])
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		#Audio files
		for depFile in projDeps.audioFiles:
			rowDataList = []
			for column in headers:
				if column == "Type":
					rowDataList.append("Audio")
				elif column == "File":
					rowDataList.append(depFile["filePath"])
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		#Image plane files
		for depFile in projDeps.imagePlaneFiles:
			rowDataList = []
			for column in headers:
				if column == "Type":
					rowDataList.append("Image plane")
				elif column == "File":
					rowDataList.append(depFile["filePath"])
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		#Point cache files
		for depFile in projDeps.pointCacheFiles:
			rowDataList = []
			for column in headers:
				if column == "Type":
					rowDataList.append("Point cache")
				elif column == "File":
					rowDataList.append(depFile["xmlPath"])
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
			rowDataList = []
			for column in headers:
				if column == "Type":
					rowDataList.append("Point cache")
				elif column == "File":
					rowDataList.append(depFile["mcxPath"])
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		#Reference files
		for depFile in projDeps.referenceFiles:
			rowDataList = []
			for column in headers:
				if column == "Type":
					rowDataList.append("Reference")
				elif column == "File":
					rowDataList.append(depFile["filePath"])
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		#Texture files
		for depFile in projDeps.textureFiles:
			rowDataList = []
			for column in headers:
				if column == "Type":
					rowDataList.append("Texture")
				elif column == "File":
					rowDataList.append(depFile["filePath"])
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)
		#Yeti files
		for depFile in projDeps.yetiFiles:
			if depFile["cacheFileName"] != "":
				rowDataList = []
				for column in headers:
					if column == "Type":
						rowDataList.append("Yeti cache file")
					elif column == "File":
						rowDataList.append(depFile["cacheFileName"])
					elif column == "Node":
						rowDataList.append(depFile["node"])
				dataList.append(rowDataList)
		#Yeti textures
		for depFile in projDeps.yetiTextureFiles:
			rowDataList = []
			for column in headers:
				if column == "Type":
					rowDataList.append("Yeti texture")
				elif column == "File":
					rowDataList.append(depFile["filePath"])
				elif column == "Node":
					rowDataList.append(depFile["node"])
			dataList.append(rowDataList)


		depsModel = dvm.DataTableModel(dataList, headers)
		self.dependencies_TABLEVIEW.setModel(depsModel)
		self.dependencies_TABLEVIEW.resizeColumnsToContents()
		self.dependencies_TABLEVIEW.selectRow(0)
		self.dependencies_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
	
	
	def populatePublished(self):
		sync = Synchronizer(self)
		sync.statusDict = self.syncStatusDict
		trgPath = self.buildAssetPath()
		filesList = []
		if trgPath != None:
			if os.path.exists(trgPath):
				filesList = [ f for f in os.listdir(trgPath) if os.path.isfile(os.path.join(trgPath,f)) ]
		
		if len(filesList) >= 1:
			for each in sorted(filesList, reverse=True):
				if each.split("_")[0] != self.projAssetsMod.FileTypes_Component.fileType_COMBOBOX.currentText().split(":")[1]:
					indexNum = filesList.index(each)
					del(filesList[indexNum])
			currentFile = mc.file(expandName= True, query= True)

		headers = ["Status","Name","Last Update","Size(Bytes)"]
		dataList = []
		for each in filesList:
			rowDataList = []
			for column in headers:
				if column == "Status":
					rowDataList.append(sync.getSyncStatus(currentFile, trgPath + "/" + each))
				elif column == "Name":
					rowDataList.append(each)
				elif column == "Last Update":
					fileModDate = time.ctime(os.path.getmtime(trgPath + "/" + each))
					rowDataList.append(fileModDate)
				elif column == "Size(Bytes)":
					statinfo = os.stat(trgPath + "/" + each)
					fileSize = float(statinfo.st_size)
					rowDataList.append(fileSize)
			dataList.append(rowDataList)
		publishedModel = PublishedDataTableModel(dataList, headers, self.published_TABLEVIEW)
		self.published_TABLEVIEW.setModel(publishedModel)
		self.published_TABLEVIEW.resizeColumnsToContents()
		self.published_TABLEVIEW.selectRow(0)
		self.published_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)

	
	
	def buildFileName(self, srcPath):
		newName = srcPath
		fileType = self.projAssetsMod.FileTypes_Component.fileType_COMBOBOX.currentText().split(":")[1]
		assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
		subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
		if subAssetName not in [None,""]:
			assetName = subAssetName
		seq = self.seqShotMod.Sequence_Component.seq_COMBOBOX.currentText()
		shot = self.seqShotMod.Shot_Component.shot_COMBOBOX.currentText()
		customSuffix = self.customSuffix_LINEEDIT.text()
		if self.customSuffix_CHKBOX.isChecked():
			fileName = srcPath.rsplit("/", 1)[1]
			newName = "{}_{}_{}.{}".format(fileType, assetName, customSuffix, "ma")
			if fileName[-2:] == "mb":
				newName = "{}_{}_{}.{}".format(fileType, assetName, customSuffix, "mb")
			#SHOT DEPENDENT ASSET FILE NAME
			if self.shotPublish_CHKBOX.isChecked():
				fileName = srcPath.rsplit("/", 1)[1]
				newName = "{}_{}_{}_{}_{}.{}".format(fileType, assetName, seq, shot, customSuffix, "ma")
				if fileName[-2:] == "mb":
					newName = "{}_{}_{}_{}_{}.{}".format(fileType, assetName, seq, shot, customSuffix, "mb")
		else:
			fileName = srcPath.rsplit("/", 1)[1]
			newName = "{}_{}.{}".format(fileType, assetName, "ma")
			if fileName[-2:] == "mb":
				newName = "{}_{}.{}".format(fileType, assetName, "mb")
			#SHOT DEPENDENT ASSET FILE NAME
			if self.shotPublish_CHKBOX.isChecked():
				fileName = srcPath.rsplit("/", 1)[1]
				newName = "{}_{}_{}_{}.{}".format(fileType, assetName, seq, shot, "ma")
				if fileName[-2:] == "mb":
					newName = "{}_{}_{}_{}.{}".format(fileType, assetName, seq, shot, "mb")
		
		return newName
	
	
	def buildBackupPath(self):
		assetType = self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
		assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
		subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
		bkupPropertiesDict = {"abstractFolderType":"subfolder",
                              "concreteFolderType":"technical_subfolder",
                              "folderFunction":"published_content",
                              "folderConcreteFunction":"published_backup",
                              "placeholder":False}
		bkupNode = self.pInfo.folderStructure.getPropertiesAndValues(bkupPropertiesDict)
		bkupPathStr = ""
		if len(bkupNode) == 1:
			node = bkupNode[0]
			pathList = self.pInfo.rebuildPath(node)
			pathList.append(node.name)
			#Build path string
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			bkupPathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])

		propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
		if subAssetName not in [None,""]:
			propertiesDict = {"concreteFolderType":"published_leaffolder",
	                        "abstractFolderType":"branchfolder",
	                        "folderFunction":"published_content",
	                        "folderConcreteFunction":"published_subasset",
	                        "placeholder":False}
		assetTypeDict={"name":assetType,
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
		assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		if assetType == 'PROPS':
			assetDict= {"name":"$PropName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'VEHICLES':
			assetDict= {"name":"$VehicleName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'SETS':
			assetDict= {"name":"$SetName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
		assetNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
		assetPathStr = ""
		for node in assetNode:
			pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict})
			if subAssetName not in [None,""]:
				pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict,subAssetName:subAssetDict})
			pathList.append(node.name)
			pathStr = "/".join(pathList[1:])
			
			if "$" not in pathStr:
				assetPathStr = pathStr
				break
		
		finalPathStr = bkupPathStr + "/" + assetPathStr
		
		return finalPathStr
		
	
	def buildSharedPath(self):
		assetType = self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
		assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
		subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()

		sharedPropertiesDict = {"abstractFolderType":"subfolder",
                        "concreteFolderType":"technical_subfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_sharedassets",
                        "placeholder":False}
		sharedNode = self.pInfo.folderStructure.getPropertiesAndValues(sharedPropertiesDict)
		sharedPathStr = ""
		if len(sharedNode) == 1:
			node = sharedNode[0]
			pathList = self.pInfo.rebuildPath(node)
			pathList.append(node.name)
			#Build path string
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			sharedPathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])

		propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
		if subAssetName not in [None,""]:
			propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_subasset",
                        "placeholder":False}
		assetTypeDict={"name":assetType,
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
		assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		if assetType == 'PROPS':
			assetDict= {"name":"$PropName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'VEHICLES':
			assetDict= {"name":"$VehicleName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'SETS':
			assetDict= {"name":"$SetName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
		assetNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
		assetPathStr = ""
		for node in assetNode:
			pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict})
			if subAssetName not in [None,""]:
				pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict,subAssetName:subAssetDict})
			pathList.append(node.name)
			pathStr = "/".join(pathList[1:])
			
			if "$" not in pathStr:
				assetPathStr = pathStr
				break
		
		finalPathStr = sharedPathStr + "/" + assetPathStr
		
		return finalPathStr
		
	
	def buildAssetPath(self):
		assetType = self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
		assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
		subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
		
		propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
		if subAssetName not in [None,""]:
			propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_subasset",
                        "placeholder":False}
		publishedNodes = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
		
		assetTypeDict={"name":assetType,
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
		assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		if assetType == 'PROPS':
			assetDict= {"name":"$PropName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'VEHICLES':
			assetDict= {"name":"$VehicleName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'SETS':
			assetDict= {"name":"$SetName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		
		subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
		for node in publishedNodes:
			pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict})
			if subAssetName not in [None,""]:
				pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict,subAssetName:subAssetDict})
			pathList.append(node.name)
			#Build path string
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
			
			if "$" not in pathStr:
				return pathStr
	
	
	def refreshPublishDependencies(self):
		if self.publishDeps_CHKBOX.isChecked():
			self.publishAllDeps_CHKBOX.setEnabled(True)
			self.publishAllDeps_CHKBOX.setChecked(True)
			self.dependencies_TABLEVIEW.setEnabled(True)
		else:
			self.publishAllDeps_CHKBOX.setEnabled(False)
			self.publishAllDeps_CHKBOX.setChecked(False)
			self.dependencies_TABLEVIEW.setEnabled(False)
		self.refreshPublishAll()
		
	
	def refreshPublishAll(self):
		if self.publishAllDeps_CHKBOX.isChecked():
			self.dependencies_TABLEVIEW.setEnabled(False)
		else:
			if self.publishDeps_CHKBOX.isChecked():
				self.dependencies_TABLEVIEW.setEnabled(True)
			else:
				self.dependencies_TABLEVIEW.setEnabled(False)
	
	
	def refreshShotPublish(self):
		if self.shotPublish_CHKBOX.isChecked():
			self.seqShotMod.container.setEnabled(True)
		else:
			self.seqShotMod.container.setEnabled(False)
	
	
	def refreshCustomSuffix(self):
		if self.customSuffix_CHKBOX.isChecked():
			self.customSuffix_LINEEDIT.setEnabled(True)
		else:
			self.customSuffix_LINEEDIT.setEnabled(False)
	
	
	def debugDialog(self,  _text, _parent):
		dialog = QtWidgets.QDialog(_parent)
		dialog.setObjectName("Dialog")
		dialog.resize(781, 556)
		gridLayout = QtWidgets.QGridLayout(dialog)
		gridLayout.setObjectName("gridLayout")
		debug_TEXTEDIT = QtWidgets.QTextEdit(dialog)
		debug_TEXTEDIT.setObjectName("debug_TEXTEDIT")
		debug_TEXTEDIT.setText(_text)
		debug_TEXTEDIT.setReadOnly(True)
		gridLayout.addWidget(debug_TEXTEDIT, 0, 0, 1, 1)
		if __binding__ in ('PySide', 'PyQt4'):
			dialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog", "Debug", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			dialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog", "Debug", None))
		dialog.show()
	
	
	def checkListDialog(self, _list, _totalIssues, _parent):
		chekDialog = CheckList_DialogGUI(_list, _totalIssues, _parent)
		chekDialog.show()
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for deps table view
	##--------------------------------------------------------------------------------------------
	def depsOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.dependencies_TABLEVIEW)
		reloadDepsQ = menu.addAction("Reload dependencies")
		if not self.publishAllDeps_CHKBOX.isChecked():
			selectNodeQ = menu.addAction("Select node")
		menu.addSeparator()
		addCustomDepQ = menu.addAction("Add custom dependency")
		menu.popup(self.dependencies_TABLEVIEW.mapToGlobal(pos))
		
		reloadDepsQ.triggered.connect(self.reloadDeps)
		if not self.publishAllDeps_CHKBOX.isChecked():
			selectNodeQ.triggered.connect(self.selectNode)
		addCustomDepQ.triggered.connect(self.addCustomDep)


	def selectNode(self):
		#List selected deps
		selected = self.dependencies_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selected:
			row = item.row()
			selRowsDirty.append(row)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		if len(selRows) > 1:
			self.baseMod.statusBar_update("Please select one dependency only to select the corresponding node.", alert=True)
		elif len(selRows) < 1:
			self.baseMod.statusBar_update("Please select at least one dependency to select the corresponding node.", alert=True)
		else:
			for index in selRows:
				rowDict = {}
				i = 0
				for header in self.dependencies_TABLEVIEW.model().headers:
					rowDict[header] = self.dependencies_TABLEVIEW.model().dataList[index][i]
					i += 1
				if rowDict["Node"] != None:
					mc.select(rowDict["Node"], add=True)


	def reloadDeps(self):
		self.populateDependencies()


	def addCustomDep(self):
		browseDialog = QtWidgets.QFileDialog(self)
		browseDialog.setDirectory(self.sInfo.getProjectsFrom())
		browseDialog.setFileMode(browseDialog.FileMode.ExistingFile)
		browseDialog.setLabelText(browseDialog.Accept,"Select")
		browseDialog.setWindowTitle("Choose custom dependency")
		dialogReturn = browseDialog.exec_()
		if dialogReturn == 1:
			thisFile = browseDialog.selectedFiles()[0]
			if "\\" in thisFile:
				thisFile = thisFile.replace("\\","/")
			self.dependencies_TABLEVIEW.model().insertRows(self.dependencies_TABLEVIEW.model().rowCount(QtCore.QModelIndex()), 1, ["Custom", thisFile, None])


##--------------------------------------------------------------------------------------------
##Create dialog
##--------------------------------------------------------------------------------------------
class CheckList_DialogGUI(QtWidgets.QDialog):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _list, _totalIssues, parent):
		super(CheckList_DialogGUI,self).__init__(parent)
		self.parent = parent
		self.totalIssues = _totalIssues
		self.setupUi(self)
		self.setConnections()
		self.initModel(_list, _totalIssues)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setupUi(self, checkList_DIALOG):
		checkList_DIALOG.setObjectName("checkList_DIALOG")
		checkList_DIALOG.resize(801, 664)
		sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(checkList_DIALOG.sizePolicy().hasHeightForWidth())
		checkList_DIALOG.setSizePolicy(sizePolicy)
		checkList_DIALOG.setMinimumSize(QtCore.QSize(801, 664))
		checkList_DIALOG.setMaximumSize(QtCore.QSize(801, 664))
		self.gridLayoutWidget = QtWidgets.QWidget(checkList_DIALOG)
		self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 781, 651))
		self.gridLayoutWidget.setObjectName("gridLayoutWidget")
		self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
		self.gridLayout.setContentsMargins(0, 0, 0, 0)
		self.gridLayout.setObjectName("gridLayout")
		self.totalIssues_LABEL = QtWidgets.QLabel(self.gridLayoutWidget)
		self.totalIssues_LABEL.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
		self.totalIssues_LABEL.setObjectName("totalIssues_LABEL")
		self.gridLayout.addWidget(self.totalIssues_LABEL, 2, 2, 1, 3)
		self.checkList_TABLEVIEW = QtWidgets.QTableView(self.gridLayoutWidget)
		self.checkList_TABLEVIEW.setObjectName("checkList_TABLEVIEW")
		self.gridLayout.addWidget(self.checkList_TABLEVIEW, 0, 1, 1, 4)
		self.yes_BTN = QtWidgets.QPushButton(self.gridLayoutWidget)
		sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.yes_BTN.sizePolicy().hasHeightForWidth())
		self.yes_BTN.setSizePolicy(sizePolicy)
		self.yes_BTN.setObjectName("yes_BTN")
		self.gridLayout.addWidget(self.yes_BTN, 3, 3, 1, 1)
		self.no_BTN = QtWidgets.QPushButton(self.gridLayoutWidget)
		sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
		sizePolicy.setHorizontalStretch(0)
		sizePolicy.setVerticalStretch(0)
		sizePolicy.setHeightForWidth(self.no_BTN.sizePolicy().hasHeightForWidth())
		self.no_BTN.setSizePolicy(sizePolicy)
		self.no_BTN.setLayoutDirection(QtCore.Qt.LeftToRight)
		self.no_BTN.setObjectName("no_BTN")
		self.gridLayout.addWidget(self.no_BTN, 3, 4, 1, 1)

		self.retranslateUi(checkList_DIALOG)
		QtCore.QMetaObject.connectSlotsByName(checkList_DIALOG)


	def retranslateUi(self, checkList_DIALOG):
		if __binding__ in ('PySide', 'PyQt4'):
			checkList_DIALOG.setWindowTitle(QtWidgets.QApplication.translate("checkList_DIALOG", "Check List", None, QtWidgets.QApplication.UnicodeUTF8))
			self.totalIssues_LABEL.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "This file has a total of {} issues. Would you like to submit anyway?".format(self.totalIssues), None, QtWidgets.QApplication.UnicodeUTF8))
			self.yes_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "Yes", None, QtWidgets.QApplication.UnicodeUTF8))
			self.no_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "No", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			checkList_DIALOG.setWindowTitle(QtWidgets.QApplication.translate("checkList_DIALOG", "Check List", None))
			self.totalIssues_LABEL.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "This file has a total of {} issues. Would you like to submit anyway?".format(self.totalIssues), None))
			self.yes_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "Yes", None))
			self.no_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "No", None))
	
	
	def setConnections(self):
		self.yes_BTN.clicked.connect(self.acceptPublish)
		self.no_BTN.clicked.connect(self.close)
		#CONTEXT MENUS
		self.checkList_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.checkList_TABLEVIEW.customContextMenuRequested.connect(self.checklistOptions)
		
		
	def initModel(self, _list, _totalIssues):
		#MODEL
		headers = ["Level", "Check", "Node", "Description"]
		self.checkModel = CheckListDataTableModel(_list, headers, self)
		
		if __binding__ in ('PySide', 'PyQt4'):
			from PySide import QtGui as oldGui
			proxyModel =  oldGui.QSortFilterProxyModel(self)
		elif __binding__ in ('PySide2', 'PyQt5'):
			proxyModel =  QtCore.QSortFilterProxyModel(self)
		proxyModel.setSourceModel(self.checkModel)
		self.checkList_TABLEVIEW.setModel(proxyModel)
		self.checkList_TABLEVIEW.sortByColumn(0,QtCore.Qt.DescendingOrder)
		self.checkList_TABLEVIEW.setAlternatingRowColors(True)
		self.checkList_TABLEVIEW.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
		self.checkList_TABLEVIEW.setColumnWidth(0,35)
		self.checkList_TABLEVIEW.setColumnWidth(1,130)
		self.checkList_TABLEVIEW.setColumnWidth(2,140)
		self.checkList_TABLEVIEW.setColumnWidth(3,300)
		self.checkList_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
		htalHeader = self.checkList_TABLEVIEW.horizontalHeader()
		htalHeader.setStretchLastSection(True)
	
	
	def acceptPublish(self):
		self.parent.publishAsset()
		self.close()
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for chek list dialog
	##--------------------------------------------------------------------------------------------
	def checklistOptions (self, pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.checkList_TABLEVIEW)
		selectNodeQ = menu.addAction("Select Node")
		menu.popup(self.checkList_TABLEVIEW.mapToGlobal(pos))
	
		selectNodeQ.triggered.connect(self.selectNodeOpt)
	
	
	def selectNodeOpt(self):
		dataList= self.checkModel.dataList
		selRowsProxy = self.checkList_TABLEVIEW.selectedIndexes()
		proxyModel = self.checkList_TABLEVIEW.model()
		selRows = []
		for each in selRowsProxy:
			selRows.append(proxyModel.mapToSource(each))
		selRowsDirty = []
		for item in selRows:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		finalList = []
		for row in selRows:
			nodeName = dataList[row][2]
			finalList.append(nodeName)
		mc.select(finalList, replace=True)
		

##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for CheckList table
##--------------------------------------------------------------------------------------------
class CheckListDataTableModel(dvm.DataTableModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, dataList, headers, parent):
		super(CheckListDataTableModel, self).__init__(dataList, headers, parent)
		self.parent = parent
			
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def flags(self, index):
		"""Set flags of each cell"""
		return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
	
	
	def data(self, index, role):
		"""Collect data and put it in the table"""
		row = index.row()
		column = index.column()
		
		if role == QtCore.Qt.DisplayRole:
			return self.dataList[row][column]

		if role == QtCore.Qt.BackgroundRole:
			if index.column() == 0:
				if self.dataList[row][column] >= 85:
					return QtGui.QColor(255, 0, 0, 255)
				elif self.dataList[row][column] < 85 and self.dataList[row][column] >= 50:
					return QtGui.QColor(255, 127, 0, 255)
				elif self.dataList[row][column] < 50 and self.dataList[row][column] >= 1:
					return QtGui.QColor(0, 127, 255, 255)
				
		if role in [QtCore.Qt.ToolTipRole, QtCore.Qt.WhatsThisRole]:
			if self.dataList[row][0] >= 85:
				return "FIX IT OR DIE!"
			elif self.dataList[row][0] < 85 and self.dataList[row][0] >= 50:
				return "FIX IT, DON'T BE LAZY!"
			elif self.dataList[row][0] < 50 and self.dataList[row][0] >= 1:
				return "FIX IT, YOU FREAKING PERFECTIONIST!"


##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for published files table
##--------------------------------------------------------------------------------------------
class PublishedDataTableModel(dvm.DataTableModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self,dataList, headers, parent):
		super(PublishedDataTableModel, self).__init__(dataList, headers, parent)
	

	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def data(self, index, role):
		"""Collect data and put it in the table"""
		row = index.row()
		column = index.column()
		
		if role == QtCore.Qt.DisplayRole:
			value = self.dataList[row][column]
			return value
		
		if role == QtCore.Qt.ToolTipRole:
			value = self.dataList[row][column]
			return value
		
		if role == QtCore.Qt.EditRole:
			return self.dataList[row][column]
		
		if role == QtCore.Qt.DecorationRole:
			value = self.dataList[row][column]
			if self.headers[column] == "Status":
				if value == "Already published":
					return QtGui.QIcon(":/AssetPublisher_inSync.png")
				elif value == "Published but different":
					return QtGui.QIcon(":/AssetPublisher_outOfSync.png")
				else:
					return QtGui.QIcon(":/AssetPublisher_rest.png")
	

##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/pipeline/AssetPublisher_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()