# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:\CGXtools\cgx\Maya\scripts\pipeline\DeadlineSubmitter\ui/checkList.ui'
#
# Created: Tue Jan 17 17:49:23 2017
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_checkList_DIALOG(object):
    def setupUi(self, checkList_DIALOG):
        checkList_DIALOG.setObjectName("checkList_DIALOG")
        checkList_DIALOG.resize(801, 664)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(checkList_DIALOG.sizePolicy().hasHeightForWidth())
        checkList_DIALOG.setSizePolicy(sizePolicy)
        checkList_DIALOG.setMinimumSize(QtCore.QSize(801, 664))
        checkList_DIALOG.setMaximumSize(QtCore.QSize(801, 664))
        self.gridLayoutWidget = QtGui.QWidget(checkList_DIALOG)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 781, 651))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.totalIssues_LABEL = QtGui.QLabel(self.gridLayoutWidget)
        self.totalIssues_LABEL.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.totalIssues_LABEL.setObjectName("totalIssues_LABEL")
        self.gridLayout.addWidget(self.totalIssues_LABEL, 2, 2, 1, 3)
        self.checkList_TABLEVIEW = QtGui.QTableView(self.gridLayoutWidget)
        self.checkList_TABLEVIEW.setObjectName("checkList_TABLEVIEW")
        self.gridLayout.addWidget(self.checkList_TABLEVIEW, 0, 1, 1, 4)
        self.yes_BTN = QtGui.QPushButton(self.gridLayoutWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.yes_BTN.sizePolicy().hasHeightForWidth())
        self.yes_BTN.setSizePolicy(sizePolicy)
        self.yes_BTN.setObjectName("yes_BTN")
        self.gridLayout.addWidget(self.yes_BTN, 3, 3, 1, 1)
        self.no_BTN = QtGui.QPushButton(self.gridLayoutWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.no_BTN.sizePolicy().hasHeightForWidth())
        self.no_BTN.setSizePolicy(sizePolicy)
        self.no_BTN.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.no_BTN.setObjectName("no_BTN")
        self.gridLayout.addWidget(self.no_BTN, 3, 4, 1, 1)

        self.retranslateUi(checkList_DIALOG)
        QtCore.QMetaObject.connectSlotsByName(checkList_DIALOG)

    def retranslateUi(self, checkList_DIALOG):
        checkList_DIALOG.setWindowTitle(QtGui.QApplication.translate("checkList_DIALOG", "Check List", None, QtGui.QApplication.UnicodeUTF8))
        self.totalIssues_LABEL.setText(QtGui.QApplication.translate("checkList_DIALOG", "This file has a total of {} issues. Would you like to submit anyway?", None, QtGui.QApplication.UnicodeUTF8))
        self.yes_BTN.setText(QtGui.QApplication.translate("checkList_DIALOG", "Yes", None, QtGui.QApplication.UnicodeUTF8))
        self.no_BTN.setText(QtGui.QApplication.translate("checkList_DIALOG", "No", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    checkList_DIALOG = QtGui.QDialog()
    ui = Ui_checkList_DIALOG()
    ui.setupUi(checkList_DIALOG)
    checkList_DIALOG.show()
    sys.exit(app.exec_())

