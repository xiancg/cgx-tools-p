# -*- coding: utf-8 -*-
'''
Sanity Check projects before sending them to render farm.

Created on Feb 06, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import maya.mel as mel
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets, QtCore, QtGui
from cgx.gui.Qt import __binding__
import cgx.Maya.scripts.maya_libs.Maya_Window as mayaGUI
import cgx.gui.DataViewModels as dvm
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityLighting import SanityLighting


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Deadline Submitter object
##--------------------------------------------------------------------------------------------
class DeadlineSubmitter ():
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self):
        self.submitter()
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def submitter (self):
        totalIssues, finalDataList = self.sanityCheck()
        if totalIssues >= 1:
            self.checkListDialog(finalDataList, totalIssues)
        else:
            mel.eval("SubmitJobToDeadline")
    
    
    def sanityCheck(self):
        """Runs the sanity check modules over the current file"""
        
        checkNodes = []
        checkSelectedOnly = False
        checkLighting = SanityLighting([], checkNodes, checkSelectedOnly)
        checkLightingResult = checkLighting.check(checkLighting.checkMethods, checkNodes, checkSelectedOnly)
        totalIssues = checkLightingResult[1]
        finalDataList = checkLightingResult[0]
        
        return totalIssues, finalDataList
    
    
    def checkListDialog(self, _list, _totalIssues, _parent=mayaGUI.getMayaWindow()):
        chekDialog = CheckList_DialogGUI(_list, _totalIssues, _parent)
        chekDialog.show()
    

##--------------------------------------------------------------------------------------------
##Create dialog
##--------------------------------------------------------------------------------------------
class CheckList_DialogGUI(QtWidgets.QDialog):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _list, _totalIssues, parent):
        super(CheckList_DialogGUI,self).__init__(parent)
        self.totalIssues = _totalIssues
        self.setupUi(self)
        self.setConnections()
        self.initModel(_list, _totalIssues)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def setupUi(self, checkList_DIALOG):
        checkList_DIALOG.setObjectName("checkList_DIALOG")
        checkList_DIALOG.resize(801, 664)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(checkList_DIALOG.sizePolicy().hasHeightForWidth())
        checkList_DIALOG.setSizePolicy(sizePolicy)
        checkList_DIALOG.setMinimumSize(QtCore.QSize(801, 664))
        checkList_DIALOG.setMaximumSize(QtCore.QSize(801, 664))
        self.gridLayoutWidget = QtWidgets.QWidget(checkList_DIALOG)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 781, 651))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.totalIssues_LABEL = QtWidgets.QLabel(self.gridLayoutWidget)
        self.totalIssues_LABEL.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.totalIssues_LABEL.setObjectName("totalIssues_LABEL")
        self.gridLayout.addWidget(self.totalIssues_LABEL, 2, 2, 1, 3)
        self.checkList_TABLEVIEW = QtWidgets.QTableView(self.gridLayoutWidget)
        self.checkList_TABLEVIEW.setObjectName("checkList_TABLEVIEW")
        self.gridLayout.addWidget(self.checkList_TABLEVIEW, 0, 1, 1, 4)
        self.yes_BTN = QtWidgets.QPushButton(self.gridLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.yes_BTN.sizePolicy().hasHeightForWidth())
        self.yes_BTN.setSizePolicy(sizePolicy)
        self.yes_BTN.setObjectName("yes_BTN")
        self.gridLayout.addWidget(self.yes_BTN, 3, 3, 1, 1)
        self.no_BTN = QtWidgets.QPushButton(self.gridLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.no_BTN.sizePolicy().hasHeightForWidth())
        self.no_BTN.setSizePolicy(sizePolicy)
        self.no_BTN.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.no_BTN.setObjectName("no_BTN")
        self.gridLayout.addWidget(self.no_BTN, 3, 4, 1, 1)

        self.retranslateUi(checkList_DIALOG)
        QtCore.QMetaObject.connectSlotsByName(checkList_DIALOG)


    def retranslateUi(self, checkList_DIALOG):
        if __binding__ in ('PySide', 'PyQt4'):
            checkList_DIALOG.setWindowTitle(QtWidgets.QApplication.translate("checkList_DIALOG", "Check List", None, QtWidgets.QApplication.UnicodeUTF8))
            self.totalIssues_LABEL.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "This file has a total of {} issues. Would you like to submit anyway?".format(self.totalIssues), None, QtWidgets.QApplication.UnicodeUTF8))
            self.yes_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "Yes", None, QtWidgets.QApplication.UnicodeUTF8))
            self.no_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "No", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            checkList_DIALOG.setWindowTitle(QtWidgets.QApplication.translate("checkList_DIALOG", "Check List", None))
            self.totalIssues_LABEL.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "This file has a total of {} issues. Would you like to submit anyway?".format(self.totalIssues), None))
            self.yes_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "Yes", None))
            self.no_BTN.setText(QtWidgets.QApplication.translate("checkList_DIALOG", "No", None))
    
    
    def setConnections(self):
        self.yes_BTN.clicked.connect(self.launchDeadline)
        self.no_BTN.clicked.connect(self.close)
        #CONTEXT MENUS
        self.checkList_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.checkList_TABLEVIEW.customContextMenuRequested.connect(self.checklistOptions)
        
        
    def initModel(self, _list, _totalIssues):
        #MODEL
        headers = ["Level", "Check", "Node", "Description"]
        self.checkModel = CheckListDataTableModel(_list, headers, self)
        if __binding__ in ('PySide', 'PyQt4'):
            from PySide import QtGui as oldGui
            proxyModel =  oldGui.QSortFilterProxyModel(self)
        elif __binding__ in ('PySide2', 'PyQt5'):
            proxyModel =  QtCore.QSortFilterProxyModel(self)
        proxyModel.setSourceModel(self.checkModel)
        self.checkList_TABLEVIEW.setModel(proxyModel)
        self.checkList_TABLEVIEW.sortByColumn(0,QtCore.Qt.DescendingOrder)
        self.checkList_TABLEVIEW.setAlternatingRowColors(True)
        self.checkList_TABLEVIEW.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.checkList_TABLEVIEW.setColumnWidth(0,35)
        self.checkList_TABLEVIEW.setColumnWidth(1,130)
        self.checkList_TABLEVIEW.setColumnWidth(2,140)
        self.checkList_TABLEVIEW.setColumnWidth(3,300)
        self.checkList_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)

        htalHeader = self.checkList_TABLEVIEW.horizontalHeader()
        htalHeader.setStretchLastSection(True)

    
    def launchDeadline(self):
        mel.eval("SubmitJobToDeadline")
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods for chek list dialog
    ##--------------------------------------------------------------------------------------------
    def checklistOptions (self, pos):
        """Method that creates the popupmenu"""
        menu = QtWidgets.QMenu(self.checkList_TABLEVIEW)
        selectNodeQ = menu.addAction("Select Node")
        menu.popup(self.checkList_TABLEVIEW.mapToGlobal(pos))
    
        selectNodeQ.triggered.connect(self.selectNodeOpt)
    
    
    def selectNodeOpt(self):
        dataList= self.checkModel.dataList
        selRowsProxy = self.checkList_TABLEVIEW.selectedIndexes()
        proxyModel = self.checkList_TABLEVIEW.model()
        selRows = []
        for each in selRowsProxy:
            selRows.append(proxyModel.mapToSource(each))
        selRowsDirty = []
        for item in selRows:
            thisRow = item.row()
            selRowsDirty.append(thisRow)
        selSet = set(selRowsDirty)
        selRows = list(selSet)
        finalList = []
        for row in selRows:
            nodeName = dataList[row][2]
            finalList.append(nodeName)
        mc.select(finalList, replace=True)
        

##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for CheckList table
##--------------------------------------------------------------------------------------------
class CheckListDataTableModel(dvm.DataTableModel):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, dataList, headers, parent):
        super(CheckListDataTableModel, self).__init__(dataList, headers, parent)
        self.parent = parent
            
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def flags(self, index):
        """Set flags of each cell"""
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
    
    
    def data(self, index, role):
        """Collect data and put it in the table"""
        row = index.row()
        column = index.column()
        
        if role == QtCore.Qt.DisplayRole:
            return self.dataList[row][column]

        if role == QtCore.Qt.BackgroundRole:
            if index.column() == 0:
                if self.dataList[row][column] >= 85:
                    return QtGui.QColor(255, 0, 0, 255)
                elif self.dataList[row][column] < 85 and self.dataList[row][column] >= 50:
                    return QtGui.QColor(255, 127, 0, 255)
                elif self.dataList[row][column] < 50 and self.dataList[row][column] >= 1:
                    return QtGui.QColor(0, 127, 255, 255)
                
        if role in [QtCore.Qt.ToolTipRole, QtCore.Qt.WhatsThisRole]:
            if self.dataList[row][0] >= 85:
                return "FIX IT OR DIE!"
            elif self.dataList[row][0] < 85 and self.dataList[row][0] >= 50:
                return "FIX IT, DON'T BE LAZY!"
            elif self.dataList[row][0] < 50 and self.dataList[row][0] >= 1:
                return "FIX IT, YOU FREAKING PERFECTIONIST!"

        
##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------
def main ():
    dSubInstance = DeadlineSubmitter()


if __name__ == '__main__' or 'eclipsePython' in __name__:
    main()