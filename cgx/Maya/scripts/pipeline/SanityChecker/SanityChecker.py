# -*- coding: utf-8 -*-
'''
Sanity checker.

Created on Nov 08, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os

from cgx.gui.Qt import QtWidgets, QtCore, QtGui
from cgx.gui.Qt import __binding__
import maya.cmds as mc
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
from cgx.Maya.scripts.maya_libs.Maya_TimeRange import Maya_TimeRange
import cgx.gui.DataViewModels as dvm
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityAnimation import SanityAnimation
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityHair import SanityHair
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityLighting import SanityLighting
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityLookdev import SanityLookdev
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityModeling import SanityModeling
from cgx.Maya.scripts.pipeline.SanityChecker.libs.SanityRigging import SanityRigging


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/pipeline/SanityChecker'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.initComponents()
			self.initUI()
			self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		#SIGNALS
		self.cancel_BTN.clicked.connect(self.baseMod.quitApp)
		self.logFile_BTN.clicked.connect(self.browsePath)
		self.check_BTN.clicked.connect(self.performChecks)
		#REFRESH
		self.exportLogFile_CHKBOX.stateChanged.connect(self.refreshOutputPath)
		self.performAllChecks_CHKBOX.stateChanged.connect(self.refreshCheckList)
	
	
	def initTool(self):
		'''
	    Initializes tool settings.
	    '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
	    Initializes variables for this tool.
	    '''
		self.__client = "defaultClient"
		self.__toolName = "Sanity Checker"
		self.__toolVersion = "2.0"
		
		self.tRange = Maya_TimeRange()
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.baseMod.MainMenu_Component.edit_config.setVisible(False)
		self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo, tRange=self.tRange)
		self.fpsComp = pipeComponents.FPS_Component(self,self.centralWidget(), self.pInfo)
		self.fpsComp.pos = [215,3]
	
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
		self.fpsComp.initComponent(self.pInfo.maya_getFPS())
		self.appTools.checkFPS(self.fpsComp)
		
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		#Sanity checkers
		selItems = []
		if self.selectedNodesOnly_CHKBOX == True:
			selItems = mc.ls(sl= True)
		checkSelectedOnly = self.selectedNodesOnly_CHKBOX.isChecked()
		self.checkModeling = SanityModeling([], selItems, checkSelectedOnly)
		self.checkLookdev = SanityLookdev([], selItems, checkSelectedOnly)
		self.checkLighting = SanityLighting([], selItems, checkSelectedOnly)
		self.checkAnimation = SanityAnimation([], selItems, checkSelectedOnly)
		self.checkHair = SanityHair([], selItems, checkSelectedOnly)
		self.checkRigging = SanityRigging([], selItems, checkSelectedOnly)
		
		#Check options tree
		self.refreshTree()
	
	
	def refreshTree(self):
		#Check options tree
		headers = ["Check options", "root/leaf","internalName"]
		checksRoot = dvm.TreeNode(["Check options", "root/leaf","internalName"], headers, parent=None)
		self.checkList_TREEVIEW.reset()
		checkListModel = dvm.DataTreeModel(checksRoot,headers)
		self.checkList_TREEVIEW.setModel(checkListModel)
		self.checkList_TREEVIEW.hideColumn(1)
		self.checkList_TREEVIEW.hideColumn(2)
		self.checkList_TREEVIEW.setSelectionMode(self.checkList_TREEVIEW.MultiSelection)
		self.populateTree()
	
	
	def populateTree(self):
		"""To populate the Checks tree with current checking posibilities"""
		#Crear checking types
		rootModeling = self.addTreeItem("Modeling", "root", "Sanity Modeling")
		rootLookdev = self.addTreeItem("Look Dev", "root", "Sanity Look Dev")
		rootLighting = self.addTreeItem("Lighting", "root", "Sanity Lighting")
		rootAnimation = self.addTreeItem("Animation", "root", "Sanity Animation")
		rootHair = self.addTreeItem("Hair", "root", "Sanity Hair")
		rootRigging = self.addTreeItem("Rigging", "root", "Sanity Rigging")
		
		#Sanity modeling
		for item in self.checkModeling.checkMethods:
			self.checkList_TREEVIEW.model().insertRows(rootModeling.childCount(), 1,[item,"leaf","Sanity Modeling"], self.checkList_TREEVIEW.model().indexFromNode(rootModeling))
		#Sanity look dev
		for item in self.checkLookdev.checkMethods:
			self.checkList_TREEVIEW.model().insertRows(rootLookdev.childCount(), 1,[item,"leaf","Sanity Look Dev"], self.checkList_TREEVIEW.model().indexFromNode(rootLookdev))
		#Sanity lighting
		for item in self.checkLighting.checkMethods:
			self.checkList_TREEVIEW.model().insertRows(rootLighting.childCount(), 1,[item,"leaf","Sanity Lighting"], self.checkList_TREEVIEW.model().indexFromNode(rootLighting))
		#Sanity animation
		for item in self.checkAnimation.checkMethods:
			self.checkList_TREEVIEW.model().insertRows(rootAnimation.childCount(), 1,[item,"leaf","Sanity Animation"], self.checkList_TREEVIEW.model().indexFromNode(rootAnimation))
		#Sanity hair
		for item in self.checkHair.checkMethods:
			self.checkList_TREEVIEW.model().insertRows(rootHair.childCount(), 1,[item,"leaf","Sanity Hair"], self.checkList_TREEVIEW.model().indexFromNode(rootHair))
		#Sanity rigging
		for item in self.checkRigging.checkMethods:
			self.checkList_TREEVIEW.model().insertRows(rootRigging.childCount(), 1,[item,"leaf","Sanity Rigging"], self.checkList_TREEVIEW.model().indexFromNode(rootRigging))
	
	
	def addTreeItem (self, item, itemType, internalName):
		if itemType == "root":
			return self.checkList_TREEVIEW.model().insertRows(self.checkList_TREEVIEW.model().dataList.childCount(), 1,[item,itemType,internalName])
		elif itemType == "geo":
			selected = self.checkList_TREEVIEW.selectedIndexes()
			if selected[0].isValid():
				indexNode = self.checkList_TREEVIEW.model().getNode(selected[0])
				return self.checkList_TREEVIEW.model().insertRows(indexNode.childCount(), 1, [item,itemType,internalName], selected[0])
	
	
	def performChecks(self):
		"Perform checks"
		checkModelingResult = []
		checkLookdevResult = []
		checkLightingResult = []
		checkAnimationResult = []
		checkHairResult = []
		checkRiggingResult = []
		
		if self.performAllChecks_CHKBOX.isChecked():
			checkSelectedOnly = False
			checkNodes = []
			if self.selectedNodesOnly_CHKBOX.isChecked():
				checkSelectedOnly = True
				checkNodes = mc.ls(sl= True)

			checkModelingResult =  self.checkModeling.check(self.checkModeling.checkMethods, checkNodes, checkSelectedOnly)
			checkLookdevResult = self.checkLookdev.check(self.checkLookdev.checkMethods, checkNodes, checkSelectedOnly)
			checkLightingResult = self.checkLighting.check(self.checkLighting.checkMethods, checkNodes, checkSelectedOnly)
			checkAnimationResult = self.checkAnimation.check(self.checkAnimation.checkMethods, checkNodes, checkSelectedOnly)
			checkHairResult = self.checkHair.check(self.checkHair.checkMethods, checkNodes, checkSelectedOnly)
			checkRiggingResult = self.checkRigging.check(self.checkRigging.checkMethods, checkNodes, checkSelectedOnly)
			
			totalIssues = 0
			modelingIssues = checkModelingResult[1]
			if modelingIssues >= 1:
				totalIssues += modelingIssues
			lookdevIssues = checkLookdevResult[1]
			if lookdevIssues >= 1:
				totalIssues += lookdevIssues
			lightingIssues = checkLightingResult[1]
			if lightingIssues >= 1:
				totalIssues += lightingIssues
			animationIssues = checkAnimationResult[1]
			if animationIssues >= 1:
				totalIssues += animationIssues
			hairIssues = checkHairResult[1]
			if hairIssues >= 1:
				totalIssues += hairIssues
			riggingIssues = checkRiggingResult[1]
			if riggingIssues >= 1:
				totalIssues += riggingIssues
				
			finalDataList = checkLightingResult[0] + checkAnimationResult[0] + checkHairResult[0] + checkLookdevResult[0] + checkModelingResult[0] + checkRiggingResult[0]
			self.checkListDialog(finalDataList, self)
			
			if totalIssues >= 1:
				self.baseMod.statusBar_update("Found {} issues.".format(str(totalIssues)), warning=True)
			else:
				self.baseMod.statusBar_update("Found no issues.", success=True)
		else:
			checkSelectedOnly = False
			checkNodes = []
			if self.selectedNodesOnly_CHKBOX.isChecked():
				checkSelectedOnly = True
				checkNodes = mc.ls(sl= True)
			
			selected = self.checkList_TREEVIEW.selectedIndexes()
			if len(selected) >= 1:
				checkListModeling = []
				checkListLookdev = []
				checkListLighting = []
				checkListAnimation = []
				checkListHair = []
				checkListRigging = []
				for item in selected:
					if item.isValid():
						thisNode = item.internalPointer()
						itemType= thisNode.data
						if itemType[1] == "root":
							children = thisNode.children
							if itemType[0] == "Lighting":
								for child in children:
									checkListLighting.append(child.name)
							if itemType[0] == "Animation":
								for child in children:
									checkListAnimation.append(child.name)
							if itemType[0] == "Hair":
								for child in children:
									checkListHair.append(child.name)
							if itemType[0] == "Look Dev":
								for child in children:
									checkListLookdev.append(child.name)
							if itemType[0] == "Modeling":
								for child in children:
									checkListModeling.append(child.name)
							if itemType[0] == "Rigging":
								for child in children:
									checkListRigging.append(child.name)
						elif itemType[1] == "leaf":
							parentItem = thisNode.parent
							parentType = parentItem.data
							if parentType[0] == "Lighting":
								checkListLighting.append(thisNode.name)
							if parentType[0] == "Animation":
								checkListAnimation.append(thisNode.name)
							if parentType[0] == "Hair":
								checkListHair.append(thisNode.name)
							if parentType[0] == "Look Dev":
								checkListLookdev.append(thisNode.name)
							if parentType[0] == "Modeling":
								checkListModeling.append(thisNode.name)
							if parentType[0] == "Rigging":
								checkListRigging.append(thisNode.name)
			
			checkListModeling = set(checkListModeling)
			checkListModeling = list(checkListModeling)
			checkListLookdev = set(checkListLookdev)
			checkListLookdev = list(checkListLookdev)
			checkListLighting = set(checkListLighting)
			checkListLighting = list(checkListLighting)
			checkListAnimation = set(checkListAnimation)
			checkListAnimation = list(checkListAnimation)
			checkListHair = set(checkListHair)
			checkListHair = list(checkListHair)
			checkListRigging = set(checkListRigging)
			checkListRigging = list(checkListRigging)
			
			checkModelingResult = self.checkModeling.check(checkListModeling, checkNodes, checkSelectedOnly)
			checkLookdevResult = self.checkLookdev.check(checkListLookdev, checkNodes, checkSelectedOnly)
			checkLightingResult = self.checkLighting.check(checkListLighting, checkNodes, checkSelectedOnly)
			checkAnimationResult = self.checkAnimation.check(checkListAnimation, checkNodes, checkSelectedOnly)
			checkHairResult = self.checkHair.check(checkListHair, checkNodes, checkSelectedOnly)
			checkRiggingResult = self.checkRigging.check(checkListRigging, checkNodes, checkSelectedOnly)
			
			totalIssues = 0
			modelingIssues = checkModelingResult[1]
			if modelingIssues >= 1:
				totalIssues += modelingIssues
			lookdevIssues = checkLookdevResult[1]
			if lookdevIssues >= 1:
				totalIssues += lookdevIssues
			lightingIssues = checkLightingResult[1]
			if lightingIssues >= 1:
				totalIssues += lightingIssues
			animationIssues = checkAnimationResult[1]
			if animationIssues >= 1:
				totalIssues += animationIssues
			hairIssues = checkHairResult[1]
			if hairIssues >= 1:
				totalIssues += hairIssues
			riggingIssues = checkRiggingResult[1]
			if riggingIssues >= 1:
				totalIssues += riggingIssues
			
			finalDataList = checkLightingResult[0] + checkAnimationResult[0] + checkHairResult[0] + checkLookdevResult[0] + checkModelingResult[0] + checkRiggingResult[0]
			self.checkListDialog(finalDataList, self)
			
			if totalIssues >= 1:
				self.baseMod.statusBar_update("Found {} issues.".format(str(totalIssues)), warning=True)
			else:
				self.baseMod.statusBar_update("Found no issues.", success=True)
		

		if self.exportLogFile_CHKBOX.isChecked():
			if self.logFile_LINEEDIT.text() == None or self.logFile_LINEEDIT.text() == "":
				self.baseMod.statusBar_update("Output directory string can't be empty.", warning=True)
			else:
				openProject = mc.file(query=True, expandName= True)
				fileName = openProject.split("/")
				fOut = open(self.logFile_LINEEDIT.text() + "/" + fileName[-1] + "_sanityCheck.txt", 'w')
				if checkModelingResult[1] >= 1:
					modelingStr = self.formatString("MODELING", len(checkModelingResult[0]), checkModelingResult[0])
					fOut.write(modelingStr + "\n")
				if checkLookdevResult[1] >= 1:
					lookDevStr = self.formatString("LOOKDEV", len(checkLookdevResult[0]), checkLookdevResult[0])
					fOut.write(lookDevStr + "\n")
				if checkLightingResult[1] >= 1:
					lightingStr = self.formatString("LIGHTING", len(checkLightingResult[0]), checkLightingResult[0])
					fOut.write(lightingStr + "\n")
				if checkAnimationResult[1] >= 1:
					animationStr = self.formatString("ANIMATION", len(checkAnimationResult[0]), checkAnimationResult[0])
					fOut.write(animationStr + "\n")
				if checkHairResult[1] >= 1:
					hairStr = self.formatString("HAIR", len(checkHairResult[0]), checkHairResult[0])
					fOut.write(hairStr + "\n")
				if checkRiggingResult[1] >= 1:
					riggingStr = self.formatString("RIGGING", len(checkRiggingResult[0]), checkRiggingResult[0])
					fOut.write(riggingStr + "\n")
				fOut.close()
				self.baseMod.statusBar_update("Log: " + self.logFile_LINEEDIT.text() + "/" + fileName[-1] + "_sanityCheck.txt", success=True)
	
	
	def formatString (self, _checkType, _numIssues, _affectedNodes, _issues=False):
		"""def to format the final string with feedback for the user"""
		
		output = """
--------------------------------------------------------------------
'{}' check found {} issues
--------------------------------------------------------------------
""".format(_checkType, _numIssues)
		
		if len(_affectedNodes) >= 1:
			for item in _affectedNodes:
				output += ">> " + item[2] + " --> " + item[3] + "\n"
		
		return output
	
	
	def browsePath(self):
		'''
		Create a Browsing Dialog to get file or directory paths.
		'''
		browseDialog = QtWidgets.QFileDialog(self)
		fileMode = browseDialog.FileMode.Directory
		directory = self.sInfo.getProjectsFrom()
		browseDialog.setFileMode(fileMode)
		if os.path.exists(directory):
			browseDialog.setDirectory(directory)
		dialogReturn = browseDialog.exec_()
		if dialogReturn == 1:
			selectedFiles = browseDialog.selectedFiles()
			self.logFile_LINEEDIT.setText(selectedFiles[0])
			
	
	def refreshOutputPath(self):
		if self.exportLogFile_CHKBOX.isChecked():
			self.logFile_LINEEDIT.setEnabled(True)
			self.logFile_BTN.setEnabled(True)
		else:
			self.logFile_LINEEDIT.setEnabled(False)
			self.logFile_BTN.setEnabled(False)
	
	
	def refreshCheckList(self):
		if self.performAllChecks_CHKBOX.isChecked():
			self.checkList_TREEVIEW.setEnabled(False)
		else:
			self.checkList_TREEVIEW.setEnabled(True)
	
	
	def checkListDialog(self, _list, _parent):
		#GUI-VIEW
		dialog = QtWidgets.QDialog(_parent)
		dialog.setObjectName("Dialog")
		dialog.resize(650, 611)
		gridLayout = QtWidgets.QGridLayout(dialog)
		gridLayout.setObjectName("gridLayout")
		self.checkList_TABLEVIEW = QtWidgets.QTableView(dialog)
		self.checkList_TABLEVIEW.setGeometry(QtCore.QRect(10, 10, 630, 551))
		self.checkList_TABLEVIEW.setMinimumSize(QtCore.QSize(0, 0))
		self.checkList_TABLEVIEW.setObjectName("checkList_TABLEVIEW")
		gridLayout.addWidget(self.checkList_TABLEVIEW, 0, 0, 1, 1)
		if __binding__ in ('PySide', 'PyQt4'):
			dialog.setWindowTitle(QtWidgets.QApplication.translate("checkList_DIALOG", "Check List", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			dialog.setWindowTitle(QtWidgets.QApplication.translate("checkList_DIALOG", "Check List", None))
		#MODEL
		headers = ["Level", "Check", "Node", "Description"]
		self.checkModel = CheckListDataTableModel(_list, headers, dialog)
		if __binding__ in ('PySide', 'PyQt4'):
			from PySide import QtGui as oldGui
			proxyModel =  oldGui.QSortFilterProxyModel(self)
		elif __binding__ in ('PySide2', 'PyQt5'):
			proxyModel =  QtCore.QSortFilterProxyModel(self)
		proxyModel.setSourceModel(self.checkModel)
		self.checkList_TABLEVIEW.setModel(proxyModel)
		self.checkList_TABLEVIEW.sortByColumn(0,QtCore.Qt.DescendingOrder)
		self.checkList_TABLEVIEW.setAlternatingRowColors(True)
		self.checkList_TABLEVIEW.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
		self.checkList_TABLEVIEW.setColumnWidth(0,35)
		self.checkList_TABLEVIEW.setColumnWidth(1,130)
		self.checkList_TABLEVIEW.setColumnWidth(2,140)
		self.checkList_TABLEVIEW.setColumnWidth(3,300)
		self.checkList_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
		htalHeader = self.checkList_TABLEVIEW.horizontalHeader()
		htalHeader.setStretchLastSection(True)
		#CONTEXT MENUS
		self.checkList_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.checkList_TABLEVIEW.customContextMenuRequested.connect(self.checklistOptions)
		
		dialog.show()
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for chek list dialog
	##--------------------------------------------------------------------------------------------
	def checklistOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.checkList_TABLEVIEW)
		selectNodeQ = menu.addAction("Select Node")
		menu.popup(self.checkList_TABLEVIEW.mapToGlobal(pos))
	
		selectNodeQ.triggered.connect(self.selectNodeOpt)
	
	
	def selectNodeOpt(self):
		dataList= self.checkModel.dataList
		selRowsProxy = self.checkList_TABLEVIEW.selectedIndexes()
		proxyModel = self.checkList_TABLEVIEW.model()
		selRows = []
		for each in selRowsProxy:
			selRows.append(proxyModel.mapToSource(each))
			
		selRowsDirty = []
		for item in selRows:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		finalList = []
		for row in selRows:
			nodeName = dataList[row][2]
			finalList.append(nodeName)
		mc.select(finalList, replace=True)
	

##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for CheckList table
##--------------------------------------------------------------------------------------------
class CheckListDataTableModel(dvm.DataTableModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, dataList, headers, parent):
		super(CheckListDataTableModel, self).__init__(dataList, headers, parent)
		self.parent = parent
			
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def flags(self, index):
		"""Set flags of each cell"""
		return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
	
	
	def data(self, index, role):
		"""Collect data and put it in the table"""
		row = index.row()
		column = index.column()
		
		if role == QtCore.Qt.DisplayRole:
			return self.dataList[row][column]

		if role == QtCore.Qt.BackgroundRole:
			if index.column() == 0:
				if self.dataList[row][column] >= 85:
					return QtGui.QColor(255, 0, 0, 255)
				elif self.dataList[row][column] < 85 and self.dataList[row][column] >= 50:
					return QtGui.QColor(255, 127, 0, 255)
				elif self.dataList[row][column] < 50 and self.dataList[row][column] >= 1:
					return QtGui.QColor(0, 127, 255, 255)
				
		if role in [QtCore.Qt.ToolTipRole, QtCore.Qt.WhatsThisRole]:
			if self.dataList[row][0] >= 85:
				return "FIX IT OR DIE!"
			elif self.dataList[row][0] < 85 and self.dataList[row][0] >= 50:
				return "FIX IT, DON'T BE LAZY!"
			elif self.dataList[row][0] < 50 and self.dataList[row][0] >= 1:
				return "FIX IT, YOU FREAKING PERFECTIONIST!"


##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/pipeline/SanityChecker_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()