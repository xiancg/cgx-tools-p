# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:\CGXtools\cgx\Maya\scripts\pipeline\SanityChecker\ui/checkList.ui'
#
# Created: Wed Jan 11 13:10:07 2017
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_checkList_DIALOG(object):
    def setupUi(self, checkList_DIALOG):
        checkList_DIALOG.setObjectName("checkList_DIALOG")
        checkList_DIALOG.resize(801, 611)
        self.gridLayoutWidget = QtGui.QWidget(checkList_DIALOG)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(10, 10, 781, 591))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.tableView = QtGui.QTableView(self.gridLayoutWidget)
        self.tableView.setObjectName("tableView")
        self.gridLayout.addWidget(self.tableView, 0, 1, 1, 1)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.gridLayout.addItem(spacerItem, 1, 1, 1, 1)

        self.retranslateUi(checkList_DIALOG)
        QtCore.QMetaObject.connectSlotsByName(checkList_DIALOG)

    def retranslateUi(self, checkList_DIALOG):
        checkList_DIALOG.setWindowTitle(QtGui.QApplication.translate("checkList_DIALOG", "Check List", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    checkList_DIALOG = QtGui.QDialog()
    ui = Ui_checkList_DIALOG()
    ui.setupUi(checkList_DIALOG)
    checkList_DIALOG.show()
    sys.exit(app.exec_())

