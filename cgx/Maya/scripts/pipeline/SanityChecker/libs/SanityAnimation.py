# -*- coding: utf-8 -*-
'''
Methods for sanity checking on the animation stage.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets
from cgx.Maya.scripts.maya_libs.Maya_TimeRange import Maya_TimeRange


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Sanity Animation
##--------------------------------------------------------------------------------------------
class SanityAnimation():
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _checkList, _checkNodes, _checkSelectedOnly):
        self.checkList = _checkList
        self.checkNodes = _checkNodes
        self.timeRange = Maya_TimeRange()
        self.timeRange.setFramesToTimeSlider()
        self.checkSelectedOnly = _checkSelectedOnly
        self.checkMethods = ["Start frame", "Animation Layers", "Maya Version", "Geometry with keys", "Geometry with NonZero Transforms",
                             "Odd file name", "Film Aspect Ratio"]
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def check (self, _checkList, _checkNodes, _checkSelectedOnly):
        """def that gets the checkList and calls the corresponding methods"""
        output = []
        self.checkList = _checkList
        self.checkSelectedOnly = _checkSelectedOnly
        self.checkNodes = _checkNodes
        issuesNumber = 0
        for chk in self.checkList:
            defName = chk.replace(" ", "_").lower()
            if defName in dir(self):
                result = eval('self.' + defName + '()')
                output += result[0]
                issuesNumber += result[1]
            else:
                msgBox = QtWidgets.QMessageBox()
                msgBox.setWindowTitle("Warning!")
                msgBox.setText("No method for " + defName + " was found.")
                msgBox.exec_()
        
        return output, issuesNumber
    
        
    def formatList(self, _checkType, _numIssues, _affectedNodes, _level):
        """Format a datalist with feedback for the user"""
        
        checkList = []
        for item in _affectedNodes.keys():
            checkList.append([ _level, _checkType, _affectedNodes[item][0], _affectedNodes[item][1] ])
        
        return checkList, _numIssues
    
    
    ##--------------------------------------------------------------------------------------------
    ##Checks
    ##--------------------------------------------------------------------------------------------
    def start_frame (self):
        """Check for timeline start frame to be set at 1001"""
        startFrameList = {}
        self.timeRange.setFramesToTimeSlider()
        listID = 1
        if self.timeRange.startFrame != 1001:
            startFrameList[listID] = [str(self.timeRange.startFrame), "Timeline start frame should be set at f1001."]
        
        rList = self.formatList("Start Frame", len(startFrameList), startFrameList, 100)
        
        return rList
    
    
    def animation_layers (self):
        """Check for existing animation layers"""
        animLayersList = []
        animLayersDict = {}
        for each in mc.ls(type="animLayer"):
            if each != "BaseAnimation":
                animLayersList.append(each)
        listID = 1
        if len(animLayersList) >= 1:
            animLayersDict[listID] = ["animationLayers", "Merge all existing animation layers and check your animation is still intact afterwards."]
        
        rList = self.formatList("Animation Layers", len(animLayersDict), animLayersDict, 70)
        
        return rList
    
    
    def maya_version (self):
        """Check for Maya Version"""
        mayaVer = {}
        acceptedVersion = "2016"
        listID = 1
        if mc.about(version=True) != acceptedVersion:
            mayaVer[listID] = ["Maya version", "You must work on Maya {}".format(acceptedVersion)]
        
        rList = self.formatList("Maya Version", len(mayaVer), mayaVer, 100)
        
        return rList
    
    
    def geometry_with_keys (self):
        """Check for geometry with keyframes"""
        geometryKeys = {}
        if self.checkSelectedOnly == True:
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    objTX = mc.keyframe( obj + '.translateX', query=True, keyframeCount=True )
                    objTY = mc.keyframe( obj + '.translateY', query=True, keyframeCount=True )
                    objTZ = mc.keyframe( obj + '.translateZ', query=True, keyframeCount=True )
                    objRX = mc.keyframe( obj + '.rotateX', query=True, keyframeCount=True )
                    objRY = mc.keyframe( obj + '.rotateY', query=True, keyframeCount=True )
                    objRZ = mc.keyframe( obj + '.rotateZ', query=True, keyframeCount=True )
                    objSX = mc.keyframe( obj + '.scaleX', query=True, keyframeCount=True )
                    objSY = mc.keyframe( obj + '.scaleY', query=True, keyframeCount=True )
                    objSZ = mc.keyframe( obj + '.scaleZ', query=True, keyframeCount=True )
                    keyCountsList = [objTX, objTY, objTZ, objRX, objRY, objRZ, objSX, objSY, objSZ]
                    for each in keyCountsList:
                        if each != 0:
                            geometryKeys[listID] = [obj, "Has keys on some of its transform attributes."]
                            break
                    listID += 1
                elif mc.nodeType(obj) == "mesh":
                    transformNode = mc.listRelatives(obj, parent= True, fullPath= True)[0]
                    objTX = mc.keyframe( transformNode + '.translateX', query=True, keyframeCount=True )
                    objTY = mc.keyframe( transformNode + '.translateY', query=True, keyframeCount=True )
                    objTZ = mc.keyframe( transformNode + '.translateZ', query=True, keyframeCount=True )
                    objRX = mc.keyframe( transformNode + '.rotateX', query=True, keyframeCount=True )
                    objRY = mc.keyframe( transformNode + '.rotateY', query=True, keyframeCount=True )
                    objRZ = mc.keyframe( transformNode + '.rotateZ', query=True, keyframeCount=True )
                    objSX = mc.keyframe( transformNode + '.scaleX', query=True, keyframeCount=True )
                    objSY = mc.keyframe( transformNode + '.scaleY', query=True, keyframeCount=True )
                    objSZ = mc.keyframe( transformNode + '.scaleZ', query=True, keyframeCount=True )
                    keyCountsList = [objTX, objTY, objTZ, objRX, objRY, objRZ, objSX, objSY, objSZ]
                    for each in keyCountsList:
                        if each != 0:
                            geometryKeys[listID] = [transformNode, "Has keys on some of its transform attributes."]
                            break
                    listID += 1
        else:
            listID = 1
            for obj in mc.ls(type= "mesh"):
                transformNode = mc.listRelatives(obj, parent= True, fullPath= True)[0]
                if mc.nodeType(transformNode) == "transform":
                    objTX = mc.keyframe( transformNode + '.translateX', query=True, keyframeCount=True )
                    objTY = mc.keyframe( transformNode + '.translateY', query=True, keyframeCount=True )
                    objTZ = mc.keyframe( transformNode + '.translateZ', query=True, keyframeCount=True )
                    objRX = mc.keyframe( transformNode + '.rotateX', query=True, keyframeCount=True )
                    objRY = mc.keyframe( transformNode + '.rotateY', query=True, keyframeCount=True )
                    objRZ = mc.keyframe( transformNode + '.rotateZ', query=True, keyframeCount=True )
                    objSX = mc.keyframe( transformNode + '.scaleX', query=True, keyframeCount=True )
                    objSY = mc.keyframe( transformNode + '.scaleY', query=True, keyframeCount=True )
                    objSZ = mc.keyframe( transformNode + '.scaleZ', query=True, keyframeCount=True )
                    keyCountsList = [objTX, objTY, objTZ, objRX, objRY, objRZ, objSX, objSY, objSZ]
                    for each in keyCountsList:
                        if each != 0:
                            geometryKeys[listID] = [transformNode, "Has keys on some of its transform attributes."]
                            break
                listID += 1
        
        rList = self.formatList("Geometry with keys", len(geometryKeys), geometryKeys, 70)
        
        return rList
    
    
    def geometry_with_nonzero_transforms (self):
        """Check for geometry with nonzero transforms"""
        nonZeroTrans = {}
        if self.checkSelectedOnly == True:
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    objTX = mc.getAttr(obj + '.translateX')
                    objTY = mc.getAttr(obj + '.translateY')
                    objTZ = mc.getAttr(obj + '.translateZ')
                    objRX = mc.getAttr(obj + '.rotateX')
                    objRY = mc.getAttr(obj + '.rotateY')
                    objRZ = mc.getAttr(obj + '.rotateZ')
                    objSX = mc.getAttr(obj + '.scaleX')
                    objSY = mc.getAttr(obj + '.scaleY')
                    objSZ = mc.getAttr(obj + '.scaleZ')
                    objTransRot = [objTX, objTY, objTZ, objRX, objRY, objRZ]
                    objScale = [objSX, objSY, objSZ]
                    foundOne = False
                    for each in objTransRot:
                        if each != 0:
                            foundOne = True
                            nonZeroTrans[listID] = [obj, "Has non-zero transforms."]
                            listID += 1
                            break
                    if not foundOne:
                        for each in objScale:
                            if each != 1:
                                nonZeroTrans[listID] = [obj, "Has non-one transforms."]
                                listID += 1
                                break
                elif mc.nodeType(obj) == "mesh":
                    transformNode = mc.listRelatives(obj, parent= True, fullPath= True)[0]
                    objTX = mc.getAttr(transformNode + '.translateX')
                    objTY = mc.getAttr(transformNode + '.translateY')
                    objTZ = mc.getAttr(transformNode + '.translateZ')
                    objRX = mc.getAttr(transformNode + '.rotateX')
                    objRY = mc.getAttr(transformNode + '.rotateY')
                    objRZ = mc.getAttr(transformNode + '.rotateZ')
                    objSX = mc.getAttr(transformNode + '.scaleX')
                    objSY = mc.getAttr(transformNode + '.scaleY')
                    objSZ = mc.getAttr(transformNode + '.scaleZ')
                    objTransRot = [objTX, objTY, objTZ, objRX, objRY, objRZ]
                    objScale = [objSX, objSY, objSZ]
                    foundOne = False
                    for each in objTransRot:
                        if each != 0:
                            foundOne = True
                            nonZeroTrans[listID] = [transformNode, "Has non-zero transforms."]
                            listID += 1
                            break
                    if not foundOne:
                        for each in objScale:
                            if each != 1:
                                nonZeroTrans[listID] = [transformNode, "Has non-one transforms."]
                                listID += 1
                                break
        else:
            listID = 1
            for obj in mc.ls(type= "mesh"):
                transformNode = mc.listRelatives(obj, parent= True, fullPath= True)[0]
                if mc.nodeType(transformNode) == "transform":
                    objTX = mc.getAttr(transformNode + '.translateX')
                    objTY = mc.getAttr(transformNode + '.translateY')
                    objTZ = mc.getAttr(transformNode + '.translateZ')
                    objRX = mc.getAttr(transformNode + '.rotateX')
                    objRY = mc.getAttr(transformNode + '.rotateY')
                    objRZ = mc.getAttr(transformNode + '.rotateZ')
                    objSX = mc.getAttr(transformNode + '.scaleX')
                    objSY = mc.getAttr(transformNode + '.scaleY')
                    objSZ = mc.getAttr(transformNode + '.scaleZ')
                    objTransRot = [objTX, objTY, objTZ, objRX, objRY, objRZ]
                    objScale = [objSX, objSY, objSZ]
                    foundOne = False
                    for each in objTransRot:
                        if each != 0:
                            foundOne = True
                            nonZeroTrans[listID] = [transformNode, "Has non-zero transforms."]
                            listID += 1
                            break
                    if not foundOne:
                        for each in objScale:
                            if each != 1:
                                nonZeroTrans[listID] = [transformNode, "Has non-one transforms."]
                                listID += 1
                                break
                    
        rList = self.formatList("Geometry with NonZero Transforms", len(nonZeroTrans), nonZeroTrans, 70)
        
        return rList
    
    
    def odd_file_name (self):
        """Check for general name conventions"""
        oddFileName = {}
        fileName = mc.file(query=True, sceneName=True, shortName=True)
        nameParts = fileName.split("_")
        listID = 1
        if len(nameParts) < 5 or len(nameParts) > 5:
            oddFileName[listID] = [fileName, "Has incorrect number of name parts. Follow this format: ANI_SEQ_01_s_010.0001.ma"]
            listID += 1
        if len(nameParts) == 5:
            verExtSplit = nameParts[4].split(".")
            if nameParts[0] != "ANI":
                oddFileName[listID] = [fileName, "File name suffix should be 'ANI'. Follow this format: ANI_SEQ_01_s_010.0001.ma"]
                listID += 1
            elif nameParts[1] != "SEQ":
                oddFileName[listID] = [fileName, "File name second part should be 'SEQ'. Follow this format: ANI_SEQ_01_s_010.0001.ma"]
                listID += 1
            elif not nameParts[2].isdigit():
                oddFileName[listID] = [fileName, "File name third part should be the sequence number. Follow this format: ANI_SEQ_01_s_010.0001.ma"]
                listID += 1
            elif nameParts[3] != "s":
                oddFileName[listID] = [fileName, "File name fourth part should be the 's' letter. Follow this format: ANI_SEQ_01_s_010.0001.ma"]
                listID += 1
            elif not verExtSplit[0].isdigit():
                oddFileName[listID] = [fileName, "File name fifth part should be the shot number. Follow this format: ANI_SEQ_01_s_010.0001.ma"]
                listID += 1
            elif verExtSplit[2] != "ma" :
                oddFileName[listID] = [fileName, "File extension must be Maya ASCII."]
                listID += 1
        
        rList = self.formatList("Odd file name", len(oddFileName), oddFileName, 40)
        
        return rList
    
    
    def film_aspect_ratio (self):
        """Check for film aspect ratio"""
        filmAspect = {}
        if self.checkSelectedOnly == True:
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    shapeNode = mc.listRelatives(obj, shapes= True, fullPath= True)[0]
                    if mc.getAttr(shapeNode + ".renderable"):
                        horizontalFilmAperture = mc.getAttr(shapeNode + ".horizontalFilmAperture")
                        verticalFilmAperture = mc.getAttr(shapeNode + ".verticalFilmAperture")
                        if horizontalFilmAperture != 0.980:
                            filmAspect[listID] = [shapeNode, "Horizontal film aperture should be 0.980 for 35mm full frame renders."]
                            listID += 1
                        if verticalFilmAperture != 0.735:
                            filmAspect[listID] = [shapeNode, "Vertical film aperture should be 0.735 for 35mm full frame renders."]
                            listID += 1
                elif mc.nodeType(obj) == "camera":
                    shapeNode = obj
                    if mc.getAttr(shapeNode + ".renderable"):
                        horizontalFilmAperture = mc.getAttr(shapeNode + ".horizontalFilmAperture")
                        verticalFilmAperture = mc.getAttr(shapeNode + ".verticalFilmAperture")
                        if horizontalFilmAperture != 0.980:
                            filmAspect[listID] = [shapeNode, "Horizontal film aperture should be 0.980 for 35mm full frame renders."]
                            listID += 1
                        if verticalFilmAperture != 0.735:
                            filmAspect[listID] = [shapeNode, "Vertical film aperture should be 0.735 for 35mm full frame renders."]
                            listID += 1
        else:
            listID = 1
            for obj in mc.ls(type= "camera"):
                shapeNode = obj
                if mc.getAttr(shapeNode + ".renderable"):
                    horizontalFilmAperture = mc.getAttr(shapeNode + ".horizontalFilmAperture")
                    verticalFilmAperture = mc.getAttr(shapeNode + ".verticalFilmAperture")
                    if horizontalFilmAperture != 0.980:
                        filmAspect[listID] = [shapeNode, "Horizontal film aperture should be 0.980 for 35mm full frame renders."]
                        listID += 1
                    if verticalFilmAperture != 0.735:
                        filmAspect[listID] = [shapeNode, "Vertical film aperture should be 0.735 for 35mm full frame renders."]
                        listID += 1
                    
        rList = self.formatList("Wrong film aspect ratio", len(filmAspect), filmAspect, 100)
        
        return rList
    

##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------            
def main():
    '''
    checkAnimationStr = ""
    checkSelectedOnly = False
    checkNodes = []
    checkSelectedOnly = False
    checkAnimationVar = SanityAnimation([], [], checkSelectedOnly)
    checkAnimationStr = checkAnimationVar.check(checkAnimationVar.checkMethods, checkNodes, checkSelectedOnly)
    print checkAnimationStr
    '''

if __name__ == "__main__": 
    main()