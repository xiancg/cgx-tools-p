# -*- coding: utf-8 -*-
'''
Methods for sanity checking on the hair stage.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import maya.cmds as mc
import maya.mel as mel
from cgx.gui.Qt import QtWidgets
from cgx.Maya.scripts.maya_libs.Maya_Dependencies import Maya_Dependencies


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Sanity Hair
##--------------------------------------------------------------------------------------------
class SanityHair():
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _checkList, _checkNodes, _checkSelectedOnly):
        self.checkList = _checkList
        self.checkNodes = _checkNodes
        self.checkSelectedOnly = _checkSelectedOnly
        self.mayaDeps = Maya_Dependencies()
        self.checkMethods = ["Odd geometry names", "NonZero Transforms", "NonZero Pivot", "Missing Yeti Cache", "Odd Yeti node name", "Missing Texture Reference object",
                             "Empty Namespace", "Existing Namespaces", "Missing Textures", "Master Layer On", "Display Layers", "Not approved file path","Existing AOVs"]
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def check (self, _checkList, _checkNodes, _checkSelectedOnly):
        """def that gets the checkList and calls the corresponding methods"""
        output = []
        self.checkList = _checkList
        self.checkSelectedOnly = _checkSelectedOnly
        self.checkNodes = _checkNodes
        issuesNumber = 0
        for chk in self.checkList:
            defName = chk.replace(" ", "_").lower()
            if defName in dir(self):
                result = eval('self.' + defName + '()')
                output += result[0]
                issuesNumber += result[1]
            else:
                msgBox = QtWidgets.QMessageBox()
                msgBox.setWindowTitle("Warning!")
                msgBox.setText("No method for " + defName + " was found.")
                msgBox.exec_()
        
        return output, issuesNumber
    
    
    def formatList(self, _checkType, _numIssues, _affectedNodes, _level):
        """Format a datalist with feedback for the user"""
        
        checkList = []
        for item in _affectedNodes.keys():
            checkList.append([ _level, _checkType, _affectedNodes[item][0], _affectedNodes[item][1] ])
        
        return checkList, _numIssues
    
    
    ##--------------------------------------------------------------------------------------------
    ##Checks
    ##--------------------------------------------------------------------------------------------
    def odd_geometry_names (self):
        """Check for non conventional naming on geometries"""
        oddNames = {}
        if self.checkSelectedOnly == True:
            oddNamesList = {}
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    if '|' not in obj:
                        nameParts = obj.split("_")
                        acceptedSides = ["C", "R", "L"]
                        if len(nameParts) < 5:
                            oddNamesList[listID] = [obj, "Has incorrect number of name parts."]
                        elif len(nameParts) > 6:
                            oddNamesList[listID] = [obj, "Has incorrect number of name parts."]
                        elif nameParts[0] not in acceptedSides:
                            oddNamesList[listID] = [obj, "Name does not specify side of model with C, R or L."]
                        elif nameParts[-1] != "MSH":
                            oddNamesList[listID] = [obj, "Name does not end with 'MSH'."]
                        elif nameParts[-1][-1].isdigit():
                            oddNamesList[listID] = [obj, "Name shouldn't end with a number."]
                    elif '|' in obj:
                        oddNamesList[listID] = [obj, "Name might be duplicate."]
                listID += 1
            oddNames = oddNamesList
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            #Get odd names
            oddNamesList = {}
            listID = 1
            for obj in allMeshTrans:
                if '|' not in obj:
                    nameParts = obj.split("_")
                    acceptedSides = ["C", "R", "L"]
                    if len(nameParts) < 5:
                        oddNamesList[listID] = [obj, "Has incorrect number of name parts."]
                    elif len(nameParts) > 6:
                        oddNamesList[listID] = [obj, "Has incorrect number of name parts."]
                    elif nameParts[0] not in acceptedSides:
                        oddNamesList[listID] = [obj, "Name does not specify side of model with C, R or L."]
                    elif nameParts[-1] != "MSH":
                        oddNamesList[listID] = [obj, "Name does not end with 'MSH'."]
                    elif nameParts[-1][-1].isdigit():
                        oddNamesList[listID] = [obj, "Name shouldn't end with a number."]
                elif '|' in obj:
                    oddNamesList[listID] = [obj, "Name might be duplicate."]
                listID += 1
            oddNames = oddNamesList
        
        rList = self.formatList("Odd geometry names", len(oddNames), oddNames, 100)

        return rList
        
    
    def nonzero_transforms (self):
        """Check for geometry with nonzero transforms"""
        nonZeroTrans = {}
        if self.checkSelectedOnly == True:
            nonZeroTransList = {}
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                translateObj = mc.xform(obj, query=True, translation=True,worldSpace=True)
                                rotateObj = mc.xform(obj, query=True, rotation=True,worldSpace=True)
                                scaleObj = mc.xform(obj, query=True, scale=True,worldSpace=True)
                                if translateObj[0] != 0 or translateObj[1] != 0 or translateObj[2] != 0 or rotateObj[0] != 0 or rotateObj[1] != 0 or rotateObj[2] != 0 or scaleObj[0] != 1 or scaleObj[1] != 1 or scaleObj[2] != 1:
                                    nonZeroTransList[listID] = [obj, "Has non-zero transforms."]
                                    listID += 1
                                break
            nonZeroTrans = nonZeroTransList
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            nonZeroTransList = {}
            listID = 1
            #Get nonzero transforms
            for obj in allMeshTrans:
                translateObj = mc.xform(obj, query=True, translation=True,worldSpace=True)
                rotateObj = mc.xform(obj, query=True, rotation=True,worldSpace=True)
                scaleObj = mc.xform(obj, query=True, scale=True,worldSpace=True)
                if translateObj[0] != 0 or translateObj[1] != 0 or translateObj[2] != 0 or rotateObj[0] != 0 or rotateObj[1] != 0 or rotateObj[2] != 0 or scaleObj[0] != 1 or scaleObj[1] != 1 or scaleObj[2] != 1:
                    nonZeroTransList[listID] = [obj, "Has non-zero transforms."]
                    listID += 1
            
            nonZeroTrans = nonZeroTransList
        
        rList = self.formatList("NonZero Transforms", len(nonZeroTrans), nonZeroTrans, 100)

        return rList
    
    
    def nonzero_pivot (self):
        """Check for geometry with nonzero pivot locations"""
        nonZeroPivot = {}
        if self.checkSelectedOnly == True:
            nonZeroPivotList = {}
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                pivot = mc.xform(obj, pivots= True, query= True, ws= True)
                                if pivot[0] != 0 or pivot[1] != 0 or pivot[2] != 0:
                                    nonZeroPivotList[listID] = [obj, "Has non-zero pivot."]
                                    listID += 1
                                break
            nonZeroPivot = nonZeroPivotList
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            nonZeroPivotList = {}
            listID = 1
            #Get nonzero pivots
            for obj in allMeshTrans:
                pivot = mc.xform(obj, pivots= True, query= True, ws= True)
                if pivot[0] != 0 or pivot[1] != 0 or pivot[2] != 0:
                    nonZeroPivotList[listID] = [obj, "Has non-zero pivot."]
                    listID += 1
            
            nonZeroPivot = nonZeroPivotList
        
        rList = self.formatList("NonZero Pivot", len(nonZeroPivot), nonZeroPivot, 100)

        return rList
    
    
    def existing_namespaces(self):
        """Check for existing namespaces"""
        existingNamespaces = []
        initNamespacesList = mc.namespaceInfo(":", listOnlyNamespaces= True)
        defaultNamespaces = ["UI","shared"]
        namespacesList = []
        for each in initNamespacesList:
            if each not in defaultNamespaces:
                namespacesList.append(each)
                
        if namespacesList != None or len(namespacesList) >= 1:
            for each in namespacesList:
                thisChildren = self.existing_namespace_recursive(each)
                existingNamespaces.append(each)
                existingNamespaces += thisChildren
        
        existingNamespacesDict = {}
        listID = 1
        for each in existingNamespaces:
            existingNamespacesDict[listID] = [each, "Existing Namespace."]
            listID += 1
        
        rList = self.formatList("Existing Namespaces", len(existingNamespacesDict), existingNamespacesDict, 100)
        
        return rList
        
    
    def existing_namespace_recursive(self, _namespace):
        allChildNamespaces = mc.namespaceInfo(_namespace, listOnlyNamespaces= True)
        if allChildNamespaces != None:
            finalChildren = []
            for each in allChildNamespaces:
                thisChildren = self.existing_namespace_recursive(each)
                if len(thisChildren) >= 1:
                    finalChildren += thisChildren
            return allChildNamespaces + finalChildren
        else:
            return []
    
    
    def empty_namespace (self):
        """Check for empty namespaces"""
        emptyNamespaces = []
        currentNamespace = mc.namespaceInfo( currentNamespace=True)
        if mc.namespaceInfo(currentNamespace, isRootNamespace= True):
            allChildNamespaces = mc.namespaceInfo(currentNamespace, listOnlyNamespaces= True)
            if allChildNamespaces != None:
                for each in allChildNamespaces:
                    thisEmpty = self.empty_namespace_recursive(each)
                    if thisEmpty != None:
                        emptyNamespaces.append(thisEmpty)
        
        emptyNamespacesDict = {}
        listID = 1
        for each in emptyNamespaces:
            emptyNamespacesDict[listID] = [each, "EMPTY Namespace."]
            listID += 1
        
        rList = self.formatList("Empty Namespaces", len(emptyNamespacesDict), emptyNamespacesDict, 70)
        
        return rList
        
    
    def empty_namespace_recursive(self, _namespace):
        allChildNamespaces = mc.namespaceInfo(_namespace, listOnlyNamespaces= True)
        if allChildNamespaces != None:
            for each in allChildNamespaces:
                thisEmpty = self.empty_namespace_recursive(each)
                if thisEmpty != None:
                    return thisEmpty
        elif mc.namespaceInfo(_namespace, listNamespace=True) == None:
            if _namespace != "shared":
                return _namespace
        else:
            return None
    
    
    def master_layer_on(self):
        """Check if masterLayer is On for rendering"""
        masterLayerOn = {}
        listID = 1
        if mc.getAttr("defaultRenderLayer.renderable") == 1:
            masterLayerOn[listID] = ["defaultRenderLayer", "masterLayer is ON as a rendereable layer. Should be OFF."]
        
        rList = self.formatList("Master Layer On", len(masterLayerOn), masterLayerOn, 100)
        
        return rList
    
    
    def display_layers(self):
        dispLayers = {}
        listID = 1
        for each in mc.ls(type="displayLayer"):
            if each != "defaultLayer":
                dispLayers[listID] = [each, "Delete all display layers before publishing."]
            listID += 1
        
        rList = self.formatList("Display Layers", len(dispLayers), dispLayers, 70)
        
        return rList
        
    
    def missing_textures (self):
        """Check for missing textures"""
        missingTextures = {}
        if self.checkSelectedOnly == True:
            missingTexturesList = {}
            for node in self.checkNodes:
                listID = 1
                if mc.nodeType(node) == "file":
                    texture = mc.getAttr(node + ".fileTextureName")
                    if not mc.file(texture, query= True, exists= True):
                        missingTexturesList[listID] = [node, texture]
                        listID += 1
                if mc.nodeType(node) == "pgYetiMaya":
                    textureNodes = mel.eval('pgYetiGraph -listNodes -type "texture" ' + node)
                    if textureNodes != None and len(textureNodes) >= 1:
                        for txtNode in textureNodes:
                            filePath =  mel.eval('pgYetiGraph -node ' + txtNode + ' -param "file_name" -getParamValue ' + node)
                            if not mc.file(filePath, query= True, exists= True):
                                missingTexturesList[listID] = [node, txtNode + " --> " + texture]
                                listID += 1
                if mc.nodeType(node) == "transform":
                    shape = mc.listRelatives(node, shapes=True, noIntermediate=True, fullPath=True)[0]
                    if mc.nodeType(shape) == "pgYetiMaya":
                        textureNodes = mel.eval('pgYetiGraph -listNodes -type "texture" ' + shape)
                        if textureNodes != None and len(textureNodes) >= 1:
                            for txtNode in textureNodes:
                                filePath =  mel.eval('pgYetiGraph -node ' + txtNode + ' -param "file_name" -getParamValue ' + shape)
                                if not mc.file(filePath, query= True, exists= True):
                                    missingTexturesList[listID] = [shape, txtNode + " --> " + texture]
                                    listID += 1
            missingTextures = missingTexturesList
        else:
            missingTexturesList = {}
            filesList = mc.ls(type="file")
            listID = 1
            for node in filesList:
                texture = mc.getAttr(node + ".fileTextureName")
                if not mc.file(texture, query=True, exists= True):
                    missingTexturesList[listID] = [node, texture]
                    listID += 1
            filesList= mc.ls(type="pgYetiMaya", long=True)
            if filesList != None:
                for node in filesList:
                    textureNodes = mel.eval('pgYetiGraph -listNodes -type "texture" ' + node)
                    if textureNodes != None and len(textureNodes) >= 1:
                        for txtNode in textureNodes:
                            filePath =  mel.eval('pgYetiGraph -node ' + txtNode + ' -param "file_name" -getParamValue ' + node)
                            if not mc.file(filePath, query= True, exists= True):
                                missingTexturesList[listID] = [node, txtNode + " --> " + texture]
                                listID += 1
            missingTextures = missingTexturesList
        
        rList = self.formatList("Missing textures", len(missingTextures), missingTextures, 100)
        
        return rList
    
    
    def existing_aovs(self):
        existingAOVs = {}
        thisList = mc.ls(type="aiAOV")
        listID = 1
        if thisList != None:
            if len(thisList) >= 1:
                for each in thisList:
                    existingAOVs[listID] = [each, "File contains AOVs."]
                    listID += 1
        
        rList = self.formatList("Existing AOVs", len(existingAOVs), existingAOVs, 100)
        
        return rList
    
    
    def not_approved_file_path (self):
        """Check for not approved file locations"""
        notValidFilePath = {}
        approvedServers = ['G','F','Z']
        listID = 1
        #Texture files
        if len( self.mayaDeps.textureFiles) >= 1:
            for each in self.mayaDeps.textureFiles:
                if each["filePath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " File coming from a not approved location."]
                    listID += 1
        #Point Cache files
        if len( self.mayaDeps.pointCacheFiles) >= 1:
            for each in self.mayaDeps.pointCacheFiles:
                if each["xmlPath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["xmlPath"] + " File coming from a not approved location."]
                    listID += 1
        #Alembic files
        if len( self.mayaDeps.alembicFiles) >= 1:
            for each in self.mayaDeps.alembicFiles:
                if each["filePath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " File coming from a not approved location."]
                    listID += 1
        #Referenced files
        if len( self.mayaDeps.referenceFiles) >= 1:
            for each in self.mayaDeps.referenceFiles:
                if each["filePath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " File coming from a not approved location."]
                    listID += 1
        #Exocortex Alembic files
        if len( self.mayaDeps.exocortexAlembicFiles) >= 1:
            for each in self.mayaDeps.exocortexAlembicFiles:
                if each["filePath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " File coming from a not approved location."]
                    listID += 1
        #Yeti files
        if len( self.mayaDeps.yetiFiles) >= 1:
            for each in self.mayaDeps.yetiFiles:
                if each["cacheFileName"] != "":
                    if each["cacheFileName"][0] not in approvedServers:
                        notValidFilePath[listID] = [each["node"], "Path: " + each["cacheFileName"] + " File coming from a not approved location."]
                        listID += 1
        #Yeti textures
        if len( self.mayaDeps.yetiTextureFiles) >= 1:
            for each in self.mayaDeps.yetiTextureFiles:
                if each["filePath"] != "":
                    if each["filePath"][0] not in approvedServers:
                        notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " Yeti Node: " + each["textureNode"] + ". File coming from a not approved location."]
                        listID += 1
        
        rList = self.formatList("Not approved file path", len(notValidFilePath), notValidFilePath, 100) 
        
        return rList
    
    
    def missing_yeti_cache(self):
        missingYetiCache = {}
        if self.checkSelectedOnly == True:
            missingYetiCacheList = {}
            listID = 1
            for node in self.checkNodes:
                if mc.nodeType(node) == "pgYetiMaya":
                    groomFilePath = mc.getAttr(node + ".cacheFileName")
                    if not mc.file(groomFilePath, query= True, exists= True):
                        missingYetiCacheList[listID] = [node, "Missing Yeti cache file."]
                        listID += 1
                if mc.nodeType(node) == "transform":
                    shape = mc.listRelatives(node, shapes=True, noIntermediate=True, fullPath=True)[0]
                    if mc.nodeType(shape) == "pgYetiMaya":
                        groomFilePath = mc.getAttr(shape + ".cacheFileName")
                        if not mc.file(groomFilePath, query= True, exists= True):
                            missingYetiCacheList[listID] = [shape, "Missing Yeti cache file."]
                            listID += 1
            missingYetiCache = missingYetiCacheList
        else:
            missingYetiCacheList = {}
            filesList= mc.ls(type="pgYetiMaya", long=True)
            if filesList != None:
                listID = 1
                for node in filesList:
                    if mc.nodeType(node) == "pgYetiMaya":
                        groomFilePath = mc.getAttr(node + ".cacheFileName")
                        if not mc.file(groomFilePath, query= True, exists= True):
                            missingYetiCacheList[listID] = [node, "Missing Yeti cache file."]
                            listID += 1
            missingYetiCache = missingYetiCacheList
        
        rList = self.formatList("Missing Yeti Cache", len(missingYetiCache), missingYetiCache, 100) 
        
        return rList
    
    
    def odd_yeti_node_name(self):
        oddYetiName = {}
        if self.checkSelectedOnly == True:
            oddYetiNameList = {}
            listID = 1
            for node in self.checkNodes:
                if mc.nodeType(node) == "transform":
                    shape = mc.listRelatives(node, shapes=True, noIntermediate=True, fullPath=True)[0]
                    if mc.nodeType(shape) == "pgYetiMaya":
                        if '|' not in node:
                            nameParts = node.split("_")
                            acceptedSides = ["C", "R", "L"]
                            if len(nameParts) < 5:
                                oddYetiNameList[listID] = [node, "Has incorrect number of name parts."]
                            elif len(nameParts) > 6:
                                oddYetiNameList[listID] = [node, "Has incorrect number of name parts."]
                            elif nameParts[0] not in acceptedSides:
                                oddYetiNameList[listID] = [node, "Name does not specify side of model with C, R or L."]
                            elif nameParts[-1] != "YETI":
                                oddYetiNameList[listID] = [node, "Name does not end with 'YETI'."]
                            elif nameParts[-1][-1].isdigit():
                                oddYetiNameList[listID] = [node, "Name shouldn't end with a number."]
                        elif '|' in node:
                            oddYetiNameList[listID] = [node, "Name might be duplicate."]
                        listID += 1
                elif mc.nodeType(node) == "pgYetiMaya":
                    transform = mc.listRelatives(node, parent=True)[0]
                    if '|' not in transform:
                        nameParts = transform.split("_")
                        acceptedSides = ["C", "R", "L"]
                        if len(nameParts) < 5:
                            oddYetiNameList[listID] = [transform, "Has incorrect number of name parts."]
                        elif len(nameParts) > 6:
                            oddYetiNameList[listID] = [transform, "Has incorrect number of name parts."]
                        elif nameParts[0] not in acceptedSides:
                            oddYetiNameList[listID] = [transform, "Name does not specify side of model with C, R or L."]
                        elif nameParts[-1] != "YETI":
                            oddYetiNameList[listID] = [transform, "Name does not end with 'YETI'."]
                        elif nameParts[-1][-1].isdigit():
                            oddYetiNameList[listID] = [transform, "Name shouldn't end with a number."]
                    elif '|' in node:
                        oddYetiNameList[listID] = [transform, "Name might be duplicate."]
                    listID += 1
            oddYetiName = oddYetiNameList
        else:
            oddYetiNameList = {}
            filesList= mc.ls(type="pgYetiMaya", long=True)
            if filesList != None:
                listID = 1
                for node in filesList:
                    if mc.nodeType(node) == "pgYetiMaya":
                        transform = mc.listRelatives(node, parent=True)[0]
                        if '|' not in transform:
                            nameParts = transform.split("_")
                            acceptedSides = ["C", "R", "L"]
                            if len(nameParts) < 5:
                                oddYetiNameList[listID] = [transform, "Has incorrect number of name parts."]
                            elif len(nameParts) > 6:
                                oddYetiNameList[listID] = [transform, "Has incorrect number of name parts."]
                            elif nameParts[0] not in acceptedSides:
                                oddYetiNameList[listID] = [transform, "Name does not specify side of model with C, R or L."]
                            elif nameParts[-1] != "YETI":
                                oddYetiNameList[listID] = [transform, "Name does not end with 'YETI'."]
                            elif nameParts[-1][-1].isdigit():
                                oddYetiNameList[listID] = [transform, "Name shouldn't end with a number."]
                        elif '|' in transform:
                            oddYetiNameList[listID] = [transform, "Name might be duplicate."]
                        listID += 1
            oddYetiName = oddYetiNameList
        
        rList = self.formatList("Odd Yeti Node Name", len(oddYetiName), oddYetiName, 50) 
        
        return rList
    
    
    def missing_texture_reference_object(self):
        missingTxtRefObj = {}
        if self.checkSelectedOnly == True:
            missingTxtRefObjList = {}
            listID = 1
            for node in self.checkNodes:
                if mc.nodeType(node) == "transform":
                    shape = mc.listRelatives(node, shapes=True, noIntermediate=True, fullPath=True)[0]
                    if mc.nodeType(shape) == "pgYetiMaya":
                        #List connected geometry
                        meshes = mc.listConnections(shape + ".inputGeometry", s=True, plugs= False)
                        if meshes != None:
                            for msh in meshes:
                                if mc.nodeType(msh) == "mesh":
                                    transform = mc.listRelatives(msh, parent=True, fullPath=True)[0]
                                    if not mc.getAttr(transform + ".template"):
                                        mshRef = mc.listConnections(msh + ".referenceObject", s=True, plugs= False)
                                        if mshRef == None:
                                            missingTxtRefObjList[listID] = [node, "Has input geometry without texture reference object. Check: " + msh]
                                            listID += 1
                                elif mc.nodeType(msh) == "transform":
                                    if not mc.getAttr(msh + ".template"):
                                        shape = mc.listRelatives(msh, shapes=True, noIntermediate=True, fullPath=True)[0]
                                        mshRef = mc.listConnections(shape + ".referenceObject", s=True, plugs= False)
                                        if mshRef == None:
                                            missingTxtRefObjList[listID] = [node, "Has input geometry without texture reference object. Check: " + msh]
                                            listID += 1
                if mc.nodeType(node) == "pgYetiMaya":
                        #List connected geometry
                        meshes = mc.listConnections(node + ".inputGeometry", s=True, plugs= False)
                        if meshes != None:
                            for msh in meshes:
                                if mc.nodeType(msh) == "mesh":
                                    transform = mc.listRelatives(msh, parent=True, fullPath=True)[0]
                                    if not mc.getAttr(transform + ".template"):
                                        mshRef = mc.listConnections(msh + ".referenceObject", s=True, plugs= False)
                                        if mshRef == None:
                                            missingTxtRefObjList[listID] = [node, "Has input geometry without texture reference object. Check: " + msh]
                                            listID += 1
                                elif mc.nodeType(msh) == "transform":
                                    if not mc.getAttr(msh + ".template"):
                                        shape = mc.listRelatives(msh, shapes=True, noIntermediate=True, fullPath=True)[0]
                                        mshRef = mc.listConnections(shape + ".referenceObject", s=True, plugs= False)
                                        if mshRef == None:
                                            missingTxtRefObjList[listID] = [node, "Has input geometry without texture reference object. Check: " + msh]
                                            listID += 1
            missingTxtRefObj = missingTxtRefObjList
        else:
            missingTxtRefObjList = {}
            filesList= mc.ls(type="pgYetiMaya", long=True)
            if filesList != None:
                listID = 1
                for node in filesList:
                    if mc.nodeType(node) == "pgYetiMaya":
                        #List connected geometry
                        meshes = mc.listConnections(node + ".inputGeometry", s=True, plugs= False)
                        if meshes != None:
                            for msh in meshes:
                                if mc.nodeType(msh) == "mesh":
                                    transform = mc.listRelatives(msh, parent=True, fullPath=True)[0]
                                    if not mc.getAttr(transform + ".template"):
                                        mshRef = mc.listConnections(msh + ".referenceObject", s=True, plugs= False)
                                        if mshRef == None:
                                            missingTxtRefObjList[listID] = [node, "Has input geometry without texture reference object. Check: " + msh]
                                            listID += 1
                                elif mc.nodeType(msh) == "transform":
                                    if not mc.getAttr(msh + ".template"):
                                        shape = mc.listRelatives(msh, shapes=True, noIntermediate=True, fullPath=True)[0]
                                        mshRef = mc.listConnections(shape + ".referenceObject", s=True, plugs= False)
                                        if mshRef == None:
                                            missingTxtRefObjList[listID] = [node, "Has input geometry without texture reference object. Check: " + msh]
                                            listID += 1
            missingTxtRefObj = missingTxtRefObjList
        
        rList = self.formatList("Missing Texture Reference Object", len(missingTxtRefObj), missingTxtRefObj, 100) 
        
        return rList
    
    
##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------            
def main():
    pass

if __name__ == "__main__": 
    main()