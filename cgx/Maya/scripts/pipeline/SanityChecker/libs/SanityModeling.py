# -*- coding: utf-8 -*-
'''
Methods for sanity checking on the modeling stage.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
Pending:
-Locked normals not working. It edits object normals instead of querying them.
Using query doesn't work either.
'''

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Sanity Modeling
##--------------------------------------------------------------------------------------------
class SanityModeling():
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _checkList, _checkNodes, _checkSelectedOnly):
        self.checkList = _checkList
        self.checkNodes = _checkNodes
        self.checkSelectedOnly = _checkSelectedOnly
        self.checkMethods = ["Odd geometry names", "Duplicate names", "Visibility Off", "NonZero Transforms", "NonZero Pivot",
                             "Multiple shapes", "Empty UV sets", "NonManifold geometry", "Lamina Faces", "History",
                             "Animation Curves", "Empty Namespace", "Existing Namespaces", "Display Layers"]
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def check (self, _checkList, _checkNodes, _checkSelectedOnly):
        """def that gets the checkList and calls the corresponding methods"""
        output = []
        self.checkList = _checkList
        self.checkSelectedOnly = _checkSelectedOnly
        self.checkNodes = _checkNodes
        issuesNumber = 0
        for chk in self.checkList:
            defName = chk.replace(" ", "_").lower()
            if defName in dir(self):
                result = eval('self.' + defName + '()')
                output += result[0]
                issuesNumber += result[1]
            else:
                msgBox = QtWidgets.QMessageBox()
                msgBox.setWindowTitle("Warning!")
                msgBox.setText("No method for " + defName + " was found.")
                msgBox.exec_()
        
        return output, issuesNumber
    
    
    def formatList(self, _checkType, _numIssues, _affectedNodes, _level):
        """Format a datalist with feedback for the user"""
        
        checkList = []
        for item in _affectedNodes.keys():
            checkList.append([ _level, _checkType, _affectedNodes[item][0], _affectedNodes[item][1] ])
        
        return checkList, _numIssues
    
    
    ##--------------------------------------------------------------------------------------------
    ##Checks
    ##--------------------------------------------------------------------------------------------
    def odd_geometry_names (self):
        """Check for non conventional naming on geometries"""
        oddNames = {}
        if self.checkSelectedOnly == True:
            oddNamesList = {}
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    if '|' not in obj:
                        nameParts = obj.split("_")
                        acceptedSides = ["C", "R", "L"]
                        if len(nameParts) < 5:
                            oddNamesList[listID] = [obj, "Has incorrect number of name parts."]
                        elif len(nameParts) > 6:
                            oddNamesList[listID] = [obj, "Has incorrect number of name parts."]
                        elif nameParts[0] not in acceptedSides:
                            oddNamesList[listID] = [obj, "Name does not specify side of model with C, R or L."]
                        elif nameParts[-1] != "MSH":
                            oddNamesList[listID] = [obj, "Name does not end with 'MSH'."]
                        elif nameParts[-1][-1].isdigit():
                            oddNamesList[listID] = [obj, "Name shouldn't end with a number."]
                    elif '|' in obj:
                        oddNamesList[listID] = [obj, "Name might be duplicate."]
                listID += 1
            oddNames = oddNamesList
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            #Get odd names
            oddNamesList = {}
            listID = 1
            for obj in allMeshTrans:
                if '|' not in obj:
                    nameParts = obj.split("_")
                    acceptedSides = ["C", "R", "L"]
                    if len(nameParts) < 5:
                        oddNamesList[listID] = [obj, "Has incorrect number of name parts."]
                    elif len(nameParts) > 6:
                        oddNamesList[listID] = [obj, "Has incorrect number of name parts."]
                    elif nameParts[0] not in acceptedSides:
                        oddNamesList[listID] = [obj, "Name does not specify side of model with C, R or L."]
                    elif nameParts[-1] != "MSH":
                        oddNamesList[listID] = [obj, "Name does not end with 'MSH'."]
                    elif nameParts[-1][-1].isdigit():
                        oddNamesList[listID] = [obj, "Name shouldn't end with a number."]
                elif '|' in obj:
                    oddNamesList[listID] = [obj, "Name might be duplicate."]
                listID += 1
            oddNames = oddNamesList
        
        rList = self.formatList("Odd geometry names", len(oddNames), oddNames, 100)

        return rList
        
    
    def duplicate_names (self):
        """Check for duplicate names in geometry"""
        dupNames = []
        dupNamesDict = {}
        if self.checkSelectedOnly == True:
            dupItemsList = []
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "mesh":
                    if '|' in obj:
                        dupItemsList.append(obj)
                elif mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                if '|' in obj:
                                    dupItemsList.append(obj)
                                break
            dupNames = dupItemsList
            listID = 1
            for each in dupNames:
                dupNamesDict[listID] = [each, "Duplicate name"]
                listID += 1
        else:
            #CHECK SHAPE NODES
            allShapes = mc.ls(type= "mesh", shortNames=True)
            dupShapesList = []
            #Get duplicated shapes
            for obj in allShapes:
                if '|' in obj:
                    dupShapesList.append(obj)
            
            #CHECK TRANSFORM NODES
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            dupTransList = []
            #Get duplicated transforms
            for obj in allMeshTrans:
                if '|' in obj:
                    dupTransList.append(obj)
            
            dupNames = dupTransList + dupShapesList
            listID = 1
            for each in dupNames:
                dupNamesDict[listID] = [each, "Duplicate name"]
                listID += 1
                        
        rList = self.formatList("Duplicate names", len(dupNamesDict), dupNamesDict, 100)
        
        return rList
    
    
    def visibility_off (self):
        """Check for geometry with off visibility"""
        visOff = []
        visOffDict = {}
        if self.checkSelectedOnly == True:
            visOffList = []
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "mesh":
                    if mc.getAttr(obj + ".visibility") == False:
                        visOffList.append(obj)
                elif mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                if mc.getAttr(obj + ".visibility") == False:
                                    visOffList.append(obj)
                                break
            visOff = visOffList
            listID = 1
            for each in visOff:
                visOffDict[listID] = [each, "Visibility off."]
                listID += 1
        else:
            #CHECK SHAPE NODES
            allShapes = mc.ls(type= "mesh", shortNames=True)
            shapesOffList = []
            #Get duplicated shapes
            for obj in allShapes:
                if mc.getAttr(obj + ".visibility") == False:
                    shapesOffList.append(obj)
            
            #CHECK TRANSFORM NODES
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            visOffTransList = []
            #Get visOff transforms
            for obj in allMeshTrans:
                if mc.getAttr(obj + ".visibility") == False:
                    visOffTransList.append(obj)
            
            visOff = visOffTransList + shapesOffList
            listID = 1
            for each in visOff:
                visOffDict[listID] = [each, "Visibility off."]
                listID += 1
        
        rList = self.formatList("Visibility Off", len(visOffDict), visOffDict, 50)
        
        return rList
    
    
    def nonzero_transforms (self):
        """Check for geometry with nonzero transforms"""
        nonZeroTrans = {}
        if self.checkSelectedOnly == True:
            nonZeroTransList = {}
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                translateObj = mc.xform(obj, query=True, translation=True,worldSpace=True)
                                rotateObj = mc.xform(obj, query=True, rotation=True,worldSpace=True)
                                scaleObj = mc.xform(obj, query=True, scale=True,worldSpace=True)
                                if translateObj[0] != 0 or translateObj[1] != 0 or translateObj[2] != 0 or rotateObj[0] != 0 or rotateObj[1] != 0 or rotateObj[2] != 0 or scaleObj[0] != 1 or scaleObj[1] != 1 or scaleObj[2] != 1:
                                    nonZeroTransList[listID] = [obj, "Has non-zero transforms."]
                                    listID += 1
                                break
            nonZeroTrans = nonZeroTransList
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            nonZeroTransList = {}
            listID = 1
            #Get nonzero transforms
            for obj in allMeshTrans:
                translateObj = mc.xform(obj, query=True, translation=True,worldSpace=True)
                rotateObj = mc.xform(obj, query=True, rotation=True,worldSpace=True)
                scaleObj = mc.xform(obj, query=True, scale=True,worldSpace=True)
                if translateObj[0] != 0 or translateObj[1] != 0 or translateObj[2] != 0 or rotateObj[0] != 0 or rotateObj[1] != 0 or rotateObj[2] != 0 or scaleObj[0] != 1 or scaleObj[1] != 1 or scaleObj[2] != 1:
                    nonZeroTransList[listID] = [obj, "Has non-zero transforms."]
                    listID += 1
            
            nonZeroTrans = nonZeroTransList
        
        rList = self.formatList("NonZero Transforms", len(nonZeroTrans), nonZeroTrans, 100)

        return rList
    
    
    def nonzero_pivot (self):
        """Check for geometry with nonzero pivot locations"""
        nonZeroPivot = {}
        if self.checkSelectedOnly == True:
            nonZeroPivotList = {}
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                pivot = mc.xform(obj, pivots= True, query= True, ws= True)
                                if pivot[0] != 0 or pivot[1] != 0 or pivot[2] != 0:
                                    nonZeroPivotList[listID] = [obj, "Has non-zero pivot."]
                                    listID += 1
                                break
            nonZeroPivot = nonZeroPivotList
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            nonZeroPivotList = {}
            listID = 1
            #Get nonzero pivots
            for obj in allMeshTrans:
                pivot = mc.xform(obj, pivots= True, query= True, ws= True)
                if pivot[0] != 0 or pivot[1] != 0 or pivot[2] != 0:
                    nonZeroPivotList[listID] = [obj, "Has non-zero pivot."]
                    listID += 1
            
            nonZeroPivot = nonZeroPivotList
        
        rList = self.formatList("NonZero Pivot", len(nonZeroPivot), nonZeroPivot, 100)

        return rList
    
    
    def multiple_shapes (self):
        """Check for geometry with multiple shape nodes"""
        multipleShapes = []
        multipleShapesDict = {}
        if self.checkSelectedOnly == True:
            multipleShapesList = []
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        if len(transShapes) > 1:
                            multipleShapesList.append(obj)
            multipleShapes = multipleShapesList
            listID = 1
            for each in multipleShapes:
                multipleShapesDict[listID] = [each, "Geometry with multiple shapes."]
                listID += 1
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            multipleShapesList = []
            #Get multiple shapes
            for obj in allMeshTrans:
                transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                if transShapes != None:
                    if len(transShapes) > 1:
                        multipleShapesList.append(obj)
            
            multipleShapes = multipleShapesList
            listID = 1
            for each in multipleShapes:
                multipleShapesDict[listID] = [each, "Geometry with multiple shapes."]
                listID += 1
        
        rList = self.formatList("Multiple shapes", len(multipleShapesDict), multipleShapesDict, 100)
        
        return rList
    
    
    def empty_uv_sets (self):
        """Check for geometry with empty uv sets"""
        emptyUVSets = []
        emptyUVSetsDict = {}
        if self.checkSelectedOnly == True:
            emptyUVSetsList = []
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "mesh":
                    objSets = mc.polyUVSet(obj, query= True, allUVSets= True)
                    if objSets != None:
                        for uvset in objSets:
                            if mc.polyEvaluate(obj, uvcoord= True, uvSetName= uvset) == 0:
                                emptyUVSetsList.append(obj)
                                break
                elif mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                objSets = mc.polyUVSet(shape, query= True, allUVSets= True)
                                if objSets != None:
                                    for uvset in objSets:
                                        if mc.polyEvaluate(shape, uvcoord= True, uvSetName= uvset) == 0:
                                            emptyUVSetsList.append(obj)
                                            break
                                break
            emptyUVSets = emptyUVSetsList
            listID = 1
            for each in emptyUVSets:
                emptyUVSetsDict[listID] = [each, "Geometry with empty uv sets."]
                listID += 1
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            emptyUVSetsList = []
            #Get meshes with empty uv sets
            for obj in allMeshTrans:
                objSets = mc.polyUVSet(obj, query= True, allUVSets= True) 
                if objSets != None:
                    for uvset in objSets:
                        if mc.polyEvaluate(obj, uvcoord= True, uvSetName= uvset) == 0:
                            emptyUVSetsList.append(obj)
                            break
            emptyUVSets = emptyUVSetsList
            listID = 1
            for each in emptyUVSets:
                emptyUVSetsDict[listID] = [each, "Geometry with empty uv sets."]
                listID += 1
        
        rList = self.formatList("Empty UV sets", len(emptyUVSetsDict), emptyUVSetsDict, 100)
        
        return rList
    
    
    def nonmanifold_geometry (self):
        """Check for nonmanifold geometry"""
        nonmanifold = {}
        if self.checkSelectedOnly == True:
            nonmanifoldList = {}
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "mesh":
                    nonManiEdges = mc.polyInfo(obj, nonManifoldEdges= True)
                    if nonManiEdges == None:
                        nonManiEdges = []
                    nonManiVtx = mc.polyInfo(obj, nonManifoldVertices= True)
                    if nonManiVtx == None:
                        nonManiVtx = []
                    if len(nonManiEdges) >= 1 or len(nonManiVtx) >= 1:
                        nonmanifoldList[listID] = [obj, "{" + ", ".join(nonManiEdges) + "} {" + ", ".join(nonManiVtx) +"}"]
                        listID += 1
                elif mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                nonManiEdges = mc.polyInfo(obj, nonManifoldEdges= True)
                                if nonManiEdges == None:
                                    nonManiEdges = []
                                nonManiVtx = mc.polyInfo(obj, nonManifoldVertices= True)
                                if nonManiVtx == None:
                                    nonManiVtx = []
                                if nonManiEdges != None or nonManiVtx != None:
                                    if len(nonManiEdges) >= 1 or len(nonManiVtx) >= 1:
                                        nonmanifoldList[listID] = [obj, "{" + ", ".join(nonManiEdges) + "} {" + ", ".join(nonManiVtx) +"}"]
                                        listID += 1
                                break
            nonmanifold = nonmanifoldList
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            nonmanifoldList = {}
            #Get meshes with empty uv sets
            listID = 1
            for obj in allMeshTrans:
                nonManiEdges = mc.polyInfo(obj, nonManifoldEdges= True)
                if nonManiEdges == None:
                    nonManiEdges = []
                nonManiVtx = mc.polyInfo(obj, nonManifoldVertices= True)
                if nonManiVtx == None:
                    nonManiVtx = []
                if len(nonManiEdges) >= 1 or len(nonManiVtx) >= 1:
                    nonmanifoldList[listID] = [obj, "{" + ", ".join(nonManiEdges) + "} {" + ", ".join(nonManiVtx) +"}"]
                    listID += 1
            nonmanifold = nonmanifoldList
            
        rList = self.formatList("NonManifold geometry", len(nonmanifold), nonmanifold, 50)
        
        return rList
    
    
    def lamina_faces (self):
        """Check for lamina faces"""
        laminafaces = {}
        if self.checkSelectedOnly == True:
            laminafacesList = {}
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "mesh":
                    lamina = mc.polyInfo(obj, laminaFaces= True)
                    if lamina == None:
                        lamina = []
                    if len(lamina) >= 1:
                        laminafacesList[listID] = [obj, "{" + ", ".join(lamina) + "}"]
                        listID += 1
                elif mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                lamina = mc.polyInfo(obj, laminaFaces= True)
                                if lamina == None:
                                    lamina = []
                                if len(lamina) >= 1:
                                    laminafacesList[listID] = [obj, "{" + ", ".join(lamina) + "}"]
                                    listID += 1
                                break
            laminafaces = laminafacesList
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            laminafacesList = {}
            #Get meshes with empty uv sets
            listID = 1
            for obj in allMeshTrans:
                lamina = mc.polyInfo(obj, laminaFaces= True)
                if lamina == None:
                    lamina = []
                if len(lamina) >= 1:
                    laminafacesList[listID] = [obj, "{" + ", ".join(lamina) + "}"]
                    listID += 1
            laminafaces = laminafacesList
        
        rList = self.formatList("Lamina Faces", len(laminafaces), laminafaces, 50)
        
        return rList
    
    
    def history (self):
        """Check for geometry with history nodes"""
        history = []
        historyDict = {}
        if self.checkSelectedOnly == True:
            historyList = []
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "mesh":
                    objHist = mc.listHistory(obj)
                    if objHist == None:
                        objHist = []
                    if len(objHist) > 1:
                        historyList.append(obj)
                elif mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                objHist = mc.listHistory(obj)
                                if objHist == None:
                                    objHist = []
                                if len(objHist) > 1:
                                    historyList.append(obj)
                                break
            history= historyList
            listID = 1
            for each in history:
                historyDict[listID] = [each, "Geometry has history."]
                listID += 1
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            historyList = []
            #Get meshes with empty uv sets
            for obj in allMeshTrans:
                objHist = mc.listHistory(obj)
                if objHist == None:
                    objHist = []
                if len(objHist) > 1:
                    historyList.append(obj)
            history = historyList
            listID = 1
            for each in history:
                historyDict[listID] = [each, "Geometry has history."]
                listID += 1
        
        rList = self.formatList("History", len(historyDict), historyDict, 100)
        
        return rList
    
    
    def locked_normals (self):
        """Check for geometry with locked normals"""
        lockedNormals = []
        lockedNormalsDict = {}
        if self.checkSelectedOnly == True:
            lockedNormalsList = []
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "mesh":
                    lckNorm = mc.polyNormalPerVertex(obj, query= True, allLocked= True)
                    if lckNorm != None:
                        lockedNormalsList.append(obj)
                elif mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                lckNorm = mc.polyNormalPerVertex(obj, query= True, allLocked= True)
                                if lckNorm != None:
                                    lockedNormalsList.append(obj)
                                break
            lockedNormals= lockedNormalsList
            listID = 1
            for each in lockedNormals:
                lockedNormalsDict[listID] = [each, "Geometry has locked normals."]
                listID += 1
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            lockedNormalsList = []
            #Get meshes with empty uv sets
            for obj in allMeshTrans:
                lckNorm = mc.polyNormalPerVertex(obj, query= True, allLocked= True)
                if lckNorm != None:
                    lockedNormalsList.append(obj)
            lockedNormals= lockedNormalsList
            listID = 1
            for each in lockedNormals:
                lockedNormalsDict[listID] = [each, "Geometry has locked normals."]
                listID += 1

        rList = self.formatList("Locked normals", len(lockedNormalsDict), lockedNormalsDict, 100)
        
        return rList
    
    
    def animation_curves (self):
        """Check for geometry with animation"""
        animationCurves = []
        animationCurvesDict = {}
        if self.checkSelectedOnly == True:
            animationCurvesList = []
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    if mc.keyframe(obj, time=(':',), query=True, keyframeCount=True):
                        animationCurvesList.append(obj)
            animationCurves= animationCurvesList
            listID = 1
            for each in animationCurves:
                animationCurvesDict[listID] = [each, "Has animation curves."]
                listID += 1
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            animationCurvesList = []
            #Get meshes with animation
            for obj in allMeshTrans:
                if mc.nodeType(obj) == "transform":
                    if mc.keyframe(obj, time=(':',), query=True, keyframeCount=True):
                        animationCurvesList.append(obj)
            animationCurves= animationCurvesList
            listID = 1
            for each in animationCurves:
                animationCurvesDict[listID] = [each, "Has animation curves."]
                listID += 1

        rList = self.formatList("Animation Curves", len(animationCurvesDict), animationCurvesDict, 100)
        
        return rList
    
    
    def existing_namespaces(self):
        """Check for existing namespaces"""
        existingNamespaces = []
        initNamespacesList = mc.namespaceInfo(":", listOnlyNamespaces= True)
        defaultNamespaces = ["UI","shared"]
        namespacesList = []
        for each in initNamespacesList:
            if each not in defaultNamespaces:
                namespacesList.append(each)
                
        if namespacesList != None or len(namespacesList) >= 1:
            for each in namespacesList:
                thisChildren = self.existing_namespace_recursive(each)
                existingNamespaces.append(each)
                existingNamespaces += thisChildren
        
        existingNamespacesDict = {}
        listID = 1
        for each in existingNamespaces:
            existingNamespacesDict[listID] = [each, "Existing Namespace."]
            listID += 1
        
        rList = self.formatList("Existing Namespaces", len(existingNamespacesDict), existingNamespacesDict, 100)
        
        return rList
        
    
    def existing_namespace_recursive(self, _namespace):
        allChildNamespaces = mc.namespaceInfo(_namespace, listOnlyNamespaces= True)
        if allChildNamespaces != None:
            finalChildren = []
            for each in allChildNamespaces:
                thisChildren = self.existing_namespace_recursive(each)
                if len(thisChildren) >= 1:
                    finalChildren += thisChildren
            return allChildNamespaces + finalChildren
        else:
            return []
    
    
    def empty_namespace (self):
        """Check for empty namespaces"""
        emptyNamespaces = []
        currentNamespace = mc.namespaceInfo( currentNamespace=True)
        if mc.namespaceInfo(currentNamespace, isRootNamespace= True):
            allChildNamespaces = mc.namespaceInfo(currentNamespace, listOnlyNamespaces= True)
            if allChildNamespaces != None:
                for each in allChildNamespaces:
                    thisEmpty = self.empty_namespace_recursive(each)
                    if thisEmpty != None:
                        emptyNamespaces.append(thisEmpty)
        
        emptyNamespacesDict = {}
        listID = 1
        for each in emptyNamespaces:
            emptyNamespacesDict[listID] = [each, "EMPTY Namespace."]
            listID += 1
        
        rList = self.formatList("Empty Namespaces", len(emptyNamespacesDict), emptyNamespacesDict, 70)
        
        return rList
        
    
    def empty_namespace_recursive(self, _namespace):
        allChildNamespaces = mc.namespaceInfo(_namespace, listOnlyNamespaces= True)
        if allChildNamespaces != None:
            for each in allChildNamespaces:
                thisEmpty = self.empty_namespace_recursive(each)
                if thisEmpty != None:
                    return thisEmpty
        elif mc.namespaceInfo(_namespace, listNamespace=True) == None:
            if _namespace != "shared":
                return _namespace
        else:
            return None
    
    
    def display_layers(self):
        dispLayers = {}
        listID = 1
        for each in mc.ls(type="displayLayer"):
            if each != "defaultLayer":
                dispLayers[listID] = [each, "Delete all display layers before publishing."]
            listID += 1
        
        rList = self.formatList("Display Layers", len(dispLayers), dispLayers, 70)
        
        return rList
    

##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------            
def main():
    pass


if __name__ == "__main__": 
    main()