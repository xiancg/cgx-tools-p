# -*- coding: utf-8 -*-
'''
Methods for sanity checking on the lighting stage.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets
from cgx.Maya.scripts.maya_libs.Maya_Dependencies import Maya_Dependencies
from cgx.Maya.scripts.maya_libs.Maya_ProjectInfo import Maya_ProjectInfo
from cgx.core.StudioInfo import StudioInfo


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Sanity lighting
##--------------------------------------------------------------------------------------------
class SanityLighting():
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _checkList, _checkNodes, _checkSelectedOnly):
        self.checkList = _checkList
        self.checkNodes = _checkNodes
        self.checkSelectedOnly = _checkSelectedOnly
        self.mayaDeps = Maya_Dependencies()
        self.checkMethods = ["Odd light names", "Duplicate names", "References failed edits", "Empty render path", "Rendereable camera", "Off render layers",
                             "Arnold min sampling settings", "Arnold gamma settings", "Arnold general settings", "Arnold motion blur", "Vray gamma settings", "Vray general settings",
                             "Not approved file path", "Odd File Name", "Film Aspect Ratio", "Render path issues",
                             "Arnold light samples high", "Arnold light samples low", "Arnold catclark subdivision"]
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def check (self, _checkList, _checkNodes, _checkSelectedOnly):
        """def that gets the checkList and calls the corresponding methods"""
        output = []
        self.checkList = _checkList
        self.checkSelectedOnly = _checkSelectedOnly
        self.checkNodes = _checkNodes
        issuesNumber = 0
        for chk in self.checkList:
            defName = chk.replace(" ", "_").lower()
            if defName in dir(self):
                result = eval('self.' + defName + '()')
                output += result[0]
                issuesNumber += result[1]
            else:
                msgBox = QtWidgets.QMessageBox()
                msgBox.setWindowTitle("Warning!")
                msgBox.setText("No method for " + defName + " was found.")
                msgBox.exec_()
        
        return output, issuesNumber
    
    
    def formatList(self, _checkType, _numIssues, _affectedNodes, _level):
        """Format a datalist with feedback for the user"""
        
        checkList = []
        for item in _affectedNodes.keys():
            checkList.append([ _level, _checkType, _affectedNodes[item][0], _affectedNodes[item][1] ])
        
        return checkList, _numIssues
    
    
    ##--------------------------------------------------------------------------------------------
    ##Checks
    ##--------------------------------------------------------------------------------------------
    def odd_light_names (self):
        """Check for odd light names"""
        oddNames = {}
        if self.checkSelectedOnly == True:
            oddNamesDict = {}
            lightNodes = mc.listNodeTypes('light')
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) in lightNodes:
                    lgtTransform = mc.listRelatives(parent=True, fullPath= True)
                    nameParts = lgtTransform[0].split("_")
                    if '|' not in lgtTransform[0]:
                        if len(nameParts) < 4:
                            oddNamesDict[listID] = [lgtTransform[0], "Has incorrect number of name parts."]
                        elif len(nameParts) > 5:
                            oddNamesDict[listID] = [lgtTransform[0], "Has incorrect number of name parts."]
                        elif nameParts[-1][-1].isdigit():
                            oddNamesDict[listID] = [lgtTransform[0], "Name shouldn't end with a number."]
                        elif nameParts[0] != "LGT":
                            oddNamesDict[listID] = [lgtTransform[0], "Name does not start with 'LGT'."]
                    elif '|' in lgtTransform[0]:
                        oddNamesDict[listID] = [lgtTransform[0], "Name might be duplicate."]
                elif mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) in lightNodes:
                                if '|' not in obj:
                                    if len(nameParts) < 4:
                                        oddNamesDict[listID] = [obj, "Has incorrect number of name parts."]
                                    elif len(nameParts) > 5:
                                        oddNamesDict[listID] = [obj, "Has incorrect number of name parts."]
                                    elif nameParts[-1][-1].isdigit():
                                        oddNamesDict[listID] = [obj, "Name shouldn't end with a number."]
                                    elif nameParts[0] != "LGT":
                                        oddNamesDict[listID] = [obj, "Name does not start with 'LGT'."]
                                elif '|' in obj:
                                    oddNamesDict[listID] = [obj, "Name might be duplicate."]
                                break
                listID += 1
            oddNames = oddNamesDict
        else:
            #Filter all lights
            allLights = mc.ls(lights= True, shortNames=True)
            oddNamesDict = {}
            lightNodes = mc.listNodeTypes('light')
            listID = 1
            for obj in allLights:
                if mc.nodeType(obj) in lightNodes:
                    transform = mc.listRelatives(obj, parent=True)[0]
                    nameParts = transform.split("_")
                    if '|' not in transform:
                        if len(nameParts) < 4:
                            oddNamesDict[listID] = [transform, "Has incorrect number of name parts."]
                        elif len(nameParts) > 5:
                            oddNamesDict[listID] = [transform, "Has incorrect number of name parts."]
                        elif nameParts[-1][-1].isdigit():
                            oddNamesDict[listID] = [transform, "Name shouldn't end with a number."]
                        elif nameParts[0] != "LGT":
                            oddNamesDict[listID] = [transform, "Name does not start with 'LGT'."]
                    elif '|' in transform:
                        oddNamesDict[listID] = [transform, "Name might be duplicate."]
                listID += 1
            oddNames = oddNamesDict
        
        rList = self.formatList("Odd light names", len(oddNames), oddNames, 1)

        return rList
    
    
    def duplicate_names (self):
        """Check for lights with duplicate names"""
        dupNames = {}
        if self.checkSelectedOnly == True:
            dupNamesDict = {}
            lightNodes = mc.listNodeTypes('light')
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) in lightNodes:
                    lgtTransform = mc.listRelatives(parent=True)
                    if '|' in lgtTransform[0]:
                        dupNamesDict[listID] = [lgtTransform[0], "Name might be duplicate."]
                elif mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) in lightNodes:
                                if '|' in obj:
                                    dupNamesDict[listID] = [obj, "Name might be duplicate."]
                                break
                listID += 1
            dupNames = dupNamesDict
        else:
            #Filter all lights
            allLights = mc.ls(lights= True, shortNames=True)
            dupNamesDict = {}
            lightNodes = mc.listNodeTypes('light')
            listID = 1
            for obj in allLights:
                if mc.nodeType(obj) in lightNodes:
                    if '|' in obj:
                        dupNamesDict[listID] = [obj, "Name might be duplicate."]
                listID += 1
            dupNames = dupNamesDict
        
        rList = self.formatList("Duplicate names", len(dupNames), dupNames, 1)
        
        return rList
    
    
    def references_failed_edits (self):
        """Check for references with failed edits"""
        failedEdits = {}
        failedEditsDict = {}
        references = mc.file(query= True, reference= True)
        listID = 1
        for referencedFile in references:
            failed = mc.referenceQuery(referencedFile, editAttrs= True, failedEdits= True)
            if failed != None:
                if len(failed) >= 1:
                    failedEditsDict[listID] = [referencedFile, "Reference has failed edits."]
            listID += 1
        failedEdits = failedEditsDict
        
        rList = self.formatList("References failed edits", len(failedEdits), failedEdits, 50)
        
        return rList
    
    
    def empty_render_path (self):
        """Check for empty render path"""
        emptyRenderPath = {}
        renderPath = mc.getAttr("defaultRenderGlobals.imageFilePrefix")
        listID = 1
        if renderPath == None or renderPath == "":
            emptyRenderPath[listID] = ["defaultRenderGlobals", "Render path has not been set."]
        
        rList = self.formatList("Empty render path", len(emptyRenderPath), emptyRenderPath, 100)
        
        return rList
    
    
    def rendereable_camera (self):
        """Check if there are no rendereable cameras"""
        renderCam = {}
        allCams = mc.ls(cameras= True)
        renderCamTrue = []
        listID = 1
        for cam in allCams:
            if mc.getAttr(cam + ".renderable"):
                camTrans = mc.listRelatives(cam, parent= True)
                renderCamTrue.append(camTrans)
                if camTrans[0] == "persp":
                    renderCam[listID] = [camTrans[0], "This camera is set as rendereable."]
            listID += 1
        if len(renderCamTrue) < 1:
            renderCam[listID] = ["allCameras", "There are no rendereable cameras."]
        
        rList = self.formatList("Rendereable camera", len(renderCam), renderCam, 100)
        
        return rList
    
    
    def off_render_layers (self):
        """Check for off render layers"""
        offRL = {}
        allRL = mc.ls(type= "renderLayer")
        listID = 1
        for rl in allRL:
            rlStatus = mc.getAttr(rl + ".renderable")
            if not rlStatus:
                offRL[listID] = [rl, "Render layer is off."]
            listID += 1
        
        rList = self.formatList("Off render layers", len(offRL), offRL, 1)
        
        return rList
    

    def arnold_min_sampling_settings (self):
        """Check Arnold minimum sampling settings"""
        arnoldMinSettings = {}
        arnoldLoaded = mc.pluginInfo("mtoa", query=True, loaded=True)
        currRender = mc.getAttr("defaultRenderGlobals.currentRenderer")
        if arnoldLoaded:
            if currRender == "arnold":
                allLayers  = mc.ls(type="renderLayer")
                #Ignore disconnected layers but alert about them
                functionalLayers = []
                disconnectedLayers = []
                for lyr in allLayers:
                    lyrConnections = mc.listConnections(lyr + '.identification', d=False, s=True)
                    if lyrConnections == None:
                        disconnectedLayers.append(lyr)
                    else:
                        functionalLayers.append(lyr)
                listID = 1
                if len(disconnectedLayers) >= 1:
                    arnoldMinSettings[listID] = ['disconnectedRenderLayers', ', '.join(disconnectedLayers)]
                    listID += 1
                for lyr in functionalLayers:
                    if not mc.referenceQuery(lyr, isNodeReferenced= True):
                        if lyr != "defaultRenderLayer":
                            if mc.objExists(lyr):
                                antialias = mc.getAttr("defaultArnoldRenderOptions.AASamples")
                                diffuse = mc.getAttr("defaultArnoldRenderOptions.GIDiffuseSamples")
                                sss = mc.getAttr("defaultArnoldRenderOptions.GISssSamples")
                                if antialias < 3:
                                    arnoldMinSettings[listID] = ['defaultArnoldRenderOptions', "AA Samples are below 3."]
                                    listID += 1
                                if diffuse < 2:
                                    arnoldMinSettings[listID] = ['defaultArnoldRenderOptions', "Diffuse Samples are below 2."]
                                    listID += 1
                                if sss < 2:
                                    arnoldMinSettings[listID] = ['defaultArnoldRenderOptions', "SSS Samples are below 2."]
                                    listID += 1
                                for light in mc.ls(lights= True, shortNames=True):
                                    if mc.getAttr(light + ".aiSamples") < 3:
                                        arnoldMinSettings[listID] = [light, "Light samples are below 2."]
                                        listID += 1
                                if int(mc.pluginInfo('mtoa', version=True, query=True)[0]) < 2: #Sampling attributes have changed since version 2
                                    glossy = mc.getAttr("defaultArnoldRenderOptions.GIGlossySamples")
                                    refraction = mc.getAttr("defaultArnoldRenderOptions.GIRefractionSamples")
                                    if glossy < 2:
                                        arnoldMinSettings[listID] = ['defaultArnoldRenderOptions', "Glossy Samples are below 2."]
                                        listID += 1
                                    if refraction < 2:
                                        arnoldMinSettings[listID] = ['defaultArnoldRenderOptions', "Refraction Samples are below 2."]
                                        listID += 1
                                else:
                                    specular = mc.getAttr("defaultArnoldRenderOptions.GISpecularSamples")
                                    transmission = mc.getAttr("defaultArnoldRenderOptions.GITransmissionSamples")
                                    volume = mc.getAttr("defaultArnoldRenderOptions.GIVolumeSamples")
                                    if specular < 2:
                                        arnoldMinSettings[listID] = ['defaultArnoldRenderOptions', "Diffuse Samples are below 2."]
                                        listID += 1
                                    if transmission < 2:
                                        arnoldMinSettings[listID] = ['defaultArnoldRenderOptions', "Transmission Samples are below 2."]
                                        listID += 1
                                    if volume < 2:
                                        arnoldMinSettings[listID] = ['defaultArnoldRenderOptions', "Volume Samples are below 2."]
                                        listID += 1
        
        rList = self.formatList("Arnold min sampling settings", len(arnoldMinSettings), arnoldMinSettings, 20)                
        
        return rList
    
    
    def arnold_gamma_settings (self):
        """Check Arnold gamma settings"""
        arnoldGamma = {}
        arnoldLoaded = mc.pluginInfo("mtoa", query=True, loaded=True)
        currRender = mc.getAttr("defaultRenderGlobals.currentRenderer")
        if arnoldLoaded:
            if currRender == "arnold":
                if int(mc.pluginInfo('mtoa', version=True, query=True)[0]) < 2:
                    listID = 1
                    displayGamma = mc.getAttr("defaultArnoldRenderOptions.display_gamma")
                    lightsGamma = mc.getAttr("defaultArnoldRenderOptions.light_gamma")
                    shadersGamma = mc.getAttr("defaultArnoldRenderOptions.shader_gamma")
                    texturesGamma = mc.getAttr("defaultArnoldRenderOptions.texture_gamma")
                    if displayGamma != 1.0:
                        arnoldGamma[listID] = ['defaultArnoldRenderOptions','Display Gamma is not 1.0']
                        listID += 1
                    if round(lightsGamma,1) != 1.0:
                        arnoldGamma[listID] = ['defaultArnoldRenderOptions','Lights Gamma is not 1.0']
                        listID += 1
                    if shadersGamma != 1.0:
                        arnoldGamma[listID] = ['defaultArnoldRenderOptions','Shaders Gamma is not 1.0']
                        listID += 1
                    if texturesGamma != 1.0:
                        arnoldGamma[listID] = ['defaultArnoldRenderOptions','Textures Gamma is not 1.0']
                        listID += 1
                
        rList = self.formatList("Arnold gamma settings", len(arnoldGamma), arnoldGamma, 100)   
        
        return rList
    
    
    def arnold_general_settings (self):
        """Check for Arnold general settings"""
        generalSets = {}
        arnoldLoaded = mc.pluginInfo("mtoa", query=True, loaded=True)
        currRender = mc.getAttr("defaultRenderGlobals.currentRenderer")
        if arnoldLoaded:
            if currRender == "arnold":
                allLayers  = mc.ls(type="renderLayer")
                #Ignore disconnected layers but alert about them
                functionalLayers = []
                disconnectedLayers = []
                for lyr in allLayers:
                    lyrConnections = mc.listConnections(lyr + '.identification', d=False, s=True)
                    if lyrConnections == None:
                        disconnectedLayers.append(lyr)
                    else:
                        functionalLayers.append(lyr)
                listID = 1
                if len(disconnectedLayers) >= 1:
                    generalSets[listID] = ['disconnectedRenderLayers', ', '.join(disconnectedLayers)]
                    listID += 1
                for lyr in functionalLayers:
                    if not mc.referenceQuery(lyr, isNodeReferenced= True):
                        if lyr != "defaultRenderLayer":
                            if mc.objExists(lyr):
                                mc.editRenderLayerGlobals(currentRenderLayer= lyr)
                                mergeSwitch = mc.getAttr("defaultArnoldDriver.mergeAOVs")
                                txTextures = mc.getAttr("defaultArnoldRenderOptions.use_existing_tiled_textures")
                                verbosity = mc.getAttr("defaultArnoldRenderOptions.log_verbosity")
                                halfPrecision = mc.getAttr("defaultArnoldDriver.halfPrecision")
                                if not mergeSwitch:
                                    generalSets[listID] = ['defaultArnoldDriver', "Merge AOVs is not active in Arnold Driver for render layer: {}.".format(lyr)]
                                    listID += 1
                                if not txTextures:
                                    generalSets[listID] = ['defaultArnoldRenderOptions', "Use .TX textures is not active for render layer: {}.".format(lyr)]
                                    listID += 1
                                if verbosity != 0:
                                    generalSets[listID] = ['defaultArnoldRenderOptions', "Verbosity level should be set to Errors for render layer: {}.".format(lyr)]
                                    listID += 1
                                if not halfPrecision:
                                    if "FULLFLOAT" not in lyr or "TECH" not in lyr:
                                        generalSets[listID] = ['defaultArnoldDriver', "Half precision bit depth is not active for render layer: {}.".format(lyr)]
                                        listID += 1
                                
                preMel = mc.getAttr("defaultRenderGlobals.preMel")
                postMel = mc.getAttr("defaultRenderGlobals.postMel")
                if preMel == None or postMel == None:
                    generalSets[listID] = ['defaultRenderGlobals',"Render Callbacks found."]
                    listID += 1
        
        rList = self.formatList("Arnold general settings", len(generalSets), generalSets, 100) 
        
        return rList
    
    
    def arnold_motion_blur (self):
        """Check for Arnold motion blur settings"""
        generalSets = {}
        arnoldLoaded = mc.pluginInfo("mtoa", query=True, loaded=True)
        currRender = mc.getAttr("defaultRenderGlobals.currentRenderer")
        if arnoldLoaded:
            if currRender == "arnold":
                allLayers  = mc.ls(type="renderLayer")
                #Ignore disconnected layers but alert about them
                functionalLayers = []
                disconnectedLayers = []
                for lyr in allLayers:
                    lyrConnections = mc.listConnections(lyr + '.identification', d=False, s=True)
                    if lyrConnections == None:
                        disconnectedLayers.append(lyr)
                    else:
                        functionalLayers.append(lyr)
                listID = 1
                if len(disconnectedLayers) >= 1:
                    generalSets[listID] = ['disconnectedRenderLayers', ', '.join(disconnectedLayers)]
                    listID += 1
                for lyr in functionalLayers:
                    if not mc.referenceQuery(lyr, isNodeReferenced= True):
                        if lyr != "defaultRenderLayer":
                            if mc.objExists(lyr):
                                mc.editRenderLayerGlobals(currentRenderLayer= lyr)
                                motionBlur = mc.getAttr("defaultArnoldRenderOptions.motion_blur_enable")
                                aovsList = mc.ls(type='aiAOV')
                                motionVectorAOV = None
                                if len(aovsList):
                                    for aov in aovsList:
                                        if mc.getAttr(aov + '.name') == 'motionvector' and mc.getAttr(aov + '.enabled'):
                                            motionVectorAOV = aov
                                            break
                                if not motionBlur:
                                    generalSets[listID] = ['defaultArnoldDriver', "Motion Blur is not active for render layer: {}.".format(lyr)]
                                    listID += 1
                                if motionVectorAOV != None:
                                    aovMode = mc.getAttr("defaultArnoldRenderOptions.aovMode")
                                    if aovMode == 1:
                                        for cam in mc.ls(type='camera'):
                                            if mc.getAttr(cam + '.renderable'):
                                                motionBlurPosition = mc.getAttr("defaultArnoldRenderOptions.range_type")
                                                shutterValue = 0.5
                                                if motionBlurPosition == 0:
                                                    shutterValue = 0
                                                elif motionBlurPosition == 2:
                                                    shutterValue = 1
                                                if mc.getAttr(cam + '.aiShutterEnd') != shutterValue or mc.getAttr(cam + '.aiShutterStart') != shutterValue:
                                                    generalSets[listID] = [lyr,"Motion Vector AOV found but renderable camera's {} shutter attributes are not {} for render layer: {}.".format(cam, str(shutterValue), lyr)]
                                                    listID += 1
                                                if not motionBlur:
                                                    generalSets[listID] = [lyr,"Motion Vector AOV found but Motion Blur is not active for render layer: {}.".format(lyr)]
                                                    listID += 1
        
        rList = self.formatList("Arnold motion blur", len(generalSets), generalSets, 40) 
        
        return rList
    
    
    def vray_gamma_settings (self):
        """Check for non conventional naming on geometries"""
        vrayGamma = {}
        vrayLoaded = mc.pluginInfo("vrayformaya", query=True, loaded=True)
        currRender = mc.getAttr("defaultRenderGlobals.currentRenderer")
        if vrayLoaded:
            if currRender == "vray":
                listID = 1
                colorMappingType = mc.getAttr("vraySettings.cmap_type")
                darkMult = mc.getAttr("vraySettings.cmap_darkMult")
                brightMult = mc.getAttr("vraySettings.cmap_brightMult")
                gamma = mc.getAttr("vraySettings.cmap_gamma")
                adaptOnly = mc.getAttr("vraySettings.cmap_adaptationOnly")
                defaultViewerVray = mc.getAttr("vraySettings.sRGBOn")
                imgColorProfile = mc.getAttr("defaultViewColorManager.imageColorProfile")
                dispColorProfile = mc.getAttr("defaultViewColorManager.displayColorProfile")
                if colorMappingType != 0:
                    vrayGamma[listID] = ['vraySettings',"Color mapping type should be set to Linear Multiply."]
                    listID += 1
                if darkMult != 1.0:
                    vrayGamma[listID] = ['vraySettings',"Color mapping dark multiplier should be set to 1.0"]
                    listID += 1
                if brightMult != 1.0:
                    vrayGamma[listID] = ['vraySettings',"Color mapping bright multiplier should be set to 1.0"]
                    listID += 1
                if gamma != 1.0:
                    vrayGamma[listID] = ['vraySettings',"Color mapping gamma should be set to 1.0"]
                    listID += 1
                if not adaptOnly:
                    vrayGamma[listID] = ['vraySettings',"Don't affect colors option should be active."]
                    listID += 1
                if not defaultViewerVray:
                    vrayGamma[listID] = ['vraySettings',"Convert image to sRGB for Render View option should be active."]
                    listID += 1
                if imgColorProfile != 3:
                    vrayGamma[listID] = ['defaultViewColorManager',"Viewer's image color profile is not sRGB."]
                    listID += 1
                if dispColorProfile != 3:
                    vrayGamma[listID] = ['defaultViewColorManager',"Viewer's display color profile is not sRGB."]
                    listID += 1
        
        rList = self.formatList("Vray gamma settings", len(vrayGamma), vrayGamma, 100)       
        
        return rList
    
    
    def vray_general_settings (self):
        """Check for Arnold general settings"""
        generalSets = {}
        vrayLoaded = mc.pluginInfo("vrayformaya", query=True, loaded=True)
        currRender = mc.getAttr("defaultRenderGlobals.currentRenderer")
        if vrayLoaded:
            if currRender == "vray":
                listID = 1
                maxSubdivs = mc.getAttr("vraySettings.ddisplac_maxSubdivs")
                if maxSubdivs > 5:
                    generalSets[listID] = ['vraySettings',"VRay global max subdivs is higher than 5."]
                    listID += 1
        
        rList = self.formatList("Vray general settings", len(generalSets), generalSets, 100) 
        
        return rList
    
    
    def not_approved_file_path (self):
        """Check for not approved file locations"""
        notValidFilePath = {}
        approvedServers = ['G','F','Z']
        listID = 1
        #Texture files
        if len( self.mayaDeps.textureFiles) >= 1:
            for each in self.mayaDeps.textureFiles:
                if each["filePath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " File coming from a not approved location."]
                    listID += 1
        #Point Cache files
        if len( self.mayaDeps.pointCacheFiles) >= 1:
            for each in self.mayaDeps.pointCacheFiles:
                if each["xmlPath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["xmlPath"] + " File coming from a not approved location."]
                    listID += 1
        #Alembic files
        if len( self.mayaDeps.alembicFiles) >= 1:
            for each in self.mayaDeps.alembicFiles:
                if each["filePath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " File coming from a not approved location."]
                    listID += 1
        #Referenced files
        if len( self.mayaDeps.referenceFiles) >= 1:
            for each in self.mayaDeps.referenceFiles:
                if each["filePath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " File coming from a not approved location."]
                    listID += 1
        #Audio files
        if len( self.mayaDeps.audioFiles) >= 1:
            for each in self.mayaDeps.audioFiles:
                if each["filePath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " File coming from a not approved location."]
                    listID += 1
        #Image plane files
        if len( self.mayaDeps.imagePlaneFiles) >= 1:
            for each in self.mayaDeps.imagePlaneFiles:
                if each["filePath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " File coming from a not approved location."]
                    listID += 1
        #Exocortex Alembic files
        if len( self.mayaDeps.exocortexAlembicFiles) >= 1:
            for each in self.mayaDeps.exocortexAlembicFiles:
                if each["filePath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " File coming from a not approved location."]
                    listID += 1
        #Yeti files
        if len( self.mayaDeps.yetiFiles) >= 1:
            for each in self.mayaDeps.yetiFiles:
                if each["cacheFileName"] != "":
                    if each["cacheFileName"][0] not in approvedServers:
                        notValidFilePath[listID] = [each["node"], "Path: " + each["cacheFileName"] + " File coming from a not approved location."]
                        listID += 1
        #Yeti textures
        if len( self.mayaDeps.yetiTextureFiles) >= 1:
            for each in self.mayaDeps.yetiTextureFiles:
                if each["filePath"] != "":
                    if each["filePath"][0] not in approvedServers:
                        notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " Yeti Node: " + each["textureNode"] + ". File coming from a not approved location."]
                        listID += 1
        
        rList = self.formatList("Not approved file path", len(notValidFilePath), notValidFilePath, 100) 
        
        return rList
    
    
    def render_path_issues(self):
        '''Check for not approved render locations'''
        renderPathIssues = {}
        listID = 1
        renderPath = mc.renderSettings(gin=True, fullPath=True)[0]
        renderSplitList = self.__splitPath(renderPath)
        projRenderPath = renderSplitList[1]
        if '_' in projRenderPath:
            if projRenderPath.rsplit('_',1)[1] != 'Comp':
                renderPathIssues[listID] = ['defaultArnoldRenderOptions', 'Render path project {} is not a \'Comp\' project.'.format(projRenderPath)]
                listID += 1
        else:
            renderPathIssues[listID] = ['defaultArnoldRenderOptions', 'Render path project {} is not a \'Comp\' project.'.format(projRenderPath)]
            listID += 1
        
        projRenderPath = projRenderPath.rsplit('_',1)[0]
        currProjShort = ''
        currProjComplete = ''
        filePath = mc.file(expandName= True, query= True)
        if '\\' in filePath:
            filePath.replace('\\','/')
        projectByFolder = filePath.split('/')[1]
        if projectByFolder not in ['',None,'None']:
            currProjShort = projectByFolder
            currProjComplete = projectByFolder
            currProjShort = currProjShort.rsplit('_',1)[0]
        if projRenderPath != currProjShort:
            renderPathIssues[listID] = ['defaultArnoldRenderOptions', 'Render path project {} and current file project {} do not match.'.format(projRenderPath, currProjShort)]
            listID += 1
        
        if currProjShort not in ['',None,'None']:
            sInfo = StudioInfo()
            pInfo = Maya_ProjectInfo(sInfo, currProjComplete)
            seqsList = pInfo.getSequences(sInfo.getSequencesFrom())
            currentSeq = pInfo.maya_getSequence()
            seqName = ''
            for each in renderSplitList:
                if each[:4] == 'SEQ_':
                    seqName = each
                    break
            if seqsList != None:
                if len(seqsList) > 0:
                    for each in renderSplitList:
                        if each in seqsList:
                            seqName = each
                            break
            if seqName != currentSeq:
                renderPathIssues[listID] = ['defaultArnoldRenderOptions', 'Render path sequence {} and current file sequence {} do not match.'.format(seqName, currentSeq)]
                listID += 1
            
            currentShot = pInfo.maya_getShot()
            shotsList = pInfo.getShots(currentSeq, sInfo.getSequencesFrom())
            shotName = ''
            for each in renderSplitList:
                if each[:2] == 's_':
                    shotName = each
                    break
            if shotsList != None:
                if len(shotsList) > 0:
                    for each in renderSplitList:
                        if each in shotsList:
                            shotName = each
                            break
            if shotName != currentShot:
                renderPathIssues[listID] = ['defaultArnoldRenderOptions', 'Render path shot {} and current file shot {} do not match.'.format(shotName, currentShot)]
                listID += 1
        
        rList = self.formatList("Render path issue", len(renderPathIssues), renderPathIssues, 100) 
        
        return rList
            


    def odd_file_name (self):
        """Check for general name conventions"""
        oddFileName = {}
        fileName = mc.file(query=True, sceneName=True, shortName=True)
        nameParts = fileName.split("_")
        listID = 1
        if len(nameParts) < 5 or len(nameParts) > 5:
            oddFileName[listID] = [fileName, "Has incorrect number of name parts. Follow this format: LGT_SEQ_01_s_010.0001.ma"]
            listID += 1
        if len(nameParts) == 5:
            verExtSplit = nameParts[4].split(".")
            if nameParts[0] != "LGT":
                oddFileName[listID] = [fileName, "File name suffix should be 'LGT'. Follow this format: LGT_SEQ_01_s_010.0001.ma"]
                listID += 1
            elif nameParts[1] != "SEQ":
                oddFileName[listID] = [fileName, "File name second part should be 'SEQ'. Follow this format: LGT_SEQ_01_s_010.0001.ma"]
                listID += 1
            elif not nameParts[2].isdigit():
                oddFileName[listID] = [fileName, "File name third part should be the sequence number. Follow this format: LGT_SEQ_01_s_010.0001.ma"]
                listID += 1
            elif nameParts[3] != "s":
                oddFileName[listID] = [fileName, "File name fourth part should be the 's' letter. Follow this format: LGT_SEQ_01_s_010.0001.ma"]
                listID += 1
            elif not verExtSplit[0].isdigit():
                oddFileName[listID] = [fileName, "File name fifth part should be the shot number. Follow this format: LGT_SEQ_01_s_010.0001.ma"]
                listID += 1
        
        rList = self.formatList("Odd file name", len(oddFileName), oddFileName, 40) 
        
        return rList
    
    
    def film_aspect_ratio (self):
        """Check for film aspect ratio"""
        filmAspect = {}
        if self.checkSelectedOnly == True:
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    shapeNode = mc.listRelatives(obj, shapes= True, fullPath= True)[0]
                    if mc.getAttr(shapeNode + ".renderable"):
                        horizontalFilmAperture = mc.getAttr(shapeNode + ".horizontalFilmAperture")
                        verticalFilmAperture = mc.getAttr(shapeNode + ".verticalFilmAperture")
                        if horizontalFilmAperture != 0.980:
                            filmAspect[listID] = [shapeNode, "Horizontal film aperture should be 0.980 for 35mm full frame renders."]
                            listID += 1
                        if verticalFilmAperture != 0.735:
                            filmAspect[listID] = [shapeNode, "Vertical film aperture should be 0.735 for 35mm full frame renders."]
                            listID += 1
                elif mc.nodeType(obj) == "camera":
                    shapeNode = obj
                    if mc.getAttr(shapeNode + ".renderable"):
                        horizontalFilmAperture = mc.getAttr(shapeNode + ".horizontalFilmAperture")
                        verticalFilmAperture = mc.getAttr(shapeNode + ".verticalFilmAperture")
                        if horizontalFilmAperture != 0.980:
                            filmAspect[listID] = [shapeNode, "Horizontal film aperture should be 0.980 for 35mm full frame renders."]
                            listID += 1
                        if verticalFilmAperture != 0.735:
                            filmAspect[listID] = [shapeNode, "Vertical film aperture should be 0.735 for 35mm full frame renders."]
                            listID += 1
        else:
            listID = 1
            for obj in mc.ls(type= "camera"):
                shapeNode = obj
                if mc.getAttr(shapeNode + ".renderable"):
                    horizontalFilmAperture = mc.getAttr(shapeNode + ".horizontalFilmAperture")
                    verticalFilmAperture = mc.getAttr(shapeNode + ".verticalFilmAperture")
                    if horizontalFilmAperture != 0.980:
                        filmAspect[listID] = [shapeNode, "Horizontal film aperture should be 0.980 for 35mm full frame renders."]
                        listID += 1
                    if verticalFilmAperture != 0.735:
                        filmAspect[listID] = [shapeNode, "Vertical film aperture should be 0.735 for 35mm full frame renders."]
                        listID += 1
                    
        rList = self.formatList("Wrong film aspect ratio", len(filmAspect), filmAspect, 100)
        
        return rList
    
    
    def arnold_light_samples_high(self):
        """Check for samples on all lights"""
        lightSamples = {}
        lightTypes = ["ambientLight","directionalLight","pointLight","areaLight","spotLight","volumeLight","aiAreaLight","aiSkyDomeLight","aiPhotometricLight"]
        if self.checkSelectedOnly == True:
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    shapeNode = mc.listRelatives(obj, shapes= True, fullPath= True)[0]
                    if mc.nodeType(shapeNode) in lightTypes:
                        samples = mc.getAttr(shapeNode + ".aiSamples")
                        if samples > 5:
                            lightSamples[listID] = [shapeNode, "Light samples {} are too high.".format(samples)]
                            listID += 1
                elif mc.nodeType(obj) in lightTypes:
                    shapeNode = obj
                    samples = mc.getAttr(shapeNode + ".aiSamples")
                    if samples > 5:
                        lightSamples[listID] = [shapeNode, "Light samples {} are too high.".format(samples)]
                        listID += 1
        else:
            listID = 1
            for obj in mc.ls(type= "light"):
                shapeNode = obj
                samples = mc.getAttr(shapeNode + ".aiSamples")
                if samples > 5:
                    lightSamples[listID] = [shapeNode, "Light samples {} are too high.".format(samples)]
                    listID += 1
                    
        rList = self.formatList("Arnold light samples too high", len(lightSamples), lightSamples, 100)
        
        return rList
    
    
    def arnold_light_samples_low(self):
        """Check for samples on all lights"""
        lightSamples = {}
        lightTypes = ["ambientLight","directionalLight","pointLight","areaLight","spotLight","volumeLight","aiAreaLight","aiSkyDomeLight","aiPhotometricLight"]
        if self.checkSelectedOnly == True:
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    shapeNode = mc.listRelatives(obj, shapes= True, fullPath= True)[0]
                    if mc.nodeType(shapeNode) in lightTypes:
                        samples = mc.getAttr(shapeNode + ".aiSamples")
                        if samples <= 1:
                            lightSamples[listID] = [shapeNode, "Light samples {} are way too low.".format(samples)]
                            listID += 1
                elif mc.nodeType(obj) in lightTypes:
                    shapeNode = obj
                    samples = mc.getAttr(shapeNode + ".aiSamples")
                    if samples <=  1:
                        lightSamples[listID] = [shapeNode, "Light samples {} are too low.".format(samples)]
                        listID += 1
        else:
            listID = 1
            for obj in mc.ls(type= "light"):
                shapeNode = obj
                samples = mc.getAttr(shapeNode + ".aiSamples")
                if samples <= 1:
                    lightSamples[listID] = [shapeNode, "Light samples {} are too low.".format(samples)]
                    listID += 1
                    
        rList = self.formatList("Arnold light samples too low", len(lightSamples), lightSamples, 70)
        
        return rList
    
    
    def arnold_catclark_subdivision(self):
        """Check for Arnold catclark subdivision"""
        subdivs = {}
        if self.checkSelectedOnly == True:
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    shapeNode = mc.listRelatives(obj, shapes= True, fullPath= True, noIntermediate=True)[0]
                    if mc.nodeType(shapeNode) == 'mesh':
                        subdivType = mc.getAttr(shapeNode + ".aiSubdivType")
                        subdivIterations = mc.getAttr(shapeNode + ".aiSubdivIterations")
                        if subdivType != 1:
                            subdivs[listID] = [shapeNode, "Catclark subdivision method hasn't been set."]
                            listID += 1
                        if subdivIterations == 0:
                            subdivs[listID] = [shapeNode, "Subdivision iterations are zero."]
                            listID += 1
                elif mc.nodeType(obj) in 'mesh':
                    shapeNode = obj
                    subdivType = mc.getAttr(shapeNode + ".aiSubdivType")
                    subdivIterations = mc.getAttr(shapeNode + ".aiSubdivIterations")
                    if subdivType != 1:
                        subdivs[listID] = [shapeNode, "Catclark subdivision method hasn't been set."]
                        listID += 1
                    if subdivIterations == 0:
                        subdivs[listID] = [shapeNode, "Subdivision iterations are zero."]
                        listID += 1
        else:
            listID = 1
            for obj in mc.ls(type= "mesh"):
                shapeNode = obj
                subdivType = mc.getAttr(shapeNode + ".aiSubdivType")
                subdivIterations = mc.getAttr(shapeNode + ".aiSubdivIterations")
                if subdivType != 1:
                    subdivs[listID] = [shapeNode, "Catclark subdivision method hasn't been set."]
                    listID += 1
                if subdivIterations == 0:
                    subdivs[listID] = [shapeNode, "Subdivision iterations are zero."]
                    listID += 1
                    
        rList = self.formatList("Arnold catclarck subdivision", len(subdivs), subdivs, 100)
        
        return rList
    
    
    def __splitPath(self, _path):
        '''
        Retrieves the given path split by folders.
        :return: Absolute path to a file.
        :rtype: string
        '''
        slashIndex = _path.rfind("/")
        splitList = []
        if slashIndex == -1:
            splitList = _path.split("\\")
        else:
            splitList = _path.split("/")
            
        return splitList
    

##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------            
def main():
    pass


if __name__ == "__main__": 
    main()