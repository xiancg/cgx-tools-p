# -*- coding: utf-8 -*-
'''
Methods for sanity checking on the look dev/shading stage.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets
from cgx.Maya.scripts.maya_libs.Maya_Dependencies import Maya_Dependencies


##--------------------------------------------------------------------------------------------
##Class: Sanity Lookdev
##--------------------------------------------------------------------------------------------
class SanityLookdev():
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _checkList, _checkNodes, _checkSelectedOnly):
        self.checkList = _checkList
        self.checkNodes = _checkNodes
        self.checkSelectedOnly = _checkSelectedOnly
        self.mayaDeps = Maya_Dependencies()
        self.checkMethods = ["Odd shader names", "NonRelative texture paths", "Missing textures", "Default shader", "No shader",
                             "Shader without Shading Group", "Gamma settings", "Empty Namespace", "Existing Namespaces", "Master Layer On", "Display Layers",
                             "Not approved file path", "Existing AOVs", "Arnold catclark subdivision"]
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def check (self, _checkList, _checkNodes, _checkSelectedOnly):
        """def that gets the checkList and calls the corresponding methods"""
        output = []
        self.checkList = _checkList
        self.checkSelectedOnly = _checkSelectedOnly
        self.checkNodes = _checkNodes
        issuesNumber = 0
        for chk in self.checkList:
            defName = chk.replace(" ", "_").lower()
            if defName in dir(self):
                result = eval('self.' + defName + '()')
                output += result[0]
                issuesNumber += result[1]
            else:
                msgBox = QtWidgets.QMessageBox()
                msgBox.setWindowTitle("Warning!")
                msgBox.setText("No method for " + defName + " was found.")
                msgBox.exec_()
        
        return output, issuesNumber
    
    
    def formatList(self, _checkType, _numIssues, _affectedNodes, _level):
        """Format a datalist with feedback for the user"""
        
        checkList = []
        for item in _affectedNodes.keys():
            checkList.append([ _level, _checkType, _affectedNodes[item][0], _affectedNodes[item][1] ])
        
        return checkList, _numIssues
    
    
    ##--------------------------------------------------------------------------------------------
    ##Checks
    ##--------------------------------------------------------------------------------------------
    def odd_shader_names (self):
        """Check for odd shader names"""
        oddNames = {}
        if self.checkSelectedOnly == True:
            oddNamesList = {}
            listID = 1
            for obj in self.checkNodes:
                if obj not in ['lambert1','particleCloud1']:
                    shaderNodes = mc.listNodeTypes('shader')
                    if mc.nodeType(obj) in shaderNodes:
                        nameParts = obj.split("_")
                        if len(nameParts) < 5:
                            oddNamesList[listID] = [obj, "Has incorrect number of name parts."]
                        elif len(nameParts) > 6:
                            oddNamesList[listID] = [obj, "Has incorrect number of name parts."]
                        elif nameParts[-1][-1].isdigit():
                            oddNamesList[listID] = [obj, "Name shouldn't end with a number."]
                        listID += 1
            oddNames = oddNamesList
        else:
            #Filter all shaders
            allShaders = mc.ls(materials= True, shortNames=True)
            #Get odd names
            oddNamesList = {}
            listID = 1
            for obj in allShaders:
                if obj not in ['lambert1','particleCloud1']:
                    shaderNodes = mc.listNodeTypes('shader')
                    if mc.nodeType(obj) in shaderNodes:
                        nameParts = obj.split("_")
                        if len(nameParts) < 5:
                            oddNamesList[listID] = [obj, "Has incorrect number of name parts."]
                        elif len(nameParts) > 6:
                            oddNamesList[listID] = [obj, "Has incorrect number of name parts."]
                        elif nameParts[-1][-1].isdigit():
                            oddNamesList[listID] = [obj, "Name shouldn't end with a number."]
                        listID += 1
            oddNames = oddNamesList
        
        rList = self.formatList("Odd shader names", len(oddNames), oddNames, 1)

        return rList
    
    
    def nonrelative_texture_paths (self):
        """Check for non relative texture paths"""
        nonRelativePaths = {}
        if self.checkSelectedOnly == True:
            nonRelativePathsList = {}
            currProj = mc.workspace(shortName = True)
            listID = 1
            for node in self.checkNodes:
                if mc.nodeType(node) == "file":
                    texture = mc.getAttr(node + ".fileTextureName")
                    if texture[:(len(currProj)+2)] == currProj + "//":
                        nonRelativePathsList[listID] = [node, texture]
                    elif texture[:(len(currProj)+2)] == currProj + "/":
                        nonRelativePathsList[listID] = [node, texture]
                    elif texture[:5] == "/MAPS":
                        nonRelativePathsList[listID] = [node, texture]
                    elif texture == None or texture == "":
                        nonRelativePathsList[listID] = [node, texture]
                    listID += 1
            nonRelativePaths = nonRelativePathsList
        else:
            #Filter all file nodes
            currProj = mc.workspace(shortName = True)
            filesList = mc.ls(type="file")
            #Get odd names
            nonRelativePathsList = {}
            listID = 1
            for node in filesList:
                texture = mc.getAttr(node + ".fileTextureName")
                if texture[:(len(currProj)+2)] == currProj + "//":
                    nonRelativePathsList[listID] = [node, texture]
                elif texture[:(len(currProj)+2)] == currProj + "/":
                    nonRelativePathsList[listID] = [node, texture]
                elif texture[:5] == "/MAPS":
                    nonRelativePathsList[listID] = [node, texture]
                elif texture == None or texture == "":
                    nonRelativePathsList[listID] = [node, texture]
                listID += 1
            nonRelativePaths = nonRelativePathsList
        
        rList = self.formatList("NonRelative texture paths", len(nonRelativePaths), nonRelativePaths, 70)

        return rList
    
    
    def missing_textures (self):
        """Check for missing textures"""
        missingTextures = {}
        if self.checkSelectedOnly == True:
            missingTexturesList = {}
            listID = 1
            for node in self.checkNodes:
                if mc.nodeType(node) == "file":
                    texture = mc.getAttr(node + ".fileTextureName")
                    if not mc.file(texture, query= True, exists= True):
                        missingTexturesList[listID] = [node, texture]
                        listID += 1
            missingTextures = missingTexturesList
        else:
            missingTexturesList = {}
            filesList = mc.ls(type="file")
            listID = 1
            for node in filesList:
                texture = mc.getAttr(node + ".fileTextureName")
                if not mc.file(texture, query=True, exists= True):
                    missingTexturesList[listID] = [node, texture]
                    listID += 1
            missingTextures = missingTexturesList
        
        rList = self.formatList("Missing textures", len(missingTextures), missingTextures, 100)

        return rList
    
    
    def default_shader (self):
        """Check for geometry with default lambert shader assigned"""
        defaultShader = {}
        if self.checkSelectedOnly == True:
            defaultShaderObjs = {}
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "mesh":
                    setsObj = mc.listSets(object= obj, type= 1)
                    if "initialShadingGroup" in setsObj:
                        defaultShaderObjs[listID] = [obj, "Default lambert shader assigned."]
                        listID += 1
                elif mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                setsObj = mc.listSets(object= shape, type= 1)
                                if "initialShadingGroup" in setsObj:
                                    defaultShaderObjs[listID] = [shape, "Default lambert shader assigned."]
                                    listID += 1
                                    break
            defaultShader = defaultShaderObjs
        else:
            defaultShaderList = mc.sets( "initialShadingGroup", q=True )
            defaultShader = {}
            listID = 1
            if defaultShaderList != None:
                for each in defaultShaderList:
                    defaultShader[listID] = [each, "Default lambert shader assigned."]
                    listID += 1
            
            if defaultShader == None:
                defaultShader = {}
        
        rList = self.formatList("Default shader", len(defaultShader), defaultShader, 70)

        return rList
    
    
    def no_shader (self):
        """Check for geometry with no shader assigned"""
        noShader = {}
        if self.checkSelectedOnly == True:
            noShaderList = {}
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "mesh":
                    shadingGroup = mc.listConnections(obj, destination= True, source= False, plugs= False, type= "shadingEngine")
                    if shadingGroup != None:
                        if len(shadingGroup) < 1:
                            noShaderList[listID] = [obj, "Geometry with no shader assigned."]
                            listID += 1
                    else:
                        noShaderList[listID] = [obj, "Geometry with no shader assigned."]
                        listID += 1
                elif mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            shadingGroup = mc.listConnections(shape, destination= True, source= False, plugs= False, type= "shadingEngine")
                            if shadingGroup != None:
                                if len(shadingGroup) < 1:
                                    noShaderList[listID] = [obj, "Geometry with no shader assigned."]
                                    listID += 1
                            else:
                                noShaderList[listID] = [obj, "Geometry with no shader assigned."]
                                listID += 1
                            break
            noShader = noShaderList
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            noShaderList = {}
            listID = 1
            for obj in allMeshTrans:
                transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                for shape in transShapes:
                    if mc.nodeType(shape) == "mesh":
                        shadingGroup = mc.listConnections(shape, destination= True, source= False, plugs= False, type= "shadingEngine")
                        if shadingGroup != None:
                            if len(shadingGroup) < 1:
                                noShaderList[listID] = [obj, "Geometry with no shader assigned."]
                                listID += 1
                        else:
                            noShaderList[listID] = [obj, "Geometry with no shader assigned."]
                            listID += 1
                        break
            noShader = noShaderList
        
        rList = self.formatList("No shader", len(noShader), noShader, 100)

        return rList
    
    
    def shader_without_shading_group (self):
        """Check for shaders without a shading group"""
        shaderWOSG = {}
        if self.checkSelectedOnly == True:
            shaderWOSGList = {}
            listID = 1
            for node in self.checkNodes:
                shaderNodes = mc.listNodeTypes('shader')
                if mc.nodeType(node) in shaderNodes:
                    shadingGroup = mc.listConnections(node, destination= True, source= False, plugs= False, type= "shadingEngine")
                    if shadingGroup != None:
                        if len(shadingGroup) < 1:
                            shaderWOSGList[listID] = [node, "Shader without shading group."]
                            listID += 1
                    else:
                        shaderWOSGList[listID] = [node, "Shader without shading group."]
                        listID += 1
            shaderWOSG = shaderWOSGList
        else:
            #Filter all shaders
            allShaders = mc.ls(materials= True, shortNames=True)
            shaderWOSGList = {}
            shaderNodes = mc.listNodeTypes('shader')
            listID = 1
            for node in allShaders:
                if mc.nodeType(node) in shaderNodes:
                    shadingGroup = mc.listConnections(node, destination= True, source= False, plugs= False, type= "shadingEngine")
                    if shadingGroup != None:
                        if len(shadingGroup) < 1:
                            shaderWOSGList[listID] = [node, "Shader without shading group."]
                            listID += 1
                    else:
                        shaderWOSGList[listID] = [node, "Shader without shading group."]
                        listID += 1
            shaderWOSG = shaderWOSGList
        
        rList = self.formatList("Shader without Shading Group", len(shaderWOSG), shaderWOSG, 100)

        return rList
    
    
    def gamma_settings (self):
        """Check for settings in all gamma nodes"""
        gammaSettings = {}
        if self.checkSelectedOnly == True:
            gammaSettingsList = {}
            listID = 1
            for node in self.checkNodes:
                if mc.nodeType(node) == "gammaCorrect":
                    gammaX = round(mc.getAttr(node + ".gammaX"),3)
                    gammaY = round(mc.getAttr(node + ".gammaY"),3)
                    gammaZ = round(mc.getAttr(node + ".gammaZ"),3)
                    if gammaX != 0.454 or gammaY != 0.454 or gammaZ != 0.454:
                        gammaSettingsList[listID] = [node, "Bad settings on gamma node."]
                        listID += 1
            gammaSettings = gammaSettingsList
        else:
            allGammaNodes = mc.ls(type= "gammaCorrect", shortNames=True)
            gammaSettingsList = {}
            listID = 1
            for node in allGammaNodes:
                gammaX = round(mc.getAttr(node + ".gammaX"),3)
                gammaY = round(mc.getAttr(node + ".gammaY"),3)
                gammaZ = round(mc.getAttr(node + ".gammaZ"),3)
                if gammaX != 0.454 or gammaY != 0.454 or gammaZ != 0.454:
                    gammaSettingsList[listID] = [node, "Bad settings on gamma node."]
                    listID += 1
            gammaSettings = gammaSettingsList
        
        rList = self.formatList("Gamma settings", len(gammaSettings), gammaSettings, 100)

        return rList


    def existing_namespaces(self):
        """Check for existing namespaces"""
        existingNamespaces = []
        initNamespacesList = mc.namespaceInfo(":", listOnlyNamespaces= True)
        defaultNamespaces = ["UI","shared"]
        namespacesList = []
        for each in initNamespacesList:
            if each not in defaultNamespaces:
                namespacesList.append(each)
                
        if namespacesList != None or len(namespacesList) >= 1:
            for each in namespacesList:
                thisChildren = self.existing_namespace_recursive(each)
                existingNamespaces.append(each)
                existingNamespaces += thisChildren
        
        existingNamespacesDict = {}
        listID = 1
        for each in existingNamespaces:
            existingNamespacesDict[listID] = [each, "Existing Namespace."]
            listID += 1
        
        rList = self.formatList("Existing Namespaces", len(existingNamespacesDict), existingNamespacesDict, 100)
        
        return rList
        
    
    def existing_namespace_recursive(self, _namespace):
        allChildNamespaces = mc.namespaceInfo(_namespace, listOnlyNamespaces= True)
        if allChildNamespaces != None:
            finalChildren = []
            for each in allChildNamespaces:
                thisChildren = self.existing_namespace_recursive(each)
                if len(thisChildren) >= 1:
                    finalChildren += thisChildren
            return allChildNamespaces + finalChildren
        else:
            return []
    
    
    def empty_namespace (self):
        """Check for empty namespaces"""
        emptyNamespaces = []
        currentNamespace = mc.namespaceInfo( currentNamespace=True)
        if mc.namespaceInfo(currentNamespace, isRootNamespace= True):
            allChildNamespaces = mc.namespaceInfo(currentNamespace, listOnlyNamespaces= True)
            if allChildNamespaces != None:
                for each in allChildNamespaces:
                    thisEmpty = self.empty_namespace_recursive(each)
                    if thisEmpty != None:
                        emptyNamespaces.append(thisEmpty)
        
        emptyNamespacesDict = {}
        listID = 1
        for each in emptyNamespaces:
            emptyNamespacesDict[listID] = [each, "EMPTY Namespace."]
            listID += 1
        
        rList = self.formatList("Empty Namespaces", len(emptyNamespacesDict), emptyNamespacesDict, 70)
        
        return rList
        
    
    def empty_namespace_recursive(self, _namespace):
        allChildNamespaces = mc.namespaceInfo(_namespace, listOnlyNamespaces= True)
        if allChildNamespaces != None:
            for each in allChildNamespaces:
                thisEmpty = self.empty_namespace_recursive(each)
                if thisEmpty != None:
                    return thisEmpty
        elif mc.namespaceInfo(_namespace, listNamespace=True) == None:
            if _namespace != "shared":
                return _namespace
        else:
            return None
    
    
    def master_layer_on(self):
        """Check if masterLayer is On for rendering"""
        masterLayerOn = {}
        listID = 1
        if mc.getAttr("defaultRenderLayer.renderable") == 1:
            masterLayerOn[listID] = ["defaultRenderLayer", "masterLayer is ON as a rendereable layer. Should be OFF."]
        
        rList = self.formatList("Master Layer On", len(masterLayerOn), masterLayerOn, 100)
        
        return rList
    
    
    def display_layers(self):
        dispLayers = {}
        listID = 1
        for each in mc.ls(type="displayLayer"):
            if each != "defaultLayer":
                dispLayers[listID] = [each, "Delete all display layers before publishing."]
            listID += 1
        
        rList = self.formatList("Display Layers", len(dispLayers), dispLayers, 70)
        
        return rList
    
    
    def not_approved_file_path (self):
        """Check for not approved file locations"""
        notValidFilePath = {}
        approvedServers = ['G','F','Z']
        listID = 1
        #Texture files
        if len( self.mayaDeps.textureFiles) >= 1:
            for each in self.mayaDeps.textureFiles:
                if each["filePath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " File coming from a not approved location."]
                    listID += 1
        #Point Cache files
        if len( self.mayaDeps.pointCacheFiles) >= 1:
            for each in self.mayaDeps.pointCacheFiles:
                if each["xmlPath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["xmlPath"] + " File coming from a not approved location."]
                    listID += 1
        #Alembic files
        if len( self.mayaDeps.alembicFiles) >= 1:
            for each in self.mayaDeps.alembicFiles:
                if each["filePath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " File coming from a not approved location."]
                    listID += 1
        #Referenced files
        if len( self.mayaDeps.referenceFiles) >= 1:
            for each in self.mayaDeps.referenceFiles:
                if each["filePath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " File coming from a not approved location."]
                    listID += 1
        #Exocortex Alembic files
        if len( self.mayaDeps.exocortexAlembicFiles) >= 1:
            for each in self.mayaDeps.exocortexAlembicFiles:
                if each["filePath"][0] not in approvedServers:
                    notValidFilePath[listID] = [each["node"], "Path: " + each["filePath"] + " File coming from a not approved location."]
                    listID += 1
        
        rList = self.formatList("Not approved file path", len(notValidFilePath), notValidFilePath, 100) 
        
        return rList
    
    
    def existing_aovs(self):
        existingAOVs = {}
        thisList = mc.ls(type="aiAOV")
        listID = 1
        if thisList != None:
            if len(thisList) >= 1:
                for each in thisList:
                    existingAOVs[listID] = [each, "File contains AOVs."]
                    listID += 1
        
        rList = self.formatList("Existing AOVs", len(existingAOVs), existingAOVs, 100)
        
        return rList
    
    
    def arnold_catclark_subdivision(self):
        """Check for Arnold catclark subdivision"""
        subdivs = {}
        if self.checkSelectedOnly == True:
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    shapeNode = mc.listRelatives(obj, shapes= True, fullPath= True, noIntermediate=True)[0]
                    if mc.nodeType(shapeNode) == 'mesh':
                        subdivType = mc.getAttr(shapeNode + ".aiSubdivType")
                        subdivIterations = mc.getAttr(shapeNode + ".aiSubdivIterations")
                        if subdivType != 1:
                            subdivs[listID] = [shapeNode, "Catclark subdivision method hasn't been set."]
                            listID += 1
                        if subdivIterations == 0:
                            subdivs[listID] = [shapeNode, "Subdivision iterations are zero."]
                            listID += 1
                elif mc.nodeType(obj) in 'mesh':
                    shapeNode = obj
                    subdivType = mc.getAttr(shapeNode + ".aiSubdivType")
                    subdivIterations = mc.getAttr(shapeNode + ".aiSubdivIterations")
                    if subdivType != 1:
                        subdivs[listID] = [shapeNode, "Catclark subdivision method hasn't been set."]
                        listID += 1
                    if subdivIterations == 0:
                        subdivs[listID] = [shapeNode, "Subdivision iterations are zero."]
                        listID += 1
        else:
            listID = 1
            for obj in mc.ls(type= "mesh"):
                shapeNode = obj
                subdivType = mc.getAttr(shapeNode + ".aiSubdivType")
                subdivIterations = mc.getAttr(shapeNode + ".aiSubdivIterations")
                if subdivType != 1:
                    subdivs[listID] = [shapeNode, "Catclark subdivision method hasn't been set."]
                    listID += 1
                if subdivIterations == 0:
                    subdivs[listID] = [shapeNode, "Subdivision iterations are zero."]
                    listID += 1
                    
        rList = self.formatList("Arnold catclarck subdivision", len(subdivs), subdivs, 100)
        
        return rList
        
    

##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------            
def main():
    pass


if __name__ == "__main__": 
    main()