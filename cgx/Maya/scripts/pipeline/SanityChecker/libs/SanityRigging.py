# -*- coding: utf-8 -*-
'''
Methods for sanity checking on the rigging stage.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Sanity Rigging
##--------------------------------------------------------------------------------------------
class SanityRigging():
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _checkList, _checkNodes, _checkSelectedOnly):
        self.checkList = _checkList
        self.checkNodes = _checkNodes
        self.checkSelectedOnly = _checkSelectedOnly
        self.checkMethods = ["Duplicate names", "NonZero Transforms", "NonZero Pivot",
                             "Animation Curves", "Empty Namespace", "Existing Namespaces"]
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def check (self, _checkList, _checkNodes, _checkSelectedOnly):
        """def that gets the checkList and calls the corresponding methods"""
        output = []
        self.checkList = _checkList
        self.checkSelectedOnly = _checkSelectedOnly
        self.checkNodes = _checkNodes
        issuesNumber = 0
        for chk in self.checkList:
            defName = chk.replace(" ", "_").lower()
            if defName in dir(self):
                result = eval('self.' + defName + '()')
                output += result[0]
                issuesNumber += result[1]
            else:
                msgBox = QtWidgets.QMessageBox()
                msgBox.setWindowTitle("Warning!")
                msgBox.setText("No method for " + defName + " was found.")
                msgBox.exec_()
        
        return output, issuesNumber
    
    
    def formatList(self, _checkType, _numIssues, _affectedNodes, _level):
        """Format a datalist with feedback for the user"""
        
        checkList = []
        for item in _affectedNodes.keys():
            checkList.append([ _level, _checkType, _affectedNodes[item][0], _affectedNodes[item][1] ])
        
        return checkList, _numIssues
    
    
    ##--------------------------------------------------------------------------------------------
    ##Checks
    ##--------------------------------------------------------------------------------------------
    def duplicate_names (self):
        """Check for duplicate names in geometry"""
        dupNames = []
        dupNamesDict = {}
        if self.checkSelectedOnly == True:
            dupItemsList = []
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "mesh":
                    if '|' in obj:
                        dupItemsList.append(obj)
                elif mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                if '|' in obj:
                                    dupItemsList.append(obj)
                                break
            dupNames = dupItemsList
            listID = 1
            for each in dupNames:
                dupNamesDict[listID] = [each, "Duplicate name"]
                listID += 1
        else:
            #CHECK SHAPE NODES
            allShapes = mc.ls(type= "mesh", shortNames=True)
            dupShapesList = []
            #Get duplicated shapes
            for obj in allShapes:
                if '|' in obj:
                    dupShapesList.append(obj)
            
            #CHECK TRANSFORM NODES
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            dupTransList = []
            #Get duplicated transforms
            for obj in allMeshTrans:
                if '|' in obj:
                    dupTransList.append(obj)
            
            dupNames = dupTransList + dupShapesList
            listID = 1
            for each in dupNames:
                dupNamesDict[listID] = [each, "Duplicate name"]
                listID += 1
                        
        rList = self.formatList("Duplicate names", len(dupNamesDict), dupNamesDict, 20)
        
        return rList
    
    
    def nonzero_transforms (self):
        """Check for geometry with nonzero transforms"""
        nonZeroTrans = {}
        if self.checkSelectedOnly == True:
            nonZeroTransList = {}
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                translateObj = mc.xform(obj, query=True, translation=True,worldSpace=True)
                                rotateObj = mc.xform(obj, query=True, rotation=True,worldSpace=True)
                                scaleObj = mc.xform(obj, query=True, scale=True,worldSpace=True)
                                if translateObj[0] != 0 or translateObj[1] != 0 or translateObj[2] != 0 or rotateObj[0] != 0 or rotateObj[1] != 0 or rotateObj[2] != 0 or scaleObj[0] != 1 or scaleObj[1] != 1 or scaleObj[2] != 1:
                                    nonZeroTransList[listID] = [obj, "Has non-zero transforms."]
                                    listID += 1
                                break
            nonZeroTrans = nonZeroTransList
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            nonZeroTransList = {}
            listID = 1
            #Get nonzero transforms
            for obj in allMeshTrans:
                translateObj = mc.xform(obj, query=True, translation=True,worldSpace=True)
                rotateObj = mc.xform(obj, query=True, rotation=True,worldSpace=True)
                scaleObj = mc.xform(obj, query=True, scale=True,worldSpace=True)
                if translateObj[0] != 0 or translateObj[1] != 0 or translateObj[2] != 0 or rotateObj[0] != 0 or rotateObj[1] != 0 or rotateObj[2] != 0 or scaleObj[0] != 1 or scaleObj[1] != 1 or scaleObj[2] != 1:
                    nonZeroTransList[listID] = [obj, "Has non-zero transforms."]
                    listID += 1
            
            nonZeroTrans = nonZeroTransList
        
        rList = self.formatList("NonZero Transforms", len(nonZeroTrans), nonZeroTrans, 20)

        return rList
    
    
    def nonzero_pivot (self):
        """Check for geometry with nonzero pivot locations"""
        nonZeroPivot = {}
        if self.checkSelectedOnly == True:
            nonZeroPivotList = {}
            listID = 1
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    transShapes = mc.listRelatives(obj, shapes= True, fullPath= True)
                    if transShapes != None:
                        for shape in transShapes:
                            if mc.nodeType(shape) == "mesh":
                                pivot = mc.xform(obj, pivots= True, query= True, ws= True)
                                if pivot[0] != 0 or pivot[1] != 0 or pivot[2] != 0:
                                    nonZeroPivotList[listID] = [obj, "Has non-zero pivot."]
                                    listID += 1
                                break
            nonZeroPivot = nonZeroPivotList
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            nonZeroPivotList = {}
            listID = 1
            #Get nonzero pivots
            for obj in allMeshTrans:
                pivot = mc.xform(obj, pivots= True, query= True, ws= True)
                if pivot[0] != 0 or pivot[1] != 0 or pivot[2] != 0:
                    nonZeroPivotList[listID] = [obj, "Has non-zero pivot."]
                    listID += 1
            
            nonZeroPivot = nonZeroPivotList
        
        rList = self.formatList("NonZero Pivot", len(nonZeroPivot), nonZeroPivot, 20)

        return rList
    
    
    def animation_curves (self):
        """Check for geometry with animation"""
        animationCurves = []
        animationCurvesDict = {}
        if self.checkSelectedOnly == True:
            animationCurvesList = []
            for obj in self.checkNodes:
                if mc.nodeType(obj) == "transform":
                    if mc.keyframe(obj, time=(':',), query=True, keyframeCount=True):
                        animationCurvesList.append(obj)
            animationCurves= animationCurvesList
            listID = 1
            for each in animationCurves:
                animationCurvesDict[listID] = [each, "Has animation curves."]
                listID += 1
        else:
            #Filter all transforms that are not meshes
            allTrans = mc.ls(type= "transform", shortNames=True)
            allMeshTrans = []
            for trans in allTrans:
                transShapes = mc.listRelatives(trans, shapes= True, fullPath= True)
                if transShapes != None:
                    for shape in transShapes:
                        if mc.nodeType(shape) == "mesh":
                            allMeshTrans.append(trans)
                            break
            animationCurvesList = []
            #Get meshes with animation
            for obj in allMeshTrans:
                if mc.nodeType(obj) == "transform":
                    if mc.keyframe(obj, time=(':',), query=True, keyframeCount=True):
                        animationCurvesList.append(obj)
            animationCurves= animationCurvesList
            listID = 1
            for each in animationCurves:
                animationCurvesDict[listID] = [each, "Has animation curves."]
                listID += 1

        rList = self.formatList("Animation Curves", len(animationCurvesDict), animationCurvesDict, 100)
        
        return rList
    
    
    def existing_namespaces(self):
        """Check for existing namespaces"""
        existingNamespaces = []
        initNamespacesList = mc.namespaceInfo(":", listOnlyNamespaces= True)
        defaultNamespaces = ["UI","shared"]
        namespacesList = []
        for each in initNamespacesList:
            if each not in defaultNamespaces:
                namespacesList.append(each)
                
        if namespacesList != None or len(namespacesList) >= 1:
            for each in namespacesList:
                thisChildren = self.existing_namespace_recursive(each)
                existingNamespaces.append(each)
                existingNamespaces += thisChildren
        
        existingNamespacesDict = {}
        listID = 1
        for each in existingNamespaces:
            existingNamespacesDict[listID] = [each, "Existing Namespace."]
            listID += 1
        
        rList = self.formatList("Existing Namespaces", len(existingNamespacesDict), existingNamespacesDict, 100)
        
        return rList
        
    
    def existing_namespace_recursive(self, _namespace):
        allChildNamespaces = mc.namespaceInfo(_namespace, listOnlyNamespaces= True)
        if allChildNamespaces != None:
            finalChildren = []
            for each in allChildNamespaces:
                thisChildren = self.existing_namespace_recursive(each)
                if len(thisChildren) >= 1:
                    finalChildren += thisChildren
            return allChildNamespaces + finalChildren
        else:
            return []
    
    
    def empty_namespace (self):
        """Check for empty namespaces"""
        emptyNamespaces = []
        currentNamespace = mc.namespaceInfo( currentNamespace=True)
        if mc.namespaceInfo(currentNamespace, isRootNamespace= True):
            allChildNamespaces = mc.namespaceInfo(currentNamespace, listOnlyNamespaces= True)
            if allChildNamespaces != None:
                for each in allChildNamespaces:
                    thisEmpty = self.empty_namespace_recursive(each)
                    if thisEmpty != None:
                        emptyNamespaces.append(thisEmpty)
        
        emptyNamespacesDict = {}
        listID = 1
        for each in emptyNamespaces:
            emptyNamespacesDict[listID] = [each, "EMPTY Namespace."]
            listID += 1
        
        rList = self.formatList("Empty Namespaces", len(emptyNamespacesDict), emptyNamespacesDict, 100)
        
        return rList
        
    
    def empty_namespace_recursive(self, _namespace):
        allChildNamespaces = mc.namespaceInfo(_namespace, listOnlyNamespaces= True)
        if allChildNamespaces != None:
            for each in allChildNamespaces:
                thisEmpty = self.empty_namespace_recursive(each)
                if thisEmpty != None:
                    return thisEmpty
        elif mc.namespaceInfo(_namespace, listNamespace=True) == None:
            if _namespace != "shared":
                return _namespace
        else:
            return None
    

##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------            
def main():
    pass


if __name__ == "__main__": 
    main()