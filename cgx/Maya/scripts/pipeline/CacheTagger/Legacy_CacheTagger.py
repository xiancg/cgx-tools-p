# -*- coding: utf-8 -*-
'''
Cache Tagger tool. Tag geometry for caching.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
from cgx.gui.Qt import QtWidgets, QtCore
from cgx.gui.Qt import __binding__
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
import cgx.gui.DataViewModels as dvm

import maya.cmds as mc


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
    appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/pipeline/CacheTagger'
main_ui = appRootFolder + "/ui/" + "main_legacy.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
        '''
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
        '''
        super(Main_GUI,self).__init__(parent)
        self.setupUi(self)
        
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        
        self.appTools = appTools
        if self.appTools != None:
            self.initTool()
            self.createComponents()
            self.setConnections()
            self.initComponents()
            self.initUI()
            self.checkPlugins()
            self.refreshTree()
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        
        
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def setConnections(self):
        '''
        Connect signals.
        '''
        #SIGNALS
        self.edit_removeTags.triggered.connect(self.removeExistingTags)
        self.rigCtrl_BTN.clicked.connect(self.selectedCtrl)
        self.createTags_BTN.clicked.connect(self.createTags)
        self.prePublishRig_BTN.clicked.connect(self.prePublishRig)
        #REFRESH
        self.create360_CHKBOX.clicked.connect(self.refreshAnimation)
        self.skipTags_CHKBOX.clicked.connect(self.refreshSkipTags)
        #STATUS
        self.y_RBTN.setChecked(True)
        self.filePerCharProp_CHKBOX.setChecked(True)
        
        #CONTEXT MENUS
        self.charsProps_TREEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.charsProps_TREEVIEW.customContextMenuRequested.connect(self.charsPropsOptions)
        
    
    def initTool(self):
        '''
        Initializes tool settings.
        '''
        self.initAttrs()
    
    
    def resetTool(self):
        self.initAttrs()
        self.initComponents()
        self.initUI()
        self.checkPlugins()
        self.refreshTree()
        
        
    def initAttrs(self):
        '''
        Initializes variables and components for this tool.
        '''
        self.__client = "defaultClient"
        self.__toolName = "Cache Tagger"
        self.__toolVersion = "2.0"
        
        #Set time range
        self.appTools.tRange.handleIn = 0
        self.appTools.tRange.handleOut = 0
        self.appTools.tRange.startFrame = 1
        self.appTools.tRange.endFrame = 100
        
        #Attributes
        self.usedGeom = []
        
    
    def createComponents(self):
        self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
        self.baseMod = self.Base_GUIModulesFactory.createModule('base')
        self.baseMod.MainMenu_Component.edit_config.setVisible(False)
        self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo, tRange=self.appTools.tRange)
        self.projAssetsMod = self.Pipeline_GUIModulesFactory.createModule('ProjAssets')
        self.projAssetsMod.pos = [0,40]
        self.fpsComp = pipeComponents.FPS_Component(self,self.centralWidget(), self.pInfo)
        self.fpsComp.pos = [265,45]
        self.cacheMod = self.Pipeline_GUIModulesFactory.createModule('Cache') 
        self.cacheMod.pos = [0,300]
        self.progComp = pipeComponents.Progress_Component(self,self.centralWidget())
        self.progComp.pos = [350,410]
        
        #Add Remove Existing Tags menu option
        self.edit_removeTags = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
        self.edit_removeTags.setObjectName("edit_removeTags")
        self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[1], self.edit_removeTags)
        className= str(self.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.edit_removeTags.setText(QtWidgets.QApplication.translate(className, "Remove Existing Tags", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.edit_removeTags.setText(QtWidgets.QApplication.translate(className, "Remove Existing Tags", None))
        
    
    def initComponents(self):
        self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
        self.appTools.app_initUI.setCurrentProject(self.projAssetsMod.Project_Component)
        self.appTools.app_initUI.setCurrentAssetType(self.projAssetsMod.AssetTypes_Component)
        self.appTools.app_initUI.setCurrentAsset(self.projAssetsMod.Assets_Component)
        self.appTools.app_initUI.setCurrentSubAsset(self.projAssetsMod.SubAssets_Component)
        self.appTools.app_initUI.setCurrentFileType(self.projAssetsMod.FileTypes_Component)
        self.fpsComp.initComponent(self.pInfo.maya_getFPS())
        self.appTools.app_initUI.checkFPS(self.fpsComp)
    
    
    def initUI(self):
        '''
        Initialize GUI components.
        '''
        pass
    
    
    def checkPlugins(self):
        '''
        Check if plugins are loaded.
        :return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
        :rtype: dict
        '''
        checkDict = self.appTools.checkPlugins(self)
        return checkDict
    
    
    def prePublishRig(self):
        if self.charsProps_TREEVIEW.model().rowCount(QtCore.QModelIndex()) <= 0:
            self.baseMod.statusBar_update("You have to create a Root and add geometry to it for this tool to work.", warning=True)
        elif not mc.objExists(self.rigCtrl_LINEEDIT.text()) and self.create360_CHKBOX.isChecked():
            self.baseMod.statusBar_update("Rig control doesn't exist in the scene.", warning=True)
        else:
            #Create Tags
            if not self.skipTags_CHKBOX.isChecked():
                if mc.objExists("*_cgxGeoSet"):
                    self.baseMod.statusBar_update("This project already contains a cgxGeoSet with tagged geometry. Please remove it or use the Skip Tags Creation option.", warning=True)
                else:
                    self.createTags()
                    if self.create360_CHKBOX.isChecked():
                        self.createAnimation()
                        self.exportCache()
                        self.deleteAnimation()
            else:
                if self.create360_CHKBOX.isChecked():
                    self.createAnimation()
                    self.exportCache()
                    self.deleteAnimation()
    
    
    def createAnimation(self):
        rigCtrl = self.rigCtrl_LINEEDIT.text()
        upAxis = "y"
        if self.x_RBTN.isChecked():
            upAxis = "x"
        elif self.z_RBTN.isChecked():
            upAxis = "z"
        
        mc.setKeyframe(rigCtrl + ".r" + upAxis, itt="linear", ott="linear", time=1, value= 0)
        mc.setKeyframe(rigCtrl + ".r" + upAxis, itt="linear", ott="linear", time=101, value= 360)
    
    
    def deleteAnimation(self):
        rigCtrl = self.rigCtrl_LINEEDIT.text()
        upAxis = "y"
        if self.x_RBTN.isChecked():
            upAxis = "x"
        elif self.z_RBTN.isChecked():
            upAxis = "z"
            
        mc.cutKey(rigCtrl, cl=True, at= "r" + upAxis, t=(1,101), f=(1,101))
        mc.setAttr(rigCtrl + ".r" + upAxis, 0)
    
    
    def exportCache(self):
        check = self.checkPlugins()
        if len(check.keys()) <= 0:
            #Set progress bar
            self.progComp.reset()
            self.progComp.minimum = 0
            
            #Stop refresh
            mc.refresh(suspend=True)
            currentEvalManager = mc.evaluationManager( query=True, mode=True )[0]
            mc.evaluationManager(mode='off')
            
            assetType = self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
            assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
            subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
            
            #Collect geometry for baking and create Baking Method object
            geoDict = {} #GEO_CURRENT_NAME = [GEO_USER_NAME, rootName, rootNode]. Using node name to store reference to real object, and datalist for user values
            roots = self.charsProps_TREEVIEW.model().dataList.children
            for each in roots:
                childGeom = each.children
                for item in childGeom:
                    geoCurrentName = item.data[2]
                    geoUserName = item.name
                    if geoCurrentName in geoDict:
                        self.baseMod.statusBar_update("Found duplicated names in the project. Some geometry will be missing after exporting.", warning=True)
                    else:
                        geoDict[geoCurrentName] = [geoUserName, each.name, each]
            bakingMethods = self.appTools.createBakingMethods(geoDict, self.progComp.progress_PBAR, 1)
            
            #More progress bar
            multProgress = 1
            if self.exportObjs_CHKBOX.isChecked():
                multProgress = 2
            self.progComp.maximum = len(geoDict.keys()) * multProgress
            
            #CREATE FOLDERS
            thisPath = self.buildCachePath(assetType, assetName, subAssetName)
            finalPathObjs = thisPath + "/OBJs"
            if not os.path.exists(thisPath):
                os.makedirs(thisPath)
            if self.exportObjs_CHKBOX.isChecked():
                if not os.path.exists(finalPathObjs):
                    os.makedirs(finalPathObjs)
            
            #EXPORT ONE FILE PER CHARACTER/PROP
            if self.filePerCharProp_CHKBOX.isChecked():
                #Bake geometry first, then export
                #Create a list of used roots
                rootList = []
                for e in geoDict.keys():
                    if geoDict[e][2] not in rootList:
                        rootList.append(geoDict[e][2])
                #Loop through rootList and compare to geoDict to bake and export
                for rootNode in rootList:
                    rootName = rootNode.name
                    bakedGeomList = []
                    for e in geoDict.keys():
                        if geoDict[e][2] == rootNode:
                            #Bake geometry
                            bakedGeom = ""
                            if self.cacheMod.CacheBakingMethods_Component.vertexBake_RBTN.isChecked():
                                bakedGeom = bakingMethods.vertexBakeMethod(e)
                                bakedGeomList.append(bakedGeom)
                            elif self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                                bakedGeom = e
                                bakedGeomList.append(bakedGeom)
                    exporter = self.appTools.createExporter(bakedGeomList, self.appTools.tRange, self.progComp.progress_PBAR, thisPath, 1)
                    #Export baked geom list
                    cleanName = rootName + "_bkd_1"
                    if self.cacheMod.CacheFileTypes_Component.pointCache_RBTN.isChecked():
                        exporter.exportPointCache(cleanName, True)
                        if self.exportObjs_CHKBOX.isChecked():
                            exporter.exportObj()
                    elif self.cacheMod.CacheFileTypes_Component.alembic_RBTN.isChecked():
                        if self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                            exporter.exportAlembicBake(cleanName, True)
                        else:
                            exporter.exportAlembic(cleanName, True)
                    elif self.cacheMod.CacheFileTypes_Component.alembicExo_RBTN.isChecked():
                        if self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                            if self.exoExportSurface_CHKBOX.isChecked():
                                exporter.exportAlembicBakeExo(cleanName, True, True)
                            else:
                                exporter.exportAlembicBakeExo(cleanName, True)
                        else:
                            if self.exoExportSurface_CHKBOX.isChecked():
                                exporter.exportAlembicExo(cleanName, True, True)
                            else:
                                exporter.exportAlembicExo(cleanName, True)
                    #Delete baked geometry
                    if not self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                        mc.delete(bakedGeomList)
            #EXPORT EACH GEOMETRY IN A SEPARATE FILE
            else:
                #Bake geometry and export immediately
                for e in geoDict.keys():
                    bakedGeom = ""
                    exporter = ""
                    #Bake with vertex bake method
                    if self.cacheMod.CacheBakingMethods_Component.vertexBake_RBTN.isChecked():
                        bakedGeom = bakingMethods.vertexBakeMethod(e)
                        #Create output object
                        exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, thisPath, 1)
                    #Bake with normal alembic procedures (transforms when it feels it's convenient, with entire hierarchies)
                    elif self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                        bakedGeom = e
                        #Create output object
                        exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, thisPath, 1)
                    
                    #Export
                    if self.cacheMod.CacheFileTypes_Component.pointCache_RBTN.isChecked():
                        exporter.exportPointCache()
                        if self.exportObjs_CHKBOX.isChecked():
                            exporter.exportObj()
                    elif self.cacheMod.CacheFileTypes_Component.alembic_RBTN.isChecked():
                        cleanName = geoDict[e][1] + "_" + geoDict[e][0] + "_bkd"
                        if self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                            exporter.exportAlembicBake(cleanName)
                        else:
                            exporter.exportAlembic()
                    elif self.cacheMod.CacheFileTypes_Component.alembicExo_RBTN.isChecked():
                        cleanName = geoDict[e][1] + "_" + geoDict[e][0] + "_bkd"
                        if self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                            if self.exoExportSurface_CHKBOX.isChecked():
                                exporter.exportAlembicBakeExo(cleanName, False, True)
                            else:
                                exporter.exportAlembicBakeExo(cleanName)
                        else:
                            if self.exoExportSurface_CHKBOX.isChecked():
                                exporter.exportAlembicExo(cleanName, False, True)
                            else:
                                exporter.exportAlembicExo(cleanName)
                    
                    #Delete baked geometry
                    if not self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                        #Delete clusters list
                        clustersList = bakingMethods.getClustersList()
                        mc.delete(clustersList)
                        mc.delete(bakedGeom)
                        bakingMethods.resetGeomClusters()
            
            self.progComp.reset()
            
            #Restart refresh
            mc.refresh(suspend=False)
            mc.evaluationManager(mode=currentEvalManager)
            
            #Done!!
            self.baseMod.statusBar_update("Done pre-publishing rig.", success=True)
    
    
    def buildCachePath(self, _assetType, _assetName, _subAssetName):
        propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
        if _subAssetName not in [None,""]:
            propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_subasset",
                        "placeholder":False}
        publishedNodes = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
        
        assetTypeDict={"name":"CHARACTERS",
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
        assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
        for node in publishedNodes:
            pathList = self.pInfo.rebuildPath(node, {_assetType:assetTypeDict,_assetName:assetDict})
            if _subAssetName not in [None,""]:
                pathList = self.pInfo.rebuildPath(node, {_assetType:assetTypeDict,_assetName:assetDict,_subAssetName:subAssetDict})
            pathList.append(node.name)
            #Build path string
            server = self.sInfo.getProjectsFrom()
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
            
            if "$" not in pathStr:
                return pathStr + "/Cache360"
    
    
    def createTags(self):
        #Enlistar roots
        mainNode = self.charsProps_TREEVIEW.model().dataList

        for root in mainNode.children:
            if not mc.objExists(root.data[0] + "_cgxGeoSet"):
                geoSet = mc.sets(name= root.data[0] + "_cgxGeoSet", empty= True)
                for geo in root.children:
                    item = geo.data[2]
                    attrExist = mc.listAttr(item, string= "CGX_geoExport", locked= True)
                    if attrExist == None:
                        #Add, set and lock geo attributes
                        mc.addAttr(item, longName= "CGX_geoExport", attributeType='bool')
                        mc.setAttr(item + ".CGX_geoExport", 1)
                        mc.setAttr(item + ".CGX_geoExport", lock= True)
                                        
                        mc.addAttr(item, longName= "CGX_rootName", dt= "string")
                        rootName = geo.parent.data[0]
                        mc.setAttr(item + ".CGX_rootName", rootName, type= "string")
                        mc.setAttr(item + ".CGX_rootName", lock= True)
                        
                        mc.addAttr(item, longName= "CGX_itemName", dt= "string")
                        userName = geo.data[0]
                        mc.setAttr(item + ".CGX_itemName", userName, type= "string")
                        mc.setAttr(item + ".CGX_itemName", lock= True)
                        
                        mc.sets(item, edit= True, forceElement=geoSet)
                    else:
                        self.baseMod.statusBar_update(geo.data[0] + " has already been tagged.", alert=True)
            else:
                self.baseMod.statusBar_update(root.data[0] + "_cgxGeoSet" + " already exists.", alert=True)
    
    
    def removeExistingTags(self):
        selShapes = mc.ls(sl=True, dag=True, leaf=True, long= True, ni= True, type= "mesh")
        if len(selShapes) >= 1:
            for e in selShapes:
                transformNode = mc.listRelatives(e, parent=True, fullPath=True)[0]
                attrExistCheck = mc.listAttr(transformNode, string= "CGX_geoExport", locked= True)
                attrExistRoot = mc.listAttr(transformNode, string= "CGX_rootName", locked= True)
                attrExistItem = mc.listAttr(transformNode, string= "CGX_itemName", locked= True)
                if attrExistCheck != None:
                    mc.setAttr(transformNode + ".CGX_geoExport", lock= False)
                    mc.deleteAttr(transformNode, attribute= "CGX_geoExport")
                if attrExistRoot != None:
                    rootName = mc.getAttr(transformNode + ".CGX_rootName")
                    setName = rootName + "_cgxGeoSet"
                    mc.setAttr(transformNode + ".CGX_rootName", lock= False)
                    mc.deleteAttr(transformNode, attribute= "CGX_rootName")
                    if mc.objExists(setName):
                        #Delete transformNode from set
                        mc.sets(transformNode, remove=setName)
                        #If set is empty delete set
                        setMembers= mc.sets(setName, q=True)
                        if setMembers == None:
                            mc.delete(setName)
                if attrExistItem != None:
                    mc.setAttr(transformNode + ".CGX_itemName", lock= False)
                    mc.deleteAttr(transformNode, attribute= "CGX_itemName")
                if attrExistCheck == None and attrExistRoot == None and attrExistItem == None:
                    self.baseMod.statusBar_update(transformNode + " has not been tagged.", alert=True)
        else:
            self.baseMod.statusBar_update("Select at least one object to remove the tags from.", alert=True)
        
    
    def selectedCtrl(self):
        curvesSel = []
        for each in mc.ls(sl=True):
            if mc.nodeType(each) == "transform":
                shapeNope = mc.listRelatives(each, shapes=True, noIntermediate=True, fullPath=True)[0]
                if mc.nodeType(shapeNope) == "nurbsCurve":
                    curvesSel.append(each)
            elif mc.nodeType(each) == "nurbsCurve":
                transform = mc.listRelatives(each, parent=True, fullPath=True)[0]
                curvesSel.append(transform)
        
        if len(curvesSel) == 1:
            self.rigCtrl_LINEEDIT.setText(curvesSel[0])
        elif len(curvesSel) > 1:
            self.baseMod.statusBar_update("Please select only one control.", alert=True)
        else:
            self.baseMod.statusBar_update("No control was found in your selection.", status=True)
    
    
    def selectedGeometry(self):
        geoSel = []
        selShapes = mc.ls(sl=True, dag=True, leaf=True, long= True, ni= True, type= "mesh")
        if len(selShapes) >= 1:
            for each in selShapes:
                if mc.nodeType(each) == "transform":
                    shapeNope = mc.listRelatives(each, shapes=True, noIntermediate=True, fullPath=True)[0]
                    if mc.nodeType(shapeNope) == "mesh":
                        geoSel.append(each)
                elif mc.nodeType(each) == "mesh":
                    transform = mc.listRelatives(each, parent=True,fullPath=True)[0]
                    geoSel.append(transform)
            
            if len(geoSel) >= 1:
                self.autoCreateTags(geoSel)
            elif len(geoSel) < 1:
                self.baseMod.statusBar_update("No geometry was found in your selection.", status=True)
    
    
    def autoCreateTags(self, _geoSel):
        assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
        subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
        thisRoot = ""
        if subAssetName not in [None,""]:
            thisRoot = self.addTreeItem(subAssetName, "root", subAssetName)
        else:
            thisRoot = self.addTreeItem(assetName, "root", assetName)
        for each in _geoSel:
            self.usedGeom.append(each)
            shortName = each.rsplit("|",1)[1]
            geoNode = self.charsProps_TREEVIEW.model().insertRows(thisRoot.childCount(), 1, [shortName,"geo", each], self.charsProps_TREEVIEW.model().indexFromNode(thisRoot))
    
    
    def addTreeItem (self, item, itemType, internalName):
        if itemType == "root":
            return self.charsProps_TREEVIEW.model().insertRows(self.charsProps_TREEVIEW.model().dataList.childCount(), 1,[item,itemType,"internalName"])
        elif itemType == "geo":
            selected = self.charsProps_TREEVIEW.selectedIndexes()
            if selected[0].isValid():
                indexNode = self.charsProps_TREEVIEW.model().getNode(selected[0])
                return self.charsProps_TREEVIEW.model().insertRows(indexNode.childCount(), 1, [item,itemType,internalName], selected[0])
            
    
    def populateTree(self):
        """To populate the Characters/Props tree with the scene pre-tagged geometry"""
        #List all CGX sets
        allSets = mc.ls(type="objectSet")
        cgxSets = []
        if len(self.usedGeom) >= 1:
            self.usedGeom[:] = []
        for geoset in allSets:
            if geoset[-9:] == "cgxGeoSet":
                cgxSets.append(geoset)
        cgxSets.sort()
        if len(cgxSets) >= 1:
            for e in cgxSets:
                #Remove namespace if exists
                cleanName = e[:-10]
                if mc.referenceQuery(e, isNodeReferenced= True):
                    thisNamespace = mc.referenceQuery(e, namespace= True, shortName= True)
                    if thisNamespace != "":
                        cleanName = cleanName[len(thisNamespace)+1:]
                #Crear root
                charProp = self.addTreeItem(cleanName, "root", e)
                setMembers= mc.sets(e, query= True)
                for dagNode in setMembers:
                    #Remove namespace if exists
                    child = dagNode
                    if mc.referenceQuery(dagNode, isNodeReferenced= True):
                        thisNamespace = mc.referenceQuery(dagNode, namespace= True, shortName= True)
                        if thisNamespace != "":
                            child = dagNode[len(thisNamespace)+1:]
                    self.usedGeom.append(dagNode)
                    userItemName = mc.getAttr(dagNode + ".CGX_itemName")
                    self.charsProps_TREEVIEW.model().insertRows(charProp.childCount(), 1,[userItemName,"geo", dagNode], self.charsProps_TREEVIEW.model().indexFromNode(charProp))
        else:
            self.baseMod.statusBar_update("The scene doesn't have any tagged Character/Prop.", status=True)


    def refreshTree(self):
        #Set Chars/Props model
        headers = ["Characters/Props", "Root/Geo", "internalName"]
        charsPropsRoot = dvm.TreeNode(["Characters/Props", "Root/Geo", "internalName"], headers)
        charsPropsModel = CharPropsDataTreeModel(charsPropsRoot,headers, self.charsProps_TREEVIEW)
        self.charsProps_TREEVIEW.setModel(charsPropsModel)
        self.charsProps_TREEVIEW.hideColumn(1)
        self.charsProps_TREEVIEW.hideColumn(2)
        self.charsProps_TREEVIEW.setSelectionMode(self.charsProps_TREEVIEW.MultiSelection)
        
        if self.skipTags_CHKBOX.isChecked():
            self.populateTree()
        else:
            if self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText() not in [None,""]:
                if mc.objExists("*_cgxGeoSet"):
                    self.baseMod.statusBar_update("This project already contains a cgxGeoSet with tagged geometry. Please remove it or use the Skip Tags Creation option.", warning=True)
                else:
                    #Auto populate
                    #Guess control
                    self.selectedCtrl()
                    #Guess geometry
                    self.selectedGeometry()
                
    
    def refreshSkipTags(self):
        if self.skipTags_CHKBOX.isChecked():
            self.refreshTree()
            self.charsProps_TREEVIEW.setEnabled(False)
        else:
            self.charsProps_TREEVIEW.setEnabled(True)
    
    
    def refreshAnimation(self):
        if self.create360_CHKBOX.isChecked():
            self.rigCtrl_LABEL.setEnabled(True)
            self.rigCtrl_LINEEDIT.setEnabled(True)
            self.rigCtrl_BTN.setEnabled(True)
            self.cacheMod.container.setEnabled(True)
            self.exportObjs_CHKBOX.setEnabled(True)
            self.filePerCharProp_CHKBOX.setEnabled(True)
            self.exoExportSurface_CHKBOX.setEnabled(True)
            self.output_LABEL.setEnabled(True)
            self.upAxis_LABEL.setEnabled(True)
            self.x_RBTN.setEnabled(True)
            self.y_RBTN.setEnabled(True)
            self.z_RBTN.setEnabled(True)
        else:
            self.rigCtrl_LABEL.setEnabled(False)
            self.rigCtrl_LINEEDIT.setEnabled(False)
            self.rigCtrl_BTN.setEnabled(False)
            self.cacheMod.container.setEnabled(False)
            self.exportObjs_CHKBOX.setEnabled(False)
            self.filePerCharProp_CHKBOX.setEnabled(False)
            self.exoExportSurface_CHKBOX.setEnabled(False)
            self.output_LABEL.setEnabled(False)
            self.upAxis_LABEL.setEnabled(False)
            self.x_RBTN.setEnabled(False)
            self.y_RBTN.setEnabled(False)
            self.z_RBTN.setEnabled(False)
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods for adding/removing tags
    ##--------------------------------------------------------------------------------------------     
    def charsPropsOptions (self,pos):
        '''
        Right-click popup menu for cameras list.
        '''
        menu = QtWidgets.QMenu(self.charsProps_TREEVIEW)
        addRootTagQ = menu.addAction("Add root")
        addGeoTagQ = menu.addAction("Add geo tag")
        removeItemQ = menu.addAction("Remove tag")
        selectgGeoQ = menu.addAction("Select Geo")
        menu.popup(self.charsProps_TREEVIEW.mapToGlobal(pos))
        
        addRootTagQ.triggered.connect(self.addRoot)
        addGeoTagQ.triggered.connect(self.tagGeo)
        removeItemQ.triggered.connect(self.removeTag)
        selectgGeoQ.triggered.connect(self.selectGeoForItem)
    
    
    def addRoot(self):
        self.addTreeItem("Type Character/Prop name here", "root", "internalName")
    
    
    def tagGeo(self):
        allSel = mc.ls(sl= True, long=True)
        if len(allSel) <= 0:
            self.baseMod.statusBar_update("Nothing is selected. Select at least one node to be tagged.", status=True)
        else:
            for item in allSel:
                if self.usedGeom.count(item) == 0:
                    attrExist = mc.listAttr(item, string= "CGX_geoExport", locked= True)
                    if attrExist == None:
                        selectedRootItem = self.charsProps_TREEVIEW.selectedIndexes()
                        itemShape = mc.listRelatives(item , shapes = True, noIntermediate = True, fullPath=True)
                        thisNodeType = mc.nodeType(itemShape[0])
                        if thisNodeType == "mesh" or thisNodeType == "nurbsSurface" or thisNodeType == "subdiv":
                            if len(selectedRootItem) >= 1:
                                #Add item to tree view
                                shortName = item.rsplit("|",1)[1]
                                self.addTreeItem(shortName, "geo", item)
                                self.usedGeom.append(item)
                            else:
                                self.baseMod.statusBar_update("You must select one Character/Prop item to add this geo to.", status=True)
                        else:
                            self.baseMod.statusBar_update(item + " is not a valid node.", alert=True)
                    else:
                        self.baseMod.statusBar_update(item + " has already been tagged.", alert=True)
                else:
                    self.baseMod.statusBar_update(item + " has already been added to a Character/Prop.", alert=True)
    
    
    def removeTag(self):
        selected = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
        if len(selected) >= 1:
            delChildren= []
            for e in range(len(selected)):
                item = selected[e]
                if item.isValid():
                    tagType= self.charsProps_TREEVIEW.model().getNode(item).data
                    if tagType[1] == 'root':
                        setName = tagType[0].replace(" ", "_").replace("/","_") + "_cgxGeoSet"
                        children = self.charsProps_TREEVIEW.model().getNode(item).children
                        if mc.objExists(setName):
                            mc.delete(setName)
                        for child in children:
                            geoName = child.data[2]
                            delChildren.append(geoName)
                            self.usedGeom.remove(geoName)
                            attrExistCheck = mc.listAttr(geoName, string= "CGX_geoExport", locked= True)
                            attrExistRoot = mc.listAttr(geoName, string= "CGX_rootName", locked= True)
                            attrExistItem = mc.listAttr(geoName, string= "CGX_itemName", locked= True)
                            if attrExistCheck != None:
                                mc.setAttr(geoName + ".CGX_geoExport", lock= False)
                                mc.deleteAttr(geoName, attribute= "CGX_geoExport")
                            if attrExistRoot != None:
                                mc.setAttr(geoName + ".CGX_rootName", lock= False)
                                mc.deleteAttr(geoName, attribute= "CGX_rootName")
                            if attrExistItem != None:
                                mc.setAttr(geoName + ".CGX_itemName", lock= False)
                                mc.deleteAttr(geoName, attribute= "CGX_itemName")
                    elif tagType[1] == 'geo':
                        geoName = self.charsProps_TREEVIEW.model().getNode(item).data[2]
                        if geoName not in delChildren:
                            self.usedGeom.remove(geoName)
                        attrExistCheck = mc.listAttr(geoName, string= "CGX_geoExport", locked= True)
                        attrExistRoot = mc.listAttr(geoName, string= "CGX_rootName", locked= True)
                        attrExistItem = mc.listAttr(geoName, string= "CGX_itemName", locked= True)
                        if attrExistCheck != None:
                            mc.setAttr(geoName + ".CGX_geoExport", lock= False)
                            mc.deleteAttr(geoName, attribute= "CGX_geoExport")
                        if attrExistRoot != None:
                            mc.setAttr(geoName + ".CGX_rootName", lock= False)
                            mc.deleteAttr(geoName, attribute= "CGX_rootName")
                        if attrExistItem != None:
                            mc.setAttr(geoName + ".CGX_itemName", lock= False)
                            mc.deleteAttr(geoName, attribute= "CGX_itemName")
                        setName = self.charsProps_TREEVIEW.model().getNode(item).parent.data[0] + "_cgxGeoSet"
                        if mc.objExists(setName):
                            mc.sets(geoName, remove=setName)
            #Remove from UI
            self.charsProps_TREEVIEW.model().removeRows(selected)
    
    
    def selectGeoForItem(self):
        selected = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
        mc.select(clear=True)
        if len(selected) >= 1:
            for e in range(len(selected)):
                item = selected[e]
                if item.isValid():
                    tagType= self.charsProps_TREEVIEW.model().getNode(item).data
                    if tagType[1] == "root":
                        children = self.charsProps_TREEVIEW.model().getNode(item).children
                        for x in children:
                            geoName = x.data[2]
                            mc.select(geoName, add = True)
                    elif tagType[1] == "geo":
                        geoName = item.internalPointer().data[2]
                        mc.select(geoName, add = True)
    
    
    def filterSelected(self, _indexes):
        '''
        Given a list of indexes, return only one index per row.
        '''
        if len(_indexes) > 0:
            #Group by parent first
            filterDict = {}
            for each in _indexes:
                if each.parent() not in filterDict.keys():
                    filterDict[each.parent()] = [each]
                else:
                    filterDict[each.parent()].append(each)
            #Delete redundancies
            filteredList = []
            for key in filterDict.keys():
                usedRows = []
                for item in filterDict[key]:
                    if item.row() not in usedRows:
                        usedRows.append(item.row())
                        filteredList.append(item)
            
            return filteredList
        else:
            return _indexes


##--------------------------------------------------------------------------------------------
##Class: Tree Data Model reimplementation for Chars/Props tree
##--------------------------------------------------------------------------------------------
class CharPropsDataTreeModel(dvm.DataTreeModel):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, root, headers, parent):
        '''
        DataTreeModel for Model/View programming.
        :param root: root TreeNode for this model.
        :type root: TreeNode
        :param headers: list of headers to be displayed in the table. Must be same len as data passed to rootnode
        :type headers: list
        :param parent: QWidget that uses this model. Default=None
        :type parent: QWidget
        '''
        super(CharPropsDataTreeModel, self).__init__(root, headers, parent)
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def flags(self, index):
        '''
        Set flags for each item.  Re-implement if different flags are needed and use conditional chains if per-item flags are needed.
        :param index: Item to be filtered
        :type index: QModelIndex
        :return: Flags to be used
        :rtype: QtCore.Qt.Flags
        '''
        if not index.isValid():
            return QtCore.Qt.NoItemFlags
        else:
            return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable


##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent):
        '''
        Creates Help Dialog for current tool.
        :param parent: Main tools window.
        :type parent: QMainWindow
        '''
        super(Help_DialogGUI,self).__init__(parent)
        self.setupUi(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent):
        '''
        Creates About Dialog for current tool.
        :param parent: Main tools window.
        :type parent: QMainWindow
        '''
        super(About_DialogGUI,self).__init__(parent)
        self.setupUi(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent):
        '''
        Creates Config Dialog for current tool.
        :param parent: Main tools window.
        :type parent: QMainWindow
        '''
        super(Config_DialogGUI,self).__init__(parent)
        self.setupUi(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        
            
##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    if __DEVMODE:
        x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/pipeline/CacheTagger_dev.py','r')
        exec x.read()
        x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
    main()