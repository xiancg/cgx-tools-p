# -*- coding: utf-8 -*-
'''
Cache Tagger tool. Tag geometry for caching.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
from cgx.gui.Qt import QtWidgets, QtCore
from cgx.gui.Qt import __binding__
import Red9.core.Red9_Meta as r9Meta
import cgx.Maya.scripts.maya_libs.Maya_MetaData as cgxMeta
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
import cgx.gui.DataViewModels as dvm
from cgx.core.JSONManager import JSONManager

import maya.cmds as mc


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "3.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
    appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/pipeline/CacheTagger'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"
import cgx.Maya.scripts.workflows.Animation.AnimationProtocole as aniProt
aniProtFolder = os.path.dirname(os.path.abspath(aniProt.__file__))
aniProt_config_json = aniProtFolder + "/cfg/" + "AnimationProtocole_config.json"
import cgx.Maya.scripts.pipeline.CacheExporter.CacheExporter as chExp
chExpFolder = os.path.dirname(os.path.abspath(chExp.__file__))
alembicVersion_json = chExpFolder + "/cfg/" + "AlembicVersion_config.json"
alembicVersion_ui = chExpFolder + "/ui/" + "alembicVersion.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)
alembicVersionform, alembicVersionbase = uiParser.loadUiType(alembicVersion_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
        '''
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
        '''
        super(Main_GUI,self).__init__(parent)
        self.setupUi(self)
        
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        
        self.appTools = appTools
        if self.appTools != None:
            self.initTool()
            self.createComponents()
            self.setConnections()
            self.initComponents()
            self.initUI()
            self.checkPlugins()
            self.refreshModels()
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        
        
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def setConnections(self):
        '''
        Connect signals.
        '''
        #SIGNALS
        self.edit_removeTags.triggered.connect(self.removeExistingTags)
        self.rigCtrl_BTN.clicked.connect(self.selectedMainCtrl)
        self.prePublishRig_BTN.clicked.connect(self.prePublishRig)
        self.alembicVersion_opt.triggered.connect(self.alembicVersionDialog)
        #REFRESH
        self.create360_CHKBOX.clicked.connect(self.refreshAnimation)
        #STATUS
        self.y_RBTN.setChecked(True)
        self.filePerCharProp_CHKBOX.setChecked(True)
        
        #CONTEXT MENUS
        self.charsProps_TREEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.charsProps_TREEVIEW.customContextMenuRequested.connect(self.charsPropsOptions)
        self.ctrls_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ctrls_TABLEVIEW.customContextMenuRequested.connect(self.ctrlsOptions)
        
    
    def initTool(self):
        '''
        Initializes tool settings.
        '''
        self.initAttrs()
    
    
    def resetTool(self):
        self.initAttrs()
        self.initComponents()
        self.initUI()
        self.checkPlugins()
        self.refreshModels()
        
        
    def initAttrs(self):
        '''
        Initializes variables and components for this tool.
        '''
        self.__client = "defaultClient"
        self.__toolName = "Cache Tagger"
        self.__toolVersion = __version__
        
        #Set time range
        self.appTools.tRange.handleIn = 0
        self.appTools.tRange.handleOut = 0
        self.appTools.tRange.startFrame = 1
        self.appTools.tRange.endFrame = 100
        
        #Ctrl wildcards
        self.configFile = JSONManager(aniProt_config_json, self)
        
        #Attributes
        self.usedGeom = []
        
    
    def createComponents(self):
        self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
        self.baseMod = self.Base_GUIModulesFactory.createModule('base')
        self.baseMod.MainMenu_Component.edit_config.setVisible(False)
        self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo, tRange=self.appTools.tRange)
        self.projAssetsMod = self.Pipeline_GUIModulesFactory.createModule('ProjAssets')
        self.projAssetsMod.pos = [0,40]
        self.fpsComp = pipeComponents.FPS_Component(self,self.centralWidget(), self.pInfo)
        self.fpsComp.pos = [265,45]
        self.cacheMod = self.Pipeline_GUIModulesFactory.createModule('Cache') 
        self.cacheMod.pos = [0,300]
        self.progComp = pipeComponents.Progress_Component(self,self.centralWidget())
        self.progComp.pos = [350,410]
        self.progComp.container.setHidden(True)
        
        #Add Remove Existing Tags menu option
        self.edit_removeTags = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
        self.edit_removeTags.setObjectName("edit_removeTags")
        self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[1], self.edit_removeTags)
        className= str(self.__class__.__name__)
        #Add alembic version option
        self.alembicVersion_opt = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
        self.alembicVersion_opt.setObjectName("alembic_version")
        self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[2], self.alembicVersion_opt)
        if __binding__ in ('PySide', 'PyQt4'):
            self.edit_removeTags.setText(QtWidgets.QApplication.translate(className, "Remove Existing Tags", None, QtWidgets.QApplication.UnicodeUTF8))
            self.alembicVersion_opt.setText(QtWidgets.QApplication.translate(className, "Alembic Version", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.edit_removeTags.setText(QtWidgets.QApplication.translate(className, "Remove Existing Tags", None))
            self.alembicVersion_opt.setText(QtWidgets.QApplication.translate(className, "Alembic Version", None))
        
    
    def initComponents(self):
        self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
        self.appTools.app_initUI.setCurrentProject(self.projAssetsMod.Project_Component)
        self.appTools.app_initUI.setCurrentAssetType(self.projAssetsMod.AssetTypes_Component)
        self.appTools.app_initUI.setCurrentAsset(self.projAssetsMod.Assets_Component)
        self.appTools.app_initUI.setCurrentSubAsset(self.projAssetsMod.SubAssets_Component)
        self.appTools.app_initUI.setCurrentFileType(self.projAssetsMod.FileTypes_Component)
        self.fpsComp.initComponent(self.pInfo.maya_getFPS())
        self.appTools.app_initUI.checkFPS(self.fpsComp)
    
    
    def initUI(self):
        '''
        Initialize GUI components.
        '''
        pass
    
    
    def checkPlugins(self):
        '''
        Check if plugins are loaded.
        :return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
        :rtype: dict
        '''
        checkDict = self.appTools.checkPlugins(self)
        return checkDict
    
    
    def prePublishRig(self):
        if self.charsProps_TREEVIEW.model().rowCount(QtCore.QModelIndex()) <= 0:
            self.baseMod.statusBar_update("You have to create a Root and add geometry to it for this tool to work.", warning=True)
        elif not mc.objExists(self.rigCtrl_LINEEDIT.text()) and self.create360_CHKBOX.isChecked():
            self.baseMod.statusBar_update("Rig control doesn't exist in the scene.", warning=True)
        else:
            if self.create360_CHKBOX.isChecked():
                metaTags = r9Meta.getMetaNodes(['CGXCacheMetaTag'])
                if len(metaTags) < 1:
                    self.baseMod.statusBar_update("Please tag something before Pre-Publishing.", warning=True)
                else:
                    self.createAnimation()
                    self.exportCache()
                    self.deleteAnimation()
    
    
    def createAnimation(self):
        rigCtrl = self.rigCtrl_LINEEDIT.text()
        upAxis = "y"
        if self.x_RBTN.isChecked():
            upAxis = "x"
        elif self.z_RBTN.isChecked():
            upAxis = "z"
        
        mc.setKeyframe(rigCtrl + ".r" + upAxis, itt="linear", ott="linear", time=1, value= 0)
        mc.setKeyframe(rigCtrl + ".r" + upAxis, itt="linear", ott="linear", time=101, value= 360)
    
    
    def deleteAnimation(self):
        rigCtrl = self.rigCtrl_LINEEDIT.text()
        upAxis = "y"
        if self.x_RBTN.isChecked():
            upAxis = "x"
        elif self.z_RBTN.isChecked():
            upAxis = "z"
            
        mc.cutKey(rigCtrl, cl=True, at= "r" + upAxis, t=(1,101), f=(1,101))
        mc.setAttr(rigCtrl + ".r" + upAxis, 0)
    
    
    def exportCache(self):
        check = self.checkPlugins()
        if len(check.keys()) <= 0:
            try:
                #Set progress bar
                self.progComp.reset()
                self.progComp.minimum = 0
                
                #Stop refresh
                mc.refresh(suspend=True)
                currentEvalManager = mc.evaluationManager( query=True, mode=True )[0]
                mc.evaluationManager(mode='off')
                
                assetType = self.projAssetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
                assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
                subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
                
                #Collect geometry for baking and create Baking Method object
                geoDict = {} #GEO_CURRENT_NAME = [GEO_USER_NAME, rootName, rootNode]. Using node name to store reference to real object, and datalist for user values
                roots = self.charsProps_TREEVIEW.model().dataList.children
                for each in roots:
                    childGeom = each.children
                    for item in childGeom:
                        geoCurrentName = item.data[2]
                        geoUserName = item.name
                        if geoCurrentName in geoDict:
                            self.baseMod.statusBar_update("Found duplicated names in the project. Some geometry will be missing after exporting.", warning=True)
                        else:
                            geoDict[geoCurrentName] = [geoUserName, each.name, each]
                bakingMethods = self.appTools.createBakingMethods(geoDict, self.progComp.progress_PBAR, 1)
                
                #More progress bar
                multProgress = 1
                if self.exportObjs_CHKBOX.isChecked():
                    multProgress = 2
                self.progComp.maximum = len(geoDict.keys()) * multProgress
                
                #CREATE FOLDERS
                thisPath = self.buildCachePath(assetType, assetName, subAssetName)
                finalPathObjs = thisPath + "/OBJs"
                if not os.path.exists(thisPath):
                    os.makedirs(thisPath)
                if self.exportObjs_CHKBOX.isChecked():
                    if not os.path.exists(finalPathObjs):
                        os.makedirs(finalPathObjs)
                
                #EXPORT ONE FILE PER CHARACTER/PROP
                if self.filePerCharProp_CHKBOX.isChecked():
                    #Bake geometry first, then export
                    #Create a list of used roots
                    rootList = []
                    for e in geoDict.keys():
                        if geoDict[e][2] not in rootList:
                            rootList.append(geoDict[e][2])
                    #Loop through rootList and compare to geoDict to bake and export
                    for rootNode in rootList:
                        rootName = rootNode.name
                        bakedGeomList = []
                        for e in geoDict.keys():
                            if geoDict[e][2] == rootNode:
                                #Bake geometry
                                bakedGeom = ""
                                if self.cacheMod.CacheBakingMethods_Component.vertexBake_RBTN.isChecked():
                                    bakedGeom = bakingMethods.vertexBakeMethod(e)
                                    bakedGeomList.append(bakedGeom)
                                elif self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                                    bakedGeom = e
                                    bakedGeomList.append(bakedGeom)
                        exporter = self.appTools.createExporter(bakedGeomList, self.appTools.tRange, self.progComp.progress_PBAR, thisPath, 1)
                        #Export baked geom list
                        cleanName = rootName + "_bkd_1"
                        if self.cacheMod.CacheFileTypes_Component.pointCache_RBTN.isChecked():
                            exporter.exportPointCache(cleanName, True)
                            if self.exportObjs_CHKBOX.isChecked():
                                exporter.exportObj()
                        elif self.cacheMod.CacheFileTypes_Component.alembic_RBTN.isChecked():
                            if self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                                exporter.exportAlembicBake(cleanName, True)
                            else:
                                exporter.exportAlembic(cleanName, True)
                        elif self.cacheMod.CacheFileTypes_Component.alembicExo_RBTN.isChecked():
                            if self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                                if self.exoExportSurface_CHKBOX.isChecked():
                                    exporter.exportAlembicBakeExo(cleanName, True, True)
                                else:
                                    exporter.exportAlembicBakeExo(cleanName, True)
                            else:
                                if self.exoExportSurface_CHKBOX.isChecked():
                                    exporter.exportAlembicExo(cleanName, True, True)
                                else:
                                    exporter.exportAlembicExo(cleanName, True)
                        #Delete baked geometry
                        if not self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                            mc.delete(bakedGeomList)
                #EXPORT EACH GEOMETRY IN A SEPARATE FILE
                else:
                    #Bake geometry and export immediately
                    for e in geoDict.keys():
                        bakedGeom = ""
                        exporter = ""
                        #Bake with vertex bake method
                        if self.cacheMod.CacheBakingMethods_Component.vertexBake_RBTN.isChecked():
                            bakedGeom = bakingMethods.vertexBakeMethod(e)
                            #Create output object
                            exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, thisPath, 1)
                        #Bake with normal alembic procedures (transforms when it feels it's convenient, with entire hierarchies)
                        elif self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                            bakedGeom = e
                            #Create output object
                            exporter = self.appTools.createExporter(bakedGeom, self.appTools.tRange, self.progComp.progress_PBAR, thisPath, 1)
                        
                        #Export
                        if self.cacheMod.CacheFileTypes_Component.pointCache_RBTN.isChecked():
                            exporter.exportPointCache()
                            if self.exportObjs_CHKBOX.isChecked():
                                exporter.exportObj()
                        elif self.cacheMod.CacheFileTypes_Component.alembic_RBTN.isChecked():
                            cleanName = geoDict[e][1] + "_" + geoDict[e][0] + "_bkd"
                            if self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                                exporter.exportAlembicBake(cleanName)
                            else:
                                exporter.exportAlembic()
                        elif self.cacheMod.CacheFileTypes_Component.alembicExo_RBTN.isChecked():
                            cleanName = geoDict[e][1] + "_" + geoDict[e][0] + "_bkd"
                            if self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                                if self.exoExportSurface_CHKBOX.isChecked():
                                    exporter.exportAlembicBakeExo(cleanName, False, True)
                                else:
                                    exporter.exportAlembicBakeExo(cleanName)
                            else:
                                if self.exoExportSurface_CHKBOX.isChecked():
                                    exporter.exportAlembicExo(cleanName, False, True)
                                else:
                                    exporter.exportAlembicExo(cleanName)
                        
                        #Delete baked geometry
                        if not self.cacheMod.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
                            #Delete clusters list
                            clustersList = bakingMethods.getClustersList()
                            mc.delete(clustersList)
                            mc.delete(bakedGeom)
                            bakingMethods.resetGeomClusters()
                
                self.progComp.reset()
                
                #Restart refresh
                mc.refresh(suspend=False)
                mc.evaluationManager(mode=currentEvalManager)
            except OSError as err:
                self.baseMod.statusBar_update("There was an error, nothing was exported. Check Script Editor.", warning=True)
                print("OSError: {}".format(err))
            except ValueError as err:
                self.baseMod.statusBar_update("There was an error, nothing was exported. Check Script Editor.", warning=True)
                print("ValueError: {}".format(err))
            except RuntimeError as err:
                self.baseMod.statusBar_update("There was an error, nothing was exported. Check Script Editor.", warning=True)
                print("RuntimeError: {}".format(err))
            except IndexError as err:
                self.baseMod.statusBar_update("There was an error, nothing was exported. Check Script Editor.", warning=True)
                print("IndexError: {}".format(err))
            except StandardError as err:
                self.baseMod.statusBar_update("There was an error, nothing was exported. Check Script Editor.", warning=True)
                print("StandardError: {}".format(err))
            else:
                #Done!!
                self.baseMod.statusBar_update("Done pre-publishing rig.", success=True)
    
    
    def buildCachePath(self, _assetType, _assetName, _subAssetName):
        propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
        if _subAssetName not in [None,""]:
            propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_subasset",
                        "placeholder":False}
        publishedNodes = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
        
        assetTypeDict={"name":_assetType,
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
        assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        if _assetType == 'PROPS':
            assetDict= {"name":"$PropName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        elif _assetType == 'VEHICLES':
            assetDict= {"name":"$VehicleName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        elif _assetType == 'SETS':
            assetDict= {"name":"$SetName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
        for node in publishedNodes:
            pathList = self.pInfo.rebuildPath(node, {_assetType:assetTypeDict,_assetName:assetDict})
            if _subAssetName not in [None,""]:
                pathList = self.pInfo.rebuildPath(node, {_assetType:assetTypeDict,_assetName:assetDict,_subAssetName:subAssetDict})
            pathList.append(node.name)
            #Build path string
            server = self.sInfo.getProjectsFrom()
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
            
            if "$" not in pathStr:
                return pathStr + "/Cache360"
    
    
    def removeExistingTags(self):
        selShapes = mc.ls(sl=True, dag=True, leaf=True, long= True, ni= True, type= "mesh")
        if len(selShapes) >= 1:
            existingMetaTags = r9Meta.getMetaNodes(['CGXCacheMetaTag'])
            for tag in existingMetaTags:
                usedGeom = tag.getChildren(mAttrs='taggedNodes')
                for e in selShapes:
                    transformNode = mc.listRelatives(e, parent=True, fullPath=True)[0]
                    if usedGeom.count(transformNode) >= 1:
                        tag.disconnectChild(transformNode)
                if len(tag.getChildren()) == 0:
                    tag.delete()
        else:
            self.baseMod.statusBar_update("Select at least one object to remove the tags from.", alert=True)
    
    
    def selectedMainCtrl(self):
        curvesSel = []
        for each in mc.ls(sl=True):
            if mc.nodeType(each) == "transform":
                shapeNope = mc.listRelatives(each, shapes=True, noIntermediate=True, fullPath=True)[0]
                if mc.nodeType(shapeNope) == "nurbsCurve":
                    curvesSel.append(each)
            elif mc.nodeType(each) == "nurbsCurve":
                transform = mc.listRelatives(each, parent=True, fullPath=True)[0]
                curvesSel.append(transform)
        
        if len(curvesSel) == 1:
            self.rigCtrl_LINEEDIT.setText(curvesSel[0])
        elif len(curvesSel) > 1:
            self.baseMod.statusBar_update("Please select only one control.", alert=True)
        else:
            self.baseMod.statusBar_update("No control was found in your selection.", status=True)
    
    
    def selectedCtrls(self):
        allCtrls = self.findControls()
        if len(allCtrls) >= 1:
            mainCtrl = ''
            ctrlWildcards = self.configFile.jsonObj['mainControl']
            mainCtrlFound = False
            for each in allCtrls:
                shortName = each
                if "|" in shortName:
                    shortName = each.rsplit("|",1)[1]
                tagMainCtrl = False
                if not mainCtrlFound:
                    for wildcard in ctrlWildcards:
                        if wildcard in shortName:
                            mainCtrl = each
                            tagMainCtrl = True
                            mainCtrlFound = True
                            break
                metaCtrl = cgxMeta.CGXCacheMetaCtrl(name=shortName + '_META')
                metaCtrl.connectChildren([each], 'taggedNode')
                if tagMainCtrl:
                    metaCtrl.mainControl = True
                #Keyable attributes
                ctrlAttrs = mc.listAttr(each, keyable=True, visible=True, unlocked=True, multi=False)
                if ctrlAttrs != None:
                    for attr in ctrlAttrs:
                        value = mc.getAttr(each + '.' + attr)
                        attrType = mc.getAttr(each + '.' + attr, type=True)
                        if attrType == 'enum':
                            optionsList= mc.attributeQuery(attr, node= each, listEnum= True)[0]
                            metaCtrl.addAttr(attr, value, self.mapAttrType(attrType), enumName=optionsList)
                        else:
                            metaCtrl.addAttr(attr, value, self.mapAttrType(attrType))
                self.ctrls_TABLEVIEW.model().insertRows(self.ctrls_TABLEVIEW.model().rowCount(), 1, [shortName,metaCtrl])
            if mainCtrl not in ['', None, 'None']:
                self.rigCtrl_LINEEDIT.setText(mainCtrl)
            else:
                self.baseMod.statusBar_update("No control found with tokens {}.".format(ctrlWildcards), alert=True)
        else:
            self.baseMod.statusBar_update("No control was found in your selection.", alert=True)
    
    
    def mapAttrType(self, _mayaAttr):
        if _mayaAttr in ['doubleLinear','doubleAngle','double']:
            return 'float'
        elif _mayaAttr in ['long']:
            return 'int'
        else:
            return _mayaAttr
        
    
    def findControls(self):
        #List reference curve nodes
        controlsList = []
        allCurveShapes= mc.ls(type='nurbsCurve', long=True)
        allCurveTransforms = [mc.listRelatives(x, parent=True, fullPath=True)[0] for x in allCurveShapes]
        setTransforms = set(allCurveTransforms)
        filteredTransforms = list(setTransforms)
        #Filter using wildcards
        for control in filteredTransforms:
            shortName = control
            if "|" in shortName:
                shortName = control.rsplit("|",1)[1]
            for wildcard in self.configFile.jsonObj['controlWildcards']:
                if wildcard in shortName:
                    controlsList.append(control)
                    break
        return controlsList
    
    
    def selectedGeometry(self):
        geoSel = []
        selShapes = mc.ls(sl=True, dag=True, leaf=True, long= True, ni= True, type= "mesh")
        if len(selShapes) >= 1:
            for each in selShapes:
                if mc.nodeType(each) == "transform":
                    shapeNope = mc.listRelatives(each, shapes=True, noIntermediate=True, fullPath=True)[0]
                    if mc.nodeType(shapeNope) == "mesh":
                        geoSel.append(each)
                elif mc.nodeType(each) == "mesh":
                    transform = mc.listRelatives(each, parent=True,fullPath=True)[0]
                    geoSel.append(transform)
            
            if len(geoSel) >= 1:
                self.autoCreateTags(geoSel)
            elif len(geoSel) < 1:
                self.baseMod.statusBar_update("No geometry was found in your selection.", status=True)
    
    
    def autoCreateTags(self, _geoSel):
        assetName = self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText()
        subAssetName = self.projAssetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
        thisRoot = ""
        metatag = ""
        if subAssetName not in [None,""]:
            metatag = cgxMeta.CGXCacheMetaTag(name=assetName + '_' + subAssetName)
            metatag.assetName = assetName
            metatag.subassetName = subAssetName
            compName = metatag.assetName + '_' + metatag.subassetName
            thisRoot = self.addTreeItem(compName, "root", compName, metatag)
        else:
            metatag = cgxMeta.CGXCacheMetaTag(name=assetName)
            metatag.assetName = assetName
            thisRoot = self.addTreeItem(metatag.assetName, "root", metatag.assetName, metatag)
        
        metatag.connectChildren(_geoSel, 'taggedNodes')
        for each in _geoSel:
            shortName = each.rsplit("|",1)[1]
            geoNode = self.charsProps_TREEVIEW.model().insertRows(thisRoot.childCount(), 1, [shortName,"geo",each,"None",metatag], self.charsProps_TREEVIEW.model().indexFromNode(thisRoot))
        self.populateInstanceNames()
    
    
    def addTreeItem (self, item, itemType, internalName, metatag=None):
        if itemType == "root":
            instID = metatag.instanceIdentifier
            if instID in ["",None]:
                instID = ""
            return self.charsProps_TREEVIEW.model().insertRows(self.charsProps_TREEVIEW.model().dataList.childCount(), 1,[item,itemType,item,instID,metatag])
        elif itemType == "geo":
            selected = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
            if selected[0].isValid():
                indexNode = self.charsProps_TREEVIEW.model().getNode(selected[0])
                return self.charsProps_TREEVIEW.model().insertRows(indexNode.childCount(), 1, [item,itemType,internalName,"None",metatag], selected[0])
            
    
    def populateTree(self):
        """To populate the Characters/Props tree with the scene pre-tagged geometry"""
        
        metaTags = r9Meta.getMetaNodes(['CGXCacheMetaTag'])
        if len(metaTags) >= 1:
            for metatag in metaTags:
                #Create root item
                rootname = metatag.assetName
                if metatag.subassetName not in ["",None]:
                    rootname = metatag.assetName + "_" + metatag.subassetName
                charProp = self.addTreeItem(rootname, "root", rootname, metatag)
                #Create geo item
                for child in metatag.getChildren(mAttrs='taggedNodes'):
                    #Remove namespace if exists
                    childCleanName = child
                    if '|' in childCleanName:
                        childCleanName = childCleanName.rsplit('|',1)[1]
                    if mc.referenceQuery(child, isNodeReferenced= True):
                        thisNamespace = mc.referenceQuery(child, namespace= True, shortName= True)
                        if thisNamespace != "":
                            childCleanName = childCleanName[len(thisNamespace)+1:]
                    self.charsProps_TREEVIEW.model().insertRows(charProp.childCount(), 1,[childCleanName,"geo",child,"None", metatag], self.charsProps_TREEVIEW.model().indexFromNode(charProp))
            self.populateInstanceNames()
        else:
            self.baseMod.statusBar_update("The scene doesn't have any tagged Character/Prop.", status=True)
    
    
    def populateInstanceNames(self):
        roots = self.charsProps_TREEVIEW.model().dataList.children
        rootNames = []
        for each in roots:
            rootNames.append(each.name)
        rootNamesSet = set(rootNames)
        rootNamesClean = list(rootNamesSet)
        charPropInstances = {}
        for each in rootNamesClean:
            charPropCount = rootNames.count(each)
            charPropInstances[each] = [charPropCount,1]#Dict con nombre del char/prop y numero de repeticiones
        
        for each in roots: 
            folderName = each.data[3]
            rootName = each.name
            prevList = charPropInstances[rootName]
            if folderName in [None,"","None"]:
                folderName = rootName + "_bkd_" + str(prevList[1])
                each.setData(3,folderName)
                metaNode = each.data[4]
                metaNode.instanceIdentifier = folderName
            
            prevList[1] += 1
            charPropInstances[rootName] = prevList
    
    
    def populateCtrls(self):
        metaCtrls = r9Meta.getMetaNodes(['CGXCacheMetaCtrl'])
        if len(metaCtrls) >= 1:
            for metaCtrl in metaCtrls:
                longName = metaCtrl.getCtrl()
                shortName = longName
                if "|" in shortName:
                    shortName = longName.rsplit("|",1)[1]
                self.ctrls_TABLEVIEW.model().insertRows(self.ctrls_TABLEVIEW.model().rowCount(), 1, [shortName,metaCtrl])
        else:
            self.baseMod.statusBar_update("The scene doesn't have any tagged controls.", status=True)
        

    def refreshModels(self):
        #Set Chars/Props model
        headers = ["Characters/Props", "Root/Geo", "internalName", "instanceName", "metaNode"]
        charsPropsRoot = dvm.TreeNode(["Characters/Props", "Root/Geo", "internalName", "instanceName","metaNode"], headers, parent=None)
        charsPropsModel = CharPropsDataTreeModel(charsPropsRoot,headers, self.charsProps_TREEVIEW)
        self.charsProps_TREEVIEW.setModel(charsPropsModel)
        self.charsProps_TREEVIEW.hideColumn(1)
        self.charsProps_TREEVIEW.hideColumn(2)
        self.charsProps_TREEVIEW.hideColumn(4)
        for col in range(charsPropsModel.columnCount()):
            self.charsProps_TREEVIEW.resizeColumnToContents(col)
        self.charsProps_TREEVIEW.setSelectionMode(self.charsProps_TREEVIEW.MultiSelection)
        
        #Set ctrls model
        dataList = []
        ctrlsHeaders = ['ctrl','metaNode']
        ctrlsModel = dvm.DataTableModel(dataList, ctrlsHeaders, self)
        self.ctrls_TABLEVIEW.setModel(ctrlsModel)
        self.ctrls_TABLEVIEW.hideColumn(1)
        htalHeader = self.ctrls_TABLEVIEW.horizontalHeader()
        htalHeader.setStretchLastSection(True)
        for col in range(ctrlsModel.columnCount()):
            self.ctrls_TABLEVIEW.resizeColumnToContents(col)
        self.ctrls_TABLEVIEW.setSelectionMode(self.ctrls_TABLEVIEW.MultiSelection)
        self.ctrls_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.ctrls_TABLEVIEW.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        
        metaTags = r9Meta.getMetaNodes(['CGXCacheMetaTag'])
        metaCtrls = r9Meta.getMetaNodes(['CGXCacheMetaCtrl'])
        if len(metaTags) >= 1 and len(metaCtrls) >= 1:
            self.populateTree()
            self.populateCtrls()
        elif len(metaTags) == 0 and len(metaCtrls) >= 1:
            self.selectedGeometry()
            self.populateCtrls()
        elif len(metaTags) >= 1 and len(metaCtrls) == 0:
            self.selectedCtrls()
            self.populateTree()
        elif len(metaTags) == 0 and len(metaCtrls) == 0:
            if self.projAssetsMod.Assets_Component.assetName_COMBOBOX.currentText() not in [None,""]:
                #Auto populate
                #Guess geometry
                self.selectedGeometry()
                #Guess control
                self.selectedCtrls()
    
    
    def refreshAnimation(self):
        if self.create360_CHKBOX.isChecked():
            self.rigCtrl_LABEL.setEnabled(True)
            self.rigCtrl_LINEEDIT.setEnabled(True)
            self.rigCtrl_BTN.setEnabled(True)
            self.cacheMod.container.setEnabled(True)
            self.exportObjs_CHKBOX.setEnabled(True)
            self.filePerCharProp_CHKBOX.setEnabled(True)
            self.exoExportSurface_CHKBOX.setEnabled(True)
            self.output_LABEL.setEnabled(True)
            self.upAxis_LABEL.setEnabled(True)
            self.x_RBTN.setEnabled(True)
            self.y_RBTN.setEnabled(True)
            self.z_RBTN.setEnabled(True)
        else:
            self.rigCtrl_LABEL.setEnabled(False)
            self.rigCtrl_LINEEDIT.setEnabled(False)
            self.rigCtrl_BTN.setEnabled(False)
            self.cacheMod.container.setEnabled(False)
            self.exportObjs_CHKBOX.setEnabled(False)
            self.filePerCharProp_CHKBOX.setEnabled(False)
            self.exoExportSurface_CHKBOX.setEnabled(False)
            self.output_LABEL.setEnabled(False)
            self.upAxis_LABEL.setEnabled(False)
            self.x_RBTN.setEnabled(False)
            self.y_RBTN.setEnabled(False)
            self.z_RBTN.setEnabled(False)
    
    
    def eventFilter(self, widget, event):
        '''Captures keyboard method to prevent out ot focus window.'''
        if (event.type() == QtCore.QEvent.KeyPress):
            key = event.key()
            if key == QtCore.Qt.Key_Escape:
                self.close()
            else:
                if key == QtCore.Qt.Key_Return:
                    pass
                    #print "return"
                elif key == QtCore.Qt.Key_Enter:
                    pass
                    #print "enter"
                elif key == QtCore.Qt.Key_Shift:
                    pass
                    #print "shift"
                return True
        return QtWidgets.QWidget.eventFilter(self, widget, event)
    
    
    def alembicVersionDialog(self):
        alembicVerDialog = AlembicVersion_DialogGUI(self)
        alembicVerDialog.show()
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods for adding/removing geo tags
    ##--------------------------------------------------------------------------------------------     
    def charsPropsOptions (self,pos):
        '''
        Right-click popup menu for char/props list.
        '''
        menu = QtWidgets.QMenu(self.charsProps_TREEVIEW)
        addRootTagQ = menu.addAction("Add root")
        addGeoTagQ = menu.addAction("Add geo tag")
        removeItemQ = menu.addAction("Remove tag")
        selectgGeoQ = menu.addAction("Select Geo")
        selectMetaNodeQ = menu.addAction("Select Meta Node")
        menu.popup(self.charsProps_TREEVIEW.mapToGlobal(pos))
        
        addRootTagQ.triggered.connect(self.addRoot)
        addGeoTagQ.triggered.connect(self.tagGeo)
        removeItemQ.triggered.connect(self.removeTag)
        selectgGeoQ.triggered.connect(self.selectGeoForItem)
        selectMetaNodeQ.triggered.connect(self.selectMetaNode)
    
    
    def addRoot(self):
        metatag = cgxMeta.CGXCacheMetaTag(name='proxyChar')
        self.addTreeItem("Type Character/Prop name here", "root", "None", metatag)
    
    
    def tagGeo(self):
        allSel = mc.ls(sl= True, long=True)
        if len(allSel) <= 0:
            self.baseMod.statusBar_update("Nothing is selected. Select at least one node to be tagged.", status=True)
        else:
            existingMetaTags = r9Meta.getMetaNodes(['CGXCacheMetaTag'])
            usedGeom = []
            for tag in existingMetaTags:
                usedGeom += tag.getChildren(mAttrs='taggedNodes')
            for item in allSel:
                if mc.nodeType(item) in ['transform']:
                    if usedGeom.count(item) == 0:
                        selectedRootItem = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
                        if selectedRootItem != None:
                            itemShape = mc.listRelatives(item , shapes = True, noIntermediate = True, fullPath=True)[0]
                            thisNodeType = mc.nodeType(itemShape)
                            if thisNodeType in ["mesh", "nurbsSurface", "subdiv"]:
                                if len(selectedRootItem) >= 1:
                                    #Add item to tree view and metatag
                                    indexNode = self.charsProps_TREEVIEW.model().getNode(selectedRootItem[0])
                                    metatag = indexNode.data[4]
                                    metatag.connectChildren([item], 'taggedNodes')
                                    shortName = item.rsplit("|",1)[1]
                                    self.addTreeItem(shortName, "geo", item, metatag)
                                else:
                                    self.baseMod.statusBar_update("You must select one Character/Prop item to add this geo to.", status=True)
                            else:
                                self.baseMod.statusBar_update(item + " is not a valid node.", alert=True)
                        else:
                            self.baseMod.statusBar_update("Select a root node in the list.", alert=True)
                    else:
                        self.baseMod.statusBar_update(item + " has already been tagged.", alert=True)
                else:
                    self.baseMod.statusBar_update("Select only the transform nodes of all the geometries you want to tag.", alert=True)
    
    
    def removeTag(self):
        selected = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
        if len(selected) >= 1:
            skipItems = []
            for item in selected:
                if item.isValid():
                    tagType= self.charsProps_TREEVIEW.model().getNode(item).data
                    metaNode = tagType[4]
                    if tagType[1] == 'root':
                        if mc.referenceQuery(metaNode.mNode, isNodeReferenced= True):
                            skipItems.append(item)
                            self.baseMod.statusBar_update("The metatag node is referenced. You must remove referenced tags from the referenced file.", alert=True)
                        else:
                            for child in metaNode.getChildren():
                                metaNode.disconnectChild(child)
                            metaNode.delete()
                    elif tagType[1] == 'geo':
                        geoName = self.charsProps_TREEVIEW.model().getNode(item).data[2]
                        if mc.referenceQuery(geoName, isNodeReferenced= True):
                            self.baseMod.statusBar_update(geoName + " is referenced. It'll be removed from this tag just for this project.", alert=True)
                        metaNode.disconnectChild(geoName)
            #Remove from UI
            removeList = [x for x in selected if x not in skipItems]
            self.charsProps_TREEVIEW.model().removeRows(removeList)
    
    
    def selectGeoForItem(self):
        selected = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
        mc.select(clear=True)
        if len(selected) >= 1:
            for e in range(len(selected)):
                item = selected[e]
                if item.isValid():
                    tagType= self.charsProps_TREEVIEW.model().getNode(item).data
                    if tagType[1] == "root":
                        children = self.charsProps_TREEVIEW.model().getNode(item).children
                        for x in children:
                            geoName = x.data[2]
                            mc.select(geoName, add = True)
                    elif tagType[1] == "geo":
                        geoName = item.internalPointer().data[2]
                        mc.select(geoName, add = True)
    
    
    def selectMetaNode(self):
        selected = self.filterSelected(self.charsProps_TREEVIEW.selectedIndexes())
        metatags = []
        if len(selected) >= 1:
            for each in selected:
                treeNode = self.charsProps_TREEVIEW.model().getNode(each)
                metatags.append(treeNode.data[4].mNode)
        mc.select(metatags, replace=True)
    
    
    def filterSelected(self, _indexes):
        '''
        Given a list of indexes, return only one index per row.
        '''
        if len(_indexes) > 0:
            #Group by parent first
            filterDict = {}
            for each in _indexes:
                if each.parent() not in filterDict.keys():
                    filterDict[each.parent()] = [each]
                else:
                    filterDict[each.parent()].append(each)
            #Delete redundancies
            filteredList = []
            for key in filterDict.keys():
                usedRows = []
                for item in filterDict[key]:
                    if item.row() not in usedRows:
                        usedRows.append(item.row())
                        filteredList.append(item)
            
            return filteredList
        else:
            return _indexes
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods for adding/removing ctrl tags
    ##--------------------------------------------------------------------------------------------     
    def ctrlsOptions (self,pos):
        '''
        Right-click popup menu for ctrls table.
        '''
        menu = QtWidgets.QMenu(self.ctrls_TABLEVIEW)
        addControlsQ = menu.addAction("Add controls")
        removeControlsQ = menu.addAction("Remove controls")
        selectControlsQ = menu.addAction("Select controls")
        selectMetaNodesQ = menu.addAction("Select Meta Nodes")
        menu.popup(self.ctrls_TABLEVIEW.mapToGlobal(pos))
        
        addControlsQ.triggered.connect(self.addControlsOpt)
        removeControlsQ.triggered.connect(self.removeControlsOpt)
        selectControlsQ.triggered.connect(self.selectControlsOpt)
        selectMetaNodesQ.triggered.connect(self.selectMetaControlsOpt)
    
    
    def addControlsOpt(self):
        allCurveShapes= mc.ls(type='nurbsCurve', long=True, sl=True)
        allCurveTransforms = [mc.listRelatives(x, parent=True, fullPath=True)[0] for x in allCurveShapes]
        setTransforms = set(allCurveTransforms)
        controlsList = list(setTransforms)
        for ctrl in controlsList:
            messageConnection = mc.listConnections(ctrl, source=True, destination=False, type='network')
            if messageConnection == None:
                shortName = ctrl
                if "|" in shortName:
                    shortName = ctrl.rsplit("|",1)[1]
                metaCtrl = cgxMeta.CGXCacheMetaCtrl(name=shortName + '_META')
                metaCtrl.connectChildren([ctrl], 'taggedNode')
                self.ctrls_TABLEVIEW.model().insertRows(self.ctrls_TABLEVIEW.model().rowCount(), 1, [shortName, metaCtrl])
            else:
                if mc.nodeType(messageConnection[0]) != 'network':
                    shortName = ctrl
                if "|" in shortName:
                    shortName = ctrl.rsplit("|",1)[1]
                    metaCtrl = cgxMeta.CGXCacheMetaCtrl(name=shortName + '_META')
                    metaCtrl.connectChildren([ctrl], 'taggedNode')
                    self.ctrls_TABLEVIEW.model().insertRows(self.ctrls_TABLEVIEW.model().rowCount(), 1, [shortName, metaCtrl])
                else:
                    self.baseMod.statusBar_update(ctrl + " already tagged.", alert=True)
    
    
    def removeControlsOpt(self):
        selected = self.ctrls_TABLEVIEW.selectedIndexes()
        selRowsDirty = []
        for item in selected:
            thisRow = item.row()
            selRowsDirty.append(thisRow)
        selSet = set(selRowsDirty)
        selRows = list(selSet)
        
        for each in selRows:
            metaNode = self.ctrls_TABLEVIEW.model().dataList[each][1]
            metaNode.delete()
        self.ctrls_TABLEVIEW.model().removeRows(selRows, len(selRows))
    
    
    def selectControlsOpt(self):
        selected = self.ctrls_TABLEVIEW.selectedIndexes()
        selRowsDirty = []
        for item in selected:
            thisRow = item.row()
            selRowsDirty.append(thisRow)
        selSet = set(selRowsDirty)
        selRows = list(selSet)
        transform = self.ctrls_TABLEVIEW.model().dataList[selRows[0]][0]
        mc.select(transform, replace=True)
    
    
    def selectMetaControlsOpt(self):
        selected = self.ctrls_TABLEVIEW.selectedIndexes()
        selRowsDirty = []
        for item in selected:
            thisRow = item.row()
            selRowsDirty.append(thisRow)
        selSet = set(selRowsDirty)
        selRows = list(selSet)
        metaNode = self.ctrls_TABLEVIEW.model().dataList[selRows[0]][1]
        mc.select(metaNode.mNode, replace=True)


##--------------------------------------------------------------------------------------------
##Class: Tree Data Model reimplementation for Chars/Props tree
##--------------------------------------------------------------------------------------------
class CharPropsDataTreeModel(dvm.DataTreeModel):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, root, headers, parent):
        '''
        DataTreeModel for Model/View programming.
        :param root: root TreeNode for this model.
        :type root: TreeNode
        :param headers: list of headers to be displayed in the table. Must be same len as data passed to rootnode
        :type headers: list
        :param parent: QWidget that uses this model. Default=None
        :type parent: QWidget
        '''
        super(CharPropsDataTreeModel, self).__init__(root, headers, parent)
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def flags(self, index):
        '''
        Set flags for each item.  Re-implement if different flags are needed and use conditional chains if per-item flags are needed.
        :param index: Item to be filtered
        :type index: QModelIndex
        :return: Flags to be used
        :rtype: QtCore.Qt.Flags
        '''
        text = index.model().data(index, QtCore.Qt.DisplayRole)
        if text in ["None","geo","root"] and index.column() in [1,2,3]:
            return QtCore.Qt.NoItemFlags
        else:
            return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
    
    
    def setData(self, index, value, role=QtCore.Qt.EditRole):
        '''
        Set data for specified index and display it.
        :param index: Index to be edited
        :type index: QModelIndex
        :param role: Edit role as default. Reimplement with elif chain for another role.
        :type role: QtCore.Qt.Role
        :return: Item data to be shown in the GUI. Might be None.
        :rtype: object
        '''
        if role != QtCore.Qt.EditRole:
            return False
        else:
            node = self.getNode(index)
            result = node.setData(index.column(),value)
            if result:
                metaNode = node.data[4]
                if index.column() == 3:
                    metaNode.instanceIdentifier = value
                if index.column() == 0:
                    metaNode.assetName = value
                    metaNode.subassetName = ""
                    if node.dataValue(3) in ["None","",None] and node.dataValue(1) == "root":
                        node.setData(3,value)
                        metaNode.instanceIdentifier = value
                self.dataChanged.emit(index, index)
    
            return result


##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent):
        '''
        Creates Help Dialog for current tool.
        :param parent: Main tools window.
        :type parent: QMainWindow
        '''
        super(Help_DialogGUI,self).__init__(parent)
        self.setupUi(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent):
        '''
        Creates About Dialog for current tool.
        :param parent: Main tools window.
        :type parent: QMainWindow
        '''
        super(About_DialogGUI,self).__init__(parent)
        self.setupUi(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent):
        '''
        Creates Config Dialog for current tool.
        :param parent: Main tools window.
        :type parent: QMainWindow
        '''
        super(Config_DialogGUI,self).__init__(parent)
        self.setupUi(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create alembic dialog
##--------------------------------------------------------------------------------------------
class AlembicVersion_DialogGUI(alembicVersionform, alembicVersionbase):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, parent):
        '''
        Creates Config Dialog for current tool.
        :param parent: Main tools window.
        :type parent: QMainWindow
        '''
        super(AlembicVersion_DialogGUI,self).__init__(parent)
        self.setupUi(self)
        
        self.configFile = JSONManager(alembicVersion_json, self)
        
        self.initUI()
        self.setConnections()
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def setConnections(self):
        #SIGNALS
        self.cancel_BTN.clicked.connect(self.cancelAll)
        self.apply_BTN.clicked.connect(self.applyConfig)
        
    
    def initUI(self):
        """Read JSON config file"""
        
        version = self.configFile.getValueByProperty('alembicVersion', False)
        if version == 'hdf5':
            self.hdf5_RADIOBTN.setChecked(True)
            self.ogawa_RADIOBTN.setChecked(False)
        elif version == 'ogawa':
            self.hdf5_RADIOBTN.setChecked(False)
            self.ogawa_RADIOBTN.setChecked(True)
        
    
    def applyConfig(self):
        """Apply changes to JSON file"""
        
        jsonObj = self.configFile.jsonObj
        version = 'hdf5'
        if self.ogawa_RADIOBTN.isChecked():
            version = 'ogawa'
        jsonObj['alembicVersion'] = version
        self.configFile.writeJson(jsonObj, self.configFile.fileName)
        
        self.done(1)
    
    
    def cancelAll(self):
        """Method to close the UI"""
        
        self.done(0)

            
##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    if __DEVMODE:
        x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/pipeline/CacheTagger_dev.py','r')
        exec x.read()
        x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
    main()