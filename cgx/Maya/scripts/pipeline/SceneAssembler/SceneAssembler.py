# -*- coding: utf-8 -*-
'''
Scene Assembler tool.

Created on May 23, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
from cgx.gui.Qt import QtWidgets, QtCore
from cgx.gui.Qt import __binding__
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
from cgx.core.JSONManager import JSONManager
import cgx.gui.components.Pipeline_Components as pipeComponents
import cgx.gui.DataViewModels as dvm
import maya.cmds as mc


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/pipeline/SceneAssembler'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"
addCustomAsset_ui = appRootFolder + "/ui/" + "addCustomAsset.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)
addCustomAssetform, addCustomAssetbase = uiParser.loadUiType(addCustomAsset_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.initComponents()
			self.initUI()
			self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
        Connect signals.
        '''
		#BUTTONS
		self.browseCamPath_BTN.clicked.connect(self.browseCamPath)
		self.assembleScene_BTN.clicked.connect(self.assembleScene)
		self.referenceCam_opt.triggered.connect(self.referenceCamera)
		#REFRESH
		self.projSeqShotMod.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.refreshModels)
		self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.currentIndexChanged.connect(self.refreshModels)
		self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentIndexChanged.connect(self.refreshModels)

		
		#CONTEXT MENUS
		self.assets_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.assets_TABLEVIEW.customContextMenuRequested.connect(self.assetsOptions)
		
	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Scene Assembler"
		self.__toolVersion = __version__
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.baseMod.MainMenu_Component.edit_config.setVisible(False)
		self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo)
		self.projSeqShotMod = self.Pipeline_GUIModulesFactory.createModule('ProjSeqShot')
		self.projSeqShotMod.pos = [10,38]
		#Add reference camera option
		self.referenceCam_opt = QtWidgets.QAction(self.baseMod.MainMenu_Component.parent)
		self.referenceCam_opt.setObjectName("reference_camera")
		self.baseMod.MainMenu_Component.menuEdit.insertAction(self.baseMod.MainMenu_Component.menuEdit.actions()[0], self.referenceCam_opt)
		className= str(self.__class__.__name__)
		if __binding__ in ('PySide', 'PyQt4'):
			self.referenceCam_opt.setText(QtWidgets.QApplication.translate(className, "Reference Camera Only", None, QtWidgets.QApplication.UnicodeUTF8))
		elif __binding__ in ('PySide2', 'PyQt5'):
			self.referenceCam_opt.setText(QtWidgets.QApplication.translate(className, "Reference Camera Only", None))
			
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
		self.appTools.setCurrentProject(self.projSeqShotMod.Project_Component)
		self.appTools.setCurrentSequence(self.projSeqShotMod.Sequence_Component)
		self.appTools.setCurrentShot(self.projSeqShotMod.Shot_Component)
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		self.populateAssets()
		self.populateCamera()
	
	
	def populateAssets(self):
		self.headers = ['ID','assetType','assetName','subassetName','instanceIdentifier','publishedFile','cacheFolder','isCustom']
		
		seq = str(self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.currentText())
		shot = str(self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentText())
		jsonPath = self.buildProjSeqShotPath(seq, shot) + '/metadata.json'
		
		if os.path.exists(jsonPath):
			shotMetadata = JSONManager(jsonPath, self)
			dataList = []
			for key, value in shotMetadata.jsonObj.iteritems():
				rowData = []
				publishedFiles = self.populatePublished(shotMetadata.jsonObj[key])
				SHDFileName = self.guessFileName(shotMetadata.jsonObj[key], publishedFiles)
				for column in self.headers:
					if column == "ID":
						rowData.append(key)
					elif column == "assetType":
						rowData.append(value[column])
					elif column == "assetName":
						rowData.append(value[column])
					elif column == "subassetName":
						rowData.append(value[column])
					elif column == "instanceIdentifier":
						rowData.append(value[column])
					elif column == "publishedFile":
						if len(publishedFiles) >= 1:
							rowData.append(SHDFileName)
						else:
							rowData.append(None)
					elif column == "cacheFolder":
						rowData.append(value[column])
					elif column == 'isCustom':
						rowData.append(False)
				dataList.append(rowData)
			
			self.assetsModel = AssetsDataTableModel(dataList, self.headers, self)
			proxyModel =  AssetsProxyModel(self)
			proxyModel.setSourceModel(self.assetsModel)
			self.assets_TABLEVIEW.setModel(proxyModel)
			self.assets_TABLEVIEW.sortByColumn(0,QtCore.Qt.AscendingOrder)
			self.assets_TABLEVIEW.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
			self.assets_TABLEVIEW.setVerticalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
			self.assets_TABLEVIEW.hideColumn(7)
			for col in range(self.assetsModel.columnCount()):
				self.assets_TABLEVIEW.resizeColumnToContents(col)
			delegate = AssetsDelegate(self)
			self.assets_TABLEVIEW.setItemDelegate(delegate)
		else:
			self.baseMod.statusBar_update("There is no metadata for {} {}".format(seq, shot), alert=True)
	
	
	def guessFileName(self, _rowDict, _publishedFiles):
		SHDAssetName = 'SHD_{}.ma'.format(_rowDict['assetName'])
		SHDSubAssetName = 'SHD_{}_{}.ma'.format(_rowDict['assetName'],_rowDict['subassetName'])
		SHDAssetNameInstance = 'SHD_{}_{}.ma'.format(_rowDict['assetName'],_rowDict['instanceIdentifier'])
		SHDSubassetNameInstance = 'SHD_{}_{}_{}.ma'.format(_rowDict['assetName'], _rowDict['subassetName'], _rowDict['instanceIdentifier'])
		
		if _rowDict['subassetName'] not in ["", None]:
			if SHDSubassetNameInstance in _publishedFiles:
				return SHDSubassetNameInstance
			elif SHDSubAssetName in _publishedFiles:
				return SHDAssetNameInstance
		elif SHDAssetNameInstance in _publishedFiles:
			return SHDAssetNameInstance
		elif SHDAssetName in _publishedFiles:
			return SHDAssetName
		else:
			return None
	
	
	def populateCamera(self):
		seq = str(self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.currentText())
		shot = str(self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentText())
		camFolder = self.buildCameraPath(seq, shot)
		filesAndDirs = os.listdir(camFolder)
		camFile = ""
		for each in filesAndDirs:
			if os.path.isfile(os.path.join(camFolder, each)):
				if each[-3:] == ".ma":
					camFile = each
					break
		cameraPath = camFolder + "/" + camFile
		if os.path.exists(cameraPath) and camFile not in ["",None]:
			self.camera_LINEEDIT.setText(cameraPath)
		else:
			self.baseMod.statusBar_update("There is no camera for {} {}".format(seq, shot), alert=True)
			
	
	def browseCamPath(self):
		'''
		Create a Browsing Dialog to get file or directory paths.
		'''
		seq = str(self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.currentText())
		shot = str(self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentText())
		
		browseDialog = QtWidgets.QFileDialog(self)
		fileMode = browseDialog.FileMode.ExistingFile
		directory = self.buildCameraPath(seq, shot)
		browseDialog.setFileMode(fileMode)
		filters = ["Maya Projects (*.ma *.mb)"]
		browseDialog.setNameFilters(filters)
		if os.path.exists(directory):
			browseDialog.setDirectory(directory)
		dialogReturn = browseDialog.exec_()
		if dialogReturn == 1:
			selectedFile = browseDialog.selectedFiles()[0]
			self.camera_LINEEDIT.setText(selectedFile)
			
	
	def assembleScene(self):
		mc.file(prompt=False)
		selRowsFinal = range(0,self.assetsModel.rowCount())
		if self.assembleSelectedOnly_CHKBOX.isChecked():
			selRowsProxy = self.assets_TABLEVIEW.selectedIndexes()
			proxyModel = self.assets_TABLEVIEW.model()
			selRows = []
			for each in selRowsProxy:
				selRows.append(proxyModel.mapToSource(each))
			selRowsDirty = []
			for item in selRows:
				thisRow = item.row()
				selRowsDirty.append(thisRow)
			selSet = set(selRowsDirty)
			selRowsFinal = list(selSet)
		
		#Reference camera and set time range
		self.referenceCamera()
		
		#Reference assets
		dataList = self.assetsModel.dataList
		for each in selRowsFinal:
			rowDict = {'assetType':dataList[each][self.headers.index('assetType')],
					'assetName':dataList[each][self.headers.index('assetName')],
					'subassetName':dataList[each][self.headers.index('subassetName')],
					}
			filePath = ""
			fileName = ""
			if dataList[each][self.headers.index('isCustom')]:
				if rowDict['assetType'] in ['',None] or rowDict['assetName'] in ['',None] or rowDict['subassetName'] in ['',None]:
					filePath = dataList[each][self.headers.index('publishedFile')]
					fileName = filePath
				else:
					fileName = dataList[each][self.headers.index('publishedFile')]
					filePath = self.buildAssetPath(rowDict) + '/' + fileName
			else:
				fileName = dataList[each][self.headers.index('publishedFile')]
				filePath = self.buildAssetPath(rowDict) + '/' + fileName
			
			if os.path.exists(filePath):
				refFile = mc.file(filePath, reference=True , namespace = fileName[:-3])
				allNodes = mc.referenceQuery(refFile, nodes=True, dagPath=True)
				cacheNodes = []
				for node in allNodes:
					if mc.nodeType(node) == 'ExocortexAlembicFile':
						cacheNodes.append(node)
				if len(cacheNodes) >=1 :
					for thisNode in cacheNodes:
						exoFileName = mc.getAttr(thisNode + ".fileName")
						lastSlashLen = exoFileName.rfind("\\")
						if lastSlashLen == -1:
							lastSlashLen = exoFileName.rfind("/")
						newPath = dataList[each][self.headers.index('cacheFolder')] + "\\" + exoFileName[lastSlashLen+1:]
						mc.setAttr(thisNode + ".fileName", newPath, type= "string")
				self.baseMod.statusBar_update("Done assembling file.".format(), success=True)
			else:
				self.baseMod.statusBar_update("File for ID:{} couldn't be found {}".format(dataList[each][self.headers.index('ID')], filePath), alert=True)
		
		mc.file(prompt=True)
			

	def populatePublished(self, _rowDict):
		trgPath = self.buildAssetPath(_rowDict)
		filesList = []
		if trgPath != None:
			if os.path.exists(trgPath):
				filesList = [ f for f in os.listdir(trgPath) if os.path.isfile(os.path.join(trgPath,f)) ]
		
		if len(filesList) >= 1:
			for each in sorted(filesList, reverse=True):
				if each.split("_")[0] != 'SHD':
					indexNum = filesList.index(each)
					del(filesList[indexNum])
		
		return filesList
	
	
	def referenceCamera(self):
		camPath = self.camera_LINEEDIT.text()
		if os.path.exists(camPath):
			lastSlashLen = camPath.rfind("\\")
			if lastSlashLen == -1:
				lastSlashLen = camPath.rfind("/")
			camFile = camPath[lastSlashLen+1:]
			refFile = mc.file(camPath, reference=True , namespace = camFile[:-3])
			transform = mc.referenceQuery(refFile, nodes=True)[0]
			transformParts = transform.split("_")
			startFrame = 0
			endFrame = 100
			i = 0
			for each in transformParts:
				if each[0] == "f":
					startFrame = int(each[1:])
					endFrame = int(transformParts[i+1])
					break
				i += 1
			mc.playbackOptions(edit= True, minTime = startFrame, animationStartTime= startFrame)
			mc.playbackOptions(edit= True, maxTime = endFrame, animationEndTime= endFrame)
	
	
	def buildAssetPath(self, _rowDict):
		assetType = _rowDict['assetType']
		assetName = _rowDict['assetName']
		subassetName = _rowDict['subassetName']
		
		propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
		if subassetName not in [None,""]:
			propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_subasset",
                        "placeholder":False}
		publishedNodes = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
		
		assetTypeDict={"name":assetType,
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
		assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		if assetType == 'PROPS':
			assetDict= {"name":"$PropName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'VEHICLES':
			assetDict= {"name":"$VehicleName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		
		subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
		for node in publishedNodes:
			pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict})
			if subassetName not in [None,""]:
				pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict,subassetName:subAssetDict})
			pathList.append(node.name)
			#Build path string
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
			
			if "$" not in pathStr:
				return pathStr
	
	
	def buildCameraPath(self, _sequence, _shot):
		'''
		Given a sequence name and a shot name, rebuild camera export path.
		:param _sequence: Sequence name retrieved from the tool gui.
		:type _sequence: string
		:param _shot: Shot name retrieved from the tool gui.
		:type _shot: string
		:return: Absolute path to camera final location. None if path was impossible to rebuild.
		:rtype: string
		'''
		propertiesDict = {"abstractFolderType":"leaffolder",
						  "concreteFolderType":"camera_leaffolder",
						  "folderFunction":"published_content",
						  "folderConcreteFunction":"published_camera",
						  "placeholder":False}
		camNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)[0]
		if camNode != None:
			seqDict={"abstractFolderType":"subfolder",
					"concreteFolderType":"sequence_subfolder",
					"folderFunction":"grouping",
					"folderConcreteFunction":"",
					"placeholder":True}
			shotDict= {"abstractFolderType":"subfolder",
					"concreteFolderType":"shot_subfolder",
					"folderFunction":"grouping",
					"folderConcreteFunction":"",
					"placeholder":True}
			pathList = self.pInfo.rebuildPath(camNode, {_sequence:seqDict,_shot:shotDict})
			pathList.append(camNode.name)
			#Build path string
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
			return pathStr
		else:
			return None
	
	
	def buildProjSeqShotPath(self, _seq, _shot):
		propertiesDict = {"abstractFolderType":"leaffolder",
		                  "concreteFolderType":"published_leaffolder",
		                  "folderFunction":"published_content",
		                  "folderConcreteFunction":"published_cache",
		                  "placeholder":False}
		cacheNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)[0]
		if cacheNode != None:
			seqDict={"name":"$SEQ_##",
					"abstractFolderType":"subfolder",
					"concreteFolderType":"sequence_subfolder",
					"folderFunction":"grouping",
					"folderConcreteFunction":"",
					"placeholder":True}
			shotDict={"name":"$s_###",
					"abstractFolderType":"subfolder",
					"concreteFolderType":"shot_subfolder",
					"folderFunction":"grouping",
					"folderConcreteFunction":"",
					"placeholder":True}
			pathList = self.pInfo.rebuildPath(cacheNode, {_seq:seqDict, _shot:shotDict})
			pathList.append(cacheNode.name)
			
			#Build path string
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
			
			return pathStr
		else:
			return None
	
	
	def refreshModels(self):
		self.populateAssets()
		self.populateCamera()
		
	
	##--------------------------------------------------------------------------------------------
	##Methods for assets table view
	##--------------------------------------------------------------------------------------------
	def assetsOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.assets_TABLEVIEW)
		customAssetQ = menu.addAction("Add custom asset")
		removeAssetsQ = menu.addAction("Remove assets")
		menu.popup(self.assets_TABLEVIEW.mapToGlobal(pos))
		
		customAssetQ.triggered.connect(self.addCustomAssetOpt)
		removeAssetsQ.triggered.connect(self.removeAssetsOpt)
	
	
	def addCustomAssetOpt(self):
		addCustomAssetDialog = AddCustomAsset_DialogGUI(self)
		addCustomAssetDialog.exec_()
	
	
	def removeAssetsOpt(self):
		selRowsProxy = self.assets_TABLEVIEW.selectedIndexes()
		proxyModel = self.assets_TABLEVIEW.model()
		selRows = []
		for each in selRowsProxy:
			selRows.append(proxyModel.mapToSource(each))
		
		selRowsDirty = []
		for item in selRows:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRowsFinal = list(selSet)
		self.assets_TABLEVIEW.model().removeRows(selRowsFinal, len(selRowsFinal))


##--------------------------------------------------------------------------------------------
##Create dialog
##--------------------------------------------------------------------------------------------
class AddCustomAsset_DialogGUI(addCustomAssetform, addCustomAssetbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		super(AddCustomAsset_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.parent = parent
		self.createComponents()
		self.initComponents()
		self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------	
	def setConnections(self):
		#BUTTONS
		self.add_BTN.clicked.connect(self.addAsset)
		self.cancel_BTN.clicked.connect(self.cancelAddAsset)
		self.browseCachePath_BTN.clicked.connect(self.browseCachePath)
		#CHKBOX
		self.customPathComp.customPath_CHKBOX.clicked.connect(self.refreshUseCustomPath)
		#COMBOBOX
		self.assetsMod.AssetTypes_Component.assetType_COMBOBOX.currentIndexChanged.connect(self.populatePublished)
		self.assetsMod.Assets_Component.assetName_COMBOBOX.currentIndexChanged.connect(self.populatePublished)
		self.assetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentIndexChanged.connect(self.populatePublished)
		self.assetsMod.FileTypes_Component.fileType_COMBOBOX.currentIndexChanged.connect(self.populatePublished)
		
		
	def createComponents(self):
		self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.parent.sInfo, self.parent.pInfo)
		self.assetsMod = self.Pipeline_GUIModulesFactory.createModule('Assets')
		self.assetsMod.pos = [10,5]
		self.customPathComp = pipeComponents.CustomPath_Component(self,self, self.parent.sInfo)
		self.customPathComp.pos = [10,210]
		
	
	def initComponents(self):
		self.parent.appTools.setCurrentAssetType(self.assetsMod.AssetTypes_Component)
		self.parent.appTools.setCurrentAsset(self.assetsMod.Assets_Component)
		self.parent.appTools.setCurrentSubAsset(self.assetsMod.SubAssets_Component)
		self.assetsMod.FileTypes_Component.setToItem('shading:SHD')
		self.customPathComp.fileMode = QtWidgets.QFileDialog.FileMode.ExistingFile
		self.populatePublished()
			
	
	def populatePublished(self):
		trgPath = self.buildAssetPath()
		filesList = []
		if trgPath != None:
			if os.path.exists(trgPath):
				filesList = [ f for f in os.listdir(trgPath) if os.path.isfile(os.path.join(trgPath,f)) ]
		if len(filesList) >= 1:
			for each in sorted(filesList, reverse=True):
				fileType = self.assetsMod.FileTypes_Component.fileType_COMBOBOX.currentText()
				print fileType
				if each.split("_")[0] != fileType.split(':')[1]:
					indexNum = filesList.index(each)
					del(filesList[indexNum])
		
		model = dvm.ObjectsListModel(filesList,self)
		self.publishedFiles_COMBOBOX.setModel(model)
	
	
	def buildAssetPath(self):
		assetType = self.assetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText()
		assetName = self.assetsMod.Assets_Component.assetName_COMBOBOX.currentText()
		subassetName = self.assetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText()
		
		propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
		if subassetName not in [None,""]:
			propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_subasset",
                        "placeholder":False}
		publishedNodes = self.parent.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
		
		assetTypeDict={"name":assetType,
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
		assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		if assetType == 'PROPS':
			assetDict= {"name":"$PropName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'VEHICLES':
			assetDict= {"name":"$VehicleName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'SETS':
			assetDict= {"name":"$SetName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		
		subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
		for node in publishedNodes:
			pathList = self.parent.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict})
			if subassetName not in [None,""]:
				pathList = self.parent.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict,subassetName:subAssetDict})
			pathList.append(node.name)
			#Build path string
			server = self.parent.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			pathStr = server + "/" + self.parent.pInfo.project + "/" + "/".join(pathList[1:])
			
			if "$" not in pathStr:
				return pathStr
	
	
	def browseCachePath(self):
		'''
        Create a Browsing Dialog to get file or directory paths.
        '''
		browseDialog = QtWidgets.QFileDialog(self)
		fileMode = browseDialog.FileMode.Directory
		directory = self.parent.sInfo.getProjectsFrom()
		browseDialog.setFileMode(fileMode)
		if os.path.exists(directory):
			browseDialog.setDirectory(directory)
		dialogReturn = browseDialog.exec_()
		if dialogReturn == 1:
			selectedFile = browseDialog.selectedFiles()[0]
			self.cachePath_LINEEDIT.setText(selectedFile)
			
	
	def refreshUseCustomPath(self):
		if self.customPathComp.customPath_CHKBOX.isChecked():
			self.assetsMod.container.setEnabled(False)
			self.publishedFiles_COMBOBOX.setEnabled(False)
		else:
			self.assetsMod.container.setEnabled(True)
			self.publishedFiles_COMBOBOX.setEnabled(True)
	
	
	def addAsset(self):
		
		proxyModel = self.parent.assets_TABLEVIEW.model()
		rowData = ['ID','assetType','assetName','subassetName','instanceIdentifier','publishedFile','cacheFolder']
		if self.customPathComp.customPath_CHKBOX.isChecked():
			rowData = [proxyModel.rowCount(),
					'',
					'',
					'',
					'',
					self.customPathComp.customPath_LINEEDIT.text(),
					self.cachePath_LINEEDIT.text(),
					True]
		else:
			rowData = [proxyModel.rowCount(),
					self.assetsMod.AssetTypes_Component.assetType_COMBOBOX.currentText(),
					self.assetsMod.Assets_Component.assetName_COMBOBOX.currentText(),
					self.assetsMod.SubAssets_Component.subAssetName_COMBOBOX.currentText(),
					'',
					self.publishedFiles_COMBOBOX.currentText(),
					self.cachePath_LINEEDIT.text(),
					True]
		proxyModel.insertRows(proxyModel.rowCount(),1,rowData)
		
		self.done(1)
	
	
	def cancelAddAsset(self):
		self.done(0)


##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for Assets table
##--------------------------------------------------------------------------------------------
class AssetsDataTableModel(dvm.DataTableModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, dataList, headers, parent):
		super(AssetsDataTableModel, self).__init__(dataList, headers, parent)


	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def flags(self, index):
		"""Set flags of each cell"""
		if not index.isValid():
			return QtCore.Qt.NoItemFlags
		elif index.column() in [5] and index.model().dataList[index.row()][7] != True:
			return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
		else:
			return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable


##--------------------------------------------------------------------------------------------
##Class: Table Proxy Model reimplementation for Assets table
##--------------------------------------------------------------------------------------------
class AssetsProxyModel(QtCore.QSortFilterProxyModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		super(AssetsProxyModel, self).__init__(parent)


	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def insertRows(self,position, count, data, parent = QtCore.QModelIndex()):
		self.sourceModel().insertRows(position, count, data,parent)
	
	
	def removeRows(self, indexList, count, parent = QtCore.QModelIndex()):
		self.sourceModel().removeRows(indexList, count, parent)
		

##--------------------------------------------------------------------------------------------
##Class: Assets Delegate
##--------------------------------------------------------------------------------------------
class AssetsDelegate(QtWidgets.QItemDelegate):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent=None):
		super(AssetsDelegate, self).__init__(parent)
		self.parent = parent
		
			
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def createEditor(self, parent, option, index):
		sourceIndex = index.model().mapToSource(index)
		modelHeaders = sourceIndex.model().headers
		if modelHeaders[index.column()] == "publishedFile":
			combobox = QtWidgets.QComboBox(parent)
			dataList = self.populatePublished(index)
			combobox.addItems(sorted(dataList))
			combobox.setEditable(False)
			return combobox
		else:
			return QtWidgets.QItemDelegate.createEditor(self, parent, option,index)
		
		
	def setEditorData(self, editor, index):
		sourceIndex = index.model().mapToSource(index)
		modelHeaders = sourceIndex.model().headers
		text = index.model().data(index, QtCore.Qt.DisplayRole)
		if modelHeaders[index.column()] == "publishedFile":
			i = editor.findText(text)
			if i == -1:
				i = 0
			editor.setCurrentIndex(i)
		else:
			QtWidgets.QItemDelegate.setEditorData(self, editor, index)
	
	
	def setModelData(self, editor, model, index):
		sourceIndex = index.model().mapToSource(index)
		modelHeaders = sourceIndex.model().headers
		if modelHeaders[index.column()] == "publishedFile":
			model.setData(index, editor.currentText())
		else:
			QtWidgets.QItemDelegate.setModelData(self, editor, model, index)
	
	
	def populatePublished(self, index):
		trgPath = self.buildAssetPath(index)
		filesList = []
		if trgPath != None:
			if os.path.exists(trgPath):
				filesList = [ f for f in os.listdir(trgPath) if os.path.isfile(os.path.join(trgPath,f)) ]
		
		if len(filesList) >= 1:
			for each in sorted(filesList, reverse=True):
				if each.split("_")[0] != 'SHD':
					indexNum = filesList.index(each)
					del(filesList[indexNum])
		
		return filesList
	
	
	def buildAssetPath(self, index):
		sourceIndex = index.model().mapToSource(index)
		modelHeaders = sourceIndex.model().headers
		assetType = sourceIndex.model().dataList[sourceIndex.row()][modelHeaders.index('assetType')]
		assetName = sourceIndex.model().dataList[sourceIndex.row()][modelHeaders.index('assetName')]
		subassetName = sourceIndex.model().dataList[sourceIndex.row()][modelHeaders.index('subassetName')]
		
		propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
		if subassetName not in [None,""]:
			propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_subasset",
                        "placeholder":False}
		publishedNodes = self.parent.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
		
		assetTypeDict={"name":assetType,
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
		assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		if assetType == 'PROPS':
			assetDict= {"name":"$PropName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		elif assetType == 'VEHICLES':
			assetDict= {"name":"$VehicleName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
		
		subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
		for node in publishedNodes:
			pathList = self.parent.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict})
			if subassetName not in [None,""]:
				pathList = self.parent.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict,subassetName:subAssetDict})
			pathList.append(node.name)
			#Build path string
			server = self.parent.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			pathStr = server + "/" + self.parent.pInfo.project + "/" + "/".join(pathList[1:])
			
			if "$" not in pathStr:
				return pathStr


##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/pipeline/SceneAssembler_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()