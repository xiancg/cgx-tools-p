# -*- coding: utf-8 -*-
'''
Cache Reseter tool.

Created on Feb 06, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os
import maya.cmds as mc
import maya.mel as mel
from cgx.gui.Qt import QtWidgets, QtCore
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
import cgx.gui.DataViewModels as dvm


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/pipeline/CacheReseter'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.initComponents()
			self.initUI()
			self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
        Connect signals.
        '''
		#SIGNALS
		self.reImport_CHKBOX.stateChanged.connect(self.refreshReImport)
		self.resetCacheNodes_BTN.clicked.connect(self.preResetCaches)
		self.nodesPath_CHKBOX.stateChanged.connect(self.refreshChangeBaseDir)
		self.customPathComp.customPath_CHKBOX.stateChanged.connect(self.refreshCustomPath)
		self.cacheComp.fileType_BTNGRP.buttonClicked.connect(self.refreshFileType)
		self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentIndexChanged.connect(self.populateSubfolders)
		#STATUS
		self.cacheComp.alembicExo_RBTN.setChecked(True)
		self.nodesPath_CHKBOX.setChecked(True)
		self.customPathComp.customPath_LINEEDIT.setEnabled(False)
		self.customPathComp.customPath_BTN.setEnabled(False)
		self.minmaxComp.container.setEnabled(False)
		#CONTEXT MENUS
		self.cacheNodes_LISTVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.cacheNodes_LISTVIEW.customContextMenuRequested.connect(self.cacheNodesOptions)
	
	
	def initTool(self):
		'''
        Initializes tool settings.
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Cache Reseter"
		self.__toolVersion = __version__
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.baseMod.MainMenu_Component.edit_config.setVisible(False)
		self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo, tRange=self.appTools.tRange)
		self.projSeqShotMod = self.Pipeline_GUIModulesFactory.createModule('ProjSeqShot')
		self.projSeqShotMod.pos = [0,251]
		self.customPathComp = pipeComponents.CustomPath_Component(self,self.centralWidget(), self.sInfo)
		self.customPathComp.pos = [0,330]
		self.customPathComp.fileMode = self.customPathComp.browseDialog.FileMode.Directory
		self.cacheComp = pipeComponents.CacheFileTypes_Component(self,self.centralWidget())
		self.cacheComp.pos = [0,0]
		self.fpsComp = pipeComponents.FPS_Component(self,self.centralWidget(), self.pInfo)
		self.fpsComp.pos = [262,155]
		self.minmaxComp = pipeComponents.MinMax_Component(self,self.centralWidget())
		self.minmaxComp.pos = [0,140]
	
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
		self.appTools.app_initUI.setCurrentProject(self.projSeqShotMod.Project_Component)
		self.appTools.app_initUI.setCurrentSequence(self.projSeqShotMod.Sequence_Component)
		self.appTools.app_initUI.setCurrentShot(self.projSeqShotMod.Shot_Component)
		self.fpsComp.initComponent(self.pInfo.maya_getFPS())
		self.appTools.app_initUI.checkFPS(self.fpsComp)
		self.appTools.tRange.setFramesToTimeSlider()
		
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		self.populateList()
		self.populateSubfolders()
	
	
	def populateSubfolders(self):
		thisPath = self.buildProjSeqShotPath(False)
		dirList = []
		if os.path.exists(thisPath):
			dirList = [x for x in os.listdir(thisPath) if os.path.isdir(os.path.join(thisPath,x))]
		dirList.insert(0,'')
		subfoldersModel = dvm.ObjectsListModel(dirList, self)
		self.subfolder_COMBOBOX.setModel(subfoldersModel)
	
	
	def checkPlugins(self):
		'''
        Check if plugins are loaded.
        :return: plugin:MessageString dictionary if plugins were not found loaded. Empty dict if everything was found.
        :rtype: dict
        '''
		checkDict = self.appTools.checkPlugins(self)
		return checkDict
	
	
	def preResetCaches(self):
		check = self.checkPlugins()
		if len(check.keys()) <= 0:
			self.resetCacheNodes()
		else:
			if self.cacheComp.CacheFileTypes_Component.alembic_RBTN.isChecked():
				if "AlembicImport" in check.keys():
					self.baseMod.statusBar_update(check["AlembicImport"], alert=True)
				elif "GPUcache" in check.keys():
					self.baseMod.statusBar_update(check["GPUcache"], alert=True)
				else:
					self.resetCacheNodes()
			elif self.cacheComp.CacheFileTypes_Component.alembicExo_RBTN.isChecked():
				if "Exocortex" in check.keys():
					self.baseMod.statusBar_update(check["Exocortex"], alert=True)
				else:
					self.resetCacheNodes()
	
	
	def resetCacheNodes(self):
		"""Reset all cache nodes with given parameters"""
		#Point cache
		if self.cacheComp.pointCache_RBTN.isChecked():
			if self.reImport_CHKBOX.isChecked():
				cacheNodes = mc.ls(type="cacheFile")
				for node in cacheNodes:
					#Get current info
					baseDirectory = mc.getAttr(node + ".cachePath")
					cacheName = mc.getAttr(node + ".cacheName")
					objNode = mc.cacheFile(node, query= True, geometry= True)
					#Delete node
					mc.delete(node)
					#Re-import cache
					switch = mel.eval('createHistorySwitch("' +  objNode[0] + '",false);')
					newCacheNode = mc.cacheFile(f=(baseDirectory + cacheName + ".xml"), ia='%s.inp[0]' % switch , attachFile=True)
					mc.connectAttr((newCacheNode + ".inRange") , (switch + ".playFromCache") , force=True)
			else:
				#Get selected nodes
				selected = self.cacheNodes_LISTVIEW.selectedIndexes()
				selectedNames = []
				for each in selected:
					selectedNames.append( str( each.data()) )
				for node in selectedNames:
					#Set new time range
					mc.setAttr(node + ".startFrame", self.appTools.tRange.startFrame)
					mc.setAttr(node + ".sourceStart", self.appTools.tRange.startFrame)
					mc.setAttr(node + ".originalStart", self.appTools.tRange.startFrame)
					mc.setAttr(node + ".sourceEnd", self.appTools.tRange.endFrame)
					mc.setAttr(node + ".originalEnd", self.appTools.tRange.endFrame)
				if self.nodesPath_CHKBOX.isChecked():
					if self.customPathComp.customPath_CHKBOX.isChecked():
						customPath = str(self.customPathComp.customPath_LINEEDIT.text())
						currProj = mc.workspace(fullName = True)
						projLen = len(currProj)
						newPath = customPath
						if customPath[:projLen] == currProj:
							newPath = customPath[projLen+1:]
						for node in selectedNames:
							mc.setAttr(node + ".cachePath", newPath, type= "string")
					else:
						currProj = mc.workspace(fullName = True)
						newPath = self.buildProjSeqShotPath(False)
						if self.pInfo.project == currProj:
							newPath = self.buildProjSeqShotPath(True)
						for node in selectedNames:
							mc.setAttr(node + ".cachePath", newPath, type= "string")
		#Maya Alembic
		elif self.cacheComp.alembic_RBTN.isChecked():
			if self.reImport_CHKBOX.isChecked():
				cacheNodes = mc.ls(type="AlembicNode")
				gpuNodes = mc.ls(type= "gpuCache", fullPath= True)
				for node in cacheNodes:
					abcFileName = mc.getAttr(node + ".abc_File")
					#lyr-shadingGroup dict
					lyrShading = {}
					#Get transform node
					shapeNode = mc.listConnections(node + ".outPolyMesh[0]", shapes= True)
					transformNode = mc.listConnections(node + ".outPolyMesh[0]")
					controlNode = mc.listRelatives(transformNode, parent= True)
					#List all render layers
					renderLyrs = mc.ls(type= "renderLayer")
					for lyr in renderLyrs:
						if not mc.referenceQuery(lyr, isNodeReferenced= True):
							lyrMembers = mc.editRenderLayerMembers(lyr, query=True )
							if transformNode[0] in lyrMembers or shapeNode[0] in lyrMembers:
								mc.editRenderLayerGlobals(currentRenderLayer= lyr)
								shaderGroup = mc.listConnections(shapeNode[0], type= "shadingEngine")
								lyrShading[lyr] = shaderGroup[0]
					#Delete node
					mc.delete(transformNode)
					#Re-import cache
					melEvalStr = 'AbcImport -mode import "' + abcFileName + '";'
					alembicNode = mel.eval(melEvalStr)
					newShapeNode = mc.listConnections(alembicNode + ".outPolyMesh[0]", shapes= True)
					newTransformNode = mc.listConnections(alembicNode + ".outPolyMesh[0]")
					mc.parent(newTransformNode, controlNode)
					#Reconstruct render layer memberships and shader assignments
					for lyr in lyrShading.keys():
						mc.editRenderLayerGlobals(currentRenderLayer= lyr)
						if lyr != "defaultRenderLayer":
							mc.editRenderLayerMembers(lyr, newTransformNode[0])
						mc.sets(newShapeNode[0], edit= True, forceElement= lyrShading[lyr])
				for node in gpuNodes:
					melEvalStr = 'gpuCache -e -refresh ' + node + '";'
					mel.eval(melEvalStr)
			else:
				#Get selected nodes
				selected = self.cacheNodes_LISTVIEW.selectedIndexes()
				selectedNames = []
				for each in selected:
					selectedNames.append( str( each.data()) )
				if self.nodesPath_CHKBOX.isChecked():
					if self.customPathComp.customPath_CHKBOX.isChecked():
						customPath = str(self.customPathComp.customPath_LINEEDIT.text())
						for node in selectedNames:
							if mc.referenceQuery(node, isNodeReferenced= True):
								#Unlock referenced node to be able to overwrite filepath
								mc.lockNode(node, lock=False)
							if mc.nodeType(node) == "AlembicNode":
								abcFileName = mc.getAttr(node + ".abc_File")
								lastSlashLen = abcFileName.rfind("\\")
								if lastSlashLen == -1:
									lastSlashLen = abcFileName.rfind("/")
								newPath = customPath + "\\" + abcFileName[lastSlashLen+1:]
								mc.setAttr(node + ".abc_File", newPath, type= "string")
							elif mc.nodeType(node) == "gpuCache":
								gpuFileName = mc.getAttr(node + ".cacheFileName")
								lastSlashLen = gpuFileName.rfind("\\")
								if lastSlashLen == -1:
									lastSlashLen = gpuFileName.rfind("/")
								newPath = customPath + "\\" + gpuFileName[lastSlashLen+1:]
								mc.setAttr(node + ".cacheFileName", newPath, type= "string")
					else:
						thisPath = self.buildProjSeqShotPath(False)
						subfolder = self.subfolder_COMBOBOX.currentText()
						if subfolder != '':
							thisPath += '/' + subfolder
						for node in selectedNames:
							if mc.referenceQuery(node, isNodeReferenced= True):
								#Unlock referenced node to be able to overwrite filepath
								mc.lockNode(node, lock=False)
							if mc.nodeType(node) == "AlembicNode":
								abcFileName = mc.getAttr(node + ".abc_File")
								lastSlashLen = abcFileName.rfind("\\")
								if lastSlashLen == -1:
									lastSlashLen = abcFileName.rfind("/")
								newPath = thisPath + "\\" + abcFileName[lastSlashLen+1:]
								mc.setAttr(node + ".abc_File", newPath, type= "string")
							elif mc.nodeType(node) == "gpuCache":
								gpuFileName = mc.getAttr(node + ".cacheFileName")
								lastSlashLen = gpuFileName.rfind("\\")
								if lastSlashLen == -1:
									lastSlashLen = gpuFileName.rfind("/")
								newPath = thisPath + "\\" + gpuFileName[lastSlashLen+1:]
								mc.setAttr(node + ".cacheFileName", newPath, type= "string")
		#Exocortex Alembic
		elif self.cacheComp.alembicExo_RBTN.isChecked():
			if self.reImport_CHKBOX.isChecked():
				cacheNodes = mc.ls(type="ExocortexAlembicFile")
				for node in cacheNodes:
					exoFileName = mc.getAttr(node + ".fileName")
					#lyr-shadingGroup dict
					lyrShading = {}
					#Get transform node
					exoFileConnections = mc.listConnections(node + ".outFileName")
					exoXFormNode = exoFileConnections[0]
					for each in exoFileConnections:
						if mc.nodeType(each) == "ExocortexAlembicXform":
							exoXFormNode = each
							break
					transformNode = mc.listConnections(exoXFormNode, d=True, type="transform", scn=False)[0]
					shapeNode = mc.listRelatives(transformNode, shapes=True, noIntermediate=True)[0]
					#List all render layers
					renderLyrs = mc.ls(type= "renderLayer")
					for lyr in renderLyrs:
						if not mc.referenceQuery(lyr, isNodeReferenced= True):
							lyrMembers = mc.editRenderLayerMembers(lyr, query=True )
							if transformNode in lyrMembers or shapeNode in lyrMembers:
								mc.editRenderLayerGlobals(currentRenderLayer= lyr)
								shaderGroup = mc.listConnections(shapeNode, type= "shadingEngine")
								lyrShading[lyr] = shaderGroup[0]
					#Delete node
					mc.delete(transformNode)
					#Re-import cache
					fileNameStr = "filename=" + exoFileName
					mainOptions = "attachToExisting=0"
					variuosOptions = "normals=1;uvs=0;facesets=0"
					finalStr = ";".join([fileNameStr, mainOptions, variuosOptions])
					importExo = mc.ExocortexAlembic_import(j=[finalStr])
					importedExoTransform = ""
					for node in importExo:
						if mc.nodeType(node) == "transform":
							importedExoTransform = node
							break
					newShapeNode = mc.listRelatives(importedExoTransform, shapes=True, noIntermediate=True)[0]
					#Reconstruct render layer memberships and shader assignments
					for lyr in lyrShading.keys():
						mc.editRenderLayerGlobals(currentRenderLayer= lyr)
						if lyr != "defaultRenderLayer":
							mc.editRenderLayerMembers(lyr, importedExoTransform)
						mc.sets(newShapeNode, edit= True, forceElement= lyrShading[lyr])
			else:
				selected = self.cacheNodes_LISTVIEW.selectedIndexes()
				selectedNames = []
				for each in selected:
					selectedNames.append( str( each.data()) )
				if self.nodesPath_CHKBOX.isChecked():
					if self.customPathComp.customPath_CHKBOX.isChecked():
						customPath = str(self.customPathComp.customPath_LINEEDIT.text())
						for node in selectedNames:
							if mc.referenceQuery(node, isNodeReferenced= True):
								#Unlock referenced node to be able to overwrite filepath
								mc.lockNode(node, lock=False)
							exoFileName = mc.getAttr(node + ".fileName")
							lastSlashLen = exoFileName.rfind("\\")
							if lastSlashLen == -1:
								lastSlashLen = exoFileName.rfind("/")
							newPath = customPath + "\\" + exoFileName[lastSlashLen+1:]
							mc.setAttr(node + ".fileName", newPath, type= "string")
					else:
						thisPath = self.buildProjSeqShotPath(False)
						subfolder = self.subfolder_COMBOBOX.currentText()
						if subfolder != '':
							thisPath += '/' + subfolder
						for node in selectedNames:
							if mc.referenceQuery(node, isNodeReferenced= True):
								#Unlock referenced node to be able to overwrite filepath
								mc.lockNode(node, lock=False)
							exoFileName = mc.getAttr(node + ".fileName")
							lastSlashLen = exoFileName.rfind("\\")
							if lastSlashLen == -1:
								lastSlashLen = exoFileName.rfind("/")
							newPath = thisPath + "\\" + exoFileName[lastSlashLen+1:]
							mc.setAttr(node + ".fileName", newPath, type= "string")
		#Done Alert!!
		self.baseMod.statusBar_update("Done reseting caches.", success=True)
	
	
	def buildProjSeqShotPath(self, _relative):
		seq = str(self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.currentText())
		shot = str(self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentText())
		propertiesDict = {"abstractFolderType":"leaffolder",
		                  "concreteFolderType":"published_leaffolder",
		                  "folderFunction":"published_content",
		                  "folderConcreteFunction":"published_cache",
		                  "placeholder":False}
		cacheNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)[0]
		if cacheNode != None:
			seqDict={"name":"$SEQ_##",
		            "abstractFolderType":"subfolder",
		            "concreteFolderType":"sequence_subfolder",
		            "folderFunction":"grouping",
		            "folderConcreteFunction":"",
		            "placeholder":True}
			shotDict={"name":"$s_###",
		            "abstractFolderType":"subfolder",
		            "concreteFolderType":"shot_subfolder",
		            "folderFunction":"grouping",
		            "folderConcreteFunction":"",
		            "placeholder":True}
			pathList = self.pInfo.rebuildPath(cacheNode, {seq:seqDict, shot:shotDict})
			pathList.append(cacheNode.name)
			
			#Build path string
			server = self.sInfo.getProjectsFrom()
			if server [-1] in ["/","\\"]:
				server = server[:-1]
			
			pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
			if _relative:
				pathStr = "/".join(pathList[1:])
				
			return pathStr
		else:
			return None
	
	
	def populateList(self):
		"""Fills list with cache nodes"""
		#List nodes
		cacheNodes = mc.ls(type="cacheFile")
		if self.cacheComp.alembic_RBTN.isChecked():
			alembicNodes = mc.ls(type="AlembicNode")
			gpuNodes = mc.ls(type= "gpuCache")
			cacheNodes = alembicNodes + gpuNodes
		elif self.cacheComp.alembicExo_RBTN.isChecked():
			cacheNodes = mc.ls(type="ExocortexAlembicFile")
		cacheNodesModel = dvm.ObjectsListModel(cacheNodes)
		self.cacheNodes_LISTVIEW.setModel(cacheNodesModel)
		self.cacheNodes_LISTVIEW.setSelectionMode(self.cacheNodes_LISTVIEW.MultiSelection)
		
	
	def refreshReImport(self):
		if self.reImport_CHKBOX.isChecked():
			self.minmaxComp.container.setEnabled(False)
			self.nodesPath_CHKBOX.setEnabled(False)
			self.nodesPath_CHKBOX.setChecked(False)
			self.customPathComp.customPath_CHKBOX.setChecked(False)
			self.customPathComp.customPath_LINEEDIT.setEnabled(False)
			self.customPathComp.customPath_BTN.setEnabled(False)
			self.cacheNodes_LISTVIEW.setEnabled(False)
		else:
			if self.cacheComp.alembic_RBTN.isChecked():
				self.minmaxComp.container.setEnabled(False)
			else:
				self.minmaxComp.container.setEnabled(True)
			self.nodesPath_CHKBOX.setEnabled(True)
			self.nodesPath_CHKBOX.setChecked(True)
			if self.nodesPath_CHKBOX.isChecked():
				self.customPathComp.customPath_CHKBOX.setEnabled(True)
			self.cacheNodes_LISTVIEW.setEnabled(True)
	
	
	def refreshChangeBaseDir(self):
		if self.nodesPath_CHKBOX.isChecked():
			self.projSeqShotMod.container.setEnabled(True)
			self.customPathComp.customPath_CHKBOX.setEnabled(True)
			self.subfolder_COMBOBOX.setEnabled(True)
			self.subfolder_LABEL.setEnabled(True)
		else:
			self.projSeqShotMod.container.setEnabled(False)
			self.customPathComp.customPath_CHKBOX.setEnabled(False)
			self.subfolder_COMBOBOX.setEnabled(False)
			self.subfolder_LABEL.setEnabled(False)
	
	
	def refreshCustomPath(self):
		if self.customPathComp.customPath_CHKBOX.isChecked():
			self.projSeqShotMod.container.setEnabled(False)
			self.subfolder_COMBOBOX.setEnabled(False)
			self.subfolder_LABEL.setEnabled(False)
		else:
			self.projSeqShotMod.container.setEnabled(True)
			self.subfolder_COMBOBOX.setEnabled(True)
			self.subfolder_LABEL.setEnabled(True)
	
	
	def refreshFileType(self):
		if self.cacheComp.pointCache_RBTN.isChecked():
			self.minmaxComp.container.setEnabled(True)
			self.populateList()
		else:
			self.minmaxComp.container.setEnabled(False)
			self.populateList()

	
	##--------------------------------------------------------------------------------------------
	##Methods for Characters/Props tree view
	##--------------------------------------------------------------------------------------------     
	def cacheNodesOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.cacheNodes_LISTVIEW)
		selectAllQ = menu.addAction("Select All")
		selectCacheNodeQ = menu.addAction("Select Cache Node")
		selectGeometryQ = menu.addAction("Select Geometry")
		menu.popup(self.cacheNodes_LISTVIEW.mapToGlobal(pos))
		
		selectAllQ.triggered.connect(self.selectAllList)
		selectCacheNodeQ.triggered.connect(self.selectCacheNodeOpt)
		selectGeometryQ.triggered.connect(self.selectGeometryOpt)


	def selectAllList(self):
		self.cacheNodes_LISTVIEW.selectAll()
		

	def selectCacheNodeOpt(self):
		selected = self.cacheNodes_LISTVIEW.selectedIndexes()
		if len(selected) <= 0:
			self.baseMod.statusBar_update("Select at least one item in Characters/Props list.", alert=True)
		elif len(selected) > 1:
			self.baseMod.statusBar_update("Select just one item from the list to select its Cache Node.", alert=True)
		else:
			mc.select(self.cacheNodes_LISTVIEW.model().data(selected[0]), r=True)
		

	def selectGeometryOpt(self):
		selected = self.cacheNodes_LISTVIEW.selectedIndexes()
		if len(selected) <= 0:
			self.baseMod.statusBar_update("Select at least one item in Characters/Props list.", alert=True)
		else:
			mc.select(clear=True)
			for each in selected:
				cacheNode = self.cacheNodes_LISTVIEW.model().data(each)
				historyNode =  mc.listConnections(cacheNode + ".outCacheData", d=True, s=False)[0]
				if mc.nodeType(historyNode) == "historySwitch":
					transformNode = mc.listConnections(historyNode + ".outputGeometry", d=True, s=False)[0]
					mc.select(transformNode, add=True)
				elif mc.nodeType(historyNode) == "transform":
					mc.select(historyNode, add=True)
	

##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/pipeline/CacheReseter_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()