# -*- coding: utf-8 -*-
'''
Make incremental save and add status metadata to Maya files.

Created on Feb 06, 2014

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

__DEVMODE= True
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os, socket
import maya.cmds as mc
from cgx.gui.Qt import QtWidgets, QtCore
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
import cgx.gui.DataViewModels as dvm
from cgx.core.JSONManager import JSONManager


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
if __DEVMODE:
	appRootFolder = 'M:/CGXtools/cgx/Maya/scripts/pipeline/IncrementalSave'
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"
config_json = appRootFolder + "/cfg/" + "IncrementalSave_config.json"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)



##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, _studioInfo, _projectInfo, parent= None, appTools= None):
		'''
		:param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param parent: Main application window as a QMainWindow instance
        :type parent: QMainWindow
        :param appTools: Application Tools library for Camera Exporter
        :type appTools: Class inherits from Abstract_CameraTools
		'''
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = _studioInfo
		self.pInfo = _projectInfo
		
		self.appTools = appTools
		if self.appTools != None:
			self.initTool()
			self.createComponents()
			self.initComponents()
			self.initUI()
			self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		'''
        Connect signals.
        '''
		#BUTTONS
		self.save_BTN.clicked.connect(self.incrementalSave)
		self.cancel_BTN.clicked.connect(self.baseMod.quitApp)
		#SIGNALS
		self.projSeqShotMod.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.pipeStagesComp.initComponent)
	
	
	def initTool(self):
		'''
        Initializes tool settings
        '''
		self.initAttrs()
	
	
	def resetTool(self):
		self.initAttrs()
		self.initComponents()
		self.initUI()
		
		
	def initAttrs(self):
		'''
		Initializes variables and components for this tool.
		'''
		self.__client = "defaultClient"
		self.__toolName = "Incremental Save"
		self.__toolVersion = "2.0"
		
		self.configFile = JSONManager(config_json, self)
	
	
	def createComponents(self):
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo)
		self.projSeqShotMod = self.Pipeline_GUIModulesFactory.createModule('ProjSeqShot')
		self.projSeqShotMod.pos = [0,10]
		self.pipeStagesComp = pipeComponents.PipelineStages_Component(self,self.centralWidget(),self.pInfo)
		self.pipeStagesComp.pos = [0,50]
		
	
	def initComponents(self):
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded.", status=True)
		self.appTools.setCurrentProject(self.projSeqShotMod.Project_Component)
		self.appTools.setCurrentSequence(self.projSeqShotMod.Sequence_Component)
		self.appTools.setCurrentShot(self.projSeqShotMod.Shot_Component)
		self.appTools.setCurrentPipelineStage(self.pipeStagesComp)
	
	
	def initUI(self):
		'''
        Initialize GUI components.
        '''
		self.populateAuthors()
	
	
	def incrementalSave(self):
		currFile = mc.file(query=True, expandName=True)
		currProj = self.pInfo.project
		startDir = self.sInfo.getProjectsFrom()
		project = self.projSeqShotMod.Project_Component.project_COMBOBOX.currentText()
		seq = self.projSeqShotMod.Sequence_Component.seq_COMBOBOX.currentText()
		shot = self.projSeqShotMod.Shot_Component.shot_COMBOBOX.currentText()
		stage = self.pipeStagesComp.pipelineStage_COMBOBOX.currentText()
		author = self.author_COMBOBOX.currentText()
		if currFile[-8:] == "untitled":
			browseDialog = QtWidgets.QFileDialog(self)
			browseDialog.setDirectory(startDir + currProj)
			browseDialog.setFileMode(browseDialog.FileMode.AnyFile)
			browseDialog.setNameFilters(["Maya Binary (*.mb)", "Maya ASCII (*.ma)"])
			browseDialog.setLabelText(browseDialog.Accept,"Save")
			browseDialog.setWindowTitle("Save")
			dialogReturn = browseDialog.exec_()
			if dialogReturn == 1:
				selectedFilter = browseDialog.selectedNameFilter()
				extension = ".mb"
				if selectedFilter == "Maya ASCII (*.ma)":
					extension = ".ma"
				selectedFiles = browseDialog.selectedFiles()
				finalPath = selectedFiles[0]
				if "\\" in finalPath:
					finalPath = finalPath.replace("\\","/")
				directory = finalPath[:finalPath.rfind("/")]
				sceneNameWExt = finalPath.split("/")[-1]
				dotIndex = sceneNameWExt.rfind(".")
				sceneName = sceneNameWExt[:dotIndex]
				if dotIndex == -1:
					sceneName = sceneNameWExt
				newName = ""
				if sceneName.rfind(".") != -1: #Check if scene has version
					currentVersion = sceneName[sceneName.rfind(".")+1:]
					digitsNumber = len(currentVersion)
					intVersion = int(currentVersion)
					newVersion = str(intVersion + 1).zfill(digitsNumber)
					newName = sceneName[:sceneName.rfind(".")+1] + newVersion + extension
				else:
					newName = sceneName + ".0001" + extension
				mc.fileInfo('CGXnotes', self.notes_PLAINTEXTEDIT.toPlainText())
				mc.fileInfo('CGXproject', project)
				mc.fileInfo('CGXseq', seq)
				mc.fileInfo('CGXshot', shot)
				mc.fileInfo('CGXstage', stage)
				mc.fileInfo('CGXauthor', author)
				mc.file(rename= directory + "/" + newName)
				mc.file(save=True, force=True)
				
				self.close()
		else:
			mc.file(save=True, force=True)
			mc.fileInfo('CGXnotes', self.notes_PLAINTEXTEDIT.toPlainText())
			mc.fileInfo('CGXproject', project)
			mc.fileInfo('CGXseq', seq)
			mc.fileInfo('CGXshot', shot)
			mc.fileInfo('CGXstage', stage)
			mc.fileInfo('CGXauthor', author)
			if "\\" in currFile:
				currFile = currFile.replace("\\","/")
			finalPath = currFile
			directory = finalPath[:finalPath.rfind("/")]
			sceneNameWExt = finalPath.split("/")[-1]
			dotIndex = sceneNameWExt.rfind(".")
			sceneName = sceneNameWExt[:dotIndex]
			if dotIndex == -1:
				sceneName = sceneNameWExt
			newName = ""
			if sceneName.rfind(".") != -1: #Check if scene has version
				currentVersion = sceneName[sceneName.rfind(".")+1:]
				digitsNumber = len(currentVersion)
				intVersion = int(currentVersion)
				newVersion = str(intVersion + 1).zfill(digitsNumber)
				newName = sceneName[:sceneName.rfind(".")+1] + newVersion + sceneNameWExt[sceneNameWExt.rfind("."):]
			else:
				newName = sceneName + ".0001" + sceneNameWExt[sceneNameWExt.rfind("."):]
			mc.file(rename= directory + "/" + newName)
			mc.file(save=True, force=True)
			
			self.close()
	
	
	def populateAuthors(self):
		authorsDict = self.configFile.getName("authors", False)[0].asDict()
		dataList = []
		for row in authorsDict.keys():
			if row != "name":
				dataList.append(row)
		authorsModel = dvm.ObjectsListModel(dataList)
		self.author_COMBOBOX.setModel(authorsModel)
		
		currWorkstation = socket.gethostname()
		thisAuthor="Freelancer"
		for each in authorsDict.keys():
			if authorsDict[each] == currWorkstation:
				thisAuthor = each
				break
		
		i = len(dataList)
		for x in range(i):
			if dataList[x] == thisAuthor:
				self.author_COMBOBOX.setCurrentIndex(x)
				break
	

##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Help Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates About Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		'''
		Creates Config Dialog for current tool.
		:param parent: Main tools window.
        :type parent: QMainWindow
		'''
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		
		self.configFile = JSONManager(config_json, self)
		
		self.initUI()
		self.setConnections()
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		#SIGNALS
		self.cancel_BTN.clicked.connect(self.cancelAll)
		self.apply_BTN.clicked.connect(self.applyConfig)
		
		self.authors_TABLEVIEW.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
		self.authors_TABLEVIEW.customContextMenuRequested.connect(self.authorsOptions)
	
	
	def initUI(self):
		"""Read JSON config file"""
		
		#Authors
		self.populateAuthors()
	
	
	def populateAuthors(self):
		authorsDict = self.configFile.getName("authors", False)[0].asDict()
		dataList = []
		for row in authorsDict.keys():
			if row != "name":
				dataList.append([row,authorsDict[row]])
		headers = ["Author", "Workstation"]
		authorsModel = AuthorsDataTableModel(dataList, headers, self)
		self.authors_TABLEVIEW.setModel(authorsModel)
	
	
	def applyConfig(self):
		"""Apply changes to JSON file"""
		
		jsonObj = self.configFile.jsonObj
		newAuthors = self.authors_TABLEVIEW.model().dataList
		newAuthorsDict = {}
		for each in newAuthors:
			newAuthorsDict[each[0]] = each[1]
		jsonObj["authors"] = newAuthorsDict
		self.configFile.writeJson(jsonObj, self.configFile.fileName)
		
		self.done(1)
	
	
	def cancelAll(self):
		"""Method to close the UI"""
		
		self.done(0)
	
	
	##--------------------------------------------------------------------------------------------
	##Methods for authors
	##--------------------------------------------------------------------------------------------
	def authorsOptions (self,pos):
		"""Method that creates the popupmenu"""
		menu = QtWidgets.QMenu(self.authors_TABLEVIEW)
		addAuthorQ = menu.addAction("Add author")
		deleteAuthorQ = menu.addAction("Delete author")
		menu.popup(self.authors_TABLEVIEW.mapToGlobal(pos))
		
		addAuthorQ.triggered.connect(self.addAuthor)
		deleteAuthorQ.triggered.connect(self.deleteAuthor)
		

	def addAuthor(self):
		self.authors_TABLEVIEW.model().insertRows(self.authors_TABLEVIEW.model().rowCount(QtCore.QModelIndex()), 1, ["New author", "New workstation"])


	def deleteAuthor(self):
		selected = self.authors_TABLEVIEW.selectedIndexes()
		selRowsDirty = []
		for item in selected:
			thisRow = item.row()
			selRowsDirty.append(thisRow)
		selSet = set(selRowsDirty)
		selRows = list(selSet)
		self.authors_TABLEVIEW.model().removeRows(selRows, len(selRows))


##--------------------------------------------------------------------------------------------
##Class: Table Data Model reimplementation for authors config table
##--------------------------------------------------------------------------------------------
class AuthorsDataTableModel(dvm.DataTableModel):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, dataList, headers, parent):
		super(AuthorsDataTableModel, self).__init__(dataList, headers, parent)

	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def flags(self, index):
		"""Set flags of each cell"""
		return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	if __DEVMODE:
		x = open('M:/CGXtools/DEV/Maya/plugin/oneLiners/pipeline/IncrementalSave_dev.py','r')
		exec x.read()
		x.close()


if __name__ == '__main__' or 'eclipsePython' in __name__:
	main()