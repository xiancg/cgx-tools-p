# -*- coding: utf-8 -*-
'''
Test the implementation of the main components for GUIs. This file serves as a template as well.

Created on Mar 28, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.gui.Qt import QtWidgets, QtCore
import sys, os
import cgx.gui.uiParser as uiParser
import cgx.gui.components.GUIModules_Factories as guiFactories
import cgx.gui.components.Pipeline_Components as pipeComponents
from cgx.core.StudioInfo import StudioInfo
from cgx.core.ProjectInfo import ProjectInfo
from cgx.libs.Abstract_TimeRange import Abstract_TimeRange


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados", "Max Rocamora"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
## EXTERNAL FILES // CFG + UI + Globals
##--------------------------------------------------------------------------------------------
appRootFolder = os.path.dirname(__file__)
main_ui = appRootFolder + "/ui/" + "main.ui"
help_ui = appRootFolder + "/ui/" + "help.ui"
about_ui = appRootFolder + "/ui/" + "about.ui"
config_ui = appRootFolder + "/ui/" + "config.ui"

form, base = uiParser.loadUiType(main_ui)
helpform, helpbase = uiParser.loadUiType(help_ui)
aboutform, aboutbase = uiParser.loadUiType(about_ui)
configform, configbase = uiParser.loadUiType(config_ui)


##------------------------------------------------------------------------------------------------
## Class: Main GUI
##------------------------------------------------------------------------------------------------
class Main_GUI(form,base):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent= None):
		super(Main_GUI,self).__init__(parent)
		self.setupUi(self)
		
		self.sInfo = StudioInfo()
		self.pInfo = ProjectInfo(self.sInfo, self.sInfo.getProjects('','')[0])
		self.Base_GUIModulesFactory = guiFactories.Base_GUIModulesFactory(self,configDialog=Config_DialogGUI, helpDialog=Help_DialogGUI, aboutDialog=About_DialogGUI)
		self.baseMod = self.Base_GUIModulesFactory.createModule('base')
		self.Pipeline_GUIModulesFactory = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfo, self.pInfo, tRange=Abstract_TimeRange())
		self.projSeqShotMod = self.Pipeline_GUIModulesFactory.createModule('ProjSeqShot')
		self.assetsMod = self.Pipeline_GUIModulesFactory.createModule('Assets')
		self.timeRangeMod = self.Pipeline_GUIModulesFactory.createModule('TimeRange')
		self.timeRangeMod.OffsetStartFrame_Component.container.setVisible(False)
		self.cacheMod = self.Pipeline_GUIModulesFactory.createModule('Cache')
		
		self.sInfoB = StudioInfo()
		self.pInfoB = ProjectInfo(self.sInfoB, self.sInfoB.getProjects('','')[0])
		self.Pipeline_GUIModulesFactoryB = guiFactories.Pipeline_GUIModulesFactory(self, self.sInfoB, self.pInfoB, tRange=Abstract_TimeRange())
		self.projAssetsMod = self.Pipeline_GUIModulesFactoryB.createModule('ProjAssets')
		self.seqShotMod = self.Pipeline_GUIModulesFactoryB.createModule('SeqShot')
		
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
		self.setConnections()
		self.createComponents()
		self.initTool()
		
		
	##--------------------------------------------------------------------------------------------
	##Methods
	##--------------------------------------------------------------------------------------------
	def setConnections(self):
		self.projSeqShotMod.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.init_assetsMod)
	
	def init_assetsMod(self):
		self.assetsMod.initComponents()
	
	def initTool(self):
		'''
	    Initializes tool settings, also reset the tool on demand.
	    '''
		self.initAttrs()
		self.baseMod.statusBar_update(self.__toolName + " " + self.__toolVersion + " loaded!", status=True)
		
	def initAttrs(self):
		'''
	    Initializes variables for this tool.
	    '''
		self.__client = "defaultClient"
		self.__toolName = "Base Gui"
		self.__toolVersion = "1.0"
	
	def createComponents(self):
		self.customPathComp = pipeComponents.CustomPath_Component(self,self.centralWidget(), self.sInfo)
		self.customPathComp.pos = [0,275]
		self.customPathComp.fileMode = self.customPathComp.browseDialog.FileMode.Directory
		self.pipeStagesComp = pipeComponents.PipelineStages_Component(self,self.centralWidget(), self.pInfo)
		self.pipeStagesComp.pos = [0,325]
		self.progressComp = pipeComponents.Progress_Component(self,self.centralWidget())
		self.progressComp.pos = [0,475]
		self.progressComp.maximum = 100
		self.progressComp.reset()


##--------------------------------------------------------------------------------------------
##Create help dialog
##--------------------------------------------------------------------------------------------
class Help_DialogGUI(helpform, helpbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		super(Help_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create about dialog
##--------------------------------------------------------------------------------------------
class About_DialogGUI(aboutform, aboutbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		super(About_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Create config dialog
##--------------------------------------------------------------------------------------------
class Config_DialogGUI(configform, configbase):
	##--------------------------------------------------------------------------------------------
	##Constructor
	##--------------------------------------------------------------------------------------------
	def __init__(self, parent):
		super(Config_DialogGUI,self).__init__(parent)
		self.setupUi(self)
		self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
	app = QtWidgets.QApplication(sys.argv)
	mainWindowTest = Main_GUI()
	mainWindowTest.show()
	sys.exit(app.exec_())


if __name__ == '__main__':
	main()