# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:/CGXtools/gui/ui/CustomPath_Component.ui'
#
# Created: Thu Apr 07 15:23:04 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(332, 62)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(0, 0, 331, 61))
        self.widget.setObjectName("widget")
        self.customPath_LINEEDIT = QtGui.QLineEdit(self.widget)
        self.customPath_LINEEDIT.setEnabled(False)
        self.customPath_LINEEDIT.setGeometry(QtCore.QRect(10, 30, 281, 22))
        self.customPath_LINEEDIT.setObjectName("customPath_LINEEDIT")
        self.customPath_CHKBOX = QtGui.QCheckBox(self.widget)
        self.customPath_CHKBOX.setGeometry(QtCore.QRect(10, 10, 141, 22))
        self.customPath_CHKBOX.setObjectName("customPath_CHKBOX")
        self.customPath_BTN = QtGui.QPushButton(self.widget)
        self.customPath_BTN.setEnabled(False)
        self.customPath_BTN.setGeometry(QtCore.QRect(300, 30, 22, 22))
        self.customPath_BTN.setObjectName("customPath_BTN")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.customPath_CHKBOX.setText(QtGui.QApplication.translate("MainWindow", "Use custom path:", None, QtGui.QApplication.UnicodeUTF8))
        self.customPath_BTN.setText(QtGui.QApplication.translate("MainWindow", "...", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

