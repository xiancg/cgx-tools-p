# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:/CGXtools/gui/ui/PipelineStages_Component.ui'
#
# Created: Thu Apr 07 15:23:22 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(172, 52)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(0, 0, 171, 51))
        self.widget.setObjectName("widget")
        self.pipelineStage_COMBOBOX = QtGui.QComboBox(self.widget)
        self.pipelineStage_COMBOBOX.setGeometry(QtCore.QRect(10, 20, 151, 22))
        self.pipelineStage_COMBOBOX.setObjectName("pipelineStage_COMBOBOX")
        self.pipelineStage_LABEL = QtGui.QLabel(self.widget)
        self.pipelineStage_LABEL.setGeometry(QtCore.QRect(10, 0, 81, 20))
        font = QtGui.QFont()
        font.setWeight(50)
        font.setBold(False)
        self.pipelineStage_LABEL.setFont(font)
        self.pipelineStage_LABEL.setObjectName("pipelineStage_LABEL")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.pipelineStage_LABEL.setText(QtGui.QApplication.translate("MainWindow", "Pipeline Stage", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

