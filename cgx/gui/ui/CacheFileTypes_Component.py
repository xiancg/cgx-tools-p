# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:/CGXtools/gui/ui/CacheFileTypes_Component.ui'
#
# Created: Thu Apr 07 15:22:48 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(332, 61)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(0, 0, 331, 61))
        self.widget.setObjectName("widget")
        self.alembic_RBTN = QtGui.QRadioButton(self.widget)
        self.alembic_RBTN.setGeometry(QtCore.QRect(110, 30, 82, 19))
        self.alembic_RBTN.setObjectName("alembic_RBTN")
        self.fileType_BTNGRP = QtGui.QButtonGroup(MainWindow)
        self.fileType_BTNGRP.setObjectName("fileType_BTNGRP")
        self.fileType_BTNGRP.addButton(self.alembic_RBTN)
        self.alembicExo_RBTN = QtGui.QRadioButton(self.widget)
        self.alembicExo_RBTN.setGeometry(QtCore.QRect(190, 30, 111, 19))
        self.alembicExo_RBTN.setObjectName("alembicExo_RBTN")
        self.fileType_BTNGRP.addButton(self.alembicExo_RBTN)
        self.fileType_LABEL = QtGui.QLabel(self.widget)
        self.fileType_LABEL.setGeometry(QtCore.QRect(10, 10, 51, 16))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.fileType_LABEL.setFont(font)
        self.fileType_LABEL.setObjectName("fileType_LABEL")
        self.pointCache_RBTN = QtGui.QRadioButton(self.widget)
        self.pointCache_RBTN.setGeometry(QtCore.QRect(10, 30, 82, 19))
        self.pointCache_RBTN.setObjectName("pointCache_RBTN")
        self.fileType_BTNGRP.addButton(self.pointCache_RBTN)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.alembic_RBTN.setText(QtGui.QApplication.translate("MainWindow", "Alembic", None, QtGui.QApplication.UnicodeUTF8))
        self.alembicExo_RBTN.setText(QtGui.QApplication.translate("MainWindow", "Alembic Exocortex", None, QtGui.QApplication.UnicodeUTF8))
        self.fileType_LABEL.setText(QtGui.QApplication.translate("MainWindow", "File Type", None, QtGui.QApplication.UnicodeUTF8))
        self.pointCache_RBTN.setText(QtGui.QApplication.translate("MainWindow", "Point cache", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

