# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:/CGXtools/gui/ui/Assets_Module.ui'
#
# Created: Thu Apr 07 12:13:26 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(332, 102)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(0, 0, 331, 101))
        self.widget.setObjectName("widget")
        self.assetType_COMBOBOX = QtGui.QComboBox(self.widget)
        self.assetType_COMBOBOX.setGeometry(QtCore.QRect(10, 20, 151, 22))
        self.assetType_COMBOBOX.setObjectName("assetType_COMBOBOX")
        self.assetType_LABEL = QtGui.QLabel(self.widget)
        self.assetType_LABEL.setGeometry(QtCore.QRect(10, 0, 71, 20))
        font = QtGui.QFont()
        font.setWeight(50)
        font.setBold(False)
        self.assetType_LABEL.setFont(font)
        self.assetType_LABEL.setObjectName("assetType_LABEL")
        self.assetName_LABEL = QtGui.QLabel(self.widget)
        self.assetName_LABEL.setGeometry(QtCore.QRect(10, 50, 71, 20))
        font = QtGui.QFont()
        font.setWeight(50)
        font.setBold(False)
        self.assetName_LABEL.setFont(font)
        self.assetName_LABEL.setObjectName("assetName_LABEL")
        self.assetName_COMBOBOX = QtGui.QComboBox(self.widget)
        self.assetName_COMBOBOX.setGeometry(QtCore.QRect(10, 70, 151, 22))
        self.assetName_COMBOBOX.setObjectName("assetName_COMBOBOX")
        self.subAssetName_COMBOBOX = QtGui.QComboBox(self.widget)
        self.subAssetName_COMBOBOX.setGeometry(QtCore.QRect(170, 70, 151, 22))
        self.subAssetName_COMBOBOX.setObjectName("subAssetName_COMBOBOX")
        self.subAssetName_LABEL = QtGui.QLabel(self.widget)
        self.subAssetName_LABEL.setGeometry(QtCore.QRect(170, 50, 81, 22))
        font = QtGui.QFont()
        font.setWeight(50)
        font.setBold(False)
        self.subAssetName_LABEL.setFont(font)
        self.subAssetName_LABEL.setObjectName("subAssetName_LABEL")
        self.fileType_COMBOBOX = QtGui.QComboBox(self.widget)
        self.fileType_COMBOBOX.setGeometry(QtCore.QRect(170, 20, 151, 22))
        self.fileType_COMBOBOX.setObjectName("fileType_COMBOBOX")
        self.fileType_LABEL = QtGui.QLabel(self.widget)
        self.fileType_LABEL.setGeometry(QtCore.QRect(170, 0, 121, 22))
        font = QtGui.QFont()
        font.setWeight(50)
        font.setBold(False)
        self.fileType_LABEL.setFont(font)
        self.fileType_LABEL.setObjectName("fileType_LABEL")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.assetType_LABEL.setText(QtGui.QApplication.translate("MainWindow", "Asset type", None, QtGui.QApplication.UnicodeUTF8))
        self.assetName_LABEL.setText(QtGui.QApplication.translate("MainWindow", "Asset Name", None, QtGui.QApplication.UnicodeUTF8))
        self.subAssetName_LABEL.setText(QtGui.QApplication.translate("MainWindow", "SubAsset Name", None, QtGui.QApplication.UnicodeUTF8))
        self.fileType_LABEL.setText(QtGui.QApplication.translate("MainWindow", "File type", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

