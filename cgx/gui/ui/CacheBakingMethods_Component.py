# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:/CGXtools/gui/ui/CacheBakingMethods_Component.ui'
#
# Created: Thu Apr 07 15:22:29 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(332, 61)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(0, 0, 331, 61))
        self.widget.setObjectName("widget")
        self.vertexBake_RBTN = QtGui.QRadioButton(self.widget)
        self.vertexBake_RBTN.setGeometry(QtCore.QRect(10, 30, 82, 19))
        self.vertexBake_RBTN.setObjectName("vertexBake_RBTN")
        self.bakingMethos_BTNGRP = QtGui.QButtonGroup(MainWindow)
        self.bakingMethos_BTNGRP.setObjectName("bakingMethos_BTNGRP")
        self.bakingMethos_BTNGRP.addButton(self.vertexBake_RBTN)
        self.bakingMethod_LABEL = QtGui.QLabel(self.widget)
        self.bakingMethod_LABEL.setGeometry(QtCore.QRect(10, 10, 91, 16))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.bakingMethod_LABEL.setFont(font)
        self.bakingMethod_LABEL.setObjectName("bakingMethod_LABEL")
        self.alembicBake_RBTN = QtGui.QRadioButton(self.widget)
        self.alembicBake_RBTN.setGeometry(QtCore.QRect(110, 30, 91, 19))
        self.alembicBake_RBTN.setObjectName("alembicBake_RBTN")
        self.bakingMethos_BTNGRP.addButton(self.alembicBake_RBTN)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.vertexBake_RBTN.setText(QtGui.QApplication.translate("MainWindow", "Vertex bake", None, QtGui.QApplication.UnicodeUTF8))
        self.bakingMethod_LABEL.setText(QtGui.QApplication.translate("MainWindow", "Baking Method", None, QtGui.QApplication.UnicodeUTF8))
        self.alembicBake_RBTN.setText(QtGui.QApplication.translate("MainWindow", "Alembic bake", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

