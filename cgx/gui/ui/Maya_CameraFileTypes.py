# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:/CGXtools/gui/ui/Maya_CameraFileTypes.ui'
#
# Created: Fri Apr 08 16:59:15 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(332, 62)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(0, 0, 331, 61))
        self.widget.setObjectName("widget")
        self.mb_CHKBOX = QtGui.QCheckBox(self.widget)
        self.mb_CHKBOX.setGeometry(QtCore.QRect(150, 10, 121, 19))
        self.mb_CHKBOX.setObjectName("mb_CHKBOX")
        self.ma_CHKBOX = QtGui.QCheckBox(self.widget)
        self.ma_CHKBOX.setGeometry(QtCore.QRect(10, 10, 111, 19))
        self.ma_CHKBOX.setObjectName("ma_CHKBOX")
        self.abc_CHKBOX = QtGui.QCheckBox(self.widget)
        self.abc_CHKBOX.setGeometry(QtCore.QRect(150, 40, 111, 19))
        self.abc_CHKBOX.setObjectName("abc_CHKBOX")
        self.fbx_CHKBOX = QtGui.QCheckBox(self.widget)
        self.fbx_CHKBOX.setGeometry(QtCore.QRect(10, 40, 121, 19))
        self.fbx_CHKBOX.setObjectName("fbx_CHKBOX")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.mb_CHKBOX.setText(QtGui.QApplication.translate("MainWindow", ".mb (Maya Binary)", None, QtGui.QApplication.UnicodeUTF8))
        self.ma_CHKBOX.setText(QtGui.QApplication.translate("MainWindow", ".ma (Maya ASCII)", None, QtGui.QApplication.UnicodeUTF8))
        self.abc_CHKBOX.setText(QtGui.QApplication.translate("MainWindow", ".abc (Alembic)", None, QtGui.QApplication.UnicodeUTF8))
        self.fbx_CHKBOX.setText(QtGui.QApplication.translate("MainWindow", ".fbx (Autodesk FBX)", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

