# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:/CGXtools/gui/ui/Progress_Component.ui'
#
# Created: Thu Apr 07 15:23:42 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(332, 71)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(0, 0, 331, 71))
        self.widget.setObjectName("widget")
        self.progress_LABEL = QtGui.QLabel(self.widget)
        self.progress_LABEL.setGeometry(QtCore.QRect(10, 0, 61, 22))
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.progress_LABEL.setFont(font)
        self.progress_LABEL.setObjectName("progress_LABEL")
        self.progress_PBAR = QtGui.QProgressBar(self.widget)
        self.progress_PBAR.setGeometry(QtCore.QRect(10, 30, 311, 31))
        self.progress_PBAR.setProperty("value", 0)
        self.progress_PBAR.setObjectName("progress_PBAR")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.progress_LABEL.setText(QtGui.QApplication.translate("MainWindow", "Progress", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

