# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'M:/CGXtools/gui/ui/ProjSeqShot_Module.ui'
#
# Created: Wed Apr 06 16:45:11 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(332, 52)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtGui.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(0, 0, 331, 51))
        self.widget.setObjectName("widget")
        self.shot_LABEL = QtGui.QLabel(self.widget)
        self.shot_LABEL.setGeometry(QtCore.QRect(250, 0, 31, 20))
        self.shot_LABEL.setObjectName("shot_LABEL")
        self.seq_LABEL = QtGui.QLabel(self.widget)
        self.seq_LABEL.setGeometry(QtCore.QRect(170, 0, 51, 20))
        self.seq_LABEL.setObjectName("seq_LABEL")
        self.shot_COMBOBOX = QtGui.QComboBox(self.widget)
        self.shot_COMBOBOX.setGeometry(QtCore.QRect(250, 20, 71, 22))
        self.shot_COMBOBOX.setObjectName("shot_COMBOBOX")
        self.project_COMBOBOX = QtGui.QComboBox(self.widget)
        self.project_COMBOBOX.setEnabled(True)
        self.project_COMBOBOX.setGeometry(QtCore.QRect(10, 20, 151, 22))
        self.project_COMBOBOX.setObjectName("project_COMBOBOX")
        self.project_LABEL = QtGui.QLabel(self.widget)
        self.project_LABEL.setGeometry(QtCore.QRect(10, 0, 46, 20))
        self.project_LABEL.setObjectName("project_LABEL")
        self.seq_COMBOBOX = QtGui.QComboBox(self.widget)
        self.seq_COMBOBOX.setGeometry(QtCore.QRect(170, 20, 71, 22))
        self.seq_COMBOBOX.setObjectName("seq_COMBOBOX")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.shot_LABEL.setText(QtGui.QApplication.translate("MainWindow", "Shot", None, QtGui.QApplication.UnicodeUTF8))
        self.seq_LABEL.setText(QtGui.QApplication.translate("MainWindow", "Sequence", None, QtGui.QApplication.UnicodeUTF8))
        self.project_LABEL.setText(QtGui.QApplication.translate("MainWindow", "Project", None, QtGui.QApplication.UnicodeUTF8))


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

