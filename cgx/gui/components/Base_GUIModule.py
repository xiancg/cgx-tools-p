# -*- coding: utf-8 -*-
'''
Base gui module to be used with QMainWindow.

Created on Mar 28, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.gui.components.Abstract_Classes import Abstract_GUIModule
import cgx.gui.components.Base_Components as bComp


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados", "Max Rocamora"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"
    

##------------------------------------------------------------------------------------------------
## Base_GUIModule
##------------------------------------------------------------------------------------------------
class Base_GUIModule(Abstract_GUIModule):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _parent, kwargsDict):
        '''
        Base GUI Module class creates a menu bar and a status bar component for QMainWindows.
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtGui.QMainWindow
        :param kwargsDict: {configDialog=QtGui.QDialog, helpDialog=QtGui.QDialog, aboutDialog=QtGui.QDialog}
        :type kwargsDict: dict
        '''
        self.parent = _parent
        self.kwargs = kwargsDict
        
        self.createComponents()
        self.setConnections()
        self.initTool()
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createComponents(self):
        '''
        Create all standard QWidgets.
        '''
        self.MainMenu_Component = bComp.MainMenu_Component(self.parent)
        self.StatusBar_Component = bComp.StatusBar_Component(self.parent)
        
    
    def setConnections(self):
        '''
        Connect signals.
        '''
        if 'initTool' in dir(self.parent):
            self.MainMenu_Component.edit_reset.triggered.connect(self.parent.resetTool)
        else:
            self.MainMenu_Component.edit_reset.triggered.connect(self.initTool)
        
        self.MainMenu_Component.edit_quit.triggered.connect(self.quitApp)
        self.MainMenu_Component.edit_config.triggered.connect(lambda: self.createDialog(self.kwargs['configDialog']))
        self.MainMenu_Component.help_help.triggered.connect(lambda: self.createDialog(self.kwargs['helpDialog']))
        self.MainMenu_Component.help_about.triggered.connect(lambda: self.createDialog(self.kwargs['aboutDialog']))
        self.StatusBar_Component.statusbar.messageChanged.connect(self.statusBar_style)
    
    
    def initTool(self):
        '''
        Updated status bar when everything is loaded.
        '''
        self.statusBar_update("Loaded",status=True)
    
    
    def quitApp(self):
        '''
        Close the parent GUI
        '''
        self.parent.close()
    
    
    def createDialog(self, _dialogClass):
        '''
        Opens the passed QDialog.
        :param _dialogClass: Dialog that implements the QtGui.QDialog class
        :type _dialogClass: QtGui.QDialog
        '''
        createdDialog = _dialogClass(self.parent)
        createdDialog.show()
    
    
    def statusBar_update(self, _message, _timeout= 5000, **kwargs):
        '''
        Updates the message on the UI status bar.
        :param _message: Message to display.
        :type _message: string
        :param _timeout: After this miliseconds statusbar will be cleared. Default: 5000
        :type _timeout: int
        :param _kwargs: status=True or warning=True. Override if you need more.
        :type _kwargs: dict
        '''
        if 'status' in kwargs.keys():
            if kwargs['status']:
                self.StatusBar_Component.statusbar.showMessage(_message, _timeout)
                self.StatusBar_Component.statusbar.setStyleSheet("background: rgba(172, 172, 172,100);")
        elif 'warning' in kwargs.keys():
            if kwargs['warning']: 
                self.StatusBar_Component.statusbar.showMessage(_message)
                self.StatusBar_Component.statusbar.setStyleSheet("background: rgba(255, 0, 0,100);")
        elif 'success' in kwargs.keys():
            if kwargs['success']:
                self.StatusBar_Component.statusbar.showMessage(_message)
                self.StatusBar_Component.statusbar.setStyleSheet("background: rgba(0, 255, 0,100);")
        elif 'alert' in kwargs.keys():
            if kwargs['alert']:
                self.StatusBar_Component.statusbar.showMessage(_message, _timeout)
                self.StatusBar_Component.statusbar.setStyleSheet("background: rgba(255, 255, 0,100);")
    
    
    def statusBar_style(self):
        '''
        Updates statusBar if no message is being displayed.
        '''
        if self.StatusBar_Component.statusbar.currentMessage() == "":
            self.StatusBar_Component.statusbar.setStyleSheet("background: rgba(0, 0, 0, 0);")
                
        
##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------

def main():
    pass
    

if __name__=="__main__":
    main()