# -*- coding: utf-8 -*-
'''
Pipeline gui componentes

Created on Mar 28, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.gui.Qt import QtWidgets, QtCore, QtGui
from cgx.gui.Qt import __binding__
import cgx.gui.DataViewModels as dvm
import os
from cgx.gui.components.Abstract_Classes import Abstract_Component


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados", "Max Rocamora"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##------------------------------------------------------------------------------------------------
## MinMax_Component
##------------------------------------------------------------------------------------------------
class MinMax_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent):
        '''
        MinMax GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        '''
        super(MinMax_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.container = self.createContainer()
        self.pos = [0, 0]
        self.size = [271, 51]
        self.createWidgets()
        self.setConnections()
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def setConnections(self):
        '''
        Connect signals.
        '''
        self.timeSlider_CHKBOX.stateChanged.connect(self.refreshUseTimeSlider)
        #STATUS
        self.timeSlider_CHKBOX.setChecked(True)
        
           
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.timeRangeMin_SPINBOX = QtWidgets.QSpinBox(self.container)
        self.timeRangeMin_SPINBOX.setGeometry(QtCore.QRect(10, 20, 51, 22))
        self.timeRangeMin_SPINBOX.setMinimum(-100000)
        self.timeRangeMin_SPINBOX.setMaximum(100000)
        self.timeRangeMin_SPINBOX.setProperty("value", 1)
        self.timeRangeMin_SPINBOX.setObjectName("timeRangeMin_SPINBOX")
        self.timeRangeMax_SPINBOX = QtWidgets.QSpinBox(self.container)
        self.timeRangeMax_SPINBOX.setGeometry(QtCore.QRect(80, 20, 51, 22))
        self.timeRangeMax_SPINBOX.setMinimum(-100000)
        self.timeRangeMax_SPINBOX.setMaximum(100000)
        self.timeRangeMax_SPINBOX.setProperty("value", 24)
        self.timeRangeMax_SPINBOX.setObjectName("timeRangeMax_SPINBOX")
        self.guion02_LABEL = QtWidgets.QLabel(self.container)
        self.guion02_LABEL.setGeometry(QtCore.QRect(60, 20, 21, 22))
        self.guion02_LABEL.setAlignment(QtCore.Qt.AlignCenter)
        self.guion02_LABEL.setObjectName("guion02_LABEL")
        self.relativeIn_LABEL = QtWidgets.QLabel(self.container)
        self.relativeIn_LABEL.setGeometry(QtCore.QRect(10, 0, 41, 22))
        self.relativeIn_LABEL.setObjectName("relativeIn_LABEL")
        self.relativeOut_LABEL = QtWidgets.QLabel(self.container)
        self.relativeOut_LABEL.setGeometry(QtCore.QRect(80, 0, 41, 22))
        self.relativeOut_LABEL.setObjectName("relativeOut_LABEL")
        self.timeSlider_CHKBOX = QtWidgets.QCheckBox(self.container)
        self.timeSlider_CHKBOX.setGeometry(QtCore.QRect(150, 20, 100, 22))
        self.timeSlider_CHKBOX.setObjectName("timeSlider_CHKBOX")
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
        
        
    def refreshUseTimeSlider(self):
        '''
        Refresh MinMax components. Disable them if Use time slider is checked.
        '''
        if self.timeSlider_CHKBOX.isChecked():
            self.timeRangeMax_SPINBOX.setEnabled(False)
            self.timeRangeMin_SPINBOX.setEnabled(False)
        else:
            self.timeRangeMax_SPINBOX.setEnabled(True)
            self.timeRangeMin_SPINBOX.setEnabled(True)
        
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.relativeIn_LABEL.setText(QtWidgets.QApplication.translate(className, "In", None, QtWidgets.QApplication.UnicodeUTF8))
            self.relativeOut_LABEL.setText(QtWidgets.QApplication.translate(className, "Out", None, QtWidgets.QApplication.UnicodeUTF8))
            self.timeSlider_CHKBOX.setText(QtWidgets.QApplication.translate(className, "Use time slider", None, QtWidgets.QApplication.UnicodeUTF8))
            self.guion02_LABEL.setText(QtWidgets.QApplication.translate(className, " -", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.relativeIn_LABEL.setText(QtWidgets.QApplication.translate(className, "In", None))
            self.relativeOut_LABEL.setText(QtWidgets.QApplication.translate(className, "Out", None))
            self.timeSlider_CHKBOX.setText(QtWidgets.QApplication.translate(className, "Use time slider", None))
            self.guion02_LABEL.setText(QtWidgets.QApplication.translate(className, " -", None))
        

##------------------------------------------------------------------------------------------------
## Handles_Component
##------------------------------------------------------------------------------------------------
class Handles_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent):
        '''
        Handles GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        '''
        super(Handles_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.container = self.createContainer()
        self.pos = [0,0]
        self.size = [161,51]
        self.createWidgets()
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.handleIn_LABEL = QtWidgets.QLabel(self.container)
        self.handleIn_LABEL.setGeometry(QtCore.QRect(10, 0, 51, 22))
        self.handleIn_LABEL.setObjectName("handleIn_LABEL")
        self.handleIn_SPINBOX = QtWidgets.QSpinBox(self.container)
        self.handleIn_SPINBOX.setGeometry(QtCore.QRect(10, 20, 51, 22))
        self.handleIn_SPINBOX.setMinimum(0)
        self.handleIn_SPINBOX.setMaximum(100)
        self.handleIn_SPINBOX.setProperty("value", 2)
        self.handleIn_SPINBOX.setObjectName("handleIn_SPINBOX")
        self.handleOut_LABEL = QtWidgets.QLabel(self.container)
        self.handleOut_LABEL.setGeometry(QtCore.QRect(80, 0, 61, 22))
        self.handleOut_LABEL.setObjectName("handleOut_LABEL")
        self.handleOut_SPINBOX = QtWidgets.QSpinBox(self.container)
        self.handleOut_SPINBOX.setGeometry(QtCore.QRect(80, 20, 51, 22))
        self.handleOut_SPINBOX.setMinimum(0)
        self.handleOut_SPINBOX.setMaximum(100)
        self.handleOut_SPINBOX.setProperty("value", 2)
        self.handleOut_SPINBOX.setObjectName("handleOut_SPINBOX")
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.handleIn_LABEL.setText(QtWidgets.QApplication.translate(className, "Handle In", None, QtWidgets.QApplication.UnicodeUTF8))
            self.handleOut_LABEL.setText(QtWidgets.QApplication.translate(className, "Handle Out", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.handleIn_LABEL.setText(QtWidgets.QApplication.translate(className, "Handle In", None))
            self.handleOut_LABEL.setText(QtWidgets.QApplication.translate(className, "Handle Out", None))


##------------------------------------------------------------------------------------------------
## SampleBy_Component
##------------------------------------------------------------------------------------------------
class SampleBy_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent):
        '''
        SampleBy GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        '''
        super(SampleBy_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.container = self.createContainer()
        self.pos = [0,0]
        self.size = [81,51]
        self.createWidgets()
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.sampleBy_LABEL = QtWidgets.QLabel(self.container)
        self.sampleBy_LABEL.setGeometry(QtCore.QRect(10, 0, 51, 22))
        self.sampleBy_LABEL.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.sampleBy_LABEL.setObjectName("sampleBy_LABEL")
        self.sampleBy_DBLSPINBOX = QtWidgets.QDoubleSpinBox(self.container)
        self.sampleBy_DBLSPINBOX.setGeometry(QtCore.QRect(10, 20, 51, 22))
        self.sampleBy_DBLSPINBOX.setMaximum(10000.99)
        self.sampleBy_DBLSPINBOX.setProperty("value", 1.0)
        self.sampleBy_DBLSPINBOX.setObjectName("sampleBy_DBLSPINBOX")
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.sampleBy_LABEL.setText(QtWidgets.QApplication.translate(className, "Sample by", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.sampleBy_LABEL.setText(QtWidgets.QApplication.translate(className, "Sample by", None))


##------------------------------------------------------------------------------------------------
## FPS_Component
##------------------------------------------------------------------------------------------------
class FPS_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent, _projectInfo):
        '''
        FPS GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        '''
        super(FPS_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.pInfo = _projectInfo
        self.container = self.createContainer()
        self.pos = [0,0]
        self.size = [60,22] 
        self.createWidgets()
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.fpsText_LABEL = QtWidgets.QLabel(self.container)
        self.fpsText_LABEL.setGeometry(QtCore.QRect(10, 0, 31, 22))
        self.fpsText_LABEL.setFont(font)
        self.fpsText_LABEL.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.fpsText_LABEL.setObjectName("fpsText_LABEL")
        self.fpsNumber_LABEL = QtWidgets.QLabel(self.container)
        self.fpsNumber_LABEL.setGeometry(QtCore.QRect(40, 0, 21, 22))
        self.fpsNumber_LABEL.setFont(font)
        self.fpsNumber_LABEL.setAlignment(QtCore.Qt.AlignBottom|QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft)
        self.fpsNumber_LABEL.setObjectName("fpsNumber_LABEL")
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
    
    
    def componentUpdate(self, _FPS):
        '''
        If given _FPS is different from project fps, set text color red. Default is black.
        '''
        correct = "color: rgb(0, 0, 0);"
        incorrect = "color: rgb(255, 0, 0);"
        if _FPS == self.pInfo.fps:
            self.fpsNumber_LABEL.setStyleSheet(correct)
            self.fpsText_LABEL.setStyleSheet(correct)
        else:
            self.fpsNumber_LABEL.setStyleSheet(incorrect)
            self.fpsText_LABEL.setStyleSheet(incorrect)
    
    
    def initComponent(self, _fps):
        self.fpsNumber_LABEL.setText(str(_fps))
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.fpsNumber_LABEL.setText(QtWidgets.QApplication.translate(className, "24", None, QtWidgets.QApplication.UnicodeUTF8))
            self.fpsText_LABEL.setText(QtWidgets.QApplication.translate(className, "FPS:", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.fpsNumber_LABEL.setText(QtWidgets.QApplication.translate(className, "24", None))
            self.fpsText_LABEL.setText(QtWidgets.QApplication.translate(className, "FPS:", None))


##------------------------------------------------------------------------------------------------
## OffsetStartFrame_Component
##------------------------------------------------------------------------------------------------
class OffsetStartFrame_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent):
        '''
        Offset Start Frame GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        '''
        super(OffsetStartFrame_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.container = self.createContainer()
        self.pos = [0,0]
        self.size = [211,41]
        self.createWidgets()
        self.setConnections()
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def setConnections(self):
        '''
        Connect signals.
        '''
        self.offsetStartFrame_CHKBOX.stateChanged.connect(self.refreshOffsetStartFrame)
        
    
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.offsetStartFrame_SPINBOX = QtWidgets.QSpinBox(self.container)
        self.offsetStartFrame_SPINBOX.setGeometry(QtCore.QRect(10, 10, 51, 22))
        self.offsetStartFrame_SPINBOX.setMinimum(0)
        self.offsetStartFrame_SPINBOX.setMaximum(100000)
        self.offsetStartFrame_SPINBOX.setEnabled(False)
        self.offsetStartFrame_SPINBOX.setProperty("value", 1001)
        self.offsetStartFrame_SPINBOX.setObjectName("offsetStartFrame_SPINBOX")
        self.offsetStartFrame_CHKBOX = QtWidgets.QCheckBox(self.container)
        self.offsetStartFrame_CHKBOX.setGeometry(QtCore.QRect(80, 10, 111, 22))
        self.offsetStartFrame_CHKBOX.setObjectName("offsetStartFrame_CHKBOX")
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
    
    
    def refreshOffsetStartFrame(self):
        '''
        Refresh Offset Start Frame if checked.
        '''
        if self.offsetStartFrame_CHKBOX.isChecked():
            self.offsetStartFrame_SPINBOX.setEnabled(True)
        else:
            self.offsetStartFrame_SPINBOX.setEnabled(False)
        
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.offsetStartFrame_CHKBOX.setText(QtWidgets.QApplication.translate(className, "Offset start frame", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.offsetStartFrame_CHKBOX.setText(QtWidgets.QApplication.translate(className, "Offset start frame", None))
        
 
##------------------------------------------------------------------------------------------------
## AssetTypes_Component
##------------------------------------------------------------------------------------------------
class AssetTypes_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent, _projectInfo):
        '''
        Project GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        '''
        super(AssetTypes_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.pInfo = _projectInfo
        self.container = self.createContainer()
        self.pos = [0, 0]
        self.size = [171, 51]
        self.createWidgets()
        self.initComponent()
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.assetType_COMBOBOX = QtWidgets.QComboBox(self.container)
        self.assetType_COMBOBOX.setGeometry(QtCore.QRect(10, 30, 151, 22))
        self.assetType_COMBOBOX.setObjectName("assetType_COMBOBOX")
        self.assetType_LABEL = QtWidgets.QLabel(self.container)
        self.assetType_LABEL.setGeometry(QtCore.QRect(10, 10, 71, 21))
        self.assetType_LABEL.setObjectName("assetType_LABEL")
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
    
    
    def initComponent(self):
        '''
        Initialize asset type component with ProjectInfo
        '''
        assetTypesData = self.pInfo.getAssetTypes()
        assetTypesModel = dvm.ObjectsListModel(assetTypesData)
        self.assetType_COMBOBOX.setModel(assetTypesModel)
        
    
    def setToItem(self, _item):
        '''
        Set asset type to given item if possible.
        '''
        dataList = self.assetType_COMBOBOX.model().dataList
        i = len(dataList)
        for x in range(i):
            if dataList[x] == _item:
                self.assetType_COMBOBOX.setCurrentIndex(x)
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.assetType_LABEL.setText(QtWidgets.QApplication.translate(className, "Asset type", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.assetType_LABEL.setText(QtWidgets.QApplication.translate(className, "Asset type", None))


##------------------------------------------------------------------------------------------------
## Assets_Component
##------------------------------------------------------------------------------------------------
class Assets_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent, _projectInfo):
        '''
        Project GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        '''
        super(Assets_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.pInfo = _projectInfo
        self.container = self.createContainer()
        self.pos = [0, 0]
        self.size = [171, 51]
        self.createWidgets()
        self.initComponent(self.pInfo.getAssetTypes()[0])
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.assetName_LABEL = QtWidgets.QLabel(self.container)
        self.assetName_LABEL.setGeometry(QtCore.QRect(10, 10, 71, 20))
        self.assetName_LABEL.setObjectName("assetName_LABEL")
        self.assetName_COMBOBOX = QtWidgets.QComboBox(self.container)
        self.assetName_COMBOBOX.setGeometry(QtCore.QRect(10, 30, 151, 22))
        self.assetName_COMBOBOX.setObjectName("assetName_COMBOBOX")
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
    
    
    def initComponent(self, _assetType):
        '''
        List all assets of given _assetType.
        :param _assetType: Asset type name
        :type _assetType: string
        '''
        assetsData = self.pInfo.getAssets(_assetType)
        assetsModel = dvm.ObjectsListModel([None])
        if assetsData != None:
            assetsModel = dvm.ObjectsListModel(assetsData)
        self.assetName_COMBOBOX.setModel(assetsModel)
    
    
    def setToItem(self, _item):
        '''
        Set asset to given item if possible.
        '''
        dataList = self.assetName_COMBOBOX.model().dataList
        i = len(dataList)
        for x in range(i):
            if dataList[x] == _item:
                self.assetName_COMBOBOX.setCurrentIndex(x)
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.assetName_LABEL.setText(QtWidgets.QApplication.translate(className, "Asset Name", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.assetName_LABEL.setText(QtWidgets.QApplication.translate(className, "Asset Name", None))


##------------------------------------------------------------------------------------------------
## SubAssets_Component
##------------------------------------------------------------------------------------------------
class SubAssets_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent, _projectInfo):
        '''
        SubAssets GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        '''
        super(SubAssets_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.pInfo = _projectInfo
        self.container = self.createContainer()
        self.pos = [0, 0]
        self.size = [171, 51]
        self.createWidgets()
        assetType = self.pInfo.getAssetTypes()
        if assetType != None:
            assetName = self.pInfo.getAssets(assetType[0])
            if assetName != None:
                self.initComponent(assetType, assetName[0])
            else:
                self.initComponent(None, None)
        else:
            self.initComponent(None, None)
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.subAssetName_LABEL = QtWidgets.QLabel(self.container)
        self.subAssetName_LABEL.setGeometry(QtCore.QRect(10, 10, 81, 22))
        self.subAssetName_COMBOBOX = QtWidgets.QComboBox(self.container)
        self.subAssetName_COMBOBOX.setGeometry(QtCore.QRect(10, 30, 151, 22))
        self.subAssetName_COMBOBOX.setObjectName("subAssetName_COMBOBOX")
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
    
    
    def initComponent(self, _assetType, _assetName):
        '''
        List all subassets of given _assetType and _assetName
        :param _assetType: Asset type name
        :type _assetType: string
        :param _assetType: Asset name
        :type _assetType: string
        '''

        subassetsData = self.pInfo.getSubAssets(_assetType, _assetName)
        subassetsModel = dvm.ObjectsListModel([None])
        if subassetsData != None:
            subassetsModel = dvm.ObjectsListModel([None] + subassetsData)
        
        self.subAssetName_COMBOBOX.setModel(subassetsModel)
    
    
    def setToItem(self, _item):
        '''
        Set subasset to given item if possible.
        '''
        dataList = self.subAssetName_COMBOBOX.model().dataList
        i = len(dataList)
        for x in range(i):
            if dataList[x] == _item:
                self.subAssetName_COMBOBOX.setCurrentIndex(x)
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.subAssetName_LABEL.setText(QtWidgets.QApplication.translate(className, "SubAsset Name", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.subAssetName_LABEL.setText(QtWidgets.QApplication.translate(className, "SubAsset Name", None))


##------------------------------------------------------------------------------------------------
## FileTypes_Component
##------------------------------------------------------------------------------------------------
class FileTypes_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent, _projectInfo):
        '''
        Project GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        '''
        super(FileTypes_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.pInfo = _projectInfo
        self.container = self.createContainer()
        self.pos = [0, 0]
        self.size = [171, 51]
        self.createWidgets()
        self.initComponent()
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.fileType_LABEL = QtWidgets.QLabel(self.container)
        self.fileType_LABEL.setGeometry(QtCore.QRect(10, 10, 121, 22))
        self.fileType_LABEL.setObjectName("fileType_LABEL")
        self.fileType_COMBOBOX = QtWidgets.QComboBox(self.container)
        self.fileType_COMBOBOX.setGeometry(QtCore.QRect(10, 30, 151, 22))
        self.fileType_COMBOBOX.setObjectName("fileType_COMBOBOX")
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
    
    
    def initComponent(self):
        '''
        Initialize component with all File Types suffixes.
        '''
        fileTypesDict = self.pInfo.getFileTypes()
        stringsList = []
        for pair in fileTypesDict.items():
            stringsList.append(pair[0] + ":" + pair[1])
        fileTypesModel = dvm.ObjectsListModel(sorted(stringsList))
        self.fileType_COMBOBOX.setModel(fileTypesModel)
        
    
    def setToItem(self, _item):
        '''
        Set file type to given item if possible.
        '''
        dataList = self.fileType_COMBOBOX.model().dataList
        i = len(dataList)
        for x in range(i):
            if dataList[x] == _item:
                self.fileType_COMBOBOX.setCurrentIndex(x)
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.fileType_LABEL.setText(QtWidgets.QApplication.translate(className, "File type", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.fileType_LABEL.setText(QtWidgets.QApplication.translate(className, "File type", None))
            

##------------------------------------------------------------------------------------------------
## Project_Component
##------------------------------------------------------------------------------------------------
class Project_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent, _studioInfo, _projectInfo, kwargsDict={}):
        '''
        Project GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        '''
        super(Project_Component, self).__init__(_mainWindow, _parent)
        
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.kwargs = kwargsDict
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        self.container = self.createContainer()
        self.pos = [0, 0]
        self.size = [171, 51]
        self.createWidgets()
        self.initComponent()
        self.setConnections()
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##-------------------------------------------------------------------------------------------- 
    def setConnections(self):
        '''
        Connect signals.
        '''
        self.project_COMBOBOX.currentIndexChanged.connect(self.changePInfo)
        
         
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.project_LABEL = QtWidgets.QLabel(self.container)
        self.project_LABEL.setGeometry(QtCore.QRect(10, 10, 46, 22))
        self.project_LABEL.setObjectName("project_LABEL")
        self.project_COMBOBOX = QtWidgets.QComboBox(self.container)
        self.project_COMBOBOX.setEnabled(True)
        self.project_COMBOBOX.setGeometry(QtCore.QRect(10, 30, 151, 22))
        self.project_COMBOBOX.setObjectName("project_COMBOBOX")
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
        
        
    def initComponent(self):
        '''
        Initialize component with all studio projects.
        '''
        projsData = self.sInfo.getProjects('','',exceptions=['FileManager','Temp','_hdri_backup'])
        if '_department' in self.kwargs.keys():
            if self.kwargs['_department'] == 'Comp':
                projsData = self.sInfo.getProjects('', 'Comp', _department='Comp')
        projectsModel = dvm.ObjectsListModel(projsData)
        self.project_COMBOBOX.setModel(projectsModel)
        self.changePInfo()
    
    
    def changePInfo(self):
        '''
        Sets ProjectInfo to currently selected project.
        '''
        selectedProj = str(self.project_COMBOBOX.currentText())
        self.pInfo.project = selectedProj
        
    
    def setToItem(self, _item):
        '''
        Set project to given item if possible.
        '''
        dataList = self.project_COMBOBOX.model().dataList
        i = len(dataList)
        for x in range(i):
            if dataList[x] == _item:
                self.pInfo.project = _item
                self.project_COMBOBOX.setCurrentIndex(x)
                break
                
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.project_LABEL.setText(QtWidgets.QApplication.translate(className, "Project", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.project_LABEL.setText(QtWidgets.QApplication.translate(className, "Project", None))

##------------------------------------------------------------------------------------------------
## Sequence_Component
##------------------------------------------------------------------------------------------------
class Sequence_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent, _studioInfo, _projectInfo, kwargsDict={}):
        '''
        Sequence GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        '''
        super(Sequence_Component, self).__init__(_mainWindow, _parent)
        
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.kwargs = kwargsDict
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        self.container = self.createContainer()
        self.pos = [0, 0]
        self.size = [91, 51]
        self.createWidgets()
        self.initComponent()
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.seq_LABEL = QtWidgets.QLabel(self.container)
        self.seq_LABEL.setGeometry(QtCore.QRect(10, 10, 55, 22))
        self.seq_LABEL.setObjectName("seq_LABEL")
        self.seq_COMBOBOX = QtWidgets.QComboBox(self.container)
        self.seq_COMBOBOX.setGeometry(QtCore.QRect(10, 30, 71, 22))
        self.seq_COMBOBOX.setObjectName("seq_COMBOBOX")
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
        
        
    def initComponent(self):
        '''
        Initialize component with all sequences for ProjectInfo project.
        '''
        seqsData = self.pInfo.getSequences(self.sInfo.getSequencesFrom())
        if '_department' in self.kwargs.keys():
            if self.kwargs['_department'] == 'Comp':
                seqsData = self.pInfo.getSequences(self.sInfo.getSequencesFrom(_department='Comp'),_department='Comp')
        seqsModel = dvm.ObjectsListModel(seqsData)
        self.seq_COMBOBOX.setModel(seqsModel)
        
    
    def setToItem(self, _item):
        '''
        Set sequence to given item if possible.
        '''
        dataList = self.seq_COMBOBOX.model().dataList
        i = len(dataList)
        for x in range(i):
            if dataList[x] == _item:
                self.seq_COMBOBOX.setCurrentIndex(x)
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.seq_LABEL.setText(QtWidgets.QApplication.translate(className, "Sequence", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.seq_LABEL.setText(QtWidgets.QApplication.translate(className, "Sequence", None))


##------------------------------------------------------------------------------------------------
## Shot_Component
##------------------------------------------------------------------------------------------------
class Shot_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent, _studioInfo, _projectInfo, kwargsDict={}):
        '''
        Sequence GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        '''
        super(Shot_Component, self).__init__(_mainWindow, _parent)
        
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.kwargs = kwargsDict
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        self.container = self.createContainer()
        self.pos = [0, 0]
        self.size = [91, 51]
        self.createWidgets()
        projSequences = self.pInfo.getSequences(self.sInfo.getSequencesFrom())
        if '_department' in self.kwargs.keys():
            if self.kwargs['_department'] == 'Comp':
                projSequences = self.pInfo.getSequences(self.sInfo.getSequencesFrom(_department='Comp'),_department='Comp')
        if projSequences != None:
            if len(projSequences) >= 1:
                self.initComponent(projSequences[0])
            else:
                self.initComponent(None)
        else:
            self.initComponent(None)
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.shot_LABEL = QtWidgets.QLabel(self.container)
        self.shot_LABEL.setGeometry(QtCore.QRect(10, 10, 31, 20))
        self.shot_LABEL.setObjectName("shot_LABEL")
        self.shot_COMBOBOX = QtWidgets.QComboBox(self.container)
        self.shot_COMBOBOX.setGeometry(QtCore.QRect(10, 30, 71, 22))
        self.shot_COMBOBOX.setObjectName("shot_COMBOBOX")
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
        
        
    def initComponent(self, _sequence):
        '''
        Initialize component with all shots for the given _sequence for ProjectInfo project.
        :param _sequence: Sequence name
        :type _sequence: string
        '''
        shotsData = self.pInfo.getShots(_sequence,self.sInfo.getSequencesFrom())
        if '_department' in self.kwargs.keys():
            if self.kwargs['_department'] == 'Comp':
                shotsData = self.pInfo.getShots(_sequence,self.sInfo.getSequencesFrom(_department='Comp'),_department='Comp')
        shotsModel = dvm.ObjectsListModel(shotsData)
        self.shot_COMBOBOX.setModel(shotsModel)
        
    
    def setToItem(self, _item):
        '''
        Set shot to given item if possible.
        '''
        dataList = self.shot_COMBOBOX.model().dataList
        i = len(dataList)
        for x in range(i):
            if dataList[x] == _item:
                self.shot_COMBOBOX.setCurrentIndex(x)
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.shot_LABEL.setText(QtWidgets.QApplication.translate(className, "Shot", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.shot_LABEL.setText(QtWidgets.QApplication.translate(className, "Shot", None))
            

##------------------------------------------------------------------------------------------------
## CustomPath_Component
##------------------------------------------------------------------------------------------------
class CustomPath_Component(Abstract_Component):
    Accepted = QtCore.Signal()
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent, _studioInfo):
        '''
        SampleBy GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        '''
        super(CustomPath_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.sInfo = _studioInfo
        self.container = self.createContainer()
        self.container
        self.pos = [0,0]
        self.size = [331,71]
        self.createWidgets()
        
        self.setConnections()
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def setConnections(self):
        '''
        Connect signals.
        '''
        self.customPath_CHKBOX.stateChanged.connect(self.refreshCustomPath)
        self.customPath_BTN.clicked.connect(self.browsePath)
    
    
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.customPath_LINEEDIT = QtWidgets.QLineEdit(self.container)
        self.customPath_LINEEDIT.setGeometry(QtCore.QRect(10, 30, 281, 22))
        self.customPath_LINEEDIT.setObjectName("customPath_LINEEDIT")
        self.customPath_LINEEDIT.setEnabled(False)
        self.customPath_BTN = QtWidgets.QPushButton(self.container)
        self.customPath_BTN.setGeometry(QtCore.QRect(300, 30, 22, 22))
        self.customPath_BTN.setObjectName("customPath_BTN")
        self.customPath_BTN.setEnabled(False)
        self.customPath_CHKBOX = QtWidgets.QCheckBox(self.container)
        self.customPath_CHKBOX.setGeometry(QtCore.QRect(10, 10, 141, 22))
        self.customPath_CHKBOX.setObjectName("customPath_CHKBOX")
        self.browseDialog = QtWidgets.QFileDialog(self.container)
        self.fileMode = self.browseDialog.FileMode.ExistingFile
        self.directory = self.sInfo.getProjectsFrom()
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
    
    
    def refreshCustomPath(self):
        '''
        Refresh component. If checkbox not checked disable them.
        '''
        if self.customPath_CHKBOX.isChecked():
            self.customPath_LINEEDIT.setEnabled(True)
            self.customPath_BTN.setEnabled(True)
        else:
            self.customPath_LINEEDIT.setEnabled(False)
            self.customPath_BTN.setEnabled(False)
           
            
    def browsePath(self):
        '''
        Create a Browsing Dialog to get file or directory paths.
        '''
        self.browseDialog.setFileMode(self.fileMode)
        if os.path.exists(self.directory):
            self.browseDialog.setDirectory(self.directory)
        dialogReturn = self.browseDialog.exec_()
        if dialogReturn == 1:
            selectedFiles = self.browseDialog.selectedFiles()
            self.customPath_LINEEDIT.setText(selectedFiles[0])
            self.Accepted.emit()
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.customPath_CHKBOX.setText(QtWidgets.QApplication.translate(className, "Use custom path", None, QtWidgets.QApplication.UnicodeUTF8))
            self.customPath_BTN.setText(QtWidgets.QApplication.translate(className, "...", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.customPath_CHKBOX.setText(QtWidgets.QApplication.translate(className, "Use custom path", None))
            self.customPath_BTN.setText(QtWidgets.QApplication.translate(className, "...", None))

##------------------------------------------------------------------------------------------------
## PipelineStages_Component
##------------------------------------------------------------------------------------------------
class PipelineStages_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent, _projectInfo):
        '''
        Pipeline Stages GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        '''
        super(PipelineStages_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.pInfo = _projectInfo
        self.container = self.createContainer()
        self.pos = [0, 0]
        self.size = [171, 51]
        self.createWidgets()
        self.initComponent()
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.pipelineStage_COMBOBOX = QtWidgets.QComboBox(self.container)
        self.pipelineStage_COMBOBOX.setGeometry(QtCore.QRect(10, 20, 151, 22))
        self.pipelineStage_COMBOBOX.setObjectName("pipelineStage_COMBOBOX")
        self.pipelineStage_LABEL = QtWidgets.QLabel(self.container)
        self.pipelineStage_LABEL.setGeometry(QtCore.QRect(10, 0, 81, 20))
        self.pipelineStage_LABEL.setObjectName("pipelineStage_LABEL")
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
    
    
    def initComponent(self):
        '''
        Initialize component with all Pipeline Stages.
        '''
        pipeStagesData = self.pInfo.getPipelineStages()
        pipeStagesModel = dvm.ObjectsListModel(pipeStagesData)
        self.pipelineStage_COMBOBOX.setModel(pipeStagesModel)
    
    
    def setToItem(self, _item):
        '''
        Set pipeline stage to given item if possible.
        '''
        dataList = self.pipelineStage_COMBOBOX.model().dataList
        i = len(dataList)
        for x in range(i):
            if dataList[x] == _item:
                self.pipelineStage_COMBOBOX.setCurrentIndex(x)
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.pipelineStage_LABEL.setText(QtWidgets.QApplication.translate(className, "Pipeline Stage", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.pipelineStage_LABEL.setText(QtWidgets.QApplication.translate(className, "Pipeline Stage", None))
            

##------------------------------------------------------------------------------------------------
## CacheFileTypes_Component
##------------------------------------------------------------------------------------------------
class CacheFileTypes_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent):
        '''
        Pipeline Stages GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        '''
        super(CacheFileTypes_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.container = self.createContainer()
        self.pos = [0, 0]
        self.size = [331, 61]
        self.createWidgets()
        self.initComponent()
   
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.fileType_LABEL = QtWidgets.QLabel(self.container)
        self.fileType_LABEL.setGeometry(QtCore.QRect(10, 10, 51, 16))
        self.fileType_LABEL.setFont(font)
        self.fileType_LABEL.setObjectName("fileType_LABEL")
        self.alembic_RBTN = QtWidgets.QRadioButton(self.container)
        self.alembic_RBTN.setGeometry(QtCore.QRect(110, 30, 82, 19))
        self.alembic_RBTN.setObjectName("alembic_RBTN")
        self.alembicExo_RBTN = QtWidgets.QRadioButton(self.container)
        self.alembicExo_RBTN.setGeometry(QtCore.QRect(190, 30, 111, 19))
        self.alembicExo_RBTN.setObjectName("alembicExo_RBTN")
        self.pointCache_RBTN = QtWidgets.QRadioButton(self.container)
        self.pointCache_RBTN.setGeometry(QtCore.QRect(10, 30, 82, 19))
        self.pointCache_RBTN.setObjectName("pointCache_RBTN")
        self.fileType_BTNGRP = QtWidgets.QButtonGroup(self.container)
        self.fileType_BTNGRP.setObjectName("fileType_BTNGRP")
        self.fileType_BTNGRP.addButton(self.alembicExo_RBTN)
        self.fileType_BTNGRP.addButton(self.alembic_RBTN)
        self.fileType_BTNGRP.addButton(self.pointCache_RBTN)
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
        
    
    def initComponent(self):
        '''
        Initialize component, setting status to Alembic Exocortex by default.
        '''
        self.alembicExo_RBTN.setChecked(True)
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.alembic_RBTN.setText(QtWidgets.QApplication.translate(className, "Alembic", None, QtWidgets.QApplication.UnicodeUTF8))
            self.alembicExo_RBTN.setText(QtWidgets.QApplication.translate(className, "Alembic Exocortex", None, QtWidgets.QApplication.UnicodeUTF8))
            self.fileType_LABEL.setText(QtWidgets.QApplication.translate(className, "File Type", None, QtWidgets.QApplication.UnicodeUTF8))
            self.pointCache_RBTN.setText(QtWidgets.QApplication.translate(className, "Point cache", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.alembic_RBTN.setText(QtWidgets.QApplication.translate(className, "Alembic", None))
            self.alembicExo_RBTN.setText(QtWidgets.QApplication.translate(className, "Alembic Exocortex", None))
            self.fileType_LABEL.setText(QtWidgets.QApplication.translate(className, "File Type", None))
            self.pointCache_RBTN.setText(QtWidgets.QApplication.translate(className, "Point cache", None))
            

##------------------------------------------------------------------------------------------------
## CacheBakingMethods_Component
##------------------------------------------------------------------------------------------------
class CacheBakingMethods_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent):
        '''
        Cache Baking Methods GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        '''
        super(CacheBakingMethods_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.container = self.createContainer()
        self.pos = [0, 0]
        self.size = [331, 61]
        self.createWidgets()
        self.initComponent()
   
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.bakingMethod_LABEL = QtWidgets.QLabel(self.container)
        self.bakingMethod_LABEL.setGeometry(QtCore.QRect(10, 10, 91, 16))
        self.bakingMethod_LABEL.setFont(font)
        self.bakingMethod_LABEL.setObjectName("bakingMethod_LABEL")
        self.vertexBake_RBTN = QtWidgets.QRadioButton(self.container)
        self.vertexBake_RBTN.setGeometry(QtCore.QRect(10, 30, 82, 19))
        self.vertexBake_RBTN.setObjectName("vertexBake_RBTN")
        self.alembicBake_RBTN = QtWidgets.QRadioButton(self.container)
        self.alembicBake_RBTN.setGeometry(QtCore.QRect(110, 30, 91, 19))
        self.alembicBake_RBTN.setObjectName("alembicBake_RBTN")
        self.bakingMethods_BTNGRP = QtWidgets.QButtonGroup(self.container)
        self.bakingMethods_BTNGRP.setObjectName("bakingMethos_BTNGRP")
        self.bakingMethods_BTNGRP.addButton(self.vertexBake_RBTN)
        self.bakingMethods_BTNGRP.addButton(self.alembicBake_RBTN)
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
        
    
    def initComponent(self):
        '''
        Initialize component, setting status to Vertex Bake by default.
        '''
        self.vertexBake_RBTN.setChecked(True)
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.vertexBake_RBTN.setText(QtWidgets.QApplication.translate(className, "Vertex bake", None, QtWidgets.QApplication.UnicodeUTF8))
            self.bakingMethod_LABEL.setText(QtWidgets.QApplication.translate(className, "Baking Method", None, QtWidgets.QApplication.UnicodeUTF8))
            self.alembicBake_RBTN.setText(QtWidgets.QApplication.translate(className, "Alembic bake", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.vertexBake_RBTN.setText(QtWidgets.QApplication.translate(className, "Vertex bake", None))
            self.bakingMethod_LABEL.setText(QtWidgets.QApplication.translate(className, "Baking Method", None))
            self.alembicBake_RBTN.setText(QtWidgets.QApplication.translate(className, "Alembic bake", None))
            

##------------------------------------------------------------------------------------------------
## Progress_Component
##------------------------------------------------------------------------------------------------
class Progress_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent):
        '''
        Progress Component Methods GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        '''
        super(Progress_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.container = self.createContainer()
        self.pos = [0, 0]
        self.size = [331, 71]
        self.createWidgets()
        self.__maximum = 100
        self.__minimum = 0
        self.__value = 0
   
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        font = QtGui.QFont()
        font.setWeight(75)
        font.setBold(True)
        self.progress_LABEL = QtWidgets.QLabel(self.container)
        self.progress_LABEL.setGeometry(QtCore.QRect(10, 0, 61, 22))
        self.progress_LABEL.setFont(font)
        self.progress_LABEL.setObjectName("progress_LABEL")
        self.progress_PBAR = QtWidgets.QProgressBar(self.container)
        self.progress_PBAR.setGeometry(QtCore.QRect(10, 30, 311, 31))
        self.progress_PBAR.setProperty("value", 0)
        self.progress_PBAR.setObjectName("progress_PBAR")
        self.progress_PBAR.setMinimum(0)
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
        
        
    def reset(self):
        '''
        Reset progress bar status to 0%.
        '''
        self.progress_PBAR.reset()
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.progress_LABEL.setText(QtWidgets.QApplication.translate(className, "Progress", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.progress_LABEL.setText(QtWidgets.QApplication.translate(className, "Progress", None))


    ##--------------------------------------------------------------------------------------------
    ##Properties
    ##--------------------------------------------------------------------------------------------      
    @property
    def maximum(self):
        return self.__maximum
    @maximum.setter
    def maximum(self,m):
        self.__maximum = m
        self.progress_PBAR.setMaximum(self.__maximum)
    
    @property
    def minimum(self):
        return self.__minimum
    @minimum.setter
    def minimum(self,m):
        self.__minimum = m
        self.progress_PBAR.setMinimum(self.__minimum)
    
    @property
    def value(self):
        return self.__value
    @value.setter
    def value(self,m):
        self.__value = m
        self.progress_PBAR.setValue(self.__value)
                
        
##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------

def main():
    pass
    

if __name__=="__main__":
    main()