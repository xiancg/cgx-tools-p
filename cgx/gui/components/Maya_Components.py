# -*- coding: utf-8 -*-
'''
Pipeline gui componentes

Created on Mar 28, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


# --------------------------------------------------------------------------------------------
# imports
# --------------------------------------------------------------------------------------------
from cgx.gui.Qt import QtWidgets, QtCore
from cgx.gui.Qt import __binding__
from cgx.gui.components.Abstract_Classes import Abstract_Component


# --------------------------------------------------------------------------------------------
# Metadata
# --------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados", "Max Rocamora"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


# ------------------------------------------------------------------------------------------------
# CameraFormats_Component
# ------------------------------------------------------------------------------------------------
class CameraFormats_Component(Abstract_Component):
    # --------------------------------------------------------------------------------------------
    # Constructor
    # --------------------------------------------------------------------------------------------
    def __init__(self, _mainWindow, _parent):
        '''
        Camera Formats GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        '''
        super(CameraFormats_Component, self).__init__(_mainWindow, _parent)
        self.mainWindow = _mainWindow
        self.parent = _parent
        self.container = self.createContainer()
        self.pos = [0, 0]
        self.size = [331, 61]
        self.createWidgets()
        print "esta cosa"

    # --------------------------------------------------------------------------------------------
    # Methods
    # --------------------------------------------------------------------------------------------
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.mb_CHKBOX = QtWidgets.QCheckBox(self.container)
        self.mb_CHKBOX.setGeometry(QtCore.QRect(150, 10, 121, 19))
        self.mb_CHKBOX.setObjectName("mb_CHKBOX")
        self.ma_CHKBOX = QtWidgets.QCheckBox(self.container)
        self.ma_CHKBOX.setGeometry(QtCore.QRect(10, 10, 111, 19))
        self.ma_CHKBOX.setObjectName("ma_CHKBOX")
        self.abc_CHKBOX = QtWidgets.QCheckBox(self.container)
        self.abc_CHKBOX.setGeometry(QtCore.QRect(150, 40, 111, 19))
        self.abc_CHKBOX.setObjectName("abc_CHKBOX")
        self.fbx_CHKBOX = QtWidgets.QCheckBox(self.container)
        self.fbx_CHKBOX.setGeometry(QtCore.QRect(10, 40, 121, 19))
        self.fbx_CHKBOX.setObjectName("fbx_CHKBOX")
        self.components = {'mb': self.mb_CHKBOX, 'ma': self.ma_CHKBOX, 'abc': self.abc_CHKBOX, 'fbx': self.fbx_CHKBOX}

        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)

    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className = str(self.mainWindow.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.mb_CHKBOX.setText(QtWidgets.QApplication.translate(className, ".mb (Maya Binary)", None, QtWidgets.QApplication.UnicodeUTF8))
            self.ma_CHKBOX.setText(QtWidgets.QApplication.translate(className, ".ma (Maya ASCII)", None, QtWidgets.QApplication.UnicodeUTF8))
            self.abc_CHKBOX.setText(QtWidgets.QApplication.translate(className, ".abc (Alembic)", None, QtWidgets.QApplication.UnicodeUTF8))
            self.fbx_CHKBOX.setText(QtWidgets.QApplication.translate(className, ".fbx (Autodesk FBX)", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.mb_CHKBOX.setText(QtWidgets.QApplication.translate(className, ".mb (Maya Binary)", None))
            self.ma_CHKBOX.setText(QtWidgets.QApplication.translate(className, ".ma (Maya ASCII)", None))
            self.abc_CHKBOX.setText(QtWidgets.QApplication.translate(className, ".abc (Alembic)", None))
            self.fbx_CHKBOX.setText(QtWidgets.QApplication.translate(className, ".fbx (Autodesk FBX)", None))


# --------------------------------------------------------------------------------------------
# Main
# --------------------------------------------------------------------------------------------

def main():
    pass


if __name__ == "__main__":
    main()
