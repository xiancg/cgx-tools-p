# -*- coding: utf-8 -*-
'''
GUI Modules Factories

Created on Mar 28, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.gui.components.Abstract_Classes import Abstract_GUIModulesFactory
import cgx.gui.components.Base_GUIModule as bMod
import cgx.gui.components.Pipeline_GUIModules as pMods


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados", "Max Rocamora"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##------------------------------------------------------------------------------------------------
## Base_GUIModulesFactory
##------------------------------------------------------------------------------------------------
class Base_GUIModulesFactory(Abstract_GUIModulesFactory):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _parent, **kwargs):
        '''
        Base GUI Modules Factory class. Creates all modules related to base functionality of QMainWindows.
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtGui.QMainWindow
        :param _kwargs: configDialog=QtGui.QDialog, helpDialog=QtGui.QDialog, aboutDialog=QtGui.QDialog
        :type _kwargs: dict
        '''
        self.parent = _parent
        self.kwargs = kwargs
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createModule(self, _module):
        '''
        Create modules of components.
        :param _module: Name of the module to be created
        :type _module: string
        :return: Created gui module. None if name is not recognized.
        :rtype: Abstract_GUIModule
        '''
        if _module == "base":
            mod = bMod.Base_GUIModule(self.parent, self.kwargs)
            return mod
        else:
            return None
        

##------------------------------------------------------------------------------------------------
## Pipeline_GUIModulesFactory
##------------------------------------------------------------------------------------------------
class Pipeline_GUIModulesFactory(Abstract_GUIModulesFactory):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _parent, _studioInfo, _projectInfo, **kwargs):
        '''
        Pipeline GUI Modules Factory class. Creates all modules related to pipeline tools functionality.
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtGui.QMainWindow
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param _kwargs: Keyword arguments if needed.
        :type _kwargs: dict
        '''
        self.parent = _parent
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        self.kwargsDict = kwargs
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createModule(self, _module):
        '''
        Create modules of components.
        :param _module: Name of the module to be created
        :type _module: string
        :return: Created gui module. None if name is not recognized.
        :rtype: Abstract_GUIModule
        '''
        if _module == "ProjSeqShot":
            mod = pMods.ProjSeqShot_GUIModule(self.parent, self.sInfo, self.pInfo, self.kwargsDict)
            return mod
        elif _module == "Assets":
            mod = pMods.Assets_GUIModule(self.parent, self.sInfo, self.pInfo, self.kwargsDict)
            return mod
        elif _module == "TimeRange":
            mod = pMods.TimeRange_GUIModule(self.parent, self.sInfo, self.pInfo, self.kwargsDict)
            return mod
        elif _module == "ProjAssets":
            mod = pMods.ProjAssets_GUIModule(self.parent, self.sInfo, self.pInfo, self.kwargsDict)
            return mod
        elif _module == "SeqShot":
            mod = pMods.SeqShot_GUIModule(self.parent, self.sInfo, self.pInfo, self.kwargsDict)
            return mod
        elif _module == "Cache":
            mod = pMods.Cache_GUIModule(self.parent, self.kwargsDict)
            return mod
        else:
            return None
                
        
##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------

def main():
    pass
    

if __name__=="__main__":
    main()