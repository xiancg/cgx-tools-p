# -*- coding: utf-8 -*-
'''
Abstract classes for gui factories, modules and components.

Created on Mar 28, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.gui.Qt import QtCore, QtWidgets
import abc

##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados", "Max Rocamora"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##------------------------------------------------------------------------------------------------
## Abstract_GUIModulesFactory
##------------------------------------------------------------------------------------------------
class Abstract_GUIModulesFactory(object):
    __metaclass__ = abc.ABCMeta
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _parent):
        '''
        Abstract GUI Modules Factory class.
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        '''
        self.parent = _parent
        
        self.createModule()
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    @abc.abstractmethod
    def createModule(self):
        '''
        Create modules of components
        '''
        pass


##------------------------------------------------------------------------------------------------
## Abstract_GUIModule
##------------------------------------------------------------------------------------------------
class Abstract_GUIModule(object):
    __metaclass__ = abc.ABCMeta
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _parent):
        '''
        Abstract GUI Module class. Creates, groups and connects components, giving them functionality.
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QMainWindow
        '''
        self.parent = _parent
        self.__pos = [0,0]
        self.__size = [0,0]
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createContainer(self):
        '''
        Create an empty QWidget to put all components inside of it.
        '''
        container = QtWidgets.QWidget(self.parent)
        container.setGeometry(QtCore.QRect(self.pos[0], self.pos[1], self.size[0], self.size[1]))
        container.setObjectName("widget")
        
        return container
    
    @abc.abstractmethod
    def createComponents(self):
        '''
        Create components for this GUI Module
        '''
        pass
    
    ##--------------------------------------------------------------------------------------------
    ##Properties
    ##--------------------------------------------------------------------------------------------      
    @property
    def pos(self):
        return self.__pos
    @pos.setter
    def pos(self,p):
        self.__pos = [p[0], p[1]]
        self.container.move(QtCore.QPoint(self.__pos[0],self.__pos[1]))
    
    @property
    def size(self):
        return self.__size
    @size.setter
    def size(self,s):
        self.__size = [s[0], s[1]]
        self.container.setFixedSize(self.__size[0], self.__size[1])



##------------------------------------------------------------------------------------------------
## Abstract_Component
##------------------------------------------------------------------------------------------------
class Abstract_Component(QtCore.QObject):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _mainWindow, _parent):
        '''
        Abstract GUI component class.
        :param _mainWindow: The top level widget, QMainWindow, which will contain this component
        :type _mainWindow: QtWidgets.QMainWindow
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QWidget
        '''
        super(Abstract_Component, self).__init__()
        self.parent = _parent
        self.mainWindow = _mainWindow
        self.__pos = [0,0]
        self.__size = [331,61]
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createContainer(self):
        '''
        Create an empty QWidget to put all QWidgets inside of it.
        '''
        container = QtWidgets.QWidget(self.parent)
        container.setGeometry(QtCore.QRect(self.pos[0], self.pos[1], self.size[0], self.size[1]))
        container.setObjectName("widget")
        
        return container
    
    
    def createWidgets(self):
        '''
        Create all QWidgets.
        '''
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.mainWindow)
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        pass
    
    
    ##--------------------------------------------------------------------------------------------
    ##Properties
    ##--------------------------------------------------------------------------------------------      
    @property
    def pos(self):
        return self.__pos
    @pos.setter
    def pos(self,p):
        self.__pos = [p[0], p[1]]
        self.container.move(QtCore.QPoint(self.__pos[0],self.__pos[1]))
    
    @property
    def size(self):
        return self.__size
    @size.setter
    def size(self,s):
        self.__size = [s[0], s[1]]
        self.container.setFixedSize(self.__size[0], self.__size[1])
                
        
##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------

def main():
    pass
    

if __name__=="__main__":
    main()