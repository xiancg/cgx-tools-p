# -*- coding: utf-8 -*-
'''
Pipeline gui modules.

Created on Mar 28, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.gui.components.Abstract_Classes import Abstract_GUIModule
import cgx.gui.components.Pipeline_Components as pComps


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados", "Max Rocamora"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"
    

##------------------------------------------------------------------------------------------------
## ProjSeqShot_GUIModule
##------------------------------------------------------------------------------------------------
class ProjSeqShot_GUIModule(Abstract_GUIModule):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _parent, _studioInfo, _projectInfo, kwargsDict={}):
        '''
        Project Sequence Shot GUI Module class. Creates, groups and connects components, giving them functionality.
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtGui.QMainWindow
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param kwargsDict: Keyword arguments if needed.
        :type kwargsDict: dict
        '''
        super(ProjSeqShot_GUIModule, self).__init__(_parent)
        self.parent = _parent
        self.kwargs = kwargsDict
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        self.container = self.createContainer()
        self.pos = [0,10]
        self.size = [331,51]
        
        self.createComponents()
        self.setConnections()
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------      
    def setConnections(self):
        '''
        Connect signals.
        '''
        self.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.Sequence_Component.initComponent)
        self.Sequence_Component.seq_COMBOBOX.currentIndexChanged.connect(self.init_Shot_Component)
        
            
    def createComponents(self):
        '''
        Create all QWidgets.
        '''
        self.Project_Component = pComps.Project_Component(self.parent, self.container, self.sInfo, self.pInfo, self.kwargs)
        self.Sequence_Component = pComps.Sequence_Component(self.parent, self.container, self.sInfo, self.pInfo, self.kwargs)
        self.Sequence_Component.pos = [160,0]
        self.Shot_Component = pComps.Shot_Component(self.parent, self.container, self.sInfo, self.pInfo, self.kwargs)
        self.Shot_Component.pos = [240,0]
        
    
    def init_Shot_Component(self):
        '''
        Initialize shot component with selected sequence.
        '''
        selectedSeq = str(self.Sequence_Component.seq_COMBOBOX.currentText())
        self.Shot_Component.initComponent(selectedSeq)


##------------------------------------------------------------------------------------------------
## Assets_GUIModule
##------------------------------------------------------------------------------------------------
class Assets_GUIModule(Abstract_GUIModule):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _parent, _studioInfo, _projectInfo, kwargsDict):
        '''
        Assets GUI Module class. Creates, groups and connects components, giving them functionality.
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtGui.QMainWindow
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param kwargsDict: Keyword arguments if needed.
        :type kwargsDict: dict
        '''
        super(Assets_GUIModule, self).__init__(_parent)
        
        self.parent = _parent
        self.kwargs = kwargsDict
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        
        self.container = self.createContainer()
        self.pos = [0,61]
        self.size = [331,101]
        
        self.createComponents()
        self.setConnections()
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------      
    def setConnections(self):
        '''
        Connect signals.
        '''
        self.AssetTypes_Component.assetType_COMBOBOX.currentIndexChanged.connect(self.init_Assets_Component)
        self.AssetTypes_Component.assetType_COMBOBOX.currentIndexChanged.connect(self.init_SubAssets_Component)
        self.Assets_Component.assetName_COMBOBOX.currentIndexChanged.connect(self.init_SubAssets_Component)
        
            
    def createComponents(self):
        '''
        Create all QWidgets.
        '''
        self.AssetTypes_Component = pComps.AssetTypes_Component(self.parent, self.container, self.pInfo)
        self.Assets_Component = pComps.Assets_Component(self.parent, self.container, self.pInfo)
        self.Assets_Component.pos = [0,50]
        self.SubAssets_Component = pComps.SubAssets_Component(self.parent, self.container, self.pInfo)
        self.SubAssets_Component.pos = [160,50]
        self.FileTypes_Component = pComps.FileTypes_Component(self.parent, self.container, self.pInfo)
        self.FileTypes_Component.pos = [160,0]
    
    
    def initComponents(self):
        '''
        Initialize asset types component.
        '''
        self.AssetTypes_Component.initComponent()
    
        
    def init_Assets_Component(self):
        '''
        Initialize assets component with selected asset type.
        '''
        selectedAssetType = str(self.AssetTypes_Component.assetType_COMBOBOX.currentText())
        self.Assets_Component.initComponent(selectedAssetType)
    
    
    def init_SubAssets_Component(self):
        '''
        Initialize subassets component with selected asset type and asset.
        '''
        selectedAssetType = str(self.AssetTypes_Component.assetType_COMBOBOX.currentText())
        selectedAsset = str(self.Assets_Component.assetName_COMBOBOX.currentText())
        self.SubAssets_Component.initComponent(selectedAssetType, selectedAsset)


##------------------------------------------------------------------------------------------------
## ProjAssets_GUIModule
##------------------------------------------------------------------------------------------------
class ProjAssets_GUIModule(Abstract_GUIModule):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _parent, _studioInfo, _projectInfo, kwargsDict):
        '''
        Project Assets GUI Module class. Creates, groups and connects components, giving them functionality.
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtGui.QMainWindow
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param kwargsDict: Keyword arguments if needed.
        :type kwargsDict: dict
        '''
        super(ProjAssets_GUIModule, self).__init__(_parent)
        
        self.parent = _parent
        self.kwargs = kwargsDict
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        
        self.container = self.createContainer()
        self.pos = [332,10]
        self.size = [331,151]
        
        self.createComponents()
        self.setConnections()
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------      
    def setConnections(self):
        '''
        Connect signals.
        '''
        self.Project_Component.project_COMBOBOX.currentIndexChanged.connect(self.initComponents)
        self.AssetTypes_Component.assetType_COMBOBOX.currentIndexChanged.connect(self.init_Assets_Component)
        self.AssetTypes_Component.assetType_COMBOBOX.currentIndexChanged.connect(self.init_SubAssets_Component)
        self.Assets_Component.assetName_COMBOBOX.currentIndexChanged.connect(self.init_SubAssets_Component)
        
            
    def createComponents(self):
        '''
        Create all QWidgets.
        '''
        self.Project_Component = pComps.Project_Component(self.parent, self.container, self.sInfo, self.pInfo)
        self.AssetTypes_Component = pComps.AssetTypes_Component(self.parent, self.container, self.pInfo)
        self.AssetTypes_Component.pos = [0,50]
        self.Assets_Component = pComps.Assets_Component(self.parent, self.container, self.pInfo)
        self.Assets_Component.pos = [0,100]
        self.SubAssets_Component = pComps.SubAssets_Component(self.parent, self.container, self.pInfo)
        self.SubAssets_Component.pos = [160,100]
        self.FileTypes_Component = pComps.FileTypes_Component(self.parent, self.container, self.pInfo)
        self.FileTypes_Component.pos = [160,50]
    
    
    def initComponents(self):
        '''
        Initialize asset types component.
        '''
        self.AssetTypes_Component.initComponent()
    
        
    def init_Assets_Component(self):
        '''
        Initialize assets component with selected asset type.
        '''
        selectedAssetType = str(self.AssetTypes_Component.assetType_COMBOBOX.currentText())
        self.Assets_Component.initComponent(selectedAssetType)
    
    def init_SubAssets_Component(self):
        '''
        Initialize subassets component with selected asset type and asset.
        '''
        selectedAssetType = str(self.AssetTypes_Component.assetType_COMBOBOX.currentText())
        selectedAsset = str(self.Assets_Component.assetName_COMBOBOX.currentText())
        self.SubAssets_Component.initComponent(selectedAssetType, selectedAsset)


##------------------------------------------------------------------------------------------------
## SeqShot_GUIModule
##------------------------------------------------------------------------------------------------
class SeqShot_GUIModule(Abstract_GUIModule):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _parent, _studioInfo, _projectInfo, kwargsDict):
        '''
        Sequence Shot GUI Module class. Creates, groups and connects components, giving them functionality.
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtGui.QMainWindow
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param kwargsDict: Keyword arguments if needed.
        :type kwargsDict: dict
        '''
        super(SeqShot_GUIModule, self).__init__(_parent)
        self.parent = _parent
        self.kwargs = kwargsDict
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        self.container = self.createContainer()
        self.pos = [332,161]
        self.size = [171,51]
        
        self.createComponents()
        self.setConnections()
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------      
    def setConnections(self):
        '''
        Connect signals.
        '''
        self.Sequence_Component.seq_COMBOBOX.currentIndexChanged.connect(self.init_Shot_Component)
        
            
    def createComponents(self):
        '''
        Create all QWidgets.
        '''
        self.Sequence_Component = pComps.Sequence_Component(self.parent, self.container, self.sInfo, self.pInfo)
        self.Shot_Component = pComps.Shot_Component(self.parent, self.container, self.sInfo, self.pInfo)
        self.Shot_Component.pos = [81,0]
        
        
    def initComponents(self):
        self.Sequence_Component.initComponent()
        
    
    def init_Shot_Component(self):
        '''
        Initialize shot component with selected sequence.
        '''
        selectedSeq = str(self.Sequence_Component.seq_COMBOBOX.currentText())
        self.Shot_Component.initComponent(selectedSeq)


##------------------------------------------------------------------------------------------------
## Cache_GUIModule
##------------------------------------------------------------------------------------------------
class Cache_GUIModule(Abstract_GUIModule):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _parent, kwargsDict):
        '''
        Cache GUI Module class. Creates, groups and connects components, giving them functionality.
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtGui.QMainWindow
        :param kwargsDict: Keyword arguments if needed.
        :type kwargsDict: dict
        '''
        super(Cache_GUIModule, self).__init__(_parent)
        self.parent = _parent
        self.kwargs = kwargsDict
        self.container = self.createContainer()
        self.pos = [0,385]
        self.size = [331,101]
        
        self.createComponents()
        self.setConnections()
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------      
    def setConnections(self):
        '''
        Connect signals.
        '''
        self.CacheFileTypes_Component.fileType_BTNGRP.buttonClicked.connect(self.refreshFileType)
        self.CacheBakingMethods_Component.bakingMethods_BTNGRP.buttonClicked.connect(self.refreshBakingMethod)
        
            
    def createComponents(self):
        '''
        Create all QWidgets.
        '''
        self.CacheFileTypes_Component = pComps.CacheFileTypes_Component(self.parent, self.container)
        self.CacheBakingMethods_Component = pComps.CacheBakingMethods_Component(self.parent, self.container)
        self.CacheBakingMethods_Component.pos = [0,51]
    
    
    def refreshFileType(self):
        '''
        Refresh baking methods component depending on File Type choice. Avoids alembic bake and point cache being selected at the same time.
        '''
        if self.CacheFileTypes_Component.pointCache_RBTN.isChecked():
            self.CacheBakingMethods_Component.vertexBake_RBTN.setChecked(True)
            self.CacheBakingMethods_Component.alembicBake_RBTN.setChecked(False)
            self.CacheBakingMethods_Component.alembicBake_RBTN.setEnabled(False)
        elif self.CacheFileTypes_Component.alembic_RBTN.isChecked():
            self.CacheBakingMethods_Component.alembicBake_RBTN.setEnabled(True)
        elif self.CacheFileTypes_Component.alembicExo_RBTN.isChecked():
            self.CacheBakingMethods_Component.alembicBake_RBTN.setEnabled(True)
            self.CacheBakingMethods_Component.vertexBake_RBTN.setChecked(True)
            self.CacheBakingMethods_Component.alembicBake_RBTN.setChecked(False)
    
    
    def refreshBakingMethod(self):
        '''
        Refresh File Type component depending on baking method choice. Avoids alembic bake and point cache being selected at the same time.
        '''
        if self.CacheBakingMethods_Component.alembicBake_RBTN.isChecked():
            if not self.CacheFileTypes_Component.alembicExo_RBTN.isChecked():
                self.CacheFileTypes_Component.pointCache_RBTN.setChecked(False)
                self.CacheFileTypes_Component.setChecked(True) 


##------------------------------------------------------------------------------------------------
## TimeRange_GUIModule
##------------------------------------------------------------------------------------------------
class TimeRange_GUIModule(Abstract_GUIModule):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _parent, _studioInfo, _projectInfo, kwargsDict):
        '''
        Time Range Module class. Creates, groups and connects components, giving them functionality.
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtGui.QMainWindow
        :param _studioInfo: StudioInfo object to manage all studio level properties.
        :type _studioInfo: core.StudioInfo
        :param _projectInfo: ProjectInfo object to manage all project level properties.
        :type _projectInfo: core.ProjectInfo
        :param kwargsDict: {tRange:Abstract_TimeRange}
        :type kwargsDict: dict
        '''
        super(TimeRange_GUIModule, self).__init__(_parent)
        self.parent = _parent
        self.kwargs = kwargsDict
        self.sInfo = _studioInfo
        self.pInfo = _projectInfo
        self.tRange = self.kwargs['tRange']
        
        self.container = self.createContainer()
        self.pos = [0,171]
        self.size = [331,141]
        
        self.createComponents()
        self.setConnections()
        self.initComponents()
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------      
    def setConnections(self):
        '''
        Connect signals.
        '''
        self.MinMax_Component.timeSlider_CHKBOX.stateChanged.connect(self.refreshTimeRangeOptions)
        self.MinMax_Component.timeRangeMin_SPINBOX.valueChanged.connect(self.refreshTimeRangeObject)
        self.MinMax_Component.timeRangeMax_SPINBOX.valueChanged.connect(self.refreshTimeRangeObject)
        self.Handles_Component.handleIn_SPINBOX.valueChanged.connect(self.refreshTimeRangeObject)
        self.Handles_Component.handleOut_SPINBOX.valueChanged.connect(self.refreshTimeRangeObject)
        
            
    def createComponents(self):
        '''
        Create all QWidgets.
        '''
        self.MinMax_Component = pComps.MinMax_Component(self.parent, self.container)
        self.Handles_Component = pComps.Handles_Component(self.parent, self.container)
        self.Handles_Component.pos = [0,51]
        self.SampleBy_Component = pComps.SampleBy_Component(self.parent, self.container)
        self.SampleBy_Component.pos = [140,51]
        self.FPS_Component = pComps.FPS_Component(self.parent, self.container, self.pInfo)
        self.FPS_Component.pos = [260,15]
        self.OffsetStartFrame_Component = pComps.OffsetStartFrame_Component(self.parent, self.container)
        self.OffsetStartFrame_Component.pos = [70,100]
    
    
    def initComponents(self):
        '''
        Initialize all components with Time Range object info.
        '''
        self.MinMax_Component.timeRangeMin_SPINBOX.setValue(self.tRange.startFrame)
        self.MinMax_Component.timeRangeMax_SPINBOX.setValue(self.tRange.endFrame)
        self.Handles_Component.handleIn_SPINBOX.setValue(self.tRange.handleIn)
        self.Handles_Component.handleOut_SPINBOX.setValue(self.tRange.handleOut)
    
    
    def refreshTimeRangeOptions(self):
        '''
        Refresh MinMax components if Use time slider is selected.
        '''
        if self.MinMax_Component.timeSlider_CHKBOX.isChecked():
            self.tRange.setFramesToTimeSlider()
            self.MinMax_Component.timeRangeMin_SPINBOX.setValue(self.tRange.startFrame)
            self.MinMax_Component.timeRangeMax_SPINBOX.setValue(self.tRange.endFrame)
    
            
    def refreshTimeRangeObject(self):
        '''
        Refresh Time Range object if Use time slider is selected.
        '''
        if self.MinMax_Component.timeSlider_CHKBOX.isChecked():
            self.refreshTimeRangeOptions()
        else:
            self.tRange.startFrame = self.MinMax_Component.timeRangeMin_SPINBOX.value()
            self.tRange.endFrame = self.MinMax_Component.timeRangeMax_SPINBOX.value()
        self.tRange.handleIn = self.Handles_Component.handleIn_SPINBOX.value()
        self.tRange.handleOut = self.Handles_Component.handleOut_SPINBOX.value()
                
        
##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------

def main():
    pass
    

if __name__=="__main__":
    main()