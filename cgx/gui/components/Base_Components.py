# -*- coding: utf-8 -*-
'''
Base gui componentes to be used with QMainWindow.

Created on Mar 28, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.gui.Qt import QtWidgets, QtCore
from cgx.gui.Qt import __binding__
from cgx.gui.components.Abstract_Classes import Abstract_Component

##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados", "Max Rocamora"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"
    

##------------------------------------------------------------------------------------------------
## MainMenu_Component
##------------------------------------------------------------------------------------------------
class MainMenu_Component(Abstract_Component):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _parent):
        '''
        Main Menu components class creates a menu bar with Config, Reset, Quit, Help and About options.
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QMainWindow
        '''
        self.parent = _parent
        
        self.createWidgets()
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create QWidgets.
        '''
        self.menubar = QtWidgets.QMenuBar(self.parent)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 452, 18))
        self.menubar.setObjectName("menubar")
        self.menuEdit = QtWidgets.QMenu(self.menubar)
        self.menuEdit.setObjectName("menuEdit")
        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        self.parent.setMenuBar(self.menubar)
        self.edit_reset = QtWidgets.QAction(self.parent)
        self.edit_reset.setObjectName("edit_reset")
        self.edit_config = QtWidgets.QAction(self.parent)
        self.edit_config.setObjectName("edit_config")
        self.edit_quit = QtWidgets.QAction(self.parent)
        self.edit_quit.setObjectName("edit_quit")
        self.help_help = QtWidgets.QAction(self.parent)
        self.help_help.setObjectName("help_help")
        self.help_about = QtWidgets.QAction(self.parent)
        self.help_about.setObjectName("help_about")
        self.menuEdit.addAction(self.edit_reset)
        self.menuEdit.addAction(self.edit_config)
        self.menuEdit.addSeparator()
        self.menuEdit.addAction(self.edit_quit)
        self.menuHelp.addAction(self.help_help)
        self.menuHelp.addAction(self.help_about)
        self.menubar.addAction(self.menuEdit.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.parent)
        
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        className= str(self.parent.__class__.__name__)
        if __binding__ in ('PySide', 'PyQt4'):
            self.menuEdit.setTitle(QtWidgets.QApplication.translate(className, "Edit", None, QtWidgets.QApplication.UnicodeUTF8))
            self.menuHelp.setTitle(QtWidgets.QApplication.translate(className, "Help", None, QtWidgets.QApplication.UnicodeUTF8))
            self.edit_reset.setText(QtWidgets.QApplication.translate(className, "Reset Tool", None, QtWidgets.QApplication.UnicodeUTF8))
            self.edit_config.setText(QtWidgets.QApplication.translate(className, "Configuration", None, QtWidgets.QApplication.UnicodeUTF8))
            self.edit_quit.setText(QtWidgets.QApplication.translate(className, "Quit", None, QtWidgets.QApplication.UnicodeUTF8))
            self.help_help.setText(QtWidgets.QApplication.translate(className, "Help", None, QtWidgets.QApplication.UnicodeUTF8))
            self.help_about.setText(QtWidgets.QApplication.translate(className, "About", None, QtWidgets.QApplication.UnicodeUTF8))
        elif __binding__ in ('PySide2', 'PyQt5'):
            self.menuEdit.setTitle(QtWidgets.QApplication.translate(className, "Edit", None))
            self.menuHelp.setTitle(QtWidgets.QApplication.translate(className, "Help", None))
            self.edit_reset.setText(QtWidgets.QApplication.translate(className, "Reset Tool", None))
            self.edit_config.setText(QtWidgets.QApplication.translate(className, "Configuration", None))
            self.edit_quit.setText(QtWidgets.QApplication.translate(className, "Quit", None))
            self.help_help.setText(QtWidgets.QApplication.translate(className, "Help", None))
            self.help_about.setText(QtWidgets.QApplication.translate(className, "About", None))
        
        
##------------------------------------------------------------------------------------------------
## StatusBar_Component
##------------------------------------------------------------------------------------------------
class StatusBar_Component(object):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------       
    def __init__(self, _parent):
        '''
        Status bar components class creates a status bar.
        :param _parent: All QWidgets declared here will be parented to this QWidget.
        :type _parent: QtWidgets.QMainWindow
        '''
        self.parent = _parent
        
        self.createWidgets()
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------       
    def createWidgets(self):
        '''
        Create QWidgets.
        '''
        self.statusbar = QtWidgets.QStatusBar(self.parent)
        self.statusbar.setObjectName("statusbar")
        self.parent.setStatusBar(self.statusbar)
        
        self.retranslateUi()
        QtCore.QMetaObject.connectSlotsByName(self.parent)
    
    
    def retranslateUi(self):
        '''
        Set properties for QWidgets
        '''
        pass
                
        
##--------------------------------------------------------------------------------------------
## Main
##--------------------------------------------------------------------------------------------

def main():
    pass
    

if __name__=="__main__":
    main()