# -*- coding: utf-8 -*-
'''
Library to use naming conventions from a Json file.

Created on Apr 1, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.core.JSONManager import JSONManager
import cgx.core

##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Naming conventions object
##--------------------------------------------------------------------------------------------
class NamingConventions(object):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _studioNamingConventions=cgx.core.STUDIO_NAMINGCONVENTIONS):
        '''
        Library to manage studio info.
        :param _studioNamingConventions: Absolute path to the studio_namingconventions.json file
        :type _studioNamingConventions: string
        '''
        self.__studioNaming = JSONManager(_studioNamingConventions)
        self.__wildcards = ["*","#","$"]
        self.__placeholders = ["EXT","$asset","$seq","$shot","$side"]
    
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def getNaming(self, _naming, _namingCategory, **kwargs):
        '''
        Retrieves the given naming convention for the studio.
        :param _naming: The naming convention needed
        :type _naming: string
        :param _namingCategory: The category under which _naming lives inside the JSON file
        :type _namingCategory: string
        :param kwargs: wildcards=False, placeholders=False If any of these is passed, it'll be filtered from the naming convention.
        :type kwargs: key=boolean
        '''
        dirtValue = ""
        parentNode= self.studioNaming.getName(_namingCategory, False)[0]
        nodeAsDict = parentNode.asDict()
        if _naming in nodeAsDict.keys():
            dirtValue= nodeAsDict[_naming]
        
        if dirtValue != "":
            if 'wildcards' in kwargs.keys():
                if not kwargs['wildcards']:
                    for each in self.wildcards:
                        dirtValue = dirtValue.replace(each,"")
            elif 'placeholders' in kwargs.keys():
                if not kwargs['placeholders']:
                    for each in self.placeholders:
                        dirtValue = dirtValue.replace(each,"")
            return dirtValue
        else:
            return None
        
    
    ##--------------------------------------------------------------------------------------------
    ##Properties
    ##--------------------------------------------------------------------------------------------
    @property
    def studioNaming(self):
        return self.__studioNaming
    
    @property
    def wildcards(self):
        return self.__wildcards
    @wildcards.setter
    def wildcards(self,w):
        self.__wildcards = w
    
    @property
    def placeholders(self):
        return self.__placeholders
    @placeholders.setter
    def placeholders(self,w):
        self.__placeholders = w

##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass

if __name__=="__main__":
    main()