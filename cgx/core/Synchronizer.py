# -*- coding: utf-8 -*-
'''
Library to copy and compare files and directories.

Created on Mar 25, 2015

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''


# --------------------------------------------------------------------------------------------
# imports
# --------------------------------------------------------------------------------------------
import os
import time
import shutil


# --------------------------------------------------------------------------------------------
# Metadata
# --------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


# --------------------------------------------------------------------------------------------
# Class: Synchronizer
# --------------------------------------------------------------------------------------------
class Synchronizer():
    # --------------------------------------------------------------------------------------------
    # Constructor
    # --------------------------------------------------------------------------------------------
    def __init__(self, _parentUI=None):
        '''
        Compares given files or directories. Copies files, textures, sequences.
        :param _parentUI: QtGui.QWidget that uses this tool instance. Default=None
        :type _parentUI: QtGui.QWidget
        '''
        self.parentUI = _parentUI
        self.__statusDict = {1: "In sync",
                             2: "Out of sync",
                             3: "Same date, different size",
                             4: "Different date, same size",
                             5: "Both paths missing",
                             6: "Source file missing",
                             7: "Target file missing",
                             8: "Different kind of path (file-file, dir-dir)"}
        self.__log = []

    # --------------------------------------------------------------------------------------------
    # Methods
    # --------------------------------------------------------------------------------------------
    def getSyncStatus(self, _srcPath, _trgPath):
        '''
        Compares two given files and returns a status
        :param _srcPath: Path to a file or directory
        :type _srcPath: string
        :param _trgPath: Path to a file or directory
        :type _trgPath: string
        :return: Result of comparing both files, including if something was not found.
        :rtype: string
        '''
        if os.path.exists(_srcPath) and os.path.exists(_trgPath):
            srcTime = time.ctime(os.path.getmtime(_srcPath))
            trgTime = time.ctime(os.path.getmtime(_trgPath))
            srcSize = os.path.getsize(_srcPath)
            trgSize = os.path.getsize(_trgPath)
            if os.path.isdir(_srcPath) and os.path.isdir(_trgPath):
                srcSize = self.getDirSize(_srcPath)
                trgSize = self.getDirSize(_trgPath)
            # In sync
            if srcTime == trgTime and srcSize == trgSize:
                return self.statusDict[1]
            # Out of sync
            elif srcTime != trgTime and srcSize != trgSize:
                return self.statusDict[2]
            # Same date, different size
            elif srcTime == trgTime and srcSize != trgSize:
                return self.statusDict[3]
            # Different date, same size
            elif srcTime != trgTime and srcSize == trgSize:
                return self.statusDict[4]
        # Both paths missing
        elif not os.path.exists(_srcPath) and not os.path.exists(_trgPath):
            return self.statusDict[5]
        # Source path missing
        elif not os.path.exists(_srcPath):
            return self.statusDict[6]
        # Target path missing
        elif not os.path.exists(_trgPath):
            return self.statusDict[7]
        # Different kind of path (file-file, dir-dir)
        elif os.path.isdir(_srcPath) or os.path.isdir(_trgPath):
            return self.statusDict[8]
        # Else
        else:
            return "Failed to find status."

    def getDirSize(self, _path):
        '''
        Get the total size of files and sub-directories for given path.
        :param _path: Path to a directory
        :type _path: string
        :return: Sum of file and sub-directories sizes
        :rtype: float. None if _path is not a directory.
        '''
        if os.path.isdir(_path):
            total_size = 0
            for dirpath, dirnames, filenames in os.walk(_path):
                for f in filenames:
                    fp = os.path.join(dirpath, f)
                    total_size += os.path.getsize(fp)
            return total_size
        else:
            return None

    def getSequenceFiles(self, _parentFolder, _fileWithExt):
        '''
        Given a folder a file name, find all sibling files for the sequence.
        :param _parentFolder: Path to a directory. Ex= X:/Files/render
        :type _parentFolder: string
        :param _fileWithExt: File name with extension included. Ex= RND_beauty.0001.exr
        :type _fileWithExt: string
        :return: List of files (full paths) with the same name pattern in the given directory
        :rtype: list
        '''
        # Split extension
        fileName, fileExt = _fileWithExt.rsplit(".", 1)
        # Get number of digits
        digitsNumber = 0
        for each in fileName[::-1]:
            if each.isdigit() or each == '#':
                digitsNumber += 1
            else:
                break
        namePattern = fileName[:-digitsNumber]
        hashStr = "#" * digitsNumber
        self.log.append("Name pattern for given sequence is: {}{}.{}".format(namePattern, hashStr, fileExt))
        # Build list of files with the same name structure in the directory
        filesAndDirs = os.listdir(_parentFolder)
        preFileList = []
        for each in filesAndDirs:
            if os.path.isfile(os.path.join(_parentFolder, each)):
                preFileList.append(each)
        fileList = []
        for each in preFileList:
            thisFileExt = each.rsplit(".", 1)[1]
            if each[:len(namePattern)] == namePattern and thisFileExt == fileExt:
                fileList.append(_parentFolder + "/" + each)

        return fileList

    def processPaths(self, _srcPath, _trgPath, **kwargs):
        '''
        Copies a file, texture (with .tx), file sequence (with .tx) or directory from _srcPath to _trgPath
        Sequence requires passing the full path to one file from the sequence for both source and target.
        Use only one kwarg at a time.
        Source and target must be same type (files or directories).
        :param _srcPath: Full path to a file or directory
        :type _srcPath: string
        :param _trgPath: Full path to a desired file or directory final location
        :type _trgPath: string
        :param kwargs: isTexture=True or isSequence=True
        :type kwargs: dict
        :return: True if successful.
        :rtype: boolean
        '''
        # Change backslash for slash
        if "\\" in _srcPath:
            _srcPath = _srcPath.replace("\\", "/")
        if "\\" in _trgPath:
            _trgPath = _trgPath.replace("\\", "/")

        # Check if source path is dir or file
        passedDirs = False
        if os.path.isdir(_srcPath):
            passedDirs = True

        # Sanity check
        if _srcPath == _trgPath:
            self.log.append("Source and target are the same, it will be skipped: {}".format(_srcPath))
            return False
        elif not os.path.exists(_srcPath):
            self.log.append("Source file doesn't exist, it will be skipped: {}".format(_srcPath))
            return False
        elif passedDirs:
            result = self.__processDirs(_srcPath, _trgPath)
            self.log.append("Finished processing directories: {} and {}".format(_srcPath, _trgPath))
            return result
        else:
            result = self.__processFiles(_srcPath, _trgPath, kwargs)
            self.log.append("Finished processing files: {} and {}".format(_srcPath, _trgPath))
            return result

    def __processDirs(self, _srcPath, _trgPath):
        '''
        Copies a directory from _srcPath to _trgPath
        :param _srcPath: Full path to a file
        :type _srcPath: string
        :param _trgPath: Full path to a desired file final location
        :type _trgPath: string
        :return: True if successful.
        :rtype: boolean
        '''
        # Create folder structure
        if not os.path.exists(_trgPath):
            # Copy file
            try:
                shutil.copytree(_srcPath, _trgPath)
                self.log.append("Copied {} to {}".format(_srcPath, _trgPath))
            except IOError:
                self.log.append("The specified source doesn't exist: {}".format(_srcPath))
            return True
        else:
            self.log.append("The specified target directory must not exist: {}".format(_trgPath))
            return False

    def __processFiles(self, _srcPath, _trgPath, kwargs):
        '''
        Copies a file, texture (with .tx), file sequence (with .tx) from _srcPath to _trgPath
        Sequence requires passing the full path to one file from the sequence.
        Use only on kwarg at a time.
        :param _srcPath: Full path to a file
        :type _srcPath: string
        :param _trgPath: Full path to a desired file final location
        :type _trgPath: string
        :param kwargs: isTexture=True or isSequence=True
        :type kwargs: dict
        :return: True if successful.
        :rtype: boolean
        '''
        pathParts = _trgPath.rsplit("/", 1)
        finalFolder = pathParts[0]

        if "isTexture" in kwargs.keys():
            if kwargs["isTexture"]:
                # Create folder structure
                if not os.path.exists(finalFolder):
                    try:
                        os.makedirs(finalFolder)
                        self.log.append("Directory structure created: {}".format(finalFolder))
                    except WindowsError:
                        self.log.append("Directory structure couldn't be created: {}".format(finalFolder))
                        return False
                else:
                    self.log.append("Directory structure already existed: {}".format(finalFolder))
                # Copy file
                try:
                    shutil.copy2(_srcPath, finalFolder)
                    self.log.append("Copied {} to {}".format(_srcPath, finalFolder))
                except IOError:
                    self.log.append("The specified source doesn't exist: {}".format(_srcPath))
                    return False
                #Copy .tx
                txPath = _srcPath.rsplit(".", 1)[0] + ".tx"
                if os.path.exists(txPath):
                    try:
                        shutil.copy2(txPath, finalFolder)
                        self.log.append("Copied {} to {}".format(txPath, finalFolder))
                    except IOError:
                        self.log.append("The specified source TX file doesn't exist: {}".format(txPath))
                else:
                    self.log.append("The specified source TX file doesn't exist: {}".format(txPath))
                return True
            else:
                return False
        elif "isSequence" in kwargs.keys():
            if kwargs["isSequence"]:
                srcFolderPath = _srcPath.rsplit("/", 1)[0]
                trgFolderPath = _trgPath.rsplit("/", 1)[0]
                # Get file name
                fileWithExt = pathParts[1]
                # Build list of files with the same name structure in the directory
                fileList = self.getSequenceFiles(srcFolderPath, fileWithExt)

                # Create folder structure
                if not os.path.exists(trgFolderPath):
                    try:
                        os.makedirs(trgFolderPath)
                        self.log.append("Directory structure created: {}".format(trgFolderPath))
                    except WindowsError:
                        self.log.append("Directory structure couldn't be created: {}".format(_trgPath))
                        return False
                else:
                    self.log.append("Directory structure already existed: {}".format(finalFolder))
                # Copy files
                for each in fileList:
                    try:
                        shutil.copy2(each, trgFolderPath)
                        self.log.append("Copied {} to {}".format(each, trgFolderPath))
                    except IOError:
                        self.log.append("The specified source doesn't exist: {}".format(each))
                    #Copy .tx
                    txPath = each.rsplit(".", 1)[0] + ".tx"
                    if os.path.exists(txPath):
                        try:
                            shutil.copy2(txPath, trgFolderPath)
                            self.log.append("Copied {} to {}".format(txPath, trgFolderPath))
                        except IOError:
                            self.log.append("The specified source TX file doesn't exist: {}".format(txPath))
                    else:
                        self.log.append("The specified source TX file doesn't exist: {}".format(txPath))
                return True
            else:
                return False
        else:
            # Create folder structure
            if not os.path.exists(finalFolder):
                try:
                    os.makedirs(finalFolder)
                    self.log.append("Directory structure created: {}".format(finalFolder))
                except WindowsError:
                    self.log.append("Directory structure couldn't be created: {}".format(finalFolder))
                    return False
            else:
                self.log.append("Directory structure already existed: {}".format(finalFolder))
            # Copy file
            try:
                shutil.copy2(_srcPath, finalFolder)
                self.log.append("Copied {} to {}".format(_srcPath, finalFolder))
            except IOError:
                self.log.append("The specified source doesn't exist: {}".format(_srcPath))
            return True

    def __repr__(self):
        output = ""
        for each in self.log:
            output += "--> " + each + "\n"
        return output

    # --------------------------------------------------------------------------------------------
    # Properties
    # --------------------------------------------------------------------------------------------
    @property
    def statusDict(self):
        return self.__statusDict

    @statusDict.setter
    def statusDict(self, d):
        self.__statusDict = d

    @property
    def log(self):
        return self.__log


# --------------------------------------------------------------------------------------------
# Main
# --------------------------------------------------------------------------------------------
def main():
    pass


if __name__ == "__main__":
    main()
