# -*- coding: utf-8 -*-
'''
Library to manage studio info

Modified on Mar 30, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.core.JSONManager import JSONManager
from cgx.core.NamingConventions import NamingConventions
import cgx.core
import os


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Studio info object
##--------------------------------------------------------------------------------------------
class StudioInfo(object):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _studioStructure=cgx.core.STUDIO_STUCTURE, _studioProperties= cgx.core.STUDIO_PROPERTIES, _studioNamingConventions= cgx.core.STUDIO_NAMINGCONVENTIONS):
        '''
        Library to manage studio info.
        :param _studioStructure: Aboslute path to the studio_structure.json file
        :type _studioStructure: string
        :param _studioProperties: Aboslute path to the studio_properties.json file
        :type _studioProperties: string
        :param _studioNamingConventions: Absolute path to the studio_namingconventions.json file
        :type _studioNamingConventions: string
        '''
        self.__studioStructure = JSONManager(_studioStructure)
        self.__studioProperties = JSONManager(_studioProperties)
        self.__studioNaming = NamingConventions(_studioNamingConventions)
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def getProjects(self, prefix, suffix, exceptions = [], _department='3D'):
        '''
        Gets the projects currently at work.
        :param prefix: Look for folders with his prefix pattern. Pass empty string if not needed.
        :type prefix: string
        :param suffix: Look for folders with his suffix pattern. Pass empty string if not needed.
        :type suffix: string
        :param exceptions: Ignore this specific folders.
        :type exceptions: strings list
        :return: Projects list
        :rType: list
        '''
        server = self.getProjectsFrom(_department)
        if os.path.exists(server):
            folders = os.listdir(server)
            projList = []
            #Find project folders
            for e in folders:
                if e not in exceptions:
                    projectName = e
                    prefixLen = len(prefix)
                    suffixLen = len(suffix)
                    if prefixLen > 0 and suffixLen > 0:
                        projectName = e[prefixLen:-suffixLen]
                    elif prefixLen > 0 and suffixLen <= 0:
                        projectName = e[prefixLen:]
                    elif suffixLen > 0 and prefixLen <= 0:
                        projectName = e[:-suffixLen]
                    
                    if (prefix + projectName + suffix) == e:
                        projList.append(e)
            return projList
        else:
            return None
    
    
    def getProjectsFrom(self, _department='3D'):
        '''
        Gets the server path for project listing.
        :return: Server path
        :rType: string
        '''
        generalNode = self.studioProperties.getProperty('get3DProjectsFrom', False)[0]
        serverName = generalNode.asDict()['get3DProjectsFrom']
        if _department == 'Comp':
            generalNode = self.studioProperties.getProperty('getCompProjectsFrom', False)[0]
            serverName = generalNode.asDict()['getCompProjectsFrom']
        serversNode = self.studioProperties.getName('servers', False)[0]
        server = serversNode.asDict()[serverName]
        return server
    
    
    def getSequencesFrom(self, _department='3D'):
        '''
        Gets the folder name to list sequences from.
        :return: foler name
        :rType: string
        '''
        generalNode = self.studioProperties.getProperty('get3DSequencesFrom', False)[0]
        seqsFolder = generalNode.asDict()['get3DSequencesFrom']
        if _department == 'Comp':
            generalNode = self.studioProperties.getProperty('getCompSequencesFrom', False)[0]
            seqsFolder = generalNode.asDict()['getCompSequencesFrom']
        return seqsFolder
    
    
    def getStudioName(self):
        '''
        Gets the studio name.
        :return: Studio name
        :rType: string
        '''
        generalNode = self.studioProperties.getProperty('studioName', False)[0]
        name = generalNode.asDict()['studioName']
        return name
    

    ##--------------------------------------------------------------------------------------------
    ##Properties
    ##--------------------------------------------------------------------------------------------
    @property
    def studioProperties(self):
        return self.__studioProperties
            
    @property
    def studioStructure(self):
        return self.__studioStructure
    
    @property
    def studioNaming(self):
        return self.__studioNaming


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass

if __name__=="__main__":
    main()