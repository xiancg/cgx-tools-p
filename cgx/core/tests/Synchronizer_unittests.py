# -*- coding: utf-8 -*-
'''
unittests for the Synchronizer library.

Created on Mar 25, 2015

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import unittest
import cgx.core.Synchronizer as sync
import os


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "2.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Synchronizer tests
##--------------------------------------------------------------------------------------------
class TestSynchronizer(unittest.TestCase):
    ##--------------------------------------------------------------------------------------------
    ##Setup/Teardown
    ##--------------------------------------------------------------------------------------------
    def setUp(self):
        self.syncObj = sync.Synchronizer()
        self.filesFolder = os.path.dirname(__file__)
        #Source
        self.texture = self.filesFolder + "/resources/texture/src_path/C_cresta_02__MSH-BUMP.1001.png"
        self.sequence = self.filesFolder + "/resources/sequence/src_path/testSeq.00000.jpg"
        self.singleFile = self.filesFolder + "/resources/singleFile/src_path/MOD_Boxeador.ma"
        #Target
        self.trgTexture =  self.filesFolder + "/resources/texture/trg_path/C_cresta_02__MSH-BUMP.1001.png"
        self.trgSequence = self.filesFolder + "/resources/sequence/trg_path/testSeq.00000.jpg"
        self.trgSingleFile = self.filesFolder + "/resources/singleFile/trg_path/MOD_Boxeador.ma"

    def tearDown(self):
        pass


    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    '''
    def test_getSyncStatus_inSync(self):
        returned = self.syncObj.getSyncStatus(self.singleFile, self.trgSingleFile)
        self.assertEqual(returned,"In sync")
    
    def test_getSyncStatus_outOfSync(self):
        singleFile = self.filesFolder + "/resources/singleFile/src_path/RIG_Penguin.ma"
        trgSingleFile = self.filesFolder + "/resources/singleFile/trg_path/RIG_Penguin.ma"
        returned = self.syncObj.getSyncStatus(singleFile, trgSingleFile)
        self.assertEqual(returned,"Out of sync")
    
    def test_getSyncStatus_differentDateSameSize(self):
        singleFile = self.filesFolder + "/resources/singleFile/src_path/MOD_Boxeador_sameSizeDifDate.ma"
        trgSingleFile = self.filesFolder + "/resources/singleFile/trg_path/MOD_Boxeador_sameSizeDifDate.ma"
        returned = self.syncObj.getSyncStatus(singleFile, trgSingleFile)
        self.assertEqual(returned,"Different date, same size")
    
    def test_getSyncStatus_bothPathsMissing(self):
        singleFile = self.filesFolder + "/resources/singleFile/src_path/MOD_Boxeador_12.ma"
        trgSingleFile = self.filesFolder + "/resources/singleFile/trg_path/MOD_Boxeador_12.ma"
        returned = self.syncObj.getSyncStatus(singleFile, trgSingleFile)
        self.assertEqual(returned,"Both paths missing")
    
    def test_getSyncStatus_sourcePathMissing(self):
        singleFile = self.filesFolder + "/resources/singleFile/src_path/MOD_Boxeador_12.ma"
        trgSingleFile = self.filesFolder + "/resources/singleFile/trg_path/MOD_Boxeador.ma"
        returned = self.syncObj.getSyncStatus(singleFile, trgSingleFile)
        self.assertEqual(returned,"Source file missing")
    
    def test_getSyncStatus_targetPathMissing(self):
        singleFile = self.filesFolder + "/resources/singleFile/src_path/MOD_Boxeador.ma"
        trgSingleFile = self.filesFolder + "/resources/singleFile/trg_path/MOD_Boxeador_12.ma"
        returned = self.syncObj.getSyncStatus(singleFile, trgSingleFile)
        self.assertEqual(returned,"Target file missing")
    
    def test_getSyncStatus_useDirectories(self):
        srcDir = self.filesFolder + "/resources/singleFile/src_path"
        trgDir = self.filesFolder + "/resources/singleFile/trg_path"
        returned = self.syncObj.getSyncStatus(srcDir, trgDir)
        self.assertEqual(returned,"Out of sync")
    '''
        
    def test_processPaths_sourceAndTargetEqual(self):
        src = self.filesFolder + "/resources/singleFile/src_path"
        trg = self.filesFolder + "/resources/singleFile/src_path"
        returned = self.syncObj.processPaths(src, trg)
        self.assertFalse(returned)
    
    def test_processPaths_sourcePathDoesntExist(self):
        src = self.filesFolder + "/resources/singleFile/src_pathxxx"
        trg = self.filesFolder + "/resources/singleFile/src_path"
        returned = self.syncObj.processPaths(src, trg)
        self.assertFalse(returned)
    
    def test_processPaths_isTextureCommonParams(self):
        returned = self.syncObj.processPaths(self.texture, self.trgTexture, isTexture=True)
        self.assertTrue(returned)
    
    def test_processPaths_isTextureFailedCreatingDirs(self):
        trg = "X:/resources/singleFile/src_path"
        returned = self.syncObj.processPaths(self.texture, trg, isTexture=True)
        self.assertFalse(returned)
    
    def test_processPaths_isTextureSourceDoesntExist(self):
        src = self.filesFolder + "/resources/singleFile/src_pathxxx"
        trg = self.filesFolder + "/resources/singleFile/src_path"
        returned = self.syncObj.processPaths(src, trg, isTexture=True)
        self.assertFalse(returned)
        
    def test_processPaths_getSequenceFiles(self):
        pathParts = self.sequence.rsplit("/", 1)
        returned = self.syncObj.getSequenceFiles(pathParts[0],pathParts[1])
        self.assertEqual(len(returned),72)
    
    def test_processPaths_isSequenceCommonParams(self):
        returned = self.syncObj.processPaths(self.sequence, self.trgSequence, isSequence=True)
        self.assertTrue(returned)
    
    def test_processPaths_isSequenceFailedCreatingDirs(self):
        trg = "X:/resources/singleFile/trg_path/testSeq.00000.jpg"
        returned = self.syncObj.processPaths(self.sequence, trg, isSequence=True)
        self.assertFalse(returned)
    
    def test_processPaths_passTwoDirs(self):
        src = self.filesFolder + "/resources/directory/src_path"
        trg = self.filesFolder + "/resources/directory/trg_path"
        returned = self.syncObj.processPaths(src, trg)
        self.assertTrue(returned)
    

##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
if __name__ == '__main__':
    unittest.main()