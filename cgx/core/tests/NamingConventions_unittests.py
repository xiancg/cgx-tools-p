# -*- coding: utf-8 -*-
'''
unittests for NamingConventions

Modified on Apr 1, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''
##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.core.NamingConventions import NamingConventions
import unittest, os


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Test for NamingConventions object
##--------------------------------------------------------------------------------------------
class Test_NamingConventions(unittest.TestCase):
    ##--------------------------------------------------------------------------------------------
    ##Setup/Teardown
    ##--------------------------------------------------------------------------------------------
    def setUp(self):
        self.appRootFolder = os.path.dirname(__file__)
        self.configFolder = "M:/CGXtools/core/tests/resources"
        #Studio info
        self.gizmoNamingFile= self.appRootFolder + "/resources/Gizmo_cfg/" + "studio_namingconventions.json"
        self.nInfo = NamingConventions(self.gizmoNamingFile)

    def tearDown(self):
        pass
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def test_getNaming(self):
        returned = self.nInfo.getNaming('sequence', 'general', wildcards=False)
        self.assertEqual(returned,'SEQ_')


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass


if __name__=="__main__":
    unittest.main()