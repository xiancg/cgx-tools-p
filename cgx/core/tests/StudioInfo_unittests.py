# -*- coding: utf-8 -*-
'''
unittests for StudioInfo

Created on Mar 30, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.core.StudioInfo import StudioInfo
import unittest, os


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Unit testing for Studio info
##--------------------------------------------------------------------------------------------
class Test_StudioInfo(unittest.TestCase):
    ##--------------------------------------------------------------------------------------------
    ##Setup/Teardown
    ##--------------------------------------------------------------------------------------------
    def setUp(self):
        self.appRootFolder = os.path.dirname(__file__)
        self.gizmoStructureFile= self.appRootFolder + "/resources/Gizmo_cfg/" + "studio_structure.json"
        self.gizmoPropertiesFile= self.appRootFolder + "/resources/Gizmo_cfg/" + "studio_properties.json"
        self.gizmoNamingFile= self.appRootFolder + "/resources/Sinema_cfg/" + "studio_namingconventions.json"
        self.sinemaStructureFile= self.appRootFolder + "/resources/Sinema_cfg/" + "studio_structure.json"
        self.sinemaPropertiesFile= self.appRootFolder + "/resources/Sinema_cfg/" + "studio_properties.json"
        self.sinemaNamingFile= self.appRootFolder + "/resources/Sinema_cfg/" + "studio_namingconventions.json"
        self.sInfo = StudioInfo(self.gizmoStructureFile, self.gizmoPropertiesFile,self.gizmoNamingFile)

    def tearDown(self):
        pass
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def test_getStudioName_gizmo(self):
        self.assertEqual(self.sInfo.getStudioName(),'Gizmo')

    def test_getStudioName_sinema(self):
        newSInfo = StudioInfo(self.sinemaStructureFile,self.sinemaPropertiesFile,self.sinemaNamingFile)
        self.assertEqual(newSInfo.getStudioName(),'Sinema')

    def test_getProjects_gizmo(self):
        projsList= self.sInfo.getProjects("","_3D")
        self.assertTrue(len(projsList) > 0)
  
    def test_getProjects_sinema(self):
        newSInfo = StudioInfo(self.sinemaStructureFile,self.sinemaPropertiesFile,self.sinemaNamingFile)
        projsList= newSInfo.getProjects("","",['CN03'])
        self.assertTrue(len(projsList) > 0)
    
    def test_getProjectsFrom_gizmo(self):
        self.assertEqual(self.sInfo.getProjectsFrom(),"M:/DT/Maya/GizmoServer")
    
    def test_getProjectsFrom_sinema(self):
        newSInfo = StudioInfo(self.sinemaStructureFile,self.sinemaPropertiesFile,self.sinemaNamingFile)
        self.assertEqual(newSInfo.getProjectsFrom(),"M:/DT/Maya/SinemaServer")
    
    def test_getSequencesFrom_gizmo(self):
        self.assertEqual(self.sInfo.getSequencesFrom(),'ANIMATION')
    
    def test_getSequencesFrom_sinema(self):
        newSInfo = StudioInfo(self.sinemaStructureFile,self.sinemaPropertiesFile,self.sinemaNamingFile)
        self.assertEqual(newSInfo.getSequencesFrom(),'sequences')


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass

if __name__=="__main__":
    unittest.main()