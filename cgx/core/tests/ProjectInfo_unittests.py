# -*- coding: utf-8 -*-
'''
unittests for Abstract_projectInfo

Created on Mar 31, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.core.ProjectInfo import ProjectInfo
from cgx.core.StudioInfo import StudioInfo
import unittest

##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Test for Project info object
##--------------------------------------------------------------------------------------------
class Test_ProjectInfo(unittest.TestCase):
    ##--------------------------------------------------------------------------------------------
    ##Setup/Teardown
    ##--------------------------------------------------------------------------------------------
    def setUp(self):
        #self.configFolder = "D:/XianCG/CGXtools/core/tests/resources"
        self.configFolder = "M:/CGXtools/core/tests/resources"
        #Studio info
        self.gizmoStructureFile= self.configFolder + "/Gizmo_cfg/studio_structure.json"
        self.gizmoPropertiesFile= self.configFolder + "/Gizmo_cfg/studio_properties.json"
        self.gizmoNamingFile= self.configFolder + "/Gizmo_cfg/studio_namingconventions.json"
        self.sinemaStructureFile= self.configFolder + "/Sinema_cfg/studio_structure.json"
        self.sinemaPropertiesFile= self.configFolder + "/Sinema_cfg/studio_properties.json"
        self.sinemaNamingFile= self.configFolder + "/Sinema_cfg/studio_namingconventions.json"
        #self.sInfo = StudioInfo(self.gizmoStructureFile, self.gizmoPropertiesFile, self.gizmoNamingFile)
        self.sInfo = StudioInfo()
        self.proj = self.sInfo.getProjects("","")

        #Project info
        self.gizmoFolderStructFile = self.configFolder + "/Gizmo_cfg/3D_structure.json"
        self.sinemaFolderStructFile =  self.configFolder + "/Sinema_cfg/project_structure.json"
        self.pInfo = ProjectInfo(self.sInfo, self.proj[0])

    def tearDown(self):
        pass
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    #@unittest.SkipTest
    '''
    def test_getSequences_gizmo(self):
        self.assertTrue(len(self.pInfo.getSequences(self.sInfo.getSequencesFrom())) > 0)

    def test_getSequences_sinema(self):
        sInfo = StudioInfo(self.sinemaStructureFile, self.sinemaPropertiesFile, self.sinemaNamingFile)
        proj = sInfo.getProjects("","",["CN03"])
        pInfo = ProjectInfo(sInfo, proj[1], self.sinemaFolderStructFile)
        self.assertTrue(len(pInfo.getSequences(sInfo.getSequencesFrom())) > 0)
        
    def test_getShots_gizmo(self):
        seqs = self.pInfo.getSequences(self.sInfo.getSequencesFrom())
        #print self.pInfo.getShots(seqs[0], self.sInfo.getSequencesFrom())
        self.assertTrue(len(self.pInfo.getShots(seqs[0],self.sInfo.getSequencesFrom())) > 0)
        
    def test_getShots_sinema(self):
        sInfo = StudioInfo(self.sinemaStructureFile, self.sinemaPropertiesFile, self.sinemaNamingFile)
        proj = sInfo.getProjects("","",["CN03"])
        pInfo = ProjectInfo(sInfo, proj[1], self.sinemaFolderStructFile)
        seqs = pInfo.getSequences(sInfo.getSequencesFrom())
        #print pInfo.getShots(seqs[0], sInfo.getSequencesFrom())
        self.assertTrue(len(pInfo.getShots(seqs[0], sInfo.getSequencesFrom())) > 0)
    
    def test_getAssetTypes_gizmo(self):
        #print self.pInfo.getAssetTypes()
        self.assertTrue(len(self.pInfo.getAssetTypes())>0)
        
    def test_getAssetTypes_sinema(self):
        sInfo = StudioInfo(self.sinemaStructureFile, self.sinemaPropertiesFile, self.sinemaNamingFile)
        proj = sInfo.getProjects("","",["CN03"])
        pInfo = ProjectInfo(sInfo, proj[1], self.sinemaFolderStructFile)
        #print pInfo.getAssetTypes()
        self.assertTrue(len(pInfo.getAssetTypes())>0)
    
    def test_getAssets_gizmo(self):
        assetType= self.pInfo.getAssetTypes()[0]
        #print self.pInfo.getAssets(assetType)
        self.assertTrue(len(self.pInfo.getAssets(assetType))>0)
        
    def test_getAssets_sinema(self):
        sInfo = StudioInfo(self.sinemaStructureFile, self.sinemaPropertiesFile, self.sinemaNamingFile)
        proj = sInfo.getProjects("","",["CN03"])
        pInfo = ProjectInfo(sInfo, proj[1], self.sinemaFolderStructFile)
        assetType= pInfo.getAssetTypes()[1]
        #print pInfo.getAssets(assetType)
        self.assertTrue(len(pInfo.getAssets(assetType))>0)
        
    def test_getSubAssets_gizmo(self):
        assetType= self.pInfo.getAssetTypes()[0]
        assetName = self.pInfo.getAssets(assetType)[0]
        #print self.pInfo.getSubAssets(assetType, assetName)
        self.assertTrue(len(self.pInfo.getSubAssets(assetType, assetName))>0)
    
    def test_getFileTypes_gizmo(self):
        #print self.pInfo.getFileTypes()
        self.assertTrue(len(self.pInfo.getFileTypes().keys())>0)
        
    def test_getFileTypes_sinema(self):
        sInfo = StudioInfo(self.sinemaStructureFile, self.sinemaPropertiesFile, self.sinemaNamingFile)
        proj = sInfo.getProjects("","",["CN03"])
        pInfo = ProjectInfo(sInfo, proj[1], self.sinemaFolderStructFile)
        #print pInfo.getAssets(assetType)
        self.assertTrue(len(pInfo.getFileTypes().keys())>0)
        
    def test_projProperties(self):
        self.assertEqual(self.pInfo.fps, 24)
        self.assertEqual(self.pInfo.frameWidth ,1920)
        self.assertEqual(self.pInfo.frameHeight, 1080)
        self.assertEqual(self.pInfo.stereo, False)
        self.assertEqual(self.pInfo.colorSpace, 'sRGB')
        
   
    def test_buildPoseCachePath_gizmo(self):
        _poseName="EstaPose"
        propertiesDict = {"abstractFolderType":"leaffolder",
                          "concreteFolderType":"published_leaffolder",
                          "folderFunction":"published_content",
                          "folderConcreteFunction":"published_pose",
                          "placeholder":False}
        publishedPoseNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)[0]
        if publishedPoseNode != None:
            poseNameDict={"name":"$poseName",
                        "concreteFolderType":"asset_subfolder",
                        "abstractFolderType":"subfolder",
                        "folderFunction":"work_content",
                        "folderConcreteFunction":"pose_folder",
                        "placeholder":True}
            pathList = self.pInfo.rebuildPath(publishedPoseNode, {_poseName:poseNameDict})
            pathList.append(publishedPoseNode.name)
            
            #Build path string
            server = self.sInfo.getProjectsFrom()
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
            
            print pathStr
    '''
    
    def test_buildProjSeqShotPath(self):
        _seq = "SEQ_11"
        _shot = "s_030"
        propertiesDict = {"abstractFolderType":"leaffolder",
                          "concreteFolderType":"published_leaffolder",
                          "folderFunction":"published_content",
                          "folderConcreteFunction":"published_cache",
                          "placeholder":False}
        cacheNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)[0]
        if cacheNode != None:
            seqDict={"name":"$SEQ_##",
                    "abstractFolderType":"subfolder",
                    "concreteFolderType":"sequence_subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
            shotDict={"name":"$s_###",
                    "abstractFolderType":"subfolder",
                    "concreteFolderType":"shot_subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
            pathList = self.pInfo.rebuildPath(cacheNode, {_seq:seqDict, _shot:shotDict})
            pathList.append(cacheNode.name)
            
            #Build path string
            server = self.sInfo.getProjectsFrom()
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            pathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
            
            print pathStr

    '''
    def test_buildSharedPath(self):
        assetType = "PROPS"
        assetName = "Pelotatenis"
        subAssetName = ""
        sharedPropertiesDict = {"abstractFolderType":"subfolder",
                        "concreteFolderType":"technical_subfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_sharedassets",
                        "placeholder":False}
        sharedNode = self.pInfo.folderStructure.getPropertiesAndValues(sharedPropertiesDict)
        sharedPathStr = ""
        if len(sharedNode) == 1:
            node = sharedNode[0]
            pathList = self.pInfo.rebuildPath(node)
            pathList.append(node.name)
            #Build path string
            server = self.sInfo.getProjectsFrom()
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            sharedPathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])
        
        propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
        if subAssetName not in [None,""]:
            propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_subasset",
                        "placeholder":False}
        assetTypeDict={"name":"CHARACTERS",
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
        assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
        assetNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
        assetPathStr = ""
        for node in assetNode:
            pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict})
            if subAssetName not in [None,""]:
                pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict,subAssetName:subAssetDict})
            pathList.append(node.name)
            pathStr = "/".join(pathList[1:])
            
            if "$" not in pathStr:
                assetPathStr = pathStr
                break
        
        finalPathStr = sharedPathStr + "/" + assetPathStr
            
        print finalPathStr
    '''
    
    '''
    def test_buildBackupPath(self):
        assetType = "PROPS"
        assetName = "Pelotatenis"
        subAssetName = ""

        bkupPropertiesDict = {"abstractFolderType":"subfolder",
                              "concreteFolderType":"technical_subfolder",
                              "folderFunction":"published_content",
                              "folderConcreteFunction":"published_backup",
                              "placeholder":False}
        bkupNode = self.pInfo.folderStructure.getPropertiesAndValues(bkupPropertiesDict)
        bkupPathStr = ""
        if len(bkupNode) == 1:
            node = bkupNode[0]
            pathList = self.pInfo.rebuildPath(node)
            pathList.append(node.name)
            #Build path string
            server = self.sInfo.getProjectsFrom()
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            bkupPathStr = server + "/" + self.pInfo.project + "/" + "/".join(pathList[1:])

        propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_asset",
                        "placeholder":False}
        if subAssetName not in [None,""]:
            propertiesDict = {"concreteFolderType":"published_leaffolder",
                        "abstractFolderType":"branchfolder",
                        "folderFunction":"published_content",
                        "folderConcreteFunction":"published_subasset",
                        "placeholder":False}
        assetTypeDict={"name":"CHARACTERS",
                    "concreteFolderType":"assetType_mainfolder",
                    "abstractFolderType":"mainfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":False}
        assetDict= {"name":"$CharacterName",
                "concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "folderConcreteFunction":"",
                "placeholder":True}
        subAssetDict= {"name":"$assetName_$subassetname",
                    "concreteFolderType":"subasset_subfolder",
                    "abstractFolderType":"subfolder",
                    "folderFunction":"grouping",
                    "folderConcreteFunction":"",
                    "placeholder":True}
        assetNode = self.pInfo.folderStructure.getPropertiesAndValues(propertiesDict)
        assetPathStr = ""
        for node in assetNode:
            pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict})
            if subAssetName not in [None,""]:
                pathList = self.pInfo.rebuildPath(node, {assetType:assetTypeDict,assetName:assetDict,subAssetName:subAssetDict})
            pathList.append(node.name)
            pathStr = "/".join(pathList[1:])
            
            if "$" not in pathStr:
                assetPathStr = pathStr
                break
        
        finalPathStr = bkupPathStr + "/" + assetPathStr
        
        print finalPathStr
        '''
    

##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass


if __name__=="__main__":
    unittest.main()