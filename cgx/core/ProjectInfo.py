# -*- coding: utf-8 -*-
'''
Manages project info

Created on Mar 31, 2016

@author: Chris Granados - Xian chris.granados@xiancg.com http://www.chrisgranados.com/
'''

##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
from cgx.core.JSONManager import JSONManager
import cgx.core
import os


##--------------------------------------------------------------------------------------------
##Metadata
##--------------------------------------------------------------------------------------------
__author__ = "Chris Granados"
__copyright__ = "Copyright 2016, Chris Granados"
__credits__ = ["Chris Granados"]
__version__ = "1.0.0"
__email__ = "chris.granados@xiancg.com"


##--------------------------------------------------------------------------------------------
##Class: Project info object
##--------------------------------------------------------------------------------------------
class ProjectInfo(object):
    ##--------------------------------------------------------------------------------------------
    ##Constructor
    ##--------------------------------------------------------------------------------------------
    def __init__(self, _studioInfo, _project, _folderStructure=cgx.core.FOLDER_STRUCTURE):
        '''
        Abstract class to retrieve info about a specific project.
        :param _studioInfo: StudioInfo instance to get info about studio naming conventions and servers.
        :type _studioInfo: StudioInfo
        :param _folderStructure: Absolute path to the json template file for the project folder structure.
        :type _folderStructure: string
        :param _project: Project name. Must match actual folder name. Use StudioInfo.getProjects()
        :type _project: string
        '''
        self.__folderStructure = JSONManager(_folderStructure)
        self.__studioInfo = _studioInfo
        self.__studioProperties = self.sInfo.studioProperties
        self.__studioNaming = self.sInfo.studioNaming
        self.__project = _project
        self.__projectProperties = JSONManager(self.getProjConfig())
        if self.folderStructure == cgx.core.COMP_FOLDER_STRUCTURE:
            self.__projectProperties = JSONManager(self.getProjConfig('Comp'))
        #Project properties
        self.__fps = self.projProperties.getValueByProperty('fps', False)
        self.__frameWidth = self.projProperties.getValueByProperty('frameWidth', False)
        self.__frameHeight = self.projProperties.getValueByProperty('frameHeight', False)
        self.__stereo = self.projProperties.getValueByProperty('stereo', False)
        self.__colorSpace = self.projProperties.getValueByProperty('colorSpace', False)
        
    
    ##--------------------------------------------------------------------------------------------
    ##Methods
    ##--------------------------------------------------------------------------------------------
    def getSequences(self, _sequencesFolder, _exceptions=[], _department='3D'):
        '''
        Retrieves the sequences for the class project.
        :param _sequencesFolder: Folder name from where to get sequences. StudioInfo.getSequencesFrom()
        :type _sequencesFolder: string
        :param _exceptions: List of folders to ignore if found.
        :type _exceptions: list
        :return: List of sequences
        :rtype: list
        '''
        #Get all sequence type folders
        propertiesDict = {"abstractFolderType":"subfolder",
            "concreteFolderType":"sequence_subfolder",
            "folderFunction":"grouping",
            "placeholder":True}
        nodes = self.folderStructure.getPropertiesAndValues(propertiesDict)
        
        #Pick from this list which node to be used, using as parameter the parent's name
        pickNode = None
        for each in nodes:
            if each.parent.name == _sequencesFolder:
                pickNode = each
                break
        #Rebuild path recursively
        if pickNode != None:
            pathList = self.rebuildPath(pickNode)
            #Build path string
            server = self.sInfo.getProjectsFrom(_department)
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            pathStr = server + "/" + self.project + "/" + "/".join(pathList[1:])
            if os.path.exists(pathStr):
                #List dirs from path and filter them to get sequences only
                foldersList = os.listdir(pathStr)
                sequenceNaming = self.sNaming.getNaming('sequence', 'general', wildcards=False)
                filteredList = self.__filterFolders(foldersList, sequenceNaming, _exceptions)
                return sorted(filteredList)
            else:
                return None
        else:
            return None
    
    
    def getShots (self, _sequence, _sequencesFolder, _exceptions=[], _department='3D'):
        '''
        Retrieves the shots for the given sequence.
        :param _sequence: Required sequence name.
        :type _sequence: string
        :param _sequencesFolder: Folder name from where to get sequences. StudioInfo.getSequencesFrom()
        :type _sequencesFolder: string
        :param _exceptions: List of folders to ignore if found.
        :type _exceptions: list
        :return: List of sequences
        :rtype: list
        '''
        #Get all shot type folders
        propertiesDict = {"abstractFolderType":"subfolder",
            "concreteFolderType":"shot_subfolder",
            "folderFunction":"grouping",
            "placeholder":True}
        nodes = self.folderStructure.getPropertiesAndValues(propertiesDict)
        #Pick from this list which node to be used, using as parameter the sequence.parent name
        shotNode = None
        for each in nodes:
            if each.parent.parent.name == _sequencesFolder:
                shotNode = each
                break
        #Rebuild path recursively
        if shotNode != None:
            pathList = self.rebuildPath(shotNode)
            #Replace sequence wildcard with sequence name
            if _sequence != None:
                pathList[-1] = _sequence
            #Build path string
            server = self.sInfo.getProjectsFrom(_department)
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            pathStr = server + "/" + self.project + "/" + "/".join(pathList[1:])
            if os.path.exists(pathStr):
                #List dirs from path and filter them to get shots only
                foldersList = os.listdir(pathStr)
                shotNaming = self.sNaming.getNaming('shot', 'general', wildcards=False)
                filteredList = self.__filterFolders(foldersList, shotNaming, _exceptions)
                return sorted(filteredList)
            else:
                return None
        else:
            return None
    
    
    def getAssetTypes(self, exceptions=[]):
        '''
        Retrieves the asset types
        :param _exceptions: List of folders to ignore if found.
        :type _exceptions: list
        :return: List of asset types
        :rtype: list
        '''
        #Get all assetType type folders
        propertiesDict = {"concreteFolderType":"assetType_mainfolder",
            "abstractFolderType":"mainfolder",
            "folderFunction":"grouping",
            "placeholder":False}
        nodes = self.folderStructure.getPropertiesAndValues(propertiesDict)
        if len(nodes) >= 1:
            assetTypesList = [each.name for each in nodes if each not in exceptions]
            return sorted(assetTypesList)
        else:
            return None
            
    
    def getAssets(self, _assetType, _exceptions=[], _department='3D'):
        '''
        Retrieves the assets for the given asset type.
        :param _assetType: Asset type to be used for listing.
        :type _assetType: string
        :param _exceptions: List of folders to ignore if found.
        :type _exceptions: list
        :return: List of assets
        :rtype: list
        '''
        #Get all assets folders
        propertiesDict = {"concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "placeholder":True}
        nodes = self.folderStructure.getPropertiesAndValues(propertiesDict)
        #Pick from this list which node to be used
        assetNode = None
        for each in nodes:
            if each.parent.name == _assetType:
                assetNode = each
                break
        #Rebuild path recursively
        if assetNode != None:
            pathList = self.rebuildPath(assetNode)
            #Build path string
            server = self.sInfo.getProjectsFrom(_department)
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            pathStr = server + "/" + self.project + "/" + "/".join(pathList[1:])
            #List dirs from path and filter them to get assets only
            if os.path.exists(pathStr):
                foldersList = os.listdir(pathStr)
                filteredList = self.__filterFolders(foldersList, None, _exceptions)
                return sorted(filteredList)
            else: 
                return None
        else:
            return None
    
    
    def getSubAssets(self, _assetType, _asset, _exceptions=[], _department='3D'):
        '''
        Retrieves the subassets for the given asset.
        :param _assetType: Asset type to be used for listing.
        :type _assetType: string
        :param _asset: Asset to be used for listing.
        :type _asset: string
        :param _exceptions: List of folders to ignore if found.
        :type _exceptions: list
        :return: List of subassets
        :rtype: list
        '''
        #LOOK FOR ASSET FIRST
        #Get all assets folders
        propertiesDict = {"concreteFolderType":"asset_subfolder",
                "abstractFolderType":"subfolder",
                "folderFunction":"grouping",
                "placeholder":True}
        nodes = self.folderStructure.getPropertiesAndValues(propertiesDict)
        #Pick from this list which node to be used
        assetNode = None
        for each in nodes:
            if each.parent.name == _assetType:
                assetNode = each
                break
        #Rebuild path to asset recursively
        if assetNode != None:
            pathList = self.rebuildPath(assetNode)
            #Build path string
            server = self.sInfo.getProjectsFrom(_department)
            if server [-1] in ["/","\\"]:
                server = server[:-1]
            pathStr = server + "/" + self.project + "/" + "/".join(pathList[1:])
            if os.path.exists(pathStr):
                #List dirs from path and filter them to get assets only
                foldersList = os.listdir(pathStr)
                filteredList = sorted(self.__filterFolders(foldersList, None, _exceptions))
                if _asset in filteredList:
                    pathStr += "/" + _asset
                    #THEN LOOK FOR SUBASSETS
                    #Find if asset has a subassets folder
                    subassetGroupingDict = {"concreteFolderType":"subassetType_subfolder",
                        "abstractFolderType":"subfolder",
                        "folderFunction":"grouping",
                        "placeholder":False}
                    subassetGrpNodes = self.folderStructure.getPropertiesAndValues(subassetGroupingDict)
                    #Pick from this list which node to be used
                    subassetGrpNode = None
                    for each in subassetGrpNodes:
                        if each.parent.parent.name == _assetType:
                            subassetGrpNode = each
                            break
                    if subassetGrpNode != None:
                        pathStr += "/" + subassetGrpNode.name
                        #List dirs from path and filter them to get assets only
                        if os.path.exists(pathStr):
                            subAssetsFoldersList = os.listdir(pathStr)
                            subAssetsFilteredList = self.__filterFolders(subAssetsFoldersList, None, _exceptions)
                            return sorted(subAssetsFilteredList)
                        else:
                            return None
                    else:
                        return None
                else:
                    return None
            else:
                return None
        else:
            return None
        
        
    def getFileTypes(self):
        '''
        Retrieves the file types for the studio.
        :return: pipelineStage:filePrefix naming dict pairs
        :rtype: dict
        '''
        node= self.sNaming.studioNaming.getName('fileTypes', False)
        if len(node) >= 1:
            nodeDict = node[0].asDict()
            del(nodeDict['name'])
            return nodeDict
        else:
            return None
    
    
    def getPipelineStages(self, exceptions=[]):
        '''
        Retrieves the pipeline steps.
        :param _exceptions: List of folders to ignore if found.
        :type _exceptions: list
        :return: List of pipeline steps
        :rtype: list
        '''
        #Get all pipeline step type folders
        propertiesDict = {"concreteFolderType":"pipelineStage_mainfolder",
                          "abstractFolderType":"mainFolder",
                          "folderFunction":"grouping",
                          "placeholder":False}
        nodes = self.folderStructure.getPropertiesAndValues(propertiesDict)
        pipelineStagesList = [each.name for each in nodes if each not in exceptions]
        
        return sorted(pipelineStagesList)
        
    
    def getProjConfig(self, _department='3D'):
        '''
        Retrieves absolute path to project_properties.json for the current project.
        :return: Full path to project_properties.json for self.project
        :rtype: string
        '''
        propertiesDict = {"abstractFolderType":"subfolder",
                          "concreteFolderType":"technical_subfolder",
                          "folderFunction":"published_content",
                          "folderConcreteFunction":"published_cfg",
                          "placeholder":False}
        configNode = self.folderStructure.getPropertiesAndValues(propertiesDict)
        if len(configNode) >= 1:
            #Rebuild path recursively
            if configNode[0] != None:
                pathList = self.rebuildPath(configNode[0])
                #Build path string
                server = self.sInfo.getProjectsFrom(_department)
                if server [-1] in ["/","\\"]:
                    server = server[:-1]
                pathStr = server + "/" + self.project + "/" + "/".join(pathList[1:]) + "/" + configNode[0].name + "/" + 'project_properties.json'
                if os.path.exists(pathStr):
                    return pathStr
                else:
                    return cgx.core.PROJECT_PROPERTIES
            else:
                return cgx.core.PROJECT_PROPERTIES
        else:
            return cgx.core.PROJECT_PROPERTIES
        
    
    def __filterFolders(self, _foldersList, _namingFilter, _exceptions=[]):
        '''
        Retrieves given folder list filtered from folders that are not needed.
        Filters folders name with wildcards as defined by NamingConventions, by exceptions and by naming convention.
        :param _foldersList: Folders list
        :type _foldersList: list
        :param _namingFilter: Naming convention to be used for filtering. None if nothing is required.
        :type _namingFilter: string
        :param _exceptions: List of folders to ignore if found.
        :type _exceptions: list
        :return: List of folders
        :rtype: list
        '''
        #Filter by wildcards
        wildcardsFiltered = []
        for each in _foldersList:
            i = 0
            for wildcard in self.sNaming.wildcards:
                if wildcard in each:
                    break
                elif each not in wildcardsFiltered and i == len(self.sNaming.wildcards)-1:
                    wildcardsFiltered.append(each)
                i += 1
        #Filter by exceptions
        exceptionsFiltered = []
        for each in wildcardsFiltered:
            if each not in _exceptions:
                exceptionsFiltered.append(each)
        #Filter by naming conventions
        if _namingFilter != None:
            filteredList = []
            for each in exceptionsFiltered:
                if each[:len(_namingFilter)] == _namingFilter:
                    filteredList.append(each)
            return sorted(filteredList)
        else:
            return sorted(exceptionsFiltered)

    
    def rebuildPath(self, _node, _dict={}):
        '''
        Retrieves rebuilt path list, up to the root folder for the given node.
        :param _node: Node to be used for recursive exploration of its parents.
        :type _node: TreeNode
        :param _dict: Key:{properties}. If properties match in recursive exploration, item will be replaced by Key.
        Useful to look for and replace placeholders.
        :type _dict: dict
        :return: Path list
        :rtype: list
        '''
        pathList = []
        if _node.parent != None and _node.depth > 0:
            match = self.__matchProperties(_node.parent,_dict)
            if match:
                pathList.append(match)
            else:
                pathList.append(_node.parent.name)
            pathList= self.__rebuildPath_recursive(_node.parent,pathList,_dict)
        pathList = pathList[::-1]
        
        return pathList
    
    
    def __rebuildPath_recursive(self, _node, _pathList, _dict):
        '''
        Recursive function for self.reduiltPath()
        :param _node: Node to be used for recursive exploration of its parents.
        :type _node: TreeNode
        :param _pathList: Path list
        :type _pathList: list
        :param _dict: Key:{properties}. If properties match in recursive exploration, item will be replaced by Key.
        Useful to look for and replace placeholders.
        :type _dict: dict
        :return: Path list
        :rtype: list
        '''
        if _node.parent != None and _node.parent.depth > 0:
            match = self.__matchProperties(_node.parent,_dict)
            if match != None:
                _pathList.append(match)
            else:
                _pathList.append(_node.parent.name)
            self.__rebuildPath_recursive(_node.parent,_pathList,_dict)
            return _pathList
        else:
            return _pathList
        
    
    def __matchProperties(self, _node, _dict):
        '''
        If node properties match with any given properties dict in _dict, return its key.
        :param _node: Node to be used for properties exploration.
        :type _node: TreeNode
        :param _dictList: {Key:{properties},Key:{properties}}. If properties match return Key, else return None.
        :type _dictList: dict
        :return: String to be used instead of placeholder name.
        :rtype: string
        '''
        for key, propertiesDict in _dict.iteritems():
            if bool( set(_node.headers) & set(propertiesDict.keys()) ):#Properties names match
                filteredNodeDict = {}
                nodeAsDict = _node.asDict()
                for each in propertiesDict.keys():
                    if each in nodeAsDict.keys():
                        filteredNodeDict[each] = nodeAsDict[each]
                if filteredNodeDict == propertiesDict:#Properties values match
                    return key
    
    
    def __splitPath(self, _path):
        '''
        Retrieves the given path split by folders.
        :return: Absolute path to a file.
        :rtype: string
        '''
        slashIndex = _path.rfind("/")
        splitList = []
        if slashIndex == -1:
            splitList = _path.split("\\")
        else:
            splitList = _path.split("/")
            
        return splitList
        
    
    ##--------------------------------------------------------------------------------------------
    ##Properties
    ##--------------------------------------------------------------------------------------------
    @property
    def folderStructure(self):
        return self.__folderStructure
    
    @property
    def sInfo(self):
        return self.__studioInfo
    
    @property
    def sNaming(self):
        return self.__studioNaming
    
    @property
    def sProperties(self):
        return self.__studioProperties
    
    @property
    def projProperties(self):
        return self.__projectProperties
    @projProperties.setter
    def projProperties(self, pProp):
        self.__projectProperties = pProp
    
    @property
    def project(self):
        return self.__project
    
    @project.setter
    def project(self, p):
        self.__project = p
        if self.folderStructure == cgx.core.FOLDER_STRUCTURE:
            self.projProperties = JSONManager(self.getProjConfig())
        elif self.folderStructure == cgx.core.COMP_FOLDER_STRUCTURE:
            self.projProperties = JSONManager(self.getProjConfig('Comp'))
        self.fps = self.projProperties.getValueByProperty('fps', False)
        self.frameWidth = self.projProperties.getValueByProperty('frameWidth', False)
        self.frameHeight = self.projProperties.getValueByProperty('frameHeight', False)
        self.stereo = self.projProperties.getValueByProperty('stereo', False)
        self.colorSpace = self.projProperties.getValueByProperty('colorSpace', False)
    
    @property
    def fps(self):
        return self.__fps
    @fps.setter
    def fps(self, f):
        self.__fps = f
    
    @property
    def frameWidth(self):
        return self.__frameWidth
    @frameWidth.setter
    def frameWidth(self, fw):
        self.__frameWidth = fw
    
    @property
    def frameHeight(self):
        return self.__frameHeight
    @frameHeight.setter
    def frameHeight(self, fh):
        self.__frameHeight = fh
    
    @property
    def stereo(self):
        return self.__stereo
    @stereo.setter
    def stereo(self, s):
        self.__stereo = s
    
    @property
    def colorSpace(self):
        return self.__colorSpace
    @colorSpace.setter
    def colorSpace(self, c):
        self.__colorSpace = c


##--------------------------------------------------------------------------------------------
##Main
##-------------------------------------------------------------------------------------------- 
def main():
    pass


if __name__=="__main__":
    main()