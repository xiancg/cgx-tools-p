##--------------------------------------------------------------------------------------------
##imports
##--------------------------------------------------------------------------------------------
import os


##--------------------------------------------------------------------------------------------
##Global Variables
##--------------------------------------------------------------------------------------------
CoreRootFolder = os.path.dirname(__file__)

STUDIO_STUCTURE = CoreRootFolder + '/cfg/' + 'studio_structure.json'
if not os.path.exists(STUDIO_STUCTURE):
    STUDIO_STUCTURE = CoreRootFolder + '/proxyCfg/' + 'studio_structure.json'
    
STUDIO_PROPERTIES = CoreRootFolder + '/cfg/' + 'studio_properties.json'
if not os.path.exists(STUDIO_PROPERTIES):
    STUDIO_PROPERTIES = CoreRootFolder + '/proxyCfg/' + 'studio_properties.json'
    
STUDIO_NAMINGCONVENTIONS = CoreRootFolder + '/cfg/' + 'studio_namingconventions.json'
if not os.path.exists(STUDIO_NAMINGCONVENTIONS):
    STUDIO_NAMINGCONVENTIONS = CoreRootFolder + '/proxyCfg/' + 'studio_namingconventions.json'
    
FOLDER_STRUCTURE = CoreRootFolder + '/cfg/' + '3D_structure.json'
if not os.path.exists(FOLDER_STRUCTURE):
    FOLDER_STRUCTURE = CoreRootFolder + '/proxyCfg/' + '3D_structure.json'

COMP_FOLDER_STRUCTURE = CoreRootFolder + '/cfg/' + 'Comp_structure.json'
if not os.path.exists(COMP_FOLDER_STRUCTURE):
    COMP_FOLDER_STRUCTURE = CoreRootFolder + '/proxyCfg/' + 'Comp_structure.json'
    
PROJECT_PROPERTIES = CoreRootFolder + '/cfg/' + 'project_properties.json'
if not os.path.exists(PROJECT_PROPERTIES):
    PROJECT_PROPERTIES = CoreRootFolder + '/proxyCfg/' + 'project_properties.json'
    
#Legacy compatibility
LEGACY_STUDIO_STUCTURE = CoreRootFolder + '/legacyCfg/' + 'studio_structure.json'
LEGACY_STUDIO_PROPERTIES = CoreRootFolder + '/legacyCfg/' + 'studio_properties.json'
LEGACY_STUDIO_NAMINGCONVENTIONS = CoreRootFolder + '/legacyCfg/' + 'studio_namingconventions.json'
LEGACY_FOLDER_STRUCTURE = CoreRootFolder + '/legacyCfg/' + '3D_structure.json'
LEGACY_COMP_FOLDER_STRUCTURE = CoreRootFolder + '/legacyCfg/' + 'Comp_structure.json'
LEGACY_PROJECT_PROPERTIES = CoreRootFolder + '/legacyCfg/' + 'project_properties.json'